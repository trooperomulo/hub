# README #

### What is this repository for? ###

* This is a hub program to be used with the command prompt to route as much of the processing through instead of relying so much on batch files

### How do I get set up? ###

* You will need a file the contains the shortcuts you want to use for navigating to individual solutions
* You will need to define your user parameters in c:\ProgramData\Hub\UserParams.ini
* You will probably want to set up a handful of batch files that just call into Hub.exe, for me those are stored at c:\bat
* The csproj is set up to automatically copy the exe created after building to c:\bat so if you store your batch files elsewhere, you'll need to update, but not commit, the post build event in the csproj file
* There is a help command to help explain the different commands that can be used.  Help with no parameters will just list the different commands.  Help with another command passed as a parameter with give a more detailed description of that command.

### Example .bat files ###

Most of my batch files look identical, I just change the command being passed into the Hub

b.bat
```
#!bat

@call Hub.exe build %*
```

The main one that is different is for the go command, the command to change the working directory:
g.bat

```
#!bat

@echo off
for /f "tokens=*" %%a in (
'call hub.exe go %*'
) do (
echo %%a
set myvar=%%a
)
cd %myvar%
```
This was the only way I could get it to work.  What this does is call hub.exe go with all passed in parameters, then it loops through all the lines that are printed out to the prompt by the hub, printing each one and setting myvar equal to it.  Then at the end, it calls cd with whatever myvar has in it (the last line printed)