﻿using Hub.Commands.Param;

namespace Hub.Decider.Param
{
	/// <summary>
	/// decides which user parameters command to use
	/// </summary>
	public class ParamDecider : BaseDecider<ParamBase> { }

}	// end Hub.decider.Param
