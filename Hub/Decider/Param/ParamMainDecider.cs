﻿using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider.Param
{
	/// <summary>
	/// decides which action to perform
	/// </summary>
	public class ParamMainDecider : ParamDecider
	{
		private static ParamMainDecider _instance;

		private ParamMainDecider() { }

		///
		public static ParamMainDecider Instance => _instance ?? (_instance = new ParamMainDecider());
		
		///
		public void Perform(Args args)
		{
			var command = Decide(args);
			var isDefault = command == DefaultCommand;
			command.Perform(isDefault ? args : GetRest(args));

		}	// end Perform(Args)

	}	// end ParamMainDecider

}	// end Hub.Decider.Param
