﻿using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider.Param
{
	/// <summary>
	/// decides which help to display
	/// </summary>
	public class ParamHelpDecider : ParamDecider
	{
		private static ParamHelpDecider _instance;

		private ParamHelpDecider() { }

		///
		public static ParamHelpDecider Instance => _instance ?? (_instance = new ParamHelpDecider());

		///
		public void Help(Args args) => Decide(args).Help();

	}	// end ParamHelpDecider

}	// end Hub.Decider.Param
