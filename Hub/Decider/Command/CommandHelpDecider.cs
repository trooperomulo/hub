﻿using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider.Command
{
	/// <summary>
	/// decides which help to display
	/// </summary>
	public class CommandHelpDecider : CommandDecider
	{
		private static CommandHelpDecider _instance;

		private CommandHelpDecider() { }

		///
		public static CommandHelpDecider Instance => _instance ?? (_instance = new CommandHelpDecider());

		///
		public void Help(Args args)
		{
			var command = Decide(args);
			var isDefault = command == DefaultCommand;
			command.Help(isDefault ? args : GetRest(args));

		}	// end Help(Args)

	}	// end CommandHelpDecider

}	// end Hub.Decider.Command
