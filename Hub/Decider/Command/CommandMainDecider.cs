﻿using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider.Command
{
	/// <summary>
	/// decides which action to perform
	/// </summary>
	public class CommandMainDecider : CommandDecider
	{
		private static CommandMainDecider _instance;

		private CommandMainDecider() { }

		///
		public static CommandMainDecider Instance => _instance ?? (_instance = new CommandMainDecider());
		
		///
		public void Perform(Args args)
		{
			var command = Decide(args);
			var isDefault = command == DefaultCommand;
			command.Perform(isDefault ? args : GetRest(args));

		}	// end Perform(Args)

	}	// end CommandMainDecider

}	// end Hub.Decider.Command
