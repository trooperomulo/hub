﻿using Hub.Interface;

namespace Hub.Decider.Command
{
	/// <summary>
	/// base class for deciding which command to use
	/// </summary>
	public class CommandDecider : BaseDecider<ICommand> { }

}	// end Hub.Decider.Command
