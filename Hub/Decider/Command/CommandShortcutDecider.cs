﻿using System.Collections.Generic;

namespace Hub.Decider.Command
{
	/// <summary>
	/// class that decides if the user's command requires shortcut
	/// </summary>
	public class CommandShortcutDecider : CommandDecider
	{
		private static CommandShortcutDecider _instance;

		private CommandShortcutDecider() { }

		///
		public static CommandShortcutDecider Instance => _instance ?? (_instance = new CommandShortcutDecider());

		/// <summary>
		/// does the inputted command need shortcuts to be loaded
		/// </summary>
		public bool NeedsShortcut(List<string> args)
		{
			var command = Decide(args);
			return command.NeedShortcut;
		}

	}
}
