﻿using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider
{
	/// <summary>
	/// class that decides which command to perform based on the input arguments
	/// </summary>
	/// <typeparam name="T">the type of the options, must implement <see cref="IInput" /></typeparam>
	public abstract class BaseDecider<T>
		where T : IInput
	{
		/// <summary>
		/// the commands that can be invoked
		/// </summary>
		public static List<T> Commands { get; private set; }
		
		/// <summary>
		/// the command to be called if invalid input is given
		/// </summary>
		protected static T DefaultCommand { get; private set; }
		
		/// <summary>
		/// the command to be called if no input is given
		/// </summary>
		private static T NoneCommand { get; set; }
		
		/// <summary>
		/// initialize the commands, using none for default as well
		/// </summary>
		public static void InitCommands(List<T> commands, T none) => InitCommands(commands, none, none);

		/// <summary>
		/// initialize the commands
		/// </summary>
		public static void InitCommands(List<T> commands, T def, T none)
		{
			Commands = commands;
			DefaultCommand = def;
			NoneCommand = none;

		}	// end InitCommands(List<T>, T, T)

		/// <summary>
		/// takes the first item out of the list and returns the rest
		/// </summary>
		protected static Args GetRest(Args args)
		{
			if (args == null)
			{
				return new Args();
			}
			if (args.Count == 0)
			{
				return args;
			}
			return args.GetRange(1, args.Count - 1);
		}

		/// <summary>
		/// decides which of the options to return based on the input
		/// </summary>
		/// <param name="args">the inputs to check</param>
		protected static T Decide(Args args)
		{
			if (args == null || args.Count < 1)
			{
				return NoneCommand;
			}

			var first = args.First().ToLower();
			var opts = Commands.Where(opt => opt.Inputs.Contains(first)).ToList();
			if (opts.Count < 1)
			{
				return DefaultCommand;
			}

			return opts[0];
		}
	}
}
