﻿using Hub.Commands.Resx;

namespace Hub.Decider.Resx
{

	/// <summary>
	/// decides which action to perform
	/// </summary>
	public class ResxDecider : BaseDecider<ResxBase> { }

}	// end Hub.Decider.Resx
