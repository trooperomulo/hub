﻿using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider.Resx
{
	/// <summary>
	/// decides which help to display
	/// </summary>
	public class ResxHelpDecider : ResxDecider
	{
		private static ResxHelpDecider _instance;

		private ResxHelpDecider() { }

		///
		public static ResxHelpDecider Instance => _instance ?? (_instance = new ResxHelpDecider());

		///
		public void Help(Args args) => Decide(args).Help();
	}
}
