﻿using System;
using System.Collections.Generic;

using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Decider.Resx
{
	/// <summary>
	/// decides which resx action to perform
	/// </summary>
	public class ResxMainDecider : ResxDecider
	{
		#region Variables
		
		private static ResxMainDecider _instance;

		private static Func<Args> _getResxFolders;

		#endregion Variables

		#region Constructor / Initialize

		///
		public static ResxMainDecider Instance => _instance ?? (_instance = new ResxMainDecider());

		///
		public static void Initialize(Func<Args> getRes) => _getResxFolders = getRes;

		#endregion Constructor / Initialize

		#region Methods

		///
		public void Perform(Args args)
		{
			var command = Decide(args);

			if (command == DefaultCommand)
			{
				command.Perform(args, args.Count, Res.None, "");
				return;
			}	// end if

			args = GetRest(args);
			var core = Utils.ReadFirstArg(args, Flags._flagCore);
			var xm8 = Utils.ReadFirstArg(args, Flags._flagXm81);
			var shared = Utils.ReadFirstArg(args, Flags._flagShrd);
			if (!GetResParams(core, xm8, shared, command.ShowAll, out var res, out var path))
			{
				return;
			}	// end if

			command.Perform(args, args.Count, res, path);

		}	// end Perform(Args)

		/// <summary>
		/// determines if core or xm8 was specified to be used
		/// </summary>
		/// <param name="core">core was specified</param>
		/// <param name="xm8">xm8 was specified</param>
		/// <param name="shared">shared was specified</param>
		/// <param name="res">the res files specified</param>
		/// <returns>the number of files specified</returns>
		public static int DetermineUseCore(bool core, bool xm8, bool shared, out Res res)
		{
			res = Res.None;
			var count = 0;

			if (core)
			{
				count++;
				res |= Res.Core;
			}	// end if

			if (xm8)
			{
				count++;
				res |= Res.Xm8;
			}	// end if

			if (shared)
			{
				count++;
				res |= Res.Shared;
			}	// end if

			return count;

		}	// end DetermineUseCore(bool, bool, bool, out Res)

		///
		public static bool GetResParams(bool core, bool xm8, bool shared, bool showAll, out Res res, out string path)
		{
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			var numSols = DetermineUseCore(core, xm8, shared, out res);
			if (numSols > 1)
			{
				// if all are selected, and the command allows for all, then move along
				if (res != Res.All || !showAll)
				{
					path = null;
					Logger.Instance.LogRes(multSol: true);
					Out.Line(showAll
						? "You are only allowed to specify one or all resource solutions."
						: "You are only allowed to specify one or no resource solutions.  Do not specify more than one.");
					return false;
				}	// end if

			}	// end if

			path = GetResPath(dirs, res, showAll);
			if (!path.IsNullOrEmpty())
			{
				if (path == ResxBase.UseAll)
				{
					res = Res.All;
				}

				return true;
			}	// end if

			// path should only be empty if the user didn't respond in time
			Logger.Instance.LogRes(noPath: true);
			return false;

		}	// end GetResParams(bool, bool, bool, bool, out Res, out string)
		
		///
		public static string GetResPath(IDirectories dirs, Res res, bool showAll)
		{
			switch (res)
			{
				case Res.None:
					return AskUserForPath(dirs, showAll);
				case Res.All:
					return ResxBase.UseAll;
				case Res.Core:
					return @"xactimate\xactimate.shared\xm8core\Core.Data.Res";
				case Res.Xm8:
					return @"xactimate\xactimate.shared\xm8core\Xm8.Data.Res";
				case Res.Shared:
					return @"xactimate\xactimate.shared\Shared.Res";
				default:
					return null;
			}	// end switch

		}	// end GetResPath(IDirectories, Res, bool)

		///
		public static string AskUserForPath(IDirectories dirs, bool showAll)
		{
			dirs.SetCurrentDirectory(Env.Root);
			// find all the res directories
			var folders = _getResxFolders();
			if (showAll)
			{
				folders.Insert(0, ResxBase.UseAll);
			}	// end if

			var explanation = new List<string> {"I found these folders that have resx files:"};
			var question = "Which path would you like to use?";
			var waitForUser = IoCContainer.Instance.Resolve<IWaitForUser>();
			var selection = waitForUser.SingleSelection(explanation, folders, question, "Quitting");

			if (selection < 0)
			{
				return "";
			}	// end if

			Out.Blank();
			var selected = folders[selection];
			Out.Line(selected);
			return selected;

		}	// end AskUserForPath(IDirectories, bool)

		#endregion Methods

	}	// end ResxMainDecider

}	// end Hub.Decider.Resx
