﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Hub
{
	/// <summary>
	/// holds references to the environment variables (and stuff built from them)
	/// </summary>
	[ExcludeFromCodeCoverage]
	public static class Env
	{
		#region Properties

		/// <summary>
		/// c:
		/// </summary>
		public static string Local => Environment.GetEnvironmentVariable("local");
		/// <summary>
		/// \git\1\xactimate
		/// </summary>
		public static string Version => Environment.GetEnvironmentVariable("version_tree");
		/// <summary>
		/// debug vs prod
		/// </summary>
		public static string Mode => Environment.GetEnvironmentVariable("mode");
		/// <summary>
		/// the branch number
		/// </summary>
		public static string Repo => Environment.GetEnvironmentVariable("num");
		/// <summary>
		/// the name of the xm8 folder
		/// </summary>
		public static string Branch => Environment.GetEnvironmentVariable("branch");

		/// <summary>
		/// c:\git\1\xactimate
		/// </summary>
		public static string Root => UseRepo ? $@"{Local}\git\{Repo}\xactimate" : $@"{Local}\git\xactimate";

		/// <summary>
		/// c:\git\1\xactimate
		/// </summary>
		public static string Xm8 => $"{Local}{Version}";

		/// <summary>
		/// c:\git\1\xactimate\xm8\bin\debug
		/// </summary>
		public static string BinFolder => $@"{Xm8}\bin\{Mode}";

		/// <summary>
		/// should a repo number be a part of the path
		/// </summary>
		public static bool UseRepo { get; set; }

		#endregion Properties

		#region Methods

		///
		public static string GetBinFilename(string filename) => $@"{BinFolder}\{filename}";

		///
		public static void SetEnvironmentVariable(string var, string val, bool force = false)
		{
			if (Environment.GetEnvironmentVariable(var) == null || force)
			{
				Environment.SetEnvironmentVariable(var, val, EnvironmentVariableTarget.Process);
			}
		}

		#endregion Methods
	}
}
