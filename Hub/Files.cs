﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// manages interactions with files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class Files : IFiles
	{
		/// <inheritdoc />
		public void AppendAllLines(string path, List<string> lines) => File.AppendAllLines(path, lines);

		/// <inheritdoc />
		public void Copy(string source, string destination) => File.Copy(source, destination, true);

		/// <inheritdoc />
		public void Delete(string path) => File.Delete(path);

		/// <inheritdoc />
		public bool Exists(string path) => File.Exists(path);

		/// <inheritdoc />
		public DateTime GetLastWriteTime(string path) => File.GetLastWriteTime(path);

		/// <inheritdoc />
		public Stream GetStream(string path, FileMode fileMode) => new FileStream(path, fileMode);

		/// <inheritdoc />
		public string ReadAllText(string path) => File.ReadAllText(path);

		/// <inheritdoc />
		public IEnumerable<string> ReadLines(string path) => File.ReadLines(path);

		/// <inheritdoc />
		public void WriteAllLines(string path, IEnumerable<string> lines) => File.WriteAllLines(path, lines);

		/// <inheritdoc />
		public void WriteXml(string path, string xml)
		{
			var xDoc = new XmlDocument();
			xDoc.LoadXml(xml);
			xDoc.Save(path);
		}
	}
}
