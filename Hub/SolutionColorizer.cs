﻿using System;
using System.Collections.Generic;
using System.IO;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// makes sure the solution has the proper color
	/// </summary>
	public class SolutionColorizer : ISolutionColorizer
	{
		#region Data Member

		private Dictionary<string, string> _colors = new Dictionary<string, string>();

		#endregion Data Member

		#region Implementation of ISolutionColorizer

		/// <inheritdoc />
		public void ColorizeSolution(string colors, string path, string shortcut)
		{
			ProcessColors(colors);

			// if we don't know what color to do, don't do anything
			var colorLine = GetColorString(shortcut.ToLower());
			if (colorLine.IsNullOrEmpty())
			{
				return;
			}	// end if

			var dir = IoCContainer.Instance.Resolve<IDirectories>();

			var dirName = Path.GetDirectoryName(path);
			// ReSharper disable once AssignNullToNotNullAttribute
			var vsPath = Path.Combine(dirName, ".vs");
			var folder = Path.Combine(vsPath, Path.GetFileNameWithoutExtension(path));
			var file = Path.Combine(folder, "color.txt");

			if (!dir.Exists(vsPath))
			{
				dir.CreateDirectory(vsPath);
			}	// end if

			if (!dir.Exists(folder))
			{
				dir.CreateDirectory(folder);
			}	// end if

			var files = IoCContainer.Instance.Resolve<IFiles>();

			// we haven't opened this solution before
			if (!files.Exists(file))
			{
				// create the file
				files.AppendAllLines(file, new List<string>{colorLine});
				return;
			}	// end if

			// we have opened it before, and it has the correct color already set
			if (files.ReadAllText(file).Contains(colorLine))
			{
				return;
			}	// en dif

			if (files.ReadAllText(file).Contains("master"))
			{
				// file has an entry, but the wrong color, so remove the old file and make a new one
				files.WriteAllLines(file, new List<string> { colorLine });
				return;
			}	// end if
			
			// file had other entries, but not the one we want, add it
			files.AppendAllLines(file, new List<string> { colorLine });
			

		}	// end ColorizeSolution(string, string, string)

		#endregion Implementation of ISolutionColorizer

		#region Helpers

		private string GetColorString(string shortcut)
		{
			var repo = Env.Repo;
			var comb = $"{shortcut}-{repo}";
			string color = "";
			if (_colors.ContainsKey(comb))
			{
				color = _colors[comb];
			}	// end if

			else if (_colors.ContainsKey(shortcut))
			{
				color = _colors[shortcut];
			}	// end else if

			else if (_colors.ContainsKey(repo))
			{
				color = _colors[repo];
			}	// end else if

			if (color.IsNullOrEmpty())
			{
				Out.Line($"no color defined for shortcut {shortcut} or repo {repo} or {comb}");
				return "";
			}	// end if

			return $"master:{color}";

		}	// end GetColorString(string)

		private void ProcessColors(string colors)
		{
			/* string should be in the following format
				s = shortcut
				r = repo
				c = combo
				
				s:color|r:color|s:color|s:color|c:color|c:color|r:color|r:color|r:color
			*/

			var pairs = colors.Split('|', StringSplitOptions.RemoveEmptyEntries);
			if (pairs.Length == 0)
			{
				return;
			}	// end if

			if (_colors.Count > 0)
			{
				// if we've there are already values in the dictionary, then we're probably just launching multiple solutions and we already processed the preferences.
				return;
			}

			foreach (var pair in pairs)
			{
				var parts = pair.Split(':');
				if (parts.Length != 2)
				{
					continue;
				}   // end if

				var color = parts[1];

				if (_colors.ContainsKey(parts[0]))
				{
					Out.Line($"{parts[0]} was already set to {_colors[parts[0]]}.  Changing to {color}");
					_colors[parts[0]] = color;
				}	// end if
				else
				{
					_colors.Add(parts[0], color);
				}	// end else

			}	// end foreach

		}	// end ProcessColors(string)

		#endregion Helpers
	}
}
