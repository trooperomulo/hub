﻿using System;
using System.Linq;

using Hub.Enum;

namespace Hub
{
	/// <summary>
	/// prepares strings for display
	/// </summary>
	public static class Prep
	{
		#region Methods

		/// <summary>
		/// surrounds the given text with square brackets
		/// </summary>
		public static string Brackets(params string[] input) => $"[{Combine(" ", input)}]";

		/// <summary>
		/// puts a space before the value and fills out the rest of the width with spaces
		/// </summary>
		/// <param name="value">the string to be left aligned inside the column</param>
		/// <param name="width">the total width of the column</param>
		/// <returns></returns>
		public static string ColumnLeft(string value, int width)
		{
			var available = width - 2;  // since we want a space before and after any text, we need to remove 2 from the width to see what is available
			if (value.Length > available)
			{
				return $" {value.Substring(0, available)} ";
			}

			return FillOut($" {value}", width);
		}

		/// <summary>
		/// removes the front portion off of the given string and returns it
		/// </summary>
		/// <param name="len">how long is the portion that is being cut off</param>
		/// <param name="remaining">the string the portion will be cut off of</param>
		/// <returns>the portion that was cut off</returns>
		public static string CutChunkOff(int len, ref string remaining)
		{
			if (remaining.Length == 0)
			{
				return "";
			}

			if (remaining.Length <= len)
			{
				var all = remaining;
				remaining = "";
				return all;
			}

			var chunk = remaining.Substring(0, len);
			remaining = remaining.Substring(len);
			return chunk;
		}

		/// <summary>
		/// describes an argument
		/// </summary>
		/// <param name="main">the argument</param>
		/// <param name="description">the description</param>
		/// <returns></returns>
		public static string Describe(string main, string description) => $"{main}\t- {description}";

		/// <summary>
		/// combines the two parts with an equals sign
		/// </summary>
		public static string Equal(string front, string back) => $"{front}={back}";

		/// <summary>
		/// combines the two parts with an equals sign
		/// </summary>
		public static string Equal(string front, int back) => Equal(front, back.ToString());

		/// <summary>
		/// adds enough spaces to the end of the string to match the given width
		/// </summary>
		public static string FillOut(string str, int width) => FillOut(str, width, ' ');
		
		/// <summary>
		/// adds enough spaces to the end of the string to match the given width
		/// </summary>
		public static string FillOut(string str, int width, char ch) => $"{str}{string.Concat(Enumerable.Repeat(ch, width - str.Length))}";

		/// <summary>
		/// Gets a header string for each repo
		/// </summary>
		public static string GetHeaderForRepo(Repository repo)
		{
			switch (repo)
			{
				case Repository.Xm8:
					return "Xm8";
				case Repository.Data:
					return "Data";
				default:
					throw new ArgumentOutOfRangeException(nameof(repo));
			}
		}

		/// <summary>
		/// create the path to a language resource file
		/// </summary>
		/// <param name="path">the solution path</param>
		/// <param name="lang">the language</param>
		/// <returns></returns>
		public static string Lang(string path, string lang) => $@"{Env.Root}\{path}\strings.{lang}.resx";

		/// <summary>
		/// gets the unit test log file for the given dll
		/// </summary>
		public static string Log(string dll) => $@"{Env.BinFolder}\Unit Test Results\{dll}.dll.NUnit.log";

		/// <summary>
		/// adds a minus to the given string
		/// </summary>
		/// <param name="back">text to prepend the minus to</param>
		/// <param name="addSpace">should a space be added between the minus and the text</param>
		public static string Minus(string back, bool addSpace = false) => addSpace ? $" - {back}" : $"-{back}";

		/// <summary>
		/// surrounds the given text with parentheses
		/// </summary>
		public static string Paren(string str) => $"({str})";

		/// <summary>
		/// surrounds the given parts with parentheses
		/// </summary>
		public static string Paren(string front, string back) => Paren($"{front} {back}");

		/// <summary>
		/// combines all the inputs with a slash
		/// </summary>
		public static string Slash(params string[] inputs) => Combine("/", inputs);

		/// <summary>
		/// Gets unit test xml file for the given dll
		/// </summary>
		public static string Xml(string dll) => $@"{Env.BinFolder}\Unit Test Results\{dll}.dll.NUnit.xml";

		#endregion Methods

		#region Helper

		private static string Combine(string separator, params string[] inputs) => inputs?.Aggregate("", (c, i) => $"{c}{separator}{i}").Substring(separator.Length) ?? "";

		#endregion Helper

	}	// end Prep

}	// end Hub
