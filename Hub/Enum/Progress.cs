﻿namespace Hub.Enum
{
	/// <summary>
	/// how should unit test progress be displayed
	/// </summary>
	public enum Progress
	{
		/// <summary>do not display progress at all</summary>
		None,
		/// <summary>percentage of total solutions completed</summary>
		Solution,
		/// <summary>per solution, but weighted by estimate time</summary>
		Time
	}
}
