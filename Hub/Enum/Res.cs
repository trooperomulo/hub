﻿using System;

namespace Hub.Enum
{
	/// <summary>
	/// The different Res folder s
	/// </summary>
	[Flags]
	public enum Res
	{
		///
		None =   0,
		///
		Xm8 =    1 << 0,
		///
		Core =   1 << 1,
		///
		Shared = 1 << 2,

		///
		All = Xm8 | Core | Shared

	}	// end Res

}	// end Hub.Enum
