﻿namespace Hub.Enum
{
	/// <summary>
	/// should we display output when running external programs
	/// </summary>
	public enum ShowOutput
	{
		/// <summary>show all output</summary>
		Yes,
		/// <summary>only show error output</summary>
		Errors,
		/// <summary>don't show any output</summary>
		No
	}
}
