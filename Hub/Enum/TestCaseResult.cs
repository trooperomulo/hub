﻿namespace Hub.Enum
{
	/// <summary>
	/// the result of the test case
	/// </summary>
	public enum TestCaseResult
	{
		/// <summary>test passed</summary>
		Pass,
		/// <summary>Test failed</summary>
		Fail,
		/// <summary>test was skipped</summary>
		Skip,
		/// <summary>results were inconclusive</summary>
		Incl
	}
}
