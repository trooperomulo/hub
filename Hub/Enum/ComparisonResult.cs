﻿namespace Hub.Enum
{
	/// <summary>
	/// the result of comparing two parsers
	/// </summary>
	public enum ComparisonResult
	{
		/// <summary>nothing changed</summary>
		Same,
		/// <summary>unit was added</summary>
		Added,
		/// <summary>unit was removed</summary>
		Removed,
		/// <summary>unit didn't pass but now it does</summary>
		Fixed,
		/// <summary>unit passed but now it doesn't</summary>
		Broken,
		/// <summary>unit still doesn't pass, but is different</summary>
		BrokenDifferently
	}
}
