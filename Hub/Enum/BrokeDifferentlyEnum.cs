﻿namespace Hub.Enum
{
	/// <summary>
	/// how is the unit broken differently
	/// </summary>
	public enum BrokeDifferentlyEnum
	{
		/// <summary>not broken differently</summary>
		Not,
		/// <summary>was failing, now it's inconclusive</summary>
		FailToIncl,
		/// <summary>was failing, now it skips</summary>
		FailToSkip,
		/// <summary>was inconclusive, now it skips</summary>
		InclToSkip,
		/// <summary>was inconclusive, now it fails</summary>
		InclToFail,
		/// <summary>was skipping, now it's inconclusive</summary>
		SkipToIncl,
		/// <summary>was skipping, now it fails</summary>
		SkipToFail,
		/// <summary>still skipping, but with a different message</summary>
		SkipDiff,
		/// <summary>still failing, but with a different call stack</summary>
		FailCallStack,
		/// <summary>still failing, but with a different message</summary>
		FailMessage
	}
}
