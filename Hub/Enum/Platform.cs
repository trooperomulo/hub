﻿using System;

namespace Hub.Enum
{
	/// <summary>
	/// platforms specified by the user
	/// </summary>
	[Flags]
	public enum Platform
	{
		///
		None	= 0,
		///
		Windows	= 1,
		///
		Android	= 2,
		///
		iOS		= 4

	}	// end Platform

}	// end Hub.Enum