﻿using System.Collections.Generic;

using Hub.Decider.Resx;
using Hub.Enum;
using Hub.Logging;

namespace Hub.Commands.Resx
{
	///
	public class ResxNoneCommand : ResxBase
	{
		#region Overrides of ResxBase

		/// <inheritdoc />
		public override string Description => "Used to perform one of various actions on resx files";

		/// <inheritdoc />
		public override string Input { get; } = "";

		/// <inheritdoc />
		public override bool ShowAll => false;

		/// <inheritdoc />
		public override void Help()
		{
			// resx/re -a/-f/-r/-s/-t

			NoneCommand.DoNoneHelp(ResxDecider.Commands, Action, Description);

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args, int count, Res res, string path)
		{
			Logger.LogRes(noAct: true);

			NoneCommand.DoNonePerform("Resx", Help, ResxDecider.Commands);

		}	// end Perform(List<string>, int, Res, string)

		#endregion Overrides of ResxBase
	}
}
