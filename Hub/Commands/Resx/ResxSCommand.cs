﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Enum;

namespace Hub.Commands.Resx
{
	/// <summary>
	/// sort a resource
	/// </summary>
	public class ResxSCommand : ResxBase
	{
		#region Property

		/// <summary>
		/// function to actually sort a resx file
		/// </summary>
		public static Func<string, bool> SortResFile { private get; set; }

		#endregion Property

		#region Overrides of ResxBase

		/// <inheritdoc />
		public override string Description => "Sort the resx files";

		/// <inheritdoc />
		public override string Input => "-s";

		/// <inheritdoc />
		public override bool ShowAll => false;

		/// <inheritdoc />
		public override void Help()
		{
			// resx/re -s [-c/-x/-s]
			var sAction = $"{Action} {Input}";
			Out.Example(sAction, Reses);
			Out.Explain("Used to sort the resx files", sAction, () =>
			{
				Out.Indent(Prep.Describe(Flags._flagCore, "Sort the files in Core.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Sort the files in Xm8.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Sort the files in Shared.Res"));
				Out.Indent("If none are specified, you will be prompted", 2);
			});
		}

		/// <inheritdoc />
		public override void Perform(List<string> args, int count, Res res, string path)
		{
			Logger.LogResS(res);
			DoResS(path);
		}

		#endregion Overrides of ResxBase

		#region Constructor

		///
		public ResxSCommand() => SortResFile = SortResxFile;

		#endregion Constructor

		#region Methods

		private static void DoResS(string path) => SortResxFiles(null, path);

		///
		public static void SortResxFiles(string solution) => SortResxFiles(solution, "");

		///
		private static void SortResxFiles(string solution, string path)
		{
			if (path.IsNullOrEmpty())
			{
				path = Dirs.GetCurrentDirectory();
			}

			if (!solution.IsNullOrEmpty())
			{
				Out.Write($"Sorting {solution}");
			}

			var files = Dirs.GetFiles($@"{Env.Root}\{path}", "*.resx");
			var warn = files.Aggregate(false, (current, file) => current | SortResFile(file));

			if (!solution.IsNullOrEmpty())
			{
				Out.Line(" - done");
			}

			if (warn)
			{
				Out.Line("At least one file didn't get its comment copied over");
			}
		}

		#endregion Methods
	}
}
