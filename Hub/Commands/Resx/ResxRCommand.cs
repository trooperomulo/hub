﻿using System.Collections.Generic;
using System.Resources;

using Hub.Enum;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands.Resx
{
	/// <summary>
	/// remove a resource
	/// </summary>
	public class ResxRCommand : ResxBase
	{
		#region Overrides of ResxBase

		/// <inheritdoc />
		public override string Description => "Remove an existing resource";

		/// <inheritdoc />
		public override string Input => "-r";

		/// <inheritdoc />
		public override bool ShowAll => false;

		/// <inheritdoc />
		public override void Help()
		{
			// resx/re -r [-c/-x/-s] (key)
			var rAction = $"{Action} {Input}";
			var key = Prep.Paren("key");
			Out.Example(rAction, Reses, key);
			Out.Explain("Used to remove an existing resource from a resx file", rAction, () =>
			{
				Out.Line(Reses);
				Out.Indent(Prep.Describe(Flags._flagCore, "Remove from Core.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Remove from Xm8.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Remove from Shared.Res"));
				Out.Indent("If none are specified, you will be prompted", 2);
				Out.Line(Prep.Describe(key, "the key of the resource to be removed"));
			});
		}

		/// <inheritdoc />
		public override void Perform(List<string> args, int count, Res res, string path)
		{
			if (count < 1)
			{
				Logger.LogRes(noKeys: true);
				return;
			}
			Logger.LogResR(res, count);
			DoResR(path, args);
		}

		#endregion Overrides of ResxBase

		#region Methods

		private void DoResR(string path, Args keys)
		{
			var files = GetResFiles(path, false);
			RemoveResourcesFromFile(keys, $@"{Env.Root}\{path}\strings.resx");
			foreach (var lang in files)
			{
				RemoveResourcesFromFile(keys, Prep.Lang(path, lang));
			}
		}

		private void RemoveResourcesFromFile(Args keys, string file)
		{
			var writer = new ResXResourceWriter(file);
			PopulateResxWriterAndDictionaries(file, writer, keys);
			writer.Dispose();	// dispose writes first thing
		}

		#endregion Methods
	}
}
