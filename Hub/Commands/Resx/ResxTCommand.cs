﻿using System;
using System.Linq;

using Hub.Commands.Helpers.Resx;
using Hub.Enum;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands.Resx
{
	/// <summary>
	/// find translations
	/// </summary>
	public class ResxTCommand : ResxBase
	{
		#region Variable
		
		private bool _firstLang = true;

		#endregion Variable

		#region Property

		///
		public static Func<ResxTResults, ResxTResultsDisplayer> GetDisplayer { private get; set; }

		#endregion Property

		#region Overrides of ResxBase

		/// <inheritdoc />
		public override string Description => "Find the translations of a resource";

		/// <inheritdoc />
		public override string Input => "-t";

		/// <inheritdoc />
		public override bool ShowAll => true;

		/// <inheritdoc />
		public override void Help()
		{
			// resx/re -t [-c/-x/-s] (key)
			var tAction = $"{Action} {Input}";
			var keys = Prep.Paren("key");
			Out.Example(tAction, Reses, keys);
			Out.Explain("Used to find the translations of a resource", tAction, () =>
			{
				Out.Line(Reses);
				Out.Indent(Prep.Describe(Flags._flagCore, "Find in Core.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Find in Xm8.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Find in Shared.Res"));
				Out.Indent("If none are specified, you will be prompted", 2);
				Out.Line(Prep.Describe(keys, "the key of the resource whose translations should be found"));
			});
		}

		/// <inheritdoc />
		public override void Perform(Args args, int count, Res res, string path)
		{
			if (count < 1)
			{
				Logger.LogRes(noTVals: true);
				return;
			}	// end if
			Logger.LogResT(res, count);
			DoResT(args, res == Res.All ? "" : path);

		}	// end Perform(Args, int, Res, string)

		#endregion Overrides of ResxBase

		#region Methods

		private void DoResT(Args keys, string path)
		{
			var folders = path.IsNullOrEmpty() ? GetResFolders() : new Args {path};
			var displayer = GetDisplayer(FindTranslations(keys, folders));
			displayer.PrintResults();
		}

		private ResxTResults FindTranslations(Args keys, Args folders)
		{
			var results = new ResxTResults(keys, folders);
			folders.Sort();

			foreach (var folder in folders)
			{
				FindTranslations(keys, folder, results);
			}

			return results;
		}

		private void FindTranslations(Args keys, string folder, ResxTResults results)
		{
			Out.Line($"Searching {folder}");
			var files = GetResFiles(folder, false);
			files.Sort();
			foreach (var lang in files)
			{
				FindTranslations(keys, folder, lang, results);
			}
			Out.Blank();
			_firstLang = true;
		}

		private void FindTranslations(Args keys, string folder, string lang, ResxTResults results)
		{
			Out.Write($"{(_firstLang ? "\t" : ", ")}{lang}");
			_firstLang = false;
			var file = Prep.Lang(folder, lang);
			var ktv = keys.ToDictionary(k => k, k => "");
			PopulateResxWriterAndDictionaries(file, ktv);
			results.AddResultsFromLang(folder, lang, ktv);
		}

		#endregion Methods
	}
}
