﻿using System.Collections.Generic;
using System.Linq;

using Hub.Enum;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands.Resx
{
	/// <summary>
	/// looks for a resource
	/// </summary>
	public class ResxFCommand : ResxBase
	{
		#region Overrides of ResxBase

		/// <inheritdoc />
		public override string Description => "Find an existing resource";

		/// <inheritdoc />
		public override string Input => "-f";

		/// <inheritdoc />
		public override bool ShowAll => true;

		/// <inheritdoc />
		public override void Help()
		{
			// resx/re -f [-c/-x/-s] (value)
			var fAction = $"{Action} {Input}";
			var val = Prep.Paren("value");
			Out.Example(fAction, Reses, val);
			Out.Explain("Used to find a resource in a resx file", fAction, () =>
			{
				Out.Line(Reses);
				Out.Indent(Prep.Describe(Flags._flagCore, "Look in Core.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Look in Xm8.Data.Res"));
				Out.Indent(Prep.Describe(Flags._flagXm81, "Look in Shared.Res"));
				Out.Indent("If none are specified, you will be prompted", 2);
				Out.Line(Prep.Describe(val, "the value to be found"));
				Out.Indent("When selecting the Res folder, you can search one folder, or all", 2);
				Out.Indent("When search all Res folders, results will be displayed per folder", 2);
			});
		}

		/// <inheritdoc />
		public override void Perform(List<string> args, int count, Res res, string path)
		{
			if (count < 1)
			{
				Logger.LogRes(noFVals: true);
				return;
			}
			Logger.LogResF(res, count);
			DoResF(res == Res.All ? "" : path, args);
		}

		#endregion Overrides of ResxBase

		#region Methods

		private void DoResF(string path, Args vals)
		{
			if (!path.IsNullOrEmpty())
			{
				FindValuesInFile(path, vals);
				return;
			}

			var folders = GetResFolders();
			folders.Sort();
			foreach (var folder in folders)
			{
				FindValuesInFile(folder, vals);
			}
		}

		private void FindValuesInFile(string path, Args vals)
		{
			Out.Blank();
			Out.Dash();
			Out.Line(path);
			Out.Blank();
			FindValuesInFile(EnUs, $@"{Env.Root}{path}\strings.resx", vals);
		}

		private void FindValuesInFile(string lang, string path, Args values)
		{
			Out.Line(lang);
			var vtk = values.ToDictionary(v => v, v => new List<string>());
			PopulateResxWriterAndDictionaries(path, vtk);
			RemoveEmptyValues(vtk);

			if (vtk.Count == 0)
			{
				Out.Indent("No matches found");
				return;
			}

			var keys = vtk.Keys.ToList();
			keys.Sort();
			foreach (var key in keys)
			{
				Out.Line(key);
				vtk[key].Sort();
				foreach (var item in vtk[key])
				{
					Out.Indent(item);
				}
			}
		}

		private static void RemoveEmptyValues(Dictionary<string, List<string>> vtk)
		{
			var keys = vtk.Keys.ToList();
			foreach (var key in keys)
			{
				var list = vtk[key];
				if (list.Count == 0)
				{
					vtk.Remove(key);
				}
			}
		}

		#endregion Methods
	}
}
