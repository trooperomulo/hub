﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Xml;
using System.Xml.Linq;

using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

// ReSharper disable VirtualMemberNeverOverridden.Global
namespace Hub.Commands.Resx
{
	///
	public abstract class ResxBase : IInput, ISubCommand
	{
		#region Variables

		private Args _inputs;

		// ReSharper disable once InconsistentNaming
		internal static readonly Args _ins = new Args {"resx", "re"};

		///
		protected static readonly string Action = Prep.Slash(_ins.ToArray());

		///
		protected readonly string Reses = Prep.Brackets(Prep.Slash(Flags._flagCore, Flags._flagXm81, Flags._flagShrd));

		///
		protected static Logger Logger => Logger.Instance;

		///
		protected static IDirectories Dirs;

		///
		protected const string EnUs = "en-US";

		///
		public const string UseAll = "Use All";

		private static Action<XElement, string> _saveElement;
		
		#endregion Variables

		#region Implementation of ICommand

		/// <summary>
		/// a short description of the command
		/// </summary>
		public abstract string Description { get; }

		/// <summary>
		///  the input for this command
		/// </summary>
		public abstract string Input { get; }

		/// <inheritdoc />
		public Args Inputs => _inputs ?? (_inputs = new Args {Input});

		/// <summary>
		/// is all resx an option
		/// </summary>
		public abstract bool ShowAll { get; }

		/// <summary>
		/// the help describing this command
		/// </summary>
		public abstract void Help();

		/// <summary>
		/// performs that actual command
		/// </summary>
		public abstract void Perform(Args args, int count, Res res, string path);

		#endregion Implementation of ICommand

		#region Constructor

		///
		public static void Initialize(Action<XElement, string> saveElement)
		{
			Dirs = IoCContainer.Instance.Resolve<IDirectories>();
			_saveElement = saveElement;
		}

		#endregion Constructor

		#region Methods

		private static string GetResFile(FileInfo info, bool returnEnUs)
		{
			var parts = info.Name.Split('.');
			// foreign languages look like this strings.es-ES.resx
			// US English is strings.resx
			if (parts.Length == 3)
			{
				// ignore string exception files
				return parts[0].Contains("exception") ? null : parts[1];
			}

			if (returnEnUs && parts.Length == 2)
			{
				return "en-US";
			}

			return null;
		}

		///
		public virtual Args GetResFiles(string path, bool returnEnUs)
		{
			var folder = $"{Env.Root}\\{path}";
			var files = Dirs.GetFileInfos(folder, "*.resx");
			return files.Select(info => GetResFile(info, returnEnUs)).Where(f => !f.IsNullOrEmpty()).OrderBy(t => t).ToList();
		}

		private static string GetResFolder(DirectoryInfo info)
		{
			var lower = info.FullName.ToLower();

			// we don't care about online or unit test
			if (lower.Contains("online") || lower.Contains("unit test"))
			{
				return null;
			}

			// remove xv folders
			if (lower.Contains("xv"))
			{
				return null;
			}

			// we're getting some pictures folders
			if (lower.EndsWith("pictures"))
			{
				return null;
			}

			// sometimes we get files that don't actually end with res
			if (!lower.EndsWith("res"))
			{
				return null;
			}

			// we don't care about any folder that doesn't have any resx files
			if (Dirs.GetFiles(info.FullName, "*.resx").Count < 1)
			{
				return null;
			}

			return info.FullName.Substring(Env.Root.Length);
		}

		///
		public virtual Args GetResFolders()
		{
			var subs = Dirs.GetDirectories(Env.Root, "*Res");
			return subs.Select(GetResFolder).Where(f => !f.IsNullOrEmpty()).ToList();
		}

		///
		public virtual void PopulateResxWriterAndDictionaries(string file, ResXResourceWriter writer, Args keysToNotAdd)
		{
			PopulateResxWriterAndDictionaries(file, writer, null, null, keysToNotAdd);
		}

		///
		public virtual void PopulateResxWriterAndDictionaries(string file, ResXResourceWriter writer,
			Dictionary<string, string> keyToVal, Dictionary<string, Args> valToKey)
		{
			PopulateResxWriterAndDictionaries(file, writer, keyToVal, valToKey, null);
		}

		///
		public virtual void PopulateResxWriterAndDictionaries(string file, Dictionary<string, string> keyToVal)
		{
			PopulateResxWriterAndDictionaries(file, null, keyToVal, null, null);
		}

		///
		public virtual void PopulateResxWriterAndDictionaries(string file, Dictionary<string, Args> valToKey)
		{
			PopulateResxWriterAndDictionaries(file, null, null, valToKey, null);
		}

		///
		public virtual void PopulateResxWriterAndDictionaries(string file, ResXResourceWriter writer,
			Dictionary<string, string> keyToVal, Dictionary<string, Args> valToKey, Args keysToNotAdd)
		{
			if (writer == null && keyToVal == null && valToKey == null)
			{
				// if those are all empty, we don't need to do anything
				Out.Line($"{nameof(PopulateResxWriterAndDictionaries)} was called but was given nothing to do");
				return;
			}

			using (var reader = new ResXResourceReader(file))
			{
				reader.UseResXDataNodes = true;
				foreach (var node in (from DictionaryEntry entry in reader select entry.Value).OfType<ResXDataNode>())
				{
					ProcessNode(node, writer, keyToVal, valToKey, keysToNotAdd);
				}
			}
		}

		// ReSharper disable SuggestBaseTypeForParameter
		private static void ProcessNode(ResXDataNode node, ResXResourceWriter writer, Dictionary<string, string> keyToVal,
			Dictionary<string, Args> valToKey, Args keysToNotAdd)
		{
			var key = node.Name;
			if (keysToNotAdd != null && keysToNotAdd.Contains(key))
			{
				keyToVal?.Remove(key);
				valToKey?.Remove(key.ToLower());
				return;
			}

			writer?.AddResource(node);

			if (keyToVal == null && valToKey == null)
			{
				return;
			}

			var value = node.GetValue((AssemblyName[]) null);
			if (value == null)
			{
				return;
			}

			var val = value.ToString();

			keyToVal?.Update(key, val);
			valToKey?.AddIfExists(val, key);
		}
		// ReSharper restore SuggestBaseTypeForParameter

		private static IEnumerable<XElement> SortElements(IEnumerable<XElement> elements)
		{
			var xElements = elements as XElement[] ?? elements.ToArray();

			var results = new List<XElement>();

			results.AddRange(xElements.Where(element => !element.Name.ToString().Equals("data")).ToList());
			results.AddRange(xElements.Where(element => element.Name.ToString().Equals("data")).OrderBy(e => e.Attribute("name")?.Value));

			return results;
		}

		///
		public static bool SortResxFile(string file)
		{
			var warn = false;
			var files = IoCContainer.Instance.Resolve<IFiles>();
			var stream = files.GetStream(file, FileMode.Open);
			var root = XElement.Load(stream);
			var comment = root.FirstNode.NodeType == XmlNodeType.Comment ? root.FirstNode : null;
			var sortedElements = SortElements(root.Elements());
			var sortedRoot = new XElement(root.Name, root.Attributes(), sortedElements);
			if (comment == null)
			{
				warn = true;
			}
			else
			{
				sortedRoot.AddFirst(comment);
			}

			stream.Close();
			sortedRoot.Save(file);
			_saveElement(sortedRoot, file);
			return warn;
		}

		#endregion Methods
	}
}
