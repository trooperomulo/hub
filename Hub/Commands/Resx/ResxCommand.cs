﻿using System.Collections.Generic;

using Hub.Decider.Resx;
using Hub.Interface;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands.Resx
{
	/// <summary>
	/// perform one of the resource file commands
	/// </summary>
	public class ResxCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Modify a resx file";

		/// <inheritdoc />
		public bool HasSubCommands => true;

		/// <inheritdoc />
		public Args Inputs => ResxBase._ins;

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(Args args) => ResxHelpDecider.Instance.Help(args);

		/// <inheritdoc />
		public void Perform(Args args) => ResxMainDecider.Instance.Perform(args);

		#endregion Implementation of ICommand

		#region Constructor
		
		///
		public ResxCommand(List<ResxBase> resxes, ResxBase none) => ResxDecider.InitCommands(resxes, none);

		#endregion Constructor
	}
}
