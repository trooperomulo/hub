﻿using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// command for running a full build that can be resumed after fixing a broken solution
	/// </summary>
	public class ResumableFullBuildCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Runs a resumable full build";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new Args {"rb", "resume", "rfull"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// resume/rfull/rf
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action);
			Out.Explain("Runs a full build that can be resumed after fixing a broken solution", action, () => { });
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			Logger.Instance.LogRfb();

			DoResumableBuild();
		}

		#endregion Implementation of ICommand

		#region Method

		private void DoResumableBuild()
		{
			IoCContainer.Instance.Resolve<IProgram>().RunBatchFile("medusa.exe -o build.xml", rso: false);
			IoCContainer.Instance.Resolve<IConsole>().Beep();
		}

		#endregion Method
	}
}
