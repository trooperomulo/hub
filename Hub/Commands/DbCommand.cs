﻿using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// launches the db reader tool
	/// </summary>
	public class DbCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Launch the DB tool";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"db", "d"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// db/d
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action);
			Out.Explain("Launches the Xact Support database tool", action, () => { });
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			Logger.Instance.LogDb();
			DoDb();
		}

		#endregion Implementation of ICommand

		#region Method

		private static void DoDb() => IoCContainer.Instance.Resolve<IProgram>().LaunchBinProgram("XactSupport.exe", "XactSupport Database tool", wait: false);

		#endregion Method
	}
}
