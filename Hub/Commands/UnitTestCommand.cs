﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// run unit tests
	/// </summary>
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global - used for mocking in unit tests
	public class UnitTestCommand : ICommand
	{
		#region Variables

		private readonly IFiles _files;
		private readonly IDirectories _dirs;
		private readonly IProgram _prog;

		private readonly Action _trk;
		private readonly Action _xm8;

		private readonly bool _trackUnitTestTime;

		private const string FileFormat = "{0}.xml";
		private readonly string _results = Path.Combine(Hub.ProgData, "Results");

		#endregion  Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Run Unit Tests";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"unit", "test", "u"};

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// user param - track how long each solution takes

			// show no progress, progress by solution, progress by estimated time
			// hide time, show time
			// show in console, show in txt file, save xml
			// no input, input

			// unit/u/test [-e (-p n/s/t) -tm -rc -rt -rx (-i file) -x/l -tr/dm] [solutions]
			var action = Prep.Slash(Inputs.ToArray());
			var tEst = Flags._flagTEst;
			var prog = Prep.Paren(Flags._flagProg, Prep.Slash(Flags._fPrNo, Flags._fPrSl, Flags._fPrTm));
			var time = Flags._flagShTm;
			var resCon = Flags._flagRsCn;
			var resTxt = Flags._flagRsTx;
			var resXml = Flags._flagRsXm;
			var input = Prep.Paren(Flags._flagInpt, "fn");
			var options = Prep.Brackets($"{tEst} {prog} {time} {resCon} {resTxt} {resXml} {input} {Flags._flagXm8_} {Flags._flagTrck}");
			Out.Example(action, options, Hub.Solutions);
			Out.Explain("Runs unit tests for specified solutions and displays the results, does not build", action, () =>
			{
				Out.Line(options);
				Out.Indent(Prep.Describe(tEst, "Show an estimate of how long the tests will take to run"));
				Out.Indent(Prep.Describe(Flags._flagProg, "How is progress to be shown, must give a value:"));
				Out.Indent(Prep.Describe(Flags._fPrNo, "Do not display any progress, ignores tm"), 2);
				Out.Indent(Prep.Describe(Flags._fPrSl, "Display progress # sols completed/total soltuions, DEFAULT"), 2);
				Out.Indent(Prep.Describe(Flags._fPrTm, "Display progress based on estimated completion time"), 2);
				Out.Indent(Prep.Describe(time, "Show the time each solution took to run in the progress"));
				Out.Indent(Prep.Describe(resCon, "Display the results in the console"));
				Out.Indent(Prep.Describe(resTxt, "Display the results in a txt file"));
				Out.Indent(Prep.Describe(resXml, "Save the results to an xml file (to be used for future comparisons)"));
				Out.Indent(Prep.Describe(input, "int number of xml file of previous run to compare against, 0 to use most recent"));
				Out.Indent(Prep.Describe(Flags._flagXm8_, "launch Xactimate"));
				Out.Indent(Prep.Describe(Flags._flagTrck, "launch the trickler"));
				Out.Line(Prep.Describe(Hub.Solutions, "will be run in FIFO order, must specify something"));
				Out.Indent("to run the tests for all solutions, specify 'all'", 2);
				Out.Blank();
				Out.Line("Notes about saved times:");
				Out.Indent("Run times will be saved for the following scenarios:", 2);
				Out.Indent($"{nameof(UserParams)}.{nameof(UserParams.TrackUnitTestTime)} is set to true", 3);
				Out.Indent("Progress has been set to be by time", 3);
				Out.Blank();
				Out.Indent($"Times will be saved to {TestTimeTracker.Path}", 2);
				Out.Blank();
				Out.Indent("If a solution doesn't have any saved run times,", 2);
				Out.Indent($"then a default time of {TestTimeTracker.DefaultTime} seconds will be used", 2);
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			var xm8 = Utils.HasXm8Arg(args);
			var trk = Utils.HasTrkArg(args);

			var settings = new UnitTestSettings();
			settings.LoadArguments(args);

			Logger.Instance.LogUnit(settings.EstimateTime, settings.Progress == Progress.Solution,
				settings.Progress == Progress.Time, settings.ResultsConsole, settings.ResultsTextFile,
				settings.ResultsXmlFile, settings.InputFile > -1, xm8, trk, args.Count);

			DoUnit(args, settings);

			if (xm8)
			{
				_xm8();
			}
			if (trk)
			{
				_trk();
			}
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public UnitTestCommand(bool trackUtt, Action xm8, Action trk)
		{
			_trackUnitTestTime = trackUtt;

			_files = IoCContainer.Instance.Resolve<IFiles>();
			_dirs = IoCContainer.Instance.Resolve<IDirectories>();
			_prog =IoCContainer.Instance.Resolve<IProgram>();

			_xm8 = xm8;
			_trk = trk;
		}

		#endregion Constructor

		#region Methods

		///
		public virtual void CompareResults(UnitTestResultParser previous, UnitTestResultParser current, int input)
		{
			var comparer = new UtrpComparer(previous, current);
			var lines = comparer.GetLines();

			var results = $@"Unit Test Results\Comparison {input} {DateTime.Now:yy-MM-dd}.log";

			_files.WriteAllLines(results, lines);
			_prog.StartProcess(results);
		}

		///
		public virtual void DetermineTimeEstimate(Args solutions, bool printEstimate, out TestTimeTracker ttt,
			out TestProgressTimeDisplayer displayer)
		{
			ttt = new TestTimeTracker(_files, _dirs);
			displayer = new TestProgressTimeDisplayer();
			foreach (var sol in solutions)
			{
				displayer.TestToBeRun(sol, ttt.GetEstimatedTime(sol));
			}

			if (printEstimate)
			{
				Out.Line($"Tests should take approximately {TimeSpan.FromSeconds(displayer.RemainingSecs):g}");
			}
		}

		///
		public void DoUnit(Args solutions) => DoUnit(solutions, new UnitTestSettings());

		///
		public virtual void DoUnit(Args solutions, IUnitTestSettings settings)
		{
			if (!settings.ResultsConsole && !settings.ResultsTextFile && !settings.ResultsXmlFile)
			{
				Out.Line("I'm not going to waste my time running the tests if you don't want me to output the results anywhere");
				return;
			}

			_dirs.SetCurrentDirectory(Env.BinFolder);
			if (solutions.Contains("all"))
			{
				// if any of the "solutions" passed in is all, let's save time and jump right to testing them all
				TestSolutions(GetAllSolutions(), settings);
				return;
			}

			var dlls = new Args();

			foreach (var solution in solutions)
			{
				ProcessSolution(solution, dlls);
			}

			if (dlls.Count < 1)
			{
				Out.Line("No valid solutions were specified");
			}
			else
			{
				TestSolutions(dlls, settings);
			}
		}

		///
		public virtual Args GetAllSolutions()
		{
			// get a list of all the dlls in the bin folder
			var names = _dirs.GetFiles(Env.BinFolder, "*.dll").Select(Path.GetFileName).ToList();

			var dlls = new Args();

			// the migration tool doesn't exist in next gen
			if (!IoCContainer.Instance.Resolve<IGit>().IsNextGen())
			{
				dlls.Add("DataMigrationTool.exe");
			}

			// find all the test dlls
			var tests = names.Where(dll => dll.Contains("Tests")).ToList();
			var nonTests = new Args();
			foreach (var test in tests)
			{
				// find any dlls that have a corresponding test dll
				if (test.Contains(".UnitTests."))
				{
					nonTests.Add(test.Replace(".UnitTests.", "."));
				}
			}

			// we want any dll that is for core, shared (which includes xm8 operations), or xm8 (which includes xv), if they don't have a corresponding test dll
			dlls.AddRange(names.Where(dll =>
				(dll.StartsWith("Xm8.") || dll.StartsWith("Core.") || dll.StartsWith("Xm8Operations.") ||
				 dll.StartsWith("XV.") || dll.StartsWith("Shared")) &&
				!tests.Contains(dll) && !nonTests.Contains(dll)));

			// add all the test dlls
			dlls.AddRange(tests);

			dlls.Sort();

			return dlls;
		}

		private string GetFilename(out int count)
		{
			count = 0;
			string result;
			do
			{
				result = ResultXml(++count);
			} while (_files.Exists(result));

			return result;
		}

		private UnitTestResultParser GetPreviousParser(int num)
		{
			string file;
			if (num > 0)
			{
				file = ResultXml(num);
			}
			else
			{
				file = _dirs.GetMostRecentlyChangedFile(_results, "*.xml")?.FullName ?? "";
			}

			return _files.Exists(file) ? UnitTestResultParser.FromXmlString(_files.ReadAllText(file)) : null;
		}

		private string GetProgressSolution(int count, int totSol)
		{
			var percent = totSol == 0 ? 0 : (decimal) count / totSol;
			return $"{(totSol == 0 ? 0 : count)} of {totSol} - {percent:P}";
		}

		///
		public virtual void InvalidInputFile(int input)
		{
			Out.Line(input == 0
				? "There were no results in the results folder"
				: $"Couldn't find '{ResultXml(input)}'");
		}

		private void ProcessSolution(string solution, Args dlls)
		{
			if (!Shortcuts.Instance.HasShortcut(solution))
			{
				Out.NotAShortcut(solution);
				return;
			}

			var other = Shortcuts.Instance.GetOther(solution);
			if (other.IsNullOrEmpty())
			{
				Out.Line($"{solution} does not have other info defined in the shortcut file");
				return;
			}

			if (dlls.Contains(other))
			{
				Out.Line($"you entered {other} to be tested twice");
				return;
			}

			dlls.Add(other);
		}

		private string ResultXml(int input)
		{
			return Path.Combine(_results, string.Format(FileFormat, input));
		}

		///
		public virtual void TestResults(IUnitTestSettings settings, UnitTestResultParser parser)
		{
			var displayer = new UnitTestResultDisplayer(parser);

			if (settings.ResultsConsole)
			{
				TestResultsConsole(displayer.GetResultsToDisplayInConsole());
			}

			if (settings.ResultsTextFile)
			{
				TestResultsTextFile(displayer.GetResultsToDisplayInTextFile());
			}

			if (settings.ResultsXmlFile)
			{
				var filename = GetFilename(out var count);
				var xml = parser.ToXmlString();
				_files.WriteXml(filename, xml);
				Out.Line($"xml results file '{count}' was created");
			}

			var input = settings.InputFile;
			if (input <= -1)
			{
				return;
			}

			var prevParser = GetPreviousParser(input);
			if (prevParser == null)
			{
				InvalidInputFile(input);
			}
			else
			{
				CompareResults(prevParser, parser, input);
			}
		}

		///
		public virtual void TestResultsConsole(List<string> lines)
		{
			foreach (var line in lines)
			{
				Out.Line(line);
			}
		}

		///
		public virtual void TestResultsTextFile(List<string> lines)
		{
			if (!_dirs.Exists(_results))
			{
				_dirs.CreateDirectory(_results);
			}

			var logFile = $@"{_results}\Results.log";

			_files.WriteAllLines(logFile, lines);
			_prog.StartProcess(logFile);
		}

		///
		public virtual void TestSolution(string solution, bool showTime, int count, int total,
			TestProgressTimeDisplayer displayer, ref UnitTestResultParser parser)
		{
			Out.Write($"Testing {solution}");
			var start = DateTime.Now;
			_prog.RunProgram("nunit3-console.exe", solution);
			var end = DateTime.Now;

			var stream = _files.GetStream(Path.Combine(Env.BinFolder, "TestResult.xml"), FileMode.Open);
			var temp = new UnitTestResultParser(XElement.Load(stream), !showTime && !_trackUnitTestTime)
				{TotalDuration = end - start};
			if (parser == null)
			{
				parser = temp;
			}
			else
			{
				parser.AddResults(temp);
			}

			string progress;
			if (displayer != null)
			{
				displayer.TestHasFinished(solution, temp.TotalDuration.TotalSeconds);
				progress = displayer.Display;
			}
			else
			{
				progress = GetProgressSolution(count, total);
			}

			var time = showTime ? $" [{temp.TotalDuration}]" : "";
			Out.Line($" - done{time} ({progress})");
		}

		///
		public virtual void TestSolution(string solution, bool showTime, int count, int total,
			ref UnitTestResultParser parser)
		{
			TestSolution(solution, showTime, count, total, null, ref parser);
		}

		///
		public virtual void TestSolution(string solution, bool showTime, TestProgressTimeDisplayer displayer,
			ref UnitTestResultParser parser)
		{
			TestSolution(solution, showTime, 0, 0, displayer, ref parser);
		}

		///
		public virtual void TestSolutions(Args solutions, IUnitTestSettings settings)
		{
			_dirs.SetCurrentDirectory(Env.BinFolder);
			var total = solutions.Count;
			var closer = IoCContainer.Instance.Resolve<IPopupCloser>();
			closer.StartProcess();
			UnitTestResultParser parser = null;
			TestTimeTracker ttt = null;
			TestProgressTimeDisplayer displayer = null;
			if(settings.EstimateTime || settings.Progress == Progress.Time)
			{
				DetermineTimeEstimate(solutions, settings.EstimateTime, out ttt, out displayer);
			}

			switch (settings.Progress)
			{
				case Progress.None:
					TestSolutionsInOneGo(solutions, out parser);
					break;
				case Progress.Solution:
					for (var i = 0; i < total; i++)
					{
						TestSolution(solutions[i], settings.ShowTime, i + 1, total, ref parser);
					}
					break;
				case Progress.Time:
					foreach (var sol in solutions)
					{
						TestSolution(sol, settings.ShowTime, displayer, ref parser);
					}
					break;
				default:
					closer.StopProcess();
					Out.Line($"A new Progress type has been added but isn't being handled in {nameof(UnitTestCommand)}.{nameof(TestSolutions)}");
					return;
			}
			closer.StopProcess();

			if (_trackUnitTestTime || settings.Progress == Progress.Time)
			{
				if (ttt == null)
				{
					ttt = new TestTimeTracker(_files, _dirs);
				}

				// ReSharper disable once PossibleNullReferenceException
				foreach (var result in parser.Results)
				{
					ttt.AddTime(result.Solution, result.TotalDuration.TotalSeconds);
				}
			}

			TestResults(settings, parser);
		}

		///
		public virtual void TestSolutionsInOneGo(Args solutions, out UnitTestResultParser parser)
		{
			var arg = "";
			foreach (var sol in solutions)
			{
				arg = $"{arg} {sol}";
			}
			
			_prog.RunProgram("nunit3-console.exe", arg.Trim());
			var stream = _files.GetStream(Path.Combine(Env.BinFolder, "TestResults.xml"), FileMode.Open);
			parser = new UnitTestResultParser(XElement.Load(stream), !_trackUnitTestTime);
		}

		#endregion Methods
	}
}
