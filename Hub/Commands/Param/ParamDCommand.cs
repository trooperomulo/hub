﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Commands.Param
{
	/// <summary>
	/// command to delete a user set parameter
	/// </summary>
	public class ParamDCommand : ParamBase
	{
		#region Overrides of ParamBase

		/// <inheritdoc />
		public override string Description => "Deletes a preference";

		/// <inheritdoc />
		public override string Input => "-d";

		/// <inheritdoc />
		public override void Help()
		{
			// prm/p -a (property)

			var aAction = $"{Action} {Input}";
			var propVal = Prep.Paren("property");

			Out.Example(aAction);
			Out.Explain("Used to stop setting a user parameter", aAction, () =>
			{
				Out.Indent("The user can unset any parameter is currently set");
				Out.Line(Prep.Describe(propVal, "the parameter to be unset, can do multiple at once"));
			});

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args)
		{
			if (args.Count == 0)
			{
				Logger.LogParam(noDel: true);
				return;

			}	// end if

			Logger.LogParamD(args.Count);

			DoParamD(args);

		}	// end Perform(List<string>)

		#endregion Overrides of ParamBase

		#region Method

		private void DoParamD(List<string> args)
		{
			var userParam = IoCContainer.Instance.Resolve<IUserParams>();

			foreach (var arg in args)
			{
				userParam.UnsetProperty(arg);
			}	// end foreach

			userParam.Save();
			
		}	// end DoParamD(List<string>)

		#endregion Method

	}	// end ParamDCommand

}	// end Hub.Commands.Param
