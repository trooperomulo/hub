﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hub.Commands.Param
{
	/// <summary>
	/// gives the user an explanation of what a parameter is used for
	/// </summary>
	public class ParamECommand : ParamBase
	{
		#region Overrides of ParamBase

		/// <inheritdoc />
		public override string Description => "Explains the use of a parameter";

		/// <inheritdoc />
		public override string Input => "-e";

		/// <inheritdoc />
		public override void Help()
		{
			// prm/p -e [parameter]

			var aAction = $"{Action} {Input}";
			var prm = Prep.Brackets("parameters");
			Out.Example(aAction);
			Out.Explain("Explains the purpose of parameter(s)", aAction, () =>
			{
				Out.Indent("Explains to the user what the purpose of the given parameters is");
				Out.Line(Prep.Describe(prm, "the parameter(s) to be explained"));
				Out.Indent("If no valid parameters are passed in, the user can multi-select from a list", 2);
			});

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args)
		{
			var count = args.Count;

			Logger.LogParamE(count);

			DoParamE(args);

		}   // end Perform(List<string>)

		#endregion Overrides of ParamBase

		#region Method

		private void DoParamE(List<string> args)
		{
			if (args.Count == 0)
			{
				// args = WaitForUser
			}

			// loop through each arg, ask UserParams for its explanation 
		}

		#endregion Method

	}   // end ParamECommand

}	// end Hub.Commands.Param
