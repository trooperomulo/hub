﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Commands.Helpers;
using Hub.Interface;

namespace Hub.Commands.Param
{
	/// <summary>
	/// shows all the user parameters, set and unset
	/// </summary>
	public class ParamSCommand : ParamBase
	{
		#region Member Variables

		private int _paraLen, _cValLen, _dValLen;
		private bool _wrapping;

		#endregion Member Variables

		#region Overrides of ParamBase

		/// <inheritdoc />
		public override string Description => "Shows all the parameters, indicating which are set and which aren't";

		/// <inheritdoc />
		public override string Input => "-s";

		/// <inheritdoc />
		public override void Help()
		{
			// prm/p -s
			
			var aAction = $"{Action} {Input}";
			Out.Example(aAction);
			Out.Explain("Shows all parameters", aAction, () =>
			{
				Out.Indent("Displays all parameters.  Those that have been set are indicated.");
			});

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args)
		{
			Logger.LogParamS();

			DoParamS();

		}	// end Perform(List<string>)

		#endregion Overrides of ParamBase

		#region Methods

		private void DoParamS()
		{
			var values = GetValues();

			Out.Blank();
			Out.Line("USER PARAMETERS");
			Out.Blank();
			PrintHeader();

			foreach (var value in values)
			{
				PrintParameter(value);
			}	// end foreach

		}	// end DoParamS()

		private List<PropertyValues> GetValues()
		{
			var userParams = IoCContainer.Instance.Resolve<IUserParams>();

			var properties = userParams.GetProperties();

			var maxParaLen = 0;
			var maxCValLen = 0;
			var maxDValLen = 0;

			var values = new List<PropertyValues>();

			foreach (var prop in properties)
			{
				string cVal = GetCurrentValue(userParams, prop);
				string dVal = GetDefaultValue(userParams, prop);
				bool set = userParams.HasPropertyBeenSet(prop);
				var pVal = new PropertyValues { Property = prop, CurVal = cVal, DefVal = dVal, HasBeenSet = set };
				if (pVal.PropertyLen > maxParaLen)
				{
					maxParaLen = pVal.PropertyLen;
				} // end if

				if (pVal.CurValLen > maxCValLen)
				{
					maxCValLen = pVal.CurValLen;
				} // end if

				if (pVal.DefValLen > maxDValLen)
				{
					maxDValLen = pVal.DefValLen;
				} // end if

				values.Add(pVal);
			} // end foreach

			// if you print characters all the way to the edge of the console, it will wrap the line, so we want to pull back by one
			var consoleWidth = IoCContainer.Instance.Resolve<IConsole>().Width - 1;

			var columnInfo = ParameterColumnWidthCalculator.GetColumnsInfo(consoleWidth, maxParaLen, maxCValLen, maxDValLen);

			_paraLen = columnInfo.ParaLen;
			_cValLen = columnInfo.CValLen;
			_dValLen = columnInfo.DValLen;
			_wrapping = columnInfo.NeedToWrap;

			return values;

		}	// end GetValues(out int, out int, out int)
		
		private string GetCurrentValue(IUserParams userParams, string property) => GetPossibleNullValue(userParams.GetPropertyValue(property));

		private string GetDefaultValue(IUserParams userParams, string property) => GetPossibleNullValue(userParams.GetDefaultValue(property));

		private string GetPossibleNullValue(string s)
		{
			if (s == null)
			{
				return "{null}";
			}	// end if

			if (s.IsNullOrEmpty())
			{
				return "\" \"";
			}	// end if

			return s;

		}	// end GetPossibleNullValue(string)

		private string FillValue(string value, int maxLength) => Prep.FillOut($" {value}", maxLength);

		private void PrintHeader()
		{
			PrintSingleLineOfAParameter(PrintPrm("Property"), " Set ", PrintCur("Current"), PrintDef("Default"));
			PrintSingleLineOfAParameter(BlankPrm('-'), BlankSet('-'), BlankCur('-'), BlankDef('-'));

		}	// PrintHeader()

		private string BlankPrm(char fill = ' ') => Prep.FillOut("", _paraLen, fill);

		private string BlankSet(char fill = ' ') => Prep.FillOut("", 5, fill);

		private string BlankCur(char fill = ' ') => Prep.FillOut("", _cValLen, fill);

		private string BlankDef(char fill = ' ') => Prep.FillOut("", _dValLen, fill);

		private string PrintPrm(string value) => Prep.ColumnLeft($"{value}", _paraLen);
		
		private string PrintCur(string value) => Prep.ColumnLeft($"{value}", _cValLen);
		
		private string PrintDef(string value) => Prep.ColumnLeft($"{value}", _dValLen);

		private void PrintParameter(PropertyValues values)
		{
			// this will need to handle wrapping
			if (_wrapping)
			{
				PrintMultiLineParameter(values);
			}
			else
			{
				PrintSingleLineOfAParameter(FillValue(values.Property, _paraLen), Set(values.HasBeenSet),
					FillValue(values.CurVal, _cValLen), FillValue(values.DefVal, _dValLen));
			}

		}

		private static void PrintSingleLineOfAParameter(string property, string set, string currentValue, string defaultValue)
		{
			Out.Line($"{property}|{set}|{currentValue}|{defaultValue}");
		}

		private void PrintMultiLineParameter(PropertyValues values)
		{
			// we need to determine which values need to wrap (current, default, or both) and then walk through
			// cutting off the chuck that we need until we've displayed everything

			var cValRemaining = values.CurVal;
			var dValRemaining = values.DefVal;


			PrintSingleLineOfAParameter(PrintPrm(values.Property), Set(values.HasBeenSet),
				PrintCur(Prep.CutChunkOff(_cValLen - 2, ref cValRemaining)),
				PrintDef(Prep.CutChunkOff(_dValLen - 2, ref dValRemaining)));

			while (cValRemaining.Length > 0 || dValRemaining.Length > 0)
			{
				PrintSingleLineOfAParameter(BlankPrm(), BlankSet(),
					PrintCur(Prep.CutChunkOff(_cValLen - 2, ref cValRemaining)),
					PrintDef(Prep.CutChunkOff(_dValLen - 2, ref dValRemaining)));
			}
		}

		private string Set(bool set) => $" [{(set ? "X" : " ")}] ";

		#endregion Methods

		#region Inner Class

		[ExcludeFromCodeCoverage]
		internal struct PropertyValues
		{
			internal string Property { get; set; }
			internal string CurVal { get; set; }
			internal string DefVal { get; set; }
			internal bool HasBeenSet { get; set; }
			internal int PropertyLen => Property.Length;
			internal int CurValLen => CurVal.Length;
			internal int DefValLen => DefVal.Length;

		}	// end PropertyValues

		#endregion Inner Class

	}	// end ParamSCommand

}	// end Hub.Commands.Param
