﻿using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands.Param
{
	/// <summary>
	/// base class for user parameter commands
	/// </summary>
	public abstract class ParamBase : IInput, ISubCommand
	{
		#region Variables

		private Args _inputs;
		
		// ReSharper disable once InconsistentNaming
		internal static readonly Args _ins = new Args {"prm", "p"};

		///
		protected static IFiles Files;
		
		///
		protected static readonly string Action = Prep.Slash(_ins.ToArray());
		
		///
		protected static Logger Logger => Logger.Instance;

		#endregion Variables
		
		#region Implementation of ICommand

		/// <inheritdoc />
		public abstract string Description { get; }

		/// <inheritdoc />
		public abstract string Input { get; }

		/// <inheritdoc />
		public Args Inputs => _inputs ?? (_inputs = new Args {Input});

		/// <summary>
		/// the help describing this command
		/// </summary>
		public abstract void Help();

		/// <summary>
		/// performs that actual command
		/// </summary>
		public abstract void Perform(Args args);

		#endregion Implementation of ICommand

		#region Constructor

		///
		public static void Initialize() => Files = IoCContainer.Instance.Resolve<IFiles>();

		#endregion Constructor

	}	// end ParamBase

}	// end Hub.Commands.Param
