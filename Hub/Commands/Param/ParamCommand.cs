﻿using System.Collections.Generic;

using Hub.Decider.Param;
using Hub.Interface;

namespace Hub.Commands.Param
{
	/// <summary>
	/// perform one of the user parameter commands
	/// </summary>
	public class ParamCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Modify the user parameters file";

		/// <inheritdoc />
		public bool HasSubCommands => true;

		/// <inheritdoc />
		public List<string> Inputs => ParamBase._ins;

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args) => ParamHelpDecider.Instance.Help(args);

		/// <inheritdoc />
		public void Perform(List<string> args) => ParamMainDecider.Instance.Perform(args);

		#endregion Implementation of ICommand

		#region Constructor

		///
		public ParamCommand(List<ParamBase> prms, ParamBase none) => ParamDecider.InitCommands(prms, none);

		#endregion Constructor

	}	// end ParamCommand

}	// end Hub.Commands.Param
