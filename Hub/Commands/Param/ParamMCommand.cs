﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Commands.Param
{
	/// <summary>
	/// allows the user to change a parameter's value
	/// </summary>
	public class ParamMCommand : ParamBase
	{
		#region Overrides of ParamBase

		/// <inheritdoc />
		public override string Description => "Modifies the value of a set preference";

		/// <inheritdoc />
		public override string Input => "-m";

		/// <inheritdoc />
		public override void Help()
		{
			// prm/p -m (property value)
			
			var aAction = $"{Action} {Input}";
			var propVal = Prep.Paren("prop", "value");
			Out.Example(aAction);
			Out.Explain("Used to change the value of an already set user parameter", aAction, () =>
			{
				Out.Indent("The user can change any parameter that is already set.");
				Out.Line(Prep.Describe(propVal, "the parameter and value to be changed, can do multiple"));
			});

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args)
		{
			var count = args.Count;
			
			if (count == 0)
			{
				Logger.LogParam(noMod: true);
				return;
			}	// end if

			if (count % 2 == 1)
			{
				// bad pairs
				Logger.LogParam(badMod: true);
				return;
			}	// end if

			var pairs = new List<(string prop, string value)>();

			for (var i = 0; i < count; i += 2)
			{
				pairs.Add((prop: args[i], value: args[i + 1]));
			}	// end for

			Logger.LogParamM(pairs.Count);

			DoParamM(pairs);

		}	// end Perform(List<string>)

		#endregion Overrides of ParamBase

		#region Method
		
		private void DoParamM(List<(string prop, string value)> pairs)
		{
			var userParams = IoCContainer.Instance.Resolve<IUserParams>();

			foreach (var (prop, value) in pairs)
			{
				userParams.ModifyProperty(prop, value);
			}	// end foreach

			userParams.Save();

		}	// end DoParamM(List<(string prop, string value)>)

		#endregion Method

	}	// end ParamMCommand

}	// end Hub.Commands.Param
