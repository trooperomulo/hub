﻿using System.Collections.Generic;

using Hub.Decider.Param;

namespace Hub.Commands.Param
{
	///
	public class ParamNoneCommand : ParamBase
	{
		#region Overrides of ParamBase

		/// <inheritdoc />
		public override string Description => "Used to perform one of various actions involving the user parameters";

		/// <inheritdoc />
		public override string Input { get; } = "";

		/// <inheritdoc />
		public override void Help()
		{
			// prm/p -a/-d/-m/-s

			NoneCommand.DoNoneHelp(ParamDecider.Commands, Action, Description);

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args)
		{
			Logger.LogParam(noAct: true);

			NoneCommand.DoNonePerform("Parameter", Help, ParamDecider.Commands);

		}	// end Perform(List<string>)

		#endregion Overrides of ParamBase

	}	// end ParamNoneCommand

}	// end Hub.Commands.Param
