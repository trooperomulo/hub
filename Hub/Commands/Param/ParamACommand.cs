﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Commands.Param
{
	/// <summary>
	/// adds a value to a user parameter
	/// </summary>
	public class ParamACommand : ParamBase
	{
		#region Overrides of ParamBase

		/// <inheritdoc />
		public override string Description => "Adds a value to a preference";

		/// <inheritdoc />
		public override string Input => "-a";

		/// <inheritdoc />
		public override void Help()
		{
			// prm/p -a (property value)
			
			var aAction = $"{Action} {Input}";
			var propVal = Prep.Paren("prop", "value");
			Out.Example(aAction);
			Out.Explain("Used to start setting a user parameter", aAction, () =>
			{
				Out.Indent("The user can set any parameter that isn't already set.");
				Out.Line(Prep.Describe(propVal, "the parameter and value to be set, can do multiple"));
			});

		}	// end Help()

		/// <inheritdoc />
		public override void Perform(List<string> args)
		{
			var count = args.Count;

			if (count == 0)
			{
				Logger.LogParam(noAdd: true);
				return;
			}	// end inf

			if (count % 2 == 1)
			{
				// bad pairs
				Logger.LogParam(badAdd: true);
				return;
			}	// end if
			
			var pairs = new List<(string prop, string value)>();

			for (var i = 0; i < count; i += 2)
			{
				pairs.Add((prop: args[i], value: args[i + 1]));
			}	// end for

			Logger.LogParamA(pairs.Count);

			DoParamA(pairs);

		}	// end Perform(List<string>)

		#endregion Overrides of ParamBase

		#region Method

		private void DoParamA(List<(string prop, string value)> pairs)
		{
			var userParams = IoCContainer.Instance.Resolve<IUserParams>();

			foreach (var (prop, value) in pairs)
			{
				userParams.SetProperty(prop, value, false);
			}	// end foreach

			userParams.Save();

		}	// end DoParamA(List<(string prop, string value)>)

		#endregion Method

	}	// end ParamACommand

}	// end Hub.Commands.Param
