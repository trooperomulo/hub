﻿using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// displays the automation log
	/// </summary>
	public class AutoLogCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Show automation logs";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"autolog", "al"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// autolog/al
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action);
			Out.Explain("Opens Windows Explorer to the location of the automation logs", action, () => { });
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			Logger.Instance.LogAutoLog();
			DoAutoLog();
		}

		#endregion Implementation of ICommand

		#region Method

		private static void DoAutoLog()
		{
			IoCContainer.Instance.Resolve<IProgram>().StartProcess($@"{Env.Xm8}\automation\bin\Debug\Logging");
		}

		#endregion Method
	}
}
