﻿using System;
using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// Launch Trickler
	/// </summary>
	public class TricklerCommand : ICommand
	{
		#region Variables

		private readonly Action<bool, bool, bool, List<string>, bool> _bld;
		private readonly Action _xm8;

		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Launch Trickler";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"trickle", "t"};

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// trickle/t [-x/l -u/t] [solutions]
			var action = Prep.Slash(Inputs.ToArray());
			var options = Prep.Brackets($"{Flags._flagXm8_} {Flags._flagUnit}");
			Out.Example(action, options, Hub.Solutions);
			Out.Explain("Launches the trickler after building any specified solutions", action, () =>
			{
				Out.Line(options);
				Out.Indent(Prep.Describe(Flags._flagXm8_, "also launch Xactimate"));
				Out.Indent(Prep.Describe(Flags._flagUnit, "run units tests, will be ignored if no solutions are specified"));
				Out.Line(Prep.Describe(Hub.Solutions, "will be run in FIFO order; be aware of build order"));
				Out.Indent("nothing will be built if no solutions are specified, i.e., no buildall", 2);
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			var xm8 = Utils.IsFirstXm8(args);
			var unt = Utils.IsFirstUnt(args);
			var count = args.Count;

			Logger.Instance.LogTrickler(xm8, unt, count);

			if (count < 1)
			{
				DoTrickler();
				if (xm8)
				{
					_xm8();
				}
				return;
			}

			_bld(xm8, true, unt, args, false);
		}

		#endregion  Implementation of ICommand

		#region Constructor

		///
		public TricklerCommand(Action<bool, bool, bool, List<string>, bool> bld, Action xm8)
		{
			_bld = bld;
			_xm8 = xm8;
		}

		#endregion Constructor

		#region Method

		///
		public static void DoTrickler() => IoCContainer.Instance.Resolve<IProgram>().LaunchBinProgram("DataMigrationTool.exe", "Trickler", wait: false);

		#endregion Method
	}
}
