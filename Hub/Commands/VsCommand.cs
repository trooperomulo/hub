﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// opens visual studio
	/// </summary>
	public class VsCommand : ICommand
	{
		#region Data Members

		private static string _colors;
		// ReSharper disable once InconsistentNaming
		private static Action<string, string> _colorize;

		#endregion Data Members

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Open Visual Studio";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"vs", "v"};

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// vs/v [solutions]
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action, Hub.Solutions);
			Out.Explain("Opens Visual Studio for the specified solutions", action, () =>
			{
				Out.Line(Prep.Describe(Hub.Solutions, "will try to open in FIFO order"));
				Out.Indent("nothing will happen if no solutions are specified", 2);
			});

		}	// end Help(List<string>)

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			Logger.Instance.LogVs(args.Count);

			foreach (var solution in args)
			{
				DoVs(solution);

			}	// end foreach

		}   // end Perform(List<string>)

		#endregion Implementation of ICommand

		#region Constructor

		///
		public VsCommand(string colors)
		{
			_colors = colors;
			if (colors.IsNullOrEmpty())
			{
				// if the user hasn't defined any colors, don't worry about colorizing
				_colorize = (f, s) => { };
			}	// end if
			else
			{
				_colorize = Colorize;
			}	// end else

		}	// end VsCommand(string)

		#endregion Constructor

		#region Methods

		private void DoVs(string solution)
		{
			if (!SomethingToOpen(solution, out var files))
			{
				return;
			}	// end if

			var somethingOpened = false;
			var allowed = GetAllowed(solution);
			var program = IoCContainer.Instance.Resolve<IProgram>();
			foreach (var file in files)
			{
				somethingOpened |= SomethingOpened(solution, file, allowed, program);
			}	// end for

			if (!somethingOpened)
			{
				Out.Line($"everything was filtered out for {solution}");
			}	// end if

		}	// end DoVs(string)

		private static bool SomethingToOpen(string solution, out List<string> files)
		{
			files = new List<string>();
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			if (!Shortcuts.Instance.HasShortcut(solution))
			{
				Out.NotAShortcut(solution);
				return false;
			}	// end if

			var path = GetPath(solution);
			if (!dirs.Exists(path))
			{
				Out.Line($"Could not find '{path}', the location '{solution}' is set to");
				return false;
			}	// end if

			files = dirs.GetFiles(path, "*.sln");
			if (files.Count < 1)
			{
				files = dirs.GetFiles(path, "*.csproj");
			}	// end if

			if (files.Count < 1)
			{
				Out.Line("No .sln or .csproj files were found, so nothing will be opened");
				return false;
			}	// end if

			return true;

		}	// end SomethingToOpen(string solution, out List<string>)

		private static bool SomethingOpened(string solution, string file, List<string> allowed, IProgram program)
		{
			var filename = Path.GetFileName(file);
			if (ShouldOpen(filename, allowed))
			{
				_colorize(file, solution);

				Out.Line($"opening {solution} - {filename}");
				program.RunBatchFile($"start {Path.GetFullPath(file).QuoteTheSpaces()}");
				return true;
			}	// end if

			return false;

		}	// end SomethingOpened(string, string, List<string>, IProgram)

		private static bool ShouldOpen(string filename, List<string> allowed)
		{
			if (allowed.Count < 1)
			{
				// if no other info was defined for this solution, open everything
				return true;
			}	// end if

			if (allowed.Count == 1)
			{
				// for some shortcuts, the other info is for a dll, if so, open everything
				if (allowed[0].EndsWith("dll"))
				{
					return true;
				}	// end if
			}	// end if

			return allowed.Contains(filename);
		}	// end ShouldOpen(string, List<string>)

		private static List<string> GetAllowed(string solution)
		{
			var other = Shortcuts.Instance.GetOther(solution);
			return other.IsNullOrEmpty() ? new List<string>() : other.Split(',').ToList();
		}	// end GetAllowed(string)

		private static string GetPath(string solution)
		{
			var shortcut = Shortcuts.Instance.GetPath(solution);
			return shortcut.StartsWith("c:") ? shortcut : $"{Env.Local}{Env.Version}{shortcut}";
		}	// end GetPath(string)

		private static void Colorize(string file, string solution)
		{
			var colorizer = IoCContainer.Instance.Resolve<ISolutionColorizer>();
			colorizer.ColorizeSolution(_colors, file, solution);
		}	// end Colorize(string, string)

		#endregion Methods

	}	// end VsCommand

}	// end Hub.Commands
