﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

using Hub.Interface;
using Hub.Logging;

using Args= System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	// Retiring this for now since nobody uses a text file for their standup
	/// <summary>
	/// create scrum file
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class ScrumCommand : ICommand
	{
		#region Variables

		private readonly IFiles _files;

		private readonly string _name, _scrumFile, _scrumLocation;

		private readonly bool _scrumHours;

		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Create scrum file";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"scrum", "s"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// scrum/s [lines] [-h hours] [-w lines] [-b lines] [-t lines]
			var action = Prep.Slash(Inputs.ToArray());
			var lines = Prep.Brackets("lines");
			var hours = Prep.Brackets($"{Flags._flagHour} hours");
			var wLines = Prep.Brackets($"{Flags._flagWlin} lines");
			var bLines = Prep.Brackets($"{Flags._flagBlin} lines");
			var tLines = Prep.Brackets($"{Flags._flagTlin} lines");
			var allLines = $"{lines} {hours} {wLines} {bLines} {tLines}";
			Out.Example(action, allLines);
			Out.Explain("Opens my file for the next scrum, creating it if it doesn't exist", action, () =>
			{
				Out.Line(Prep.Describe($"{lines}\t", "the lines that should be added for the Did section"));
				Out.Line(Prep.Describe(hours, "the number of hours logged the previous day"));
				Out.Line(Prep.Describe(wLines, "the lines that should be added for the Will Do section"));
				Out.Line(Prep.Describe(bLines, "the lines that should be added for the Road Blocks section"));
				Out.Line(Prep.Describe(tLines, "the tasks that are currently being worked on and their status"));
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			if (_scrumLocation.IsNullOrEmpty())
			{
				Out.Error($"You haven't defined the location for your scrum files in {UserParams.UserParamsFile}");
				return;
			}

			var future = false;
			// determine the date for the file
			var date = DateTime.Now;
			if (date.Hour > 9)
			{
				// if it is after 10 am, we need to do the file for the following day
				// for now we are going to ignore holidays and vacations
				future = true;
				date = DateTime.Today.AddDays(date.DayOfWeek == DayOfWeek.Friday ? 3 : 1);
			}

			var did = ProcessDid(args);
			var hour = ProcessHour(args);
			var will = ProcessWill(args);
			var block = ProcessBlock(args);
			var tasks = ProcessTask(args);

			Logger.Instance.LogScrum(did.Count, hour, will.Count, block.Count, tasks.Count, future);

			DoScrum(date, hour.ToDoub(), did, will, block, tasks);
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public ScrumCommand(string name, string scrumLocation, bool scrumHours)
		{
			_files = IoCContainer.Instance.Resolve<IFiles>();

			_name = name;
			_scrumFile = name + ".txt";
			_scrumLocation = scrumLocation;
			_scrumHours = scrumHours;
		}

		#endregion Constructor

		#region Methods

		private static void AddScrumGroup(string group, double toAdd, Args lines)
		{
			// ReSharper disable once SpecifyACultureInStringConversionExplicitly
			AddScrumGroup(group, new Args { toAdd.ToString() }, lines);
		}

		private static void AddScrumGroup(string group, Args toAdd, Args lines)
		{
			lines.Add("");
			lines.Add(group);
			lines.AddRange(toAdd.Select(line => Prep.Minus(line, true)));
		}

		private void CreateScrumFile(double hour, Args did, Args will, Args block, Args tasks)
		{
			var lines = new Args { _name };

			if (_scrumHours || hour > 0)
			{
				AddScrumGroup("Hours Logged:", hour, lines);
			}

			AddScrumGroup("Did:", did, lines);
			AddScrumGroup("Will Do:", will, lines);
			if (block.Count > 0)
			{
				AddScrumGroup("Road Blocks:", block, lines);
			}
			if (tasks.Count > 0)
			{
				AddScrumGroup("Tasks:", tasks, lines);
			}

			_files.AppendAllLines(_scrumFile, lines);
		}

		private void DoScrum(DateTime date, double hour, Args did, Args will, Args block, Args tasks)
		{
			try
			{
				IoCContainer.Instance.Resolve<IDirectories>().SetCurrentDirectory(_scrumLocation);
			}
			catch (IOException)
			{
				Out.Line("I cannot access the scrum file location right now.");
				return;
			}

			GoToScrumFolder(date.Year.ToString());
			GoToScrumFolder(date.Month.ToString("D2"));
			GoToScrumFolder(date.Day.ToString("D2"));

			if (_files.Exists(_scrumFile))
			{
				UpdateScrumFile(ref hour, did, will, block, tasks);
			}
			CreateScrumFile(hour, did, will, block, tasks);

			IoCContainer.Instance.Resolve<IProgram>().RunProgram(@"C:\Windows\System32\notepad.exe", _scrumFile, wait: false);
		}

		private void GoToScrumFolder(string path)
		{
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			if (!dirs.Exists(path))
			{
				dirs.CreateDirectory(path);
			}
			dirs.SetCurrentDirectory(path);
		}

		private static Args ProcessDid(Args args)
		{
			var did = new Args();

			foreach (var arg in args)
			{
				if (arg == Flags._flagHour || arg == Flags._flagWlin || arg == Flags._flagBlin ||
				    arg == Flags._flagTlin)
					break;
				did.Add(arg);
			}

			return did;
		}

		private static string ProcessHour(Args args)
		{
			var hour = new Args();

			foreach (var arg in args)
			{
				if (arg == Flags._flagWlin || arg == Flags._flagBlin || arg == Flags._flagTlin)
					break;

				if (arg == Flags._flagHour)
					continue;

				hour.Add(arg);
			}

			return hour.Count > 0 ? hour[0] : "";
		}

		private static Args ProcessWill(Args args)
		{
			var will = new Args();

			foreach (var arg in args)
			{
				if (arg == Flags._flagBlin || arg == Flags._flagTlin)
					break;

				if (arg == Flags._flagWlin)
					continue;

				will.Add(arg);
			}

			return will;
		}

		private static Args ProcessBlock(Args args)
		{
			var block = new Args();

			foreach (var arg in args)
			{
				if (arg == Flags._flagTlin)
					break;

				if (arg == Flags._flagBlin)
					continue;

				block.Add(arg);
			}

			return block;
		}

		private static Args ProcessTask(Args args)
		{
			var task = new Args();

			foreach (var arg in args)
			{
				if (arg == Flags._flagTlin)
					continue;

				task.Add(arg);
			}

			return task;
		}

		private void UpdateScrumFile(ref double hour, Args did, Args will, Args block, Args tasks)
		{
			var lines = _files.ReadLines(_scrumFile).ToList();

			UpdateHour(lines, ref hour);
			UpdateDid(lines, did);
			UpdateWill(lines, will);
			UpdateBlock(lines, block);
			UpdateTask(lines, tasks);

			_files.Delete(_scrumFile);
		}

		private void UpdateHour(Args lines, ref double hour)
		{
			var toRemove = 0;
			foreach (var line in lines)
			{
				var lower = line.ToLower();
				if (lower == "did:" || lower == "will do:" || lower == "road blocks:" || lower == "tasks:")
					break;

				if (lower == "hours logged:" || line == _name || line.IsNullOrEmpty())
				{
					toRemove++;
					continue;
				}
				
				hour += line.Substring(3).ToDoub();   // remove the " - "
			}

			lines.RemoveRange(0, toRemove);
		}

		private void UpdateDid(Args lines, Args did)
		{
			var toRemove = 0;
			var added = 0;
			foreach (var line in lines)
			{
				var lower = line.ToLower();
				if (lower == "will do:" || lower == "road blocks:" || lower == "tasks:")
					break;

				if (lower == "did:" || line == _name || line.IsNullOrEmpty())
				{
					toRemove++;
					continue;
				}

				did.Insert(added++, line.Substring(3));	// remove the " - "
			}

			lines.RemoveRange(0, toRemove);
		}

		private void UpdateWill(Args lines, Args will)
		{
			var toRemove = 0;
			var added = 0;
			foreach (var line in lines)
			{
				var lower = line.ToLower();
				if (lower == "road blocks:" || lower == "tasks:")
					break;

				if (lower == "will do:" || line == _name || line.IsNullOrEmpty())
				{
					toRemove++;
					continue;
				}

				will.Insert(added++, line.Substring(3));   // remove the " - "
			}

			lines.RemoveRange(0, toRemove);
		}

		private void UpdateBlock(Args lines, Args block)
		{
			var toRemove = 0;
			var added = 0;
			foreach (var line in lines)
			{
				var lower = line.ToLower();
				if (lower == "tasks:")
					break;

				if (lower == "road blocks:" || line == _name || line.IsNullOrEmpty())
				{
					toRemove++;
					continue;
				}

				block.Insert(added++, line.Substring(3));   // remove the " - "
			}

			lines.RemoveRange(0, toRemove);
		}

		private void UpdateTask(Args lines, Args task)
		{
			var toRemove = 0;
			var added = 0;
			foreach (var line in lines)
			{
				if (line.ToLower() == "tasks:" || line == _name || line.IsNullOrEmpty())
				{
					toRemove++;
					continue;
				}

				task.Insert(added++, line.Substring(3));   // remove the " - "
			}

			lines.RemoveRange(0, toRemove);
		}

		#endregion Methods
	}
}
