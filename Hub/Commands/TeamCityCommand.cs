﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// Loads Team City
	/// </summary>
	public class TeamCityCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Display branch in Team City";

		/// <inheritdoc />
		// ReSharper disable once StringLiteralTypo
		public List<string> Inputs { get; } = new List<string> {"teamc", "tc"};

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// teamcity/tc [-w/-a/-i] [branches]
			var action = Prep.Slash(Inputs.ToArray());
			var platforms = Prep.Slash(Flags._flagWind, Flags._flagAndr, Flags._flagIos_);
			var branches = Prep.Brackets("branches");
			Out.Example(action, platforms, branches);
			Out.Explain("Display builds for the given branch(es) in Team City", action,
				() =>
				{
					Out.Line(platforms);
					Out.Indent(Prep.Describe(Flags._flagWind, "Display Windows' build"));
					Out.Indent(Prep.Describe(Flags._flagAndr, "Display Android's build"));
					Out.Indent(Prep.Describe(Flags._flagIos_, "Display iOS's build"));
					Out.Indent("If none are specified, you will be prompted", 2);
					Out.Line(Prep.Describe(branches,
						"will be run in FIFO order; uses the current branch if nothing is passed in"));
				});

		}	// end Help(List<string>)

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			HandlePlatform(args, out var platform);

			Logger.Instance.LogTeamCity(platform, args.Count);
			
			if (platform == Platform.None)
			{
				// if there are no platforms, we don't need to do anything
				return;
			}	// end if

			var branches = args.Where(branch => !branch.IsNullOrEmpty()).ToList();

			if (!branches.Any())
			{
				branches.Add(IoCContainer.Instance.Resolve<IGit>().GetBranchName());
			}	// end if
			
			foreach (var branch in branches)
			{
				DoTeamCity(branch, platform);
			}	// end foreach

		}	// end Perform(List<string>)

		#endregion Implementation of ICommand

		#region Methods

		private void DoTeamCity(string branch, Platform platform)
		{
			var prog = IoCContainer.Instance.Resolve<IProgram>();

			// TODO 450 : look into handling main as the branch name

			Out.Line($"Displaying {branch}");
			if(platform.HasFlag(Platform.Windows))
			{
				prog.StartProcess($"https://teamcity.xactware.com/project.html?projectId=Xactimate&branch_Xactimate={branch}");
			}	// end if

			// TODO 430 : I need to strip out some of the "folders" for the mobile sites

			if(platform.HasFlag(Platform.Android))
			{
				prog.StartProcess($"https://teamcity.xactware.com/project.html?projectId=ClaimsSolutionsAndroid&branch_ClaimsSolutionsAndroid={branch}");
			}	// end if

			if(platform.HasFlag(Platform.iOS))
			{
				prog.StartProcess($"https://teamcity.xactware.com/project.html?projectId=ClaimsSolutionsIOS&branch_ClaimsSolutionsIOS={branch}");
			}	// end if

		}	// dnd DoTeamCity(string, Platform)

		private void HandlePlatform(List<string> args, out Platform platform)
		{
			platform = Platform.None;
			var win = Utils.HasWinArg(args);
			var and = Utils.HasAndArg(args);
			var ios = Utils.HasIosArg(args);

			if (win)
			{
				platform |= Platform.Windows;
			}	// end if

			if (and)
			{
				platform |= Platform.Android;
			}	// end if

			if (ios)
			{
				platform |= Platform.iOS;
			}	// end if
			
			// make sure a platform has been specified
			if (platform == Platform.None)
			{
				platform = AskUserWhichPlatform();
			}	// end if

		}	// end HandlePlatform(List<string>, out Platform)

		private static Platform AskUserWhichPlatform()
		{
			var wait = IoCContainer.Instance.Resolve<IWaitForUser>();

			var explanation = new List<string> {"You didn't specify any platforms to display."};
			var options = new List<string> {"Windows", "Android", "iOS"};
			var question = "Which platforms do you want to see?";

			var selected =
				wait.MultipleSelectionEnterNotRequired(explanation, options, question, "Quitting", "Displaying");

			return ConvertIndexesToPlatform(selected);

		}	// end AskUserWhichPlatform()

		private static Platform ConvertIndexesToPlatform(List<int> indexes)
		{
			/* - The return statement is the same as the following code

			 var platform = Platform.None;

			foreach (var i in indexes)
			{
				platform |= (Platform) Math.Pow(2, i);
			}	// end foreach

			return platform;
			*/

			return indexes?.Aggregate(Platform.None, (current, i) => current | (Platform) Math.Pow(2, i)) ??
			       Platform.None;

		}	// end ConvertIndexesToPlatform(List<int>)

		#endregion Methods

	}	// end TeamCityCommand

}	// end Hub.Commands
