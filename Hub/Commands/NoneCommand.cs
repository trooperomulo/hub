﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// command invoked when no input is given
	/// </summary>
	public class NoneCommand : ICommand
	{
		#region Variable

		private readonly Action _hlp;

		#endregion Variable

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string>();

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(Args args) { } // nothing because we shouldn't ever get here

		/// <inheritdoc />
		public void Perform(Args args) => _hlp();

		#endregion Implementation of ICommand

		#region Constructor

		///
		public NoneCommand(Action hlp) => _hlp = hlp;

		#endregion Constructor

		#region Public Static Helpers

		/// <summary>
		/// handles the help for a none command
		/// </summary>
		public static void DoNoneHelp<T>(List<T> cmds, string action, string description)
			where T : ISubCommand
		{
			var inputs = cmds.Select(cmd => cmd.Input).ToArray();
			var mainOptions = Prep.Slash(inputs);

			Out.Example(action, mainOptions);
			Out.Explain(description, action, () =>
			{
				Out.Line(mainOptions);
				foreach (var cmd in cmds)
				{
					Out.Indent(Prep.Describe(cmd.Input, cmd.Description));
				}	// end foreach

				Out.Indent("Only one action will be honored per call");
				Out.Indent("This help will be printed if no action is specified", 2);
			});

		}	// end DoNoneHelp<T>(string, List<T>)

		/// <summary>
		/// handles the perform for a none command
		/// </summary>
		public static void DoNonePerform<T>(string family, Action help, List<T> cmds)
			where T : ISubCommand
		{
			Out.Line($"You did not specify a valid {family} Action to do");
			help();
			foreach (var cmd in cmds)
			{
				cmd.Help();
			}	// end foreach

		}	// end DoNonePerform<T>(string, Action, List<T>)

		#endregion Public Static Helpers
	}
}
