﻿using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// Kill the trickler
	/// </summary>
	public class KillTrickCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Kill the Trickler";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public Args Inputs { get; } = new Args {"killt", "kt"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(Args args)
		{
			// killtrick/kt
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action);
			Out.Explain("Kills the trickler process and deletes the trickler folders", action, () => { });
		}

		/// <inheritdoc />
		public void Perform(Args args)
		{
			Logger.Instance.LogKill();
			DoKill();
		}

		#endregion Implementation of ICommand

		#region Method

		private void DoKill()
		{
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			// Kill the process
			Out.Line("Killing process");
			IoCContainer.Instance.Resolve<IProgram>().KillProcess("DataMigrationTool");

			// delete the 28 DMT folder
			Out.Line("Deleting 28 folder");
			const string dmt28 = @"C:\ProgramData\Xactware\Xactimate28\DataMigrationTool";
			if (dirs.Exists(dmt28))
			{
				dirs.Delete(dmt28);
			}

			// ReSharper disable once InconsistentNaming
			const string dmt28b = @"C:\ProgramData\Xactware\Xactimate28\DataMigration";
			if (dirs.Exists(dmt28b))
			{
				dirs.Delete(dmt28b);
			}
			// delete the 29 DMT folder
			Out.Line("Deleting 29 folder");
			const string dmt29 = @"C:\ProgramData\Xactware\Xactimate29\DataMigration";
			if (dirs.Exists(dmt29))
			{
				dirs.Delete(dmt29);
			}

			// delete the DMT folder
			Out.Line("Deleting Trickler folder");
			const string dmt = @"C:\ProgramData\Xactware\DataMigrationTool";
			if (dirs.Exists(dmt))
			{
				dirs.Delete(dmt);
			}

			// delete the DMT folder
			const string dmtb = @"C:\ProgramData\Xactware\DataMigration";
			if (dirs.Exists(dmtb))
			{
				dirs.Delete(dmtb);
			}

		}

		#endregion Method
	}
}
