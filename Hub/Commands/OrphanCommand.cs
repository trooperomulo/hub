﻿using System.Collections.Generic;
using System.Linq;

using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// Find orphans
	/// </summary>
	public class OrphanCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Find Orphans";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"orphan", "or"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// orphan/o [-x -s]
			var action = Prep.Slash(Inputs.ToArray());
			var options = Prep.Brackets($"{Flags._flagXm81} {Flags._flagData}");
			Out.Example(action, options);
			Out.Explain("Finds and lists all orphan branches, branches that exist locally but not remotely", action, () =>
			{
				Out.Line(options);
				Out.Indent(Prep.Describe(Flags._flagXm81, "find the orphans for the xm8 repo"));
				Out.Indent(Prep.Describe(Flags._flagData, "find the orphans for the data repo"));
				Out.Indent("if no repos are specified, nothing will happen", 2);
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			var xm8 = Utils.ReadFirstArg(args, Flags._flagXm81);
			var dta = Utils.ReadFirstArg(args, Flags._flagData);

			Logger.Instance.LogOrphan(xm8, false);

			if (xm8)
			{
				DoOrphan(Repository.Xm8);
			}

			if (dta)
			{
				DoOrphan(Repository.Data);
			}

			if (!xm8 && !dta)
			{
				Out.Line("You need to specify a repo");
			}
		}

		#endregion Implementation of ICommand

		#region Methods

		private void DoOrphan(Repository repo)
		{
			Utils.NavigateToRepo(repo);
			// call Ben's code

			var git = IoCContainer.Instance.Resolve<IGit>();

			var remoteBranches = git.GetBranches(true);
			var localBranches = git.GetBranches(false);

			var orphans = localBranches.Where(branch => !remoteBranches.Contains(branch)).ToList();
			var header = Prep.GetHeaderForRepo(repo);
			if (orphans.Count > 0)
			{
				Out.Line(header);
				foreach (var orphan in orphans)
				{
					Out.Line(orphan);
				}
				Out.Blank();
			}
			else
			{
				Out.Line($"No orphans found for the {header} repo");
			}
		}

		#endregion Methods
	}
}
