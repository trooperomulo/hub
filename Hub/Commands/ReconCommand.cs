﻿using System;

using Hub.Interface;
using Hub.Logging;

using Args= System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// reset the db
	/// </summary>
	public class ReconCommand : ICommand
	{
		#region Variables

		private readonly Action _xm8;
		private readonly Action _trk;

		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Reconfigure Xactimate";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public Args Inputs { get; } = new Args {"recon", "r"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(Args args)
		{
			// recon/r [-x/l -tr/dm] [reg files]
			var action = Prep.Slash(Inputs.ToArray());
			var options = Prep.Brackets($"{Flags._flagXm8_} {Flags._flagTrck}");
			var regFiles = Prep.Brackets("reg files");
			Out.Example(action, options, regFiles);
			Out.Explain("Reconfigures Xactimate", action, () =>
			{
				Out.Line(options);
				Out.Indent(Prep.Describe(Flags._flagXm8_, "launch Xactimate after reconfiguring and re-adding any registry keys"));
				Out.Indent(Prep.Describe(Flags._flagTrck, "launch the trickler after reconfiguring and re-adding any registry keys"));
				Out.Blank();
				Out.Line(Prep.Describe(regFiles, "any registry files to be run to re-add registry keys"));
				Out.Indent($"only need the file name, the file must be located in '{RegFile("")}'", 2);
			});
		}

		/// <inheritdoc />
		public void Perform(Args args)
		{
			var xm8 = Utils.IsFirstXm8(args);
			var trk = Utils.IsFirstTrk(args);

			Logger.Instance.LogRecon(xm8, trk, args.Count);

			DoRecon(xm8, trk, args);
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public ReconCommand(Action xm8, Action trk)
		{
			_xm8 = xm8;
			_trk = trk;
		}

		#endregion Constructor

		#region Methods

		private void DoRecon(bool xm8, bool tric, Args regFiles)
		{
			var prog = IoCContainer.Instance.Resolve<IProgram>();
			prog.LaunchBinProgram("x.exe", "Reconfigure", "-reconfigure");
			foreach (var regFile in regFiles)
			{
				var path = RegFile(regFile);
				if (!IoCContainer.Instance.Resolve<IFiles>().Exists(path))
				{
					Out.Line($"{path} is not a valid file");
					continue;
				}

				Out.Line($"adding keys found in {regFile}");
				prog.RunProgram("regedit.exe", $"/s {path}");
			}

			if (xm8)
			{
				_xm8();
			}

			if (tric)
			{
				_trk();
			}
		}

		private static string RegFile(string regFile) => $@"c:\bat\SystemSetup\{regFile}";

		#endregion Methods
	}
}
