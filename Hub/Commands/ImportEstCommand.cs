﻿using System.Collections.Generic;

using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// imports estimates
	/// </summary>
	public class ImportEstCommand : ICommand
	{
		#region Variable

		private readonly string _defUser;

		#endregion Variable

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Import 28 estimates";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"import", "ie"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args = null)
		{
			// import/ie num prof [days] [delete] [user] [status]
			var action = Prep.Slash(Inputs.ToArray());
			const string num = "num";
			const string prof = "prof";
			var delete = Prep.Brackets("delete");
			var days = Prep.Brackets("days");
			var user = Prep.Brackets("user");
			var status = Prep.Brackets("status");
			var req = $"{num} {prof}";
			var options = $"{delete} {days} {user} {status}";

			Out.Example(action, req, options);
			Out.Explain("Import estimates into 28", action, () =>
			{
				Out.Line(Prep.Describe(num, "the number of estimates to import, note for now you can't do more than 10, required"));
				Out.Line(Prep.Describe(prof, "the profile code of the estimates, required"));
				Out.Line(Prep.Describe(delete, "should we delete what estimates are already in the database (T or F), defaults to true"));
				Out.Line(Prep.Describe(days, "how many days ago to set the modified, defaults to 0"));
				Out.Line(Prep.Describe(user, $"the user for the estimates, default set in {nameof(UserParams)} to {_defUser}"));
				Out.Line(Prep.Describe(status, "the status of the project, as an int, defaults to 1 (In Progress)"));
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			if (args.Count < 2)
			{
				Out.Error("not enough parameters passed to Import");
				Help();
				return;
			}

			var num = args[0].ToInt(5);
			var prof = args[1].ToUpper();
			var delete = true;
			var days = 0;
			var user = _defUser;
			var status = 1;

			bool del = false, day = false, usr = false, sts = false;

			if (args.Count > 2)
			{
				delete = !args[2].ToLower().Equals("f");
				del = true;
				if (args.Count > 3)
				{
					days = args[3].ToInt();
					day = true;
					if (args.Count > 4)
					{
						user = args[4];
						usr = true;
						if (args.Count > 5)
						{
							status = args[5].ToInt(1);
							sts = true;
						}
					}
				}
			}

			Logger.Instance.LogImport(num, del, day, usr, sts);
			DoImportEstimates(num, prof, delete, days, user, status);
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public ImportEstCommand(string defUser) => _defUser = defUser;

		#endregion Constructor

		#region Method

		private static void DoImportEstimates(int num, string prof, bool delete, int days, string user, int status)
		{
			var del = delete ? "T" : "F";
			var args = $"{del} 1 {user} {prof} {status} {num} {days}";
			IoCContainer.Instance.Resolve<IProgram>().LaunchBinProgram("ImportEstimatesTo28ForAutomation.exe", $"Import {args}", args, show: ShowOutput.Yes);
		}

		#endregion Method
	}
}
