﻿using System.Collections.Generic;
using System.Linq;

namespace Hub.Commands.Helpers.Resx
{
	/// <summary>
	/// handles the results after searching for translations
	/// </summary>
	public class ResxTResults
	{
		///
		public Dictionary<string, ResxTKeyResults> Keys { get; }

		///
		public ResxTResults(List<string> keys, List<string> folders)
		{
			Keys = keys.ToDictionary(k => k, k => new ResxTKeyResults(k, folders));

		}	// end ResxTResults(List<string>, List<string>)

		/// <summary>
		/// add the results that were just read from a lang file
		/// </summary>
		/// <param name="folder">the folder this language file was in</param>
		/// <param name="lang">the language the translations are in</param>
		/// <param name="ktv">the keys and their values</param>
		public void AddResultsFromLang(string folder, string lang, Dictionary<string, string> ktv)
		{
			foreach (var kvp in ktv)
			{
				Keys[kvp.Key].AddResultFromLang(folder, lang, kvp.Value);
			}	// end foreach

		}	// end AddResultsFromLang(string, string, Dictionary<string, string>

	}	// end ResxTResults

}	// end Hub.Commands.Helpers.Resx
