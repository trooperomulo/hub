﻿namespace Hub.Commands.Helpers.Resx
{
	/// <summary>
	/// displays the results held by an instance of <see cref="ResxTFolderResults" />
	/// </summary>
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
	public class ResxTFolderResultsDisplayer
	{
		private readonly ResxTFolderResults _results;

		///
		public ResxTFolderResultsDisplayer(ResxTFolderResults results) => _results = results;

		///
		public virtual void PrintResults()
		{
			Out.Dash();
			Out.Line(_results.Folder);
			Out.Dash();

			var values = _results.Values;

			if (values.Count < 1)
			{
				Out.Line("No translations found");
			}	// end if
			else
			{
				foreach (var (lang, val) in values)
				{
					Out.Line($"{lang} - {val}");
				}	// end foreach
			}	// end else
			Out.Blank();
			Out.Blank();

		}	// end PrintResults()

	}	// end ResxTFolderResultsDisplayer

}	// end Hub.Commands.Helpers.Resx
