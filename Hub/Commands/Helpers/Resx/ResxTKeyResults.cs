﻿using System.Collections.Generic;
using System.Linq;

namespace Hub.Commands.Helpers.Resx
{
	/// <summary>
	/// handles the results for one key
	/// </summary>
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
	public class ResxTKeyResults
	{
		/// <summary>
		/// list of results for each folder
		/// </summary>
		public virtual Dictionary<string, ResxTFolderResults> FolderResults { get; }

		///
		public virtual string Key { get; }

		///
		public ResxTKeyResults(string key, List<string> folders)
		{
			Key = key;
			FolderResults = folders.ToDictionary(f => f, f => new ResxTFolderResults(f));

		}	// end ResxTKeyResults(string, List<string>)

		///
		public virtual void AddResultFromLang(string folder, string lang, string value)
		{
			FolderResults[folder].AddResultFromLang(lang, value);

		}	// end AddResultFromLang(string, string, string)

	}	// end ResxTKeyResults

}	// end Hub.Commands.Helpers.Resx
