﻿using System.Collections.Generic;

namespace Hub.Commands.Helpers.Resx
{
	/// <summary>
	/// handles the results for key found in one folder
	/// </summary>
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
	public class ResxTFolderResults
	{
		/// <summary>
		/// the different translations found
		/// </summary>
		public List<(string lang, string val)> Values { get; }

		/// <summary>
		/// the folder (Core, Xm8, Shared)
		/// </summary>
		public string Folder { get; }

		///
		public ResxTFolderResults(string folder)
		{
			Folder = folder;
			Values = new List<(string lang, string val)>();

		}	// end ResxTFolderResults(string)

		///
		public virtual void AddResultFromLang(string lang, string val)
		{
			if (!val.IsNullOrEmpty())
			{
				Values.Add((lang, val));
			}	// end if

		}	// end AddResultFromLang(string, string)

	}	// end ResxTFolderResults

}	// end Hub.Commands.Helpers.Resx
