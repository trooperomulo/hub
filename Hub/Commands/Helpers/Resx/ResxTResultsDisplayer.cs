﻿using System;
using System.Linq;

namespace Hub.Commands.Helpers.Resx
{
	/// <summary>
	/// displays the results held in an instance of <see cref="ResxTResults" />
	/// </summary>
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
	public class ResxTResultsDisplayer
	{
		private readonly ResxTResults _results;

		///
		public static Func<ResxTKeyResults, ResxTKeyResultsDisplayer> GetChildDisplayer { private get; set; }

		///
		public ResxTResultsDisplayer(ResxTResults results) => _results = results;

		///
		public virtual void PrintResults()
		{
			var keys = _results.Keys.Keys.ToList();
			keys.Sort();
			foreach (var key in keys)
			{
				var displayer = GetChildDisplayer(_results.Keys[key]);
				displayer.PrintResults();
			}	// end foreach

		}	// end PrintResults()

	}	// end ResxTResultsDisplayer

}	// end Hub.Commands.Helpers.Resx
