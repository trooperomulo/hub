﻿using System;
using System.Linq;

namespace Hub.Commands.Helpers.Resx
{
	/// <summary>
	/// displays the results held by an instance of <see cref="ResxTKeyResults" />
	/// </summary>
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
	public class ResxTKeyResultsDisplayer
	{
		private readonly ResxTKeyResults _results;

		///
		public static Func<ResxTFolderResults, ResxTFolderResultsDisplayer> GetChildDisplayer { private get; set; }

		///
		public ResxTKeyResultsDisplayer(ResxTKeyResults results) => _results = results;

		///
		public virtual void PrintResults()
		{
			Out.Line($"Translations for RS.{_results.Key}");

			var keys = _results.FolderResults.Keys.ToList();
			keys.Sort();
			foreach (var key in keys)
			{
				var displayer = GetChildDisplayer(_results.FolderResults[key]);
				displayer.PrintResults();
			}	// end foreach

			Out.Dash();
			Out.Dash();

		}	// end PrintResults()

	}	// end ResxTKeyResultsDisplayer

}	// end Hub.Commands.Helpers.Resx
