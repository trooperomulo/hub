﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Enum;
using Hub.Interface;

namespace Hub.Commands.Helpers
{
	/// <summary>
	/// parses the arguments passed to email command
	/// </summary>
	public class EmailCommandArgumentParser
	{
		#region Variables
		
		private readonly bool _preferCurrent;
		private readonly string _gitXm8;
		private readonly bool _nextGen;

		private string _folder;
		private string _xm8;
		private string _data;
		private string _patch;
		private string _major;
		private string _minor;
		private string _build;
		private string _revision;

		#endregion Variables

		#region Constants

		private const string DataDefaultOrigin = "28";
		private const string Xm8DefaultOrigin = "main";

		private const string DataDefaultNextGen = "develop_nextGen";
		private const string Xm8DefaultNextGen = "develop_nextGen";

		#endregion Constants

		#region Defaults Properties

		private string DataDefault => _nextGen ? DataDefaultNextGen : DataDefaultOrigin;
		private string Xm8Default => _nextGen ? Xm8DefaultNextGen : Xm8DefaultOrigin;

		#endregion Defaults Properties

		#region Set Properties

		///
		public bool FolderSet { get; private set; }

		///
		public bool Xm8Set { get; private set; }

		///
		public bool DataSet { get; private set; }

		///
		public bool PatchSet { get; private set; }

		///
		public bool MajorSet { get; private set; }

		///
		public bool MinorSet { get; private set; }

		///
		public bool BuildSet { get; private set; }

		///
		public bool RevisionSet { get; private set; }

		#endregion Set Properties

		#region Properties

		///
		public string Folder => _folder;

		///
		public string Xm8 => _xm8;

		///
		public string Data => _data;

		///
		public string Patch => _patch;

		///

		public int Major => GetInt(MajorSet, _major, _nextGen ? 2 : -1);

		///
		public int Minor => MinorSet ? _minor.ToInt(-1) : -1;

		///
		public int Build => BuildSet ? _build.ToInt(-1) : -1;

		///
		public int Revision => RevisionSet ? _revision.ToInt(-1) : -1;

		#endregion Properties

		#region Constructor

		///
		public EmailCommandArgumentParser(bool preferCurrent, string gitXm8, bool nextGen, List<string> args)
		{
			_preferCurrent = preferCurrent;
			_gitXm8 = gitXm8;
			_nextGen = nextGen;
			ParseArgs(args);

		}	// end EmailCommandArgumentParser(bool, string, bool, List<string>)

		#endregion Constructor

		#region Methods
		
		private string DetermineValueForUnsetRepo(Repository repo, string repoDefault)
		{
			// there are four options here:
			// 1) We can't find any info					=> repo default
			// 2) Current matches xm8						=> xm8
			// 3) Current is different, xm8 doesn't exist	=> current
			// 4) Current is different, xm8 does exist		=> PreferCurrent ? current : xm8
			var git = IoCContainer.Instance.Resolve<IGit>();
			List<string> branches;
			try
			{
				Utils.NavigateToRepo(repo);
				branches = git.GetBranches(true).ToList();
			}	// end try
			catch
			{
				// we can't find any info => use the repo default
				return repoDefault;
			}	// end catch

			var current = git.GetBranchName();
			if (_gitXm8.Equals(current))
			{
				// current matches xm8 => use xm8
				return _gitXm8;
			}	// end if

			var exists = branches.Contains(_gitXm8);

			if (!exists)
			{
				// current is different, xm8 doesn't exist => use current
				return current;
			}	// end if

			// current is different, xm8 does exist => based on _userParams.PreferCurrent
			return _preferCurrent ? current : _gitXm8;

		}	// end DetermineValueForUnsetRepo(Repository, string)

		private static int GetInt(bool wasSet, string val, int def) => wasSet ? val.ToInt(def) : def;

		private void GetValues(List<string> args)
		{
			FolderSet = Utils.ReadFirstArg(args, Flags._flagFold, out _folder);
			Xm8Set = Utils.ReadFirstArg(args, Flags._flagXm81, out _xm8);
			DataSet = Utils.ReadFirstArg(args, Flags._flagData, out _data);
			PatchSet = Utils.ReadFirstArg(args, Flags._flagPatc, out _patch);
			MajorSet = Utils.ReadFirstArg(args, Flags._flagMajr, out _major);
			MinorSet = Utils.ReadFirstArg(args, Flags._flagMinr, out _minor);
			BuildSet = Utils.ReadFirstArg(args, Flags._flagBuil, out _build);
			RevisionSet = Utils.ReadFirstArg(args, Flags._flagRevi, out _revision);

		}	// end GetValues(List<string>)

		private void ParseArgs(List<string> args)
		{
			// Get Values From Arguments
			GetValues(args);

			// the folder path
			if (!FolderSet)
			{
				// ReSharper disable once StringLastIndexOfIsCultureSpecific.1
				_folder = _gitXm8.Substring(_gitXm8.LastIndexOf("/") + 1);
			}	// end if

			// the repository branches
			SetBranchNames();

		}	// end ParseArgs(List<string>)

		private static void SetBranchName(bool wasSet, Func<string, string> xm8, string def, ref string branch)
		{
			if (!wasSet)
			{
				branch = xm8(def);
			}	// end if
			else if ("default".Equals(branch))
			{
				branch = def;
			}	// end else if

		}	// end SetBranchName(bool, Func<string, string>, string, ref string)

		private void SetBranchNames()
		{
			SetBranchName(Xm8Set, def => _gitXm8, Xm8Default, ref _xm8);
			SetBranchName(DataSet, def => DetermineValueForUnsetRepo(Repository.Data, def), DataDefault, ref _data);

		}	// end SetBranchNames()

		#endregion Methods

	}	// end EmailCommandArgumentParser

}	// end Hub.Commands.Helpers
