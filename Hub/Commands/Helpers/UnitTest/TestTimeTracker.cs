﻿using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

namespace Hub.Commands.Helpers.UnitTest
{
	/// <summary>
	/// tracks unit test times
	/// </summary>
	public class TestTimeTracker
	{
		#region Constants

		///
		public const double DefaultTime = 2.6051973;	// I calculated this by running all the tests on main one time and averaging all times except the longest 10%
		///
		public static readonly string Path = System.IO.Path.Combine(Hub.ProgData, "TestTimes");

		private const int Tier1Num = 1;
		private const int Tier2Num = 2;
		private const int Tier3Num = 3;
		private const int Tier4Num = 4;
		private const int Tier5Num = 5;
		private const int Tier6Num = 8;
		private const int Tier7Num = 10;

		private const int Tier1 = Tier1Num;
		private const int Tier2 = Tier1 + Tier2Num;
		private const int Tier3 = Tier2 + Tier3Num;
		private const int Tier4 = Tier3 + Tier4Num;
		private const int Tier5 = Tier4 + Tier5Num;
		private const int Tier6 = Tier5 + Tier6Num;
		private const int Tier7 = Tier6 + Tier7Num;

		#endregion Constants

		#region Variables

		private readonly IFiles _files;
		private readonly Dictionary<string, List<double>> _times;

		#endregion Variables

		#region Constructor

		///
		public TestTimeTracker(IFiles files, IDirectories dirs)
		{
			_files = files;
			_times = new Dictionary<string, List<double>>();
			if (!dirs.Exists(Path))
			{
				dirs.CreateDirectory(Path);
			}	// end if

		}	// end TestTimeTracker(IFiles, IDirectories)

		#endregion Constructor

		#region Methods

		/// <summary>
		/// adds a new time to the front of the list
		/// </summary>
		public void AddTime(string solution, double time)
		{
			LoadFile(solution);
			if (_times.ContainsKey(solution))
			{
				_times[solution].Insert(0, time);
			}	// end if
			else
			{
				_times.Add(solution, new List<double> {time});
			}	// end else

			WriteFile(solution);

		}	// end AddTime(string, double)

		/// <summary>
		/// gets the entire list of times for the given solution
		/// </summary>
		public List<double> GetAllTimes(string solution)
		{
			LoadFile(solution);
			return _times.ContainsKey(solution) ? _times[solution] : new List<double>();

		}	// end GetAllTimes(string)

		/// <summary>
		/// gets the estimated time it will take for the given solution to run
		/// </summary>
		public double GetEstimatedTime(string solution)
		{
			LoadFile(solution);
			if (!_times.ContainsKey(solution))
			{
				return DefaultTime;
			}	// end if

			var times = _times[solution];
			double total = 0;
			int count = 0;
			for (var i = 0; i < times.Count; i++)
			{
				var weight = GetWeight(i);
				total += weight * times[i];
				count += weight;
			}	// end for

			return total / count;

		}	// end GetEstimatedTime(string)

		private static int GetWeight(int index)
		{
			if (index < Tier1)
				return 10;

			if (index < Tier2)
				return 8;

			if (index < Tier3)
				return 6;

			if (index < Tier4)
				return 5;

			if (index < Tier5)
				return 4;

			if (index < Tier6)
				return 3;

			if (index < Tier7)
				return 2;

			return 1;

		}	// end GetWeight(int)

		private void LoadFile(string solution)
		{
			if (_times.ContainsKey(solution))
			{
				return;
			}	// end if

			var file = System.IO.Path.Combine(Path, $"{solution}.txt");
			if (!_files.Exists(file))
			{
				return;
			}	// end if

			var lines = _files.ReadLines(file);
			var list = lines.Select(line => line.ToDoub(-1)).Where(doub => doub > 0).ToList();
			if (list.Count > 0)
			{
				_times.Add(solution, list);
			}	// end if

		}	// end LoadFile(string)

		private void WriteFile(string solution)
		{
			var times = _times[solution];
			var tms = new List<string>();
			foreach (var time in times)
			{
				// ReSharper disable once SpecifyACultureInStringConversionExplicitly
				tms.Add(time.ToString());
			}	// end foreach
			_files.WriteAllLines(System.IO.Path.Combine(Path, $"{solution}.txt"), tms);

		}	// end WriteFile(string)

		#endregion Methods

	}	// end TestTimeTracker

}	// end Hub.Commands.Helpers.UnitTest
