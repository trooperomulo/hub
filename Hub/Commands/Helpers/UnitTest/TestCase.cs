﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using Hub.Enum;

namespace Hub.Commands.Helpers.UnitTest
{
	/// <summary>
	/// Information for a single test case
	/// </summary>
	public class TestCase
	{
		#region Core Properties

		/// <summary>
		/// the name of the test run
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// the result of the test
		/// </summary>
		public TestCaseResult Result { get; }

		/// <summary>
		/// a message concerning the test, typically the failure message, or the reason the test has been ignored
		/// </summary>
		public string Message { get; }

		/// <summary>
		/// the call stack for a failed test
		/// </summary>
		public List<string> CallStack { get; }

		/// <summary>
		/// how long the test case took to run
		/// </summary>
		public TimeSpan Duration { get; }

		#endregion Core Properties

		#region Helper Properties

		private bool Pass => Result == TestCaseResult.Pass;
		private bool Fail => Result == TestCaseResult.Fail;
		private bool Skip => Result == TestCaseResult.Skip;
		private bool Incl => Result == TestCaseResult.Incl;

		#endregion Helper Properties

		#region Constructor

		///
		public TestCase(string name, TestCaseResult result, TimeSpan duration, string message = null, List<string> callStack = null)
		{
			Name = name;
			Result = result;
			Duration = duration;
			Message = message;
			CallStack = callStack ?? new List<string>();

		}	// end TestCase(string, TestCaseResult, TimeSpan, string, List<string>)

		#endregion Constructor

		#region Methods

		///
		public static TestCase FromXmlString(string xml)
		{
			var xDoc = new XmlDocument();
			xDoc.LoadXml(xml);

			if (!xDoc.FirstChild.Name.Equals("Case"))
			{
				return null;
			}	// end if

			var name = GetNameFromXml(xDoc);
			var result = GetResultFromXml(xDoc);
			var duration = GetDurationFromXml(xDoc);
			var message = GetMessageFromXml(xDoc);
			var stack = GetCallStackFromXml(xDoc);

			return new TestCase(name, result, duration, message, stack);

		}	// end FromXmlString(string)

		/// <summary>
		/// Gets the lines explaining this failure
		/// </summary>
		public List<string> GetFailureLines()
		{
			var result = new List<string>();

			if (!Fail)
			{
				return result;
			}	// end if

			result.AddRange(GetNameAndMessage());

			result.Add("");
			
			if (CallStack.Count == 0)
			{
				result.Add("No call stack was given");
			}	// end if
			else
			{
				result.AddRange(CallStack);
			}	// end else
			
			return result;

		}	// end GetFailureLines()

		/// <summary>
		/// gets the lines explaining this inconclusive
		/// </summary>
		public List<string> GetInconclusiveLines()
		{
			return Incl
				? GetNameAndMessage()
				: new List<string>();

		}	// end GetInconclusiveLines()

		/// <summary>
		/// gets the lines explaining this passing
		/// </summary>
		public List<string> GetPassedLines()
		{
			return Pass
				? new List<string> {GetName()}
				: new List<string>();

		}	// end GetPassedLines()

		/// <summary>
		/// gets the lines explaining this skip
		/// </summary>
		public List<string> GetSkippedLines()
		{
			return Skip
				? GetNameAndMessage()
				: new List<string>();

		}	// end GetSkippedLines()

		private List<string> GetNameAndMessage() => new List<string> {GetName(), Message.IsNullOrEmpty() ? "No message was given" : Message};

		private string GetName()
		{
			return Duration.Equals(null) || Duration.Equals(TimeSpan.Zero)
				? Name
				: $"{Name} {Duration.TotalSeconds}";

		}	// end GetName()

		///
		public string ToXmlString()
		{
			var xml = $"<Case name=\"{Name}\" res=\"{(int)Result}\" ticks=\"{Duration.Ticks}\">";

			if (!Message.IsNullOrEmpty())
			{
				xml += $"<msg>{Message}</msg>";
			}	// end if

			foreach (var line in CallStack)
			{
				xml += $"<line>{line}</line>";
			}	// end foreach

			xml += "</Case>";

			return xml;

		}	// end ToXmlString()
		
		/// <inheritdoc />
		public override string ToString()
		{
			List<string> lines;
			switch (Result)
			{
				case TestCaseResult.Pass:
					return GetName();
				case TestCaseResult.Fail:
					lines = GetFailureLines();
					break;
				case TestCaseResult.Skip:
					lines = GetSkippedLines();
					break;
				case TestCaseResult.Incl:
					lines = GetInconclusiveLines();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}   // end switch

			var sb = new StringBuilder();
			foreach (var line in lines)
			{
				sb.AppendLine(line);
			}	// end foreach

			return sb.ToString();

		}	// end ToString()

		#endregion Methods

		#region Get From Xml Methods

		private static List<string> GetCallStackFromXml(XmlDocument xml)
		{
			var stack = new List<string>();

			foreach (XmlNode child in xml.FirstChild.ChildNodes)
			{
				if (child.Name.Equals("line"))
				{
					stack.Add(child.InnerText);
				}	// end if
			}	// end foreach

			return stack;

		}	// end GetCallStackFromXml(XmlDocument)

		private static TimeSpan GetDurationFromXml(XmlDocument xml)
		{
			var duration = GetAttribute(xml, "ticks");
			return duration.IsNullOrEmpty() || !long.TryParse(duration, out var ticks)
				? TimeSpan.Zero
				: TimeSpan.FromTicks(ticks);

		}	// end GetDurationFromXml(XmlDocument)

		private static string GetMessageFromXml(XmlDocument xml)
		{
			foreach (XmlNode child in xml.FirstChild.ChildNodes)
			{
				if (child.Name.Equals("msg"))
				{
					return child.InnerText;
				}	// end if
			}	// end foreach
			
			return null;

		}	// end GetMessageFromXml(XmlDocument)

		private static string GetNameFromXml(XmlDocument xml) => GetAttribute(xml, "name");

		private static TestCaseResult GetResultFromXml(XmlDocument xml)
		{
			var res = GetAttribute(xml, "res");
			return res.IsNullOrEmpty() || !int.TryParse(res, out var result)
				? TestCaseResult.Fail
				: (TestCaseResult) result;

		}	// end GetResultFromXml(XmlDocument)

		private static string GetAttribute(XmlDocument xml, string attrName)
		{
			// ReSharper disable once PossibleNullReferenceException
			var name = xml.FirstChild.Attributes[attrName].Value;
			return name ?? "";

		}	// end GetAttribute(XmlDocument, string)

		#endregion Get From Xml Methods

	}	// end TestCase

}	// end Hub.Commands.Helpers.UnitTest
