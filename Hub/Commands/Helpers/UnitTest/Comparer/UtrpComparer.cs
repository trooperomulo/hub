﻿using System.Collections.Generic;
using System.Linq;

using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;

namespace Hub.Commands.Helpers.UnitTest.Comparer
{
	/// <summary>
	/// compares two instances of <see cref="UnitTestResultParser" />
	/// </summary>
	public class UtrpComparer
	{
		#region Properties

		///
		public List<UtrapComparer> Comparers { get; } = new List<UtrapComparer>();

		#endregion Properties

		#region Constructor

		///
		public UtrpComparer(UnitTestResultParser before, UnitTestResultParser after) => Compare(before, after);

		#endregion Constructor

		#region Methods

		private void Compare(UnitTestResultParser before, UnitTestResultParser after)
		{
			var common = before.Results.Intersect(after.Results, new UtrapComp()).Select(tc => tc.Solution).ToList();

			bool InCommon(UnitTestResultAssemblyParser parser) => common.Contains(parser.Solution);
			bool NotInCommon(UnitTestResultAssemblyParser parser) => !common.Contains(parser.Solution);

			HandleRemoved(before.Results.Where(NotInCommon));
			CompareCommon(before.Results.Where(InCommon).ToList(), after.Results.Where(InCommon).ToList());
			HandleAdded(after.Results.Where(NotInCommon));

			Sort();

		}	// end Compare(UnitTestResultParser, UnitTestResultParser)

		private void CompareCommon(List<UnitTestResultAssemblyParser> befores, List<UnitTestResultAssemblyParser> afters)
		{
			foreach (var before in befores)
			{
				var after = afters.First(tc => Equals(before.Solution, tc.Solution));
				Comparers.Add(new UtrapComparer(before, after));
			}	// end foreach

		}	// end CompareCommon(List<UnitTestResultAssemblyParser>, List<UnitTestResultAssemblyParser>)

		///
		public List<string> GetLines()
		{
			Sort();
			return UtrpcDisplayer.GetLines(Comparers);

		}	// end GetLines

		private void HandleAdded(IEnumerable<UnitTestResultAssemblyParser> added)
		{
			foreach (var add in added)
			{
				Comparers.Add(UtrapComparer.Added(add));
			}	// end foreach

		}	// end HandleAdded(IEnumerable<UnitTestResultAssemblyParser>)

		private void HandleRemoved(IEnumerable<UnitTestResultAssemblyParser> removed)
		{
			foreach (var remove in removed)
			{
				Comparers.Add(UtrapComparer.Removed(remove));
			}	// end foreach

		}	// end HandleRemoved(IEnumerable<UnitTestResultAssemblyParser>)

		private void Sort() => Comparers.Sort(new UtrapcComp());

		#endregion Methods

		#region Inner Classes

		private class UtrapComp : IEqualityComparer<UnitTestResultAssemblyParser>
		{
			#region Implementation of IEqualityComparer<in TestCase>

			/// <inheritdoc />
			public bool Equals(UnitTestResultAssemblyParser x, UnitTestResultAssemblyParser y) => Equals(x?.Solution, y?.Solution);

			/// <inheritdoc />
			public int GetHashCode(UnitTestResultAssemblyParser obj) => obj.Solution.GetHashCode();

			#endregion Implementation of IEqualityComparer<in TestCase>

		}	// end UtrapComp

		private class UtrapcComp : IComparer<UtrapComparer>
		{
			#region Implementation of IComparer<in UtrapComparer>

			/// <inheritdoc />
			// ReSharper disable once StringCompareIsCultureSpecific.1
			public int Compare(UtrapComparer x, UtrapComparer y) => string.Compare(x?.Solution, y?.Solution);

			#endregion Implementation of IComparer<in UtrapComparer>

		}	// end UtrapcComp

		#endregion Inner Classes

	}	// end UtrpComparer

}	// end Hub.Commands.Helpers.UnitTest.Comparer
