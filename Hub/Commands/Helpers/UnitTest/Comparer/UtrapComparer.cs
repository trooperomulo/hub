﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

namespace Hub.Commands.Helpers.UnitTest.Comparer
{
	/// <summary>
	/// compares two instances of <see cref="UnitTestResultAssemblyParser" />
	/// </summary>
	public class UtrapComparer
	{
		#region Properties

		///
		public List<TestCaseComparer> Comparers { get; } = new List<TestCaseComparer>();

		///
		public ComparisonResult Result { get; }

		///
		public UnitTestResultAssemblyParser Before { get; private set; }

		///
		public UnitTestResultAssemblyParser After { get; private set; }

		///
		public string Solution { get; private set; }

		#endregion Properties

		#region Constructors

		///
		public UtrapComparer(UnitTestResultAssemblyParser before, UnitTestResultAssemblyParser after)
			: this(ComparisonResult.Same)
		{
			if (!Equals(before.Solution, after.Solution))
			{
				throw new ArgumentException("Solution names don't match");
			}
			Solution = before.Solution;
			Before = before;
			After = after;
			Compare(before, after);

		}	// end UtrapComparer(UnitTestResultAssemblyParser, UnitTestResultAssemblyParser)

		private UtrapComparer(ComparisonResult result) => Result = result;

		#endregion Constructors

		#region Methods

		private void Compare(UnitTestResultAssemblyParser before, UnitTestResultAssemblyParser after)
		{
			var common = before.TestCases.Intersect(after.TestCases, new TCEqualComp()).Select(tc => tc.Name).ToList();
			
			bool InCommon(TestCase tc) => common.Contains(tc.Name);
			bool NotInCommon(TestCase tc) => !common.Contains(tc.Name);

			HandleRemoved(before.TestCases.Where(NotInCommon));
			CompareCommon(before.TestCases.Where(InCommon).ToList(), after.TestCases.Where(InCommon).ToList());
			HandleAdded(after.TestCases.Where(NotInCommon));

			Sort();

		}	// end Compare(UnitTestResultAssemblyParser, UnitTestResultAssemblyParser)

		private void CompareCommon(List<TestCase> befores, List<TestCase> afters)
		{
			foreach (var before in befores)
			{
				var name = before.Name;
				var after = afters.First(tc => Equals(name, tc.Name));
				Comparers.Add(new TestCaseComparer(before, after));
			}	// end foreach

		}	// end CompareCommon(List<TestCase>, List<TestCase>)

		private void HandleAdded(IEnumerable<TestCase> added)
		{
			foreach (var add in added)
			{
				Comparers.Add(TestCaseComparer.Added(add));
			}	// end foreach

		}	// end HandleAdded(IEnumerable<TestCase>)

		private void HandleRemoved(IEnumerable<TestCase> removed)
		{
			foreach (var remove in removed)
			{
				Comparers.Add(TestCaseComparer.Removed(remove));
			}	// end foreach

		}	// end HandleRemoved(IEnumerable<TestCase>)

		private void Sort() => Comparers.Sort(new TccComp());

		///
		public static UtrapComparer Added(UnitTestResultAssemblyParser added)
		{
			var comparer = new UtrapComparer(ComparisonResult.Added) {After = added, Solution = added.Solution};
			comparer.HandleAdded(added.TestCases);
			comparer.Sort();
			return comparer;

		}	// end Added(UnitTestResultAssemblyParser)

		///
		public static UtrapComparer Removed(UnitTestResultAssemblyParser removed)
		{
			var comparer = new UtrapComparer(ComparisonResult.Removed) {Before = removed, Solution = removed.Solution};
			comparer.HandleRemoved(removed.TestCases);
			comparer.Sort();
			return comparer;

		}	// end Removed(UnitTestResultAssemblyParser)

		#endregion Methods

		#region Inner Classes

		// ReSharper disable once InconsistentNaming
		private class TCEqualComp : IEqualityComparer<TestCase>
		{
			#region Implementation of IEqualityComparer<in TestCase>

			/// <inheritdoc />
			public bool Equals(TestCase x, TestCase y) => Equals(x?.Name, y?.Name);

			/// <inheritdoc />
			public int GetHashCode(TestCase obj) => obj.Name.GetHashCode();

			#endregion Implementation of IEqualityComparer<in TestCase>

		}	// end TCEqualComp

		private class TccComp : IComparer<TestCaseComparer>
		{
			#region Implementation of IComparer<in TestCaseComparer>

			/// <inheritdoc />
			// ReSharper disable once StringCompareIsCultureSpecific.1
			public int Compare(TestCaseComparer x, TestCaseComparer y) => string.Compare(x?.Test, y?.Test);

			#endregion Implementation of IComparer<in TestCaseComparer>

		}	// end TccComp

		#endregion Inner Classes

	}	// end UtrapComparer

}	// end Hub.Commands.Helpers.UnitTest.Comparer
