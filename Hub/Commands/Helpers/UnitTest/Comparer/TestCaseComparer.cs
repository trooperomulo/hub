﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Enum;

namespace Hub.Commands.Helpers.UnitTest.Comparer
{
	/// <summary>
	/// compares two instance of <see cref="TestCase" />
	/// </summary>
	public class TestCaseComparer
	{
		#region Properties

		/// <summary>
		/// how do they two cases compare
		/// </summary>
		public ComparisonResult ComparisonResult { get; private set; }

		/// <summary>
		/// if both broke differently, in what way
		/// </summary>
		public BrokeDifferentlyEnum BrokeDifferently { get; private set; }

		/// <summary>
		/// the test results afterwards
		/// </summary>
		public TestCase After { get; private set; }

		/// <summary>
		/// the test results beforehand
		/// </summary>
		public TestCase Before { get; private set; }

		/// <summary>
		/// the name of the test
		/// </summary>
		public string Test { get; private set; }

		#endregion Properties

		#region Constructors

		///
		public TestCaseComparer(TestCase before, TestCase after)
		{
			if (!Equals(before.Name, after.Name))
			{
				throw new ArgumentException("Names don't match");
			}	// end if
			After = after;
			Before = before;
			Test = before.Name;
			Compare(before, after);

		}	// end TestCaseComparer(TestCase, TestCase)

		private TestCaseComparer(ComparisonResult result) => ComparisonResult = result;

		///
		public static TestCaseComparer Added(TestCase after)
		{
			return new TestCaseComparer(ComparisonResult.Added) {After = after, Test = after.Name};

		}	// end Added(TestCase)

		///
		public static TestCaseComparer Removed(TestCase before)
		{
			return new TestCaseComparer(ComparisonResult.Removed) {Before = before, Test = before.Name};

		}	// end Removed(TestCase)

		#endregion Constructors

		#region Methods

		private void Compare(TestCase before, TestCase after)
		{
			BrokeDifferently = BrokeDifferentlyEnum.Not;
			switch (before.Result)
			{
				case TestCaseResult.Pass:
					BeforePassed(after);
					return;
				case TestCaseResult.Fail:
					BeforeFailed(after, before.Message, before.CallStack);
					return;
				case TestCaseResult.Skip:
					BeforeSkipped(after, before.Message);
					return;
				case TestCaseResult.Incl:
					BeforeIncl(after);
					return;
				default:
					throw new ArgumentOutOfRangeException(ThrowMessage(nameof(Compare)));
			}	// end switch

		}	// end Compare(TestCase, TestCase)

		private void BeforeFailed(TestCase after, string beforeMsg, List<string> beforeStack)
		{
			switch (after.Result)
			{
				case TestCaseResult.Fail:
					if (!Equals(after.Message, beforeMsg))
					{
						ComparisonResult = ComparisonResult.BrokenDifferently;
						BrokeDifferently = BrokeDifferentlyEnum.FailMessage;
					}	// end if
					else if (ListsAreDifferent(beforeStack, after.CallStack))
					{
						ComparisonResult = ComparisonResult.BrokenDifferently;
						BrokeDifferently = BrokeDifferentlyEnum.FailCallStack;
					}	// end else if
					else
					{
						ComparisonResult = ComparisonResult.Same;
					}	// end else
					return;
				case TestCaseResult.Pass:
					ComparisonResult = ComparisonResult.Fixed;
					return;
				case TestCaseResult.Skip:
					ComparisonResult = ComparisonResult.BrokenDifferently;
					BrokeDifferently = BrokeDifferentlyEnum.FailToSkip;
					return;
				case TestCaseResult.Incl:
					ComparisonResult = ComparisonResult.BrokenDifferently;
					BrokeDifferently = BrokeDifferentlyEnum.FailToIncl;
					return;
				default:
					throw new ArgumentOutOfRangeException(ThrowMessage(nameof(BeforeFailed)));
			}	// end switch

		}	// end BeforeFailed(TestCase, string, List<string>)

		private void BeforeIncl(TestCase after)
		{
			switch (after.Result)
			{
				case TestCaseResult.Pass:
					ComparisonResult = ComparisonResult.Fixed;
					return;
				case TestCaseResult.Incl:
					ComparisonResult = ComparisonResult.Same;
					return;
				case TestCaseResult.Fail:
					ComparisonResult = ComparisonResult.BrokenDifferently;
					BrokeDifferently = BrokeDifferentlyEnum.InclToFail;
					return;
				case TestCaseResult.Skip:
					ComparisonResult = ComparisonResult.BrokenDifferently;
					BrokeDifferently = BrokeDifferentlyEnum.InclToSkip;
					return;
				default:
					throw new ArgumentOutOfRangeException(ThrowMessage(nameof(BeforeIncl)));
			}	// end switch

		}	// end BeforeIncl(TestCase)

		private void BeforePassed(TestCase after)
		{
			switch (after.Result)
			{
				case TestCaseResult.Pass:
					ComparisonResult = ComparisonResult.Same;
					return;
				case TestCaseResult.Fail:
				case TestCaseResult.Skip:
				case TestCaseResult.Incl:
					ComparisonResult = ComparisonResult.Broken;
					return;
				default:
					throw new ArgumentOutOfRangeException(ThrowMessage(nameof(BeforePassed)));
			}	// end switch

		}	// end BeforePassed(TestCase)

		private void BeforeSkipped(TestCase after, string beforeMsg)
		{
			switch (after.Result)
			{
				case TestCaseResult.Skip:
					if (Equals(beforeMsg, after.Message))
					{
						ComparisonResult = ComparisonResult.Same;
					}	// end if
					else
					{
						ComparisonResult = ComparisonResult.BrokenDifferently;
						BrokeDifferently = BrokeDifferentlyEnum.SkipDiff;
					}	// end else
					return;
				case TestCaseResult.Pass:
					ComparisonResult = ComparisonResult.Fixed;
					return;
				case TestCaseResult.Fail:
					ComparisonResult = ComparisonResult.BrokenDifferently;
					BrokeDifferently = BrokeDifferentlyEnum.SkipToFail;
					return;
				case TestCaseResult.Incl:
					ComparisonResult = ComparisonResult.BrokenDifferently;
					BrokeDifferently = BrokeDifferentlyEnum.SkipToIncl;
					return;
				default:
					throw new ArgumentOutOfRangeException(ThrowMessage(nameof(BeforeSkipped)));
			}	// end switch

		}	// end BeforeSkipped(TestCase, string)

		private static bool ListsAreDifferent(List<string> beforeStack, List<string> afterStack)
		{
			return beforeStack.Count != afterStack.Count ||
			       beforeStack.Where((t, i) => !Equals(t, afterStack[i])).Any();

		}	// end ListsAreDifferent(List<string>, List<string>)

		private string ThrowMessage(string method)
		{
			return $"A new {nameof(TestCaseResult)} was added but isn't being addressed in {nameof(TestCaseComparer)}.{method}";

		}	// end ThrowMessage(string)

		#endregion Methods

	}	// end TestCaseComparer

}	// end Hub.Commands.Helpers.UnitTest.Comparer
