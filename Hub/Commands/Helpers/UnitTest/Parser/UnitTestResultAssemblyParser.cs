﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Hub.Commands.Helpers.UnitTest.Parser
{
	/// <summary>
	/// parses the results for a single assembly from the unit test result xml doc
	/// </summary>
	public class UnitTestResultAssemblyParser : IComparable<UnitTestResultAssemblyParser>
	{
		#region Properties

		/// <summary>
		/// the test cases that were run
		/// </summary>
		public List<TestCase> TestCases { get; }

		/// <summary>
		/// was the solution an invalid one
		/// </summary>
		public bool Invalid { get; private set; }

		/// <summary>
		/// the total number of failed tests
		/// </summary>
		public int NumFailed { get; }

		/// <summary>
		/// the total number of successful tests
		/// </summary>
		public int NumPassed { get; }

		/// <summary>
		/// the number of tests that were skipped
		/// </summary>
		public int NumSkipped { get; }

		/// <summary>
		/// the solution tested
		/// </summary>
		public string Solution { get; }

		/// <summary>
		/// was this result a failure
		/// </summary>
		public bool Failed => NumFailed > 0;

		/// <summary>
		/// were no tests run
		/// </summary>
		public bool NoneTested => TotalTests == 0 && !Invalid;

		/// <summary>
		/// was this result some tests were skipped, but none failed
		/// </summary>
		public bool Skipped => !Failed && NumSkipped > 0;

		/// <summary>
		/// was this result a success
		/// </summary>
		public bool Success => NumPassed == TotalTests && TotalTests > 0;

		/// <summary>
		/// the total amount of time all the tests took to run
		/// </summary>
		public TimeSpan TotalDuration { get; private set; }

		/// <summary>
		/// the total number of tests run
		/// </summary>
		public int TotalTests { get; }
		
		#endregion Properties

		#region Constructors

		///
		public UnitTestResultAssemblyParser(XElement xml, bool ignoreDuration)
		{
			TestCases = new List<TestCase>();
			Solution = xml.Attribute("name")?.Value;

			TotalTests = GetInt(xml, "total");
			NumPassed = GetInt(xml, "passed");
			NumSkipped = GetInt(xml, "skipped");
			NumFailed = GetInt(xml, "failed") + GetInt(xml, "inconclusive");

			LoadCases(xml, ignoreDuration);

			TotalDuration = TimeSpan.Zero;

			UpdateDuration();
		}

		///
		public UnitTestResultAssemblyParser(string sol, int fail, int pass, int skip, int tot, bool inv, List<TestCase> cases)
		{
			Solution = sol;
			NumFailed = fail;
			NumPassed = pass;
			NumSkipped = skip;
			TotalTests = tot;
			Invalid = inv;
			TestCases = cases;

			UpdateDuration();
		}

		#endregion Constructors

		#region Methods

		///
		public static UnitTestResultAssemblyParser FromXmlString(string xml)
		{
			var xDoc = new XmlDocument();
			xDoc.LoadXml(xml);

			if (!xDoc.FirstChild.Name.Equals("Result"))
			{
				return null;
			}

			var sol = GetSolutionFromXml(xDoc);
			var fail = GetFailedFromXml(xDoc);
			var pass = GetPassedFromXml(xDoc);
			var skip = GetSkippedFromXml(xDoc);
			var tot = GetTotalFromXml(xDoc);
			var inv = GetInvalidFromXml(xDoc);
			var cases = GetTestCasesFromXml(xDoc);

			return new UnitTestResultAssemblyParser(sol, fail, pass, skip, tot, inv, cases);
		}

		private static int GetInt(XElement ele, string name) => Convert.ToInt32(ele.Attribute(name)?.Value);

		private void LoadCases(XElement xml, bool ignoreDuration)
		{
			var eles = xml.Elements();
			foreach (var ele in eles)
			{
				switch (ele.Name.ToString())
				{
					case "test-suite":
						LoadCases(ele, ignoreDuration);
						continue;
					case "test-case":
						try
						{
							TestCases.Add(new UnitTestResultTestCaseParser(ele, ignoreDuration).TestCase);
						}
						// ReSharper disable once EmptyGeneralCatchClause - It shouldn't matter if it fails
						catch { }
						continue;
					case "reason":
						if (TotalTests > 0)
						{
							// since some tests were run, we can igore any node that isn't a test case or test suite
							continue;
						}
						// we'll have no tests for one of two reasons, the solution had no tests, or we tried to test an invalid solution
						// we tell those apart by the comment
						var message = ele.Elements("message").ToList()[0]?.Value.StripCData();
						Invalid = message?.StartsWith("File not found:") ?? false;
						continue;
					default:
						continue;
				}
				
			}

		}

		///
		public string ToXmlString()
		{
			var xml = $"<Result sol=\"{Solution}\" nF=\"{NumFailed}\" nP=\"{NumPassed}\" nS=\"{NumSkipped}\" tot=\"{TotalTests}\" inv=\"{(Invalid ? 1 : 0)}\">";

			foreach (var test in TestCases)
			{
				xml += test.ToXmlString();
			}

			xml += "</Result>";

			return xml;
		}

		private void UpdateDuration()
		{
			foreach (var test in TestCases)
			{
				TotalDuration = TotalDuration.Add(test.Duration);
			}
		}

		#endregion Methods

		#region Overrides of Object

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			// ReSharper disable once StringCompareToIsCultureSpecific
			return obj is UnitTestResultAssemblyParser other && Solution.CompareTo(other.Solution) == 0 &&
			       NumFailed == other.NumFailed && NumPassed == other.NumPassed && NumSkipped == other.NumSkipped &&
			       TotalTests == other.TotalTests && Invalid == other.Invalid;
		}

		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode()
		{
			return Solution.GetHashCode() + NumFailed * 329 + NumPassed * 203 + NumSkipped * 234 + TotalTests * 398;
		}

		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return $"{Solution} [{TotalDuration:g}] - P-{NumPassed}, F-{NumFailed}, S-{NumSkipped}, T-{TotalTests}";
		}

		#endregion Overrides of Object

		#region Implementation of IComparable<in UnitTestResultAssemblyParser>

		/// <inheritdoc />
		// ReSharper disable once StringCompareToIsCultureSpecific
		public int CompareTo(UnitTestResultAssemblyParser other) => Solution.CompareTo(other.Solution);

		#endregion Implementation of IComparable<in UnitTestResultAssemblyParser>

		#region Get From Xml Methods

		private static string GetAttribute(XmlDocument xml, string attrName)
		{
			// ReSharper disable once PossibleNullReferenceException
			var name = xml.FirstChild.Attributes[attrName].Value;
			return name ?? "";
		}

		private static int GetFailedFromXml(XmlDocument xml) => GetIntAttribute(xml, "nF");

		private static int GetIntAttribute(XmlDocument xml, string attrName) =>
			int.TryParse(GetAttribute(xml, attrName), out var val) ? val : 0;

		private static bool GetInvalidFromXml(XmlDocument xml) => GetIntAttribute(xml, "inv") == 1;

		private static int GetPassedFromXml(XmlDocument xml) => GetIntAttribute(xml, "nP");

		private static int GetSkippedFromXml(XmlDocument xml) => GetIntAttribute(xml, "nS");

		private static List<TestCase> GetTestCasesFromXml(XmlDocument xml)
		{
			var stack = new List<TestCase>();

			foreach (XmlNode child in xml.FirstChild.ChildNodes)
			{
				var tCase = TestCase.FromXmlString(child.OuterXml);
				if (tCase != null)
				{
					stack.Add(tCase);
				}
			}

			return stack;
		}

		private static int GetTotalFromXml(XmlDocument xml) => GetIntAttribute(xml, "tot");

		private static string GetSolutionFromXml(XmlDocument xml) => GetAttribute(xml, "sol");

		#endregion Get From Xml Methods
	}
}
