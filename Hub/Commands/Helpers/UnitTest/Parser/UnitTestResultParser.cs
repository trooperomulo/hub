﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Hub.Commands.Helpers.UnitTest.Parser
{
	/// <summary>
	/// parses the xml doc with the unit test results
	/// </summary>
	public class UnitTestResultParser
	{
		#region Properties

		/// <summary>
		/// the results from the tests
		/// </summary>
		public List<UnitTestResultAssemblyParser> Results { get; } = new List<UnitTestResultAssemblyParser>();

		/// <summary>
		/// the solutions tested
		/// </summary>
		public List<string> Solutions => Results.Select(sol => sol.Solution).ToList();
		
		/// <summary>
		/// how many solutions were failures
		/// </summary>
		public int SolutionsFailed => Results.Count(sol => sol.Failed);

		/// <summary>
		/// how many solutions were invalid
		/// </summary>
		public int SolutionsInvalid => Results.Count(sol => sol.Invalid);

		/// <summary>
		/// how many solutions had no tests
		/// </summary>
		public int SolutionsNoTests => Results.Count(sol => sol.NoneTested);

		/// <summary>
		/// how many solutions were successful
		/// </summary>
		public int SolutionsPassed => Results.Count(sol => sol.Success);

		/// <summary>
		/// how many solutions had skips, but no failures
		/// </summary>
		public int SolutionsSkips => Results.Count(sol => sol.Skipped);

		/// <summary>
		/// how many solutions were tested
		/// </summary>
		public int SolutionsTested => Results.Count;
		
		/// <summary>
		/// the total number of failed tests
		/// </summary>
		public int TestsFailed => Results.Sum(sol => sol.NumFailed);

		/// <summary>
		/// the total number of tests that passed
		/// </summary>
		public int TestsPassed => Results.Sum(sol => sol.NumPassed);

		/// <summary>
		/// the total number of tests that were skipped
		/// </summary>
		public int TestsSkipped => Results.Sum(sol => sol.NumSkipped);

		/// <summary>
		/// the total amount of time all the tests took to run
		/// </summary>
		public TimeSpan TotalDuration { get; set; }

		/// <summary>
		/// the total number of tests run
		/// </summary>
		public int TestsTotal => Results.Sum(sol => sol.TotalTests);

		#endregion Properties

		#region Constructors

		///
		public UnitTestResultParser(XElement xml, bool ignoreDuration)
		{
			var elements = xml.Elements();
			foreach (var ele in elements)
			{
				if (ele.Name == "test-suite" && ele.Attribute("type")?.Value == "Assembly")
				{
					Results.Add(new UnitTestResultAssemblyParser(ele, ignoreDuration));
					continue;
				}

				Results.AddRange(new UnitTestResultParser(ele, ignoreDuration).Results);
			}
			UpdateDuration();
		}

		///
		public UnitTestResultParser(List<UnitTestResultAssemblyParser> assemblies)
		{
			Results = assemblies;
			UpdateDuration();
		}

		#endregion Constructors

		#region Overrides of Object

		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj)
		{
			return obj is UnitTestResultParser other && Results.All(parser => other.Results.Contains(parser)) &&
			       other.Results.All(parser => Results.Contains(parser));
		}

		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode()
		{
			return Results.GetHashCode();
		}

		#endregion Overrides of Object

		#region Methods

		/// <summary>
		/// adds the results from another parser to this instance
		/// </summary>
		public void AddResults(UnitTestResultParser other)
		{
			Results.AddRange(other.Results);
			UpdateDuration();
		}

		///
		public static UnitTestResultParser FromXmlString(string xml)
		{
			var xDoc = new XmlDocument();
			xDoc.LoadXml(xml);

			if (!xDoc.FirstChild.Name.Equals("Results"))
			{
				return null;
			}

			return new UnitTestResultParser(GetResultsFromXml(xDoc));
		}

		private static List<UnitTestResultAssemblyParser> GetResultsFromXml(XmlDocument xml)
		{
			var stack = new List<UnitTestResultAssemblyParser>();

			foreach (XmlNode child in xml.FirstChild.ChildNodes)
			{
				var tCase = UnitTestResultAssemblyParser.FromXmlString(child.OuterXml);
				if (tCase != null)
				{
					stack.Add(tCase);
				}
			}

			return stack;
		}

		/// <summary>
		/// converts this instance into an xml formatted string
		/// </summary>
		public string ToXmlString()
		{
			var xml = "<Results>";

			foreach (var result in Results)
			{
				xml += result.ToXmlString();
			}

			xml += "</Results>";

			return xml;
		}

		private void UpdateDuration()
		{
			TotalDuration = TimeSpan.Zero;
			foreach (var result in Results)
			{
				TotalDuration = TotalDuration.Add(result.TotalDuration);
			}
		}

		#endregion Methods
	}
}
