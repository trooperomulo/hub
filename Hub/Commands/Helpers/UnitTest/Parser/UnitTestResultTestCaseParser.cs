﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Hub.Enum;

namespace Hub.Commands.Helpers.UnitTest.Parser
{
	/// <summary>
	/// parses the xml for an individual test result
	/// </summary>
	public class UnitTestResultTestCaseParser
	{
		#region Properties

		/// <summary>
		/// the test case generated from the xml
		/// </summary>
		// ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local - needed so there is a set method for reflection to find for unit tests
		public TestCase TestCase { get; private set; }

		private List<string> Stack { get; } = new List<string>();

		#endregion Properties

		#region Constructor

		///
		public UnitTestResultTestCaseParser(XElement xml, bool ignoreDuration)
		{
			var name = xml.Attribute("name")?.Value;
			var result = GetTestCaseResult(xml.Attribute("result")?.Value);
			var duration = (xml.Attribute("duration")?.Value).ToTimeSpan(ignoreDuration);
			string message = null;

			// ReSharper disable once ConvertIfStatementToSwitchStatement
			if (result == TestCaseResult.Fail)
			{
				GetFailedProps(xml, out message);
			}
			else if (result == TestCaseResult.Skip)
			{
				GetSkippedProps(xml, out message);
			}

			TestCase = new TestCase(name, result, duration, message, Stack);
		}

		#endregion Constructor

		#region Methods
		
		private void AddTrim(string str) => Stack.Add(str.Trim());

		private static TestCaseResult GetTestCaseResult(string result)
		{
			switch (result)
			{
				case "Passed":
					return TestCaseResult.Pass;
				case "Failed":
					return TestCaseResult.Fail;
				case "Skipped":
					return TestCaseResult.Skip;
				case "Inconclusive":
					return TestCaseResult.Incl;
				default:
					var method = $"{nameof(UnitTestResultTestCaseParser)}.{nameof(GetTestCaseResult)}";
					throw new ArgumentOutOfRangeException($"A new {nameof(TestCaseResult)} was added but isn't being addressed in {method}");
			}
		}

		private void GetFailedProps(XContainer xml, out string message)
		{
			message = null;
			var elements = xml.Elements("failure").ToList();
			if (elements.Count != 1)
			{
				return;
			}

			var failure = elements[0];
			var messages = failure.Elements("message").ToList();
			if (messages.Count != 1)
			{
				return;
			}

			var traces = failure.Elements("stack-trace").ToList();
			if (traces.Count != 1)
			{
				return;
			}
			
			message = messages[0].Value.Trim();
			traces[0].Value.Trim().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(AddTrim);
		}

		private static void GetSkippedProps(XContainer xml, out string message)
		{
			message = null;

			var reasons = xml.Elements("reason").ToList();
			if (reasons.Count != 1)
			{
				return;
			}

			var messages = reasons.Elements("message").ToList();
			if (messages.Count != 1)
			{
				return;
			}

			message = messages[0].Value.StripCData();
		}

		#endregion Methods
	}
}
