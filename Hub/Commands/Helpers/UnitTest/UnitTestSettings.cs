﻿using System.Collections.Generic;
using System.Linq;

using Hub.Enum;
using Hub.Interface;

namespace Hub.Commands.Helpers.UnitTest
{
	/// <inheritdoc />
	public class UnitTestSettings : IUnitTestSettings
	{
		#region Defaults

		private const bool DefEstimateTime = false;
		private const Progress DefProgress = Progress.Solution;
		private const bool DefShowTime = false;
		private const bool DefResultsConsole = false;
		private const bool DefResultsTextFile = true;
		private const bool DefResultsXmlFile = false;
		private const int DefInputFile = -1;

		#endregion Defaults

		#region User Defaults

		private bool? UserEstimateTime { get; set; }
		private Progress? UserProgress { get; set; }
		private bool? UserShowTime { get; set; }
		private bool? UserResultsConsole { get; set; }
		private bool? UserResultsTextFile { get; set; }
		private bool? UserResultsXmlFile { get; set; }
		private int? UserInputFile { get; set; }

		#endregion User Defaults

		#region Argurments

		private bool? ArgEstimateTime { get; set; }
		private Progress? ArgProgress { get; set; }
		private bool? ArgShowTime { get; set; }
		private bool? ArgResultsConsole { get; set; }
		private bool? ArgResultsTextFile { get; set; }
		private bool? ArgResultsXmlFile { get; set; }
		private int? ArgInputFile { get; set; }

		#endregion Argurments

		#region Public Properties

		///
		public static string UserDefault { private get; set; }

		/// <inheritdoc />
		public bool EstimateTime => GetPropertyValue(ArgEstimateTime, UserEstimateTime, DefEstimateTime);

		/// <inheritdoc />
		public Progress Progress => GetPropertyValue(ArgProgress, UserProgress, DefProgress);

		/// <inheritdoc />
		public bool ShowTime => GetPropertyValue(ArgShowTime, UserShowTime, DefShowTime);

		/// <inheritdoc />
		public bool ResultsConsole => GetPropertyValue(ArgResultsConsole, UserResultsConsole, DefResultsConsole);

		/// <inheritdoc />
		public bool ResultsTextFile => GetPropertyValue(ArgResultsTextFile, UserResultsTextFile, DefResultsTextFile);

		/// <inheritdoc />
		public bool ResultsXmlFile => GetPropertyValue(ArgResultsXmlFile, UserResultsXmlFile, DefResultsXmlFile);

		/// <inheritdoc />
		// ReSharper disable once ConstantNullCoalescingCondition - for some reason ReSharper sometimes thinks that UserInputFile is never null
		public int InputFile => ArgInputFile ?? (UserInputFile ?? DefInputFile);

		#endregion Public Properties

		#region Constructor

		///
		public UnitTestSettings() => LoadUserDefaults();

		#endregion Constructor

		#region Methods

		private static Progress? GetProgressFromString(string val, string source)
		{
			switch (val)
			{
				case Flags._fPrNo:
					return Progress.None;
				case Flags._fPrTm:
					return Progress.Time;
				case Flags._fPrSl:
					return Progress.Solution;
				default:
					Out.Line($"invalid value passed for progress from {source}");
					return null;
			}	// end switch

		}	// end GetProgressFromString(string, string)

		private static T GetPropertyValue<T>(T? arg, T? user, T def) where T : struct => arg ?? (user ?? def);

		private void LoadUserDefaults()
		{
			ParseString(UserDefault?.Split(' ').ToList(), "user params", out var est, out var prog, out var show, out var cons, out var text,
				out var xml, out var input);
			UserEstimateTime = est;
			UserProgress = prog;
			UserShowTime = show;
			UserResultsConsole = cons;
			UserResultsTextFile = text;
			UserResultsXmlFile = xml;
			UserInputFile = input;

		}	// end LoadUserDefaults()

		/// <inheritdoc />
		public void LoadArguments(List<string> args)
		{
			ParseString(args, "the arguments", out var est, out var prog, out var show, out var cons, out var text,
				out var xml, out var input);
			ArgEstimateTime = est;
			ArgProgress = prog;
			ArgShowTime = show;
			ArgResultsConsole = cons;
			ArgResultsTextFile = text;
			ArgResultsXmlFile = xml;
			ArgInputFile = input;

		}	// end LoadArguments(List<string>)

		private static void ParseString(List<string> args, string source, out bool? est, out Progress? prog,
			out bool? show, out bool? cons, out bool? text, out bool? xml, out int? input)
		{
			est = show = cons = text = xml = null;
			prog = null;
			input = null;

			if (args == null || args.Count < 1 || (args.Count == 1 && args[0].IsNullOrEmpty()))
			{
				return;
			}	// end if

			var origCount = args.Count;

			est = Utils.ReadFirstArg(args, Flags._flagTEst);

			if (Utils.ReadFirstArg(args, Flags._flagProg, out var val))
			{
				prog = GetProgressFromString(val, source);
			}	// end if

			show = Utils.ReadFirstArg(args, Flags._flagShTm);
			cons = Utils.ReadFirstArg(args, Flags._flagRsCn);
			text = Utils.ReadFirstArg(args, Flags._flagRsTx);
			xml = Utils.ReadFirstArg(args, Flags._flagRsXm);

			if (Utils.ReadFirstArg(args, Flags._flagInpt, out var value))
			{
				if (int.TryParse(value, out var num))
				{
					input = num;
				}	// end if
			}	// end if

			if (args.Count != origCount)
			{
				return;
			}	// end if

			// if we didn't use any of the arguments, everything needs to be null
			est = show = cons = text = xml = null;
			prog = null;
			input = null;

		}	// end ParseString(List<string>, string, out bool?, out Progress?, out bool?, out bool?, out bool?, out bool?, out int?)

		#endregion Methods

	}	// end UnitTestSettings

}	// end Hub.Commands.Helpers.UnitTest

