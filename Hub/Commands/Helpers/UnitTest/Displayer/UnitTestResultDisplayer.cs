﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

// ReSharper disable ReturnTypeCanBeEnumerable.Local
namespace Hub.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// displays the unit test results
	/// </summary>
	public class UnitTestResultDisplayer
	{
		#region Constants

		private const string ShortLine = "--------";
		private const string MedLine = ShortLine + ShortLine;
		private const string LongLine = MedLine + ShortLine;

		#endregion Constants

		#region Variable

		private readonly UnitTestResultParser _parser;

		#endregion Variable

		#region Constructor

		///
		public UnitTestResultDisplayer(UnitTestResultParser parser) => _parser = parser;

		#endregion Constructor

		#region Public Methods

		///
		public List<string> GetResultsToDisplayInConsole() => GetResultsToDisplay(true);

		///
		public List<string> GetResultsToDisplayInTextFile() => GetResultsToDisplay(false);

		#endregion Public Methods

		#region Private Methods

		#region General

		private Func<int, string, List<string>> GetHeaderMethod(bool console) => console ? (Func<int, string, List<string>>) Head : Header;

		private List<string> GetResultsToDisplay(bool console)
		{
			if (_parser.SolutionsTested < 1)
			{
				return NothingTested();
			}	// end if

			var lines = new List<string>();
			lines.AddRange(PrintInvalid(console));
			lines.AddRange(PrintNoTests(console));
			lines.AddRange(PrintSucesses(console));
			lines.AddRange(PrintSkips(console));
			lines.AddRange(PrintFailures(console));
			return lines;

		}	// end GetResultsToDisplay(bool)

		private static List<string> Head(int count, string header) => new List<string> {LongLine, $"{count} {header.ToUpper()}"};

		private static List<string> Header(int count, string header) => new List<string> {LongLine, $"{count} {header.ToUpper()}", LongLine, ""};

		private static List<string> NothingTested() => new List<string> {"No solutions were tested"};

		private List<string> PrintGroup(int count, string title, bool console,
			Func<UnitTestResultAssemblyParser, bool> filter,
			Func<UnitTestResultAssemblyParser, List<string>> solToLines)
		{
			if (count < 1)
			{
				return new List<string>();

			}	// end if

			var lines = new List<string>();
			var header = GetHeaderMethod(console);

			lines.AddRange(header(count, title));
			var solutions = _parser.Results.Where(filter).ToList();
			solutions.Sort();

			foreach (var solution in solutions)
			{
				lines.AddRange(solToLines(solution));
				if (!console)
				{
					lines.Add("");
				}	// end if
			}	// end foreach

			if (console)
			{
				lines.Add("");
			}	// end if

			return lines;

		}	// end PrintGroup(int, string, bool, Func<UnitTestResultAssemblyParser, bool>, Func<UnitTestResultAssemblyParser, List<string>>)

		private static string Result(string dll, int count, int total, string verb) =>
			$"{dll} - {count} of {total} {(total == 1 ? "Test" : "Tests")} {verb}";

		#endregion General

		#region Failure

		private static List<string> Failure(UnitTestResultAssemblyParser aPar, bool skippedSome, bool console)
		{
			var lines = new List<string>
			{
				MedLine,
				Result(aPar.Solution, aPar.NumFailed + aPar.NumSkipped, aPar.TotalTests,
					skippedSome ? "failed/skipped" : "failed")
			};

			if (!console)
			{
				lines.Add(MedLine);
			}	// end if

			return lines;

		}	// end Failure(UnitTestResultAssemblyParser, bool, bool)

		private static List<string> GetFailureLines(UnitTestResultAssemblyParser aPar, bool console)
		{
			var lines = new List<string>();
			var count = 1;
			foreach (var @case in aPar.TestCases)
			{
				// get the inconclusive and skipped lines as well
				var fails = @case.GetFailureLines();
				if (fails.Count < 1)
				{
					fails = @case.GetSkippedLines();
					if (fails.Count < 1)
					{
						continue;
					}	// end if
				}	// end if

				var title = @case.Result == TestCaseResult.Fail ? "Failure" : "Skip";
				var nums = $"{title} {count++} of {aPar.NumFailed + aPar.NumSkipped}";
				if (console)
				{
					fails[0] = $"{nums} - {fails[0]}";
					lines.Add(ShortLine);
				}	// end if
				else
				{
					lines.Add(nums);
					lines.Add("");
				}	// end else

				lines.AddRange(fails);

				if(!console)
				{
					lines.Add(ShortLine);
				}	// end if
			}	// end foreach

			return lines;

		}	// end GetFailureLines(UnitTestResultAssemblyParser, bool)

		private List<string> PrintFailures(bool console)
		{
			var count = _parser.SolutionsFailed;

			List<string> Getter(UnitTestResultAssemblyParser parser)
			{
				var lines = new List<string>();

				lines.AddRange(Failure(parser, parser.NumSkipped > 0, console));
				lines.AddRange(GetFailureLines(parser, console));
				if (!console)
				{
					lines.Add(LongLine);
				}	// end if

				return lines;
			}	// end Getter(UnitTestResultAssemblyParser)

			return PrintGroup(count, count == 1 ? "Failure" : "Failures", console, r => r.Failed, Getter);

		}	// end PrintFailures(bool)

		#endregion Failure

		#region Invalid

		private List<string> PrintInvalid(bool console)
		{
			return PrintGroup(_parser.SolutionsInvalid, "Invalid", console, r => r.Invalid,
				p => new List<string> {p.Solution});

		}	// end PrintInvalid(bool)

		#endregion Invalid

		#region No Tests

		private List<string> PrintNoTests(bool console)
		{
			return PrintGroup(_parser.SolutionsNoTests, "No Tests", console, r => r.NoneTested,
				p => new List<string> {p.Solution});

		}	// end PrintNoTests(bool)

		#endregion No Tests

		#region Skips

		private static List<string> Skip(UnitTestResultAssemblyParser aPar, bool console)
		{
			var lines = new List<string>
			{
				MedLine,
				Result(aPar.Solution, aPar.NumSkipped, aPar.TotalTests, "skipped")
			};

			if (!console)
			{
				lines.Add(MedLine);
			}	// end if

			return lines;

		}	// end Skip(UnitTestResultAssemblyParser, bool)

		private static List<string> GetSkipLines(UnitTestResultAssemblyParser aPar, bool console)
		{
			var lines = new List<string>();
			var count = 1;
			foreach (var @case in aPar.TestCases)
			{
				var skips = @case.GetSkippedLines();
				if (skips.Count <= 0)
				{
					continue;
				}

				var nums = $"Skip {count++} of {aPar.NumSkipped}";
				if (console)
				{
					skips[0] = $"{nums} - {skips[0]}";
					lines.Add(ShortLine);
				}	// end if
				else
				{
					lines.Add(nums);
					lines.Add("");
				}	// end else

				lines.AddRange(skips);

				if(!console)
				{
					lines.Add(ShortLine);
				}	// en dif
			}	// end foreach

			return lines;

		}	// end GetSkipLines(UnitTestResultAssemblyParser, bool)

		private List<string> PrintSkips(bool console)
		{
			var count = _parser.SolutionsSkips;

			List<string> Getter(UnitTestResultAssemblyParser parser)
			{
				var lines = new List<string>();

				lines.AddRange(Skip(parser, console));
				lines.AddRange(GetSkipLines(parser, console));
				if (!console)
				{
					lines.Add(LongLine);
				}	// end if

				return lines;
			}	// end Getter(UnitTestResultAssemblyParser)

			return PrintGroup(count, count == 1 ? "Skip" : "Skips", console, r => r.Skipped, Getter);

		}	// end PrintSkips(bool)

		#endregion Skips

		#region Success

		private List<string> PrintSucesses(bool console)
		{
			var count = _parser.SolutionsPassed;
			return PrintGroup(count, count == 1 ? "Success" : "Successes", console, r => r.Success, Success);

		}	// end PrintSucesses(bool)

		private static List<string> Success(UnitTestResultAssemblyParser aPar)
		{
			return new List<string> {Result(aPar.Solution, aPar.TotalTests, aPar.TotalTests, "passed")};

		}	// end Success(UnitTestResultAssemblyParser)

		#endregion Success

		#endregion Private Methods

	}	// end UnitTestResultDisplayer

}	// end Hub.Commands.Helpers.UnitTest.Displayer
