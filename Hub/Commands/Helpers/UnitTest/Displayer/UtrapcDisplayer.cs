﻿using System.Collections.Generic;

using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Enum;

using CDU = Hub.Commands.Helpers.UnitTest.Displayer.ComparerDisplayerUtils;

namespace Hub.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// displays the results from a <see cref="UtrapComparer" />
	/// </summary>
	public static class UtrapcDisplayer
	{
		///
		public static List<string> GetLines(UtrapComparer comparer)
		{
			var results = new List<string>();

			results.AddRange(Header(comparer.Solution, comparer.Result));

			var count = comparer.Comparers.Count;
			for (var i = 0; i < count; i++)
			{
				results.AddRange(TccDisplayer.GetLines(comparer.Comparers[i], i + 1, count, comparer.Result));
			}	// end for

			// the last line is currently some dashes, we want it cleared
			results[results.Count - 1] = CDU.Blank();

			return results;

		}	// end GetLines(UtrapColmparer)

		private static List<string> Header(string name, ComparisonResult result)
		{
			var head = CDU.Center(name);
			return new List<string>
			{
				CDU.Header(result),
				CDU.Line(head, result),
				CDU.Header(result)
			};

		}	// end Header(name, ComparisonResult)

	}	// end UtrapcDisplayer

}	// end Hub.Commands.Helpers.UnitTest.Displayer
