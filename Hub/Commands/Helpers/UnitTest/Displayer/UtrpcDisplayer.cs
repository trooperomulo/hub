﻿using System.Collections.Generic;

using Hub.Commands.Helpers.UnitTest.Comparer;

namespace Hub.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// displays the results of the <see cref="UtrpComparer"/>
	/// </summary>
	public static class UtrpcDisplayer
	{
		/// <summary>
		/// gets the lines from the given comparers
		/// </summary>
		public static List<string> GetLines(List<UtrapComparer> comparers)
		{
			var results = new List<string>();

			foreach (var comparer in comparers)
			{
				results.AddRange(UtrapcDisplayer.GetLines(comparer));
			}	// end foreach

			results.RemoveAt(results.Count - 1);

			return results;

		}	// end GetLines(List<UtrapComparer>)

	}	// end UtrpcDisplayer

}	// end Hub.Commands.Helpers.UnitTest.Displayer
