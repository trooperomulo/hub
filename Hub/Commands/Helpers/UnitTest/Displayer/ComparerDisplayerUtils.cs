﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Enum;

namespace Hub.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// Utility class for the comparer displayer classes
	/// </summary>
	public static class ComparerDisplayerUtils
	{
		#region Constants

		private const int ResLen = 4;

		private const char Sep = '|';

		private const string TestSep = " - ";

		private const int Width = 70;

		private static readonly string HeaderLine = string.Concat(Enumerable.Repeat("-", Width));

		private static readonly string Buffer = string.Concat(Enumerable.Repeat(" ", Width));

		#endregion Constants

		#region Public Methods

		///
		public static string Blank() => Both(Buffer);

		///
		public static string Center(string str) => Utils.Center(str, Width);

		///
		public static string Header(ComparisonResult result) => Line(HeaderLine, result);

		///
		public static string Line(string str, ComparisonResult result)
		{
			var len = str.Length;
			if (len > Width)
			{
				throw new ArgumentException($"Passed in string is longer ({len}) than max width ({Width})");
			}	// end if

			Func<string, string> method;
			switch (result)
			{
				case ComparisonResult.Added:
					method = After;
					break;
				case ComparisonResult.Removed:
					method = Before;
					break;
				case ComparisonResult.Same:
				case ComparisonResult.Fixed:
				case ComparisonResult.Broken:
				case ComparisonResult.BrokenDifferently:
					method = Both;
					break;
				default:
					throw new ArgumentException();
			}	// end switch

			return method(FillOut(str));

		}	// end Line(string, ComparisonResult)

		///
		public static string Line(string right, string left) => Comb(FillOut(right), FillOut(left));

		///
		public static string Result(ComparisonResult result)
		{
			switch (result)
			{
				case ComparisonResult.Same:
				case ComparisonResult.Added:
				case ComparisonResult.Removed:
				case ComparisonResult.Fixed:
				case ComparisonResult.Broken:
					return $"{result}";
				case ComparisonResult.BrokenDifferently:
					return "Broken Differently";
				default:
					throw new ArgumentOutOfRangeException(nameof(result), result, null);
			}	// end switch

		}	// end Result(ComparisonResult)
		
		///
		public static List<string> Test(string test, TestCaseResult? before, TestCaseResult? after)
		{
			if (!before.HasValue && !after.HasValue)
			{
				throw new ArgumentException();
			}	// end if

			if (before.HasValue && after.HasValue)
			{
				return TestBoth(test, before.Value, after.Value);
			}	// end if

			if (before.HasValue)
			{
				return TestOne(test, before.Value, Before);
			}	// end if

			return TestOne(test, after.Value, After);

		}	// end Test(string, TestCaseResult?, TestCaseResult?)

		///
		public static List<string> TestBoth(string test, TestCaseResult before, TestCaseResult after)
		{
			// There are 4 possible scenarios:
			// 1 - test name is short enough to include the result afterwards
			// 2 - test name needs to be shared across the columns, with the results either side
			// 3 - test name doesn't need to be wrapped, but there isn't enough room for the results
			// 4 - test name is so long it needs to be wrapped

			var nameAndRes = test.Length + TestSep.Length + ResLen;
			const int fullWidth = Width * 2 + 1;

			// Scenario 1
			if (nameAndRes <= Width)
			{
				return new List<string> {TestResults(test, before, after)};
			}	// end if

			// Scenario 2
			if (nameAndRes + TestSep.Length + ResLen <= fullWidth)
			{
				return new List<string>
				{
					Utils.Center($"{before}{TestSep}{test}{TestSep}{after}", fullWidth)
				};
			}	// end if

			var resultsLine = Line($"{before}", $"{after}");

			// Scenario 3
			if (test.Length <= fullWidth)
			{
				return new List<string> { Utils.Center(test, fullWidth), resultsLine };
			}	// end if

			// Scenario 4
			var results = Wrap(test, fullWidth);

			results.Add(resultsLine);

			return results;

		}	// end TestBoth(string, TestCaseResult, TestCaseResult)

		///
		public static List<string> TestOne(string test, TestCaseResult result, Func<string, string> getLine)
		{
			// There are 3 possible scenarios:
			// 1 - test name is short enough to include the result afterwards
			// 2 - test name doesn't need to be wrapped, but there isn't enough room for the results
			// 3 - test name is so long it needs to be wrapped

			var nameAndRes = test.Length + TestSep.Length + ResLen;

			// Scenario 1
			if (nameAndRes <= Width)
			{
				return new List<string> {getLine(FillOut($"{test}{TestSep}{result}"))};
			}	// end if

			// Scenario 2
			if (test.Length <= Width)
			{
				return new List<string>
				{
					getLine(FillOut(test)),
					getLine(FillOut($"{result}"))
				};
			}	// end if

			// Scenario 3
			var wrapped = Wrap(test);
			var results = new List<string>();
			foreach (var toWrap in wrapped)
			{
				results.Add(getLine(FillOut(toWrap)));
			}	// end foreach
			results.Add(getLine(FillOut($"{result}")));
			return results;

		}	// end TestOne(string, TestCaseResult, Func<string, string>)

		///
		public static string Test(string name, TestCaseResult? result)
		{
			return !result.HasValue ? Buffer : FillOut($"{name}{TestSep}{result}");

		}	// end Test(string, TestCaseResult?)

		///
		public static string TestResults(string test, TestCaseResult? befResult, TestCaseResult? aftResult)
		{
			if (!befResult.HasValue && !aftResult.HasValue)
			{
				throw new ArgumentException("One test case needs to have a result");
			}	// end if

			return Comb(Test(test, befResult), Test(test, aftResult));

		}	// end TestResults(string, TestCaseResult?, TestCaseResult?)

		///

		public static List<string> Wrap(string toWrap, int len = Width)
		{
			if (toWrap.Length < len)
			{
				return new List<string> {FillOut(toWrap, len)};
			}	// end if

			var lines = new List<string> {toWrap.Substring(0, len) };

			var temp = $" -{toWrap.Substring(len)}";

			while (temp.Length > len)
			{
				lines.Add(temp.Substring(0, len));
				temp = $" -{temp.Substring(len)}";
			}	// end while

			lines.Add(FillOut(temp, len));

			return lines;

		}	// end Wrap(string, int)

		#endregion Public Methods

		#region Private Methods

		private static string After(string str) => Comb(Buffer, str);

		private static string Before(string str) => Comb(str, Buffer);

		private static string Both(string str) => Comb(str, str);

		private static string Comb(string front, string back) => $"{front}{Sep}{back}";

		private static string FillOut(string str, int width = Width) => Prep.FillOut(str, width);

		#endregion Private Methods

	}	// end ComparerDisplayerUtils

}	// end Hub.Commands.Helpers.UnitTest.Displayer
