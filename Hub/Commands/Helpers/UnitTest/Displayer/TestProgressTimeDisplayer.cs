﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hub.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// displays the time progress when running tests
	/// </summary>
	public class TestProgressTimeDisplayer
	{
		private List<(string sol, bool run, double time)> Solutions { get; }
		
		///
		public string Display => $"{ElapsedTime:g} passed with an estimated {RemainingTime:g} remaining - {Percent:P}";

		/// <summary>
		/// the number of seconds that have already passed
		/// </summary>
		public double ElapsedSecs => Solutions.Where(sol => sol.run).Sum(sol => sol.time);

		private TimeSpan ElapsedTime => TimeSpan.FromSeconds(ElapsedSecs);

		private decimal Percent => Math.Abs(ElapsedSecs + RemainingSecs) < .001
			? 0
			: (decimal) ElapsedSecs / ((decimal) ElapsedSecs + (decimal) RemainingSecs);

		/// <summary>
		/// the number of seconds remaining
		/// </summary>
		public double RemainingSecs => Solutions.Where(sol => !sol.run).Sum(sol => sol.time);

		private TimeSpan RemainingTime => TimeSpan.FromSeconds(RemainingSecs);

		///
		public TestProgressTimeDisplayer() => Solutions = new List<(string sol, bool run, double time)>();

		/// <summary>
		/// adds an unrun test to the list
		/// </summary>
		public void TestToBeRun(string sol, double time)
		{
			var solution = Solutions.FirstOrDefault(s => s.sol.Equals(sol));
			if (!solution.sol.IsNullOrEmpty())
			{
				// if it was in the finished list, remove it
				Solutions.Remove(solution);
			}	// end if

			Solutions.Add((sol, false, time));

		}	// end TestToBeRun(string, double)

		/// <summary>
		/// marks the given solution has having been run and updates its time
		/// </summary>
		public void TestHasFinished(string sol, double time)
		{
			var solution = Solutions.FirstOrDefault(s => s.sol.Equals(sol));
			if (solution.sol.IsNullOrEmpty())
			{
				return;
			}	// end if

			Solutions.Remove(solution);
			Solutions.Add((sol, true, time));

		}	// end TestHasFinished(string, double)

	}	// end TestProgressTimeDisplayer

}	// end Hub.Commands.Helpers.UnitTest.Displayer

