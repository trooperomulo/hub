﻿using System.Collections.Generic;

using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Enum;

using CDU = Hub.Commands.Helpers.UnitTest.Displayer.ComparerDisplayerUtils;

namespace Hub.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// displays the results from a <see cref="TestCaseComparer" />
	/// </summary>
	public static class TccDisplayer
	{
		/// <summary>
		/// gets the lines for the comparison of one test case before/after
		/// </summary>
		/// <param name="comparer">the comparison</param>
		/// <param name="num">which number test this was</param>
		/// <param name="count">out of how many tests</param>
		/// <param name="solCompResult">the result of comparing the before and after</param>
		/// <returns>the lines to be displayed</returns>
		public static List<string> GetLines(TestCaseComparer comparer, int num, int count, ComparisonResult solCompResult)
		{
			var result = comparer.ComparisonResult;
			// the header
			var results = new List<string>
			{
				CDU.Line($"Test {num} of {count} - {CDU.Result(result)}", result),
				CDU.Blank()
			};

			// the test name, before results, and after results
			results.AddRange(CDU.Test(comparer.Test, comparer.Before?.Result, comparer.After?.Result));
			// the before and after messages and stack lists
			results.AddRange(GetRest(comparer.Before, comparer.After));
			// the result of comparing the before and after
			results.Add(CDU.Line("--------", solCompResult));

			return results;

		}	// end GetLines(TestCaseComparer, int, int, ComparisonResult)

		/// <summary>
		/// gets the messages and stack lists
		/// </summary>
		private static List<string> GetRest(TestCase before, TestCase after)
		{
			var rest = new List<string> {CDU.Blank()};
			if (GetMessageLists(before, after, out var bMsg, out var aMsg))
			{
				var cnt = bMsg.Count;
				for (var i = 0; i < cnt; i++)
				{
					rest.Add(CDU.Line(bMsg[i], aMsg[i]));
				}	// end for
			}	// end if

			if (GetStackLists(before, after, out var bStack, out var aStack))
			{
				if (rest.Count > 1)
				{
					rest.Add(CDU.Blank());
				}	// end if
				var cnt = bStack.Count;
				for (var i = 0; i < cnt; i++)
				{
					rest.Add(CDU.Line(bStack[i], aStack[i]));
				}	// end for
			}	// end if

			// if we don't have anything, return an empty list
			return rest.Count > 1 ? rest : new List<string>();

		}	// end GetRest(TestCase, TestCase)

		private static bool GetMessageLists(TestCase before, TestCase after, out List<string> bList,
			out List<string> aList)
		{
			bList = new List<string>();
			aList = new List<string>();

			// get the message from the before
			if (before != null && !before.Message.IsNullOrEmpty())
			{
				bList = CDU.Wrap(before.Message);
			}	// end if

			// get the message from the before
			if (after != null && !after.Message.IsNullOrEmpty())
			{
				aList = CDU.Wrap(after.Message);
			}	// end if

			// make sure there are as many after lines as before lines
			while (bList.Count > aList.Count)
			{
				aList.Add("");
			}	// end while

			// make sure there are as many before lines as after lines
			while (aList.Count > bList.Count)
			{
				bList.Add("");
			}	// end while

			// true if there was anything, and since both lists should have the same number
			// of lines, we just need to check the before list
			return bList.Count > 0;

		}	// end GetMessageLists(TestCase, TestCase, out List<string>, out List<string>)

		private static bool GetStackLists(TestCase before, TestCase after, out List<string> bList,
			out List<string> aList)
		{
			bList = new List<string>();
			aList = new List<string>();

			// get the before call stack
			if (before != null && before.CallStack.Count > 0)
			{
				foreach (var stack in before.CallStack)
				{
					// fill out each line
					bList.AddRange(CDU.Wrap(stack));
				}	// end foreach
			}	// end if

			// get the after call stack
			if (after != null && after.CallStack.Count > 0)
			{
				foreach (var stack in after.CallStack)
				{
					// fill out each line
					aList.AddRange(CDU.Wrap(stack));
				}	// end foreach
			}	// end if

			// make sure the after list is as long as the before list
			while (bList.Count > aList.Count)
			{
				aList.Add("");
			}	// end while

			// make sure the before list is as long as the after list
			while (aList.Count > bList.Count)
			{
				bList.Add("");
			}	// end while
			
			// true if there was anything, and since both lists should have the same number
			// of lines, we just need to check the before list
			return bList.Count > 0;

		}	// end GetStackLists(TestCase, TestCase, out List<string>, out List<string>)

	}	// end TccDisplayer

}	// end Hub.Commands.Helpers.Units.Displayer
