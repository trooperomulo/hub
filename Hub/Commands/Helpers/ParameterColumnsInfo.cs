﻿using System.Diagnostics.CodeAnalysis;

namespace Hub.Commands.Helpers
{
	/// <summary>
	/// information about the columns that parameters are print in
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class ParameterColumnsInfo
	{
		/// <summary>
		/// the length needed for the parameter column
		/// </summary>
		public int ParaLen { get; }

		/// <summary>
		/// the length needed for the current value column
		/// </summary>
		public int CValLen { get; }

		/// <summary>
		/// the length needed for the default value column
		/// </summary>
		public int DValLen { get; }

		/// <summary>
		/// do we need to wrap anything
		/// </summary>
		public bool NeedToWrap { get; }

		///
		public ParameterColumnsInfo(int paraLen, int cValLen, int dValLen, bool needToWrap)
		{
			ParaLen = paraLen;
			CValLen = cValLen;
			DValLen = dValLen;
			NeedToWrap = needToWrap;
		}
	}
}
