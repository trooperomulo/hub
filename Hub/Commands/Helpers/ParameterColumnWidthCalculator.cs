﻿using Hub.Interface;

namespace Hub.Commands.Helpers
{
	/// <summary>
	/// class used to determine the column widths when displaying all the parameters
	/// </summary>
	public static class ParameterColumnWidthCalculator
	{
		/// <summary>
		/// determines how wide each column should be, plus if there should be any wrapping
		/// </summary>
		/// <param name="consoleWidth">the width of the console</param>
		/// <param name="maxParaLen">the length of the longest parameter</param>
		/// <param name="maxCValLen">the length of the longest current value</param>
		/// <param name="maxDValLen">the length of the longest default value</param>
		/// <returns></returns>
		public static ParameterColumnsInfo GetColumnsInfo(int consoleWidth, int maxParaLen, int maxCValLen, int maxDValLen)
		{
			// there are 4 columns: parameter, set, current value, default value
			// we use a vertical bar for column lines
			// we want a space before all values and at least one space after so the values don't touch the bars

			var fullParaLen = maxParaLen + 2;
			var fullCValLen = maxCValLen + 2;
			var fullDValLen = maxDValLen + 2;

			var bars = 3; // for the 3 bars in between the 4 columns
			var set = 5;  // for the set column;
			
			// two twos for the buffer spaces in the default and current value columns
			if (consoleWidth >= (fullParaLen + fullCValLen + fullDValLen + bars + set))
			{
				// if the console is wide enough for everything, 
				return new ParameterColumnsInfo(fullParaLen, fullCValLen, fullDValLen, false);
			}



			/* At this point, either default is longer than half, current is longer than half, or both are.
			 * If just one is, then we just want to have that one wrap.  If both are, we'll need to wrap
			 * them both.  */

			var remaining = consoleWidth - fullParaLen - bars - set;   // this is how much space we have for the last two columns

			var half = remaining / 2;   // if remaining is odd, half will be rounded down (ex. 5 / 2 = 2)

			if (fullCValLen > half && fullDValLen > half)
			{
				return BothAreTooLong(fullParaLen, remaining, fullCValLen, fullDValLen);
			}

			return OneIsTooLong(fullParaLen, remaining, fullCValLen, fullDValLen);

		}	// end ParameterColumnsInfo GetColumnsInfo(int, int, int)

		private static ParameterColumnsInfo BothAreTooLong(int paraLen, int remaining, int fullCValLen, int fullDValLen)
		{
			var half = remaining / 2;
			var cValLen = half;
			var dValLen = half;

			// if remaining is odd, we'll give the extra character to the longer of default and current
			if (half + half < remaining)
			{
				if (fullCValLen < fullDValLen)
				{
					dValLen++;
				}
				else
				{
					cValLen++;
				}

			}

			return new ParameterColumnsInfo(paraLen, cValLen, dValLen, true);
		}

		private static ParameterColumnsInfo OneIsTooLong(int paraLen, int remaining, int fullCValLen, int fullDValLen)
		{
			// only one is too long
			// let the shorter one have the length it wants and give the longer one the rest of the width

			if (fullCValLen > fullDValLen)
			{
				return new ParameterColumnsInfo(paraLen, remaining - fullDValLen, fullDValLen, true);
			}

			return new ParameterColumnsInfo(paraLen, fullCValLen, remaining - fullCValLen, true);
		}
	}
	
}
