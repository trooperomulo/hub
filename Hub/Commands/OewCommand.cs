﻿using System.Collections.Generic;
using System.IO;
using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// Launches Oew
	/// </summary>
	public class OewCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Launches OEW";

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> { "o", "oew" };

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// oew/o
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action);
			Out.Explain("Launches the Online Estimate Writer tool", action, () => { });

		}	// end Help(List<string>)

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			Logger.Instance.LogOew();
			DoOew();

		}	// end Perform(List<string)

		#endregion Implementation of ICommand

		#region Method

		private static void DoOew()
		{
			Out.Line("Launching Online Estimate Writer");
			var path = Path.Combine(Env.Root, "xactimate.oew", "bin", "debug", "Xactimate online Estimate Writer.exe");
			IoCContainer.Instance.Resolve<IProgram>().RunProgram(path, wait: false);
		}

		#endregion Method

	}   // end OewCommand

}	// end Hub.Commands
