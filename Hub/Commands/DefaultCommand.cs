﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// command invoked for invalid input
	/// </summary>
	public class DefaultCommand : ICommand
	{
		#region Variable

		private readonly Action _hlp;

		#endregion Variable

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "";

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string>();

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(Args args)
		{
			var command = args.First();

			Out.Line($"'{command}' is not a command that I am familiar with.  Please try again.");
		}

		/// <inheritdoc />
		public void Perform(Args args)
		{
			var command = args.First();

			Out.Dash();
			Out.Blank();
			Out.Line($"You have entered an invalid command - {command}");
			Out.Blank();
			_hlp();
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public DefaultCommand(Action hlp) => _hlp = hlp;

		#endregion Constructor
	}
}
