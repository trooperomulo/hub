﻿using System;
using System.Collections.Generic;
using System.Text;

using Hub.Commands.Helpers;
using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// composes the email to send to BM
	/// </summary>
	public class EmailCommand : ICommand
	{
		#region Variables
		
		private const string DataDefault = "28";
		private const string Xm8Default = "main";

		private readonly bool _preferCurrent;
		private bool _nextGen;
		
		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Compose BM email";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"email", "e"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// email/e [-f folder] [-x xm8] [-dt data] [-p patch] [-ma major] [-mi minor] [-bu build] [-r revision]
			var action = Prep.Slash(Inputs.ToArray());
			var folder = Prep.Brackets($"{Flags._flagFold} folder");
			var xm8 = Prep.Brackets($"{Flags._flagXm81} xm8");
			var data = Prep.Brackets($"{Flags._flagData} data");
			var patch = Prep.Brackets($"{Flags._flagPatc} patch");
			var major = Prep.Brackets($"{Flags._flagMajr} major");
			var minor = Prep.Brackets($"{Flags._flagMinr} minor");
			var build = Prep.Brackets($"{Flags._flagBuil} build");
			var revision = Prep.Brackets($"{Flags._flagRevi} revision");
			var options = $"{folder} {xm8} {data} {patch} {major} {minor} {build} {revision}";
			Out.Example(action, options);
			Out.Explain("Creates the email to send to BM, uses the current branch for xm8", action, () =>
			{
				Out.Line(Prep.Describe(folder, "the folder to save the build inS, defaults to the current branch's name"));
				Out.Line(Prep.Describe(xm8, $"the xm8 branch to use, default is {Xm8Default}"));
				Out.Line(Prep.Describe(data, $"the data branch to use, default is {DataDefault}"));
				Out.Line(Prep.Describe(patch, "the previous build to create the patch off of, defaults to not included"));
				Out.Line(Prep.Describe(major, "the major version to set the build to, defaults to not included"));
				Out.Line(Prep.Describe(minor, "the minor version to set the build to, defaults to not included"));
				Out.Line(Prep.Describe(build, "the build version to set the build to, defaults to not included"));
				Out.Line(Prep.Describe(revision, "the revision version to set the build to, defaults to not included"));
				Out.Blank();
				Out.Line("*Notes:");
				Out.Indent("To force the default branch for any repo, specify \"default\"");
				Out.Indent($"Defaults: xm8 => {Xm8Default}, data => {DataDefault}", 2);
				Out.Indent("if any repo is not specified, we will automatically determine a branch to use based on the 4 possible situations");
				Out.Indent("1) We cannot determine any branches for the repo ==>> default for the repo");
				Out.Indent("2) The currently checked out branch matches the xm8 repo's branch ==>> use that branch");
				Out.Indent("3) The current is different from xm8, no branch on the repo matches xm8 ==>> use the current branch");
				Out.Indent("4) The current is different from xm8 but a branch does match xm8 ==>> _userParams.PreferCurrent ? current : xm8");
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			var git = IoCContainer.Instance.Resolve<IGit>();
			_nextGen = git.IsNextGen();
			var parser = new EmailCommandArgumentParser(_preferCurrent, git.GetBranchName(), _nextGen, args);
			// log
			Logger.Instance.LogEmail(parser.FolderSet, parser.Xm8Set, parser.DataSet, parser.PatchSet, parser.MajorSet,
				parser.MinorSet, parser.BuildSet, parser.RevisionSet);

			// move along
			DoEmail(parser.Folder, parser.Xm8, parser.Data, parser.Patch, parser.Major,
				parser.Minor, parser.Build, parser.Revision);
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public EmailCommand(bool preferCurrent) => _preferCurrent = preferCurrent;

		#endregion Constructor

		#region Methods
		
		private void DoEmail(string fo, string xm, string da, string pa, int ma, int mi, int bu, int re)
		{
			try
			{
				IoCContainer.Instance.Resolve<IEmail>().DisplayEmail("BuildMaster5@xactware.com", GetSubject(), GetEmailBody(fo, xm, da, pa, ma, mi, bu, re));
			}
			catch(Exception ex)
			{
				Out.Error($"Unable to create email - {ex.Message}");
			}
		}

		private static string GetEmailBody(string folder, string xm8, string data, string patch, int major, int minor, int build, int revision)
		{
			/* build_name=XMW-1988-rdt
			 * git_xm8_branch=vib/XMW-1988-rdt-rc
			 * git_xm8Data_branch=28
			 * **Only Include the Following Line If Patches Are Desired**
			 * bm2ini_textBoxPreviousProgram=Q:\XM8_Builds\Vibranium\Acceptance\Beta 1.1\29.0 (0.0)
			 * **Only include the following lines if a specific version is desired. Leaving a field blank will result in the default value being supplied.**
			 * bm2ini_numericUpDownMajor=30 (max is 255)
			 * bm2ini_numericUpDownMinor=100 (max is 255)
			 * bm2ini_numericUpDownBuild=85 (max is 65535)
			 * bm2ini_numericUpDownRevision=40 (max is 65535) 
			 */

			var builder = new StringBuilder();

			builder.AppendLine(Prep.Equal("build_name", folder));
			builder.AppendLine(Prep.Equal("git_xm8_branch", xm8));
			builder.AppendLine(Prep.Equal("git_xm8Data_branch", data));
			if (!patch.IsNullOrEmpty())
			{
				builder.AppendLine(Prep.Equal("bm2ini_textBoxPreviousProgram", patch));
			}
			if (major > -1 && major < 256)
			{
				builder.AppendLine(Prep.Equal("bm2ini_numericUpDownMajor", major));
			}
			if (minor > -1 && minor < 256)
			{
				builder.AppendLine(Prep.Equal("bm2ini_numericUpDownMinor", minor));
			}
			if (build > -1 && build < 65536)
			{
				builder.AppendLine(Prep.Equal("bm2ini_numericUpDownBuild", build));
			}
			if (revision > -1 && revision < 65536)
			{
				builder.AppendLine(Prep.Equal("bm2ini_numericUpDownRevision", revision));
			}

			return builder.ToString();
		}
		
		private string GetSubject() => $"Build {(_nextGen ? "NextGen" : "Vibranium")}";

		#endregion Methods
	}
}
