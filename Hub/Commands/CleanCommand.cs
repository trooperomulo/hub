﻿using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// performs a clean build
	/// </summary>
	public class CleanCommand : ICommand
	{
		// add the ability to call git clean
		// default to bat, xm8 and c:/nuget
		// add the ability to select which of the four happen
		// the git clean watches the output and reruns until there's a run without any output
		// num only applies to bat, since xm8 repeat and nuget only need happen once

		#region Constant

		private const int Def = 2;

		#endregion Constant

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Clean";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"clean", "c"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// clean/c [num -b -x -n -q]
			var action = Prep.Slash(Inputs.ToArray());
			const string num = "num";
			//var batch = Flags._flagBatc;
			//var xm8 = Flags._flagXm81;
			var quiet = Flags._flagQiet;
			var options = Prep.Brackets(num/*, batch, xm8*/, quiet);
			Out.Example(action, options);
			Out.Explain("Used to clean up", action, () =>
			{
				Out.Line(Prep.Describe(options, ""));
				Out.Indent(Prep.Describe(num, "number of times to call the clean batch file"));
				Out.Indent($"defaults to {Def}; must be between 1 and 10, inclusive", 2);
				Out.Indent("If you want it to run more than 10 times, call this again", 2);
				Out.Indent(Prep.Describe(quiet, "suppress the output"));
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			// determine if this should be quiet
			var quiet = Utils.HasArg(args, Flags._flagQiet);

			// determine how many times we should loop
			var num = Def;
			if (args.Count > 0)
			{
				var val = args[0];
				if (!int.TryParse(val, out num) || num < 1 || num > 10)
				{
					num = Def;
				}
			}

			Logger.Instance.LogClean(quiet, num, num == Def);

			DoClean(quiet, num);
		}

		#endregion Implementation of ICommand

		#region Method

		private static void DoClean(bool quiet, int num)
		{
			Out.Line("calling clean.bat");
			Out.Blank();
			for (var i = 0; i < num; i++)
			{
				if (!quiet)
				{
					if (i > 0)
					{
						Out.Blank();
					}

					Out.Line($"clean run #{i + 1} of {num}");
				}
				IoCContainer.Instance.Resolve<IProgram>().RunBatchFile("clean.bat", rso: quiet, rse: quiet);
			}

			Out.Blank();
			Out.Line("clean.bat finished");
		}

		#endregion Method
	}
}
