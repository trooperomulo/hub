﻿using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// navigates to a location
	/// </summary>
	public class GoCommand : ICommand
	{
		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Navigate to";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"go", "g"};

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// go/g [solutions]
			var action = Prep.Slash(Inputs.ToArray());
			Out.Example(action, Hub.Solutions);
			Out.Explain("Navigate to a solution", action, () => { Out.Line(Prep.Describe(Hub.Solutions, "will be run in FIFO order; really only the last one matters")); });
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			var lastGoodOne = "";
			foreach (var arg in args)
			{
				if (Shortcuts.Instance.HasShortcut(arg))
				{
					lastGoodOne = arg;
				}
				else
				{
					Out.NotAShortcut(arg);
				}
			}

			Logger.Instance.LogGo();

			// to save time, we'll only go to the last valid shortcut in the list
			DoGo(lastGoodOne, false);
		}

		#endregion Implementation of ICommand

		#region Method

		///
		public static void DoGo(string solution) => DoGo(solution, true);

		///
		public static void DoGo(string solution, bool silent)
		{
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			var path = Shortcuts.Instance.GetPath(solution);
			var location = solution.IsNullOrEmpty()
				? $"{Env.Local}{Env.Version}"
				: (dirs.Exists(path) ? path : $"{Env.Local}{Env.Version}{path}");
			if (location.Contains("%"))
			{
				location = location.Replace("%mode%", Env.Mode);
			}
			dirs.SetCurrentDirectory(location);
			if (!silent)
			{
				Out.Line(location);
			}
		}

		#endregion Method
	}
}
