﻿using System;
using System.IO;

using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// builds a solution
	/// </summary>
	public class BuildCommand : ICommand
	{
		#region Variables

		private static bool _shouldSort;

		private static Action _xm8, _trk;
		private static Action<string> _go, _srt;
		private static Action<Args> _unt;

		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Build the solution";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public Args Inputs { get; } = new Args {"build", "b"};

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(Args args)
		{
			// build/b [-p -d -x/l -tr/dm -u/t] xmpcc xuicc
			var action = Prep.Slash(Inputs.ToArray());
			var options = Prep.Brackets($"{Flags._flagProd} {Flags._flagDbug} {Flags._flagXm8_} {Flags._flagTrck} {Flags._flagUnit}");
			Out.Example(action, options, Hub.Solutions);
			Out.Explain("Used to build one or multiple solutions", action, () =>
			{
				Out.Line(options);
				Out.Indent(Prep.Describe(Flags._flagProd, "build in prod mode"));
				Out.Indent(Prep.Describe(Flags._flagDbug, "build in debug mode"));
				Out.Indent("if neither p or d is specified, build will be in the mode preset in the prompt", 2);
				Out.Indent(Prep.Describe(Flags._flagXm8_, "launch Xactimate after building"));
				Out.Indent(Prep.Describe(Flags._flagTrck, "launch the trickler after building"));
				Out.Indent(Prep.Describe(Flags._flagUnit, "run unit tests after building"));
				Out.Blank();
				Out.Line(Prep.Describe(Hub.Solutions, "will be run in FIFO order; be aware of build order"));
				Out.Indent("a full build will be run in no solutions are specified", 2);
			});
		}

		/// <inheritdoc />
		public void Perform(Args args)
		{
			var prod = Utils.ReadFirstArg(args, Flags._flagProd);
			var debug = Utils.ReadFirstArg(args, Flags._flagDbug);
			var xm8 = Utils.IsFirstXm8(args);
			var trk = Utils.IsFirstTrk(args);
			var unt = Utils.IsFirstUnt(args);

			Logger.Instance.LogBuild(prod, debug, xm8, trk, unt, args.Count);

			void Action()
			{
				DoBuild(xm8, trk, unt, args, prod || debug);
			}

			if (!prod && !debug)
			{
				Action();
				return;
			}

			if (prod)
			{
				ChangeToProd(Action);
			}

			if (debug)
			{
				ChangeToDebug(Action);
			}
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public BuildCommand(bool shouldSort, Action xm8, Action trk, Action<Args> unt, Action<string> go,
			Action<string> srt)
		{
			_shouldSort = shouldSort;

			_xm8 = xm8;
			_trk = trk;
			_unt = unt;
			_go = go;
			_srt = srt;
		}

		#endregion Constructor

		#region Methods

		///
		public static void DoBuild(bool xm8, bool tric, bool test, Args solutions, bool printMode)
		{
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			var current = dirs.GetCurrentDirectory();

			var somethingBuilt = false;

			if (solutions.Count < 1)
			{
				Out.Line(printMode ? $"Building all {Env.Mode}" : "Building all");
				if (!DoBuildAll())
				{
					dirs.SetCurrentDirectory(current);
					return;
				}
				somethingBuilt = true;
			}
			else
			{
				foreach (var solution in solutions)
				{
					if (!Shortcuts.Instance.HasShortcut(solution))
					{
						Out.NotAShortcut(solution);
						continue;
					}
					if (!BuildSolution(solution, printMode, false))
					{
						dirs.SetCurrentDirectory(current);
						return;
					}
					somethingBuilt = true;
				}
			}

			CopyIniFilesIfNecessary();

			dirs.SetCurrentDirectory(current);

			if (!somethingBuilt)
			{
				return;
			}

			if (xm8)
			{
				_xm8();
			}

			if (tric)
			{
				_trk();
			}

			if (test)
			{
				_unt(solutions);
			}
		}

		private static bool DoBuildAll()
		{
			var build = $@"{Env.Xm8}\build\tools\bin\build.bat";
			var result = IoCContainer.Instance.Resolve<IProgram>().RunBatchFile(build, rso: false);

			IoCContainer.Instance.Resolve<IConsole>().Beep();

			return result;
		}

		private static void ChangeMode(string mode, Action action)
		{
			var lastMode = Environment.GetEnvironmentVariable("mode");
			Env.SetEnvironmentVariable("mode", mode, true);
			action();
			Env.SetEnvironmentVariable("mode", lastMode, true);
		}

		private static void ChangeToDebug(Action action) => ChangeMode("debug", action);

		private static void ChangeToProd(Action action) => ChangeMode("prod", action);

		///
		public static bool BuildSolution(string solution, bool printMode, bool fromResx)
		{
			Out.Write(printMode ? $"Building {solution} {Env.Mode}" : $"Building {solution}");
			_go(solution);
			bool success = BuildSolution();
			if (success)
			{
				success = BuildUnitTests(solution);
			}
			Out.Line(success ? " - Success" : $"{solution} failed");
			if (ShouldSortAfterBuild(fromResx, success, _shouldSort, solution))
			{
				_srt(solution);
			}
			return success;
		}

		private static bool BuildSolution()
		{
			var newBuildFile = $@"{Env.Xm8}\build\tools\bin\build.bat";
			return IoCContainer.Instance.Resolve<IProgram>().RunBatchFile($"{newBuildFile}", ShowOutput.Errors);
		}

		private static bool BuildUnitTests(string solution)
		{
			var ut = solution + "ut";
			if (!Shortcuts.Instance.HasShortcut(ut))
			{
				return true;
			}

			Out.Write(" and tests");
			_go(ut);
			return BuildSolution();
		}

		private static void CopyIniFiles(IFiles files)
		{
			var dirs = IoCContainer.Instance.Resolve<IDirectories>();
			var dir = @"c:\Program Files\Xactware\XactimateDesktop\CORE";
			if (!dirs.Exists(dir))
			{
				return;
			}
			var inis = dirs.GetFiles(dir, "*.ini");
			if (inis.Count < 1)
			{
				return;
			}
			if (!dirs.Exists(Env.BinFolder))
			{
				dirs.CreateDirectory(Env.BinFolder);
			}
			foreach (var ini in inis)
			{
				var name = Path.GetFileName(ini);
				files.Copy(ini, Env.GetBinFilename(name));
			}
		}

		private static void CopyIniFilesIfNecessary()
		{
			var first = Env.GetBinFilename("firsttime.ini");
			var files = IoCContainer.Instance.Resolve<IFiles>();
			if (!files.Exists(first))
			{
				CopyIniFiles(files);
			}
		}

		///
		public static bool ShouldSortAfterBuild(bool fromResx, bool success, bool shouldSort, string solution)
		{
			if (fromResx)
			{
				// the build was called by a resx command so it was already sorted
				return false;
			}

			if (!success)
			{
				// the build was unsuccessful so we don't want to sort it
				return false;
			}

			if (!shouldSort)
			{
				// the user doesn't want to sort resx files after building
				return false;
			}

			if (!Shortcuts.Instance.GetPath(solution).ToLower().EndsWith("res"))
			{
				// this isn't a resource solution so there isn't anything to sort anyway
				return false;
			}

			return true;

		}

		#endregion Methods
	}
}
