﻿using System;
using System.Collections.Generic;

using Hub.Interface;
using Hub.Logging;

namespace Hub.Commands
{
	/// <summary>
	/// Open Xactimate
	/// </summary>
	public class LaunchCommand : ICommand
	{
		#region Variables

		private readonly Action<bool, bool, bool, List<string>, bool> _bld;
		private readonly Action _trk;

		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Launch Xactimate";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"launch", "l", "x"};

		/// <inheritdoc />
		public bool NeedShortcut => true;

		/// <inheritdoc />
		public void Help(List<string> args)
		{
			// launch/l/x [-u/t -tr/dm] [solutions]
			var action = Prep.Slash(Inputs.ToArray());
			var options = Prep.Brackets($"{Flags._flagTrck} {Flags._flagUnit}");
			Out.Example(action, options, Hub.Solutions);
			Out.Explain("Launches Xactimate after building any specified solutions", action, () =>
			{
				Out.Line(options);
				Out.Indent(Prep.Describe(Flags._flagTrck, "also launch the trickler"));
				Out.Indent(Prep.Describe(Flags._flagUnit, "run units tests, will be ignored if no solutions are specified"));
				Out.Line(Prep.Describe(Hub.Solutions, "will be run in FIFO order; be aware of build order"));
				Out.Indent("nothing will be built if no solutions are specified, i.e., no buildall", 2);
			});
		}

		/// <inheritdoc />
		public void Perform(List<string> args)
		{
			var tric = Utils.IsFirstTrk(args);
			var test = Utils.IsFirstUnt(args);
			
			Logger.Instance.LogLaunch(tric, test, args.Count);

			if (args.Count < 1)
			{
				DoLaunch();
				if (tric)
				{
					_trk();
				}
				return;
			}

			_bld(true, tric, test, args, false);
		}

		#endregion Implementation of ICommand

		#region Constructor

		///
		public LaunchCommand(Action<bool, bool, bool, List<string>, bool> bld, Action trk)
		{
			_bld = bld;
			_trk = trk;
		}

		#endregion Constructor

		#region Method

		///
		public static void DoLaunch() => IoCContainer.Instance.Resolve<IProgram>().LaunchBinProgram("x.exe", "Xactimate", wait: false);

		#endregion Method
	}
}
