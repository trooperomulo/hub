﻿using System.Collections.Generic;

using Hub.Decider.Command;
using Hub.Interface;
using Hub.Logging;

using Args = System.Collections.Generic.List<string>;

namespace Hub.Commands
{
	/// <summary>
	/// explains the commands
	/// </summary>
	public class HelpCommand : ICommand
	{
		#region Variables

		private static List<string> _subs;
		private static List<string> _nonSubs;

		#endregion Variables

		#region Implementation of ICommand

		/// <inheritdoc />
		public string Description => "Displays help";

		/// <inheritdoc />
		public bool HasSubCommands => false;

		/// <inheritdoc />
		public List<string> Inputs { get; } = new List<string> {"?", "help", "h"};

		/// <inheritdoc />
		public bool NeedShortcut => false;

		/// <inheritdoc />
		public void Help(Args args)
		{
			// help/h [commands]
			var action = Prep.Slash(Inputs.ToArray());
			var commands = Prep.Brackets("commands");
			Out.Example(action, commands);
			Out.Explain("Display more detailed help for one or more commands", action,
				() => { Out.Line(Prep.Describe(commands, "command(s) to be detailed, in FIFO order")); });

		}	// end Help(Args)

		/// <inheritdoc />
		public void Perform(Args args)
		{
			Logger.Instance.LogHelp(args?.Count ?? 0);
			ShowHelp(args);

		}	// end Perform(Args)

		#endregion Implementation of ICommand

		#region Constructor

		///
		public HelpCommand(List<string> subs, List<string> nonSubs)
		{
			_subs = subs;
			_nonSubs = nonSubs;

			if (!_nonSubs?.Contains(Inputs[0]) ?? false)
			{
				_nonSubs.AddRange(Inputs);
			}	// end if

		}	// end HelpCommand(List<string>, List<string>)

		#endregion Constructor

		#region Methods

		/// <summary>
		/// Goes through all the arguments passed in to the program, and groups them by command.
		/// Commands will sub commands will have their sub command's input grouped with them.
		/// </summary>
		public List<Args> GetHelpCommandsWithArgs(Args commands)
		{
			var result = new List<Args>();

			// loop through all the arguments
			for (var i = 0; i < commands.Count; i++)
			{
				var arg = commands[i];
				if (_subs.Contains(arg))
				{
					// if the input is for a command with sub commands
					var sub = new Args {arg};
					i++;

					// continue walking through the arguments
					for (; i < commands.Count; i++)
					{
						arg = commands[i];
						if (!_subs.Contains(arg) && !_nonSubs.Contains(arg))
						{
							// if the argument is not a valid input for any top level commands,
							// we'll assume it is for the current command
							sub.Add(arg);
						}	// end if
						else
						{
							// if it is for a top level command, back up and continue the outer loop
							i--;
							break;
						}	// end else
					}	// inner for

					// we're done processing this group; add it to the results
					result.Add(sub);

				}	// end if
				else // this handles both valid input for non sub commands, and invalid inputs
				{
					result.Add(new Args {arg});
				}	// end else
			}	// end outer for

			return result;
		}	// end GetHelpCommandsWithArgs(Args)

		/// <summary>
		/// displays the helps for all of the individual commands
		/// </summary>
		public static void ShowHelp()
		{
			Out.Dash();
			Out.Blank();
			Out.Line("Please use one of the following commands:");
			Out.Blank();
			var commands = CommandDecider.Commands;
			foreach (var command in commands)
			{
				Out.Command(command);
			}	// end foreach
			Out.Dash();

		}	// end ShowHelp()
		
		private void ShowHelp(Args commands)
		{
			if (commands == null || commands.Count == 0)
			{
				ShowHelp();
				return;
			}	// end ShowHelp(Args)
			
			var groups = GetHelpCommandsWithArgs(commands);
			foreach (var group in groups)
			{
				CommandHelpDecider.Instance.Help(group);
			}	// end foreach

		}	// end ShowHelp(Args)

		#endregion Methods

	}	// end HelpCommand

}	// end Hub.Commands
