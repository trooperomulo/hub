﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using Hub.Enum;
using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// class that handles running an external program
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class Program : IProgram
	{
		/// <inheritdoc />
		public ProcessStartInfo GetStartInfo(string path, string arguments, bool rso = true, bool rse = false)
		{
			return new ProcessStartInfo(path, arguments ?? "")
			{
				CreateNoWindow = false,
				UseShellExecute = false,
				RedirectStandardOutput = rso,
				RedirectStandardError = rse
			};
		}

		/// <inheritdoc />
		public void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true, ShowOutput show = ShowOutput.No)
		{
			var file = Env.GetBinFilename(filename);
			if (!File.Exists(file))
			{
				Out.Line($"{filename} needs to be built");
				return;
			}
			Out.Line($"Launching {label}");
			RunProgram(file, arguments, wait: wait, show: show);
		}

		/// <inheritdoc />
		public bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true, bool rse = false,
			bool wait = true, DataReceivedEventHandler outputHandler = null)
		{
			return RunProgram("cmd.exe", $"/c {fileAndArgs}", show, rso, rse, wait, outputHandler);
		}

		/// <inheritdoc />
		public bool RunProgram(string path, string arguments = null, ShowOutput show = ShowOutput.No, bool rso = true,
			bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
		{
			var output = "";
			var error = "";

			var hasHandler = outputHandler != null;

			var startInfo = GetStartInfo(path, arguments, rso || hasHandler, rse);

			using (var process = new Process())
			{
				process.StartInfo = startInfo;
				if (hasHandler)
				{
					process.OutputDataReceived += outputHandler;
				}
				process.Start();
				if (wait || hasHandler)
				{
					if (hasHandler)
					{
						process.BeginOutputReadLine();
					}
					if (rso && !hasHandler)
						output = Environment.NewLine + process.StandardOutput.ReadToEnd().TrimEnd();
					if (rse)
						error = Environment.NewLine + process.StandardError.ReadToEnd().TrimEnd();
					process.WaitForExit();
				}
			}

			var hasErrors = output.Contains("error") || !error.IsNullOrEmpty();

			if (show == ShowOutput.Yes)
			{
				if (!output.IsNullOrEmpty())
				{
					Out.Line(output);
				}

				if (!error.IsNullOrEmpty())
				{
					Out.Error(error);
				}
			}
			else if (show == ShowOutput.Errors && hasErrors)
			{
				if (output.Contains("error"))
				{
					Out.Error(output);
				}

				if (!error.IsNullOrEmpty())
				{
					Out.Error(error);
				}
			}

			return !hasErrors;
		}

		/// <inheritdoc />
		public void KillProcess(string name)
		{
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				process.Kill();
			}
		}

		/// <inheritdoc />
		public void StartProcess(string filename) => Process.Start(filename);
	}
}
