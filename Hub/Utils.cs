﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Enum;
using Hub.Interface;

using Args = System.Collections.Generic.List<string>;

namespace Hub
{
	///
	public static class Utils
	{
		#region Properties

		///
		public static string DataFolder { private get; set; }

		#endregion Properties

		#region Extension Methods

		// Dictionary<T1, T2>

		///
		public static void Add<T1, T2>(this Dictionary<T1, List<T2>> dict, T1 key, T2 val)
		{
			if (dict.ContainsKey(key))
			{
				dict[key].Add(val);
			}
			else
			{
				dict.Add(key, new List<T2> { val });
			}
		}

		/// <summary>
		/// updates the value of the given key, if it is already in the dictionary
		/// </summary>
		public static void AddIfExists<T1, T2>(this Dictionary<T1, List<T2>> dict, T1 key, T2 val)
		{
			if (dict.ContainsKey(key))
			{
				dict[key].Add(val);
			}
		}

		/// <summary>
		/// updates the value of the given key, if it is already in the dictionary
		/// </summary>
		public static void Update<T1, T2>(this Dictionary<T1, T2> dict, T1 key, T2 val)
		{
			if (dict.ContainsKey(key))
			{
				dict[key] = val;
			}
		}

		#endregion Extension Methods

		#region Methods

		/// <summary>
		/// returns if the given branch is a next gen branch
		/// </summary>
		public static bool BranchIsNextGen(string branch) => branch.ToLower().StartsWith("nextgen");

		/// <summary>
		/// returns a string of the given length with the given text centered
		/// </summary>
		/// <param name="text">the text to be centered</param>
		/// <param name="totalLen">the length of the returned string</param>
		public static string Center(string text, int totalLen)
		{
			var len = text.Length;
			if (len > totalLen)
			{
				throw new ArgumentException($"Passed in string is longer ({len}) than desired length ({totalLen})");
			}

			var diff = totalLen - len;
			var front = diff / 2;
			var back = diff - front;

			return $"{string.Concat(Enumerable.Repeat(" ", front))}{text}{string.Concat(Enumerable.Repeat(" ", back))}";
		}

		///
		public static string CombineArgs(Args args)
		{
			if (args == null || args.Count < 1)
				return "";

			var str = args.Aggregate("", (current, line) => current + " '" + line + "'");
			return str.Substring(1);
		}

		private static string GetLocationFromRepo(Repository repo)
		{
			switch (repo)
			{
				case Repository.Xm8:
					return Env.Branch;
				case Repository.Data:
					return DataFolder;
				default:
					throw new ArgumentOutOfRangeException(nameof(repo), repo, null);
			}
		}

		/// <summary>
		/// is the given argument any one of the passed in arguments, removing it if it is
		/// </summary>
		/// <param name="args">the arguments passed in from the console</param>
		/// <param name="arg">the argument we are looking for</param>
		/// <returns>true if the arg is in the list, false otherwise</returns>
		public static bool HasArg(Args args, string arg)
		{
			if (arg.IsNullOrEmpty() || args == null || args.Count < 1 || !args.Contains(arg))
			{
				return false;
			}

			args.Remove(arg);
			return true;
		}

		private static bool HasArg(Args args, string arg1, string arg2) => HasArg(args, arg1) || HasArg(args, arg2);
		
		///
		public static bool HasAndArg(Args args) => HasArg(args, Flags._flagAndr);

		///
		public static bool HasIosArg(Args args) => HasArg(args, Flags._flagIos_);

		///
		public static bool HasTrkArg(Args args) => HasArg(args, Flags._flagTrk1, Flags._flagTrk2);

		///
		public static bool HasXm8Arg(Args args) => HasArg(args, Flags._flagXm81, Flags._flagXm82);

		///
		public static bool HasWinArg(Args args) => HasArg(args, Flags._flagWind);

		///
		public static bool IsFirstTrk(Args args) => ReadFirstArg(args, Flags._flagTrk1, Flags._flagTrk2);

		///
		public static bool IsFirstUnt(Args args) => ReadFirstArg(args, Flags._flagUnt1, Flags._flagUnt2);

		///
		public static bool IsFirstXm8(Args args) => ReadFirstArg(args, Flags._flagXm81, Flags._flagXm82);

		///
		public static void NavigateToRepo(Repository repo)
		{
			// we need to go to the specified repo, which will be at the same level as Version
			var location = $@"{Env.Local}{Env.Version}\..\{GetLocationFromRepo(repo)}";
			IoCContainer.Instance.Resolve<IDirectories>().SetCurrentDirectory(location);
		}

		///
		public static bool ReadFirstArg(Args args, string arg)
		{
			if (arg.IsNullOrEmpty() || args == null || args.Count < 1)
			{
				return false;
			}

			var result = arg.Equals(args[0]);
			if (result)
			{
				args.RemoveAt(0);
			}
			return result;
		}

		///
		public static bool ReadFirstArg(Args args, string arg1, string arg2) => ReadFirstArg(args, arg1) || ReadFirstArg(args, arg2);

		///
		public static bool ReadFirstArg(Args args, string arg, out string result)
		{
			result = "";
			if (arg.IsNullOrEmpty() || args == null || args.Count < 2)
			{
				return false;
			}

			if (arg.Equals(args[0]))
			{
				result = args[1];
				args.RemoveAt(0);
				args.RemoveAt(0);
			}

			return !result.IsNullOrEmpty();
		}

		#endregion Methods
	}
}
