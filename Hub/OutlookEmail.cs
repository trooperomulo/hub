﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

using Hub.Interface;

using Outlook = Microsoft.Office.Interop.Outlook;

namespace Hub
{
	/// <summary>
	/// class for dealing with email via outlook
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class OutlookEmail : IEmail
	{
		#region Implementation of IEmail

		/// <inheritdoc />
		public void DisplayEmail(string to, string subject, string body)
		{
			var outlook = GetOutlook();
			if (!(outlook.CreateItem(Outlook.OlItemType.olMailItem) is Outlook.MailItem mail))
			{

				Out.Line("Unable to create email");
				return;
			}
			mail.Subject = subject;
			mail.To = to;
			mail.Body = body;
			mail.Display();
		}

		#endregion Implementation of IEmail

		#region Helper

		private Outlook.Application GetOutlook()
		{
			return Marshal.GetActiveObject("Outlook.Application") as Outlook.Application ?? new Outlook.Application();
		}

		#endregion Helper
	}
}
