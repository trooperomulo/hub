﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// class that outputs info to the user
	/// </summary>
	public static class Out
	{
		///
		public const string DashLine = "-------------------------------------------------------------";

		/// <summary>
		/// writes a blank line
		/// </summary>
		public static void Blank() => IoCContainer.Instance.Resolve<IConsole>().WriteLine();

		/// <summary>
		/// describes a command and the inputs to invoke it
		/// </summary>
		/// <param name="command">the command we are describing</param>
		public static void Command(ICommand command)
		{
			if (command == null)
			{
				return;
			}

			// currently the longest command description is 27 chars long, we want to add enough tabs to make the description 32 chars long
			var output = command.Description;
			var diff = 32 - output.Length;
			var tabs = (diff - 1) / 8;
			for (var i = 0; i < tabs; i++)
			{
				output = $"{output}\t";
			}

			foreach (var input in command.Inputs)
			{
				output = $"{output}\t{input}";
			}

			Line(output);
		}

		/// <summary>
		/// writes a dash line
		/// </summary>
		public static void Dash() => Line(DashLine);

		/// <summary>
		/// displays an error
		/// </summary>
		public static void Error(string error)
		{
			var cons = IoCContainer.Instance.Resolve<IConsole>();
			var current = cons.ForegroundColor;
			cons.ForegroundColor = GetErrorColor(cons.BackgroundColor);
			Line(error);
			cons.ForegroundColor = current;
		}

		/// <summary>
		/// displays an example usage of a command
		/// </summary>
		/// <param name="parts">the different pieces of the example</param>
		public static void Example(params string[] parts)
		{
			if (parts == null || parts.Length < 1)
			{
				return;
			}

			Blank();
			Line(parts.Aggregate("", (current, part) => current + " " + part).Substring(1));
		}

		/// <summary>
		/// explains the parts of a command
		/// </summary>
		/// <param name="title">a general description of the command</param>
		/// <param name="action">the input to invoke the command</param>
		/// <param name="explanation">a detailed description of each part of the command</param>
		public static void Explain(string title, string action, Action explanation)
		{
			Dash();
			Line(title);
			Blank();
			Line(action);
			Blank();
			explanation();
			Dash();
		}

		/// <summary>
		/// get the appropriate error color for the current background
		/// </summary>
		public static ConsoleColor GetErrorColor(ConsoleColor bgColor)
		{
			var color = ConsoleColor.Red;
			// ReSharper disable once SwitchStatementMissingSomeCases
			// we can't actually get any other values so default: isn't useful
			switch (bgColor)
			{
				case ConsoleColor.Black:
				case ConsoleColor.DarkBlue:
				case ConsoleColor.DarkGreen:
				case ConsoleColor.DarkCyan:
				case ConsoleColor.DarkRed:
				case ConsoleColor.DarkMagenta:
				case ConsoleColor.DarkYellow:
				case ConsoleColor.Gray:
				case ConsoleColor.DarkGray:
				case ConsoleColor.Blue:
				case ConsoleColor.Green:
				case ConsoleColor.Cyan:
				case ConsoleColor.Yellow:
				case ConsoleColor.White:
					color = ConsoleColor.Red;
					break;
				case ConsoleColor.Red:
				case ConsoleColor.Magenta:
					color = ConsoleColor.Blue;
					break;
			}

			return color;
		}

		/// <summary>
		/// prints the given line with an indentation
		/// </summary>
		/// <param name="line">the line to be printed</param>
		/// <param name="num">how many indentations to add</param>
		public static void Indent(string line, int num = 1)
		{
			var indents = "";
			for (var i = 0; i < num; i++)
			{
				indents += "\t";
			}
			// typically indent called with 2 is being printed under a line that is indented
			// once, and then has "- " appended to the front
			if (num == 2)
			{
				line = "  " + line;
			}

			Line(indents + line);
		}

		/// <summary>
		/// prints the given line
		/// </summary>
		public static void Line(string line) => IoCContainer.Instance.Resolve<IConsole>().WriteLine(line);

		/// <summary>
		/// this guy can be really useful for showing the args that were passed in
		/// </summary>
		public static void Line(List<string> lines) => Line(Utils.CombineArgs(lines));

		/// <summary>
		/// displays an option in a letter list
		/// </summary>
		/// <param name="ch">what character this option is</param>
		/// <param name="option">the option</param>
		public static void Option(char ch, string option) => Option($"{ch}", option);

		/// <summary>
		/// displays an option in a numbered list
		/// </summary>
		/// <param name="num">what number this option is</param>
		/// <param name="option">the option</param>
		public static void Option(int num, string option) => Option($"{num}", option);

		private static void Option(string st, string option) => Line($"  {st}\t{option}");
		
		/// <summary>
		/// displays a selectable option in a letter list
		/// </summary>
		/// <param name="ch">what character this option is</param>
		/// <param name="option">the option</param>
		/// <param name="sel">is it selected</param>
		public static void Option(char ch, string option, bool sel) => Option(ch.ToString(), option, sel);

		/// <summary>
		/// displays a selectable option in a number list
		/// </summary>
		/// <param name="num">what number this option is</param>
		/// <param name="option">the option</param>
		/// <param name="sel">is it selected</param>
		public static void Option(int num, string option, bool sel) => Option(num.ToString(), option, sel);
		
		/// <summary>
		/// displays a selectable option
		/// </summary>
		/// <param name="chr">the character to select this option</param>
		/// <param name="option">the option</param>
		/// <param name="sel">is it selected</param>
		public static void Option(string chr, string option, bool sel) => Line($"  {chr} [{(sel ? "X" : " ")}]\t{option}");

		/// <summary>
		/// displays a message that the given shortcut isn't defined
		/// </summary>
		public static void NotAShortcut(string solution) => Line($"{solution} was not found in the shortcuts");

		/// <summary>
		/// displays an unadded pair and why it wasn't added
		/// </summary>
		/// <param name="pair">the tuple pair</param>
		/// <param name="dupKey">was the key duplicated</param>
		/// <param name="dupVal">was the value duplicated</param>
		public static void UnaddedPair((string key, string value) pair, bool dupKey, bool dupVal)
		{
			var reason = dupKey
				? (dupVal ? "duplicate key and value" : "duplicate key")
				: (dupVal ? "duplicate value" : "I'm not sure why this wasn't added");

			Line($"{pair.key.ToUpper()}: {pair.value} => {reason}");
		}

		/// <summary>
		/// write the given text (no new line is added at the end)
		/// </summary>
		public static void Write(string text) => IoCContainer.Instance.Resolve<IConsole>().Write(text);
	}
}
