﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace Hub
{
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public class Cons : IConsole
	{
		#region Implementation of IConsole

		/// <inheritdoc />
		public ConsoleColor BackgroundColor
		{
			get => Console.BackgroundColor;
			set => Console.BackgroundColor = value;
		}

		/// <inheritdoc />
		public int CursorTop
		{
			get => Console.CursorTop;
			set => Console.CursorTop = value;
		}

		/// <inheritdoc />
		public ConsoleColor ForegroundColor
		{
			get => Console.ForegroundColor;
			set => Console.ForegroundColor = value;
		}

		/// <inheritdoc />
		public bool KeyAvailable => Console.KeyAvailable;

		/// <inheritdoc />
		public string Title
		{
			get => Console.Title;
			set => Console.Title = value;
		}

		/// <inheritdoc />
		public int Width => Console.WindowWidth;

		/// <inheritdoc />
		public void Beep() => Console.Beep();

		/// <inheritdoc />
		public char ReadKey() => Console.ReadKey().KeyChar;

		/// <inheritdoc />
		public void SetCursorPosition(int left, int top) => Console.SetCursorPosition(left, top);

		/// <inheritdoc />
		public void Write(string line) => Console.Write(line);

		/// <inheritdoc />
		public void WriteLine() => Console.WriteLine();

		/// <inheritdoc />
		public void WriteLine(string line) => Console.WriteLine(line);

		#endregion Implementation of IConsole
	}
}
