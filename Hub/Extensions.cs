﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace Hub
{
	/// <summary>
	/// useful extension methods
	/// </summary>
	public static class Extensions
	{
		/// <summary>
		/// clean up the given branch name
		/// </summary>
		public static string CleanBranchOutput(this string output)
		{
			return output.IsNullOrEmpty() ? "" : output.TrimEnd().Replace("*", "").Replace("origin/", "").Replace(" ", "");

		}	// end CleanBranchOutput(this string)
		
		/// <summary>
		/// Flips the given value, i.e., adds it if it isn't in the list, removes it if it is
		/// </summary>
		/// <param name="selected">the list to check</param>
		/// <param name="toFlip">the value to flip</param>
		public static void Flip<T>(this List<T> selected, T toFlip)
		{
			if (selected.Contains(toFlip))
			{
				selected.Remove(toFlip);
			}	// end if
			else
			{
				selected.Add(toFlip);
			}	// end else

		}	// end Flip<T>(this List<T>, T);

		/// <summary>
		/// is the given string null or empty
		/// </summary>
		public static bool IsNullOrEmpty(this string value) => string.IsNullOrEmpty(value);

		/// <summary>
		/// for now, this just adds quotes around any part of a path that has a space in it
		/// </summary>
		public static string QuoteTheSpaces(this string path)
		{
			if (!path.Contains(" "))
			{
				return path;
			}	// end if

			// this breaks the path into each level and adds quotes around any part that includes a space, the puts the path back together
			var result = path.Split('\\').Aggregate("", (current, part) => part.Contains(" ") ? $"{current}\\\"{part}\"" : $"{current}\\{part}");

			return result.Substring(1);

		}	// end QuoteTheSpaces(this string)

		///
		[ExcludeFromCodeCoverage]
		public static void Remove(this Dictionary<string, List<string>> dict, string key, string value)
		{
			if (!dict.ContainsKey(key))
			{
				return;
			}	// end if

			dict[key].Remove(value);
			if (dict[key].Count == 0)
			{
				dict.Remove(key);
			}	// end if
			
		}	// end Remove(this Dictionary<string, List<string>>, string, string)

		/// <summary>
		/// adds all ints from 0 to count - 1 to the given list
		/// </summary>
		public static void SelectAll(this List<int> list, int count)
		{
			list.Clear();
			for (var i = 0; i < count; i++)
			{
				list.Add(i);
			}	// end for

		}	// end SelectAll(this List<int>, int)

		/// <summary>
		/// Splits a string into substrings based on the characters in an array. You can specify whether the substrings include empty array elements.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public static string[] Split(this string str, char ch, StringSplitOptions options) => str.Split(new[] { ch }, options);

		/// <summary>
		/// gets the text that is inside a CDATA "tag"
		/// </summary>
		public static string StripCData(this string value)
		{
			const string cData = "![CDATA[";
			if (value.IsNullOrEmpty() || !value.StartsWith(cData) || !value.EndsWith("]]"))
			{
				return value ?? "";
			}	// end if
			var len = cData.Length;
			return value.Substring(len, value.Length - len - 2);

		}	// end StripCData(this string)

		/// <summary>
		/// converts the given string to a double, using the given default value if it can't
		/// </summary>
		public static double ToDoub(this string value, double? @default = 0d) => double.TryParse(value, out var num) ? num : @default ?? 0d;

		/// <summary>
		/// converts the given bool to an int
		/// </summary>
		public static int ToInt(this bool value) => value ? 1 : 0;

		/// <summary>
		/// converts the given string to an int, using the given default value if it can't
		/// </summary>
		public static int ToInt(this string value, int? @default = 0)
		{
			if (!@default.HasValue)
			{
				@default = 0;
			}	// end if
			return int.TryParse(value, out var val) ? val :
				double.TryParse(value, out var doubVal) ? Convert.ToInt32(doubVal) : @default.Value;

		}	// end ToInt(this string, int?)

		///
		[ExcludeFromCodeCoverage]
		public static (string key, string value) ToPair(this ResXDataNode node) => (node.Name, node.GetValue((AssemblyName[])null).ToString());

		/// <summary>
		/// converts a list of ints to a list of strings
		/// </summary>
		public static List<string> ToStringList(this IEnumerable<int> ints) => ints.Select(i => $"{i}").ToList();

		/// <summary>
		/// converts the given string to a <see cref="TimeSpan" />
		/// </summary>
		public static TimeSpan ToTimeSpan(this string dur, bool ignore = false)
		{
			return ignore || dur.IsNullOrEmpty() ? TimeSpan.Zero : TimeSpan.FromSeconds(dur.ToDoub());

		}	// end ToTimeSpan(this string, bool)

		/// <summary>
		/// will peek into the queue, if it has any items, otherwise will return the default for the type
		/// </summary>
		[ExcludeFromCodeCoverage]
		public static T TryPeek<T>(this Queue<T> queue) => queue.Count > 0 ? queue.Peek() : default(T);

	}	// end Extensions

}	// end Hub
