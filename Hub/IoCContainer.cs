﻿using System;
using System.Collections.Generic;

namespace Hub
{
	/// <summary>
	/// Inversion of Control container
	/// </summary>
	public class IoCContainer
	{
		#region Variables

		private static IoCContainer _instance;
		private static readonly object _lock = new object( );

		private readonly Dictionary<Type, object> _multiInstance;
		private readonly Dictionary<Type, object> _singleInstance;

		#endregion Variables

		#region Property

		/// <summary>
		/// the singleton instance
		/// </summary>
		public static IoCContainer Instance
		{
			get
			{
				lock( _lock )
				{
					return _instance ?? ( _instance = new IoCContainer( ) );
				}
			}
		}

		#endregion Property

		#region Constructor

		private IoCContainer( )
		{
			_multiInstance = new Dictionary<Type, object>( );
			_singleInstance = new Dictionary<Type, object>( );
		}

		#endregion Constructor

		#region Methods

		/// <summary>
		/// Register the given type as a multi instance.  This means the container will always hand back
		/// a new instance of the register type.  Note, if this type has a registered signle instance,
		/// that registration will be removed.
		/// </summary>
		public void RegisterAsMultiInstance<RegisterType, ImplementationType>( Func<ImplementationType> createInstanceFunc )
			where RegisterType : class where ImplementationType : class, RegisterType
		{
			Type type = typeof( RegisterType );
			_singleInstance.Remove( type );
			_multiInstance[ type ] = createInstanceFunc ?? throw new ArgumentException( "cannot register null instance" );
		}

		/// <summary>
		/// Registers the given object as a singleton.  This means the container will always hand back this
		/// specific instance.  Note, if this type has a registered multi instance, that registration will be removed.
		/// </summary>
		public void RegisterAsSingleInstance<RegisterType, ImplementationType>( ImplementationType singleton )
			where RegisterType : class
			where ImplementationType : class, RegisterType
		{
			Type type = typeof( RegisterType );
			_multiInstance.Remove( type );
			_singleInstance[ type ] = singleton ?? throw new ArgumentException( "cannot register null instance" );
		}

		/// <summary>
		/// Removes the registered type
		/// </summary>
		public void RemoveRegisteredType<RegisterType>( )
			where RegisterType : class
		{
			Type type = typeof( RegisterType );
			_singleInstance.Remove( type );
			_multiInstance.Remove( type );
		}

		/// <summary>
		/// Resolve this instance.
		/// </summary>
		/// <typeparam name="ResolveType">The type that we are trying to resolve.</typeparam>
		public ResolveType Resolve<ResolveType>()
			where ResolveType : class
		{
			Type resolveType = typeof(ResolveType);

			if (_singleInstance.TryGetValue(resolveType, out object value))
			{
				return ValidateAndReturnRegisteredSingleInstance<ResolveType>(value);
			}

			if (_multiInstance.TryGetValue(resolveType, out value))
			{
				return ValidateAndReturnRegisteredMultiInstance<ResolveType>(value);
			}

			throw new ArgumentException( $"Tried to resolve type '{resolveType.Name}', but nothing had been registered for that type" );
		}

		private RegisterType ValidateAndReturnRegisteredMultiInstance<RegisterType>( object registeredInstance )
		{
			if( registeredInstance is Func<RegisterType> func )
			{
				return func( );
			}

			_multiInstance.Remove(typeof(RegisterType));
			throw new ArgumentException( $"Somehow the registered function for type {typeof( RegisterType ).Name} returns the wrong type" );
		}

		private RegisterType ValidateAndReturnRegisteredSingleInstance<RegisterType>( object registeredInstance )
		{
			if( registeredInstance is RegisterType type )
			{
				return type;
			}

			_singleInstance.Remove(typeof(RegisterType));
			throw new ArgumentException( $"Somehow the registered instance for type {typeof( RegisterType ).Name} was the wrong type" );
		}

		#endregion Methods
	}
}
