﻿using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// Class to set the console's title
	/// </summary>
	public class Title
	{
		#region Variables

		private string _format;
		private static Title _instance;
		private bool _isAdmin, _showRemote;

		private IGit _git;

		#endregion Variables

		#region Singleton

		///
		public static Title Instance => _instance ?? (_instance = new Title());

		private Title() { }

		///
		public void Initialize(string format, IGit git, bool isAdmin, bool showRemote)
		{
			if (format.IsNullOrEmpty() || git == null)
				return;

			_format = format;
			_git = git;
			_isAdmin = isAdmin;
			_showRemote = showRemote;
		}

		#endregion Singleton

		#region Method

		/// <summary>
		/// updates the title using the pre-assigned format
		/// </summary>
		public void Update(List<string> args = null)
		{
			if (_format.IsNullOrEmpty())
			{
				return;
			}
			
			var xm8 = _showRemote ? _git.GetRemoteName() : _git.GetBranchName();
			
			var admin = _isAdmin ? "Administrator:  " : "";

			var title = string.Format(_format, admin, Env.Repo, xm8, Env.Mode);

			if (args != null && args.Count > 0)
			{
				var str = args.Aggregate("", (current, line) => current + " " + line);
				title = $"{title} -{str}";
			}
			
			IoCContainer.Instance.Resolve<IConsole>().Title = title;
		}
		
		#endregion Method
	}
}
