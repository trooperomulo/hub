﻿using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// Manages the shortcuts
	/// </summary>
	public class Shortcuts
	{
		#region Variables

		private Dictionary<string, (string path, string other)> _shortcuts;
		private Dictionary<string, string> _pathToShortcut;

		private static Shortcuts _instance;

		#endregion Variables

		#region Property

		/// <summary>
		/// have any shortcuts been loaded
		/// </summary>
		public bool Loaded { get; private set; }

		#endregion Property

		#region Constructor / Initialization

		private Shortcuts() { }

		///
		public static Shortcuts Instance => _instance ?? (_instance = new Shortcuts());

		/// <summary>
		/// loads the shortcuts at the given location
		/// </summary>
		public void LoadShortcuts(string name)
		{
			var files = IoCContainer.Instance.Resolve<IFiles>();
			if (!files.Exists(name))
			{
				return;
			}
			
			LoadDictionaries(files.ReadLines(name).ToList());

			Loaded = _shortcuts.Count > 0;
		}

		#endregion Constructor / Initialization

		#region Public Methods

		/// <summary>
		/// clears out shortcuts that might have already been loaded
		/// </summary>
		public void Clear()
		{
			_shortcuts = new Dictionary<string, (string path, string other)>();
			_pathToShortcut = new Dictionary<string, string>();
			Loaded = false;
		}

		/// <summary>
		/// gets the other info for the given shortcut, if it's been added
		/// </summary>
		public string GetOther(string shortcut)
		{
			if (!Loaded || !_shortcuts.ContainsKey(shortcut))
				return "";

			return _shortcuts[shortcut].other;
		}

		/// <summary>
		/// gets the path for the given shortcut, if it's been added
		/// </summary>
		public string GetPath(string shortcut)
		{
			if (!Loaded || !_shortcuts.ContainsKey(shortcut))
				return "";

			return _shortcuts[shortcut].path;
		}

		/// <summary>
		/// determines if the given path has been added
		/// </summary>
		public bool HasPath(string path) => Loaded && _pathToShortcut.ContainsKey(path);

		/// <summary>
		/// determines if the given shortcut has been added
		/// </summary>
		public bool HasShortcut(string shortcut) => Loaded && _shortcuts.ContainsKey(shortcut);

		/// <summary>
		/// determines the shortcut for the given path
		/// </summary>
		public string PathToShortcut(string path)
		{
			if (!Loaded || !_pathToShortcut.ContainsKey(path))
				return "";

			return _pathToShortcut[path];
		}

		#endregion Public Methods

		#region Private Methods

		private void LoadDictionaries(List<string> lines)
		{
			Clear();

			lines.ForEach(ReadLine);
		}

		private void ReadLine(string line)
		{
			if (line.StartsWith(";"))
			{
				return;
			}
			// shortcut=       path         =other
			var parts = line.Split('=');
			if (parts.Length < 2 || parts[0].IsNullOrEmpty())
			{
				return;
			}

			var shortcut = parts[0];
			var path = parts[1].Trim().ToLower();
			var other = parts.Length > 2 ? parts[2] : null;
			_shortcuts.Add(shortcut, (path, other));
			_pathToShortcut[path] = shortcut;
		}

		#endregion Private Methods
	}
}
