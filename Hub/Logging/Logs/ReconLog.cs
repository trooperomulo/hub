﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the import command
	/// </summary>
	public class ReconLog : LogFile<ReconLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Recon);

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public ReconLog(bool xm8, bool trick, int num) => Data = new ReconLogData(xm8, trick, num);

		///
		public ReconLog(string tempPath) => LoadFromLogFile(tempPath, ReconLogData.Empty());

		#endregion Constructors
	}
}
