﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the import command
	/// </summary>
	public class LaunchLog : LogFile<LaunchLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Launch);

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public LaunchLog(bool trick, bool test, int num) => Data = new LaunchLogData(trick, test, num);

		///
		public LaunchLog(string tempPath) => LoadFromLogFile(tempPath, LaunchLogData.Empty());

		#endregion Constructors
	}
}
