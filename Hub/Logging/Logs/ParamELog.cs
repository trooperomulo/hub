﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the param e command
	/// </summary>
	public class ParamELog : LogFile<ParamELogData>
	{
		#region Overrides of LogFile<ParamELogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ParamE);

		#endregion Overrides of LogFile<ParamELogData>

		#region Constructors
		
		///
		public ParamELog(int num) => Data = new ParamELogData(num);

		///
		public ParamELog(string tempPath) => LoadFromLogFile(tempPath, ParamELogData.Empty());

		#endregion Constructors

	}   // end ParamELog

}	// end Hub.Logging.Logs
