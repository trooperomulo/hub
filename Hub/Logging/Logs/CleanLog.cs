﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the clean command
	/// </summary>
	public class CleanLog : LogFile<CleanLogData>
	{
		#region Overrides of LogFile

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Clean);

		#endregion Overrides of LogFile

		#region Constructors

		///
		public CleanLog(bool quiet, int num, bool usedDef) => Data = new CleanLogData(quiet, num, usedDef);

		///
		public CleanLog(string tempPath) => LoadFromLogFile(tempPath, CleanLogData.Empty());

		#endregion Constructors
	}
}
