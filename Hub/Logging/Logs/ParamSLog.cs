﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// Logs usage of the param s command
	/// </summary>
	public class ParamSLog : LogJustTotalFile
	{
		/* The v4 params log contains the following data points
		 *
		 * Total times called */


		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.ParamS;

		#endregion Overrides of LogJustTotalFile

		#region Constructors
		
		/// <inheritdoc />
		public ParamSLog() { }
		
		/// <inheritdoc />
		public ParamSLog(string tempPath) : base(tempPath) { }

		#endregion Constructors

	}	// end ParamSLog

}	// end Hub.Logging.Logs
