﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// Logs usage of the oew command
	/// </summary>
	public class OewLog : LogJustTotalFile
	{
		/* The oew log contains the following data points
		 *
		 * Total times called */

		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.Oew;

		#endregion Overrides of LogJustTotalFile

		#region Constructors

		/// <inheritdoc />
		public OewLog() { }

		/// <inheritdoc />
		public OewLog(string tempPath) : base(tempPath) { }

		#endregion Constructors

	}	// end OewLog

}	// end Hub.Logging.Logs
