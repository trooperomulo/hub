﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of a res command with invalid parameters
	/// </summary>
	public class ResLog : LogFile<ResLogData>
	{
		#region Overrides of LogFile<ResLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Res);

		#endregion Overrides of LogFile<ResLogData>

		#region Constructors

		///
		public ResLog(bool noAct, bool multSol, bool noPath, bool badPairs, bool noKeys, bool noFVals, bool noTVals)
		{
			Data = new ResLogData(noAct, multSol, noPath, badPairs, noKeys, noFVals, noTVals);

		}	// end ResLog(bool, bool, bool, bool, bool, bool, bool)

		///
		public ResLog(string tempPath) => LoadFromLogFile(tempPath, ResLogData.Empty());

		#endregion Constructors

	}	// end ResLog

}	// end Hub.Logging.Logs
