﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the help command
	/// </summary>
	public class HelpLog : LogFile<HelpLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Help);

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public HelpLog(int num) => Data = new HelpLogData(num);

		///
		public HelpLog(string tempPath) => LoadFromLogFile(tempPath, HelpLogData.Empty());

		#endregion Constructors
	}
}
