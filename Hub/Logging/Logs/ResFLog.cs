﻿using Hub.Enum;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the res f command
	/// </summary>
	public class ResFLog : LogFile<ResFLogData>
	{
		#region Overrides of LogFile<ResFLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ResF);

		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 2)
			{
				UpgradeFromV2ToV3(ref lines);
				UpdateVersion(3, ref lines);
			}	// end if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[])

		#endregion Overrides of LogFile<ResTLogData>

		#region Constructors

		///
		public ResFLog(Res res, int num) => Data = new ResFLogData(res, num);

		///
		public ResFLog(string tempPath) => LoadFromLogFile(tempPath, ResFLogData.Empty());

		#endregion Constructors
		
		#region Helper

		private void UpgradeFromV2ToV3(ref string[] lines)
		{
			/* v0-v2 resf log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified
			 * Times all solutions were searched
			 * Total values searched for */
			 
			/* v3 resf log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times shared was specified			<-- Added
			 * Times no solution was specified
			 * Times all solutions were searched
			 * Total values searched for */

			InsertLines(ref lines, 4, 1);

		}	// end UpgradeFromV2ToV3(ref string[])

		#endregion Helper

	}	// end ResFLog

}	// end Hub.Logging.Logs
