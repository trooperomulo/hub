﻿using Hub.Enum;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the res a command
	/// </summary>
	public class ResALog : LogFile<ResALogData>
	{
		#region Overrides of LogFile<ResALogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ResA);

		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 2)
			{
				UpgradeFromV2ToV3(ref lines);
				UpdateVersion(3, ref lines);
			}	// end if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[])

		#endregion Overrides of LogFile<ResALogData>

		#region Constructors

		///
		public ResALog(Res res, int num) => Data = new ResALogData(res, num);

		///
		public ResALog(string tempPath) => LoadFromLogFile(tempPath, ResALogData.Empty());

		#endregion Constructors

		#region Helper

		private void UpgradeFromV2ToV3(ref string[] lines)
		{
			/* v0-v2 resa log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified
			 * Times resources were added from a file
			 * Total resources added */
			 
			/* v3 resa log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times shared was specified			<-- Added
			 * Times no solution was specified
			 * Times resources were added from a file
			 * Total resources added */

			InsertLines(ref lines, 4, 1);

		}	// end UpgradeFromV2ToV3(ref string[])

		#endregion Helper

	}	// end ResALog

}	// end Hub.Logging.Logs
