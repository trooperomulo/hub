﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the email command
	/// </summary>
	public class EmailLog : LogFile<EmailLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Email);

		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 1)
			{
				UpgradeV1ToV2(ref lines);
				UpdateVersion(2, ref lines);
			}	// end if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[] lines)

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public EmailLog(bool folder, bool xm8, bool data, bool patch, bool major, bool minor, bool build, bool revision)
		{
			Data = new EmailLogData(folder, xm8, data, patch, major, minor, build, revision);
		}	// end EmailLog(bool, bool, bool, bool, bool, bool, bool, bool)

		///
		public EmailLog(string tempPath) => LoadFromLogFile(tempPath, EmailLogData.Empty());

		#endregion Constructors

		#region Method

		private void UpgradeV1ToV2(ref string[] lines)
		{
			/* The v0-v1 email log contains the following data points
			 *
			 * 01 - Total times called
			 * 02 - Times no arguments are specified
			 * 03 - Times the folder is specified
			 * 04 - Times the xm8 branch is specified
			 * 05 - Times the shared branch is specified	<-- Removed
			 * 06 - Times the deploy branch is specified	<-- Removed
			 * 07 - Times the data branch is specified
			 * 08 - Times the bm branch is specified		<-- Removed
			 * 09 - Times the patch build is specified
			 * 10 - Times the major version is specified
			 * 11 - Times the minor version is specified
			 * 12 - Times the build version is specified
			 * 13 - Times the revision version is specified */

			/* The v2 email log contains the following data points
			 *
			 * 01 - Total times called
			 * 02 - Times no arguments are specified
			 * 03 - Times the folder is specified
			 * 04 - Times the xm8 branch is specified
			 * 05 - Times the data branch is specified
			 * 06 - Times the patch build is specified
			 * 07 - Times the major version is specified
			 * 08 - Times the minor version is specified
			 * 09 - Times the build version is specified
			 * 10 - Times the revision version is specified */

			// so we need to remove the 5th, 6th and 8th lines

			RemoveLines(ref lines, 5, 6, 8);

		}	// end UpgradeV1ToV2(ref string[])

		#endregion Method

	}	// end EmailLog

}	// end Hub.Logging.Logs
