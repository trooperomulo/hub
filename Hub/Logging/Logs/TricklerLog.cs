﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the trickler command
	/// </summary>
	public class TricklerLog : LogFile<TricklerLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Trickler);

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public TricklerLog(bool xm8, bool unit, int num) => Data = new TricklerLogData(xm8, unit, num);

		///
		public TricklerLog(string tempPath) => LoadFromLogFile(tempPath, TricklerLogData.Empty());

		#endregion Constructors
	}
}
