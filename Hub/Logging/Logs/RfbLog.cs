﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// Logs usage of the resumable full build command
	/// </summary>
	public class RfbLog : LogJustTotalFile
	{
		/* The rfb log contains the following data points
		 * 
		 * Total times called */

		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.Rfb;

		#endregion Overrides of LogJustTotalFile

		#region Constructors

		/// <inheritdoc />
		public RfbLog() { }

		/// <inheritdoc />
		public RfbLog(string tempPath) : base(tempPath) { }

		#endregion Constructors
	}
}
