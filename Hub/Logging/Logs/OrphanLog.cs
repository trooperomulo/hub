﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the import command
	/// </summary>
	public class OrphanLog : LogFile<OrphanLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Orphan);
		
		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 1)
			{
				UpgradeV1ToV2(ref lines);
				UpdateVersion(2, ref lines);
			}	// end if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[] lines)

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public OrphanLog(bool xm8, bool shared) => Data = new OrphanLogData(xm8, shared);

		///
		public OrphanLog(string tempPath) => LoadFromLogFile(tempPath, OrphanLogData.Empty());

		#endregion Constructors

		#region Method
		
		private void UpgradeV1ToV2(ref string[] lines)
		{
			/* the v1 orphan log contains the following data points
			 * 
			 * Total times called
			 * Times called with the xm8 flag
			 * Times called with the shared flag		<-- Changed
			 * */
			 
			/* the v1 orphan log contains the following data points
			 * 
			 * Total times called
			 * Times called with the xm8 flag
			 * Times called with the data flag		<-- Changed
			 * */

			// so we need to reset whatever count was in the 2nd position

			lines[3] = "0";

		}	// end UpgradeV1ToV2(ref string[])

		#endregion Method
	}
}
