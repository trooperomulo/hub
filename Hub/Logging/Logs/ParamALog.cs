﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the param a command
	/// </summary>
	public class ParamALog : LogFile<ParamALogData>
	{
		#region Overrides of LogFile<ParamALogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ParamA);

		#endregion Overrides of LogFile<ParamALogData>

		#region Constructors

		///
		public ParamALog(int num) => Data = new ParamALogData(num);

		///
		public ParamALog(string tempPath) => LoadFromLogFile(tempPath, ParamALogData.Empty());

		#endregion Constructors

	}	// end ParamALog

}	// end Hub.Logging.Logs
