﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the vs command
	/// </summary>
	public class VsLog : LogFile<VsLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Vs);

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public VsLog(int num) => Data = new VsLogData(num);

		///
		public VsLog(string tempPath) => LoadFromLogFile(tempPath, VsLogData.Empty());

		#endregion Constructors
	}
}
