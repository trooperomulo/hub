﻿using Hub.Enum;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the res t command
	/// </summary>
	public class ResTLog : LogFile<ResTLogData>
	{
		#region Overrides of LogFile<ResTLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ResT);

		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 0)
			{
				InsertVersion(ref lines);
				UpgradeV0ToV1(ref lines);
				UpdateVersion(1, ref lines);
			}	// end if
			else if (oldVersion == 2)
			{
				UpgradeV2ToV3(ref lines);
				UpdateVersion(3, ref lines);
			}	// end else if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[])

		#endregion Overrides of LogFile<ResTLogData>

		#region Constructors

		///
		public ResTLog(Res res, int num) => Data = new ResTLogData(res, num);

		///
		public ResTLog(string tempPath) => LoadFromLogFile(tempPath, ResTLogData.Empty());

		#endregion Constructors

		#region Methods

		private void UpgradeV0ToV1(ref string[] lines)
		{
			/* The v0 rest log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified
			 * Total keys translations were searched for
			 * */
			 
			/* The v1-v2 rest log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified
			 * Times all solutions were searched		<-- Added
			 * Total keys translations were searched for
			 * */
			 
			// 1 value was added after the times no solution was specified

			InsertLines(ref lines, 5, 1);

		}	// end UpgradeV0ToV1(ref string[])

		private void UpgradeV2ToV3(ref string[] lines)
		{
			/* The v1-v2 rest log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified
			 * Times all solutions were searched
			 * Total keys translations were searched for
			 * */
			 
			/* The v3 rest log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times shared was specified				<-- Added
			 * Times no solution was specified
			 * Times all solutions were searched
			 * Total keys translations were searched for
			 * */

			InsertLines(ref lines, 4, 1);

		}	// end UpgradeV2ToV3(ref string[])

		#endregion Methods

	}	// end ResTLog

}	// end Hub.Logging
