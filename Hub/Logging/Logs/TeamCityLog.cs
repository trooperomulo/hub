﻿
using Hub.Enum;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the team city command
	/// </summary>
	public class TeamCityLog : LogFile<TeamCityLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.TeamCity);

		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 0)
			{
				InsertVersion(ref lines);
				UpgradeV0ToV1(ref lines);
				UpdateVersion(1, ref lines);
			}	// end if
			else if (oldVersion == 3)
			{
				UpgradeV3ToV4(ref lines);
				UpdateVersion(4, ref lines);
			}	// end else if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[])

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public TeamCityLog(Platform platform, int num) => Data = new TeamCityLogData(platform, num);

		///
		public TeamCityLog(string tempPath) => LoadFromLogFile(tempPath, TeamCityLogData.Empty());

		#endregion Constructors

		#region Method

		private void UpgradeV0ToV1(ref string[] lines)
		{
			/* The v0 team log contains the following data points
			 * 
			 * Total times called
			 * Times called with an argument */
			 
			/* The v1-v3 team log contains the following data points
			 * 
			 * Total times called
			 * Times called with an argument
			 * How many arguments have been passed in	<-- Added
			 * */

			InsertLines(ref lines, 3, 1);

		}	// end UpgradeV0ToV1(ref string[])

		private void UpgradeV3ToV4(ref string[] lines)
		{
			/* The v1-v3 team log contains the following data points
			 * 
			 * Total times called
			 * Times called with a branch
			 * How many branches have been passed in
			 * */
			 
			/* The v4 team log contains the following data points
			 * 
			 * Total times called
			 * Total times Windows was displayed	<-- Added
			 * Total times Android was displayed	<-- Added
			 * Total times iOS was displayed		<-- Added
			 * Total times called with no platform	<-- Added
			 * Times called with a branch
			 * How many branches have been passed in */

			InsertLines(ref lines, 2, 4);

		}	// end UpgradeV3ToV4(ref string[])

		#endregion Method

	}	// end TeamCityLog

}	// end Hub.Logging.Logs
