﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the param m command
	/// </summary>
	public class ParamMLog : LogFile<ParamMLogData>
	{
		#region Overrides of LogFile<ParamMLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ParamM);

		#endregion Overrides of LogFile<ParamMLogData>

		#region Constructors

		///
		public ParamMLog(int num) => Data = new ParamMLogData(num);

		///
		public ParamMLog(string tempPath) => LoadFromLogFile(tempPath, ParamMLogData.Empty());

		#endregion Constructors

	}	// end ParamMLog

}	// end Hub.Logging.Logs