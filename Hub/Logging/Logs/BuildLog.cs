﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the build command
	/// </summary>
	public class BuildLog : LogFile<BuildLogData>
	{
		#region Overrides of LogFile

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Build);

		#endregion Overrides of LogFile

		#region Constructors

		///
		public BuildLog(bool prod, bool debug, bool xm8, bool trk, bool unt, int sols)
		{
			Data = new BuildLogData(prod, debug, xm8, trk, unt, sols);
		}

		///
		public BuildLog(string tempPath) => LoadFromLogFile(tempPath, BuildLogData.Empty());

		#endregion Constructors
	}
}
