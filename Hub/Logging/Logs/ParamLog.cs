﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of a param command with invalid parameters
	/// </summary>
	public class ParamLog : LogFile<ParamLogData>
	{
		#region Overrides of LogFile<ParamLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Param);

		#endregion Overrides of LogFile<ParamLogData>

		#region Constructors

		///
		public ParamLog(bool noAct, bool noAdd, bool badAdd, bool noDel, bool noMod, bool badMod)
		{
			Data = new ParamLogData(noAct, noAdd, badAdd, noDel, noMod, badMod);

		}	// end ParamLog(bool, bool, bool, bool, bool, bool)

		///
		public ParamLog(string tempPath) => LoadFromLogFile(tempPath, ParamLogData.Empty());

		#endregion Constructors

	}	// end ParamLog

}	// end Hub.Logging.Logs
