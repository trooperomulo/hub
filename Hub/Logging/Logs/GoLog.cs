﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// Logs usage of the go command
	/// </summary>
	public class GoLog : LogJustTotalFile
	{
		/* The go log contains the following data points
		 * 
		 * Total times called */

		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.Go;

		#endregion Overrides of LogJustTotalFile

		#region Constructors

		/// <inheritdoc />
		public GoLog() { }

		/// <inheritdoc />
		public GoLog(string tempPath) : base(tempPath) { }

		#endregion Constructors
	}
}
