﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// Logs usage of the kill trickler command
	/// </summary>
	public class KillLog : LogJustTotalFile
	{
		/* The kill log contains the following data points
		 * 
		 * Total times called */

		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.Kill;

		#endregion Overrides of LogJustTotalFile

		#region Constructors

		/// <inheritdoc />
		public KillLog() { }

		/// <inheritdoc />
		public KillLog(string tempPath) : base(tempPath) { }

		#endregion Constructors
	}
}
