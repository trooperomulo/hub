﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs usage of the db command
	/// </summary>
	public class DbLog : LogJustTotalFile
	{
		/* The db log contains the following data points
		 * 
		 * Total times called */

		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.Db;

		#endregion Overrides of LogJustTotalFile

		#region Constructors

		/// <inheritdoc />
		public DbLog() { }

		/// <inheritdoc />
		public DbLog(string tempPath) : base(tempPath) { }

		#endregion Constructors
	}
}
