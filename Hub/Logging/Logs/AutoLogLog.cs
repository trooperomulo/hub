﻿namespace Hub.Logging.Logs
{
	/// <summary>
	/// Logs usage of the auto log command
	/// </summary>
	public class AutoLogLog : LogJustTotalFile
	{
		/* The auto log contains the following data points
		 * 
		 * Total times called */

		#region Overrides of LogJustTotalFile

		/// <inheritdoc />
		protected override string Name => Logger.AutoLog;

		#endregion Overrides of LogJustTotalFile

		#region Constructors

		/// <inheritdoc />
		public AutoLogLog() { }

		/// <inheritdoc />
		public AutoLogLog(string tempPath) : base(tempPath) { }

		#endregion Constructors
	}
}
