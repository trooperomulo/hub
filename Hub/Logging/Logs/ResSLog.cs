﻿using Hub.Enum;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the res s command
	/// </summary>
	public class ResSLog : LogFile<ResSLogData>
	{
		#region Overrides of LogFile<ResTLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ResS);
		
		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 2)
			{
				UpgradeFromV2ToV3(ref lines);
				UpdateVersion(3, ref lines);
			}	// end if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[])

		#endregion Overrides of LogFile<ResTLogData>

		#region Constructors

		///
		public ResSLog(Res res) => Data = new ResSLogData(res);

		///
		public ResSLog(string tempPath) => LoadFromLogFile(tempPath, ResSLogData.Empty());

		#endregion Constructors
		
		#region Helper

		private void UpgradeFromV2ToV3(ref string[] lines)
		{
			/* v0-v2 ress log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified */
			 
			/* v3 ress log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times shared was specified			<-- Added
			 * Times no solution was specified */

			InsertLines(ref lines, 4, 1);

		}	// end UpgradeFromV2ToV3(ref string[])

		#endregion Helper

	}	// end ResSLog

}	// end Hub.Logging.Logs
