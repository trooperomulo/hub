﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the param d command
	/// </summary>
	public class ParamDLog : LogFile<ParamDLogData>
	{
		#region Overrides of LogFile<ParamDLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ParamD);

		#endregion Overrides of LogFile<ParamDLogData>

		#region Constructors

		///
		public ParamDLog(int num) => Data = new ParamDLogData(num);

		///
		public ParamDLog(string tempPath) => LoadFromLogFile(tempPath, ParamDLogData.Empty());

		#endregion Constructors

	}	// end ParamDLog

}	// end Hub.Logging.Logs
