﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the import command
	/// </summary>
	public class ImportLog : LogFile<ImportLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Import);

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public ImportLog(int num, bool delete, bool days, bool user, bool status)
		{
			Data = new ImportLogData(num, delete, days, user, status);
		}

		///
		public ImportLog(string tempPath) => LoadFromLogFile(tempPath, ImportLogData.Empty());

		#endregion Constructors
	}
}
