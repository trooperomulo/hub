﻿using Hub.Enum;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the res r command
	/// </summary>
	public class ResRLog : LogFile<ResRLogData>
	{
		#region Overrides of LogFile<ResTLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.ResR);
		
		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 2)
			{
				UpgradeFromV2ToV3(ref lines);
				UpdateVersion(3, ref lines);
			}	// end if
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}	// end else

		}	// end UpdateVersion(int, ref string[])

		#endregion Overrides of LogFile<ResTLogData>

		#region Constructors

		///
		public ResRLog(Res res, int num) => Data = new ResRLogData(res, num);

		///
		public ResRLog(string tempPath) => LoadFromLogFile(tempPath, ResRLogData.Empty());

		#endregion Constructors
		
		#region Helper

		private void UpgradeFromV2ToV3(ref string[] lines)
		{
			/* v0-v2 resr log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times no solution was specified
			 * Total resources removed */
			 
			/* v3 resr log contains the following data points
			 * 
			 * Total times called
			 * Times core was specified
			 * Times xm8 was specified
			 * Times shared was specified			<-- Added
			 * Times no solution was specified
			 * Times resources removed */

			InsertLines(ref lines, 4, 1);

		}	// end UpgradeFromV2ToV3(ref string[])

		#endregion Helper

	}	// end ResRLog

}	// end Hub.Logging.Logs
