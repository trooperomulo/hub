﻿using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the unit test command
	/// </summary>
	public class UnitTestLog : LogFile<UnitTestLogData>
	{
		#region Overrides of LogFile<EmailLogData>

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.UnitTest);

		/// <inheritdoc />
		protected override void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == 0)
			{
				InsertVersion(ref lines);
				UpgradeV0ToV1(ref lines);
				UpdateVersion(1, ref lines);
			}
			else
			{
				base.UpdateVersion(oldVersion, ref lines);
			}
		}

		#endregion Overrides of LogFile<EmailLogData>

		#region Constructors

		///
		public UnitTestLog(bool estTm, bool progress, bool showTm, bool console, bool text, bool xml, bool input,
			bool xm8, bool trk, int num)
		{
			Data = new UnitTestLogData(estTm, progress, showTm, console, text, xml, input, xm8, trk, num);
		}

		///
		public UnitTestLog(string tempPath) => LoadFromLogFile(tempPath, UnitTestLogData.Empty());

		#endregion Constructors

		#region Method

		private static void UpgradeV0ToV1(ref string[] lines)
		{
			/* The v0 unit log contains the following data points
			 * 
			 * Total times called
			 * Times called with the xm8 flag
			 * Times called with the trk flag
			 * Total times launched without building
			 * Total solutions built prior to launch */
		 
			/* The v1-v4 unit log contains the following data points
			 * 
			 * Total times called
			 * Times called with the estimate time flag	<-- Added
			 * Times called with the progress flag		<-- Added
			 * Times called with the show time flag		<-- Added
			 * Times called with the console flag		<-- Added
			 * Times called with the text file flag		<-- Added
			 * Times called with the xml flag			<-- Added
			 * Times called with an input file			<-- Added
			 * Times called with the xm8 flag
			 * Times called with the trk flag
			 * Total times launched without building
			 * Total solutions built prior to launch */

			// 7 values were added after the total times called

			InsertLines(ref lines, 2, 7);

		}	// end UpgradeV0ToV1(ref string[])

		#endregion Method
	}
}
