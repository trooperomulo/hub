﻿using Hub.Interface;
using Hub.Logging.Data;

namespace Hub.Logging.Logs
{
	/// <summary>
	/// logs the usage of the import command
	/// </summary>
	public class ScrumLog : LogFile<ScrumLogData>
	{
		#region Overrides of LogFile

		/// <inheritdoc />
		public override void Log()
		{
			if (IoCContainer.Instance.Resolve<IFiles>().Exists(LogPath))
			{
				var lines = GetLines(LogPath);
				UpdateVersion(ref lines);
				Data.Add(lines);
			}

			WriteFile(Data.GetLines());
		}

		/// <inheritdoc />
		protected override string LogPath => GetPath(Logger.Scrum);

		#endregion Overrides of LogFile

		#region Constructors

		///
		public ScrumLog(int d, string h, int w, int b, int t, bool future)
		{
			Data = new ScrumLogData(d, h, w, b, t, future);
		}

		///
		public ScrumLog(string tempPath) => LoadFromLogFile(tempPath, ScrumLogData.Empty());

		#endregion Constructors
	}
}
