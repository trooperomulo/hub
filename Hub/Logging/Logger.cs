﻿using System.Collections.Generic;
using System.IO;

using Hub.Commands;
using Hub.Commands.Param;
using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;
using Hub.Logging.Logs;

namespace Hub.Logging
{
	/// <summary>
	/// logs the usage of the different commands
	/// </summary>
	public class Logger
	{
		#region Variable

		private static string _pathBase;

		#endregion Variable

		#region Constants

		/// <summary>
		/// the base location for all the log files
		/// </summary>
		public const string LogBase = @"\\w1isilon1\common\rdt\HubLogs\";

		/// <summary>
		/// the location used if the base location isn't available
		/// </summary>
		public const string Temp = @"c:\temp\hub\";

#pragma warning disable 1591
		public const string AutoLog = "auto";
		public const string Build = "build";
		public const string Clean = "clean";
		public const string Db = "db";
		public const string Email = "email";
		public const string Go = "go";
		public const string Help = "help";
		public const string Import = "import";
		public const string Kill = "kill";
		public const string Launch = "launch";
		public const string Oew = "oew";
		public const string Orphan = "orphan";
		public const string Param = "param";
		public const string ParamA = "parama";
		public const string ParamD = "paramd";
		public const string ParamE = "parame";
		public const string ParamM = "paramm";
		public const string ParamS = "params";
		public const string Recon = "recon";
		public const string Res = "res";
		public const string ResA = "resa";
		public const string ResF = "resf";
		public const string ResR = "resr";
		public const string ResS = "ress";
		public const string ResT = "rest";
		public const string Rfb = "rfb";
		public const string Scrum = "scrum";
		public const string TeamCity = "team";
		public const string Trickler = "trickler";
		public const string UnitTest = "unit";
		public const string Vs = "vs";
#pragma warning restore 1591

		#endregion Constants

		#region Properties

		///
		public static IDirectories Dirs { get; private set; }

		///
		public static string Name { get; private set; }


		private static string PathBase
		{
			get
			{
				var shouldMove = false;
				if (_pathBase.IsNullOrEmpty())
				{
					_pathBase = GetPathBase(out shouldMove);
				}

				if (shouldMove)
				{
					MoveLogs(Dirs.GetFiles($"{Temp}{Name}", "*.hub"));
				}

				return _pathBase;
			}
		}

		#endregion Properties

		#region Variables

		private bool _canLog;

		private static Logger _instance;

		#endregion Variables

		#region Constructor / Initialization

		private Logger() { }

		///
		public static Logger Instance => _instance ?? (_instance = new Logger());

		///
		public void Initialize(string name, bool userPref)
		{
			if (name.IsNullOrEmpty())
			{
				return;
			}

			Dirs = IoCContainer.Instance.Resolve<IDirectories>();
			Name = name;
			_canLog = userPref;
		}
		
		#endregion Constructor / Initialization

		#region Log Methods

		/// <summary>
		/// log the usage of <see cref="AutoLogCommand" />
		/// </summary>
		public void LogAutoLog()
		{
			if (!_canLog)
			{
				return;
			}

			var auto = new AutoLogLog();
			auto.Log();
		}

		/// <summary>
		/// log the usage of <see cref="BuildCommand" />
		/// </summary>
		/// <param name="prod">was the prod flag used</param>
		/// <param name="debug">was the debug flag used</param>
		/// <param name="xm8">was an xm8 flag used</param>
		/// <param name="trk">was a trickler flag used</param>
		/// <param name="unt">was a test flag used</param>
		/// <param name="sols">how many solutions were built</param>
		public void LogBuild(bool prod, bool debug, bool xm8, bool trk, bool unt, int sols)
		{
			if (!_canLog)
				return;

			var build = new BuildLog(prod, debug, xm8, trk, unt, sols);
			build.Log();
		}

		/// <summary>
		/// log the usage of <see cref="CleanCommand" />
		/// </summary>
		/// <param name="quiet">was the quiet flag used</param>
		/// <param name="num">how many cleans were run</param>
		/// <param name="usedDef">was the default number used</param>
		public void LogClean(bool quiet, int num, bool usedDef)
		{
			if (!_canLog)
				return;

			var clean = new CleanLog(quiet, num, usedDef);
			clean.Log();
		}

		/// <summary>
		/// log the usage of <see cref="DbCommand" />
		/// </summary>
		public void LogDb()
		{
			if (!_canLog)
				return;

			var db = new DbLog();
			db.Log();
		}

		/// <summary>
		/// log the usage of <see cref="EmailCommand" />
		/// </summary>
		public void LogEmail(bool folder, bool xm8, bool data, bool patch, bool major, bool minor, bool build,
			bool revision)
		{
			if (!_canLog)
			{
				return;
			}

			var email = new EmailLog(folder, xm8, data, patch, major, minor, build, revision);
			email.Log();

		}	// end LogEmail(bool, bool, bool, bool, bool, bool, bool, bool)

		/// <summary>
		/// log the usage of <see cref="GoCommand" />
		/// </summary>
		public void LogGo()
		{
			if (!_canLog)
				return;

			var go = new GoLog();
			go.Log();
		}

		/// <summary>
		/// log the usage of <see cref="HelpCommand" />
		/// </summary>
		/// <param name="num">the number of commands help is being asked for</param>
		public void LogHelp(int num)
		{
			if (!_canLog)
				return;

			var help = new HelpLog(num);
			help.Log();
		}

		/// <summary>
		/// log the usage of <see cref="ImportEstCommand" />
		/// </summary>
		/// <param name="num">how many estimates were imported</param>
		/// <param name="delete">did we define a value for delete</param>
		/// <param name="days">did we define a value for days</param>
		/// <param name="user">did we define a value for user</param>
		/// <param name="status">did we define a value for status</param>
		public void LogImport(int num, bool delete, bool days, bool user, bool status)
		{
			if (!_canLog)
				return;

			var import = new ImportLog(num, delete, days, user, status);
			import.Log();
		}

		/// <summary>
		/// log the usage of <see cref="KillTrickCommand" />
		/// </summary>
		public void LogKill()
		{
			if (!_canLog)
				return;

			var kill = new KillLog();
			kill.Log();
		}

		/// <summary>
		/// log the usage of <see cref="LaunchCommand" />
		/// </summary>
		/// <param name="trick">was the trickler flag used</param>
		/// <param name="test">was the test flag used</param>
		/// <param name="num">the number of solutions built</param>
		public void LogLaunch(bool trick, bool test, int num)
		{
			if (!_canLog)
				return;

			var launch = new LaunchLog(trick, test, num);
			launch.Log();
		}

		/// <summary>
		/// Log the usage of <see cref="OewCommand" />
		/// </summary>
		public void LogOew()
		{
			if (!_canLog)
			{
				return;
			}   // end if

			var oew = new OewLog();
			oew.Log();

		}	// end LogOew()

		/// <summary>
		/// log the usage of <see cref="OrphanCommand" />
		/// </summary>
		/// <param name="xm8">was the xm8 flag used</param>
		/// <param name="data">was the data flag used</param>
		public void LogOrphan(bool xm8, bool data)
		{
			if (!_canLog)
			{
				return;
			}

			var orphan = new OrphanLog(xm8, data);
			orphan.Log();

		}	// end LogOrphan(bool, bool)

		/// <summary>
		/// log the usage of a Param command with invalid parameters (no action, bad pairs, etc.)
		/// </summary>
		/// <param name="noAct">called without an action</param>
		/// <param name="noAdd">called add without any pairs</param>
		/// <param name="badAdd">called add with mismatched pairs</param>
		/// <param name="noDel">called delete without any properties</param>
		/// <param name="noMod">called modify without any pairs</param>
		/// <param name="badMod">called modify with mismatched pairs</param>
		public void LogParam(bool noAct = false, bool noAdd = false, bool badAdd = false, bool noDel = false,
			bool noMod = false, bool badMod = false)
		{
			if (!_canLog)
				return;

			var param = new ParamLog(noAct, noAdd, badAdd, noDel, noMod, badMod);
			param.Log();

		}	// end LogParam(bool, bool, bool, bool, bool, bool)

		/// <summary>
		/// logs the usage of the <see cref="ParamACommand" />
		/// </summary>
		/// <param name="num">the number of properties passed in to be added</param>
		public void LogParamA(int num)
		{
			if (!_canLog)
				return;

			var param = new ParamALog(num);
			param.Log();

		}	// end LogParamA(int)
		
		/// <summary>
		/// logs the usage of the <see cref="ParamDCommand" />
		/// </summary>
		/// <param name="num">the number of properties passed in to be deleted</param>
		public void LogParamD(int num)
		{
			if (!_canLog)
				return;

			var param = new ParamDLog(num);
			param.Log();

		}	// end LogParamD(int)

		/// <summary>
		/// logs the usage of the <see cref="ParamECommand" />
		/// </summary>
		/// <param name="num">the number of parameters passed in to be explained</param>
		public void LogParamE(int num)
		{
			if (!_canLog)
				return;

			var param = new ParamELog(num);
			param.Log();

		}	// end LogParamE(int)

		/// <summary>
		/// logs the usage of the <see cref="ParamMCommand" />
		/// </summary>
		/// <param name="num">the number of properties passed in to be modified</param>
		public void LogParamM(int num)
		{
			if (!_canLog)
				return;

			var param = new ParamMLog(num);
			param.Log();

		}	// end LogParamM(int)

		/// <summary>
		/// logs the usage of the <see cref="ParamSCommand" />
		/// </summary>
		public void LogParamS()
		{
			if (!_canLog)
				return;

			var param = new ParamSLog();
			param.Log();

		}	// end LogParamS()

		/// <summary>
		/// log the usage of <see cref="ReconCommand" />
		/// </summary>
		/// <param name="xm8">was the xm8 flag set</param>
		/// <param name="trk">was the trickler flag set</param>
		/// <param name="num">how many registry files were run</param>
		public void LogRecon(bool xm8, bool trk, int num)
		{
			if (!_canLog)
				return;

			var recon = new ReconLog(xm8, trk, num);
			recon.Log();

		}	// end LogRecon(bool, bool, int)

		/// <summary>
		/// log the usage of <see cref="ResxCommand" /> with invalid parameters (no action, multiple solutions specified, etc.)
		/// </summary>
		/// <param name="noAct">called without an action</param>
		/// <param name="multSol">called with multiple solutions</param>
		/// <param name="noPath">called without selecting a path</param>
		/// <param name="badPairs">called add with mismatched key value pairs, or no pairs</param>
		/// <param name="noKeys">called remove without any keys</param>
		/// <param name="noFVals">called find without any values</param>
		/// <param name="noTVals">called translation without any values</param>
		// ReSharper disable IdentifierTypo
		public void LogRes(bool noAct = false, bool multSol = false, bool noPath = false, bool badPairs = false,
			bool noKeys = false, bool noFVals = false, bool noTVals = false)
		{
			if (!_canLog)
				return;

			var res = new ResLog(noAct, multSol, noPath, badPairs, noKeys, noFVals, noTVals);
			res.Log();

		}	// end LogRes(bool, bool, bool, bool, bool, bool, bool)
		// ReSharper restore IdentifierTypo

		/// <summary>
		/// log the usage of <see cref="ResxCommand" /> with the add action
		/// </summary>
		/// <param name="res">what solution(s) was specified</param>
		/// <param name="num">how many pairs were added</param>
		public void LogResA(Res res, int num)
		{
			if (!_canLog)
				return;

			// ReSharper disable once IdentifierTypo
			var resa = new ResALog(res, num);
			resa.Log();

		}	// end LogResA(Res, int)

		/// <summary>
		/// log the usage of <see cref="ResxCommand" /> with the find action
		/// </summary>
		/// <param name="res">what solution was specified</param>
		/// <param name="num">how many values were searched for</param>
		public void LogResF(Res res, int num)
		{
			if (!_canLog)
				return;
			
			// ReSharper disable once IdentifierTypo
			var resf = new ResFLog(res, num);
			resf.Log();

		}	// end LogResF(Res, bool, int)

		/// <summary>
		/// log the usage of <see cref="ResxCommand" /> with the remove action
		/// </summary>
		/// <param name="res">what solution was specified</param>
		/// <param name="num">how many keys were removed</param>
		public void LogResR(Res res, int num)
		{
			if (!_canLog)
				return;
			
			// ReSharper disable once IdentifierTypo
			var resr = new ResRLog(res, num);
			resr.Log();

		}	// end LogResR(Res, int)

		/// <summary>
		/// log the usage of <see cref="ResxCommand" /> with the sort action
		/// </summary>
		/// <param name="res">what solution was specified</param>
		public void LogResS(Res res)
		{
			if (!_canLog)
				return;
			
			// ReSharper disable once IdentifierTypo
			var ress = new ResSLog(res);
			ress.Log();
			
		}	// end LogResS(Res)

		/// <summary>
		/// log the usage of <see cref="ResxCommand" /> with the translation action
		/// </summary>
		/// <param name="res">what solution was specified</param>
		/// <param name="num">how many keys translations were found for</param>
		public void LogResT(Res res, int num)
		{
			if (!_canLog)
				return;

			var rest = new ResTLog(res, num);
			rest.Log();

		}	// end LogResT(Res, int)

		/// <summary>
		/// log the usage of <see cref="ResumableFullBuildCommand" />
		/// </summary>
		public void LogRfb()
		{
			if (!_canLog)
				return;

			var rfb = new RfbLog();
			rfb.Log();
		}

		/// <summary>
		/// log the usage of <see cref="ScrumCommand" />
		/// </summary>
		/// <param name="d">how many did lines were there</param>
		/// <param name="h">the hours</param>
		/// <param name="w">how many will do lines were there</param>
		/// <param name="b">how many blocker lines were there</param>
		/// <param name="t">how many task lines were there</param>
		/// <param name="future">was a file for a future day being opened</param>
		public void LogScrum(int d, string h, int w, int b, int t, bool future)
		{
			if (!_canLog)
				return;

			var scrum = new ScrumLog(d, h, w, b, t, future);
			scrum.Log();
		}

		/// <summary>
		/// log the usage of <see cref="TeamCityCommand" />
		/// </summary>
		/// <param name="platform">what platforms were specified</param>
		/// <param name="num">the number of branches passed in</param>
		public void LogTeamCity(Platform platform, int num)
		{
			if (!_canLog)
				return;

			var team = new TeamCityLog(platform, num);
			team.Log();

		}	// end LogTeamCity(Platform, int)

		/// <summary>
		/// log the usage of <see cref="TricklerCommand" />
		/// </summary>
		/// <param name="xm8">was the xm8 flag used</param>
		/// <param name="unit">was the unit flag used</param>
		/// <param name="num">how many solutions were built</param>
		public void LogTrickler(bool xm8, bool unit, int num)
		{
			if (!_canLog)
				return;

			var trick = new TricklerLog(xm8, unit, num);
			trick.Log();
		}

		/// <summary>
		/// log the usage of <see cref="UnitTestCommand" />
		/// </summary>
		/// <param name="estTm">was the estimate time flag used</param>
		/// <param name="progress">was the show time flag used</param>
		/// <param name="showTm">was the show time flag used</param>
		/// <param name="console">was the console output flag used</param>
		/// <param name="text">was the text output flag used</param>
		/// <param name="xml">was the xml output flag used</param>
		/// <param name="input">was an input file given</param>
		/// <param name="xm8">was the xm8 flag used</param>
		/// <param name="trk">was the trk flag used</param>
		/// <param name="num">how many solutions were built</param>
		public void LogUnit(bool estTm, bool progress, bool showTm, bool console, bool text, bool xml, bool input, bool xm8,
			bool trk, int num)
		{
			if (!_canLog)
				return;

			var unit = new UnitTestLog(estTm, progress, showTm, console, text, xml, input, xm8, trk, num);
			unit.Log();
		}

		/// <summary>
		/// log the usage of <see cref="VsCommand" />
		/// </summary>
		/// <param name="num">how many solutions were opened</param>
		public void LogVs(int num)
		{
			if (!_canLog)
				return;

			var vs = new VsLog(num);
			vs.Log();
		}

		#endregion Log Methods

		#region Helper Methods

		///
		public static string GetPath(string name)
		{
			var user = $"{PathBase}{Name}";
			if (!Dirs?.Exists(user) ?? false)
			{
				Dirs.CreateDirectory(user);
			}

			return $@"{user}\{name}.hub";
		}

		private static string GetPathBase(out bool shouldMove)
		{
			shouldMove = false;

			if (Dirs == null)
			{
				return Temp;
			}

			if (Dirs.Exists(LogBase))
			{
				var user = $"{Temp}{Name}";
				// check if any logs exist in the temp location
				if (Dirs.Exists(user))
				{
					var logs = Dirs.GetFiles(user, "*.hub");
					if (logs.Count > 0)
					{
						shouldMove = true;
					}
				}

				return LogBase;
			}

			if (!Dirs.Exists(Temp))
			{
				Dirs.CreateDirectory(Temp);
			}

			return Temp;
		}

		private static void MoveLog(string tempLog, string permLog, string logName, ILogMover mover)
		{
			// if we don't have a permanent log for this temp log, just copy it over, if it's an old version, writing to it will update it
			var files = IoCContainer.Instance.Resolve<IFiles>();
			if (!files.Exists(permLog))
			{
				files.Copy(tempLog, permLog);
				return;
			}

			mover.MoveLog(logName, tempLog);
		}

		private static void MoveLogs(List<string> tempLogs)
		{
			var mover = IoCContainer.Instance.Resolve<ILogMover>();
			// loop through the logs
			foreach (var tempLog in tempLogs)
			{
				var logName = new FileInfo(tempLog).Name;
				MoveLog(tempLog, $@"{_pathBase}{Name}\{logName}", logName.Replace(".hub", ""), mover);
			}
		}

		#endregion Helper Methods
	}
}
