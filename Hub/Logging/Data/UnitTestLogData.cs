﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class UnitTestLogData : ILogData
	{
		/* The unit log contains the following data points
		 * 
		 * Total times called
		 * Times called with the estimate time flag
		 * Times called with the progress flag
		 * Times called with the show time flag
		 * Times called with the console flag
		 * Times called with the text file flag
		 * Times called with the xml flag
		 * Times called with an input file
		 * Times called with the xm8 flag
		 * Times called with the trk flag
		 * Total times launched without building
		 * Total solutions built prior to launch */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int EstimateTime { get; private set; }
		public int Progress { get; private set; }
		public int ShowTime { get; private set; }
		public int Console { get; private set; }
		public int Text { get; private set; }
		public int Xml { get; private set; }
		public int Input { get; private set; }
		public int Xm8 { get; private set; }
		public int Trick { get; private set; }
		public int NoSols { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal UnitTestLogData(bool estTm, bool prog, bool showTm, bool console, bool text, bool xml, bool input,
			bool xm8, bool trk, int num)
		{
			Total = 1;
			EstimateTime = estTm.ToInt();
			Progress = prog.ToInt();
			ShowTime = showTm.ToInt();
			Console = console.ToInt();
			Text = text.ToInt();
			Xml = xml.ToInt();
			Input = input.ToInt();
			Xm8 = xm8.ToInt();
			Trick = trk.ToInt();
			NoSols = (num == 0).ToInt();
			Num = num;

		}	// end UnitTestLogData(bool, bool, bool, bool, bool, bool, bool, bool, bool, int)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 13)
			{
				return;
			}	// end if

			Total += lines[01].ToInt();
			EstimateTime += lines[02].ToInt();
			Progress += lines[03].ToInt();
			ShowTime += lines[04].ToInt();
			Console += lines[05].ToInt();
			Text += lines[06].ToInt();
			Xml += lines[07].ToInt();
			Input += lines[08].ToInt();
			Xm8 += lines[09].ToInt();
			Trick += lines[10].ToInt();
			NoSols += lines[11].ToInt();
			Num += lines[12].ToInt();

		}	// end Add(string[])

		///
		public static UnitTestLogData Empty()
		{
			return new UnitTestLogData(false, false, false, false, false, false, false, false, false, 0)
			{
				Total = 0,
				NoSols = 0
			};

		}	// end Empty()

		/// <inheritdoc />
		public int[] ToInts() => new List<int>
		{
			Total,
			EstimateTime,
			Progress,
			ShowTime,
			Console,
			Text,
			Xml,
			Input,
			Xm8,
			Trick,
			NoSols,
			Num
		}.ToArray();

		#endregion Methods

	}	// end UnitTestLogData

}	// end Hub.Logging.Data
