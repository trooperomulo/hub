﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class OrphanLogData : ILogData
	{
		/* the orphan log contains the following data points
		 * 
		 * Total times called
		 * Times called with the xm8 flag
		 * Times called with the data flag */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Xm8 { get; private set; }
		public int Data { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal OrphanLogData(bool xm8, bool data)
		{
			Total = 1;
			Xm8 = xm8.ToInt();
			Data = data.ToInt();

		}	// end OrphanLogData(bool, bool)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 4)
			{
				return;
			}

			Total += lines[1].ToInt();
			Xm8 += lines[2].ToInt();
			Data += lines[3].ToInt();
		}

		///
		public static OrphanLogData Empty() => new OrphanLogData(false, false) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Xm8, Data}.ToArray();

		#endregion Methods
	}
}
