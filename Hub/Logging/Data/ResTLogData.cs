﻿using System.Collections.Generic;

using Hub.Enum;
using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ResTLogData : ILogData
	{
		/* The rest log contains the following data points
		 * 
		 * Total times called
		 * Times core was specified
		 * Times xm8 was specified
		 * Times shared was specified
		 * Times no solution was specified
		 * Times all solutions were searched
		 * Total keys translations were searched for */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Core { get; private set; }
		public int Xm8 { get; private set; }
		public int Shared { get; private set; }
		public int NoSol { get; private set; }
		public int All { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ResTLogData(Res res, int num)
		{
			Total = 1;
			Core	= res.HasFlag(Res.Core).ToInt();
			Xm8		= res.HasFlag(Res.Xm8).ToInt();
			Shared	= res.HasFlag(Res.Shared).ToInt();
			NoSol	= (res == Res.None).ToInt();
			All		= (res == Res.All).ToInt();
			Num		= num;

		}	// end ResTLogData(Res, int)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 8)
			{
				return;
			}	// end if

			Total +=	lines[1].ToInt(); 
			Core +=		lines[2].ToInt();
			Xm8 +=		lines[3].ToInt();
			Shared+=	lines[4].ToInt();
			NoSol +=	lines[5].ToInt();
			All +=		lines[6].ToInt();
			Num +=		lines[7].ToInt();

		}	// end Add(string[])

		///
		public static ResTLogData Empty() => new ResTLogData(Res.None, 0) {Total = 0, NoSol = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Core, Xm8, Shared, NoSol, All, Num}.ToArray();

		#endregion Methods

	}	// end ResTLogData

}	// end Hub.Logging
