﻿using System.Collections.Generic;

using Hub.Enum;
using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class TeamCityLogData : ILogData
	{
		/* The team log contains the following data points
		 * 
		 * Total times called
		 * Total times Windows was displayed
		 * Total times Android was displayed
		 * Total times iOS was displayed
		 * Total times called with no platforms
		 * Times called with a branch
		 * How many branches have been passed in */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Windows { get; private set; }
		public int Android { get; private set; }
		public int Ios { get; private set; }
		public int NoPlat { get; private set; }
		public int Branches { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal TeamCityLogData(Platform platform, int num)
		{
			Total = 1;
			Windows = platform.HasFlag(Platform.Windows).ToInt();
			Android = platform.HasFlag(Platform.Android).ToInt();
			Ios = platform.HasFlag(Platform.iOS).ToInt();
			NoPlat = (platform == Platform.None).ToInt();
			Branches = (num > 0).ToInt();
			Num = num;

		}	// end TeamCityLogData(Platform, int)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 8)
			{
				return;
			}	// end if

			Total		+= lines[1].ToInt();
			Windows		+= lines[2].ToInt();
			Android		+= lines[3].ToInt();
			Ios			+= lines[4].ToInt();
			NoPlat		+= lines[5].ToInt();
			Branches	+= lines[6].ToInt();
			Num			+= lines[7].ToInt();

		}	// end Add(string[])

		///
		public static TeamCityLogData Empty() => new TeamCityLogData(Platform.None, 0) {Total = 0, NoPlat = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Windows, Android, Ios, NoPlat, Branches, Num}.ToArray();

		#endregion Methods

	}	// end TeamCityLogData

}	// end Hub.Logging.Data
