﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{

	/// <inheritdoc />
	public class CleanLogData : ILogData
	{
		/* The clean log contains the following data points
		 * 
		 * Total times called
		 * Times called with the quiet flag
		 * Times the clean batch file was called
		 * Times the default number was used */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Quiet { get; private set; }
		public int Batch { get; private set; }
		public int Deflt { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal CleanLogData(bool quiet, int num, bool usedDef)
		{
			Total = 1;
			Quiet = quiet.ToInt();
			Batch = num;
			Deflt = usedDef.ToInt();
		}

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 5)
			{
				return;
			}

			Total += lines[1].ToInt();
			Quiet += lines[2].ToInt();
			Batch += lines[3].ToInt();
			Deflt += lines[4].ToInt();
		}

		///
		public static CleanLogData Empty() => new CleanLogData(false, 0, false) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> { Total, Quiet, Batch, Deflt }.ToArray();

		#endregion Methods
	}

}
