﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class VsLogData : ILogData
	{
		/* The vs log contains the following data points
		 * 
		 * Total times called
		 * Times only one solution is opened
		 * Total solutions opened */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int One { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal VsLogData(int num)
		{
			Total = 1;
			One = (num == 1).ToInt();
			Num = num;

		}	// end VsLogData(int)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 4)
			{
				return;
			}	// end if

			Total += lines[1].ToInt();
			One += lines[2].ToInt();
			Num += lines[3].ToInt();

		}	// Add(string[])

		///
		public static VsLogData Empty() => new VsLogData(0) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, One, Num}.ToArray();

		#endregion Methods

	}	// end VsLogData

}	// end Hub.Logging.Data
