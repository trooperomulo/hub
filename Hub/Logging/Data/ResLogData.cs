﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ResLogData : ILogData
	{
		/* The res log contains the following data points
		 * 
		 * Total times called
		 * Times called with no action
		 * Times called with multiple solutions
		 * Times called without selecting a path
		 * Times called add with no pairs, or mismatched pairs
		 * Times called remove with no keys
		 * Times called find with no values
		 * Times called translation with no values */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int NoAct { get; private set; }
		// ReSharper disable once IdentifierTypo
		public int MultSol { get; private set; }
		public int NoPath { get; private set; }
		public int BadPairs { get; private set; }
		public int NoRKeys { get; private set; }
		// ReSharper disable once IdentifierTypo
		public int NoFVals { get; private set; }
		// ReSharper disable once IdentifierTypo
		public int NoTVals { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		// ReSharper disable IdentifierTypo
		internal ResLogData(bool noAct, bool multSol, bool noPath, bool badPairs, bool noKeys, bool noFVals, bool noTVals) 
		// ReSharper restore IdentifierTypo
		{
			Total = 1;
			NoAct = noAct.ToInt();
			MultSol = multSol.ToInt();
			NoPath = noPath.ToInt();
			BadPairs = badPairs.ToInt();
			NoRKeys = noKeys.ToInt();
			NoFVals = noFVals.ToInt();
			NoTVals = noTVals.ToInt();

		}	// end ResLogData(bool, bool, bool, bool, bool, bool, bool)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 9)
			{
				return;
			}	// end if

			Total += lines[1].ToInt();
			NoAct += lines[2].ToInt();
			MultSol += lines[3].ToInt();
			NoPath += lines[4].ToInt();
			BadPairs += lines[5].ToInt();
			NoRKeys += lines[6].ToInt();
			NoFVals += lines[7].ToInt();
			NoTVals += lines[8].ToInt();

		}	// end Add(string[])

		///
		public static ResLogData Empty() => new ResLogData(false, false, false, false, false, false, false) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() =>
			new List<int> {Total, NoAct, MultSol, NoPath, BadPairs, NoRKeys, NoFVals, NoTVals}.ToArray();

		#endregion Methods

	}	// end ResLogData

}	// end Hub.Logging.Data
