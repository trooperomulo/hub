﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ParamELogData : ILogData
	{
		// ReSharper disable once CommentTypo
		/* The parme log contains the following data points
		 *
		 * Total times called
		 * Total parameters passed in to be explained
		 * Total times called with no parameters passed in
		 */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Num { get; private set; }
		public int None { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ParamELogData(int num)
		{
			Total = 1;
			Num += num;
			if (num == 0)
			{
				None++;
			}	// end if

		}	// end ParamELogData(int)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 4)
			{
				return;
			}   // end if

			Total += lines[1].ToInt();
			Num += lines[2].ToInt();
			None += lines[3].ToInt();

		}	// end Add(string[])

		///
		public static ParamELogData Empty() => new ParamELogData(0) { Total = 0, None = 0 };

		/// <inheritdoc />
		public int[] ToInts() => new List<int> { Total, Num, None }.ToArray();

		#endregion Methods

	}	// end ParamELogData

}	// end Hub.Logging.Data
