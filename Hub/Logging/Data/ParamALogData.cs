﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ParamALogData : ILogData
	{
		// ReSharper disable once CommentTypo
		/* The v4 parama log contains the following data points
		 *
		 * Total times called
		 * Total properties passed in to be added
		 */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ParamALogData(int num)
		{
			Total = 1;
			Num += num;

		}	// end ParamALogData(int)

		#endregion Constructor

		#region Methods
		
		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 3)
			{
				return;
			}	// end if

			Total += lines[1].ToInt();
			Num += lines[2].ToInt();

		}	// end Add(string[])

		///
		public static ParamALogData Empty() => new ParamALogData(0) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Num}.ToArray();

		#endregion Methods

	}	// end ParamALogData

}	// end Hub.Logging.Data
