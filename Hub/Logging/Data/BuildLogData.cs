﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{

	/// <inheritdoc />
	public class BuildLogData : ILogData
	{
		/* The build log contains the following data points
		 * 
		 * Total times called
		 * Times called with the prod flag
		 * Times called with the debug flag
		 * Times xm8 was launched after building
		 * Times trickler was launched after building
		 * Times unit tests were run after building
		 * Times just one solution was built
		 * Times a full build occurred
		 * Total number of solutions specified */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Prod { get; private set; }
		public int Debug { get; private set; }
		public int Xm8 { get; private set; }
		public int Trick { get; private set; }
		public int Unit { get; private set; }
		public int One { get; private set; }
		public int All { get; private set; }
		public int Sols { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal BuildLogData(bool prod, bool debug, bool xm8, bool trk, bool unt, int sols)
		{
			Total = 1;
			Prod = prod.ToInt();
			Debug = debug.ToInt();
			Xm8 = xm8.ToInt();
			Trick = trk.ToInt();
			Unit = unt.ToInt();
			One = (sols == 1).ToInt();
			All = (sols == 0).ToInt();
			Sols = sols;
		}

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 10)
			{
				return;
			}

			Total += lines[1].ToInt();
			Prod += lines[2].ToInt();
			Debug += lines[3].ToInt();
			Xm8 += lines[4].ToInt();
			Trick += lines[5].ToInt();
			Unit += lines[6].ToInt();
			One += lines[7].ToInt();
			All += lines[8].ToInt();
			Sols += lines[9].ToInt();
		}

		///
		public static BuildLogData Empty() => new BuildLogData(false, false, false, false, false, 0) {Total = 0, All = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Prod, Debug, Xm8, Trick, Unit, One, All, Sols}.ToArray();

		#endregion Methods
	}

}
