﻿using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ScrumLogData : ILogData
	{
		/* The scrum log contains the following data points
		 * 
		 * Total times called
		 * Times called without any lines
		 * Times called for future files
		 * Times called with no did
		 * Times called with no hours
		 * Times called with no will do
		 * Times called with no blockers
		 * Times called with no tasks
		 * Total did lines
		 * Total will do lines
		 * Total blockers lines
		 * Total task lines
		 * Total hours */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Blank { get; private set; }
		public int Future { get; private set; }
		public int NumNoD { get; private set; }
		public int NumNoH { get; private set; }
		public int NumNoW { get; private set; }
		public int NumNoB { get; private set; }
		public int NumNoT { get; private set; }
		public int NumDid { get; private set; }
		public int NumWill { get; private set; }
		public int NumBlock { get; private set; }
		public int NumTasks { get; private set; }
		public double Hours { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ScrumLogData(int d, string h, int w, int b, int t, bool future)
		{
			Total = 1;
			Blank = (d + w + b == 0).ToInt();
			Future = future.ToInt();
			NumNoD = (d == 0).ToInt();
			NumNoH = h.IsNullOrEmpty().ToInt();
			NumNoW = (w == 0).ToInt();
			NumNoB = (b == 0).ToInt();
			NumNoT = (t == 0).ToInt();
			NumDid = d;
			NumWill = w;
			NumBlock = b;
			NumTasks = t;
			Hours = h.ToDoub();

		}	// end ScrumLogData(int, string, int, int, int, bool)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 14)
			{
				return;
			}	// end if

			Total += lines[01].ToInt();
			Blank += lines[02].ToInt();
			Future += lines[03].ToInt();
			NumNoD += lines[04].ToInt();
			NumNoH += lines[05].ToInt();
			NumNoW += lines[06].ToInt();
			NumNoB += lines[07].ToInt();
			NumNoT += lines[08].ToInt();
			NumDid += lines[09].ToInt();
			NumWill += lines[10].ToInt();
			NumBlock += lines[11].ToInt();
			NumTasks += lines[12].ToInt();
			Hours += lines[13].ToDoub();

		}	// end Add(string[])

		///
		public static ScrumLogData Empty()
		{
			return new ScrumLogData(0, "a", 0, 0, 0, false)
			{
				Total = 0,
				Blank = 0,
				NumNoD = 0,
				NumNoW = 0,
				NumNoB = 0,
				NumNoT = 0
			};

		}	// end Empty()

		///
		public List<string> GetLines()
		{
			var list = ToInts().Select(i => $"{i}").ToList();
			list.Add($"{Hours}");
			return list;
		}

		/// <inheritdoc />
		public int[] ToInts() => new List<int>
		{
			Total,
			Blank,
			Future,
			NumNoD,
			NumNoH,
			NumNoW,
			NumNoB,
			NumNoT,
			NumDid,
			NumWill,
			NumBlock,
			NumTasks
		}.ToArray();

		#endregion Methods

	}	// end ScrumLogData

}	// end Hub.Logging.Data
