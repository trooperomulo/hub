﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ImportLogData : ILogData
	{
		/* The import log contains the following data points
		 * 
		 * Total times called
		 * Total estimates imported
		 * Times delete was defined
		 * Times days was defined
		 * Times user was defined
		 * Times status was defined */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Import { get; private set; }
		public int Delete { get; private set; }
		public int Days { get; private set; }
		public int User { get; private set; }
		public int Status { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ImportLogData(int num, bool delete, bool days, bool user, bool status)
		{
			Total = 1;
			Import = num;
			Delete = delete.ToInt();
			Days = days.ToInt();
			User = user.ToInt();
			Status = status.ToInt();
		}

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 7)
			{
				return;
			}

			Total += lines[1].ToInt();
			Import += lines[2].ToInt();
			Delete += lines[3].ToInt();
			Days += lines[4].ToInt();
			User += lines[5].ToInt();
			Status += lines[6].ToInt();
		}

		///
		public static ImportLogData Empty() => new ImportLogData(0, false, false, false, false) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Import, Delete, Days, User, Status}.ToArray();

		#endregion Methods
	}
}
