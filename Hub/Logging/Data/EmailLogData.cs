﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class EmailLogData : ILogData
	{
		/* The email log contains the following data points
		 *
		 * Total times called
		 * Times no arguments are specified
		 * Times the folder is specified
		 * Times the xm8 branch is specified
		 * Times the data branch is specified
		 * Times the patch build is specified
		 * Times the major version is specified
		 * Times the minor version is specified
		 * Times the build version is specified
		 * Times the revision version is specified */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int None { get; private set; }
		public int Folder { get; private set; }
		public int Xm8 { get; private set; }
		public int Data { get; private set; }
		public int Patch { get; private set; }
		public int Major { get; private set; }
		public int Minor { get; private set; }
		public int Build { get; private set; }
		// ReSharper disable once IdentifierTypo
		public int Revis { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal EmailLogData(bool folder, bool xm8, bool data, bool patch, bool major, bool minor, bool build,
			bool revision)
		{
			Total = 1;

			Folder = folder.ToInt();
			Xm8 = xm8.ToInt();
			Data = data.ToInt();
			Patch = patch.ToInt();
			Major = major.ToInt();
			Minor = minor.ToInt();
			Build = build.ToInt();
			Revis = revision.ToInt();

			None = (!(folder || xm8 || data || patch || major || minor || build || revision)).ToInt();

		}	// end EmailLogData(bool, bool, bool, bool, bool, bool, bool, bool)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 11)
			{
				return;
			}

			Total +=  lines[01].ToInt();
			None +=   lines[02].ToInt();
			Folder += lines[03].ToInt();
			Xm8 +=    lines[04].ToInt();
			Data +=   lines[05].ToInt();
			Patch +=  lines[06].ToInt();
			Major +=  lines[07].ToInt();
			Minor +=  lines[08].ToInt();
			Build +=  lines[09].ToInt();
			Revis +=  lines[10].ToInt();

		}	// end Add(string []

		///
		public static EmailLogData Empty()
		{
			return new EmailLogData(false, false, false, false, false, false, false, false)
			{
				Total = 0,
				None = 0
			};
		}	// end Empty()

		/// <inheritdoc />
		public int[] ToInts() => new List<int>
		{
			Total,
			None,
			Folder,
			Xm8,
			Data,
			Patch,
			Major,
			Minor,
			Build,
			Revis
		}.ToArray();

		#endregion Methods

	}	// end EmailLogData

}	// end Hub.Logging.Data
