﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ParamDLogData : ILogData
	{
		// ReSharper disable once CommentTypo
		/* The v4 paramd log contains the following data points
		 *
		 * Total times called
		 * Total properties passed in to be deleted
		 */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ParamDLogData(int num)
		{
			Total = 1;
			Num += num;

		}	// end ParamDLogData(int)

		#endregion Constructor

		#region Methods
		
		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 3)
			{
				return;
			}	// end if

			Total += lines[1].ToInt();
			Num += lines[2].ToInt();

		}	// end Add(string[])

		///
		public static ParamDLogData Empty() => new ParamDLogData(0) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Num}.ToArray();

		#endregion Methods

	}	// end ParamDLogData

}	// end Hub.Logging.Data
