﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	///
	public class HelpLogData : ILogData
	{
		/* The help log contains the following data points
		 * 
		 * Total times called
		 * Times general help called (no arguments)
		 * Total commands help needed for */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int General { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal HelpLogData(int num)
		{
			Total = 1;
			General = (num == 0).ToInt();
			Num = num;
		}

		#endregion Constructor

		#region Implementation of ILogData

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 4)
			{
				return;
			}

			Total += lines[1].ToInt();
			General += lines[2].ToInt();
			Num += lines[3].ToInt();
		}

		///
		public static HelpLogData Empty() => new HelpLogData(0) {Total = 0, General = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, General, Num}.ToArray();

		#endregion Implementation of ILogData
	}
}
