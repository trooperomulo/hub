﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class LaunchLogData : ILogData
	{
		/* The launch log contains the following data points
		 * 
		 * Total times called
		 * Times called with the trickler flag
		 * Times called with the test flag
		 * Total times launched without building
		 * Total solutions built prior to launch */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Trick { get; private set; }
		public int Test { get; private set; }
		public int None { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal LaunchLogData(bool trick, bool test, int num)
		{
			Total = 1;
			Trick = trick.ToInt();
			Test = test.ToInt();
			None = (num == 0).ToInt();
			Num = num;
		}

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 6)
			{
				return;
			}

			Total += lines[1].ToInt();
			Trick += lines[2].ToInt();
			Test += lines[3].ToInt();
			None += lines[4].ToInt();
			Num += lines[5].ToInt();
		}

		///
		public static LaunchLogData Empty() => new LaunchLogData(false, false, 0) {Total = 0, None = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Trick, Test, None, Num}.ToArray();

		#endregion Methods
	}
}
