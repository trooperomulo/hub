﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ReconLogData : ILogData
	{
		/* The recon log contains the following data points
		 * 
		 * Total times called
		 * Times called with the xm8 flag
		 * Times called with the trickler flag
		 * Times launched without any registry files
		 * Total files run after reconfigure */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int Xm8 { get; private set; }
		public int Trick { get; private set; }
		public int None { get; private set; }
		public int Num { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ReconLogData(bool xm8, bool trk, int num)
		{
			Total = 1;
			Xm8 = xm8.ToInt();
			Trick = trk.ToInt();
			None = (num == 0).ToInt();
			Num = num;
		}

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 6)
			{
				return;
			}

			Total += lines[1].ToInt();
			Xm8 += lines[2].ToInt();
			Trick += lines[3].ToInt();
			None += lines[4].ToInt();
			Num += lines[5].ToInt();
		}

		///
		public static ReconLogData Empty() => new ReconLogData(false, false, 0) {Total = 0, None = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, Xm8, Trick, None, Num}.ToArray();

		#endregion Methods
	}
}
