﻿using System.Collections.Generic;

using Hub.Interface;

namespace Hub.Logging.Data
{
	/// <inheritdoc />
	public class ParamLogData : ILogData
	{
		/* The v4 param log contains the following data points
		 *
		 * Total times called
		 * Times called with no action
		 * Times called with no properties to add
		 * Times called with bad add pairs
		 * Times called with no properties to delete
		 * Times called with no propertied to modify
		 * Times called with bad modify pairs
		 */

		#region Properties

#pragma warning disable 1591
		public int Total { get; private set; }
		public int NoAct { get; private set; }
		public int NoAdd { get; private set; }
		public int BadAdd { get; private set; }
		public int NoDel { get; private set; }
		public int NoMod { get; private set; }
		public int BadMod { get; private set; }
#pragma warning restore 1591

		#endregion Properties

		#region Constructor

		internal ParamLogData(bool noAct, bool noAdd, bool badAdd, bool noDel, bool noMod, bool badMod)
		{
			Total = 1;
			NoAct = noAct.ToInt();
			NoAdd = noAdd.ToInt();
			BadAdd = badAdd.ToInt();
			NoDel = noDel.ToInt();
			NoMod = noMod.ToInt();
			BadMod = badMod.ToInt();

		}	// end ParamLogData(bool, bool, bool, bool, bool, bool)

		#endregion Constructor

		#region Methods

		/// <inheritdoc />
		public void Add(string[] lines)
		{
			if (lines.Length != 8)
			{
				return;
			}	// end if

			Total += lines[1].ToInt();
			NoAct += lines[2].ToInt();
			NoAdd += lines[3].ToInt();
			BadAdd += lines[4].ToInt();
			NoDel += lines[5].ToInt();
			NoMod += lines[6].ToInt();
			BadMod += lines[7].ToInt();

		}	// end Add(string[])

		///
		public static ParamLogData Empty() => new ParamLogData(false, false, false, false, false, false) {Total = 0};

		/// <inheritdoc />
		public int[] ToInts() => new List<int> {Total, NoAct, NoAdd, BadAdd, NoDel, NoMod, BadMod}.ToArray();

		#endregion Methods

	}	// end ParamLogData

}	// end Hub.Logging.Data
