﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub.Interface;

namespace Hub.Logging
{
	/// <summary>
	/// base class for any log that's just the total
	/// </summary>
	public abstract class LogJustTotalFile : LogFile<EhData>
	{
		#region Members
		
		private int _count;

		///
		protected abstract string Name { get; }

		#endregion Members

		#region Overrides of LogFile

		/// <inheritdoc />
		public override void Log() => LogJustTimesCalled();

		/// <inheritdoc />
		protected override string LogPath => GetPath(Name);

		#endregion Overrides of LogFile

		#region Constructors

		///
		protected LogJustTotalFile() => _count = 1;

		///
		protected LogJustTotalFile(string tempPath) => LoadCountFromFile(tempPath);

		#endregion Constructors

		#region Methods

		private void LoadCountFromFile(string tempPath)
		{
			var files = IoCContainer.Instance.Resolve<IFiles>();
			var lines = files.ReadLines(tempPath).ToArray();
			files.Delete(tempPath);
			UpdateVersion(ref lines);
			_count = lines[1].ToInt(1);
		}

		private void LogJustTimesCalled()
		{
			var nTot = 0;
			if (IoCContainer.Instance.Resolve<IFiles>().Exists(LogPath))
			{
				var lines = GetLines(LogPath);
				UpdateVersion(ref lines);
				nTot = lines[1].ToInt();
			}

			nTot += _count;

			WriteFile(nTot);
		}

		#endregion Methods
	}

	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	// ReSharper disable once ClassNeverInstantiated.Global
	public class EhData : ILogData
	{
		#region Implementation of ILogData

		/// <inheritdoc />
		public void Add(string[] lines) { }

		/// <inheritdoc />
		public int[] ToInts() => null;

		#endregion Implementation of ILogData
	}
}
