﻿When you update the current logging file version, you will need to:
 1- Update LogFile.Current
 2- Update all applicable 'override void UpdateVersion' to handle the new version
 3- Update LoggingTestBase.V
 4- Create a new LoggingFilesStub for the new version
 5- Create a new TestingScrumFilesStub for the new version
 6- Update LoggingTestBase.TestMultipleCalls to use the new logging files stubs
 7- Create a new TestV method in LoggingTestBase and add it to GetTests()
 8- For any logs that now have a different number of lines, update the following in {Command}LogTests
	  i- The start for TestMultipleCalls will need to be updated
	 ii- GetTestList will need to be updated to return the proper number of lines depending on the version
	iii- GetExpected will need to be updated to account for the changes to start
		a- ResTLogTests and UnitTestLogTests are examples of lines being added
		b- EmailLogTests is an example of lines being removed
		c- OrphanLogTests is an example of a line changing what it records
 9- Update MovingLogTests
	 i- update the GetLines{Log} method for the modified logs
	ii- update the GetLines<int> method

NOTE - if when running the unit tests, you get an error where two differnt indexes are consistantly returning
	the wrong value (i.e. at [2] expected 4 but was 6 and at [6] expected 5 but was 3), likely the insert lines
	is inserting at the wrong part of the array