﻿using System;
using System.Collections.Generic;
using System.Linq;

using Hub.Interface;

// ReSharper disable VirtualMemberNeverOverridden.Global
namespace Hub.Logging
{
	/// <summary>
	/// base class for log files
	/// </summary>
	public abstract class LogFile<T> : ILogFile
		where T : ILogData
	{
		#region Constants

		///
		public const int Current = 4;
		// ReSharper disable once StaticMemberInGenericType
		// ReSharper disable once InconsistentNaming
		private static readonly string CurrentVersion = $"v{Current}";

		#endregion Constants

		#region Variables

		///
		protected T Data;
		private readonly IFiles _files;

		#endregion Variables

		#region Constructor

		///
		protected LogFile() => _files = IoCContainer.Instance.Resolve<IFiles>();

		#endregion Constructor

		#region Abstract Member

		/// <summary>
		/// the path for this instance
		/// </summary>
		protected abstract string LogPath { get; }

		#endregion Abstract Member

		#region Virtual Members

		/// <inheritdoc />
		public virtual void Log()
		{
			if (_files.Exists(LogPath))
			{
				var lines = GetLines(LogPath);
				UpdateVersion(ref lines);
				Data.Add(lines);
			}	// end if

			WriteFile(Data.ToInts());

		}	// end Log()

		///
		protected virtual void UpdateVersion(int oldVersion, ref string[] lines)
		{
			if (oldVersion == Current)
			{
				return;
			}	// end if

			if (oldVersion == 0)
			{
				InsertVersion(ref lines);
				UpdateVersion(1, ref lines);
				return;
			}	// end if

			if (oldVersion < Current)
			{
				UpdateVersion(oldVersion + 1, ref lines);
				return;
			}	// end if

			if (oldVersion > Current)
			{
				throw TooBig(oldVersion);
			}	// end if

		}	// end UpdateVersion(int, ref string [])

		#endregion Virtual Members

		#region Methods

		///
		protected string[] GetLines(string path) => _files.ReadLines(path).ToArray();

		///
		protected string GetPath(string name) => Logger.GetPath(name);

		private static int GetVersion(string firstLine) => firstLine.StartsWith("v") ? firstLine.Substring(1).ToInt() : 0;

		/// <summary>
		/// inserts the given number of lines with 0 in the given location to the given lines
		/// </summary>
		/// <param name="lines">the lines to add the 0s to</param>
		/// <param name="where">where in the list to add the 0s</param>
		/// <param name="num">how many 0s to add</param>
		protected static void InsertLines(ref string[] lines, int where, int num)
		{
			var list = lines.ToList();

			for (var i = 0; i < num; i++)
			{
				list.Insert(where, "0");
			}
			
			lines = list.ToArray();

		}	// end InsertLines(ref string[], int, int)
		
		///
		protected static void InsertVersion(ref string[] lines)
		{
			var list = lines.ToList();
			list.Insert(0, "v1");
			lines = list.ToArray();

		}	// end InsertVersion(ref string[])

		///
		public virtual void LoadFromLogFile(string tempPath, T data)
		{
			Data = data;
			var lines = _files.ReadLines(tempPath).ToArray();
			_files.Delete(tempPath);
			UpdateVersion(ref lines);
			Data.Add(lines);

		}	// end LoadFromLogFile(string, T)
		
		private static List<string> ToLines(params int[] ints) => ints.Select(i => i.ToString()).ToList();

		private static Exception TooBig(int oldVersion)
		{
			// ReSharper disable once NotResolvedInText
			return new ArgumentOutOfRangeException("file version",
				$"this build only supports log files up to version {Current} but found a file with version {oldVersion}");

		}	// end TooBig(int)

		/// <summary>
		/// removes lines at the given indexes
		/// </summary>
		/// <param name="lines">array of lines that needs items removed from</param>
		/// <param name="toRemove">the indexes of the items to be removed</param>
		protected static void RemoveLines(ref string[] lines, params int[] toRemove)
		{
			var removeList = toRemove.ToList();
			// make sure the indexes are sorted from largest to smallest
			removeList.Sort();
			removeList.Reverse();

			var list = lines.ToList();

			foreach (var remove in removeList)
			{
				list.RemoveAt(remove);
			}

			lines = list.ToArray();

		}	// end RemoveLines(ref string[], params int[])

		///
		protected void UpdateVersion(ref string[] lines) => UpdateVersion(GetVersion(lines[0]), ref lines);

		///
		protected void WriteFile(params int[] ints) => WriteFile(ToLines(ints));

		///
		protected void WriteFile(List<string> lines)
		{
			lines.Insert(0, CurrentVersion);
			_files.WriteAllLines(LogPath, lines);

		}	//end WriteFile(List<string>)

		#endregion Methods

	}	// end LogFile<T>

}	// end Hub.Logging
