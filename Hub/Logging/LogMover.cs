﻿using Hub.Interface;
using Hub.Logging.Logs;

namespace Hub.Logging
{
	/// <summary>
	/// class that handles moving logs from the temp location to the base location
	/// </summary>
	public class LogMover : ILogMover
	{
		/// <inheritdoc />
		public void MoveLog(string logName, string tempPath)
		{
			ILogFile log;
			switch (logName)
			{
				case Logger.AutoLog:
					log = new AutoLogLog(tempPath);
					break;
				case Logger.Build:
					log = new BuildLog(tempPath);
					break;
				case Logger.Clean:
					log = new CleanLog(tempPath);
					break;
				case Logger.Db:
					log = new DbLog(tempPath);
					break;
				case Logger.Email:
					log = new EmailLog(tempPath);
					break;
				case Logger.Go:
					log = new GoLog(tempPath);
					break;
				case Logger.Help:
					log = new HelpLog(tempPath);
					break;
				case Logger.Import:
					log = new ImportLog(tempPath);
					break;
				case Logger.Kill:
					log = new KillLog(tempPath);
					break;
				case Logger.Launch:
					log = new LaunchLog(tempPath);
					break;
				case Logger.Oew:
					log = new OewLog(tempPath);
					break;
				case Logger.Orphan:
					log = new OrphanLog(tempPath);
					break;
				case Logger.Param:
					log = new ParamLog(tempPath);
					break;
				case Logger.ParamA:
					log = new ParamALog(tempPath);
					break;
				case Logger.ParamD:
					log = new ParamDLog(tempPath);
					break;
				case Logger.ParamE:
					log = new ParamELog(tempPath);
					break;
				case Logger.ParamM:
					log = new ParamMLog(tempPath);
					break;
				case Logger.ParamS:
					log = new ParamSLog(tempPath);
					break;
				case Logger.Recon:
					log = new ReconLog(tempPath);
					break;
				case Logger.Res:
					log = new ResLog(tempPath);
					break;
				case Logger.ResA:
					log = new ResALog(tempPath);
					break;
				case Logger.ResF:
					log = new ResFLog(tempPath);
					break;
				case Logger.ResR:
					log = new ResRLog(tempPath);
					break;
				case Logger.ResS:
					log = new ResSLog(tempPath);
					break;
				case Logger.ResT:
					log = new ResTLog(tempPath);
					break;
				case Logger.Rfb:
					log = new RfbLog(tempPath);
					break;
				case Logger.Scrum:
					log = new ScrumLog(tempPath);
					break;
				case Logger.TeamCity:
					log = new TeamCityLog(tempPath);
					break;
				case Logger.Trickler:
					log = new TricklerLog(tempPath);
					break;
				case Logger.UnitTest:
					log = new UnitTestLog(tempPath);
					break;
				case Logger.Vs:
					log = new VsLog(tempPath);
					break;
				default:
					Out.Error($"I don't know what to do with a log file titled '{logName}'");
					return;
			}

			log.Log();
		}
	}
}
