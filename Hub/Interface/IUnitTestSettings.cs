﻿using System.Collections.Generic;

using Hub.Enum;

namespace Hub.Interface
{
	/// <summary>
	/// holds the settings for the unit test command
	/// </summary>
	public interface IUnitTestSettings
	{
		/// <summary>
		/// should an estimate of how long it will take be shown
		/// </summary>
		bool EstimateTime { get; }

		/// <summary>
		/// what progress should be shown
		/// </summary>
		Progress Progress { get; }

		/// <summary>
		/// should the time taken be shown
		/// </summary>
		bool ShowTime { get; }

		/// <summary>
		/// should the results be shown in the console
		/// </summary>
		bool ResultsConsole { get; }

		/// <summary>
		/// should the results be shown via the text file
		/// </summary>
		bool ResultsTextFile { get; }

		/// <summary>
		/// should the results be shown via an xml file
		/// </summary>
		bool ResultsXmlFile { get; }

		/// <summary>
		/// an input file to be compared against
		/// </summary>
		// ReSharper disable once ConstantNullCoalescingCondition - for some reason Resharper thinks that UserInputFile is never null
		int InputFile { get; }

		///
		void LoadArguments(List<string> args);
	}
}
