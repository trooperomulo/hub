﻿using System.Collections.Generic;

namespace Hub.Interface
{
	/// <summary>
	/// wait for user input prior to proceeding
	/// </summary>
	public interface IWaitForUser
	{
		/// <summary>
		/// prints the options, then a warning to the user about how long they have to answer, waits for a valid answer
		/// </summary>
		/// <param name="explanation">the lines of explanation about the options</param>
		/// <param name="options">the options available to select</param>
		/// <param name="question">an optional question</param>
		/// <param name="warningNoEntry">the warning to display to the user if there hasn't been an entry yet, i.e., Quitting in</param>
		/// <param name="warningEntry">the warning to display to the user after there has been an entry, i.e., Quitting in</param>
		/// <returns>a list of zero base indexes of which options were selected</returns>
		List<int> MultipleSelectionEnterNotRequired(List<string> explanation, List<string> options, string question,
			string warningNoEntry, string warningEntry);

		/// <summary>
		/// prints the options, then a warning to the user about how long they have to answer, waits for a valid answer
		/// </summary>
		/// <param name="explanation">the lines of explanation about the options</param>
		/// <param name="options">the options available to select</param>
		/// <param name="question">an optional question</param>
		/// <param name="warningNoEntry">the warning to display to the user if there hasn't been an entry yet, i.e., Quitting in</param>
		/// <param name="warningEntry">the warning to display to the user after there has been an entry, i.e., Quitting in</param>
		/// <param name="maxWait">max amount of time to wait for a response</param>
		/// <returns>a list of zero base indexes of which options were selected</returns>
		List<int> MultipleSelectionEnterNotRequired(List<string> explanation, List<string> options, string question,
			string warningNoEntry, string warningEntry, int maxWait);

		/// <summary>
		/// prints the explanation and options, then a warning to the user about how long they have to answer, waits for a valid answer
		/// </summary>
		/// <param name="explanation">the lines of explanation about the options</param>
		/// <param name="options">the options the user wil be picking from</param>
		/// <param name="question">an optional question</param>
		/// <param name="warning">the warning to display to the user, i.e., Quitting in</param>
		/// <returns>the zero based index of which option was selected</returns>
		int SingleSelection(List<string> explanation, List<string> options, string question, string warning);

		/// <summary>
		/// prints the explanation and options, then a warning to the user about how long they have to answer, waits for a valid answer
		/// </summary>
		/// <param name="explanation">the lines of explanation about the options</param>
		/// <param name="options">the options the user wil be picking from</param>
		/// <param name="question">an optional question</param>
		/// <param name="warning">the warning to display to the user, i.e., Quitting in</param>
		/// <param name="maxWait">max amount of time to wait for a response</param>
		/// <returns>the zero based index of which option was selected</returns>
		int SingleSelection(List<string> explanation, List<string> options, string question, string warning, int maxWait);
	}
}
