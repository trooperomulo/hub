﻿using Args = System.Collections.Generic.List<string>;

namespace Hub.Interface
{
	/// <summary>
	/// interface for any command that can be invoked
	/// </summary>
	public interface ICommand : IInput
	{
		/// <summary>
		/// a short description of the command
		/// </summary>
		string Description { get; }

		/// <summary>
		/// does this command have sub commands
		/// </summary>
		bool HasSubCommands { get; }
		
		/// <summary>
		/// does this command need shortcuts to have been defined
		/// </summary>
		bool NeedShortcut { get; }

		/// <summary>
		/// the help describing this command
		/// </summary>
		void Help(Args args);

		/// <summary>
		/// performs that actual command
		/// </summary>
		void Perform(Args args);
	}
}
