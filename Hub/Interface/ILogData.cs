﻿namespace Hub.Interface
{
	///
	public interface ILogData
	{
		///
		void Add(string[] lines);

		///
		int[] ToInts();
	}
}
