﻿using System.Diagnostics;

using Hub.Enum;

namespace Hub.Interface
{
	/// <summary>
	/// interface for launching programs and processes
	/// </summary>
	public interface IProgram
	{
		/// <summary>
		/// create a <see cref="ProcessStartInfo" /> object with the given values
		/// </summary>
		/// <param name="path">the path to the process to start</param>
		/// <param name="arguments">the arguments to pass to the process</param>
		/// <param name="rso">should we redirect the standard output</param>
		/// <param name="rse">should we redirect the standard error</param>
		ProcessStartInfo GetStartInfo(string path, string arguments, bool rso = true, bool rse = false);

		/// <summary>
		/// lanuch a program from the bin folder
		/// </summary>
		/// <param name="filename">the name of the file</param>
		/// <param name="label">what to output to the user</param>
		/// <param name="arguments">the arguments to pass to the program</param>
		/// <param name="wait">should we wait for the program to exit</param>
		/// <param name="show">should we show the program's output</param>
		void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true,
			ShowOutput show = ShowOutput.No);

		/// <summary>
		/// run a batch file
		/// </summary>
		/// <param name="fileAndArgs">the batch name with its arguments</param>
		/// <param name="show">should we show the batch file's output</param>
		/// <param name="rso">should we redirect the standard output</param>
		/// <param name="rse">should we redirect the standard error</param>
		/// <param name="wait">should we wait for the process to exit on its own</param>
		/// <param name="outputHandler"></param>
		/// <returns>if we ended without any errors</returns>
		bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true, bool rse = false,
			bool wait = true, DataReceivedEventHandler outputHandler = null);

		/// <summary>
		/// run an external program
		/// </summary>
		/// <param name="path">the path to the external program</param>
		/// <param name="arguments">the arguments to pass to the external program</param>
		/// <param name="show">should we show any output</param>
		/// <param name="rso">should we redirect the standard output</param>
		/// <param name="rse">should we redirect the standard error</param>
		/// <param name="wait">should we wait for the process to exit on its own</param>
		/// <param name="outputHandler">event handler to process output</param>
		/// <returns>if we ended without any errors</returns>
		bool RunProgram(string path, string arguments = null, ShowOutput show = ShowOutput.No, bool rso = true,
			bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null);

		/// <summary>
		/// kills all processes with the given name
		/// </summary>
		void KillProcess(string name);

		/// <summary>
		/// starts a process using the given file
		/// </summary>
		void StartProcess(string filename);
	}
}
