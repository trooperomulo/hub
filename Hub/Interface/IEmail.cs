﻿namespace Hub.Interface
{
	/// <summary>
	/// interface for dealing with email
	/// </summary>
	public interface IEmail
	{
		/// <summary>
		/// displays an email
		/// </summary>
		void DisplayEmail(string to, string subject, string body);
	}
}
