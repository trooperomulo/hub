﻿namespace Hub.Interface
{
	///
	public interface ISubCommand
	{
		/// <summary>
		/// a short description of the command
		/// </summary>
		string Description { get; }

		/// <summary>
		///  the input for this command
		/// </summary>
		string Input { get; }

		///
		void Help();

	}	// end ISubCommand

}	// end Hub.Interface
