﻿using System.Collections.Generic;

namespace Hub.Interface
{
	/// <summary>
	/// holds the user specific data, like the name for the scrum file
	/// </summary>
	public interface IUserParams
	{
		#region Properties

		/// <summary>
		/// when we auto generate a key for a resx file, what length should we start at, (in other code 18 is used)
		/// </summary>
		int AutoGeneratedKeyStartLength { get; }

		/// <summary>
		/// a string to define what colors the solution colorizer should use
		/// </summary>
		string Colors { get; }

		/// <summary>
		/// the name of the folder the data repo is checked out to
		/// </summary>
		string DataFolder { get; }

		/// <summary>
		/// the branch to use when debugging from Visual Studio
		/// </summary>
		// ReSharper disable once StringLiteralTypo
		string DebugBranch { get; }

		/// <summary>
		/// the default user to use when importing 28 estimates
		/// </summary>
		// ReSharper disable once StringLiteralTypo
		string DefaultUser { get; }

		/// <summary>
		/// should we be logging the usages of the different commands
		/// </summary>
		bool LogUsages { get; }

		/// <summary>
		/// this user's name for the scrum file
		/// </summary>
		string Name { get; }

		/// <summary>
		/// the path to the file with the shortcut definitions
		/// </summary>
		string Path { get; }

		/// <summary>
		/// should we prefer the current branch over the branch with the same name as the xm8 branch
		/// </summary>
		// ReSharper disable once RedundantDefaultMemberInitializer
		bool PreferCurrent { get; }

		/// <summary>
		/// how long to wait when waiting for the user to respond
		/// </summary>
		int PromptWait { get; }

		/// <summary>
		/// should the scrum file display hours logged
		/// </summary>
		bool ScrumHours { get; }

		/// <summary>
		/// where the user's scrum file should be put (don't use a mapped drive, use the network address)
		/// </summary>
		string ScrumLocation { get; }

		/// <summary>
		/// the name of the file with the shortcut definitions
		/// </summary>
		string ShortcutFile { get; }

		/// <summary>
		/// should the name of the remote be included in the title
		/// </summary>
		// ReSharper disable once RedundantDefaultMemberInitializer
		bool ShowRemote { get; }

		/// <summary>
		/// should resx files automatically be sorted after building
		/// </summary>
		bool SortResx { get; }

		/// <summary>
		/// Format of the title bar, the parameters that will be passed in are:
		/// 
		/// 0- isAdministrator ? "Administrator:  " : ""
		/// 1- Repo
		/// 2- branch name
		/// 3- mode
		/// </summary>
		string TitleFormat { get; }

		/// <summary>
		/// should we keep track of how long each solution takes to run its unit tests
		/// </summary>
		bool TrackUnitTestTime { get; }

		/// <summary>
		/// the default parameters used when running unit tests, can be overriden in line
		/// </summary>
		string UnitTestDefaults { get; }

		/// <summary>
		/// should repo (the branch number) be a part of the root path
		/// </summary>
		bool UseRepo { get; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// gets the default value for the given property
		/// </summary>
		string GetDefaultValue(string property);

		/// <summary>
		/// gets all the properties that aren't set in the user's file
		/// </summary>
		List<string> GetProperties();

		/// <summary>
		/// get the value of the given property
		/// </summary>
		string GetPropertyValue(string property);

		/// <summary>
		/// Indicates if the property has been set
		/// </summary>
		bool HasPropertyBeenSet(string property);

		/// <summary>
		/// modifies an already set property
		/// </summary>
		void ModifyProperty(string property, string value);

		/// <summary>
		/// saves the file
		/// </summary>
		void Save();

		/// <summary>
		/// set the given property with the given value
		/// </summary>
		void SetProperty(string property, string value, bool canOverride);

		/// <summary>
		/// restore the given property back to the default value
		/// </summary>
		void UnsetProperty(string property);

		#endregion Methods

	}	// end IUserParams

}	// end Hub.Interface
