﻿namespace Hub.Interface
{
	/// <summary>
	/// interface for making sure a solution is colored properly
	/// </summary>
	public interface ISolutionColorizer
	{
		/// <summary>
		/// make sure the solution at the given path has the appropriate color set
		/// </summary>
		/// <param name="colors">the color settings</param>
		/// <param name="path">the full path to the solution (ex. C:\git\1\xactimate\xm8\xm8\Asm\MP\ClaimManager\Xm8.MP.ClaimManager.sln</param>
		/// <param name="shortcut">the solution shortcut</param>
		void ColorizeSolution(string colors, string path, string shortcut);

	}// end ISolutionColorizer

}// end Hub.Interface
