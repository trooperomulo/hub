﻿namespace Hub.Interface
{
	///
	public interface ILogMover
	{
		/// <summary>
		/// merges the log from the temp location to the permanant
		/// </summary>
		void MoveLog(string logName, string tempPath);
	}
}
