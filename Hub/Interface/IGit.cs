﻿using System.Collections.Generic;

namespace Hub.Interface
{
	/// <summary>
	/// interface for performing git actions
	/// </summary>
	public interface IGit
	{
		/// <summary>
		/// get the name of the currently checked out branch on the repo
		/// </summary>
		/// <returns></returns>
		string GetBranchName();
		
		/// <summary>
		/// gets all of the branches of the repo
		/// </summary>
		/// <param name="remote">should we get remote branches, or local</param>
		List<string> GetBranches(bool remote);

		/// <summary>
		/// get the name of the remote the branch was checked out from
		/// </summary>
		string GetRemoteName();

		/// <summary>
		/// is the current branch for next gen
		/// </summary>
		bool IsNextGen();
	}
}
