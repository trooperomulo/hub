﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Hub.Interface
{
	/// <summary>
	/// interface for interactions with files
	/// </summary>
	public interface IFiles
	{
		/// <summary>
		/// Appends lines to a file, and then closes the file. If the specified file does not exist, this method creates a file, writes the specified lines to the file, and then closes the file
		/// </summary>
		void AppendAllLines(string path, List<string> lines);

		/// <summary>
		/// Copies an existing file to a new file.
		/// </summary>
		void Copy(string source, string destination);

		/// <summary>
		/// Deletes the specified file
		/// </summary>
		void Delete(string path);
		
		/// <summary>
		/// Determines whether the specified file exists
		/// </summary>
		bool Exists(string path);

		/// <summary>
		/// Determines when the last time the file at the given path was writen to
		/// </summary>
		DateTime GetLastWriteTime(string path);

		/// <summary>
		/// Get a file stream to read from the specified path
		/// </summary>
		Stream GetStream(string path, FileMode fileMode);

		/// <summary>
		/// reads all the text in the given file and returns it as one string
		/// </summary>
		string ReadAllText(string path);

		/// <summary>
		/// Reads the lines of a file
		/// </summary>
		IEnumerable<string> ReadLines(string path);

		/// <summary>
		/// Creates a new file, writes one or more strings to the file, and then closes the file
		/// </summary>
		void WriteAllLines(string path, IEnumerable<string> lines);

		/// <summary>
		/// writes the given xml string to the given path
		/// </summary>
		/// <param name="path">where to write the xml</param>
		/// <param name="xml">the contents</param>
		void WriteXml(string path, string xml);
	}
}
