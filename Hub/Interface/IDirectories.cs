﻿using System.Collections.Generic;
using System.IO;

namespace Hub.Interface
{
	/// <summary>
	/// interface for interacting with directories
	/// </summary>
	public interface IDirectories
	{
		/// <summary>
		/// Creates all the directories in a specified path
		/// </summary>
		void CreateDirectory(string path);

		/// <summary>
		/// Deletes a specified directory and any subdirectories
		/// </summary>
		void Delete(string path);

		/// <summary>
		/// Determines whether the given path refers to an existing directory on disk
		/// </summary>
		bool Exists(string path);

		/// <summary>
		/// Gets the current working directory of the application
		/// </summary>
		string GetCurrentDirectory();

		/// <summary>
		/// Returns an array of directories in the given root matching the given search criteria
		/// </summary>
		/// <param name="root">the directory to search in</param>
		/// <param name="searchPattern">
		/// The search string to match against the names of directories. This parameter can contain a combination
		/// of valid literal path and wildcard (* and ?) characters, but it doesn't support regular expressions.
		/// </param>
		DirectoryInfo[] GetDirectories(string root, string searchPattern);

		/// <summary>
		/// Returns the names of files (including their paths) in the specified directory.
		/// </summary>
		List<string> GetFiles(string path);

		/// <summary>
		/// Returns the names of files that meet specified criteria
		/// </summary>
		List<string> GetFiles(string path, string filter);

		/// <summary>
		/// Returns the names of files that meet the specified criteria
		/// </summary>
		List<string> GetFiles(string path, string filter, SearchOption option);

		/// <summary>
		/// Returns a file list from the given path matching the given search pattern
		/// </summary>
		/// <param name="root">the directory to search from</param>
		/// <param name="filter">the search string to match agains the names of files</param>
		/// <returns></returns>
		FileInfo[] GetFileInfos(string root, string filter);

		/// <summary>
		/// Returns the most recently modified file in the given directory
		/// </summary>
		FileInfo GetMostRecentlyChangedFile(string dirPath, string filter);

		/// <summary>
		/// Sets the application's current working directory to the specified directory
		/// </summary>
		void SetCurrentDirectory(string path);
	}
}
