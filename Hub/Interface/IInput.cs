﻿using System.Collections.Generic;

namespace Hub.Interface
{
	///
	public interface IInput
	{
		/// <summary>
		/// the inputs that can invoke this command
		/// </summary>
		List<string> Inputs { get; }
	}
}
