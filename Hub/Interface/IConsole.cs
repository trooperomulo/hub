﻿using System;

namespace Hub.Interface
{
	/// <summary>
	/// used for interacting with the system console
	/// </summary>
	public interface IConsole
	{
		/// <summary>
		///Gets or sets the background color of the console.
		/// </summary>
		ConsoleColor BackgroundColor { get; set; }

		/// <summary>
		/// Gets or sets the row position of the cursor within the buffer area.
		/// </summary>
		int CursorTop { get; set; }

		/// <summary>
		/// Gets or sets the foreground color of the console.
		/// </summary>
		ConsoleColor ForegroundColor { get; set; }

		/// <summary>
		/// Gets a value indicating whether a key press is available in the input stream.
		/// </summary>
		bool KeyAvailable { get; }

		/// <summary>
		/// Gets or sets the title to display in the console title bar.
		/// </summary>
		string Title { get; set; }

		/// <summary>
		/// Gets the width of the console
		/// </summary>
		int Width { get; }

		/// <summary>
		/// Plays the sound of a beep through the console speaker.
		/// </summary>
		void Beep();

		/// <summary>
		/// Obtains the next character or function key pressed by the user.The pressed key is displayed in the console window.
		/// </summary>
		char ReadKey();

		/// <summary>
		/// Sets the position of the cursor.
		/// </summary>
		void SetCursorPosition(int left, int top);

		/// <summary>
		/// Writes the specified string value to the standard output stream.
		/// </summary>
		void Write(string line);

		/// <summary>
		/// Writes the current line terminator to the standard output stream.
		/// </summary>
		void WriteLine();

		/// <summary>
		/// Writes the specified string value, followed by the current line terminator, to the standard output stream.
		/// </summary>
		void WriteLine(string line);
	}
}
