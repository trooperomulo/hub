﻿namespace Hub.Interface
{
	/// <summary>
	/// closes any popups automatically
	/// </summary>
	public interface IPopupCloser
	{
		/// <summary>
		/// start listening for popups to close
		/// </summary>
		void StartProcess();

		/// <summary>
		/// stop listening for popups to close
		/// </summary>
		void StopProcess();
	}
}
