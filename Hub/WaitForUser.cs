﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// class that handles waiting for user input
	/// </summary>
	public class WaitForUser : IWaitForUser
	{
		#region Enum

		private enum OptionType
		{
			Number,
			Letter
		}	// end OptionType

		#endregion Enum

		#region Delegate

		private delegate bool CheckResponse<T>(char ch, out T val);

		#endregion Delegate

		#region Constants

		private const int NoEntry = -1;
		private const int Enter = -2;
		private const int All = -3;
		private const int None = -4;

		///
		public const char AllNumA = 'A';
		///
		public const char AllNumB = 'a';
		///
		public const char AllLet = '*';
		///
		public const char NoneNumA = 'N';
		///
		public const char NoneNumB = 'n';
		///
		public const char NoneLet = '/';

		///
		public const int NumToUpLet = NumToLoLet - 32;
		///
		public const int NumToLoLet = 97;

		#endregion Constants

		#region Variables

		private readonly Action<int> _sleeper;

		private readonly int _promptWait;

		private OptionType _optionType;

		#endregion Variables

		#region Constructor

		///
		public WaitForUser(int promptWait, Action<int> sleeper)
		{
			_promptWait = promptWait;
			_sleeper = sleeper;
		}

		#endregion Constructor

		#region Public Methods

		/// <inheritdoc />
		public List<int> MultipleSelectionEnterNotRequired(List<string> explanation, List<string> options, string question,
			string warningNoEntry, string warningEntry)
		{
			return MultipleSelectionEnterNotRequired(explanation, options, question, warningNoEntry, warningEntry, _promptWait);
		}
		
		/// <inheritdoc />
		public List<int> MultipleSelectionEnterNotRequired(List<string> explanation, List<string> options, string question,
			string warningNoEntry, string warningEntry, int maxWait)
		{
			PrintExplanation(explanation);

			var answer = 0;
			var selected = new List<int>();
			var optsCount = options.Count;
			while (answer > NoEntry)
			{
				PrintOptions(options, selected, true);
				PrintQuestion(question);
				answer = GetAllNoneResponse(optsCount, selected.Count == 0 ? warningNoEntry : warningEntry, true, ref maxWait);
				switch (answer)
				{
					case All:
						selected.SelectAll(optsCount);
						answer = 0;
						break;
					case None:
						selected.Clear();
						answer = 0;
						break;
					case NoEntry:
					case Enter:
						continue;
					default:
						selected.Flip(answer);
						break;
				}
				ResetCursor(optsCount, !question.IsNullOrEmpty(), true);
			}
			// do a loop printing the options, question and warning
			// only stop if the user enters a valid response or enter, or if time expires

			return selected;
		}
		
		/// <inheritdoc />
		public int SingleSelection(List<string> explanation, List<string> options, string question, string warning)
		{
			return SingleSelection(explanation, options, question, warning, _promptWait);
		}
		
		/// <inheritdoc />
		public int SingleSelection(List<string> explanation, List<string> options, string question, string warning, int maxWait)
		{
			PrintAll(explanation, options, question, null);

			var optionsCount = options.Count;
			
			return GetNonAllNoneResponse(optionsCount, warning, false, ref maxWait);

		}	// end SingleSelection(List<string>, List<string>, string, string, int)

		#endregion Public Methods

		#region Private Methods
		
		#region Print Options

		private void PrintOptions(List<string> options, List<int> selected, bool includeAllNone)
		{
			SetOptionType(options.Count);
			char all = '=';
			char none = '-';
			switch (_optionType)
			{
				case OptionType.Number:
					PrintOptionsNumbers(options, selected);
					all = AllNumA;
					none = NoneNumA;
					break;
				case OptionType.Letter:
					PrintOptionsLetters(options, selected);
					all = AllLet;
					none = NoneLet;
					break;

			}	// end switch
			
			if (includeAllNone)
			{
				Out.Blank();
				Out.Option(all, "All", true);
				Out.Option(none, "None", false);
			}	// end if

			// print a blank line
			Out.Blank();

		}	// end PrintOptions(List<string>, List<int>, bool)
		
		private void PrintOptionsLetters(List<string> options, List<int> selected)
		{
			for (int i = 0; i < options.Count; i++)
			{
				var option = options[i];
				var ch = (char) (i + NumToUpLet);
				if (selected == null)
				{
					Out.Option(ch, option);
				}	// end if
				else
				{
					Out.Option(ch, option, selected.Contains(i));
				}	// end else
			}	// end for

		}	// end PrintOptionsLetters(List<string>, List<int>)

		private void PrintOptionsNumbers(List<string> options, List<int> selected)
		{
			for (int i = 0; i < options.Count; i++)
			{
				var option = options[i];
				if (selected == null)
				{
					Out.Option(i, option);
				}	// end if
				else
				{
					Out.Option(i, option, selected.Contains(i));
				}	// end else
			}	// end for

		}	// end PrintOptionsNumbers(List<string>, List<int>)

		#endregion Print Options

		#region Get Response

		private int GetResponse(string warning, int maxWait, CheckResponse<int> checker, int defValue, bool allowEnter, int entValue)
		{
			var cons = IoCContainer.Instance.Resolve<IConsole>();
			var seconds = 1;
			while (true)
			{
				// the \r at the beginning says to start printing at the beginning of the line, not after any text the might already be there
				Out.Write($"\r{warning} in {maxWait + 1 - seconds} seconds    ");
				if (cons.KeyAvailable)
				{
					var ch = cons.ReadKey();
					if (checker(ch, out var val))
					{
						return val;
					}
					if (allowEnter && (ch == '\r' || ch == '\n'))
					{
						Out.Blank();
						return entValue;
					}
				}
				_sleeper(1000);
				if (seconds++ > maxWait)
				{
					return defValue;
				}
			}
		}

		private int GetAllNoneResponse(int optsCount, string warning, bool allowEnter, ref int maxWait)
		{
			var result = NoEntry;

			switch (_optionType)
			{
				case OptionType.Number:
					result = GetIntAllNoneResponse(optsCount, warning, allowEnter, ref maxWait);
					break;
				case OptionType.Letter:
					result = GetAlphaAllNoneResponse(optsCount, warning, allowEnter, ref maxWait);
					break;
			}	// end switch

			return result;

		}	// end GetAllNoneResponse(int, string, bool, ref int)

		private int GetAlphaResponse(int optsCount, string warning, bool allowEnter, ref int maxWait)
		{
			bool Checker(char ch, out int val)
			{
				val = NoEntry;

				if (!char.IsLetter(ch))
				{
					return false;
				}	// end if

				var index = GetNumFromUpperOrLowerLetter(ch);
				if (index > -1 && index <= optsCount - 1)
				{
					Out.Blank();
					val = index;
					return true;
				}	// end if

				return false;

			}	// end Checker(char, out int)

			return GetResponse(warning, maxWait, Checker, NoEntry, allowEnter, Enter);

		}	// end GetAlphaResponse(int, string, bool, ref int)

		private int GetAlphaAllNoneResponse(int optsCount, string warning, bool allowEnter, ref int maxWait)
		{
			bool Checker(char ch, out int val)
			{
				val = NoEntry;

				if (IsAll(ch))
				{
					val = All;
					Out.Blank();
					return true;
				}	// end if

				if (IsNone(ch))
				{
					val = None;
					Out.Blank();
					return true;
				}	// end if

				if (!char.IsLetter(ch))
				{
					return false;
				}	// end if
				
				var index = GetNumFromUpperOrLowerLetter(ch);
				if (index > -1 && index <= optsCount - 1)
				{
					Out.Blank();
					val = index;
					return true;
				}	// end if

				return false;

			}	// end Checker(char, out int)
			
			return GetResponse(warning, maxWait, Checker, NoEntry, allowEnter, Enter);

		}	// end GetAlphaAllNoneResponse(int, string, bool, ref int)

		private int GetIntResponse(int optsCount, string warning, bool allowEnter, ref int maxWait)
		{
			bool Checker(char ch, out int val)
			{
				val = NoEntry;
				if (!char.IsDigit(ch))
				{
					return false;
				}

				var num = int.Parse(ch.ToString());
				if (num > -1 && num <= optsCount - 1)
				{
					Out.Blank();
					val = num;
					return true;
				}

				return false;
			}

			return GetResponse(warning, maxWait, Checker, NoEntry, allowEnter, Enter);
		}

		private int GetIntAllNoneResponse(int optsCount, string warning, bool allowEnter, ref int maxWait)
		{
			bool Checker(char ch, out int val)
			{
				val = NoEntry;
				if (!char.IsLetterOrDigit(ch))
				{
					return false;
				}

				if (char.IsDigit(ch))
				{
					var num = int.Parse(ch.ToString());
					if (num < 0 || num >= optsCount)
					{
						return false;
					}

					Out.Blank();
					val = num;
					return true;
				}
				
				// if it isn't a digit, it has to be a letter at this point
				switch (ch)
				{
					case AllNumA:
					case AllNumB:
						val = All;
						break;
					case NoneNumA:
					case NoneNumB:
						val = None;
						break;
					default:
						return false;
				}

				Out.Blank();
				return true;
			}

			return GetResponse(warning, maxWait, Checker, NoEntry, allowEnter, Enter);
		}
		
		private int GetNonAllNoneResponse(int optsCount, string warning, bool allowEnter, ref int maxWait)
		{
			var result = NoEntry;
			switch (_optionType)
			{
				case OptionType.Number:
					result = GetIntResponse(optsCount, warning, allowEnter, ref maxWait);
					break;
				case OptionType.Letter:
					result = GetAlphaResponse(optsCount, warning, allowEnter, ref maxWait);
					break;
			}	// end switch

			return result;

		}	// end GetAllNoneResponse(int, string, bool, ref int)

		#endregion Get Response

		#region Others

		private static int GetNumFromUpperOrLowerLetter(char ch)
		{
			if (ch >= NumToLoLet)
			{
				return ch - NumToLoLet;
			}	// end if

			return ch - NumToUpLet;

		}	// end GetNumFromUpperOrLowerLetter(char)

		private bool IsAll(char ch) => _optionType == OptionType.Number ? ch == AllNumA || ch == AllNumB : ch == AllLet;

		private bool IsNone(char ch) => _optionType == OptionType.Number ? ch == NoneNumA || ch == NoneNumB : ch == NoneLet;

		private void PrintAll(List<string> explanation, List<string> options, string question, List<int> selected)
		{
			PrintExplanation(explanation);

			PrintOptions(options, selected, false);

			PrintQuestion(question);
		}

		private void PrintExplanation(List<string> explanation)
		{
			if (explanation.Count > 0)
			{
				foreach (var exp in explanation)
				{
					Out.Line(exp);
				}

				// print a blank line
				Out.Blank();
			}
		}

		private void PrintQuestion(string question)
		{
			if (!question.IsNullOrEmpty())
			{
				Out.Line(question);
				Out.Blank();
			}
		}

		[ExcludeFromCodeCoverage]
		private void ResetCursor(int optsCount, bool hasQuestion, bool includeAllNone)
		{
			var lines = optsCount + 1;  // add one for the blank line printed after the options

			if (hasQuestion)
			{
				lines += 2; // one for the question, one for the blank line after
			}

			if (includeAllNone)
			{
				lines += 3; // two for the all and none, one for the blank line after
			}

			lines++;    // for the warning
			var cons = IoCContainer.Instance.Resolve<IConsole>();
			cons.SetCursorPosition(0, cons.CursorTop - lines);
		}

		private void SetOptionType(int optionsCount)
		{
			if (optionsCount < 11)
			{
				_optionType = OptionType.Number;
			}	// end if
			else if (optionsCount < 27)
			{
				_optionType = OptionType.Letter;
			}	// end else if
			else
			{
				throw new NotImplementedException($"I need a new options type that handles {optionsCount} options.");
			}	// end else

		}	// end SetOptionsType(int)

		#endregion Others

		#endregion Private Methods
	}
}
