﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Threading;

using Hub.Interface;

namespace Hub
{
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public class PopupCloser : IPopupCloser
	{
		private bool _closeRequested;
		private const int BnClicked = 245;

		///
		[DllImport("User32.dll")]
		public static extern int FindWindow(string lpClassName, string lpWindowName);
  
		///
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int SendMessage(int hWnd, int msg, int wParam, IntPtr lParam);
		  
		///
		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

		/// <inheritdoc />
		public void StartProcess()
		{
			void MyThread()
			{
				while (!_closeRequested)
				{
					Thread.Sleep(200);

					var dialog = FindWindow(null, "Assertion Failed: Abort=Quit, Retry=Debug, Ignore=Continue");
					if (dialog == 0)
						continue;
					var ignore = FindWindowEx((IntPtr)dialog, IntPtr.Zero, "Button", "&Ignore");
					if (ignore == IntPtr.Zero)
						continue;
					SendMessage((int)ignore, BnClicked, 0, IntPtr.Zero);
				}
			}

			var mKillThread = new Thread(MyThread);
			mKillThread.Start();
		}

		/// <inheritdoc />
		public void StopProcess() => _closeRequested = true;
	}
}
