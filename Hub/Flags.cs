﻿using System.Diagnostics.CodeAnalysis;

namespace Hub
{
	///
	[ExcludeFromCodeCoverage]
	public class Flags
	{
		// ReSharper disable InconsistentNaming
		// ReSharper disable UnusedMember.Local
		// ReSharper disable MemberCanBePrivate.Global
		// ReSharper disable IdentifierTypo
#pragma warning disable 1591
		public const string _a = "a";
		public const string _b = "b";
		public const string _bu = "bu";
		public const string _c = "c";
		public const string _d = "d";
		public const string _dm = "dm";
		public const string _dt = "dt";
		public const string _e = "e";
		public const string _f = "f";
		public const string _g = "g";
		public const string _h = "h";
		public const string _i = "i";
		public const string _j = "j";
		public const string _k = "k";
		public const string _l = "l";
		public const string _m = "m";
		public const string _ma = "ma";
		public const string _mi = "mi";
		public const string _n = "n";
		public const string _o = "o";
		public const string _p = "p";
		public const string _q = "q";
		public const string _r = "r";
		public const string _rc = "rc";
		public const string _rt = "rt";
		public const string _rx = "rx";
		public const string _s = "s";
		public const string _t = "t";
		public const string _tm = "tm";
		public const string _tr = "tr";
		public const string _u = "u";
		public const string _v = "v";
		public const string _w = "w";
		public const string _x = "x";
		public const string _y = "y";
		public const string _z = "z";
		// ReSharper restore UnusedMember.Local

		public const string _fAndr = _a;
		public const string _fBlin = _b;
		public const string _fBatc = _b;
		public const string _fBuil = _bu;
		public const string _fCore = _c;
		public const string _fData = _dt;
		public const string _fDbug = _d;
		public const string _fFold = _f;
		public const string _fHour = _h;
		public const string _fInpt = _i;
		public const string _fIos_ = _i;
		public const string _fMajr = _ma;
		public const string _fMinr = _mi;
		public const string _fNugt = _n;
		public const string _fPatc = _p;
		public const string _fPrNo = _n;
		public const string _fProd = _p;
		public const string _fProg = _p;
		public const string _fPrSl = _s;
		public const string _fPrTm = _t;
		public const string _fQiet = _q;
		public const string _fRsCn = _rc;
		public const string _fRsTx = _rt;
		public const string _fRsXm = _rx;
		public const string _fResA = _a;
		public const string _fResF = _f;
		public const string _fResR = _r;
		public const string _fResS = _s;
		public const string _fResT = _t;
		public const string _fRevi = _r;
		public const string _fShrd = _s;
		public const string _fShTm = _tm;
		public const string _fTEst = _e;
		public const string _fTlin = _t;
		public const string _fTrk1 = _tr;
		public const string _fTrk2 = _dm;
		public const string _fUnt1 = _t;
		public const string _fUnt2 = _u;
		public const string _fWind = _w;
		public const string _fWlin = _w;
		public const string _fXm81 = _x;
		public const string _fXm82 = _l;
		// ReSharper restore MemberCanBePrivate.Global

		public static readonly string _flagAndr = Prep.Minus(_fAndr);
		public static readonly string _flagBlin = Prep.Minus(_fBlin);
		public static readonly string _flagBatc = Prep.Minus(_fBatc);
		public static readonly string _flagBuil = Prep.Minus(_fBuil);
		public static readonly string _flagCore = Prep.Minus(_fCore);
		public static readonly string _flagData = Prep.Minus(_fData);
		public static readonly string _flagDbug = Prep.Minus(_fDbug);
		public static readonly string _flagFold = Prep.Minus(_fFold);
		public static readonly string _flagHour = Prep.Minus(_fHour);
		public static readonly string _flagInpt = Prep.Minus(_fInpt);
		public static readonly string _flagIos_ = Prep.Minus(_fIos_);
		public static readonly string _flagMajr = Prep.Minus(_fMajr);
		public static readonly string _flagMinr = Prep.Minus(_fMinr);
		public static readonly string _flagNugt = Prep.Minus(_fNugt);
		public static readonly string _flagPatc = Prep.Minus(_fPatc);
		public static readonly string _flagProd = Prep.Minus(_fProd);
		public static readonly string _flagProg = Prep.Minus(_fProg);
		public static readonly string _flagQiet = Prep.Minus(_fQiet);
		public static readonly string _flagRsCn = Prep.Minus(_fRsCn);
		public static readonly string _flagRsTx = Prep.Minus(_fRsTx);
		public static readonly string _flagRsXm = Prep.Minus(_fRsXm);
		public static readonly string _flagResA = Prep.Minus(_fResA);
		public static readonly string _flagResF = Prep.Minus(_fResF);
		public static readonly string _flagResR = Prep.Minus(_fResR);
		public static readonly string _flagResS = Prep.Minus(_fResS);
		public static readonly string _flagResT = Prep.Minus(_fResT);
		public static readonly string _flagRevi = Prep.Minus(_fRevi);
		public static readonly string _flagShrd = Prep.Minus(_fShrd);
		public static readonly string _flagShTm = Prep.Minus(_fShTm);
		public static readonly string _flagTEst = Prep.Minus(_fTEst);
		public static readonly string _flagTlin = Prep.Minus(_fTlin);
		public static readonly string _flagTrck = Prep.Minus(Prep.Slash(_fTrk1, _fTrk2));
		public static readonly string _flagTrk1 = Prep.Minus(_fTrk1);
		public static readonly string _flagTrk2 = Prep.Minus(_fTrk2);
		public static readonly string _flagUnit = Prep.Minus(Prep.Slash(_fUnt1, _fUnt2));
		public static readonly string _flagUnt1 = Prep.Minus(_fUnt1);
		public static readonly string _flagUnt2 = Prep.Minus(_fUnt2);
		public static readonly string _flagWind = Prep.Minus(_fWind);
		public static readonly string _flagWlin = Prep.Minus(_fWlin);
		public static readonly string _flagXm8_ = Prep.Minus(Prep.Slash(_fXm81, _fXm82));
		public static readonly string _flagXm81 = Prep.Minus(_fXm81);
		public static readonly string _flagXm82 = Prep.Minus(_fXm82);
#pragma warning restore 1591
		// ReSharper restore IdentifierTypo
		// ReSharper restore InconsistentNaming
	}
}
