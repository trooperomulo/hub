﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// manages interactions with directories
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class Directories : IDirectories
	{
		/// <inheritdoc />
		public void CreateDirectory(string path) => Directory.CreateDirectory(path);

		/// <inheritdoc />
		public void Delete(string path) => Directory.Delete(path, true);

		/// <inheritdoc />
		public bool Exists(string path) => Directory.Exists(path);

		/// <inheritdoc />
		public string GetCurrentDirectory() => Directory.GetCurrentDirectory();

		/// <inheritdoc />
		public DirectoryInfo[] GetDirectories(string root, string searchPattern) => new DirectoryInfo(root).GetDirectories(searchPattern, SearchOption.AllDirectories);

		/// <inheritdoc />
		public List<string> GetFiles(string path) => Directory.GetFiles(path).ToList();

		/// <inheritdoc />
		public List<string> GetFiles(string path, string filter) => Directory.GetFiles(path, filter).ToList();

		/// <inheritdoc />
		public List<string> GetFiles(string path, string filter, SearchOption option) => Directory.GetFiles(path, filter, option).ToList();

		/// <inheritdoc />
		public FileInfo[] GetFileInfos(string root, string filter) => new DirectoryInfo(root).GetFiles(filter);

		/// <inheritdoc />
		public FileInfo GetMostRecentlyChangedFile(string dirPath, string filter)
		{
			var dir = new DirectoryInfo(dirPath);
			return dir.GetFiles(filter).OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
		}

		/// <inheritdoc />
		public void SetCurrentDirectory(string path) => Directory.SetCurrentDirectory(path);
	}
}
