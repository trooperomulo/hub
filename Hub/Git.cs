﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub.Enum;
using Hub.Interface;

namespace Hub
{
	/// <summary>
	/// class that performs git actions
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class Git : IGit
	{
		#region Implementation of IGit

		/// <inheritdoc />
		public string GetBranchName() => ReadOutputFromCommand("rev-parse --abbrev-ref HEAD").Trim();

		/// <inheritdoc />
		public List<string> GetBranches(bool remote)
		{
			if (!remote)
			{
				IoCContainer.Instance.Resolve<IProgram>().RunProgram("git", "-c diff.mnemonicprefix=false -c core.quotepath=false fetch --prune origin", ShowOutput.No, true, true);
			}
			
			return ReadOutputFromCommand(remote ? "branch --remote" : "branch").CleanBranchOutput().Split('\n').ToList();
		}

		/// <inheritdoc />
		public string GetRemoteName() => ReadOutputFromCommand("rev-parse --abbrev-ref HEAD@{upstream}").Split('\\').ToList()[0].Trim();

		/// <inheritdoc />
		public bool IsNextGen() => Utils.BranchIsNextGen(GetRemoteName());

		#endregion Implementation of IGit

		#region Helper

		private string ReadOutputFromCommand(string command)
		{
			string name;
			using (var process = new Process())
			{
				process.StartInfo = IoCContainer.Instance.Resolve<IProgram>().GetStartInfo("git", command);
				process.Start();
				name = process.StandardOutput.ReadToEnd();
			}

			return name.Trim();
		}

		#endregion Helper
	}
}
