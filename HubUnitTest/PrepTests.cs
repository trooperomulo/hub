﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;

using NUnit.Framework;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="Prep" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class PrepTests
	{
		///
		[TearDown]
		public void Cleanup() => Env.UseRepo = false;

		#region Tests

		/// <summary>
		/// tests <see cref="Prep.Brackets" />
		/// </summary>
		[Test]
		public void TestBrackets()
		{
			Assert.AreEqual("[]", Prep.Brackets(null));
			Assert.AreEqual("[]", Prep.Brackets(""));
			Assert.AreEqual("[ab]", Prep.Brackets("ab"));
			Assert.AreEqual("[ab bc]", Prep.Brackets("ab", "bc"));
			Assert.AreEqual("[ab bc cd]", Prep.Brackets("ab", "bc", "cd"));
			Assert.AreEqual("[ab bc cd de]", Prep.Brackets("ab", "bc", "cd", "de"));
			Assert.AreEqual("[ab bc cd de ef]", Prep.Brackets("ab", "bc", "cd", "de", "ef"));
		}

		/// <summary>
		/// tests <see cref="Prep.ColumnLeft" />
		/// </summary>
		[Test]
		public void TestColumnLeft()
		{
			Assert.AreEqual(" Value          ", Prep.ColumnLeft("Value", 16));
			Assert.AreEqual(" Value  ", Prep.ColumnLeft("Value", 8));
			Assert.AreEqual(" Value ", Prep.ColumnLeft("Value", 7));
			Assert.AreEqual(" Valu ", Prep.ColumnLeft("Value", 6));
			Assert.AreEqual(" Val ", Prep.ColumnLeft("Value", 5));
		}

		/// <summary>
		/// tests <see cref="Prep.CutChunkOff" />
		/// </summary>
		[TestCase("123456789012345", "123456", "789012345")]
		[TestCase("789012345", "789012", "345")]
		[TestCase("345", "345", "")]
		[TestCase("", "", "")]
		[TestCase("123456", "123456", "")]
		public void TestCutChunkOff(string str, string expectedResult, string expectedOut)
		{
			Assert.AreEqual(expectedResult, Prep.CutChunkOff(6, ref str));
			Assert.AreEqual(expectedOut, str);
		}

		/// <summary>
		/// tests <see cref="Prep.Describe" />
		/// </summary>
		[Test]
		public void TestDescribe() => Assert.AreEqual("arg\t- desc", Prep.Describe("arg", "desc"));

		/// <summary>
		/// tests <see cref="Prep.Equal(string,string)" /> and <see cref="Prep.Equal(string,int)" />
		/// </summary>
		[Test]
		public void TestEqual()
		{
			Assert.AreEqual("front=back", Prep.Equal("front", "back"));
			Assert.AreEqual("size=56", Prep.Equal("size", 56));
		}

		/// <summary>
		/// tests <see cref="Prep.GetHeaderForRepo" />
		/// </summary>
		[Test]
		public void TestGetHeaderForRepo()
		{
			Assert.AreEqual("Xm8", Prep.GetHeaderForRepo(Repository.Xm8));
			Assert.AreEqual("Data", Prep.GetHeaderForRepo(Repository.Data));
			var ex = Assert.Throws<ArgumentOutOfRangeException>(() => Prep.GetHeaderForRepo((Repository) 22));
			Assert.AreEqual("Specified argument was out of the range of valid values.\r\nParameter name: repo", ex.Message);
		}

		/// <summary>
		/// tests <see cref="Prep.Lang" />
		/// </summary>
		[Test]
		public void TestLang()
		{
			Env.SetEnvironmentVariable("local", "", true);
			Env.SetEnvironmentVariable("repo", "", true);
			Env.UseRepo = false;
			Assert.AreEqual(@"\git\xactimate\path\strings.es-ES.resx", Prep.Lang("path", "es-ES"));
		}

		/// <summary>
		/// tests <see cref="Prep.Log" />
		/// </summary>
		[Test]
		public void TestLog()
		{
			Env.SetEnvironmentVariable("local", "", true);
			Env.SetEnvironmentVariable("version_tree", "", true);
			Env.SetEnvironmentVariable("mode", "", true);
			Assert.AreEqual(@"\bin\\Unit Test Results\Xm8.MP.EWD.dll.NUnit.log", Prep.Log("Xm8.MP.EWD"));
		}

		/// <summary>
		/// tests <see cref="Prep.Minus" />
		/// </summary>
		[Test]
		public void TestMinus()
		{
			Assert.AreEqual("-text", Prep.Minus("text"));
			Assert.AreEqual(" - text", Prep.Minus("text", true));
		}

		/// <summary>
		/// tests <see cref="Prep.Paren(string)" /> and <see cref="Prep.Paren(string)" />
		/// </summary>
		[Test]
		public void TestParen()
		{
			Assert.AreEqual("(just_one)", Prep.Paren("just_one"));
			Assert.AreEqual("(two things)", Prep.Paren("two", "things"));
		}

		/// <summary>
		/// tests <see cref="Prep.Slash" />
		/// </summary> 
		[Test]
		public void TestSlash()
		{
			Assert.AreEqual("", Prep.Slash(null));
			Assert.AreEqual("", Prep.Slash(""));
			Assert.AreEqual("ab", Prep.Slash("ab"));
			Assert.AreEqual("ab/bc", Prep.Slash("ab", "bc"));
			Assert.AreEqual("ab/bc/cd", Prep.Slash("ab", "bc", "cd"));
			Assert.AreEqual("ab/bc/cd/de", Prep.Slash("ab", "bc", "cd", "de"));
			Assert.AreEqual("ab/bc/cd/de/ef", Prep.Slash("ab", "bc", "cd", "de", "ef"));
		}

		/// <summary>
		/// tests <see cref="Prep.Xml" />
		/// </summary>
		[Test]
		public void TestXml()
		{
			Env.SetEnvironmentVariable("local", "", true);
			Env.SetEnvironmentVariable("version_tree", "", true);
			Env.SetEnvironmentVariable("mode", "", true);
			Assert.AreEqual(@"\bin\\Unit Test Results\Xm8.MP.EWD.dll.NUnit.xml", Prep.Xml("Xm8.MP.EWD"));
		}

		#endregion Tests
	}
}
