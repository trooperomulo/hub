﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Interface;
using HubUnitTest.Stubs;

using NUnit.Framework;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="SolutionColorizer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class SolutionColorizerTests
	{
		#region Constants

		private const string Path = "testing\\solution.sln";
		private const string Vs = "testing\\.vs";
		private const string Solution = Vs + "\\solution";
		private const string File = Solution + "\\color.txt";
		
		private const string ColorHub = "master:#FFC493FF";
		private const string ColorHub2 = "master:#FFCC008A";

		private const string ColorPurple = "master:Purple";

		#endregion Constants

		#region Tests
		
		/// <summary>
		/// tests <see cref="SolutionColorizer.ColorizeSolution" /> doesn't check the directory if we don't know what color to do
		/// </summary>
		[TestCase(1, "hub", "meh")]
		[TestCase(1, "xmpcc", "meh")]
		[TestCase(5, "hub", "meh")]
		[TestCase(5, "xmpcc", "meh")]
		[TestCase(1, "hub", "")]
		[TestCase(1, "xmpcc", "")]
		[TestCase(5, "hub", "")]
		[TestCase(5, "xmpcc", "")]
		public void TestNoColor(int num, string shortcut, string colors)
		{
			// arrange
			var dirs = new DirsStub(false, false);
			var cons = new BaseConsStub();

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(dirs);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(new FilesStub(false, ""));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			SetNum(num);

			var colorizer = new SolutionColorizer();
			
			// act
			colorizer.ColorizeSolution(colors, "whatever", shortcut);

			// assert
			Assert.AreEqual($"no color defined for shortcut {shortcut} or repo {num} or {shortcut}-{num}\r\n", cons.Output);
			Assert.AreEqual(0, dirs.ExistsCalledCount);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestNoColor(int, string, string)

		/// <summary>
		/// tests <see cref="SolutionColorizer.ColorizeSolution" /> creates directories appropriately
		/// </summary>
		[TestCase(false, false)]
		[TestCase(true, false)]
		[TestCase(false, true)]	// not realistic, but let's be thorough
		[TestCase(true, true)]
		public void TestDirectoriesAreCreated(bool vsExists, bool solExists)
		{
			// arrange
			var dirs = new DirsStub(vsExists, solExists);
			var files = new FilesStub(false, "");

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(dirs);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			SetNum(2);

			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("2:Purple", Path, "notHub");

			// assert
			Assert.AreEqual(GetExpectedDirCount(vsExists, solExists), dirs.DirectoriesCreated.Count, "The wrong number of directories were created");
			Assert.AreEqual(!vsExists, dirs.DirectoriesCreated.Contains(Vs), "vs failed");
			Assert.AreEqual(!solExists, dirs.DirectoriesCreated.Contains(Solution), "solution failed");

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end TestDirectoriesAreCreated(bool, bool)

		/// <summary>
		/// tests that <see cref="SolutionColorizer.ColorizeSolution" /> will create a new file if one does not already exist
		/// </summary>
		[Test]
		public void TestCreatingNewFile()
		{
			// arrange
			var files = new FilesStub(false, "");

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			SetNum(2);

			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("2:Purple", Path, "notHub");

			// assert
			Assert.AreEqual(1, files.WrittenLines.Count);
			Assert.AreEqual(File, files.WrittenFile);
			Assert.AreEqual("master:Purple", files.WrittenLines[0]);
			Assert.IsTrue(files.WasAppendedTo);
			Assert.IsFalse(files.WasWrittenTo);
			
			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end TestCreatingNewFile()

		/// <summary>
		/// tests that <see cref="SolutionColorizer.ColorizeSolution" /> doesn't create or write to a file when the file is already there adn has the correct color
		/// </summary>
		[Test]
		public void TestFileIsGoodToGo()
		{
			// arrange
			var files = new FilesStub(true, ColorPurple);

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			SetNum(2);
			
			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("2:Purple", Path, "hub");

			// assert
			Assert.AreEqual(0, files.WrittenLines.Count);
			Assert.IsFalse(files.WasAppendedTo);
			Assert.IsFalse(files.WasWrittenTo);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end TestFileIsGoodToGo()

		/// <summary>
		/// tests that <see cref="SolutionColorizer.ColorizeSolution" /> rewrites the file when it has the wrong color
		/// </summary>
		[Test]
		public void TestFileHasWrongMasterColor()
		{
			// arrange
			var files = new FilesStub(true, ColorHub2);

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("hub:#FFC493FF", Path, "hub");

			// assert
			Assert.AreEqual(1, files.WrittenLines.Count);
			Assert.AreEqual(ColorHub, files.WrittenLines[0]);
			Assert.IsFalse(files.WasAppendedTo);
			Assert.IsTrue(files.WasWrittenTo);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end TestFileHasWrongMasterColor()

		/// <summary>
		/// tests that <see cref="SolutionColorizer.ColorizeSolution" /> rewrites the file when it has the wrong color
		/// </summary>
		[Test]
		public void TestFileHasNoMasterColor()
		{
			// arrange
			var files = new FilesStub(true, "something else");

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			var colorizer = new SolutionColorizer();

			SetNum(2);

			// act
			colorizer.ColorizeSolution("2:Purple", Path, "hub");

			// assert
			Assert.AreEqual(1, files.WrittenLines.Count);
			Assert.AreEqual(ColorPurple, files.WrittenLines[0]);
			Assert.IsTrue(files.WasAppendedTo);
			Assert.IsFalse(files.WasWrittenTo);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end TestFileHasNoMasterColor()

		/// <summary>
		/// tests that we only process colors the first time
		/// </summary>
		[Test]
		public void TestOnlyProcessColorsOnce()
		{
			// arrange
			var files = new FilesStub(true, ColorPurple);

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			SetNum(2);

			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("2:Purple", Path, "hub");
			colorizer.ColorizeSolution("2:Red", Path, "hub");

			// assert
			Assert.AreEqual(0, files.WrittenLines.Count);
			Assert.IsFalse(files.WasAppendedTo);
			Assert.IsFalse(files.WasWrittenTo);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end TestOnlyProcessColorsOnce()

		/// <summary>
		/// tests that we use the last preference for a repo/shortcut/combo if multiple are set
		/// </summary>
		[Test]
		public void TestKeepLastPreference()
		{
			// arrange
			var files = new FilesStub(true, ColorPurple);
			var cons = new BaseConsStub();

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			SetNum(2);

			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("2:Red|2:Purple", Path, "hub");

			// assert
			Assert.AreEqual("2 was already set to Red.  Changing to Purple\r\n", cons.Output);
			Assert.AreEqual(0, files.WrittenLines.Count);
			Assert.IsFalse(files.WasAppendedTo);
			Assert.IsFalse(files.WasWrittenTo);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}   // end TestKeepLastPreference()

		/// <summary>
		/// tests that we get the correct color for the given shortcut and repo combination
		/// </summary>
		[TestCase("nothub", 1, "Red")]
		[TestCase("hub", 3, "Blue")]
		[TestCase("hub", 2, "Yellow")]
		[TestCase("hub", 1, "Blue")]
		public void TestGetCorrectColorForSolutionRepoCombo(string shortcut, int num, string expectedColor)
		{
			// arrange
			var files = new FilesStub(false, "");

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);

			SetNum(num);

			var colorizer = new SolutionColorizer();

			// act
			colorizer.ColorizeSolution("1:Red|hub:Blue|hub-2:Yellow", Path, shortcut);

			// assert
			Assert.AreEqual(1, files.WrittenLines.Count);
			Assert.AreEqual(File, files.WrittenFile);
			Assert.AreEqual($"master:{expectedColor}", files.WrittenLines[0]);
			Assert.IsTrue(files.WasAppendedTo);
			Assert.IsFalse(files.WasWrittenTo);

			// clean up
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}   // end TestGetCorrectColorForSolutionRepoCombo()

		#endregion Tests

		#region Helpers

		private static int GetExpectedDirCount(bool vsExists, bool solExists)
		{
			var expectedCount = 0;
			if (!vsExists)
			{
				expectedCount++;
			}	// end if

			if (!solExists)
			{
				expectedCount++;
			}	// end if

			return expectedCount;

		}	// end GetExpectedDirCount(bool, bool)

		private static void SetNum(int num)
		{
			Environment.SetEnvironmentVariable("num", num.ToString());
		}	// SetNum(int)

		#endregion Helpers

		#region Inner Classes

		private class DirsStub : BaseDirsStub
		{
			#region Members

			public List<string> DirectoriesCreated { get; }
			public int ExistsCalledCount { get; private set; }
			private readonly bool _vsExists;
			private readonly bool _solExists;

			#endregion Members

			#region Constructor

			public DirsStub() : this(true, true) { }	// end DirsStub()

			public DirsStub(bool vsExists, bool solExists)
			{
				_vsExists = vsExists;
				_solExists = solExists;

				DirectoriesCreated = new List<string>();

			}	// end DirsStub(bool, bool)

			#endregion Constructor

			#region Overrides of BaseDirsStub

			public override void CreateDirectory(string path)
			{
				DirectoriesCreated.Add(path);

			}	// end CreateDirectory(string)

			public override bool Exists(string path)
			{
				ExistsCalledCount++;
				if (path.EndsWith(".vs"))
				{
					return _vsExists;
				}	// end if

				if (path.EndsWith("solution"))
				{
					return _solExists;
				}	// end if

				return true;

			}	// end Exists(string)

			#endregion Overrides of BaseDirsStub

		}	// end DirsStub

		private class FilesStub : BaseFileStub
		{
			#region Members

			private bool _fileExists;
			private string _existingLine;
			public List<string> WrittenLines { get; }
			public string WrittenFile { get; private set; }
			public bool WasAppendedTo { get; private set; }
			public bool WasWrittenTo { get; private set; }

			#endregion Members

			#region Constructor

			public FilesStub(bool fileExists, string existingLine)
			{
				_fileExists = fileExists;
				_existingLine = existingLine;

				WrittenLines = new List<string>();

			}	// end FilesStub(bool, string)

			#endregion Constructor

			#region Overrides of BaseFileStub

			public override void AppendAllLines(string path, List<string> lines)
			{
				WasAppendedTo = true;
				WrittenFile = path;
				WrittenLines.Clear();
				WrittenLines.AddRange(lines);

			}	// end AppendAllLines(string, List<string>)

			public override bool Exists(string path)
			{
				return _fileExists;
			}	// end Exists(string)

			public override string ReadAllText(string path)
			{
				return _existingLine;
			}	// end ReadAllText(string)

			public override void WriteAllLines(string path, IEnumerable<string> lines)
			{
				WasWrittenTo = true;
				WrittenFile = path;
				WrittenLines.Clear();
				WrittenLines.AddRange(lines);

			}	// end WriteAllLines(string, IEnumerable<string>)

			#endregion Overrides of BaseFileStub

		}	// end FilesStub

		#endregion Inner Classes

	}	// end SolutionColorizerTests

}	// HubUnitTest
