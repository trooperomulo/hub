﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="EmailCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class EmailCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="EmailCommand.Description" />, <see cref="EmailCommand.HasSubCommands"/>, <see cref="EmailCommand.NeedShortcut" />,
		/// <see cref="EmailCommand.Inputs" /> and <see cref="EmailCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new EmailCommand(false), new List<string> {"compose"}, false, false,
				new List<string> {"e", "email"},
				new List<string>
				{
					"creates",
					"email",
					"bm",
					"current",
					"xm8",
					"data",
					"previous",
					"major",
					"minor",
					"not included",
					"version",
					"revision",
					"-f",
					"-x",
					"-dt",
					"-p",
					"-ma",
					"-mi",
					"-bu",
					"-r"
				});
		}

		/// <summary>
		/// tests <see cref="EmailCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(1, false, "Build Vibranium", TestName = "Vib email")]
		[TestCase(2, true, "Build NextGen", TestName = "NG email")]
		[TestCase(3, true, "Build NextGen", TestName = "All lines")]
		public void TestPerform(int group, bool isNextGen, string subject)
		{
			void Test()
			{
				var eStub = new EmailStub();
				IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub(isNextGen));
				IoCContainer.Instance.RegisterAsSingleInstance<IEmail, EmailStub>(eStub);

				var email = new EmailCommand(true);
				email.Perform(GetArgs(group == 3));

				Assert.AreEqual("BuildMaster5@xactware.com", eStub.To, "To was wrong");
				Assert.AreEqual(subject, eStub.Subject, "Subject was wrong");
				Assert.AreEqual(GetBody(group), eStub.Body, "Body was wrong");

				IoCContainer.Instance.RemoveRegisteredType<IGit>();
				IoCContainer.Instance.RemoveRegisteredType<IEmail>();
			}

			UnitTestUtil.TestConsoleOutput(Test, "");
		}

		/// <summary>
		/// tests <see cref="EmailCommand.Perform" /> when the IEmail object throwns an exception
		/// </summary>
		[Test]
		public void TestPerformIEmailException()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub(false));
			IoCContainer.Instance.RegisterAsSingleInstance<IEmail, EmailExStub>(new EmailExStub());

			var email = new EmailCommand(true);
			email.Perform(new List<string>());

			Assert.AreEqual("Unable to create email - oops\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IGit>();
			IoCContainer.Instance.RemoveRegisteredType<IEmail>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Helpers

		private static List<string> GetArgs(bool setAll)
		{
			// email/e [-f folder] [-x xm8] [-dt data] [-p patch] [-ma major] [-mi minor] [-bu build] [-r revision]
			var args = new List<string> {"-f", "fo", "-x", "xm", "-dt", "da"};
			if (setAll)
			{
				args.AddRange(new List<string> {"-p", "pa", "-ma", "5", "-mi", "6", "-bu", "7", "-r", "8"});
			}

			return args;
		}

		private static string GetBody(int group)
		{
			var fo = Prep.Equal("build_name", "fo");
			var xm = Prep.Equal("git_xm8_branch", "xm");
			var dt = Prep.Equal("git_xm8Data_branch", "da");
			var pa = Prep.Equal("bm2ini_textBoxPreviousProgram", "pa");
			var ma = Prep.Equal("bm2ini_numericUpDownMajor", group == 2 ? "2" : "5");
			var mi = Prep.Equal("bm2ini_numericUpDownMinor", "6");
			var bu = Prep.Equal("bm2ini_numericUpDownBuild", "7");
			var rv = Prep.Equal("bm2ini_numericUpDownRevision", "8");

			const string nl = "\r\n";

			var main = $"{fo}{nl}{xm}{nl}{dt}{nl}";

			switch (group)
			{
				case 1:
					return main;
				case 2:
					return $"{main}{ma}{nl}";
				case 3:
					return $"{main}{pa}{nl}{ma}{nl}{mi}{nl}{bu}{nl}{rv}{nl}";
				default:
					return "";
			}
		}

		#endregion Helpers

		#region Inner Classes

		private class GitStub : BaseGitStub
		{
			#region Variable
			
			private readonly bool _isNextGen;

			#endregion Variable

			#region Implementation of IGit

			/// <inheritdoc />
			public override string GetBranchName() => "";

			/// <inheritdoc />
			public override bool IsNextGen() => _isNextGen;

			#endregion Implementation of IGit

			#region Constructor

			public GitStub(bool isNextGen) => _isNextGen = isNextGen;

			#endregion Constructor
		}

		private class EmailStub : BaseEmailStub
		{
			#region Variables

			///
			public string To;

			///
			public string Subject;

			///
			public string Body;

			#endregion Variables

			#region Implementation of IEmail

			/// <inheritdoc />
			public override void DisplayEmail(string to, string subject, string body)
			{
				To = to;
				Subject = subject;
				Body = body;
			}

			#endregion Implementation of IEmail
		}

		private class EmailExStub : BaseEmailStub
		{
			#region Implementation of IEmail

			/// <inheritdoc />
			public override void DisplayEmail(string to, string subject, string body) => throw new Exception("oops");

			#endregion Implementation of IEmail
		}

		#endregion Inner Classes
	}
}
