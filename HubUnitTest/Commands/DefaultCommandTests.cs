﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="DefaultCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class DefaultCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="DefaultCommand.Description" />, <see cref="DefaultCommand.NeedShortcut" />,
		/// <see cref="DefaultCommand.Inputs" /> and <see cref="DefaultCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var def = new DefaultCommand(null);
			Assert.IsFalse(def.HasSubCommands);
			Assert.IsFalse(def.NeedShortcut);
			Assert.AreEqual("", def.Description);
			Assert.AreEqual(0, def.Inputs.Count);

			def.Help(new List<string> {"cmd"});

			Assert.AreEqual("'cmd' is not a command that I am familiar with.  Please try again.\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="DefaultCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var called = false;

			void Help()
			{
				called = true;
			}

			var def = new DefaultCommand(Help);

			def.Perform(new List<string> {"cmd"});
			Assert.IsTrue(called);

			Assert.IsTrue(cons.Output.Contains("You have entered an invalid command - cmd"));

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests
	}
}
