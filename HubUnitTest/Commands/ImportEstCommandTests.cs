﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="ImportEstCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ImportEstCommandTests
	{
		#region Tests

		///
		[SetUp]
		public void Setup() => IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(new ProgStub());

		///
		[TearDown]
		public void TearDown() => IoCContainer.Instance.RemoveRegisteredType<IProgram>();

		/// <summary>
		/// tests <see cref="ImportEstCommand.Description" />, <see cref="ImportEstCommand.NeedShortcut" />,
		/// <see cref="ImportEstCommand.Inputs" />  and <see cref="ImportEstCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new ImportEstCommand(""), new List<string> {"import", "estimates"}, false, false,
				new List<string> {"ie", "import"},
				new List<string> {"import", "estimates", "28", "number", "profile", "delete", "how many days", "user", "status"});
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 0 arguments passed in
		/// </summary>
		[Test]
		public void TestPerform0Args()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string>());

			Assert.IsTrue(cons.Output.StartsWith("not enough parameters passed to Import\r\n"));

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 1 argument passed in
		/// </summary>
		[Test]
		public void TestPerform1Args()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string> {"10"});

			Assert.IsTrue(cons.Output.StartsWith("not enough parameters passed to Import\r\n"));

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 2 arguments passed in
		/// </summary>
		[Test]
		public void TestPerform2Args()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string> {"10", "ab"});
			TestArgs(prog.Args, 10, "AB");
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 3 arguments passed in
		/// </summary>
		[Test]
		public void TestPerform3Args()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string> {"10", "ab", "F"});
			TestArgs(prog.Args, 10, "AB", false);
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 4 arguments passed in
		/// </summary>
		[Test]
		public void TestPerform4Args()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string> {"10", "ab", "F", "5"});
			TestArgs(prog.Args, 10, "AB", false, 5);
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 5 arguments passed in
		/// </summary>
		[Test]
		public void TestPerform5Args()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string> {"10", "ab", "F", "5", "fred"});
			TestArgs(prog.Args, 10, "AB", false, 5, "fred");
		}

		/// <summary>
		/// tests <see cref="ImportEstCommand.Perform" /> with 6 arguments passed in
		/// </summary>
		[Test]
		public void TestPerform6Args()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ImportEstCommand("user");
			cmd.Perform(new List<string> {"10", "ab", "F", "5", "fred", "8"});
			TestArgs(prog.Args, 10, "AB", false, 5, "fred", 8);
		}

		#endregion Tests

		#region Helper

		private void TestArgs(string actual, int num, string prof, bool del = true, int days = 0, string user = "user",
			int status = 1)
		{
			var parts = actual.Split(' ');
			Assert.AreEqual(7, parts.Length);
			Assert.AreEqual(del, parts[0] == "T");
			Assert.AreEqual(user, parts[2], $"{nameof(user)} was wrong");
			Assert.AreEqual(prof, parts[3], $"{nameof(prof)} was wrong");
			Assert.AreEqual(status.ToString(), parts[4], $"{nameof(status)} was wrong");
			Assert.AreEqual(num.ToString(), parts[5], $"{nameof(num)} was wrong");
			Assert.AreEqual(days.ToString(), parts[6]);
		}

		#endregion Helper

		#region Inner Class

		private class ProgStub : BaseProgStub
		{
			internal string Args;

			#region Implementation of IProgram

			/// <inheritdoc />
			public override void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true,
				ShowOutput show = ShowOutput.No)
			{
				Args = arguments;
			}

			#endregion Implementation of IProgram
		}

		#endregion Inner Class
	}
}
