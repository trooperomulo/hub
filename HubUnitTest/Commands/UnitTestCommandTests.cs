﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

using Hub;
using Hub.Commands;
using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="UnitTestCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UnitTestCommandTests
	{
		#region Constants

		private static readonly Type _type = typeof(UnitTestCommand);

		private const string Utrp1 =
			"<Results><Result sol=\"Sola\" nF=\"0\" nP=\"1\" nS=\"1\" tot=\"2\" inv=\"0\"><Case name=\"C1\" res=\"0\" ticks=\"6\"></Case><Case name=\"C2\" res=\"2\" ticks=\"1\"></Case></Result></Results>";

		private const string Utrp2 =
			"<Results><Result sol=\"Solb\" nF=\"0\" nP=\"2\" nS=\"2\" tot=\"4\" inv=\"0\"><Case name=\"C1\" res=\"0\" ticks=\"6\"></Case><Case name=\"C2\" res=\"2\" ticks=\"1\"></Case></Result></Results>";

		private const string Utrp3 =
			"<Results>" +
				"<Result sol=\"a\" nF=\"0\" nP=\"1\" nS=\"0\" tot=\"1\" inv=\"0\">" +
					"<Case name=\"C1\" res=\"0\" ticks=\"6\"></Case>" +
				"</Result>" +
				"<Result sol=\"b\" nF=\"0\" nP=\"1\" nS=\"0\" tot=\"1\" inv=\"0\">" +
					"<Case name=\"C1\" res=\"0\" ticks=\"6\"></Case>" +
				"</Result>" +
				"<Result sol=\"c\" nF=\"0\" nP=\"1\" nS=\"0\" tot=\"1\" inv=\"0\">" +
					"<Case name=\"C1\" res=\"0\" ticks=\"6\"></Case>" +
				"</Result>" +
				"<Result sol=\"d\" nF=\"0\" nP=\"1\" nS=\"0\" tot=\"1\" inv=\"0\">" +
					"<Case name=\"C1\" res=\"0\" ticks=\"6\"></Case>" +
				"</Result>" +
				"<Result sol=\"e\" nF=\"0\" nP=\"1\" nS=\"0\" tot=\"1\" inv=\"0\">" +
					"<Case name=\"C1\" res=\"0\" ticks=\"6\"></Case>" +
				"</Result>" +
				"<Result sol=\"f\" nF=\"0\" nP=\"1\" nS=\"0\" tot=\"1\" inv=\"0\">" +
					"<Case name=\"C1\" res=\"0\" ticks=\"6\"></Case>" +
				"</Result>" +
			"</Results>";

		#endregion Constants

		#region Tests

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IGit>();
			IoCContainer.Instance.RemoveRegisteredType<IPopupCloser>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.Description" />, <see cref="UnitTestCommand.NeedShortcut" />,
		/// <see cref="UnitTestCommand.Inputs" />  and <see cref="UnitTestCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFileStub>(new BaseFileStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProgStub>(new BaseProgStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, BaseGitStub>(new BaseGitStub());
			UnitTestUtil.TestCommand(new UnitTestCommand(false, null, null),
				new List<string> {"unit", "test", "run"}, false, true, new List<string> {"u", "test", "unit"},
				new List<string>
				{
					"run",
					"unit",
					"tests",
					"specified",
					"solutions",
					"displays",
					"results",
					"not build",
					"launch",
					"xactimate",
					"fifo",
					"'all'",
					"progress",
					"sols",
					"completed",
					"estimated",
					"time",
					"console",
					"txt",
					"xml",
					"previous",
					"comparisons",
					"compare",
					"notes",
					"scenarios:",
					TestTimeTracker.Path.ToLower(),
					nameof(UserParams.TrackUnitTestTime).ToLower(),
					"default",
					"seconds",
					"int",
					"num",
					"0",
					"recent"
				});
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.CompareResults
		/// </summary>
		[Test]
		public void TestCompareResults()
		{
			var files = new CrFiles();
			var prog = new CrProg();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, CrFiles>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, CrDirs>(new CrDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, CrGit>(new CrGit());
			var utc = new UnitTestCommand(false, null, null);

			var prev = new UnitTestResultParser(new List<UnitTestResultAssemblyParser>
			{
				new UnitTestResultAssemblyParser("a", 0, 1, 0, 1, false,
					new List<TestCase> {new TestCase("a1", TestCaseResult.Pass, TimeSpan.FromTicks(8))})
			});
			var curr = UnitTestResultParser.FromXmlString(Utrp1);
			utc.CompareResults(prev, curr, 4);

			var expected = $@"Unit Test Results\Comparison 4 {DateTime.Now:yy-MM-dd}.log";
			Assert.AreEqual(expected, prog.Path, "start process path was wrong");
			Assert.AreEqual(expected, files.Path, "write path was wrong");
			UnitTestUtil.TestListsAreEqual(new UtrpComparer(prev, curr).GetLines(), files.Lines);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.DetermineTimeEstimate
		/// </summary>
		[Test]
		[TestCase(1, true)]
		[TestCase(2, true)]
		[TestCase(3, true)]
		[TestCase(4, true)]
		[TestCase(5, true)]
		[TestCase(1, false)]
		[TestCase(2, false)]
		[TestCase(3, false)]
		[TestCase(4, false)]
		[TestCase(5, false)]
		public void TestDetermineTimeEstimate(int numSols, bool print)
		{
			// since I'm testing DetermineTimeEstimate and not TestTimeTracker or TestProgressTimeDisplayer,
			// I'm just going to have all the solutions have the default time estimate

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, DteFiles>(new DteFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DteDirs>(new DteDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, DteProg>(new DteProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, DteGit>(new DteGit());
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var utc = new UnitTestCommand(false, null, null);

			var sols = new List<string>();
			for (var i = 0; i < numSols; i++)
			{
				sols.Add($"{i}");
			}

			utc.DetermineTimeEstimate(sols, print, out var ttt, out var tptd);

			var expected = print
				? $"Tests should take approximately {TimeSpan.FromSeconds(numSols * TestTimeTracker.DefaultTime):g}\r\n"
				: null;

			// verify the output was correct
			Assert.AreEqual(expected, cons.Output);

			// verify the test time tracker was set
			Assert.IsNotNull(ttt);

			// use reflection to get list of solutions from the displayer
			var solutions =
				UnitTestUtil.GetPrivateProperty<List<(string sol, bool run, double time)>>(
					typeof(TestProgressTimeDisplayer), tptd, "Solutions");
			foreach (var sol in sols)
			{
				Assert.IsTrue(solutions.Exists(s => sol.Equals(s.sol)), $"{sol} wasn't in the displayer");
			}
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.DoUnit(List{string}, IUnitTestSettings)"/>
		/// </summary>
		[Test]
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		[TestCase(3)]
		[TestCase(4)]
		public void TestDoUnitAll(int othersCount)
		{
			var sols = new List<string> {"a", "b", "c", "d", "what", "ever"};
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, DuaFiles>(new DuaFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DuaDirs>(new DuaDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, DuaProg>(new DuaProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, DuaGit>(new DuaGit());
			var utc = new Mock<UnitTestCommand>(false, null, null) { CallBase = true };
			utc.Setup(u => u.GetAllSolutions()).Returns(sols);
			utc.Setup(u => u.TestSolutions(It.Is<List<string>>(s => UnitTestUtil.ListsAreEqual(sols, s)),
				AnySettings()));

			var all = new List<string>();

			for (var i = 0; i < othersCount; i++)
			{
				all.Add(i.ToString());
			}

			all.Insert(Rnd.Int(all.Count), "all");

			utc.Object.DoUnit(all);

			utc.Verify(u => u.GetAllSolutions(), Times.Once);
			utc.Verify(u => u.TestSolutions(AnyStringList(), AnySettings()), Times.Once);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.DoUnit(List{string})" />
		/// </summary>
		[Test]
		public void TestDoUnitNoSettings()
		{
			var sols = new List<string> {"a", "b", "c", "d", "what", "ever"};
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, DunsFiles>(new DunsFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DunsDirs>(new DunsDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, DunsProg>(new DunsProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, DunsGit>(new DunsGit());
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};
			utc.Setup(u => u.DoUnit(It.Is<List<string>>(s => UnitTestUtil.ListsAreEqual(sols, s)),
				It.Is<IUnitTestSettings>(sett => sett != null)));
			utc.Object.DoUnit(sols);
			utc.Verify(u => u.DoUnit(AnyStringList(), AnySettings()), Times.Once);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.DoUnit(List{string}, IUnitTestSettings)" />
		/// </summary>
		[Test]
		public void TestDoUnitProcessSolutions()
		{
			var files = new DupsFiles();
			var sols = new List<string> {"a", "b", "c", "b", "d"};
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, DupsFiles>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DupsDirs>(new DupsDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, DupsProg>(new DupsProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, DupsGit>(new DupsGit());
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};
			utc.Setup(u =>
				u.TestSolutions(
					It.Is<List<string>>(s => UnitTestUtil.ListsAreEqual(new List<string> {"adll", "bdll"}, s)),
					AnySettings()));
			Shortcuts.Instance.LoadShortcuts("name");

			utc.Object.DoUnit(sols);

			var expected = "c does not have other info defined in the shortcut file\r\n" +
						   "you entered bdll to be tested twice\r\nd was not found in the shortcuts\r\n";

			Assert.AreEqual(expected, cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.GetAllSolutions
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestGetAllSolutions(bool isNextGen)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, GasFiles>(new GasFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, GasDirs>(new GasDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, GasProg>(new GasProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GasGit>(new GasGit(isNextGen));
			var utc = new UnitTestCommand(false, null, null);
			var actual = utc.GetAllSolutions();
			var expected = new List<string>
			{
				"Xm8.MP.ControlCenter.UnitTests.dll",
				"Core.UI.Controls.dll",
				"Core.UI.Extended.UnitTests.dll",
				"Xm8Operations.dll",
				"XV.UI.dll",
				"SharedNetworking.dll",
				"Shared.SyncQueue.dll",
				"Shared.SyncQueueTests.dll",
			};

			if (!isNextGen)
			{
				expected.Add("DataMigrationTool.exe");
			}

			expected.Sort();

			UnitTestUtil.TestListsAreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.GetFilename
		/// </summary>
		[Test]
		[TestCase(1, @"C:\ProgramData\Hub\Results\1.xml")]
		[TestCase(3, @"C:\ProgramData\Hub\Results\3.xml")]
		[TestCase(6, @"C:\ProgramData\Hub\Results\6.xml")]
		[TestCase(25, @"C:\ProgramData\Hub\Results\25.xml")]
		public void TestGetFilename(int expCount, string expName)
		{
			var files = new GfFiles(expCount);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, GfFiles>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, GfDirs>(new GfDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, GfProg>(new GfProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GfGit>(new GfGit());
			var utc = new UnitTestCommand(false, null, null);

			const int actCount = 0;
			var prms = new object[] {actCount};
			var actName = UnitTestUtil.CallPrivateMethod<string>(_type, utc, "GetFilename", prms);

			Assert.AreEqual(expCount, prms[0], "Count was wrong");
			Assert.AreEqual(expName, actName, "Name was wrong");
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.GetPreviousParser
		/// </summary>
		[Test]
		[TestCase(1)]
		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(-2)]
		[TestCase(-3)]
		[TestCase(-4)]
		public void TestGetPreviousParser(int num)
		{
			//  1 num > 0, file exists
			//  0 num > 0, file doesn't exist

			// -1 num < 1, no recent (so it doesn't exist)
			// -2 num < 1, recent doesn't have full name (so it doesn't exist)
			// -3 num < 1, recent has full name, file doesn't exist
			// -4 num < 1, recent has full name, file exists

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, GppFiles>(new GppFiles(num));
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, GppDirs>(new GppDirs(num));
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, GppProg>(new GppProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GppGit>(new GppGit());
			var utc = new UnitTestCommand(false, null, null);

			var result = UnitTestUtil.CallPrivateMethod<UnitTestResultParser>(_type, utc, "GetPreviousParser", num);

			if (num == 1 || num == -4)
			{
				Assert.AreEqual(Utrp1, result.ToXmlString());
			}
			else
			{
				Assert.AreEqual(null, result);
			}
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.GetProgressSolution
		/// </summary>
		[Test]
		[TestCase(5, 0, "0 of 0 - 0.00%")]
		[TestCase(5, 10, "5 of 10 - 50.00%")]
		[TestCase(1, 1, "1 of 1 - 100.00%")]
		[TestCase(2, 3, "2 of 3 - 66.67%")]
		public void TestGetProgressSolution(int count, int total, string expected)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, GpsFiles>(new GpsFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, GpsDirs>(new GpsDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, GpsProg>(new GpsProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GpsGit>(new GpsGit());
			var utc = new UnitTestCommand(false, null, null);

			var actual = UnitTestUtil.CallPrivateMethod<string>(_type, utc, "GetProgressSolution", count, total);
			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// tests UnitTestCommand.InvalidInputFile
		/// </summary>
		[Test]
		[TestCase(0, "There were no results in the results folder")]
		[TestCase(1, @"Couldn't find 'C:\ProgramData\Hub\Results\1.xml'")]
		[TestCase(2, @"Couldn't find 'C:\ProgramData\Hub\Results\2.xml'")]
		[TestCase(100, @"Couldn't find 'C:\ProgramData\Hub\Results\100.xml'")]
		public void TestInvalidInputFile(int input, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IifFiles>(new IifFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IifDirs>(new IifDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, IifProg>(new IifProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, IifGit>(new IifGit());
			var utc = new UnitTestCommand(false, null, null);
			utc.InvalidInputFile(input);

			Assert.AreEqual(expected + "\r\n", cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.Perform" /> changes the current directory to bin
		/// </summary>
		[Test]
		public void TestPerformChangesDirectoryToBin()
		{
			var dirs = new PcdtbDirs();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, PcdtbFiles>(new PcdtbFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, PcdtbDirs>(dirs);
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, PcdtbProg>(new PcdtbProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, PcdtbGit>(new PcdtbGit());
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			var utc = new UnitTestCommand(false, null, null);
			utc.Perform(new List<string>());

			Assert.AreEqual(Env.BinFolder, dirs.Directory);
		}

		/// <summary>
		/// tests that <see cref="UnitTestCommand.Perform" /> calls the launch helpers appropriately
		/// </summary>
		[Test]
		[TestCase(false, false)]
		[TestCase(false, true)]
		[TestCase(true, false)]
		[TestCase(true, true)]
		public void TestPerformLaunchesHelpers(bool xm8, bool trk)
		{
			var xm8Called = false;
			var trkCalled = false;

			void Xm8() => xm8Called = true;
			void Trk() => trkCalled = true;

			var args = new List<string> {"-i", "1"};	// this forces all the relevant settings off

			if (xm8)
			{
				args.Add("-x");
			}

			if (trk)
			{
				args.Add("-tr");
			}

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFileStub>(new BaseFileStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProgStub>(new BaseProgStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, BaseGitStub>(new BaseGitStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			var utc = new UnitTestCommand(false, Xm8, Trk);
			utc.Perform(args);

			Assert.AreEqual(xm8, xm8Called, "xm8 wasn't called properly");
			Assert.AreEqual(trk, trkCalled, "trk wasn't called properly");
		}

		/// <summary>
		/// tests that <see cref="UnitTestCommand.Perform" /> won't run if there is no selected output
		/// </summary>
		[Test]
		public void TestPerformNoOutput()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFileStub>(new BaseFileStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProgStub>(new BaseProgStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, BaseGitStub>(new BaseGitStub());
			var args = new List<string> {"-i", "1"}; // this forces all the relevant settings off
			var utc = new UnitTestCommand(false, null, null);
			utc.Perform(args);

			var expected =
				"I'm not going to waste my time running the tests if you don't want me to output the results anywhere\r\n";
			Assert.AreEqual(expected, cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.Perform" /> when no valid solutions are passed in
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestPerformNoSolutions(bool badSols)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var args = new List<string> {"-rt"};
			if (badSols)
			{
				args.AddRange(new List<string> {"eh", "wh", "at", "ev", "er"});
			}

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, PnsFiles>(new PnsFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, PnsDirs>(new PnsDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, PnsProg>(new PnsProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, PnsGit>(new PnsGit());
			var utc = new UnitTestCommand(false, null, null);
			utc.Perform(args);

			Assert.IsTrue(cons.Output.EndsWith("No valid solutions were specified\r\n"));
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.ProcessSolution
		/// </summary>
		[Test]
		[TestCase("a", true, null)]
		[TestCase("b", true, "you entered bdll to be tested twice\r\n")]
		[TestCase("c", false, "c does not have other info defined in the shortcut file\r\n")]
		[TestCase("d", false, "d was not found in the shortcuts\r\n")]
		public void TestProcessSolution(string sol, bool inList, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var files = new PsFiles();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, PsFiles>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, PsDirs>(new PsDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, PsProg>(new PsProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, PsGit>(new PsGit());
			var utc = new UnitTestCommand(false, null, null);
			Shortcuts.Instance.LoadShortcuts("name");

			// test with 'a, b, c, a, d' in just one test.  Verify that dlls holds the correct dlls and that the correct messages were output to the console
			var dlls = new List<string> {"bdll"};
			var prms = new object[] {sol, dlls};

			UnitTestUtil.CallPrivateVoidMethod(_type, utc, "ProcessSolution", prms);

			Assert.AreEqual(inList, dlls.Contains($"{sol}dll"));
			Assert.AreEqual(expected, cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.ResultXml
		/// </summary>
		[Test]
		[TestCase(1, @"C:\ProgramData\Hub\Results\1.xml")]
		[TestCase(3, @"C:\ProgramData\Hub\Results\3.xml")]
		[TestCase(6, @"C:\ProgramData\Hub\Results\6.xml")]
		[TestCase(25, @"C:\ProgramData\Hub\Results\25.xml")]
		public void TestResultXml(int input, string expected)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, RxFiles>(new RxFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, RxDirs>(new RxDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, RxProg>(new RxProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, RxGit>(new RxGit());
			var utc = new UnitTestCommand(false, null, null);
			var actual = UnitTestUtil.CallPrivateMethod<string>(_type, utc, "ResultXml", input);
			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.TestResults
		/// </summary>
		[Test]
		[TestCase(true, false, false)]
		[TestCase(false, false, false)]
		[TestCase(true, true, false)]
		[TestCase(false, true, false)]
		[TestCase(true, false, true)]
		[TestCase(false, false, true)]
		[TestCase(true, true, true)]
		[TestCase(false, true, true)]
		public void TestTestResults(bool con, bool txt, bool xml)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var settings = new Mock<IUnitTestSettings>();
			settings.Setup(s => s.InputFile).Returns(-1);
			settings.Setup(s => s.ResultsConsole).Returns(con);
			settings.Setup(s => s.ResultsTextFile).Returns(txt);
			settings.Setup(s => s.ResultsXmlFile).Returns(xml);

			var files = new Mock<IFiles>();
			files.Setup(f => f.WriteXml(AnyString(), AnyString()));

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, TrDirs>(new TrDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, TrProg>(new TrProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, TrGit>(new TrGit());
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};
			utc.Setup(u => u.TestResultsConsole(AnyStringList()));
			utc.Setup(u => u.TestResultsTextFile(AnyStringList()));

			utc.Object.TestResults(settings.Object, UnitTestResultParser.FromXmlString(Utrp1));

			UnitTestUtil.MoqMethodWasCalled(utc, u => u.TestResultsConsole(AnyStringList()), con);
			UnitTestUtil.MoqMethodWasCalled(utc, u => u.TestResultsTextFile(AnyStringList()), txt);
			UnitTestUtil.MoqMethodWasCalled(files, f => f.WriteXml(AnyString(), AnyString()), xml);

			Assert.AreEqual(xml ? "xml results file '1' was created\r\n" : null, cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestResults" /> passing in an input
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestTestResultsInput(bool inputExists)
		{
			var num = inputExists ? 1 : 0;
			var settings = new Mock<IUnitTestSettings>();
			settings.Setup(s => s.InputFile).Returns(num);

			var pPrev = UnitTestResultParser.FromXmlString(Utrp1);
			var pCurr = UnitTestResultParser.FromXmlString(Utrp2);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, TriFiles>(new TriFiles(num));
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, TriDirs>(new TriDirs(num));
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, TriProg>(new TriProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, TriGit>(new TriGit());
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};

			utc.Object.TestResults(settings.Object, pCurr);

			UnitTestUtil.MoqMethodWasCalled(utc, u => u.CompareResults(pPrev, pCurr, num), inputExists);
			UnitTestUtil.MoqMethodWasCalled(utc, u => u.InvalidInputFile(num), !inputExists);

			// if input exists, I want to verify that CompareResults is called, that it's called with parser, that it's called with a different parser and that it's called with 1
			// if input doesn't exist, I want to verify that InvalidInputFile is called and that it's called with 0
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.TestResultsConsole
		/// </summary>
		[Test]
		public void TestTestResultsConsole()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, TrcFiles>(new TrcFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, TrcDirs>(new TrcDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, TrcProg>(new TrcProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, TrcGit>(new TrcGit());
			var utc = new UnitTestCommand(false, null, null);
			var lines = new List<string> {"one", "2", "aobive", "ovein4", "102947", "eh", "whatever"};

			utc.TestResultsConsole(lines);

			UnitTestUtil.TestStringContainsItems(cons.Output.ToLower(), lines.Select(l => $"{l}\r\n").ToList(), "output");
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.TestResultsTextFile
		/// </summary>
		[Test]
		public void TestTestResultsTextFile()
		{
			var files = new TrtfFiles();
			var prog = new TrtfProg();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, TrtfFiles>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, TrtfDirs>(new TrtfDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, TrtfProg>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, TrtfGit>(new TrtfGit());
			var utc = new UnitTestCommand(false, null, null);
			var lines = new List<string> {"one", "2", "aobive", "ovein4", "102947", "eh", "whatever"};

			utc.TestResultsTextFile(lines);
			
			const string logFile = @"C:\ProgramData\Hub\Results\Results.log";

			UnitTestUtil.TestListsAreEqual(lines, files.Written);
			Assert.AreEqual(logFile, files.Path, "was written to the wrong path");
			Assert.AreEqual(logFile, prog.Filename, "tried to open the wrong file");
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolution(string, bool, int, int, TestProgressTimeDisplayer, ref UnitTestResultParser)" />
		/// </summary>
		[Test]
		[TestCase(false, false, false)]
		[TestCase(false, false, true)]
		[TestCase(false, true, false)]
		[TestCase(false, true, true)]
		[TestCase(true, false, false)]
		[TestCase(true, false, true)]
		[TestCase(true, true, false)]
		[TestCase(true, true, true)]
		public void TestTestSolution(bool show, bool disp, bool pars)
		{
			// set up interfaces
			var num = show ? 1 : 0;
			num += disp ? 2 : 0;
			num += pars ? 4 : 0;
			var stream = new TsStream($"Sample{num}.xml");
			GetInterfaceMocks(out var files, out var dirs, out var prog, out var git);
			files.Setup(f => f.GetStream(It.IsAny<string>(), It.IsAny<FileMode>()))
				.Returns<string, FileMode>((s, m) => stream);

			// set up parameters
			var sols = new List<string> {"sol", "uti", "ons", " to", " te", "st."};
			var sol = sols[Rnd.Int(sols.Count)];
			var total = Rnd.Int(50);
			var count = Rnd.Int(total + 1);
			var parser = pars ? UnitTestResultParser.FromXmlString(Utrp1) : null;
			var displayer = disp ? Rnd.GetRandomlyFilledTptd(sol) : null;

			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, IProgram>(prog.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, IGit>(git.Object);
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};

			// perform test
			utc.Object.TestSolution(sol, show, count, total, displayer, ref parser);

			// verify most stuff
			prog.Verify(p => p.RunProgram("nunit3-console.exe", sol, ShowOutput.No, true, false, true, null), Times.Once);
			files.Verify(f => f.GetStream(Path.Combine(Env.BinFolder, "TestResult.xml"), FileMode.Open), Times.Once);
			Assert.AreEqual(1, stream.Called, "we should have gotten exactly 1 stream");
			Assert.AreEqual(pars ? 2 : 1, parser.Results.Count, "wrong number of results in the parser");

			TimeSpan GetDurationOfMostRecent()
			{
				// ReSharper disable once PossibleNullReferenceException - it will be set by the time the test gets to here
				// due to the stream that I've "loading" the results from, the solution just run will always be fake.dll
				return parser.Results.FirstOrDefault(res => res.Solution == "fake.dll")?.TotalDuration ?? TimeSpan.Zero;
			}
			
			string GetProgressSolution(int c, int totSol)
			{
				var percent = totSol == 0 ? 0 : (decimal)c / totSol;
				return $"{(totSol == 0 ? 0 : c)} of {totSol} - {percent:P}";
			}

			string GetExpectedOutput()
			{
				// ReSharper disable once PossibleNullReferenceException - it will be set by the time the test gets to here
				var duration = pars ? GetDurationOfMostRecent() : parser.TotalDuration;
				var time = show ? $" [{duration}]" : "";
				// ReSharper disable once PossibleNullReferenceException - it will be set by the time the test gets to here
				var progress = disp ? displayer.Display : GetProgressSolution(count, total);
				return $"Testing {sol} - done{time} ({progress})\r\n";
			}

			// verify output
			Assert.AreEqual(GetExpectedOutput(), cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolution(string, bool, int, int, ref UnitTestResultParser)" />
		/// </summary>
		[Test]
		public void TestTestSolutionWithSolutionProgress()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFiles>(new BaseFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirs>(new BaseDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProg>(new BaseProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, BaseGit>(new BaseGit());
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};

			utc.Setup(u => u.TestSolution(AnyString(), AnyBool(), AnyInt(), AnyInt(), AnyTptd(), ref It.Ref<UnitTestResultParser>.IsAny));

			var rBool = Rnd.Int(2) == 1;
			var rInt1 = Rnd.Int();
			var rInt2 = Rnd.Int();
			var parser = UnitTestResultParser.FromXmlString(Rnd.Int(2) == 1 ? Utrp1 : Utrp2);

			utc.Object.TestSolution("aouse", rBool, rInt1, rInt2, ref parser);

			UnitTestUtil.MoqMethodWasCalled(utc,
				u => u.TestSolution("aouse", rBool, rInt1, rInt2, null, ref parser),
				true);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolution(string, bool, TestProgressTimeDisplayer, ref UnitTestResultParser)" />
		/// </summary>
		[Test]
		public void TestTestSolutionWithTimeProgress()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFiles>(new BaseFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirs>(new BaseDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProg>(new BaseProg());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, BaseGit>(new BaseGit());
			var utc = new Mock<UnitTestCommand>(false, null, null) {CallBase = true};

			utc.Setup(u => u.TestSolution(AnyString(), AnyBool(), AnyInt(), AnyInt(), AnyTptd(), ref It.Ref<UnitTestResultParser>.IsAny));

			var rBool = Rnd.Int(2) == 1;
			var disp = Rnd.GetRandomlyFilledTptd();
			var parser = UnitTestResultParser.FromXmlString(Rnd.Int(2) == 1 ? Utrp1 : Utrp2);

			utc.Object.TestSolution("aouse", rBool, disp, ref parser);

			UnitTestUtil.MoqMethodWasCalled(utc,
				u => u.TestSolution("aouse", rBool, 0, 0, disp, ref parser),
				true);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolutions" /> with a bad progress passed in
		/// </summary>
		[Test]
		[TestCase(true, true)]
		[TestCase(true, false)]
		[TestCase(false, true)]
		[TestCase(false, false)]
		public void TestTestSolutionsWithBadProgress(bool estTime, bool track)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			PrepareForTestSolutionsTest((Progress) 9999, estTime, track, out var settings, out var closer, out var utc,
				out var sols, out var files, out var dirs, out var parser);

			utc.Object.TestSolutions(sols, settings.Object);

			VerifyForTestSolutionsTest(closer, utc, sols, files, dirs, estTime, false, 0, estTime ? 1 : 0, 0, 0, 0,
				0, parser);

			var output = estTime
				? "Tests should take approximately 0:00:15.631\r\n"
				: "";
			output += "A new Progress type has been added but isn't being handled in UnitTestCommand.TestSolutions\r\n";

			Assert.AreEqual(output, cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolutions" /> with a Progress.None passed in
		/// </summary>
		[Test]
		[TestCase(true, true)]
		[TestCase(true, false)]
		[TestCase(false, true)]
		[TestCase(false, false)]
		public void TestTestSolutionsWithProgressNone(bool estTime, bool track)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			PrepareForTestSolutionsTest(Progress.None, estTime, track, out var settings, out var closer, out var utc,
				out var sols, out var files, out var dirs, out var parser);
			
			utc.Setup(u => u.TestSolutionsInOneGo(sols, out parser));

			utc.Object.TestSolutions(sols, settings.Object);

			VerifyForTestSolutionsTest(closer, utc, sols, files, dirs, estTime, false, track ? 6 : 0,
				estTime ? 1 : 0, 1, 1, 0, 0, parser);

			Assert.AreEqual(estTime ? "Tests should take approximately 0:00:15.631\r\n" : null, cons.Output);

		}

		private delegate void TssCallback(string sol, bool showTime, int cnt, int tot, ref UnitTestResultParser par);

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolutions" /> with a Progress.Solution passed in
		/// </summary>
		[Test]
		[TestCase(true, true)]
		[TestCase(true, false)]
		[TestCase(false, true)]
		[TestCase(false, false)]
		public void TestTestSolutionsWithProgressSolution(bool estTime, bool track)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			PrepareForTestSolutionsTest(Progress.Solution, estTime, track, out var settings, out var closer, out var utc,
				out var sols, out var files, out var dirs, out var parser);

			var showTime = settings.Object.ShowTime;
			UnitTestResultParser empty = null;

			utc.Setup(u => u.TestSolution(sols[0], showTime, 1, 6, ref empty)).Callback(new TssCallback(Callback));
			utc.Setup(u => u.TestSolution(sols[1], showTime, 2, 6, ref parser));
			utc.Setup(u => u.TestSolution(sols[2], showTime, 3, 6, ref parser));
			utc.Setup(u => u.TestSolution(sols[3], showTime, 4, 6, ref parser));
			utc.Setup(u => u.TestSolution(sols[4], showTime, 5, 6, ref parser));
			utc.Setup(u => u.TestSolution(sols[5], showTime, 6, 6, ref parser));

			// ReSharper disable once RedundantAssignment
			void Callback(string sol, bool time, int cnt, int total, ref UnitTestResultParser par)
			{
				par = parser;
			}

			utc.Object.TestSolutions(sols, settings.Object);

			VerifyForTestSolutionsTest(closer, utc, sols, files, dirs, estTime, showTime, track ? 6 : 0,
				estTime ? 1 : 0, 1, 0, 1, 0, parser);

			Assert.AreEqual(estTime ? "Tests should take approximately 0:00:15.631\r\n" : null, cons.Output);
		}

		private delegate void TstCallback(string sol, bool showTime, TestProgressTimeDisplayer disp, ref UnitTestResultParser par);

		/// <summary>
		/// tests <see cref="UnitTestCommand.TestSolutions" /> with a Progress.Time passed in
		/// </summary>
		[Test]
		[TestCase(true, true)]
		[TestCase(true, false)]
		[TestCase(false, true)]
		[TestCase(false, false)]
		public void TestTestSolutionsWithProgressTime(bool estTime, bool track)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			PrepareForTestSolutionsTest(Progress.Time, estTime, track, out var settings, out var closer, out var utc,
				out var sols, out var files, out var dirs, out var parser);

			var showTime = settings.Object.ShowTime;

			// ReSharper disable once RedundantAssignment
			void Callback(string sol, bool time, TestProgressTimeDisplayer disp, ref UnitTestResultParser par)
			{
				par = parser;
			}

			UnitTestResultParser empty = null;
			utc.Setup(u => u.TestSolution(sols[0], showTime, AnyTptd(), ref empty)).Callback(new TstCallback(Callback));
			utc.Setup(u => u.TestSolution(sols[1], showTime, AnyTptd(), ref parser));
			utc.Setup(u => u.TestSolution(sols[2], showTime, AnyTptd(), ref parser));
			utc.Setup(u => u.TestSolution(sols[3], showTime, AnyTptd(), ref parser));
			utc.Setup(u => u.TestSolution(sols[4], showTime, AnyTptd(), ref parser));
			utc.Setup(u => u.TestSolution(sols[5], showTime, AnyTptd(), ref parser));

			utc.Object.TestSolutions(sols, settings.Object);

			VerifyForTestSolutionsTest(closer, utc, sols, files, dirs, estTime, showTime, 6, 1, 1, 0, 0, 1, parser);

			Assert.AreEqual(estTime ? "Tests should take approximately 0:00:15.631\r\n" : null, cons.Output);
		}

		/// <summary>
		/// tests <see cref="UnitTestCommand" />.TestSolutionsInOneGo
		/// </summary>
		[Test]
		public void TestTestSolutionsInOneGo()
		{
			var prog = new TsiogProg();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, TsiogFiles>(new TsiogFiles());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, TsiogDirs>(new TsiogDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, TsiogProg>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, TsiogGit>(new TsiogGit());
			var utc = new UnitTestCommand(false, null, null);
			var args = new List<string> {"one", "1", "abc", "two", "2", "def"};

			utc.TestSolutionsInOneGo(args, out _);

			Assert.AreEqual("nunit3-console.exe", prog.Path, "Path wasn't set properly");
			Assert.AreEqual("one 1 abc two 2 def", prog.Args, "The arguments weren't passed properly");
		}

		#endregion Tests

		#region Helpers

		private static bool AnyBool() => It.IsAny<bool>();

		private static int AnyInt() => It.IsAny<int>();

		private static IUnitTestSettings AnySettings() => It.IsAny<IUnitTestSettings>();

		private static string AnyString() => It.IsAny<string>();

		private static List<string> AnyStringList() => It.IsAny<List<string>>();

		private static TestProgressTimeDisplayer AnyTptd() => It.IsAny<TestProgressTimeDisplayer>();

		private static void GetInterfaceMocks(out Mock<IFiles> files, out Mock<IDirectories> dirs, out Mock<IProgram> prog,
			out Mock<IGit> git)
		{
			files = new Mock<IFiles>();
			dirs = new Mock<IDirectories>();
			prog = new Mock<IProgram>();
			git = new Mock<IGit>();
		}

		private static void PrepareForTestSolutionsTest(Progress progress, bool estTime, bool track, out Mock<IUnitTestSettings> settings,
			out Mock<IPopupCloser> closer, out Mock<UnitTestCommand> utc, out List<string> sols, out Mock<IFiles> files,
			out Mock<IDirectories> dirs, out UnitTestResultParser parser)
		{
			// settings
			settings = new Mock<IUnitTestSettings>();
			settings.Setup(s => s.Progress).Returns(progress);
			settings.Setup(s => s.EstimateTime).Returns(estTime);
			settings.Setup(s => s.ShowTime).Returns(Rnd.Int(2) == 1);

			// closer
			closer = new Mock<IPopupCloser>();

			// unit test command (and its moqs)
			GetInterfaceMocks(out files, out dirs, out var prog, out var git);
			dirs.Setup(d => d.SetCurrentDirectory(AnyString()));
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, IProgram>(prog.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, IGit>(git.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IPopupCloser, IPopupCloser>(closer.Object);
			utc = new Mock<UnitTestCommand>(track, null, null) {CallBase = true};
			utc.Setup(u => u.TestResults(It.IsAny<IUnitTestSettings>(), It.IsAny<UnitTestResultParser>()));

			// common values
			sols = new List<string> {"a", "b", "c", "d", "e", "f"};
			parser = UnitTestResultParser.FromXmlString(Utrp3);
		}

		private static void VerifyForTestSolutionsTest(Mock<IPopupCloser> closer, Mock<UnitTestCommand> utc, List<string> sols,
			Mock<IFiles> files, Mock<IDirectories> dirs, bool estTime, bool showTime, int writeCalled, int detEstTimeCalled,
			int testResults, int testOneGoCalled, int testSolSol, int testSolTime, UnitTestResultParser parser)
		{
			dirs.Verify(d => d.SetCurrentDirectory(Env.BinFolder), Times.Once);
			closer.Verify(c => c.StartProcess(), Times.Once);
			utc.Verify(u => u.DetermineTimeEstimate(sols, estTime, out It.Ref<TestTimeTracker>.IsAny,
				out It.Ref<TestProgressTimeDisplayer>.IsAny), Times.Exactly(detEstTimeCalled));
			utc.Verify(u => u.TestSolutionsInOneGo(sols, out It.Ref<UnitTestResultParser>.IsAny),
				Times.Exactly(testOneGoCalled));

			utc.Verify(u => u.TestSolution(sols[0], showTime, 1, 6, ref parser), Times.Exactly(testSolSol));
			utc.Verify(u => u.TestSolution(sols[1], showTime, 2, 6, ref parser), Times.Exactly(testSolSol));
			utc.Verify(u => u.TestSolution(sols[2], showTime, 3, 6, ref parser), Times.Exactly(testSolSol));
			utc.Verify(u => u.TestSolution(sols[3], showTime, 4, 6, ref parser), Times.Exactly(testSolSol));
			utc.Verify(u => u.TestSolution(sols[4], showTime, 5, 6, ref parser), Times.Exactly(testSolSol));
			utc.Verify(u => u.TestSolution(sols[5], showTime, 6, 6, ref parser), Times.Exactly(testSolSol));

			utc.Verify(u => u.TestSolution(sols[0], showTime, It.IsAny<TestProgressTimeDisplayer>(), ref parser), Times.Exactly(testSolTime));
			utc.Verify(u => u.TestSolution(sols[1], showTime, It.IsAny<TestProgressTimeDisplayer>(), ref parser), Times.Exactly(testSolTime));
			utc.Verify(u => u.TestSolution(sols[2], showTime, It.IsAny<TestProgressTimeDisplayer>(), ref parser), Times.Exactly(testSolTime));
			utc.Verify(u => u.TestSolution(sols[3], showTime, It.IsAny<TestProgressTimeDisplayer>(), ref parser), Times.Exactly(testSolTime));
			utc.Verify(u => u.TestSolution(sols[4], showTime, It.IsAny<TestProgressTimeDisplayer>(), ref parser), Times.Exactly(testSolTime));
			utc.Verify(u => u.TestSolution(sols[5], showTime, It.IsAny<TestProgressTimeDisplayer>(), ref parser), Times.Exactly(testSolTime));

			closer.Verify(c => c.StopProcess(), Times.Once);
			files.Verify(f => f.WriteAllLines(AnyString(), AnyStringList()), Times.Exactly(writeCalled));
			utc.Verify(u => u.TestResults(It.IsAny<IUnitTestSettings>(), It.IsAny<UnitTestResultParser>()),
				Times.Exactly(testResults));
		}

		#endregion Helpers

		#region Inner Classes

		// TODO 040 : Replace these classes with Mocks

		#region Base Stubs

		private class BaseFiles : BaseFileStub { }

		private class BaseDirs : BaseDirsStub
		{
			#region Overrides of BaseDirsStub

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path) { }

			#endregion Overrides of BaseDirsStub
		}

		private class BaseProg : BaseProgStub { }

		private class BaseGit : BaseGitStub { }

		private class BaseStream : Stream
		{
			private readonly FileStream _backer;

			///
			internal BaseStream(string path)
			{
				_backer = new FileStream(UnitTestUtil.GetTestPath(path, nameof(UnitTestCommand)), FileMode.Open);
			}

			#region Overrides of Stream

			/// <inheritdoc />
			public override void Flush() => _backer.Flush();

			/// <inheritdoc />
			public override long Seek(long offset, SeekOrigin origin) => _backer.Seek(offset, origin);

			/// <inheritdoc />
			public override void SetLength(long value) => _backer.SetLength(value);

			/// <inheritdoc />
			public override int Read(byte[] buffer, int offset, int count) => _backer.Read(buffer, offset, count);

			/// <inheritdoc />
			public override void Write(byte[] buffer, int offset, int count) => _backer.Write(buffer, offset, count);

			/// <inheritdoc />
			public override bool CanRead => _backer.CanRead;

			/// <inheritdoc />
			public override bool CanSeek => _backer.CanSeek;

			/// <inheritdoc />
			public override bool CanWrite => _backer.CanWrite;

			/// <inheritdoc />
			public override long Length => _backer.Length;

			/// <inheritdoc />
			public override long Position
			{
				get => _backer.Position;
				set => _backer.Position = value;
			}

			#endregion Overrides of Stream
		}

		#endregion Base Stubs

		#region CompareResults

		private class CrFiles : BaseFiles
		{
			#region Variables

			internal List<string> Lines;
			internal string Path;

			#endregion Variable

			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override void WriteAllLines(string path, IEnumerable<string> lines)
			{
				Assert.IsTrue(Lines == null && Path == null, "Write was called twice");

				Path = path;
				Lines = lines.ToList();
			}

			#endregion Overrides of BaseFileStub
		}

		private class CrDirs : BaseDirs { }

		private class CrProg : BaseProg
		{
			#region Variable

			internal string Path;

			#endregion Variable

			#region Overrides of BaseProgStub

			/// <inheritdoc />
			public override void StartProcess(string filename)
			{
				Assert.IsTrue(Path == null, "Start was called twice");
				Path = filename;
			}

			#endregion Overrides of BaseProgStub
		}

		private class CrGit : BaseGit { }

		#endregion CompareResults

		#region DetermineTimeEstimate

		private class DteFiles : BaseFiles
		{
			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override bool Exists(string path) => false;

			#endregion Overrides of BaseFileStub
		}

		private class DteDirs : BaseDirs
		{
			#region Overrides of BaseDirsStub

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			#endregion Overrides of BaseDirsStub
		}

		private class DteProg : BaseProg { }

		private class DteGit : BaseGit { }

		#endregion DetermineTimeEstimate

		#region DoUnitAll
		
		private class DuaFiles : BaseFiles { }

		private class DuaDirs : BaseDirs { }

		private class DuaProg : BaseProg { }

		private class DuaGit : BaseGit { }

		#endregion DoUnitAll

		#region DoUnitNoSettings

		private class DunsFiles : BaseFiles { }

		private class DunsDirs : BaseDirs { }

		private class DunsProg : BaseProg { }

		private class DunsGit : BaseGit { }

		#endregion DoUnitNoSettings

		#region DoUnitProcessSolutions

		private class DupsFiles : PsFiles { }

		private class DupsDirs : BaseDirs { }

		private class DupsProg : BaseProg { }

		private class DupsGit : BaseGit { }

		#endregion DoUnitProcessSolutions

		#region GetAllSolutions

		private class GasFiles : BaseFiles { }

		private class GasDirs : BaseDirs
		{
			#region Overrides of BaseDirsStub

			/// <inheritdoc />
			public override List<string> GetFiles(string path, string filter)
			{
				return new List<string>
				{
					"Xm8.MP.ControlCenter.dll",
					"Xm8.MP.ControlCenter.UnitTests.dll",
					"Core.UI.Controls.dll",
					"Core.UI.Extended.dll",
					"Core.UI.Extended.UnitTests.dll",
					"Xm8Operations.dll",
					"XV.UI.dll",
					"SharedNetworking.dll",
					"Shared.SyncQueue.dll",
					"ThirdParty.dll",
					"Whatever.dll",
					"Shared.SyncQueueTests.dll",
				};
			}

			#endregion Overrides of BaseDirsStub
		}

		private class GasProg : BaseProg { }

		private class GasGit : BaseGit
		{
			#region Variable

			private readonly bool _isNextGen;

			#endregion Variable

			#region Constructor

			internal GasGit(bool isNextGen) => _isNextGen = isNextGen;

			#endregion Constructor

			#region Overrides of BaseGitStub

			/// <inheritdoc />
			public override bool IsNextGen() => _isNextGen;

			#endregion Overrides of BaseGitStub
		}

		#endregion GetAllSolutions

		#region GetFilename

		private class GfFiles : BaseFiles
		{
			private readonly int _num;

			internal GfFiles(int num) => _num = num;

			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override bool Exists(string path) => !path.EndsWith($@"\{_num}.xml");

			#endregion Overrides of BaseFileStub
		}

		private class GfDirs : BaseDirs { }

		private class GfProg : BaseProg { }

		private class GfGit : BaseGit { }

		#endregion GetFilename

		#region GetPreviousParser

		private class GppFiles : BaseFiles
		{
			#region Variable

			private readonly int _num;

			#endregion Variable

			#region Constructor

			internal GppFiles(int num) => _num = num;

			#endregion Constructor

			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override bool Exists(string path) => _num == 1 || _num == -4;

			/// <inheritdoc />
			public override string ReadAllText(string path)
			{
				switch (_num)
				{
					case 1:
					case -4:
						return Utrp1;
					default:
						return "";
				}
			}

			#endregion Overrides of BaseFileStub
		}

		private class GppDirs : BaseDirs
		{
			#region Variable

			private readonly int _num;

			#endregion Variable

			#region Constructor

			internal GppDirs(int num) => _num = num;

			#endregion Constructor

			#region Overrides of BaseDirsStub

			/// <inheritdoc />
			public override FileInfo GetMostRecentlyChangedFile(string dirPath, string filter)
			{
				// -1 num < 1, no recent (so it doesn't exist)
				// -2 num < 1, recent doesn't have full name (so it doesn't exist)
				// -3 num < 1, recent has full name, file doesn't exist
				// -4 num < 1, recent has full name, file exists

				FileInfo fi;

				switch (_num)
				{
					case -2:
						fi = new FileInfo(UnitTestUtil.GetTestPath("Sample.xml", nameof(UnitTestCommand)));
						UnitTestUtil.SetPrivateProperty(typeof(FileInfo), fi, "FullPath", "");
						break;
					case -3:
					case -4:
						fi = new FileInfo(UnitTestUtil.GetTestPath("Sample.xml", nameof(UnitTestCommand)));
						break;
					default:
						return null;
				}

				return fi;
			}

			#endregion Overrides of BaseDirsStub
		}

		private class GppProg : BaseProg { }

		private class GppGit : BaseGit { }

		#endregion GetPreviousParser

		#region GetProgressSolution

		private class GpsFiles: BaseFiles { }

		private class GpsDirs : BaseDirs { }

		private class GpsProg : BaseProg { }

		private class GpsGit : BaseGit { }

		#endregion GetProgressSolution

		#region InvalidInputFile

		private class IifFiles: BaseFiles { }

		private class IifDirs : BaseDirs { }

		private class IifProg : BaseProg { }

		private class IifGit : BaseGit { }

		#endregion InvalidInputFile

		#region ProcessSolution

		private class PsFiles : BaseFiles
		{
			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			/// <inheritdoc />
			public override IEnumerable<string> ReadLines(string path)
			{
				return new List<string> {"a=apath=adll", "b=bpath=bdll", "c=cpath"};
			}

			#endregion Overrides of BaseFileStub
		}

		private class PsDirs : BaseDirs { }

		private class PsProg : BaseProg { }

		private class PsGit : BaseGit { }

		#endregion ProcessSolution

		#region Perform Changes Directory To Bin

		private class PcdtbFiles : BaseFiles { }

		private class PcdtbDirs : BaseDirs
		{
			#region Variable

			internal string Directory;

			#endregion Variable

			#region Overrides of BaseDirsStub

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path)
			{
				Assert.IsTrue(Directory == null, "Directory was set more than once");
				Directory = path;
			}

			#endregion Overrides of BaseDirsStub
		}

		private class PcdtbProg : BaseProg { }

		private class PcdtbGit : BaseGit { }

		#endregion Perform Changes Directory To Bin

		#region Perform No Solutions

		private class PnsFiles : BaseFiles { }

		private class PnsDirs : BaseDirs { }

		private class PnsProg : BaseProg { }

		private class PnsGit : BaseGit { }

		#endregion Perform No Solutions

		#region ResultXml

		private class RxFiles : BaseFiles { }

		private class RxDirs : BaseDirs { }

		private class RxProg : BaseProg { }

		private class RxGit : BaseGit { }

		#endregion ResultXml

		#region TestResults

		private class TrDirs : BaseDirs { }

		private class TrProg : BaseProg { }

		private class TrGit : BaseGit { }

		#endregion TestResults

		#region TestResultsInput

		private class TriFiles : GppFiles
		{
			internal TriFiles(int num) : base(num) { }

			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override void WriteAllLines(string path, IEnumerable<string> lines) { }

			#endregion Overrides of BaseFileStub
		}

		private class TriDirs : GppDirs { internal TriDirs(int num) : base(num) { } }

		private class TriProg : GppProg
		{
			#region Overrides of BaseProgStub

			/// <inheritdoc />
			public override void StartProcess(string filename) { }

			#endregion Overrides of BaseProgStub
		}

		private class TriGit : GppGit { }

		#endregion TestResultsInput

		#region TestResultsConsole

		private class TrcFiles : BaseFiles { }

		private class TrcDirs : BaseDirs { }

		private class TrcProg : BaseProg { }

		private class TrcGit : BaseGit { }

		#endregion TestResultsConsole

		#region TestResultsTextFile

		private class TrtfFiles : BaseFiles
		{
			#region Variables

			internal string Path;
			internal List<string> Written;

			#endregion Variables

			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override void WriteAllLines(string path, IEnumerable<string> lines)
			{
				if (!Path.IsNullOrEmpty() || Written != null)
				{
					Assert.Fail("Write should have only been called once");
				}

				Path = path;
				Written = lines.ToList();
			}
			
			#endregion Overrides of BaseFileStub
		}

		private class TrtfDirs : BaseDirs
		{
			#region Overrides of BaseDirsStub

			/// <inheritdoc />
			public override bool Exists(string path) => false;

			/// <inheritdoc />
			public override void CreateDirectory(string path) { }

			#endregion Overrides of BaseDirsStub
		}

		private class TrtfProg : BaseProg
		{
			#region Variable

			internal string Filename;

			#endregion Variable

			#region Overrides of BaseProgStub

			/// <inheritdoc />
			public override void StartProcess(string filename)
			{
				if (!Filename.IsNullOrEmpty())
				{
					Assert.Fail("Start process should have only been called once");
				}

				Filename = filename;
			}

			#endregion Overrides of BaseProgStub
		}

		private class TrtfGit : BaseGit { }

		#endregion TestResultsTextFile

		#region TestSolution

		private class TsStream : BaseStream
		{
			internal readonly int Called;

			internal TsStream(string path) : base(path)
			{
				Called++;
			}
		}

		#endregion TestSolution

		#region TestSolutionsInOneGo

		private class TsiogFiles : BaseFiles
		{
			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override Stream GetStream(string path, FileMode fileMode) => new TsiogStream();

			#endregion Overrides of BaseFileStub
		}

		private class TsiogDirs : BaseDirs { }

		private class TsiogProg : BaseProg
		{
			#region Variables

			internal string Path;
			internal string Args;

			#endregion Variables

			#region Overrides of BaseProgStub

			/// <inheritdoc />
			public override bool RunProgram(string path, string arguments = null, ShowOutput show = ShowOutput.No, bool rso = true,
				bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				Assert.IsNull(Path, "Path shouldn't already be set");
				Assert.IsNull(Args, "Args shouldn't already be set");

				Path = path;
				Args = arguments;

				return true;
			}

			#endregion Overrides of BaseProgStub
		}

		private class TsiogGit : BaseGit { }

		private class TsiogStream : BaseStream
		{
			internal TsiogStream() : base("Sample.xml") { }
		}

		#endregion TestSolutionsInOneGo

		#endregion Inner Classes
	}
}
