﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="BuildCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class BuildCommandTests
	{
		#region Variable

		private static int _rbfCalled;

		#endregion Variable

		#region Tests

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(new FilesStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(new ProgStub(false));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(new Mock<IConsole>().Object);
		}

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="BuildCommand.Description" />, <see cref="BuildCommand.HasSubCommands"/>, <see cref="BuildCommand.NeedShortcut" />,
		/// <see cref="BuildCommand.Inputs" /> and <see cref="BuildCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new BuildCommand(false, null, null, null, null, null),
				new List<string> {"build", "solution"}, false, true, new List<string> {"b", "build"},
				new List<string>
				{
					"build",
					"one",
					"multiple",
					"solutions",
					"prod",
					"debug",
					"specified",
					"mode",
					"prompt",
					"launch",
					"after",
					"building",
					"trickler",
					"unit",
					"tests",
					"fifo",
					"order",
					"full"
				});
		}

		/// <summary>
		/// tests <see cref="BuildCommand.BuildSolution" />
		/// </summary>
		[Test]
		[TestCase(false, false, "Building good - Success\r\n", TestName = "Success, no mode")]
		[TestCase(true, false, "Building good happy - Success\r\n", TestName = "Success, mode")]
		[TestCase(false, true, "Building goodthe build failed\r\ngood failed\r\n", TestName = "Failure, no mode")]
		[TestCase(true, true, "Building good happythe build failed\r\ngood failed\r\n", TestName = "Failure, mode")]
		public void TestBuildSolution(bool printMode, bool failBuild, string output)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var goCalled = false;
			var sortCalled = false;

			void Go(string str) => goCalled = true;

			void Sort(string str) => sortCalled = true;

			Env.SetEnvironmentVariable("mode", "happy", true);
			_rbfCalled = 0;
			Shortcuts.Instance.LoadShortcuts("hank");
			InitBc(new ProgStub(failBuild), true, null, null, null, Go, Sort);
			var result = BuildCommand.BuildSolution("good", printMode, false);
			Assert.IsTrue(goCalled, "nothing should prevent Go from being called");
			Assert.AreEqual(1, _rbfCalled, "This should always be called");
			Assert.AreNotEqual(failBuild, sortCalled, "this should be called if the build was successful");
			Assert.AreNotEqual(failBuild, result, "this should always succeed if the build does");

			Assert.AreEqual(output, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests BuildUnitTests
		/// </summary>
		[Test]
		public void TestBuildUnitTests()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			void Go(string str) { }

			void Sort(string str) { }

			Env.SetEnvironmentVariable("mode", "happy", true);
			_rbfCalled = 0;
			Shortcuts.Instance.LoadShortcuts("fred");
			InitBc(new ProgStub(false), true, null, null, null, Go, Sort);
			var result = BuildCommand.BuildSolution("base", false, false);
			Assert.AreEqual(2, _rbfCalled, "This should always be called twice");
			Assert.AreNotEqual(false, result, "this should always succeed if the build does");

			Assert.AreEqual("Building base and tests - Success\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests CopyIniFiles and CopyIniFilesIfNecessary
		/// </summary>
		[Test]
		[TestCase(true, true, true, true, TestName = "No Firsttime, CORE, inis, bin")]
		[TestCase(false, true, true, true, TestName = "Firsttime, CORE, inis, bin")]
		[TestCase(true, false, true, true, TestName = "No Firsttime, no CORE, inis, bin")]
		[TestCase(true, true, false, true, TestName = "No Firsttime, CORE, no inis, bin")]
		[TestCase(true, true, true, false, TestName = "No Firsttime, CORE, inis, no bin")]
		public void TestCopyIniFiles(bool noFirst, bool core, bool inis, bool bin)
		{
			Env.SetEnvironmentVariable("local", "a", true);
			Env.SetEnvironmentVariable("version_tree", "b", true);
			Env.SetEnvironmentVariable("mode", "c", true);

			var files = new FilesStub(noFirst ? "bad" : "");
			var dirs = new DirsStub(core, inis, bin);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(dirs);

			InitBc(new ProgStub(false), false, null, null, null, null, null);

			BuildCommand.DoBuild(false, false, false, new List<string>(), false);

			Assert.AreEqual(bin ? "" : @"ab\bin\c", dirs.CreatedDirectory, "The wrong value for created directory");
			var shouldCopy = noFirst && core && inis;
			var expected = shouldCopy ? new List<string> {"1", "2", "3", "4"} : new List<string>();
			var msg = shouldCopy ? "List should have had 4 elements, 1, 2, 3, 4" : "List should have been empty";
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(expected, files.Copied), msg);
		}

		/// <summary>
		/// tests <see cref="BuildCommand.DoBuild" /> when building all solutions
		/// </summary>
		[Test]
		[TestCase(false, true, "Building all\r\n", TestName = "Success, No mode")]
		[TestCase(true, true, "Building all happy\r\n", TestName = "Success, mode")]
		[TestCase(false, false, "Building all\r\nthe build failed\r\n", TestName = "Failure, no mode")]
		public void TestDoBuildAll(bool printMode, bool success, string output)
		{
			var cons = new ConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			Env.SetEnvironmentVariable("mode", "happy", true);
			_rbfCalled = 0;
			InitBc(new ProgStub(!success), false, null, null, null, null, null);
			BuildCommand.DoBuild(false, false, false, new List<string>(), printMode);
			var dirs = IoCContainer.Instance.Resolve<IDirectories>() as DirsStub;
			Assert.IsNotNull(dirs);
			Assert.AreEqual(1, _rbfCalled, "This should always be called");
			Assert.IsTrue(dirs.DirRestored, "for some reason the directory wasn't restored to its previous value");
			Assert.IsTrue(dirs.SetCalled, "SetDirectory wasn't even called");
			Assert.IsTrue(cons.BeepCalled, "beep should be called on a build all regardless of success");

			Assert.AreEqual(output, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="BuildCommand.DoBuild" /> when building specified solutions
		/// </summary>
		[Test]
		[TestCaseSource(nameof(DoBuildIndividuallyTestData))]
		public void TestDoBuildIndividually(bool failBuild, int rfb, List<string> args, string expected)
		{
			var cons = new ConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var xm8Called = false;

			void Xm8() => xm8Called = true;

			_rbfCalled = 0;
			Shortcuts.Instance.LoadShortcuts("hank");
			InitBc(new ProgStub(failBuild), false, Xm8, null, null, path => { }, null);
			BuildCommand.DoBuild(true, false, false, args, false);

			var dirs = IoCContainer.Instance.Resolve<IDirectories>() as DirsStub;
			Assert.IsNotNull(dirs);
			Assert.AreEqual(rfb, _rbfCalled, "_rfbCalled had the wrong value");
			Assert.AreEqual(rfb > 0 && !failBuild, xm8Called, "xm8Called had the wrong value");
			Assert.IsTrue(dirs.DirRestored, "for some reason the directory wasn't restored to its previous value");
			Assert.IsTrue(dirs.SetCalled, "SetDirectory wasn't even called");

			Assert.IsFalse(cons.BeepCalled, "beep should not be called on a build regardless of success");

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests the other actions that can be triggered (Xm8, Trk, Unt)
		/// </summary>
		[Test]
		[TestCase(false, false, false)]
		[TestCase(true, false, false)]
		[TestCase(false, true, false)]
		[TestCase(false, false, true)]
		[TestCase(true, true, false)]
		[TestCase(false, true, true)]
		[TestCase(true, false, true)]
		[TestCase(true, true, true)]
		public void TestOtherActions(bool xm8, bool trk, bool unt)
		{
			var xm8Called = false;
			var trkCalled = false;
			var untCalled = false;

			void Xm8()
			{
				xm8Called = true;
			}

			void Trk()
			{
				trkCalled = true;
			}

			void Unt(List<string> args)
			{
				untCalled = true;
			}

			InitBc(new ProgStub(false), false, Xm8, Trk, Unt, null, null);
			BuildCommand.DoBuild(xm8, trk, unt, new List<string>(), false);

			OtherTest(nameof(Xm8), xm8, xm8Called);
			OtherTest(nameof(Trk), trk, trkCalled);
			OtherTest(nameof(Unt), unt, untCalled);
		}

		/// <summary>
		/// tests <see cref="BuildCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(false, false, "Building all\r\n", TestName = "no mode")]
		[TestCase(true, false, "Building all prod\r\n", TestName = "prod")]
		[TestCase(false, true, "Building all debug\r\n", TestName = "debug")]
		[TestCase(true, true, "Building all prod\r\nBuilding all debug\r\n", TestName = "both")]
		public void TestPerform(bool prod, bool debug, string output)
		{
			var cons = new ConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var args = new List<string>();

			if (prod)
			{
				args.Add("-p");
			}

			if (debug)
			{
				args.Add("-d");
			}
			
			Env.SetEnvironmentVariable("mode", "fred", true);
			var b = new BuildCommand(false, null, null, null, null, null);
			b.Perform(args);
			Assert.AreEqual("fred", Env.Mode, "mode was not restored");

			Assert.AreEqual(output, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="BuildCommand.ShouldSortAfterBuild" />
		/// </summary>
		[Test]
		[TestCase(true, true, true, true, TestName = "Should sort")]
		[TestCase(false, true, true, true, TestName = "Don't sort from resx")]
		[TestCase(true, false, true, true, TestName = "Don't sort failed builds")]
		[TestCase(true, true, false, true, TestName = "Don't sort if we shouldn't")]
		[TestCase(true, true, true, false, TestName = "Don't sort if it isn't res solution")]
		public void TestShouldSortResx(bool notResx, bool success, bool shouldSort, bool isRes)
		{
			Shortcuts.Instance.LoadShortcuts("hank");
			Assert.AreEqual(notResx && success && shouldSort && isRes, BuildCommand.ShouldSortAfterBuild(!notResx, success, shouldSort, isRes ? "good" : "bad"));
		}

		#endregion Tests

		#region Helpers

		private static IEnumerable DoBuildIndividuallyTestData
		{
			get
			{
				yield return new TestCaseData(false, 0, new List<string> {"bogus", "fakey"},
					"bogus was not found in the shortcuts\r\nfakey was not found in the shortcuts\r\n");
				yield return new TestCaseData(false, 1, new List<string> {"good"}, "Building good - Success\r\n");
				yield return new TestCaseData(true, 1, new List<string> {"good"},
					"Building goodthe build failed\r\ngood failed\r\n");
			}	// end get
		}	// end DoBuildIndividuallyTestData

		private static void InitBc(ProgStub prog, bool shouldSort, Action xm8, Action trk,
			Action<List<string>> unt, Action<string> go, Action<string> srt)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			// ReSharper disable once ObjectCreationAsStatement - need to initialize some variables on BuildCommand
			new BuildCommand(shouldSort, xm8, trk, unt, go, srt);
		}

		private static void OtherTest(string str, bool expected, bool actual)
		{
			Assert.AreEqual(expected, actual, $"{str} {Was(actual)} set when it {Should(expected)} have been");
		}

		private static string Should(bool val) => val ? "should" : "shouldn't";

		private static string Was(bool val) => val ? "was" : "wasn't";

		#endregion Helpers

		#region Inner Classes

		private class DirsStub : BaseDirsStub
		{
			#region Variables

			private readonly bool _coreExists;
			private readonly bool _returnInis;
			private readonly bool _binExists;

			public bool DirRestored;
			public bool SetCalled;
			public string CreatedDirectory;

			#endregion Variables

			#region Implementation of IDirectories

			/// <inheritdoc />
			public override void CreateDirectory(string path) => CreatedDirectory = path;

			/// <inheritdoc />
			public override bool Exists(string path)
			{
				switch (path)
				{
					case @"c:\Program Files\Xactware\XactimateDesktop\CORE":
						return _coreExists;
					case @"ab\bin\c":
						return _binExists;
					default:
						return false;
				}
			}

			/// <inheritdoc />
			public override string GetCurrentDirectory() => "dir";

			/// <inheritdoc />
			public override List<string> GetFiles(string path, string filter)
			{
				return "*.ini".Equals(filter)
					? (_returnInis ? new List<string> {"1", "2", "3", "4"} : new List<string>())
					: null;
			}

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path)
			{
				SetCalled = true;
				DirRestored = path.Equals("dir");
			}

			#endregion Implementation of IDirectories

			#region Constructor

			public DirsStub(bool coreExists = false, bool returnInis = false, bool binExists = false)
			{
				CreatedDirectory = "";
				SetCalled = false;
				DirRestored = false;

				_coreExists = coreExists;
				_returnInis = returnInis;
				_binExists = binExists;
			}

			#endregion Constructor
		}

		private class FilesStub : BaseFileStub
		{
			#region Variables

			private readonly string _testExists;
			public readonly List<string> Copied;

			#endregion Variables

			#region Implementation of IFiles

			/// <inheritdoc />
			public override void Copy(string source, string destination)
			{
				Copied.Add(source.Substring(source.LastIndexOf('\\') + 1));
			}

			/// <inheritdoc />
			public override bool Exists(string path) => _testExists.IsNullOrEmpty() || _testExists.Equals(path);

			/// <inheritdoc />
			public override IEnumerable<string> ReadLines(string path)
			{
				if ("hank".Equals(path))
					return new List<string> {"good=whateverres"};
				if ("fred".Equals(path))
					return new List<string> {"base=howdy", "baseut=howdyut"};
				return new List<string>();
			}

			#endregion Implementation of IFiles

			#region Constructor

			public FilesStub(string testExists = null)
			{
				_testExists = testExists;
				Copied = new List<string>();
			}

			#endregion Constructor
		}

		private class ProgStub : BaseProgStub
		{
			#region Variable

			private readonly bool _failBuild;

			#endregion Variable

			#region Implementation of IProgram

			/// <inheritdoc />
			public override bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true,
				bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				_rbfCalled++;
				if (!_failBuild)
				{
					return true;
				}

				Out.Line("the build failed");
				return false;
			}

			#endregion Implementation of IProgram

			#region Constructor

			public ProgStub(bool failBuild) => _failBuild = failBuild;

			#endregion Constructor
		}

		private class ConsStub : BaseConsStub
		{
			internal bool BeepCalled;

			#region Overrides of BaseConsStub

			/// <inheritdoc />
			public override void Beep() => BeepCalled = true;

			#endregion Overrides of BaseConsStub
		}

		#endregion Inner Classes
	}
}
