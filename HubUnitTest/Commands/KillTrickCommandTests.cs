﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Commands;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="KillTrickCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class KillTrickCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="KillTrickCommand.Description" />, <see cref="KillTrickCommand.NeedShortcut" />,
		/// <see cref="KillTrickCommand.Inputs" /> and <see cref="KillTrickCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new KillTrickCommand(), new List<string> {"kill", "trickler"}, false, false,
				new List<string> {"kt", "killt"}, new List<string> {"kills", "trickler", "process", "delete", "folders"});
		}

		/// <summary>
		/// tests <see cref="AutoLogCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var dirs = new StubDirectory();
			var prog = new ProgStub();

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, StubDirectory>(dirs);
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(new Mock<IConsole>().Object);

			var cmd = new KillTrickCommand();
			cmd.Perform(null);
			Assert.AreEqual("DataMigrationTool", prog.Killed);
			Assert.IsTrue(dirs.Deleted.Contains(@"C:\ProgramData\Xactware\Xactimate28\DataMigrationTool"));
			Assert.IsTrue(dirs.Deleted.Contains(@"C:\ProgramData\Xactware\Xactimate28\DataMigration"));
			Assert.IsTrue(dirs.Deleted.Contains(@"C:\ProgramData\Xactware\Xactimate29\DataMigration"));
			Assert.IsTrue(dirs.Deleted.Contains(@"C:\ProgramData\Xactware\DataMigrationTool"));
			Assert.IsTrue(dirs.Deleted.Contains(@"C:\ProgramData\Xactware\DataMigration"));

			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Classes

		private class StubDirectory : BaseDirsStub
		{
			#region Property

			internal List<string> Deleted { get; } = new List<string>();

			#endregion Property

			#region Implementation of IDirectories

			/// <inheritdoc />
			public override void Delete(string path) => Deleted.Add(path);

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			#endregion Implementation of IDirectories
		}

		private class ProgStub : BaseProgStub
		{
			#region Variable

			internal string Killed;

			#endregion Variable

			#region Implementation of IProgram

			/// <inheritdoc />
			public override void KillProcess(string name) => Killed = name;

			#endregion Implementation of IProgram
		}

		#endregion Inner Classes
	}
}
