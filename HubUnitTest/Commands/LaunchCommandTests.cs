﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="LaunchCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class LaunchCommandTests
	{
		#region Variables

		private bool _buildCalled;
		private bool _trickCalled;

		private bool? _xm8, _tric, _test, _mode;
		private int _argCount;

		private static bool _bin;

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests <see cref="LaunchCommand.Description" />, <see cref="LaunchCommand.NeedShortcut" />, <see cref="LaunchCommand.Inputs" />
		/// and <see cref="LaunchCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new LaunchCommand(null, null), new List<string> {"launch", "xactimate"}, false, true,
				new List<string> {"x", "l", "launch"}, new List<string> {"launches", "xactimate", "building", "solutions", "fifo"});
		}

		/// <summary>
		/// tests <see cref="LaunchCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(0, false, null, null, 0, false)]
		[TestCase(1, false, null, null, 0, true)]
		[TestCase(2, false, null, null, 0, false)]
		[TestCase(3, false, null, null, 0, true)]
		[TestCase(4, true, false, false, 1, false)]
		[TestCase(5, true, true, false, 1, false)]
		[TestCase(6, true, false, true, 1, false)]
		[TestCase(7, true, true, true, 1, false)]
		[TestCase(8, true, false, false, 2, false)]
		[TestCase(9, true, true, false, 2, false)]
		[TestCase(10, true, false, true, 2, false)]
		[TestCase(11, true, true, true, 2, false)]
		public void TestPerform(int which, bool bc, bool? tr, bool? te, int ct, bool tc)
		{
			List<string> args = null;

			switch (which)
			{
				case 0:
					args = new List<string>();
					break;
				case 1:
					args = new List<string> {"-tr"};
					break;
				case 2:
					args = new List<string> {"-t"};
					break;
				case 3:
					args = new List<string> {"-tr", "-t"};
					break;
				case 4:
					args = new List<string> {"a"};
					break;
				case 5:
					args = new List<string> {"-tr", "a"};
					break;
				case 6:
					args = new List<string> {"-t", "a"};
					break;
				case 7:
					args = new List<string> {"-tr", "-t", "a"};
					break;
				case 8:
					args = new List<string> {"a", "b"};
					break;
				case 9:
					args = new List<string> {"-tr", "a", "b"};
					break;
				case 10:
					args = new List<string> {"-t", "a", "b"};
					break;
				case 11:
					args = new List<string> {"-tr", "-t", "a", "b"};
					break;
			}

			ResetForTest();
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);

			var cmd = new LaunchCommand(Build, Trick);
			cmd.Perform(args);

			Assert.AreEqual(bc, _buildCalled, nameof(_buildCalled));
			Assert.AreEqual(bc, _xm8.HasValue && _xm8.Value, nameof(_xm8));
			Assert.AreEqual(tr, _tric, nameof(_tric));
			Assert.AreEqual(te, _test, nameof(_test));
			Assert.AreEqual(ct, _argCount, nameof(ct));
			Assert.IsFalse(_mode.HasValue && _mode.Value, nameof(_mode));
			Assert.AreEqual(tc, _trickCalled, nameof(_trickCalled));
			Assert.IsTrue(_bin, nameof(_bin));

			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
		}

		#endregion Tests

		#region Helpers

		private void Build(bool xm8, bool tric, bool test, List<string> solutions, bool printMode)
		{
			_buildCalled = true;
			_xm8 = xm8;
			_tric = tric;
			_test = test;
			_mode = printMode;
			_argCount = solutions.Count;

			LaunchCommand.DoLaunch();
		}

		private void Trick() => _trickCalled = true;

		private void ResetForTest()
		{
			_argCount = 0;
			_buildCalled = _trickCalled = false;
			_xm8 = _tric = _test = _mode = null;
			_bin = false;
		}
		
		#endregion Helpers

		#region Inner Class

		private class ProgStub : BaseProgStub
		{
			#region Implementation of IProgram

			/// <inheritdoc />
			public override void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true,
				ShowOutput show = ShowOutput.No)
			{
				_bin = true;
			}

			#endregion Implementation of IProgram
		}

		#endregion Inner Class
	}
}
