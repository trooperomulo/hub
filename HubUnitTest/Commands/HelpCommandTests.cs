﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Decider.Command;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="HelpCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class HelpCommandTests
	{
		#region Constant

		private const string ShowHelpExpected = "-------------------------------------------------------------\r\n\r\n" +
		                                        "Please use one of the following commands:\r\n\r\n" +
		                                        "CmdA\t\t\t\ta\taa\taaa\r\nCmdB\t\t\t\tb\tbb\tbbb\r\n" +
		                                        "-------------------------------------------------------------\r\n";

		#endregion Constant

		#region Variables

		private static bool _a;
		private static bool _b;

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests <see cref="HelpCommand.Description" />, <see cref="HelpCommand.NeedShortcut" />,
		/// <see cref="HelpCommand.Inputs" /> and <see cref="HelpCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new HelpCommand(null, null), new List<string> {"displays", "help"}, false, false,
				new List<string> {"help", "h", "?"}, new List<string> {"display", "detailed", "help", "command", "fifo"});
		}

		/// <summary>
		/// tests <see cref="HelpCommand.GetHelpCommandsWithArgs" />
		/// </summary>
		[Test]
		public void TestGetHelpCommandsWithArgs()
		{
			var subs = new List<string> {"a", "b", "c", "d", "e", "f"};
			var nons = new List<string> {"g", "h", "i", "j", "k", "l"};
			var cmd = new HelpCommand(subs, nons);
			var args = new List<string> {"a", "123", "909", "b", "h", "q", "f", "q", "r", "s", "t", "u", "v"};
			var groups = cmd.GetHelpCommandsWithArgs(args);
			var expected = new List<List<string>>
			{
				new List<string>{"a","123","909"},
				new List<string>{"b"},
				new List<string>{"h"},
				new List<string>{"q"},
				new List<string>{"f", "q", "r", "s", "t", "u", "v"}
			};
			Assert.AreEqual(expected.Count, groups.Count, "main lists aren't the same size");
			for (var i = 0; i < groups.Count; i++)
			{
				var gr = groups[i];
				var ex = expected[i];
				Assert.AreEqual(ex.Count, gr.Count, "sub lists aren't the same size");
				for (var j = 0; j < gr.Count; j++)
				{
					Assert.AreEqual(ex[j], gr[j], "individual items are different");
				}
			}
		}

		/// <summary>
		/// tests <see cref="HelpCommand.Perform" /> with null and empty args
		/// </summary>
		[Test]
		[TestCase(false)]
		[TestCase(true)]
		public void TestPerformNullEmptyArgs(bool empty)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			CommandDecider.InitCommands(new List<ICommand> {new CmdA(), new CmdB()}, null, null);
			var args = empty ? new List<string>() : null;
			var cmd = new HelpCommand(null, null);
			cmd.Perform(args);

			Assert.AreEqual(ShowHelpExpected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="HelpCommand.Perform" /> with command inputs passed in
		/// </summary>
		[Test]
		[TestCase(0, true, false)]
		[TestCase(1, false, true)]
		[TestCase(2, true, true)]
		public void TestPerformCommands(int which, bool a, bool b)
		{
			var args = new List<string>();
			if (which % 2 == 0)
			{
				args.Add("a");
			}

			if (which > 0)
			{
				args.Add("b");
			}

			_a = _b = false;
			var ca = new CmdA();
			var cb = new CmdB();
			var nonSubs = new List<string>();
			nonSubs.AddRange(ca.Inputs);
			nonSubs.AddRange(cb.Inputs);
			CommandDecider.InitCommands(new List<ICommand> {ca, cb}, null, null);
			var cmd = new HelpCommand(new List<string>(), nonSubs);
			cmd.Perform(args);
			Assert.AreEqual(a, _a);
			Assert.AreEqual(b, _b);
		}
		
		/// <summary>
		/// tests <see cref="HelpCommand.ShowHelp" />
		/// </summary>
		[Test]
		public void TestShowHelp()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			CommandDecider.InitCommands(new List<ICommand> {new CmdA(), new CmdB()}, null, null);

			HelpCommand.ShowHelp();

			Assert.AreEqual(ShowHelpExpected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Classes

		private class CmdA : BaseCmdStub
		{
			#region Implementation of IInput

			/// <inheritdoc />
			public override List<string> Inputs => new List<string> {"a", "aa", "aaa"};

			#endregion Implementation of IInput

			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "CmdA";

			/// <inheritdoc />
			public override void Help(List<string> args) => _a = true;

			#endregion Implementation of ICommand
		}

		private class CmdB : BaseCmdStub
		{
			#region Implementation of IInput

			/// <inheritdoc />
			public override List<string> Inputs => new List<string> {"b", "bb", "bbb"};

			#endregion Implementation of IInput

			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "CmdB";

			/// <inheritdoc />
			public override void Help(List<string> args) => _b = true;

			#endregion Implementation of ICommand
		}

		#endregion Inner Classes
	}
}
