﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="DbCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class DbCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="DbCommand.Description" />, <see cref="DbCommand.NeedShortcut" />, <see cref="DbCommand.Inputs" />
		/// and <see cref="DbCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new DbCommand(), new List<string> { "db", "tool", "launch" }, false, false,
				new List<string> { "db", "d" }, new List<string> { "database", "tool", "support", "launch" });
		}

		/// <summary>
		/// tests <see cref="DbCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new DbCommand();
			cmd.Perform(null);
			Assert.AreEqual("XactSupport.exe", prog.Process);
			Assert.AreEqual("XactSupport Database tool", prog.Label);
			Assert.IsFalse(prog.Wait);
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
		}

		#endregion Tests

		#region Inner Class

		private class ProgStub : BaseProgStub
		{
			#region Variable

			internal string Process;
			internal string Label;
			internal bool Wait;

			#endregion Variable

			#region Implementation of IProgram

			/// <inheritdoc />
			public override void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true,
				ShowOutput show = ShowOutput.No)
			{
				Process = filename;
				Label = label;
				Wait = wait;
			}

			#endregion Implementation of IProgram
		}

		#endregion Inner Class
	}
}
