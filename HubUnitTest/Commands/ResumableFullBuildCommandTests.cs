﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="ResumableFullBuildCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResumableFullBuildCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="ResumableFullBuildCommand.Description" />, <see cref="ResumableFullBuildCommand.HasSubCommands"/>, <see cref="ResumableFullBuildCommand.NeedShortcut" />,
		/// <see cref="ResumableFullBuildCommand.Inputs" /> and <see cref="ResumableFullBuildCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new ResumableFullBuildCommand(),
				new List<string> {"full", "build", "runs", "resumable"}, false, false,
				new List<string> {"resume", "rfull", "rb"},
				new List<string> {"resume", "fixing", "broken", "solution"});
		}

		/// <summary>
		/// tests <see cref="ResumableFullBuildCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var cons = new Mock<IConsole>();
			cons.Setup(c => c.Beep());

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);
			var rfb = new ResumableFullBuildCommand();
			rfb.Perform(null);

			cons.Verify(c => c.Beep(), Times.Once, "the final beep wasn't called");
			Assert.AreEqual("medusa.exe -o build.xml", prog.Args, "thw wrong args were called");

			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Classes

		private class ProgStub : BaseProgStub
		{
			#region Variable

			internal string Args;

			#endregion Variable

			#region Overrides of BaseProgStub

			/// <inheritdoc />
			public override bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true, bool rse = false,
				bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				Assert.IsTrue(Args == null, "Run batch file was called twice");

				Args = fileAndArgs;
				return true;
			}

			#endregion Overrides of BaseProgStub
		}

		#endregion Inner Classes
	}
}
