﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="OewCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class OewCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="OewCommand.Description" />, <see cref="OewCommand.NeedShortcut" />, <see cref="OewCommand.Inputs" />
		/// and <see cref="OewCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new OewCommand(), new List<string> { "oew", "launches" }, false, false,
				new List<string> { "oew", "o" }, new List<string> { "online", "estimate", "writer", "tool", "launches" });
		}

		/// <summary>
		/// tests <see cref="OewCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var prog = new ProgStub();
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			var cmd = new OewCommand();
			cmd.Perform(null);
			var expected = Path.Combine(Env.Root, "xactimate.oew", "bin", "debug", "Xactimate online Estimate Writer.exe");
			Assert.AreEqual(expected, prog.Path);
			Assert.AreEqual("Launching Online Estimate Writer\r\n", cons.Output);
			Assert.IsFalse(prog.Wait);
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Class

		private class ProgStub : BaseProgStub
		{
			#region Variable

			internal string Path;
			internal bool Wait;

			#endregion Variable

			#region Implementation of IProgram

			/// <inheritdoc />
			public override bool RunProgram(string path, string arguments = null, ShowOutput show = ShowOutput.No, bool rso = true,
				bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				Path = path;
				Wait = wait;
				return true;
			}
			
			#endregion Implementation of IProgram
		}

		#endregion Inner Class
	}   // end OewCommandTests

}	// end HubUnitTest.Commands
