﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Moq;

using NUnit.Framework;

using Hub;
using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxSCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxSCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="ResxSCommand.Description" />, <see cref="ResxSCommand.Input" />,
		/// <see cref="ResxSCommand.ShowAll" /> and <see cref="ResxSCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestResxCommand(new ResxSCommand(), new List<string> {"sort", "files"}, "-s", false,
				new List<string> {"[-c/-x/-s]", "used", "sort", "core.data.res", "xm8.data.res", "prompted"});
		}

		/// <summary>
		/// tests <see cref="ResxSCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestPerform(bool shouldWarn)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var files = new List<string> {"1", "2", "3", "4", "5", "6"};

			var dirs = new Mock<IDirectories>();

			dirs.Setup(d => d.GetCurrentDirectory()).Returns("dir");
			dirs.Setup(d => d.GetFiles($@"{Env.Root}\path", "*.resx")).Returns(files);

			var called = 0;

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);

			ResxBase.Initialize(null);
			var ress = new ResxSCommand();
			ResxSCommand.SortResFile = str =>
			{
				Assert.IsTrue(files.Contains(str), "should have been called with somthing from files");
				called++;
				return shouldWarn;
			};

			ress.Perform(new List<string> {"eh", "what", "ever"}, -3, Res.None, "path");

			Assert.AreEqual(shouldWarn ? "At least one file didn't get its comment copied over\r\n" : null, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

			Assert.AreEqual(files.Count, called, "SortResFile wasn't called the right number of times");
			dirs.Verify(d => d.GetCurrentDirectory(), Times.Never);
			dirs.Verify(d => d.GetFiles($@"{Env.Root}\path", "*.resx"), Times.Once);

			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		/// <summary>
		/// tests <see cref="ResxSCommand.SortResxFiles" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestSortResxFiles(bool shouldWarn)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var files = new List<string> {"1", "2", "3", "4", "5", "6"};

			var dirs = new Mock<IDirectories>();

			dirs.Setup(d => d.GetCurrentDirectory()).Returns("dir");
			dirs.Setup(d => d.GetFiles($@"{Env.Root}\dir", "*.resx")).Returns(files);

			var called = 0;

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);

			ResxBase.Initialize(null);
			ResxSCommand.SortResFile = str =>
			{
				Assert.IsTrue(files.Contains(str), "should have been called with somthing from files");
				called++;
				return shouldWarn;
			};

			ResxSCommand.SortResxFiles("sol");

			var output = "Sorting sol - done\r\n";
			if (shouldWarn)
			{
				output += "At least one file didn't get its comment copied over\r\n";
			}

			Assert.AreEqual(output, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

			Assert.AreEqual(files.Count, called, "SortResFile wasn't called the right number of times");
			dirs.Verify(d => d.GetCurrentDirectory(), Times.Once);
			dirs.Verify(d => d.GetFiles($@"{Env.Root}\dir", "*.resx"), Times.Once);

			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		#endregion Tests
	}
}
