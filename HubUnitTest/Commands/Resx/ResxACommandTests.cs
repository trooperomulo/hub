﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Resources;

using Hub;
using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using RyanCoreTests;

using kv = System.Collections.Generic.List<(string key, string value)>;
using Rnd = HubUnitTest.RandomData;
using UTU = HubUnitTest.UnitTestUtil;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxACommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxACommandTests
	{
		#region Tests

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFileStub>(new BaseFileStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(new Mock<IWaitForUser>().Object);
		}

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();
		}

		/// <summary>
		/// tests <see cref="ResxACommand.Description" />, <see cref="ResxACommand.Input" />,
		/// <see cref="ResxACommand.ShowAll" /> and <see cref="ResxACommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UTU.TestResxCommand(new ResxACommand(0, null), new List<string> { "add", "new" }, "-a", false,
				new List<string>
				{
					"[-c/-x/-s]",
					"key value",
					"filepath",
					"add",
					"resource",
					"resx",
					"core.data.res",
					"xm8.data.res",
					"prompted",
					"value",
					"en-us",
					"unique",
					"strings",
					"alternative",
					"suggested",
					"duplicate",
					"existing",
					"translations",
					"sort"
				});
		}

		/// <summary>
		/// tests <see cref="ResxACommand.AddTranslation" />
		/// </summary>
		[Test]
		[TestCase(false, false, false)]
		[TestCase(false, false, true)]
		[TestCase(false, true, false)]
		[TestCase(false, true, true)]
		[TestCase(true, false, false)]
		[TestCase(true, false, true)]
		[TestCase(true, true, false)]
		[TestCase(true, true, true)]
		public void TestAddTranslation(bool useEnUs, bool exists, bool lastKey)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var folder = UTU.GetTestPath("", nameof(ResxACommand));
			var key = Rnd.GetRandomStr();
			var lang = useEnUs ? "en-US" : Rnd.GetRandomStr();
			var val = Rnd.GetRandomStr();
			var file = useEnUs ? $@"{folder}\strings.resx" : $@"{folder}\strings.{lang}.resx";
			var called = 0;

			var files = new Mock<IFiles>();
			files.Setup(f => f.Exists(AnyString)).Returns(exists);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);

			ResxBase.Initialize(null);

			var resa = GetMock();
			resa.Setup(a => a.PopulateResxWriterAndDictionaries(AnyString, AnyWriter, AnyListStr)).Callback(
				(string f, ResXResourceWriter w, List<string> a) =>
				{
					ResxACommand.SortResFile = str =>
					{
						called++;
						Assert.AreEqual(file, str, "the wrong file was passed to the sorter");
						return true;
					};
				});

			Directory.CreateDirectory(folder);

			resa.Object.AddTranslation(folder, key, lang, val, lastKey);

			if (exists)
			{
				var pairs = GetPairsFromReader(file);
				Assert.AreEqual(1, pairs.Count, "the wrong number of pairs were written out");
				Assert.IsTrue(pairs.Contains((key, val)), "the wrong pair was added");
			}

			files.Verify(f => f.Exists(file), Times.Once);
			CoreTestUtils.MoqMethodWasCalled(resa,
				a => a.PopulateResxWriterAndDictionaries(file, AnyWriter, new List<string> {key}), exists);
			Assert.AreEqual(lastKey && exists ? 1 : 0, called, "sort wasn't called the right number of times");

			File.Delete(file);

			string expected = null;
			if (exists)
			{
				expected = $"Adding translation to {lang}{(lastKey ? " - Sorting" : "")} - Done\r\n";
			}

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ResxACommand.AddTranslations" />
		/// </summary>
		[Test]
		public void TestAddTranslations()
		{
			var resa = GetMock();
			resa.Setup(a => a.AddTranslation(AnyString, AnyString, AnyString, AnyString, AnyBool));

			var folder = Rnd.GetRandomStr();
			var key = Rnd.GetRandomWord();
			var translations = GetRandomTranslationsList();
			var isLast = Rnd.Bool();

			resa.Object.AddTranslations(folder, key, translations, isLast);

			VerifyAddTranslationsCalls(resa, folder, key, translations, isLast);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.BuildSolutionAfterResx" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestBuildSolutionAfterResx(bool hasSolution)
		{
			var path = Rnd.GetRandomStr();
			var solution = hasSolution ? Rnd.GetRandomStr() : "";

			bool Build(string sol, bool printMode, bool fromResx)
			{
				Assert.IsTrue(hasSolution, "build shouldn't be called if there isn't a solution");
				Assert.AreEqual(false, printMode, "we never print the mode from resx");
				Assert.AreEqual(true, fromResx, "this is use calling from resx");
				Assert.AreEqual(solution, sol, "the solution should have just been passed straight across");
				return true;
			}

			var resa = GetMock(Build);
			resa.Setup(a => a.GetShortcutFromPath(AnyString)).Returns(solution);

			resa.Object.BuildSolutionAfterResx(path);

			resa.Verify(a => a.GetShortcutFromPath(path), Times.Once);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.DisplayUnaddedPairs" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetDisplayUnaddedPairsTestData))]
		public void TestDisplayUnaddedPairs(int num)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var ktv = Rnd.GetRandomDictStringString();
			var vtk = Rnd.GetRandomDictStrListStr();

			var pairs = GetUnaddedPairs(num, ktv, vtk);

			var expected = GetUnaddedPairsExpected(pairs, ktv, vtk);

			var resa = new ResxACommand(0, null);

			resa.DisplayUnaddedPairs(ktv, vtk, pairs);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ResxACommand.DoResAPairs" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestDoResAPairs(bool someAdded)
		{
			Env.UseRepo = false;
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var pairs = Rnd.GetRandomListStrStr();
			var len = someAdded ? Rnd.Int(1, pairs.Count) : pairs.Count;
			var unadded = Rnd.GetRandomListStrStrSetLength(len);
			var path = "temp";
			var file = $@"{Env.Root}\{path}\strings.resx";
			var called = 0;

			ResXResourceWriter writer = null;
			Dictionary<string, string> ktv = null;
			Dictionary<string, List<string>> vtk = null;

			var resa = GetMock();
			resa.Setup(a =>
				a.PopulateResxWriterAndDictionaries(AnyString, AnyWriter, AnyDictStrStr, AnyDictStrListStr)).Callback(
				(string s, ResXResourceWriter w, Dictionary<string, string> k, Dictionary<string, List<string>> v) =>
				{
					writer = w;
					ktv = k;
					vtk = v;
					ResxACommand.SortResFile = str =>
					{
						Assert.AreEqual(file, str, "the wrong path was used for sorting");
						called++;
						return true;
					};

				});
			resa.Setup(a => a.ProcessAddPairs(AnyWriter, AnyListStrStr, AnyDictStrStr, AnyDictStrListStr))
				.Returns(unadded);
			resa.Setup(a => a.DisplayUnaddedPairs(AnyDictStrStr, AnyDictStrListStr, AnyListStrStr));
			resa.Setup(a => a.BuildSolutionAfterResx(AnyString));

			Directory.CreateDirectory(@"c:\git\xactimate\temp");

			resa.Object.DoResAPairs(path, pairs);

			Directory.Delete(@"c:\git\xactimate\temp", true);

			resa.Verify(a => a.PopulateResxWriterAndDictionaries(file, AnyWriter, AnyDictStrStr, AnyDictStrListStr), Times.Once);
			resa.Verify(a => a.ProcessAddPairs(writer, pairs, ktv, vtk), Times.Once);
			resa.Verify(a => a.DisplayUnaddedPairs(ktv, vtk, unadded), Times.Once);
			CoreTestUtils.MoqMethodWasCalled(resa, a => a.BuildSolutionAfterResx(path), someAdded);
			Assert.AreEqual(someAdded ? 1 : 0, called, "Sort was called the wrong number of times");

			Assert.AreEqual(someAdded ? null : "Nothing was added\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ResxACommand.DoResAPath" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestDoResAPath(bool noTrans)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var path = Rnd.GetRandomStr();
			var file = Rnd.GetRandomStr();
			var folder = $@"{Env.Root}\{path}";
			var trans = noTrans ? null : Rnd.GetRandomDictStrListStrStr();
			var resa = GetMock();
			resa.Setup(a => a.ReadResxAddFile(AnyString)).Returns(trans);
			resa.Setup(a => a.AddTranslations(AnyString, AnyString, AnyListStrStr, AnyBool));
			resa.Setup(a => a.BuildSolutionAfterResx(AnyString));

			resa.Object.DoResAPath(path, file);

			resa.Verify(a => a.ReadResxAddFile(file), Times.Once);
			VerifyDoResAPathAddTranslation(resa, folder, trans);
			CoreTestUtils.MoqMethodWasCalled(resa, a => a.BuildSolutionAfterResx(path), !noTrans);

			Assert.AreEqual(GetDoResAPathExpected(trans), cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ResxACommand.GetDuplicateKeys" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetGetDuplicateKeysTestData))]
		public void TestGetDuplicateKeys(int num)
		{
			if (num == 5)
			{
				num = Rnd.Int(10, 31);
			}

			var args = Rnd.GetRandomListStringSetLength(num);
			var resa = new ResxACommand(0, null);
			var actual = resa.GetDuplicateKeys(args);

			var expected = $"key{(args.Count == 1 ? "" : "s")} {args.Aggregate("", (c, i) => $"{c}, \"{i}\"").Substring(2)}";

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.GetShortcutFromPath" />
		/// </summary>
		[Test]
		[TestCase("SharedPath", "fromShared")]
		[TestCase("Xm8Path", "fromXm8")]
		[TestCase("BothPath", "bothShared")]
		[TestCase("neither", null)]
		public void TestGetShortcutFromPath(string path, string expected)
		{
			var files = new Mock<IFiles>();
			files.Setup(f => f.Exists(AnyString)).Returns(true);
			files.Setup(f => f.ReadLines(AnyString)).Returns(new List<string>
			{
				@"fromShared=\..\sharedpath",
				"fromXm8=path",
				@"bothShared=\..\bothpath",
				"bothXm8=hpath"
			});

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);

			Shortcuts.Instance.LoadShortcuts("harold");

			var resa = new ResxACommand(0, null);
			var actual = resa.GetShortcutFromPath(path);
			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.GetTranslations" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetGetTranslationsTestData))]
		public void TestGetTranslations(string line, List<(string key, string value)> expected)
		{
			var resa = new ResxACommand(0, null);
			var actual = resa.GetTranslations(line);

			CoreTestUtils.TestListsAreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.Keyify" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetKeyifyTestData))]
		public void TestKeyify(int howMany)
		{
			var autoLen = Rnd.Int(5, 20);
			var key = Rnd.GetRandomWord(Rnd.Int(10, 50));
			var ktv = new Dictionary<string, string>();

			var keyToLength = key.Length > autoLen ? key.Substring(0, autoLen) : key;
			AddKeyToDictionary(keyToLength, ktv, howMany);

			var resa = new ResxACommand(autoLen, null);
			var actual = resa.Keyify(key, ktv);
			var expected = howMany == 0 ? keyToLength : $"{keyToLength}_{howMany}";

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(0, null)]
		[TestCase(1, true)]
		[TestCase(1, false)]
		[TestCase(2, null)]
		[TestCase(3, null)]
		[TestCase(4, null)]
		[TestCase(5, null)]
		public void TestPerform(int num, bool? exists)
		{
			if (!exists.HasValue)
			{
				exists = Rnd.Bool();
			}

			var files = new Mock<IFiles>();
			files.Setup(f => f.Exists(AnyString)).Returns(exists.Value);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);

			ResxBase.Initialize(null);

			var args = Rnd.GetRandomListStringSetLength(num);
			const string path = "path";

			var resa = GetMock();
			var passedPairs = new kv();
			resa.Setup(a => a.DoResAPairs(AnyString, AnyListStrStr))
				.Callback<string, kv>((str, pairs) => passedPairs = pairs);
			resa.Setup(a => a.DoResAPath(AnyString, AnyString));

			resa.Object.Perform(args, num, Res.None, path);

			Verify(files, f => f.Exists(GetVerifyExistsPath(args)), num == 1);
			Verify(resa, a => a.DoResAPath(path, GetVerifyDoResAPath(args)), num == 1 && exists.Value);
			Verify(resa, a => a.DoResAPairs(path, AnyListStrStr), num > 1 && num % 2 == 0);
			CoreTestUtils.TestListsAreEqual(GetPairs(args), passedPairs);
		}

		private delegate void SnkCallback(Dictionary<string, string> ktv, string key, string val, out string autoKey);

		/// <summary>
		/// tests <see cref="ResxACommand.ProcessAddPair" /> where the key and value are duplicates
		/// </summary>
		[Test]
		[TestCase(0, 0)]
		[TestCase(1, 0)]
		[TestCase(0, 1)]
		[TestCase(1, 1)]
		public void TestProcessAddPairBothDuplicate(int key, int val) => TestProcessAddPair(key, val, "bothDupes.resx");

		/// <summary>
		/// tests <see cref="ResxACommand.ProcessAddPair" /> where the key is a duplicate
		/// </summary>
		[Test]
		[TestCase(0)]
		[TestCase(1)]
		public void TestProcessAddPairDuplicateKey(int key) => TestProcessAddPair(key, null, "keyDupes.resx");

		/// <summary>
		/// tests <see cref="ResxACommand.ProcessAddPair" /> where the value is a duplicate
		/// </summary>
		[Test]
		[TestCase(0)]
		[TestCase(1)]
		public void TestProcessAddPairDuplicateValue(int val) => TestProcessAddPair(null, val, "valDupes.resx");

		/// <summary>
		/// tests <see cref="ResxACommand.ProcessAddPair" /> where neither the key nor the value are duplicates
		/// </summary>
		[Test]
		public void TestProcessAddPairNeitherDuplicated() => TestProcessAddPair(null, null, "noDupes.resx");

		/// <summary>
		/// tests <see cref="ResxACommand.ProcessAddPairs" />
		/// </summary>
		[Test]
		public void TestProcessAddPairs()
		{
			var writer = new ResXResourceWriter(UTU.GetTestPath("temp.resx", nameof(ResxACommand)));
			var pairs = Rnd.GetRandomListStrStr();
			var ktv = Rnd.GetRandomDictStringString();
			var vtk = Rnd.GetRandomDictStrListStr();
			var expected = new List<(string key, string value)>();

			var resa = GetMock();
			resa.Setup(a => a.ProcessAddPair(AnyWriter, AnyDictStrStr, AnyDictStrListStr, AnyStrStr, AnyListStrStr))
				.Callback((ResXResourceWriter w, Dictionary<string, string> k,
					Dictionary<string, List<string>> v, (string key, string value) p,
					List<(string key, string value)> l) =>
				{
					if (Rnd.Bool())
					{
						expected.Add(p);
						l.Add(p);
					}
				});

			var actual = resa.Object.ProcessAddPairs(writer, pairs, ktv, vtk);

			CoreTestUtils.TestListsAreEqual(expected, actual);
			foreach (var pair in pairs)
			{
				resa.Verify(a => a.ProcessAddPair(writer, ktv, vtk, pair, AnyListStrStr), Times.Once);
			}
		}

		/// <summary>
		/// tests <see cref="ResxACommand.ReadResxAddFile" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetReadResxAddFileTestData))]
		public void TestReadResxAddFile(int num)
		{
			var forceFail = false;

			if (num == 5)
			{
				num = Rnd.Int(10, 31);
			}
			else if (num == -1)
			{
				forceFail = true;
				num = 2;
			}

			var resa = GetMock();
			const string file = "file";
			var lines = GetAddFileLines(num, forceFail, out var expected);

			resa.Setup(a => a.ReadSpecialCharacterLines(AnyString)).Returns(lines);

			var actual = resa.Object.ReadResxAddFile(file);

			resa.Verify(a => a.ReadSpecialCharacterLines(file), Times.Once);
			CoreTestUtils.TestDictionariesAreEqual(expected, actual, CoreTestUtils.ListsAreEqual);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.ReadSpecialCharacterLines" />
		/// </summary>
		[Test]
		public void TestReadSpecialCharacterLines()
		{
			var resa = new ResxACommand(0, null);
			var lines = resa.ReadSpecialCharacterLines(UTU.GetTestPath("translations.txt", nameof(ResxACommand)));

			var expected = new List<string>
			{
				"KEY_1",
				"lang1|this is the translation for this language~lang2|Another translation~lang3|Otra traslacion áéíóúñüæç",
				"KEY_2",
				"lang1|translation21~lang2|translation22~lang3|translation23"
			};

			CoreTestUtils.TestListsAreEqual(expected, lines);
		}

		/// <summary>
		/// tests <see cref="ResxACommand.SuggestNewKey" />
		/// </summary>
		[Test]
		public void TestSuggestNewKey()
		{
			var resa = GetMock();
			const string expAutoKey = "auto made key";
			resa.Setup(a => a.Keyify(AnyString, AnyDictStrStr)).Returns(expAutoKey);

			var expAnswer = Rnd.Int(2);
			SetupWaitForUser(expAnswer, out var waitForUser);

			var ktv = Rnd.GetRandomDictStringString();
			var newKey = Rnd.GetRandomWord();
			var newVal = Rnd.GetRandomWordLower();

			var actAnswer = resa.Object.SuggestNewKey(ktv, newKey, newVal, out var actAutoKey);
			Assert.AreEqual(expAnswer, actAnswer, "the wrong answer was passed out");
			Assert.AreEqual(expAutoKey, actAutoKey, "the wrong auto key was passed out");
			resa.Verify(a => a.Keyify(newKey, ktv), Times.Once);
			
			VerifySuggestNewKeyCalledAsExpected(waitForUser, newKey, newVal, expAutoKey);

			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();
		}

		/// <summary>
		/// tests <see cref="ResxACommand.VerifyDuplicateValue" />
		/// </summary>
		[Test]
		public void TestVerifyDuplicateValue()
		{
			var resa = GetMock();
			resa.Setup(a => a.GetDuplicateKeys(AnyListStr)).Returns("returned");

			var expAnswer = Rnd.Int(2);
			SetupWaitForUser(expAnswer, out var waitForUser);

			var vtk = Rnd.GetRandomDictStrListStr();
			var newVal = Rnd.GetRandomKeyFromDictionary(vtk);
			var dupKeys = vtk[newVal];
			var newKey = Rnd.GetRandomWord();

			var actAnswer = resa.Object.VerifyDuplicateValue(vtk, newKey, newVal);
			Assert.AreEqual(expAnswer, actAnswer, "the wrong answer was returned");
			resa.Verify(a => a.GetDuplicateKeys(dupKeys), Times.Once);
			
			VerifyDuplicateValueCalledAsExpected(waitForUser, newVal, newKey);

			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();
		}

		#endregion Tests

		#region Any

		private static bool AnyBool => It.IsAny<bool>();

		private static Dictionary<string, List<string>> AnyDictStrListStr => It.IsAny<Dictionary<string, List<string>>>();

		private static Dictionary<string, string> AnyDictStrStr => It.IsAny<Dictionary<string, string>>();

		private static List<string> AnyListStr => It.IsAny<List<string>>();

		private static kv AnyListStrStr => It.IsAny<kv>();

		private static string AnyString => It.IsAny<string>();

		private static (string key, string value) AnyStrStr => It.IsAny<(string key, string value)>();

		private static ResXResourceWriter AnyWriter => It.IsAny<ResXResourceWriter>();

		#endregion Any

		#region Get Test Data

		private static IEnumerable GetDisplayUnaddedPairsTestData() => GetTestDataSequence(0, 5);

		private static IEnumerable GetGetDuplicateKeysTestData() => GetTestDataSequence(1, 5);

		private static IEnumerable GetGetTranslationsTestData()
		{
			yield return new TestCaseData("", null);
			yield return new TestCaseData("lang1|trans1", new List<(string key, string value)> {("lang1", "trans1")});
			yield return new TestCaseData("lang1|trans1~lang2",
				new List<(string key, string value)> {("lang1", "trans1")});
			yield return new TestCaseData("lang1|trans1~lang2|trans2",
				new List<(string key, string value)> {("lang1", "trans1"), ("lang2", "trans2")});
			yield return new TestCaseData("lang1|trans1~lang2|trans2~lang3",
				new List<(string key, string value)> {("lang1", "trans1"), ("lang2", "trans2")});
			yield return new TestCaseData("lang1|trans1~lang2|trans2~lang3|trans3",
				new List<(string key, string value)> {("lang1", "trans1"), ("lang2", "trans2"), ("lang3", "trans3")});
			yield return new TestCaseData("lang1|trans1~lang2|trans2~lang3|trans3~lang4",
				new List<(string key, string value)> {("lang1", "trans1"), ("lang2", "trans2"), ("lang3", "trans3")});
			yield return new TestCaseData("lang1|trans1~lang2|trans2~lang3|trans3~lang4|trans4",
				new List<(string key, string value)> {("lang1", "trans1"), ("lang2", "trans2"), ("lang3", "trans3"), ("lang4", "trans4")});
		}

		private static IEnumerable GetKeyifyTestData() => GetTestDataSequence(0, 5);

		private static IEnumerable GetReadResxAddFileTestData() => GetTestDataSequence(-1, 5);

		private static IEnumerable GetTestDataSequence(int low, int high)
		{
			for (var i = low; i <= high; i++)
			{
				yield return new TestCaseData(i);
			}
		}

		#endregion Get Test Data

		#region Verifiers

		private static string GetVerifyDoResAPath(List<string> args) => args.Count != 1 ? AnyString : args[0];

		private static string GetVerifyExistsPath(List<string> args) => args.Count != 1 ? AnyString : args[0];

		private static void Verify<T>(Mock<T> moq, Expression<Action<T>> expression, bool shouldHaveBeenCalled,
			string message = null)
			where T : class
		{
			CoreTestUtils.MoqMethodWasCalled(moq, expression, shouldHaveBeenCalled, message);
		}

		private static void VerifyDoResAPathAddTranslation(Mock<ResxACommand> resa, string folder,
			Dictionary<string, List<(string lang, string value)>> trans)
		{
			if (trans == null)
			{
				resa.Verify(a => a.AddTranslations(AnyString, AnyString, AnyListStrStr, AnyBool), Times.Never);
				return;
			}

			var total = trans.Count;
			var count = 0;

			foreach (var tran in trans)
			{
				count++;
				var count1 = count;
				resa.Verify(a => a.AddTranslations(folder, tran.Key, tran.Value, count1 == total));
			}
		}

		private static void VerifyAddTranslationsCalls(Mock<ResxACommand> resa, string folder, string key,
			List<(string lang, string value)> translations, bool isLastKey)
		{
			foreach (var (lang, value) in translations)
			{
				resa.Verify(a => a.AddTranslation(folder, key, lang, value, isLastKey));
			}
		}
		
		private static void VerifyDuplicateValueCalledAsExpected(Mock<IWaitForUser> waitForUser, string newVal, string newKey)
		{
			var explanation = new List<string>
			{
				$"\"{newVal}\" is already in this resx file under the returned",
				$"Do you want to add a duplicate of \"{newVal}\" under the key of \"{newKey}\""
			};
			var options = new List<string>
			{
				$"Yes, create a duplicate key for \"{newVal}\"",
				$"No, skip entering \"{newVal}\" for now"
			};
			waitForUser.Verify(w => w.SingleSelection(explanation, options, "", $"Skipping \"{newVal}\""), Times.Once);
		}

		private static void VerifySuggestNewKeyCalledAsExpected(Mock<IWaitForUser> waitForUser, string newKey, string newVal, string autoKey)
		{
			var explanation = new List<string>
			{
				$"The key \"{newKey}\" already exists in this resx file, but \"{autoKey}\" does not.",
				"Would you like to use it?"
			};
			var options = new List<string>
			{
				$"Yes, use \"{autoKey}\" as the key for \"{newVal}\"",
				$"No, skip entering \"{newVal}\" for now"
			};
			waitForUser.Verify(w => w.SingleSelection(explanation, options, "", $"Skipping \"{newVal}\""), Times.Once);
		}

		#endregion Verifiers

		#region Helpers

		private static void AddKeyToDictionary(string keyToLength, Dictionary<string, string> dict, int howMany)
		{
			if (howMany == 0)
			{
				return;
			}

			dict.Add(keyToLength, "");

			if (howMany == 1)
			{
				return;
			}

			for (var i = 1; i < howMany; i++)
			{
				dict.Add($"{keyToLength}_{i}", "");
			}
		}

		private static List<string> GetAddFileLines(int num, bool forceFail, out Dictionary<string, kv> results)
		{
			var lines = new List<string>();

			var first = true;	// we always want the first key's translations to be valid, unless we are forcing the failure
			var key = "";
			results = new Dictionary<string, kv>();
			for (var i = 0; i < num; i++)
			{
				if (i % 2 == 0)	// Keys
				{
					key = Rnd.GetRandomWord();
					lines.Add(key);
				}
				else			// Translations
				{
					var line = GetRandomTranslations(first, forceFail, out var pairs);
					if (pairs != null)
					{
						results.Add(key, pairs);
					}
					lines.Add(line);
					first = false;
				}
			}

			if (results.Count < 1)
			{
				results = null;
			}

			return lines;
		}

		private static string GetDoResAPathExpected(Dictionary<string, List<(string lang, string value)>> trans)
		{
			if (trans == null)
			{
				return null;
			}

			var expected = "";
			foreach (var tran in trans)
			{
				expected += $"{tran.Key}\r\n\r\n";
			}

			return expected;
		}

		/// <summary>
		/// returns if we want a valid set of translations
		/// </summary>
		/// <param name="first">we prefer the first set of translations be valid</param>
		/// <param name="forceFail">should we force a failure no matter what</param>
		private static bool GetIsValid(bool first, bool forceFail) => !forceFail && (first || Rnd.Bool());

		private static Mock<ResxACommand> GetMock(Func<string, bool, bool, bool> bld = null) => new Mock<ResxACommand>(null, bld) {CallBase = true};

		private static kv GetPairs(List<string> args)
		{
			var pairs = new kv();

			var count = args.Count;

			if (count < 2 || count % 2 == 1)
			{
				return pairs;
			}

			for (var i = 0; i < args.Count; i += 2)
			{
				pairs.Add((key: args[i], value: args[i + 1]));
			}

			return pairs;
		}

		private static kv GetPairsFromReader(string file)
		{
			var pairs = new List<(string key, string value)>();
			using (var reader = new ResXResourceReader(file))
			{
				reader.UseResXDataNodes = true;
				// this just loops through all the resources in the reader and converts them into Tuple pairs.
				pairs.AddRange((from DictionaryEntry entry in reader select entry.Value).OfType<ResXDataNode>()
					.Select(node => node.ToPair()));
			}

			return pairs;
		}

		private static string GetRandomLang()
		{
			return $"{Rnd.GetRandomLetterLower()}{Rnd.GetRandomLetterLower()}-{Rnd.GetRandomLetter()}{Rnd.GetRandomLetter()}";
		}

		private static string GetRandomTranslation(out (string lang, string value) pair)
		{
			var lang = GetRandomLang();
			var value = Rnd.GetRandomWordLower(Rnd.Int(10, 80));
			pair = (lang, value);
			return $"{lang}|{value}";
		}

		/// <summary>
		/// gets the translations for this line
		/// </summary>
		/// <param name="first">is this the first set of translations for the file</param>
		/// <param name="forceFail">should we force a failure no matter what</param>
		/// <param name="pairs">the randomly generated pairs</param>
		private static string GetRandomTranslations(bool first, bool forceFail, out List<(string lang, string value)> pairs)
		{
			pairs = null;
			if (!GetIsValid(first, forceFail))
			{
				return Rnd.GetRandomWordLower();
			}

			pairs = new kv();
			var line = "";

			var num = Rnd.Int(1, 11);
			for (var i = 0; i < num; i++)
			{
				line += $"~{GetRandomTranslation(out var pair)}";
				pairs.Add(pair);
			}

			return line.Substring(1);
		}

		private static List<(string lang, string value)> GetRandomTranslationsList()
		{
			GetRandomTranslations(true, false, out var results);
			return results;
		}

		private static List<(string key, string value)> GetUnaddedPairs(int num, Dictionary<string, string> ktv,
			Dictionary<string, List<string>> vtk)
		{
			var pairs = new List<(string key, string value)>();

			for (var i = 0; i < num; i++)
			{
				var key = Rnd.GetRandomWordLower();
				var value = Rnd.GetRandomWordLower();
				pairs.Add((key.ToUpper(), value));
				if (Rnd.Bool())
				{
					ktv.Add(key.ToUpper(), Rnd.GetRandomStr());
				}

				if (Rnd.Bool())
				{
					vtk.Add(key, new List<string> { Rnd.GetRandomWord() });
				}
			}

			return pairs;
		}

		private static string GetUnaddedPairExpected((string key, string value) pair, Dictionary<string, string> ktv,
			Dictionary<string, List<string>> vtk)
		{
			var val = ktv.ContainsKey(pair.key.ToUpper()) ? 1 : 0;
			val += vtk.ContainsKey(pair.value) ? 2 : 0;
			var reason = "";
			switch (val)
			{
				case 0:
					reason = "I'm not sure why this wasn't added";
					break;
				case 1:
					reason = "duplicate key";
					break;
				case 2:
					reason = "duplicate value";
					break;
				case 3:
					reason = "duplicate key and value";
					break;
			}

			return $"{pair.key.ToUpper()}: {pair.value} => {reason}";
		}

		private static string GetUnaddedPairsExpected(List<(string key, string value)> pairs,
			Dictionary<string, string> ktv, Dictionary<string, List<string>> vtk)
		{
			if (pairs.Count == 0)
			{
				return null;
			}
			const string nl = "\r\n";
			var expected = $"Unadded Pairs:{nl}";

			foreach (var pair in pairs)
			{
				expected += $"{GetUnaddedPairExpected(pair, ktv, vtk)}{nl}";
			}

			return expected;
		}
		
		private static void SetupWaitForUser(int desiredResult, out Mock<IWaitForUser> waitForUser)
		{
			waitForUser = new Mock<IWaitForUser>();
			waitForUser.Setup(w => w.SingleSelection(AnyListStr, AnyListStr, AnyString, AnyString))
				.Returns(desiredResult);
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(waitForUser.Object);
		}

		private static void TestProcessAddPair(int? keyAns, int? valAns, string filename)
		{
			var ktv = Rnd.GetRandomDictStringString(Rnd.GetRandomWord);
			var vtk = Rnd.GetRandomDictStrListStr(Rnd.GetRandomWordLower);

			var key = keyAns.HasValue ? Rnd.GetRandomKeyFromDictionary(ktv).ToLower() : Rnd.GetRandomWordLower();
			var val = valAns.HasValue ? Rnd.GetRandomKeyFromDictionary(vtk) : Rnd.GetRandomWordLower();

			var file = UTU.GetTestPath(filename, nameof(ResxACommand));
			var writer = new ResXResourceWriter(file);
			var pair = (key, value: val);
			var unAdded = new List<(string key, string value)>();

			var resa = GetMock();
			var s1 = resa.Setup(a => a.SuggestNewKey(AnyDictStrStr, AnyString, AnyString, out It.Ref<string>.IsAny))
				.Callback(new SnkCallback(Callback));
			var s2 = resa.Setup(a => a.VerifyDuplicateValue(AnyDictStrListStr, AnyString, AnyString));

			if (keyAns.HasValue)
			{
				s1.Returns(keyAns.Value);
			}

			if (valAns.HasValue)
			{
				s2.Returns(valAns.Value);
			}

			void Callback(Dictionary<string, string> d, string nK, string nV, out string aK) => aK = nK;

			resa.Object.ProcessAddPair(writer, ktv, vtk, pair, unAdded);

			writer.Dispose();

			var pairs = GetPairsFromReader(file);

			var skipped = keyAns == 1 || valAns == 1;
			var newKey = key.ToUpper();
			var newVal = val;

			Assert.AreEqual(skipped, unAdded.Contains(pair), "unadded wasn't right");
			Assert.AreEqual(!skipped, pairs.Contains((newKey, newVal)), "what was written wasn't right");
			CoreTestUtils.MoqMethodWasCalled(resa, a => a.SuggestNewKey(ktv, newKey, newVal, out It.Ref<string>.IsAny), keyAns.HasValue);
			CoreTestUtils.MoqMethodWasCalled(resa, a => a.VerifyDuplicateValue(vtk, newKey, newVal), keyAns != 1 && valAns.HasValue);

			File.Delete(file);
		}

		#endregion Helpers
	}
}
