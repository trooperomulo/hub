﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Resources;

using Moq;

using NUnit.Framework;

using Hub;
using Hub.Commands.Resx;
using Hub.Enum;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxRCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxRCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="ResxRCommand.Description" />, <see cref="ResxRCommand.Input" />,
		/// <see cref="ResxRCommand.ShowAll" /> and <see cref="ResxRCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestResxCommand(new ResxRCommand(), new List<string> {"remove", "existing"}, "-r", false,
				new List<string> {"[-c/-x/-s]", "remove", "existing", "resource", "none", "prompted", "key"});
		}

		/// <summary>
		/// tests <see cref="ResxRCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		public void TestPerform(int count)
		{
			// since the populate writer and dicts method is fully tested elsewhere, I just need to verify it is called properly

			var resr = new Mock<ResxRCommand> {CallBase = true};
			const string path = "temp";
			var keys = new List<string> { "asd", "faoei", "oiawureal" };
			resr.Setup(r => r.GetResFiles(path, false)).Returns(new List<string> { "de-DE", "en-GB" });
			resr.Setup(r => r.PopulateResxWriterAndDictionaries(It.IsAny<string>(), It.IsAny<ResXResourceWriter>(), keys));

			Directory.CreateDirectory(@"c:\git\xactimate\temp");

			resr.Object.Perform(keys, count, Res.None, path);

			var times = count > 0 ? Times.Once() : Times.Never();

			resr.Verify(r => r.GetResFiles(path, false), times);
			resr.Verify(Exp($@"{Env.Root}\{path}\strings.resx"), times);
			resr.Verify(Exp(Prep.Lang(path, "de-DE")), times);
			resr.Verify(Exp(Prep.Lang(path, "en-GB")), times);

			Directory.Delete(@"c:\git\xactimate\temp", true);

			System.Linq.Expressions.Expression<Action<ResxRCommand>> Exp(string p)
			{
				return r => r.PopulateResxWriterAndDictionaries(p, It.IsAny<ResXResourceWriter>(), keys);
			}
		}

		#endregion Tests
	}
}
