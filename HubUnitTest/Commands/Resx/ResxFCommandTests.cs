﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Moq;

using NUnit.Framework;

using RyanCoreTests;

using Hub;
using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxFCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxFCommandTests
	{
		private delegate void PopCallback(string path, Dictionary<string, List<string>> valToKey);

		#region Tests

		/// <summary>
		/// tests <see cref="ResxFCommand.Description" />, <see cref="ResxFCommand.Input" />,
		/// <see cref="ResxFCommand.ShowAll" /> and <see cref="ResxFCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestResxCommand(new ResxFCommand(), new List<string> {"find", "existing"}, "-f", true,
				new List<string>
				{
					"[-c/-x/-s]",
					"find",
					"resx",
					"resource",
					"core.data.res",
					"xm8.data.res",
					"prompted",
					"value"
				});

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="ResxFCommand.Perform" /> with no args passed in
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestPerformWithNoArgs(bool useAll)
		{
			var resf = new Mock<ResxFCommand> {CallBase = true};
			resf.Setup(f => f.PopulateResxWriterAndDictionaries(AnyString, AnyVtk));
			resf.Setup(f => f.GetResFolders());

			resf.Object.Perform(Rnd.GetRandomListString(), 0, useAll ? Res.All : Res.None, useAll ? ResxBase.UseAll : "path");

			resf.Verify(f => f.PopulateResxWriterAndDictionaries(AnyString, AnyVtk), Times.Never);
			resf.Verify(f => f.GetResFolders(), Times.Never);

		}	// end TestPerformWithNoArgs(bool)

		/// <summary>
		/// tests <see cref="ResxFCommand.Perform" /> with no matches found in the resx file(s)
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestPerformWithNoMatches(bool useAll)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var folders = useAll ? Rnd.GetRandomListString() : new List<string> {"path"};
			var resf = new Mock<ResxFCommand> {CallBase = true};
			resf.Setup(f => f.PopulateResxWriterAndDictionaries(AnyString, AnyVtk));
			resf.Setup(f => f.GetResFolders()).Returns(folders);

			var count = Rnd.Int(1, 8);
			resf.Object.Perform(Rnd.GetRandomListString(), count, useAll ? Res.All : Res.None, useAll ? ResxBase.UseAll : "path");

			UnitTestUtil.MoqMethodWasCalled(resf, f => f.GetResFolders(), useAll);
			VerifyPopulate(resf, folders);

			Assert.AreEqual(GetNoMatchesOutput(folders), cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformWithNoMatches(bool)

		/// <summary>
		/// tests <see cref="ResxFCommand.Perform" /> with some matches found in the resx file(s)
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestPerformWithSomeMatches(bool useAll)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Env.SetEnvironmentVariable("local", "", true);
			var folders = useAll ? Rnd.GetRandomListString() : new List<string> {"path"};
			var vtkResults = folders.ToDictionary(Path, f => Rnd.GetRandomDictStrListStr());

			void Callback(string path, Dictionary<string, List<string>> valToKey)
			{
				var results = vtkResults[path];
				var keys = results.Keys;
				foreach (var key in keys)
				{
					valToKey[key] = results[key];
				}	// end foreach

			}	// end Callback(string, Dictionary<string, List<string>>)

			var resf = new Mock<ResxFCommand> {CallBase = true};
			resf.Setup(f => f.PopulateResxWriterAndDictionaries(AnyString, AnyVtk)).Callback(new PopCallback(Callback));
			resf.Setup(f => f.GetResFolders()).Returns(folders);

			var count = Rnd.Int(1, 8);
			resf.Object.Perform(Rnd.GetRandomListString(), count, useAll ? Res.All : Res.None, useAll ? ResxBase.UseAll : "path");

			CoreTestUtils.MoqMethodWasCalled(resf, f => f.GetResFolders(), useAll);
			VerifyPopulate(resf, folders);

			Assert.AreEqual(GetSomeMatchesOutput(vtkResults), cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformWithSomeMatches(bool)

		#endregion Tests

		#region Helpers

		private static string AnyString => It.IsAny<string>();

		private static Dictionary<string, List<string>> AnyVtk => It.IsAny<Dictionary<string, List<string>>>();

		private static string GetFolderHeader(string path) => $"\r\n{Out.DashLine}\r\n{path}\r\n\r\nen-US\r\n";

		private static string GetNoMatchesOutput(List<string> folders)
		{
			string str = null;
			folders.Sort();

			foreach (var folder in folders)
			{
				str += $"{GetFolderHeader(folder)}\tNo matches found\r\n";
			}	// end foreach

			return str;

		}	// end GetNoMatchesOutput(List<string>)

		private static string GetSomeMatchesOutput(Dictionary<string , Dictionary<string, List<string>>> vtkResults)
		{
			const string nl = "\r\n";
			string str = null;
			var oKeys = vtkResults.Keys.ToList();
			oKeys.Sort();
			foreach (var oKey in oKeys)
			{
				var folder = oKey.Replace(Env.Root, "").Replace(@"\strings.resx", "");
				str += GetFolderHeader(folder);
				var result = vtkResults[oKey];
				var iKeys = result.Keys.ToList();
				iKeys.Sort();
				foreach (var iKey in iKeys)
				{
					str += $"{iKey}{nl}";
					var list = result[iKey];
					if (list.Count < 1 || list.All(v => v.IsNullOrEmpty()))
					{
						str += "\tNo matches found\r\n";
						continue;
					}	// end if

					list.Sort();
					str = list.Aggregate(str, (current, val) => current + $"\t{val}{nl}");
				}	// end inner foreach

			}	// end outer foreach

			return str;

		}	// end GetSomeMatchesOutput(Dictionary<string , Dictionary<string, List<string>>>)

		private static string Path(string folder) => $@"{Env.Root}{folder}\strings.resx";

		private static void VerifyPopulate(Mock<ResxFCommand> resf, List<string> folders)
		{
			foreach(var folder in folders)
			{
				resf.Verify(f => f.PopulateResxWriterAndDictionaries(Path(folder), AnyVtk), Times.Once);
			}	// end foreach

		}	// end VerifyPopulate(Mock<ResxFCommand>, List<string>

		#endregion Helpers

	}	// end ResxFCommandTests

}	// end HubUnitTest.Commands.Resx
