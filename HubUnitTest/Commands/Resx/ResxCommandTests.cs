﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Moq;

using NUnit.Framework;

using Hub;
using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxCommandTests
	{
		#region Variables

		private static Res _res;
		private static int _count;
		private static string _path;
		private static string _cmd;
		private static string _output;

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests <see cref="ResxCommand.Description" />, <see cref="ResxCommand.NeedShortcut" />,
		/// <see cref="ResxCommand.Inputs" /> and <see cref="ResxCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(GetInstance(), Ls("file", "resx", "modify"), true, true, Ls("re", "resx"), null);

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="ResxCommand.Help" />
		/// </summary>
		[Test]
		[TestCase("a", "rdt", "abc", "A was called")]
		[TestCase("b", "zy", "lmt", "B was called")]
		[TestCase("ajt", "lrt", "kit", "Def was called")]
		public void TestHelp(string a, string b, string c, string expected)
		{
			var cmd = GetInstance();
			cmd.Help(Ls(a, b, c));
			Assert.AreEqual(expected, _output);

		}	// end TestHelp(string, string, string, string)

		/// <summary>
		/// tests <see cref="ResxCommand.Perform" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetPerformTestData))]
		public void TestPerform(List<string> args, Res expRes, int expCount, string expCmd, string expPath)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(new Mock<IDirectories>().Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(new Mock<IConsole>().Object);

			var cmd = GetInstance();
			cmd.Perform(args);
			Assert.AreEqual(expRes, _res, "res was wrong");
			Assert.AreEqual(expCount, _count, "count was wrong");
			Assert.AreEqual(expCmd, _cmd, "cmd was wrong");
			Assert.AreEqual(expPath, _path, "path was wrong");

			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform(List<string>, Res, int, string, string)

		#endregion Test

		#region Helpers

		private static ResxCommand GetInstance() => new ResxCommand(new List<ResxBase> {new ResxAStub(), new ResxBStub()}, new ResxDefStub());

		private static IEnumerable GetPerformTestData()
		{
			const string a = "a";
			const string b = "b";
			const string d = "c";
			const string c = "-c";
			const string x = "-x";
			const string s = "-s";
			const string r = "rdt";
			// too many res specified
			yield return new TestCaseData(Ls(a, c, x, s, r), Res.None, 0, null, null) {TestName = "All Res"};
			yield return new TestCaseData(Ls(a, c, x, r), Res.None, 0, null, null) {TestName = "Core and Xm8"};
			yield return new TestCaseData(Ls(a, c, s, r), Res.None, 0, null, null) {TestName = "Core and Shared"};
			yield return new TestCaseData(Ls(a, x, s, r), Res.None, 0, null, null) {TestName = "Xm8 and Shared"};
			// others
			yield return new TestCaseData(Ls(a, c, r), Res.Core, 1, "A", @"xactimate\xactimate.shared\xm8core\Core.Data.Res")
				{TestName = "Core"};
			yield return new TestCaseData(Ls(a, x, r), Res.Xm8, 1, "A", @"xactimate\xactimate.shared\xm8core\Xm8.Data.Res")
				{TestName = "Xm8"};
			yield return new TestCaseData(Ls(b, s, r), Res.Shared, 1, "B", @"xactimate\xactimate.shared\Shared.Res")
				{TestName = "Shared"};
			yield return new TestCaseData(Ls(d, r), Res.None, 2, "Def", "")
				{TestName = "Def"};

		}	// end GetPerformTestData()

		private static List<string> Ls(params string[] args) => args.ToList();

		#endregion Helpers

		#region Inner Classes

		private class ResxAStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "a";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() => _output = "A was called";

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path)
			{
				_count = count;
				_res = res;
				_path = path;
				_cmd = "A";

			}	// end Perform(List<string>, int, Res, string)

			#endregion Overrides of ResxBase

		}	// end ResxAStub

		private class ResxBStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "b";

			/// <inheritdoc />
			public override bool ShowAll => true;

			/// <inheritdoc />
			public override void Help() => _output = "B was called";

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path)
			{
				_count = count;
				_res = res;
				_path = path;
				_cmd = "B";

			}	// end Perform(List<string>, int, Res, string)

			#endregion Overrides of ResxBase

		}	// end ResxBStub

		private class ResxDefStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() => _output = "Def was called";

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path)
			{
				_count = count;
				_res = res;
				_path = path;
				_cmd = "Def";

			}	// end Perform(List<string>, int, Res, string)

			#endregion Overrides of ResxBase

		}	// end ResxDefStub

		#endregion Inner Classes

	}	// end ResxCommandTests

}		// end HubUnitTests.Commands.Resx
