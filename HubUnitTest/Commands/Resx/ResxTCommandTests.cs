﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Moq;

using NUnit.Framework;

using Hub;
using Hub.Commands.Helpers.Resx;
using Hub.Commands.Resx;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxTCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTCommandTests
	{
		#region Variables

		private readonly Dictionary<string, List<string>> _files = new Dictionary<string, List<string>>();
		
		private Mock<ResxTResultsDisplayer> Mock { get; set; }

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests <see cref="ResxTCommand.Description" />, <see cref="ResxTCommand.Input" />,
		/// <see cref="ResxTCommand.ShowAll" /> and <see cref="ResxTCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestResxCommand(new ResxTCommand(), new List<string> {"find", "translation"}, "-t", true,
				new List<string>
				{
					"[-c/-x/-s]",
					"used",
					"find",
					"translations",
					"key",
					"core.data.res",
					"found",
					"xm8.data.res",
					"prompted"
				});
		}

		/// <summary>
		/// tests <see cref="ResxTCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(0, true)]
		[TestCase(0, false)]
		[TestCase(1, true)]
		[TestCase(1, false)]
		[TestCase(2, true)]
		[TestCase(2, false)]
		[TestCase(3, true)]
		[TestCase(3, false)]
		public void TestPerform(int count, bool useAll)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			ResxTCommand.GetDisplayer = GetDisplayer;

			var keys = Rnd.GetRandomListStringSetLength(count);
			var path = useAll ? ResxBase.UseAll : "path";

			var folders = useAll ? Rnd.GetRandomListString() : new List<string> {path};

			var rest = new Mock<ResxTCommand> {CallBase = true};
			rest.Setup(t => t.GetResFolders()).Returns(folders);
			SetupGetFiles(rest, folders);
			rest.Setup(t => t.PopulateResxWriterAndDictionaries(It.IsAny<string>(), AnyDict));

			rest.Object.Perform(keys, count, useAll ? Res.All : Res.None, path);

			UnitTestUtil.MoqMethodWasCalled(rest, t => t.GetResFolders(), useAll && count > 0);
			VerifyGetFiles(rest, folders, count > 0);
			VerifyPopulateWasCalled(rest, folders, count > 0);
			if (count > 0)
			{
				Mock.Verify(d => d.PrintResults(), Times.Once);
			}

			Assert.AreEqual(GetExpectedOutput(count), cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			_files.Clear();
		}

		#endregion Tests

		#region Helpers

		private static Dictionary<string, string> AnyDict => It.IsAny<Dictionary<string, string>>();

		private ResxTResultsDisplayer GetDisplayer(ResxTResults results)
		{
			Mock = new Mock<ResxTResultsDisplayer>(results);

			Mock.Setup(d => d.PrintResults());

			return Mock.Object;
		}

		private string GetExpectedOutput(int count)
		{
			if (count == 0)
			{
				return null;
			}

			var output = "";

			var folders = _files.Keys.ToList();
			folders.Sort();

			foreach (var folder in folders)
			{
				var files = _files[folder];
				files.Sort();

				var first = true;

				output += $"Searching {folder}\r\n";

				foreach (var file in files)
				{
					output += $"{(first ? "\t" : ", ")}{file}";

					first = false;
				}

				output += "\r\n";
			}

			return output;
		}

		private void SetupGetFiles(Mock<ResxTCommand> rest, List<string> folders)
		{
			foreach (var folder in folders)
			{
				var files = Rnd.GetRandomListString();
				_files.Add(folder, files);
				rest.Setup(t => t.GetResFiles(folder, false)).Returns(files);
			}
		}

		private static void VerifyGetFiles(Mock<ResxTCommand> rest, List<string> folders, bool shouldHaveBeenCalled)
		{
			foreach (var folder in folders)
			{
				UnitTestUtil.MoqMethodWasCalled(rest, t => t.GetResFiles(folder, false), shouldHaveBeenCalled);
			}
		}

		private void VerifyPopulateWasCalled(Mock<ResxTCommand> rest, List<string> folders, bool shouldHaveBeenCalled)
		{
			foreach (var folder in folders)
			{
				var files = _files[folder];
				foreach (var file in files)
				{
					var prepped = Prep.Lang(folder, file);
					UnitTestUtil.MoqMethodWasCalled(rest, t => t.PopulateResxWriterAndDictionaries(prepped, AnyDict),
						shouldHaveBeenCalled);
				}
			}
		}

		#endregion Helpers
	}
}
