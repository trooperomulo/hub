﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Resources;
using System.Xml.Linq;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Commands.Resx;
using Hub.Interface;

using HubUnitTest.Stubs;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxBase" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxBaseTests
	{
		#region Constants

		private const string Root = "<?xml version=\"1.0\" encoding=\"utf-8\"?><root>";

		#region Comment
		private const string Comment = "  <!-- " +
		                               "    Microsoft ResX Schema " +
		                               "    " +
		                               "    Version 2.0" +
		                               "    " +
		                               "    The primary goals of this format is to allow a simple XML format " +
		                               "    that is mostly human readable. The generation and parsing of the " +
		                               "    various data types are done through the TypeConverter classes " +
		                               "    associated with the data types." +
		                               "    " +
		                               "    Example:" +
		                               "    " +
		                               "    ... ado.net/XML headers & schema ..." +
		                               "    <resheader name=\"resmimetype\">text/microsoft-resx</resheader>" +
		                               "    <resheader name=\"version\">2.0</resheader>" +
		                               "    <resheader name=\"reader\">System.Resources.ResXResourceReader, System.Windows.Forms, ...</resheader>" +
		                               "    <resheader name=\"writer\">System.Resources.ResXResourceWriter, System.Windows.Forms, ...</resheader>" +
		                               "    <data name=\"Name1\"><value>this is my long string</value><comment>this is a comment</comment></data>" +
		                               "    <data name=\"Color1\" type=\"System.Drawing.Color, System.Drawing\">Blue</data>" +
		                               "    <data name=\"Bitmap1\" mimetype=\"application/x-microsoft.net.object.binary.base64\">" +
		                               "        <value>[base64 mime encoded serialized .NET Framework object]</value>" +
		                               "    </data>" +
		                               "    <data name=\"Icon1\" type=\"System.Drawing.Icon, System.Drawing\" mimetype=\"application/x-microsoft.net.object.bytearray.base64\">" +
		                               "        <value>[base64 mime encoded string representing a byte array form of the .NET Framework object]</value>" +
		                               "        <comment>This is a comment</comment>" +
		                               "    </data>" +
		                               "                " +
		                               "    There are any number of \"resheader\" rows that contain simple " +
		                               "    name/value pairs." +
		                               "    " +
		                               "    Each data row contains a name, and value. The row also contains a " +
		                               "    type or mimetype. Type corresponds to a .NET class that support " +
		                               "    text/value conversion through the TypeConverter architecture. " +
		                               "    Classes that don't support this are serialized and stored with the " +
		                               "    mimetype set." +
		                               "    " +
		                               "    The mimetype is used for serialized objects, and tells the " +
		                               "    ResXResourceReader how to depersist the object. This is currently not " +
		                               "    extensible. For a given mimetype the value must be set accordingly:" +
		                               "    " +
		                               "    Note - application/x-microsoft.net.object.binary.base64 is the format " +
		                               "    that the ResXResourceWriter will generate, however the reader can " +
		                               "    read any of the formats listed below." +
		                               "    " +
		                               "    mimetype: application/x-microsoft.net.object.binary.base64" +
		                               "    value   : The object must be serialized with " +
		                               "            : System.Runtime.Serialization.Formatters.Binary.BinaryFormatter" +
		                               "            : and then encoded with base64 encoding." +
		                               "    " +
		                               "    mimetype: application/x-microsoft.net.object.soap.base64" +
		                               "    value   : The object must be serialized with " +
		                               "            : System.Runtime.Serialization.Formatters.Soap.SoapFormatter" +
		                               "            : and then encoded with base64 encoding." +
		                               "" +
		                               "    mimetype: application/x-microsoft.net.object.bytearray.base64" +
		                               "    value   : The object must be serialized into a byte array " +
		                               "            : using a System.ComponentModel.TypeConverter" +
		                               "            : and then encoded with base64 encoding." +
		                               "    -->";
		#endregion Comment

		#region Schema
		private const string Schema = "  <xsd:schema id=\"root\" xmlns=\"\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\">" +
					  "    <xsd:import namespace=\"http://www.w3.org/XML/1998/namespace\" />" +
					  "    <xsd:element name=\"root\" msdata:IsDataSet=\"true\">" +
					  "      <xsd:complexType>" +
					  "        <xsd:choice maxOccurs=\"unbounded\">" +
					  "          <xsd:element name=\"metadata\">" +
					  "            <xsd:complexType>" +
					  "              <xsd:sequence>" +
					  "                <xsd:element name=\"value\" type=\"xsd:string\" minOccurs=\"0\" />" +
					  "              </xsd:sequence>" +
					  "              <xsd:attribute name=\"name\" use=\"required\" type=\"xsd:string\" />" +
					  "              <xsd:attribute name=\"type\" type=\"xsd:string\" />" +
					  "              <xsd:attribute name=\"mimetype\" type=\"xsd:string\" />" +
					  "              <xsd:attribute ref=\"xml:space\" />" +
					  "            </xsd:complexType>" +
					  "          </xsd:element>" +
					  "          <xsd:element name=\"assembly\">" +
					  "            <xsd:complexType>" +
					  "              <xsd:attribute name=\"alias\" type=\"xsd:string\" />" +
					  "              <xsd:attribute name=\"name\" type=\"xsd:string\" />" +
					  "            </xsd:complexType>" +
					  "          </xsd:element>" +
					  "          <xsd:element name=\"data\">" +
					  "            <xsd:complexType>" +
					  "              <xsd:sequence>" +
					  "                <xsd:element name=\"value\" type=\"xsd:string\" minOccurs=\"0\" msdata:Ordinal=\"1\" />" +
					  "                <xsd:element name=\"comment\" type=\"xsd:string\" minOccurs=\"0\" msdata:Ordinal=\"2\" />" +
					  "              </xsd:sequence>" +
					  "              <xsd:attribute name=\"name\" type=\"xsd:string\" use=\"required\" msdata:Ordinal=\"1\" />" +
					  "              <xsd:attribute name=\"type\" type=\"xsd:string\" msdata:Ordinal=\"3\" />" +
					  "              <xsd:attribute name=\"mimetype\" type=\"xsd:string\" msdata:Ordinal=\"4\" />" +
					  "              <xsd:attribute ref=\"xml:space\" />" +
					  "            </xsd:complexType>" +
					  "          </xsd:element>" +
					  "          <xsd:element name=\"resheader\">" +
					  "            <xsd:complexType>" +
					  "              <xsd:sequence>" +
					  "                <xsd:element name=\"value\" type=\"xsd:string\" minOccurs=\"0\" msdata:Ordinal=\"1\" />" +
					  "              </xsd:sequence>" +
					  "              <xsd:attribute name=\"name\" type=\"xsd:string\" use=\"required\" />" +
					  "            </xsd:complexType>" +
					  "          </xsd:element>" +
					  "        </xsd:choice>" +
					  "      </xsd:complexType>" +
					  "    </xsd:element>" +
					  "  </xsd:schema>";
		#endregion Schema

		#region Headers
		private const string Headers = "  <resheader name=\"resmimetype\">" +
		                               "    <value>text/microsoft-resx</value>" +
		                               "  </resheader>" +
		                               "  <resheader name=\"version\">" +
		                               "    <value>2.0</value>" +
		                               "  </resheader>" +
		                               "  <resheader name=\"reader\">" +
		                               "    <value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>" +
		                               "  </resheader>" +
		                               "  <resheader name=\"writer\">" +
		                               "    <value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>" +
		                               "  </resheader>";
		#endregion Headers

		private const string Closer = "</root>";

		#endregion Constants

		#region Tests

		/// <summary>
		/// tests <see cref="ResxBase.GetResFiles"/>
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestGetResxFiles(bool inclEnUs)
		{
			// create mocks
			var dirs = new Mock<IDirectories>();
			var rBase = new Mock<ResxBase> {CallBase = true};

			// setup
			var folder = Rnd.GetRandomStr();
			var path = $@"{Env.Root}\{folder}";

			dirs.Setup(d => d.GetFileInfos(path, "*.resx")).Returns(GetTestFileInfo().ToArray);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(new Mock<IFiles>().Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);

			ResxBase.Initialize(null);

			// do test
			var results = rBase.Object.GetResFiles(folder, inclEnUs);

			// verify
			dirs.Verify(d => d.GetFileInfos(path, "*.resx"), Times.Once);

			var expected = new List<string> { "de-AT", "de-DE", "en-AU", "en-GB", "es-ES", "fr-CA", "fr-FR", "it-IT", "pt-BR" };
			if (inclEnUs)
			{
				expected.Insert(4, "en-US");
			}

			Assert.IsTrue(UnitTestUtil.ListsAreEqual(expected, results), "the wrong results were returned");

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		/// <summary>
		/// tests <see cref="ResxBase.GetResFolders" />
		/// </summary>
		[Test]
		public void TestGetResxFolders()
		{
			Environment.SetEnvironmentVariable("local", "c:");
			Env.UseRepo = false;

			// create mocks
			var dirs = new Mock<IDirectories>();

			// setup
			dirs.Setup(d => d.GetDirectories(Env.Root, "*Res")).Returns(GetTestDirInfo().ToArray);

			dirs.Setup(GetFilesExp("a")).Returns(new List<string>());
			dirs.Setup(GetFilesExp("b")).Returns(new List<string> {"1"});
			dirs.Setup(GetFilesExp("c")).Returns(new List<string>());
			dirs.Setup(GetFilesExp("d")).Returns(new List<string> {"1", "2"});
			dirs.Setup(GetFilesExp("e")).Returns(new List<string>());
			dirs.Setup(GetFilesExp("f")).Returns(new List<string> {"r", "e", "s", "x"});

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(new Mock<IFiles>().Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);

			ResxBase.Initialize(null);

			// do test
			var results = new ResxFCommand().GetResFolders();

			var expected = new List<string> {"\\bres", "\\dres", "\\fres"};

			UnitTestUtil.TestListsAreEqual(expected, results);

			dirs.Verify(GetFilesExp("a"), Times.Once);
			dirs.Verify(GetFilesExp("b"), Times.Once);
			dirs.Verify(GetFilesExp("c"), Times.Once);
			dirs.Verify(GetFilesExp("d"), Times.Once);
			dirs.Verify(GetFilesExp("e"), Times.Once);
			dirs.Verify(GetFilesExp("f"), Times.Once);

			Env.SetEnvironmentVariable("local", "", true);

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		/// <summary>
		/// tests <see cref="ResxBase.PopulateResxWriterAndDictionaries(string, ResXResourceWriter, Dictionary{string, string}, Dictionary{string, List{string}}, List{string})" />
		/// </summary>
		[Test]
		[TestCase(true, true)]
		[TestCase(true, false)]
		[TestCase(false, true)]
		[TestCase(false, false)]
		public void TestPopulateResxWriterAndDictionaries(bool k, bool v)
		{
			var aKtv = k ? Rnd.GetRandomDictStringString(Rnd.GetRandomLetter, () => "") : null;
			var aVtk = v ? Rnd.GetRandomDictStrListStr(Rnd.GetRandomLetterLower, () => new List<string>()) : null;

			var kKeys = aKtv?.Keys.ToList();
			var vKeys = aVtk?.Keys.ToList();

			var toNotAdd = Rnd.GetRandomKeys();

			var rBase = new Mock<ResxBase> {CallBase = true};

			var rFile = UnitTestUtil.GetTestPath("strings.xml", nameof(ResxBase));
			var wFile = $"{rFile}b";
			var writer = new ResXResourceWriter(wFile);
			rBase.Object.PopulateResxWriterAndDictionaries(rFile, writer, aKtv, aVtk, toNotAdd);

			writer.Dispose();

			using (var reader = new ResXResourceReader(wFile))
			{
				reader.UseResXDataNodes = true;
				foreach (var node in (from DictionaryEntry entry in reader select entry.Value).OfType<ResXDataNode>())
				{
					Assert.IsFalse(toNotAdd.Contains(node.Name),
						$"{node.Name} was added to the new writer when it shouldn't have been");
				}
			}

			BuildExpectedDicts(toNotAdd, kKeys, vKeys, out var eKtv, out var eVtk);

			UnitTestUtil.TestDictionariesAreEqual(eKtv, aKtv, additionalMessage: "ktv");
			UnitTestUtil.TestDictionariesAreEqual(eVtk, aVtk, UnitTestUtil.ListsAreEqual, "vtk");

			File.Delete(wFile);
		}

		/// <summary>
		/// tests <see cref="ResxBase.PopulateResxWriterAndDictionaries(string, ResXResourceWriter, Dictionary{string, string}, Dictionary{string, List{string}})" />
		/// </summary>
		[Test]
		public void TestPopulateResxWriterAndDictionariesA()
		{
			var file = Rnd.GetRandomStr();

			var writer = new ResXResourceWriter("");
			var keyToVal = Rnd.GetRandomDictStringString();
			var valToKey = Rnd.GetRandomDictStrListStr();

			var rBase = new Mock<ResxBase> { CallBase = true };
			Expression<Action<ResxBase>> expression = b => b.PopulateResxWriterAndDictionaries(file, writer, keyToVal, valToKey, null);
			rBase.Setup(expression);

			rBase.Object.PopulateResxWriterAndDictionaries(file, writer, keyToVal, valToKey);

			rBase.Verify(expression, Times.Once);
		}

		/// <summary>
		/// tests <see cref="ResxBase.PopulateResxWriterAndDictionaries(string, Dictionary{string, List{string}})" />
		/// </summary>
		[Test]
		public void TestPopulateResxWriterAndDictionariesF()
		{
			var file = Rnd.GetRandomStr();

			var valToKey = Rnd.GetRandomDictStrListStr();

			var rBase = new Mock<ResxBase> { CallBase = true };
			Expression<Action<ResxBase>> expression = b => b.PopulateResxWriterAndDictionaries(file, null, null, valToKey, null);
			rBase.Setup(expression);

			rBase.Object.PopulateResxWriterAndDictionaries(file, valToKey);

			rBase.Verify(expression, Times.Once);
		}

		/// <summary>
		/// tests <see cref="ResxBase.PopulateResxWriterAndDictionaries(string, ResXResourceWriter, List{string})" />
		/// </summary>
		[Test]
		public void TestPopulateResxWriterAndDictionariesR()
		{
			var file = Rnd.GetRandomStr();

			var writer = new ResXResourceWriter("");
			var keysToNotAdd = Rnd.GetRandomListString();

			var rBase = new Mock<ResxBase> {CallBase = true};
			Expression<Action<ResxBase>> expression = b => b.PopulateResxWriterAndDictionaries(file, writer, null, null, keysToNotAdd);
			rBase.Setup(expression);

			rBase.Object.PopulateResxWriterAndDictionaries(file, writer, keysToNotAdd);

			rBase.Verify(expression, Times.Once);
		}

		/// <summary>
		/// tests <see cref="ResxBase.PopulateResxWriterAndDictionaries(string, Dictionary{string, List{string}})" />
		/// </summary>
		[Test]
		public void TestPopulateResxWriterAndDictionariesT()
		{
			var file = Rnd.GetRandomStr();

			var keyToVal = Rnd.GetRandomDictStringString();

			var rBase = new Mock<ResxBase> { CallBase = true };
			Expression<Action<ResxBase>> expression = b => b.PopulateResxWriterAndDictionaries(file, null, keyToVal, null, null);
			rBase.Setup(expression);

			rBase.Object.PopulateResxWriterAndDictionaries(file, keyToVal);

			rBase.Verify(expression, Times.Once);
		}

		/// <summary>
		/// tests <see cref="ResxBase.PopulateResxWriterAndDictionaries(string, ResXResourceWriter, Dictionary{string, string}, Dictionary{string, List{string}}, List{string})" /> with the writer and dictionaries all null
		/// </summary>
		[Test]
		public void TestPopulateResxWriterAndDictionariesNulls()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var rBase = new Mock<ResxBase> {CallBase = true};
			rBase.Object.PopulateResxWriterAndDictionaries("", null, null, null, null);
			
			Assert.AreEqual($"{nameof(ResxBase.PopulateResxWriterAndDictionaries)} was called but was given nothing to do\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ResxBase.SortResxFile" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestSortResxFile(bool hasComment)
		{
			var file = Rnd.GetRandomStr();
			var keys = Rnd.GetRandomKeys();
			var str = GetXelementString(hasComment, keys);
			var stream = GetXelementStream(str);

			var files = new Mock<IFiles>();
			files.Setup(f => f.GetStream(file, FileMode.Open)).Returns(stream);

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(new Mock<IDirectories>().Object);

			ResxBase.Initialize(SaveElement);

			var warn = ResxBase.SortResxFile(file);
			Assert.AreEqual(!hasComment, warn);

			void SaveElement(XElement e, string f)
			{
				keys.Sort();
				var sortedStr = GetXelementString(hasComment, keys);
				var doc = XElement.Load(GetXelementStream(sortedStr));
				UnitTestUtil.TestListsAreEqual(GetDataNodes(doc), GetDataNodes(e), NodesMatch);
				Assert.AreEqual(file, f, "the xelement should have been saved to the original file name");
			}

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		#endregion Tests

		#region Helpers

		private static void BuildExpectedDicts(List<string> keys, List<string> kKeys,
			List<string> vKeys, out Dictionary<string, string> eKtv, out Dictionary<string, List<string>> eVtk)
		{
			eKtv = null;
			eVtk = null;

			if (kKeys != null)
			{
				eKtv = new Dictionary<string, string>();
				foreach (var kKey in kKeys)
				{
					if (!keys.Contains(kKey))
					{
						eKtv.Add(kKey, kKey.ToLower());
					}
				}
			}

			if (vKeys != null)
			{
				eVtk = new Dictionary<string, List<string>>();
				foreach (var vKey in vKeys)
				{
					var upper = vKey.ToUpper();
					if (!keys.Contains(upper))
					{
						eVtk.Add(vKey, upper);
						if (vKey == "a")
						{
							eVtk.Add(vKey, "A2");
						}
						else if (vKey == "b")
						{
							eVtk.Add(vKey, "B2");
						}
					}
				}
			}

		}

		private static string GetData(List<string> keys)
		{
			var result = "";

			foreach(var key in keys)
			{
				result += GetDataNode(key);
			}

			return result;
		}

		private static string GetDataNode(string key)
		{
			return $"  <data name=\"{key}\" xml:space=\"preserve\">    <value>{key.ToLower()}</value>  </data>";
		}

		private static List<XElement> GetDataNodes(XContainer ele) => ele.Elements().Where(UseIt).ToList();

		private static Expression<Func<IDirectories, List<string>>> GetFilesExp(string file)
		{
			return d => d.GetFiles($@"{Env.Root}\{file}res", "*.resx");
		}
		
		private static List<DirectoryInfo> GetTestDirInfo()
		{
			return new List<DirectoryInfo>
			{
				new DirectoryInfo($@"{Env.Root}\online things res"),
				new DirectoryInfo($@"{Env.Root}\unit tests happen here res"),
				new DirectoryInfo($@"{Env.Root}\we do not want xv at all res"),
				new DirectoryInfo($@"{Env.Root}\no pictures"),
				new DirectoryInfo($@"{Env.Root}\something random"),
				new DirectoryInfo($@"{Env.Root}\ares"),
				new DirectoryInfo($@"{Env.Root}\bres"),
				new DirectoryInfo($@"{Env.Root}\cres"),
				new DirectoryInfo($@"{Env.Root}\dres"),
				new DirectoryInfo($@"{Env.Root}\eres"),
				new DirectoryInfo($@"{Env.Root}\fres"),
			};
		}

		private static List<FileInfo> GetTestFileInfo()
		{
			return new List<FileInfo>
			{
				new FileInfo(@"shared\Core.Data.Res\str_exceptions.en-AU.resx"),
				new FileInfo(@"shared\Core.Data.Res\str_exceptions.en-GB.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.de-AT.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.fr-CA.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.es-ES.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.de-DE.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.en-AU.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.fr-FR.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.it-IT.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.en-GB.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.pt-BR.resx"),
				new FileInfo(@"shared\Core.Data.Res\strings.resx"),
				new FileInfo(@"shared\Core.Data.Res\too.many.parts.resx")
			};
		}

		private static string GetXelementString(bool hasComment, List<string> keys)
		{
			return Root + (hasComment ? Comment : "") + Schema + Headers + GetData(keys) + Closer;
		}

		private static Stream GetXelementStream(string str)
		{
			var doc = XDocument.Parse(str);

			var ms = new MemoryStream();
			doc.Save(ms);
			ms.Position = 0;

			return ms;
		}
		
		private static bool NodesMatch(XElement a, XElement b)
		{
			if (a == null && b == null)
			{
				return true;
			}

			if (a == null || b == null)
			{
				return false;
			}

			var aName = a.Attribute("name");
			var bName = b.Attribute("name");
			if (aName == null || bName == null || aName.Value.IsNullOrEmpty() || bName.Value.IsNullOrEmpty())
			{
				return false;
			}

			return aName.Value == bName.Value;
		}

		private static bool UseIt(XElement ele) => ele.Name.ToString().Equals("data");

		#endregion Helpers
	}
}
