﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands.Resx;
using Hub.Decider.Resx;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

namespace HubUnitTest.Commands.Resx
{
	/// <summary>
	/// tests <see cref="ResxNoneCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxNoneCommandTests
	{
		#region Variable

		private readonly List<string> _helps = new List<string>
		{
			"used",
			"perform",
			"various",
			"aaa",
			"bbb",
			"-a/-b",
			// these are printed by NoneCommand
			"honored",
			"printed",
			"no action",
			"specified"
		};

		#endregion Variable

		#region Tests

		/// <summary>
		/// tests <see cref="ResxNoneCommand.Description" />, <see cref="ResxNoneCommand.Input" />,
		/// <see cref="ResxNoneCommand.ShowAll" /> and <see cref="ResxNoneCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			ResxDecider.InitCommands(new List<ResxBase> {new ResxA(), new ResxB()}, null, null);
			UnitTestUtil.TestResxCommand(new ResxNoneCommand(), new List<string>(), "", false, _helps);
		}

		/// <summary>
		/// tests <see cref="ResxNoneCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			ResxDecider.InitCommands(new List<ResxBase> {new ResxA(), new ResxB()}, null, null);
			var cmd = new ResxNoneCommand();
			cmd.Perform(null, 0, 0, null);

			var helps = new List<string> {"you did not specify a valid resx action to do", "ccc", "ddd"};
			helps.AddRange(_helps);

			UnitTestUtil.TestStringContainsItems(cons.Output.ToLower(), helps);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Classes

		private class ResxA : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "aaa";

			/// <inheritdoc />
			public override string Input => "-a";

			/// <inheritdoc />
			public override bool ShowAll => true;

			/// <inheritdoc />
			public override void Help() => Out.Line("ccc");

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path) { }

			#endregion Overrides of ResxBase
		}

		private class ResxB : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "bbb";

			/// <inheritdoc />
			public override string Input => "-b";

			/// <inheritdoc />
			public override bool ShowAll => true;

			/// <inheritdoc />
			public override void Help() => Out.Line("ddd");

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path) { }

			#endregion Overrides of ResxBase
		}

		#endregion Inner Classes
	}
}
