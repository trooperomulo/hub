﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

// ReSharper disable CommentTypo
// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo
namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="VsCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class VsCommandTests
	{
		#region Constants

		private const string MsgEh = "eh was not found in the shortcuts\r\n";
		private const string MsgFa = "Could not find 'fake', the location 'f' is set to\r\n";
		private const string MsgNo = "No .sln or .csproj files were found, so nothing will be opened\r\n";
		private const string Msg1S = "opening 1s - single.sln\r\n";
		private const string Msg2S = "opening 2s - first.sln\r\nopening 2s - second.sln\r\n";
		private const string Msg1C = "opening 1c - single.csproj\r\n";
		private const string Msg2C = "opening 2c - first.csproj\r\nopening 2c - second.csproj\r\n";
		private const string Msg1F = "everything was filtered out for f1\r\n";
		private const string Msg2F = "opening f2 - first.sln\r\n";
		private const string Msg3F = "opening f3 - first.sln\r\nopening f3 - second.sln\r\n";
		private const string Msg4F = "opening f4 - first.sln\r\nopening f4 - second.sln\r\n";

		#endregion Constants

		#region Tests
		
		///
		[SetUp]
		public void Setup()
		{
			Env.SetEnvironmentVariable("local", "", true);
			Env.SetEnvironmentVariable("version_tree", "", true);
			if (Shortcuts.Instance.Loaded)
			{
				Shortcuts.Instance.Clear();
			}

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(new FilesStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, StubDir>(new StubDir());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(new ProgStub());
			IoCContainer.Instance.RegisterAsSingleInstance<ISolutionColorizer, ColorizerStub>(new ColorizerStub());
			Shortcuts.Instance.LoadShortcuts("a");

		}	// end Setup()

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<ISolutionColorizer>();

		}	// end TearDown()

		/// <summary>
		/// tests <see cref="VsCommand.Description" />, <see cref="VsCommand.NeedShortcut" />,
		/// <see cref="VsCommand.Inputs" />  and <see cref="VsCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new VsCommand(""), new List<string> {"open", "visual studio"}, false, true,
				new List<string> {"v", "vs"}, new List<string> {"opens", "visual studio", "solutions", "fifo", "nothing"});

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> when the shortcut isn't defined
		/// </summary>
		[Test]
		public void TestPerformNotAShortcut()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new VsCommand("");
			cmd.Perform(new List<string> {"eh"});

			Assert.AreEqual(MsgEh, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformNotAShortcut()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> when the directory doesn't exist
		/// </summary>
		[Test]
		public void TestPerformNotADirectory()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new VsCommand("");
			cmd.Perform(new List<string> {"f"});

			Assert.AreEqual(MsgFa, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformNotADirectory()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with no sln or csproj
		/// </summary>
		[Test]
		public void TestPerform0SlnOrCsproj()
		{
			var cons = new BaseConsStub();
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);

			var cmd = new VsCommand("");
			cmd.Perform(new List<string> {"none"});
			Assert.AreEqual(0, prog.Files.Count);

			Assert.AreEqual(MsgNo, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform0SlnOrCsproj()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 1 sln
		/// </summary>
		[Test]
		public void TestPerform1Sln()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> {"1s"});
			Assert.AreEqual(1, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\single.sln", prog.Files[0]);

			Assert.AreEqual(Msg1S, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform1Sln()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 2 sln
		/// </summary>
		[Test]
		public void TestPerform2Sln()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> { "2s" });
			Assert.AreEqual(2, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\first.sln", prog.Files[0]);
			Assert.AreEqual($@"start {CurrDir()}\second.sln", prog.Files[1]);

			Assert.AreEqual(Msg2S, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform2Sln()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 1 csproj
		/// </summary>
		[Test]
		public void TestPerform1Csproj()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> { "1c" });
			Assert.AreEqual(1, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\single.csproj", prog.Files[0]);

			Assert.AreEqual(Msg1C, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform1Csproj()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 2 csproj
		/// </summary>
		[Test]
		public void TestPerform2Csproj()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> { "2c" });
			Assert.AreEqual(2, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\first.csproj", prog.Files[0]);
			Assert.AreEqual($@"start {CurrDir()}\second.csproj", prog.Files[1]);

			Assert.AreEqual(Msg2C, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform2Csproj()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 0 sln allowed through the filter
		/// </summary>
		[Test]
		public void TestPerformFiltered0Allowed()
		{
			var cons = new BaseConsStub();
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);

			var cmd = new VsCommand("");
			cmd.Perform(new List<string> {"f1"});
			Assert.AreEqual(0, prog.Files.Count);

			Assert.AreEqual(Msg1F, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform2Csproj()
		
		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 1 sln allowed through the filter
		/// </summary>
		[Test]
		public void TestPerformFiltered1Allowed()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> {"f2"});
			Assert.AreEqual(1, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\first.sln", prog.Files[0]);

			Assert.AreEqual(Msg2F, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformFiltered1Allowed()
		
		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with 2 sln allowed through the filter
		/// </summary>
		[Test]
		public void TestPerformFiltered2Allowed()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> { "f3" });
			Assert.AreEqual(2, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\first.sln", prog.Files[0]);
			Assert.AreEqual($@"start {CurrDir()}\second.sln", prog.Files[1]);

			Assert.AreEqual(Msg3F, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformFiltered2Allowed()
		
		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with nothing filtered due to dll in other info
		/// </summary>
		[Test]
		public void TestPerformFilteredDll()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> { "f4" });
			Assert.AreEqual(2, prog.Files.Count);
			Assert.AreEqual($@"start {CurrDir()}\first.sln", prog.Files[0]);
			Assert.AreEqual($@"start {CurrDir()}\second.sln", prog.Files[1]);

			Assert.AreEqual(Msg4F, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformFilteredDll()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" /> with arguments letting it hit all paths
		/// </summary>
		[Test]
		public void TestPerformAllPaths()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			const string expected =
				MsgEh + MsgFa + MsgNo + Msg1S + Msg2S + Msg1C + Msg2C + Msg1F + Msg2F + Msg3F + Msg4F;

			var cd = CurrDir();
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new VsCommand("");
			cmd.Perform(new List<string> { "eh", "f", "none", "1s", "2s", "1c", "2c", "f1", "f2", "f3", "f4" });
			Assert.AreEqual(11, prog.Files.Count);
			Assert.AreEqual($@"start {cd}\single.sln", prog.Files[0]);
			Assert.AreEqual($@"start {cd}\first.sln", prog.Files[1]);
			Assert.AreEqual($@"start {cd}\second.sln", prog.Files[2]);
			Assert.AreEqual($@"start {cd}\single.csproj", prog.Files[3]);
			Assert.AreEqual($@"start {cd}\first.csproj", prog.Files[4]);
			Assert.AreEqual($@"start {cd}\second.csproj", prog.Files[5]);
			Assert.AreEqual($@"start {cd}\first.sln", prog.Files[6]);
			Assert.AreEqual($@"start {cd}\first.sln", prog.Files[7]);
			Assert.AreEqual($@"start {cd}\second.sln", prog.Files[8]);
			Assert.AreEqual($@"start {cd}\first.sln", prog.Files[9]);
			Assert.AreEqual($@"start {cd}\second.sln", prog.Files[10]);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerformAllPaths()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" />, verifying we call to the solution colorizer once per solution opened
		/// </summary>
		[TestCase("1s", 1)]
		[TestCase("2s", 2)]
		[TestCase("1c", 1)]
		[TestCase("1s 2s 1c", 4)]
		[TestCase("eh", 0)]
		[TestCase("f", 0)]
		[TestCase("none", 0)]
		[TestCase("f1", 0)]
		public void TestPerformColorizedSolutionWasCalled(string args, int timesCalled)
		{
			// replace the generic colorizer stub with one we can check against
			var colorStub = new ColorizerStub();
			IoCContainer.Instance.RegisterAsSingleInstance<ISolutionColorizer, ColorizerStub>(colorStub);
			// we don't need to worry about replacing this one because the other tests don't care what's there, just that there's something

			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			var cmd = new VsCommand("s:pink|r:orange");
			cmd.Perform(args.Split(' ').ToList());

			Assert.AreEqual(timesCalled, colorStub.TimesCalled);

		}   // end TestPerformColorizedSolutionWasCalled()

		/// <summary>
		/// tests <see cref="VsCommand.Perform" />, verifying we don't call to the solution colorizer if no colors were passed in
		/// </summary>
		[Test]
		public void TestPerformColorizedSolutionNotCalledIfNoColorsPassedIn()
		{
			// replace the generic colorizer stub with one we can check against
			var colorStub = new ColorizerStub();
			IoCContainer.Instance.RegisterAsSingleInstance<ISolutionColorizer, ColorizerStub>(colorStub);
			// we don't need to worry about replacing this one because the other tests don't care what's there, just that there's something

			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			var cmd = new VsCommand("");
			cmd.Perform("1s 2s 1c".Split(' ').ToList());

			Assert.AreEqual(0, colorStub.TimesCalled);

		}   // end TestPerformColorizedSolutionNotCalledIfNoColorsPassedIn()

		#endregion Tests

		#region Helper

		private static string CurrDir() => Environment.CurrentDirectory.QuoteTheSpaces();

		#endregion Helper

		#region Inner Classes

		private class StubDir : BaseDirsStub
		{
			#region Implementation of IDirectories

			/// <inheritdoc />
			public override bool Exists(string path) => !path.Equals("fake");

			/// <inheritdoc />
			public override List<string> GetFiles(string path, string filter)
			{
				var empty = new List<string>();
				switch (path)
				{
					case "ones":
						return filter == "*.sln" ? new List<string> {"single.sln"} : empty;
					case "onec":
						return filter == "*.csproj" ? new List<string> {"single.csproj"} : empty;
					case "twos":
					case "filter1":
					case "filter2":
					case "filter3":
					case "filter4":
						return filter == "*.sln" ? new List<string> {"first.sln", "second.sln"} : empty;
					case "twoc":
						return filter == "*.csproj" ? new List<string> {"first.csproj", "second.csproj"} : empty;
					default:
						return empty;

				}	// end switch

			}	// end GetFiles(string, string)

			#endregion Implementation of IDirectories

		}	// end StubDir

		private class ProgStub : BaseProgStub
		{
			internal readonly List<string> Files = new List<string>();

			#region Implementation of IProgram

			/// <inheritdoc />
			public override bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true,
				bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				Files.Add(fileAndArgs);
				return true;

			}	// end RunBatchFile(string, ShowOutput, bool, bool, bool, DataReceivedEventHandler)

			#endregion Implementation of IProgram

		}	// end ProgStub

		/// <summary>
		/// used to load the shortcuts
		/// </summary>
		private class FilesStub : BaseFileStub
		{
			#region Implementation of IFiles

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			/// <inheritdoc />
			public override IEnumerable<string> ReadLines(string path) =>
				new List<string>
				{
					"1s=ones", "2s=twos", "1c=onec", "2c=twoc", "f=fake", "none=none",
					"f1=filter4=first.csproj", "f2=filter1=first.sln,second.csproj",
					"f3=filter2=first.sln,second.sln", "f4=filter3=first.dll"
				}.ToArray();

			#endregion Implementation of IFiles

		}	// end FilesStub
		
		private class ColorizerStub : BaseSolutionColorizer
		{
			public int TimesCalled { get; private set; }

			#region Implementation of BaseSolutionColorizer

			public override void ColorizeSolution(string colors, string path, string shortcut)
			{
				TimesCalled++;
			}	// end ColorizeSolution(string, string, string)

			#endregion Implementation of BaseSolutionColorizer

		}	// end ColorizerStub

		#endregion Inner Classes

	}	// end VsCommandTests

}	// end HubUnitTest.Commands
