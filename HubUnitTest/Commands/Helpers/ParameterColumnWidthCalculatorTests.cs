﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers;

namespace HubUnitTest.Commands.Helpers
{
	/// <summary>
	/// tests <see cref="ParameterColumnWidthCalculator"/>
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParameterColumnWidthCalculatorTests
	{
		/// <summary>
		/// Verifies that the correct values are returned
		/// </summary>
		// enough width for everyone
		[TestCase(90, 10, 15, 12, 12, 17, 14, false)]
		// Both too long, remaining is even
		[TestCase(90, 10, 45, 42, 12, 35, 35, true)]
		// Both too long, remaining is odd, both are same width
		[TestCase(90, 11, 45, 45, 13, 35, 34, true)]
		// Both too long, remaining is odd, current is longer
		[TestCase(90, 11, 45, 42, 13, 35, 34, true)]
		// Both too long, remaining is odd, default is longer
		[TestCase(90, 11, 42, 45, 13, 34, 35, true)]
		// Current too long
		[TestCase(90, 10, 60, 27, 12, 41, 29, true)]
		// Default too long
		[TestCase(90, 10, 30, 57, 12, 32, 38, true)]
		public void TestsThatValuesAreCorrect(int consoleWidth, int maxParaLen, int maxCValLen, int maxDValLen,
			int expParaLen, int expCValLen, int expDValLen, bool expWrap)
		{
			var info = ParameterColumnWidthCalculator.GetColumnsInfo(consoleWidth, maxParaLen, maxCValLen, maxDValLen);
			Assert.Multiple(() =>
			{
				Assert.AreEqual(expParaLen, info.ParaLen, "ParaLen was wrong");
				Assert.AreEqual(expCValLen, info.CValLen, "CValLen was wrong");
				Assert.AreEqual(expDValLen, info.DValLen, "DValLen was wrong");
				Assert.AreEqual(expWrap, info.NeedToWrap, "NeedToWrap was wrong");
			});
		}
	}
}
