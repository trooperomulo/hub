﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Commands.Helpers;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

using Ecap = Hub.Commands.Helpers.EmailCommandArgumentParser;

namespace HubUnitTest.Commands.Helpers
{
	/// <summary>
	/// tests <see cref="EmailCommandArgumentParser" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class EmailCommandArgumentParserTests
	{
		#region Variable

		private static string _location;

		#endregion Variable

		#region Data and Tests

		///
		[OneTimeSetUp]
		public static void ClassSetup()
		{
			Env.SetEnvironmentVariable("local", "", true);
			Env.SetEnvironmentVariable("version_tree", "", true);
		}

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirsStub>(new DirsStub());
		}

		///
		[TearDown]
		public void Teardown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		private static IEnumerable RepoTestData
		{
			get
			{
				yield return new TestCaseData("branch", null, false, false, "Success", true, "Success");
				yield return new TestCaseData("branch", null, false, false, "default", true, "main");
				yield return new TestCaseData("branch", null, false, true, "default", true, "develop_nextGen");
				yield return new TestCaseData("branch", null, false, false, "", false, "main");
				yield return new TestCaseData("branch", null, false, true, "", false, "develop_nextGen");
				yield return new TestCaseData("xm8", new List<string>(), false, false, "", false, "xm8");
				yield return new TestCaseData("branch", new List<string>(), false, false, "", false, "branch");
				yield return new TestCaseData("branch", new List<string> {"a", "b", "c", "d"}, false, false, "", false, "branch");
				yield return new TestCaseData("branch", new List<string> {"a", "xm8", "c", "d"}, false, false, "", false, "xm8");
				yield return new TestCaseData("branch", new List<string> {"a", "xm8", "c", "d"}, true, false, "", false, "branch");
			} // end get
		}

		/// <summary>
		/// tests that <see cref="Ecap.Xm8Set" /> and <see cref="Ecap.Xm8" /> are set properly
		/// </summary>
		[Test]
		[TestCase("branch", "", false, false, "branch", TestName = "Xm8 not set")]
		[TestCase("branch", "default", false, true, "main", TestName = "Xm8 set to default, current gen")]
		[TestCase("branch", "default", true, true, "develop_nextGen", TestName = "Xm8 set to default, next gen")]
		[TestCase("branch", "harold", false, true, "harold", TestName = "Xm8 set to real value")]
		public void TestXm8(string branch, string arg, bool ng, bool set, string xm8)
		{
			var git = new GitStub(branch, null);
			const bool pc = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(x: arg);
			bool SetGetter(Ecap ecap) => ecap.Xm8Set;
			string PropGetter(Ecap ecap) => ecap.Xm8;
			const string sName = nameof(Ecap.Xm8Set);
			const string pName = nameof(Ecap.Xm8);

			TestPair(git, pc, ng, args, set, xm8, SetGetter, PropGetter, sName, pName);
		}

		/// <summary>
		/// tests that <see cref="Ecap.DataSet" /> and <see cref="Ecap.Data" /> are set properly
		/// </summary>
		[Test]
		[TestCaseSource(nameof(RepoTestData))]
		public void TestData(string branch, List<string> branches, bool prefer, bool next, string arg, bool set,
			string data)
		{
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(t: arg);
			bool SetGetter(Ecap ecap) => ecap.DataSet;
			string PropGetter(Ecap ecap) => ecap.Data;
			const string sName = nameof(Ecap.DataSet);
			const string pName = nameof(Ecap.Data);

			Utils.DataFolder = "data";
			data = data == "main" ? "28" : data;	// the data repo defaults to 28 in current gen
			TestRepo("data", args, SetGetter, PropGetter, sName, pName, branch, branches, prefer, next, set, data);
		}

		/// <summary>
		/// tests that <see cref="Ecap.FolderSet" /> and <see cref="Ecap.Folder" /> are set properly
		/// </summary>
		[Test]
		[TestCase("branch", "folder", true, "folder", TestName = "Folder Set")]
		[TestCase("branch", "", false, "branch", TestName = "Simple Git")]
		[TestCase("one/branch", "", false, "branch", TestName = "Double Git")]
		[TestCase("one/two/branch", "", false, "branch", TestName = "Triple Git")]
		public void TestFolder(string branch, string arg, bool set, string folder)
		{
			var git = new GitStub(branch, null);
			const bool pc = false;
			const bool ng = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(arg);
			bool SetGetter(Ecap ecap) => ecap.FolderSet;
			string PropGetter(Ecap ecap) => ecap.Folder;
			const string sName = nameof(Ecap.FolderSet);
			const string pName = nameof(Ecap.Folder);

			TestPair(git, pc, ng, args, set, folder, SetGetter, PropGetter, sName, pName);
		}

		/// <summary>
		/// tests that <see cref="Ecap.PatchSet" /> and <see cref="Ecap.Patch" /> are set properly
		/// </summary>
		[Test]
		[TestCase("5.6.8.7", true, "5.6.8.7", TestName = "Patch Set 1")]
		[TestCase("patched", true, "patched", TestName = "Patch Set 2")]
		[TestCase("", false, "", TestName = "Patch Not Set")]
		public void TestPatch(string arg, bool set, string patch)
		{
			var git = new GitStub("", null);
			const bool pc = false;
			const bool ng = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(p: arg);
			bool SetGetter(Ecap ecap) => ecap.PatchSet;
			string PropGetter(Ecap ecap) => ecap.Patch;
			const string sName = nameof(Ecap.PatchSet);
			const string pName = nameof(Ecap.Patch);

			TestPair(git, pc, ng, args, set, patch, SetGetter, PropGetter, sName, pName);
		}

		/// <summary>
		/// tests that <see cref="Ecap.MajorSet" /> and <see cref="Ecap.Major" /> are set properly
		/// </summary>
		[Test]
		[TestCase("4", false, true, 4, TestName = "Major Set Good 1")]
		[TestCase("892", false, true, 892, TestName = "Major Set Good 2")]
		[TestCase("as", false, true, -1, TestName = "Major Set Bad, Current Gen")]
		[TestCase("as", true, true, 2, TestName = "Major Set Bad, Next Gen")]
		[TestCase("", false, false, -1, TestName = "Major Not Set, Current Gen")]
		[TestCase("", true, false, 2, TestName = "Major Not Set, Next Gen")]
		public void TestMajor(string arg, bool ng, bool set, int major)
		{
			var git = new GitStub("", null);
			const bool pc = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(j: arg);
			bool SetGetter(Ecap ecap) => ecap.MajorSet;
			int PropGetter(Ecap ecap) => ecap.Major;
			const string sName = nameof(Ecap.MajorSet);
			const string pName = nameof(Ecap.Major);

			TestPair(git, pc, ng, args, set, major, SetGetter, PropGetter, sName, pName);
		}

		/// <summary>
		/// tests that <see cref="Ecap.MinorSet" /> and <see cref="Ecap.Minor" /> are set properly
		/// </summary>
		[Test]
		[TestCase("4", true, 4, TestName = "Minor Set 1")]
		[TestCase("892", true, 892, TestName = "Minor Set 2")]
		[TestCase("", false, -1, TestName = "Minor Not Set")]
		public void TestMinor(string arg, bool set, int minor)
		{
			var git = new GitStub("", null);
			const bool pc = false;
			const bool ng = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(n: arg);
			bool SetGetter(Ecap ecap) => ecap.MinorSet;
			int PropGetter(Ecap ecap) => ecap.Minor;
			const string sName = nameof(Ecap.MinorSet);
			const string pName = nameof(Ecap.Minor);

			TestPair(git, pc, ng, args, set, minor, SetGetter, PropGetter, sName, pName);
		}

		/// <summary>
		/// tests that <see cref="Ecap.BuildSet" /> and <see cref="Ecap.Build" /> are set properly
		/// </summary>
		[Test]
		[TestCase("4", true, 4, TestName = "Build Set 1")]
		[TestCase("892", true, 892, TestName = "Build Set 2")]
		[TestCase("", false, -1, TestName = "Build Not Set")]
		public void TestBuild(string arg, bool set, int build)
		{
			var git = new GitStub("", null);
			const bool pc = false;
			const bool ng = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(u: arg);
			bool SetGetter(Ecap ecap) => ecap.BuildSet;
			int PropGetter(Ecap ecap) => ecap.Build;
			const string sName = nameof(Ecap.BuildSet);
			const string pName = nameof(Ecap.Build);

			TestPair(git, pc, ng, args, set, build, SetGetter, PropGetter, sName, pName);
		}

		/// <summary>
		/// tests that <see cref="Ecap.RevisionSet" /> and <see cref="Ecap.Revision" /> are set properly
		/// </summary>
		[Test]
		[TestCase("4", true, 4, TestName = "Revision Set 1")]
		[TestCase("892", true, 892, TestName = "Revision Set 2")]
		[TestCase("", false, -1, TestName = "Revision Not Set")]
		public void TestRevision(string arg, bool set, int revision)
		{
			var git = new GitStub("", null);
			const bool pc = false;
			const bool ng = false;
			var args = arg.IsNullOrEmpty() ? new List<string>() : GetArgs(r: arg);
			bool SetGetter(Ecap ecap) => ecap.RevisionSet;
			int PropGetter(Ecap ecap) => ecap.Revision;
			const string sName = nameof(Ecap.RevisionSet);
			const string pName = nameof(Ecap.Revision);

			TestPair(git, pc, ng, args, set, revision, SetGetter, PropGetter, sName, pName);
		}

		#endregion Data and Tests

		#region Test Helpers

		private static void TestRepo(string remote, List<string> args, Func<Ecap, bool> setGetter,
			Func<Ecap, string> propGetter, string sName, string pName, string branch, List<string> branches, bool prefer, bool next,
			bool set, string repo)
		{
			var git = new GitStub(branch, branches, remote);

			TestPair(git, prefer, next, args, set, repo, setGetter, propGetter, sName, pName);
		}

		private static void TestPair<T>(GitStub git, bool pc, bool ng, List<string> args, bool set, T prop,
			Func<Ecap, bool> setGetter, Func<Ecap, T> propGetter, string setName, string propName)
		{
			_location = "";
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(git);
			var parser = new Ecap(pc, git.GetBranchName(), ng, args);
			Assert.AreEqual(set, setGetter(parser), $"{setName} was incorrectly set");
			Assert.AreEqual(prop, propGetter(parser), $"{propName} was incorrectly set");
			IoCContainer.Instance.RemoveRegisteredType<IGit>();
		}
		
		#endregion Test Helpers

		#region Data Helpers

		private static string GetArg(string existing, string arg, string flag)
		{
			return arg.IsNullOrEmpty() ? $"{existing}" : $"{existing} -{flag} {arg}".Trim();
		}

		private static List<string> GetArgs(string f = "", string x = "", string s = "", string e = "", string t = "",
			string b = "", string p = "", string j = "", string n = "", string u = "", string r = "")
		{
			var args = GetArg("", f, "f");
			args = GetArg(args, x, "x");
			args = GetArg(args, s, "s");
			args = GetArg(args, e, "dp");
			args = GetArg(args, t, "dt");
			args = GetArg(args, b, "bm");
			args = GetArg(args, p, "p");
			args = GetArg(args, j, "ma");
			args = GetArg(args, n, "mi");
			args = GetArg(args, u, "bu");
			args = GetArg(args, r, "r");

			return args.Split(' ').ToList();
		}

		#endregion Data Helpers

		#region Inner Classes

		private class GitStub : BaseGitStub
		{
			#region Variables

			private readonly string _branch;
			private readonly List<string> _branches;
			private readonly string _test;

			#endregion Variables

			#region Implementation of IGit

			/// <inheritdoc />
			public override string GetBranchName() => _test.IsNullOrEmpty() || _test == _location ? _branch : "xm8";

			/// <inheritdoc />
			public override List<string> GetBranches(bool remote) => _branches;

			#endregion Implementation of IGit

			#region Constructor

			public GitStub(string branch, List<string> branches, string test = "")
			{
				_branch = branch;
				_branches = branches;
				_test = test;
			}

			#endregion Constructor
		}
		
		private class DirsStub : BaseDirsStub
		{
			#region Implementation of IDirectories

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			/// <inheritdoc />
			// ReSharper disable once StringLastIndexOfIsCultureSpecific.1
			public override void SetCurrentDirectory(string path) => _location = path.Substring(path.LastIndexOf("\\") + 1);

			#endregion Implementation of IDirectories
		}

		#endregion Inner Classes
	}
}
