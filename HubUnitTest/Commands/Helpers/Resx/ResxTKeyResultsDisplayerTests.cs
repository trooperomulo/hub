﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Commands.Helpers.Resx;
using Hub.Interface;

using HubUnitTest.Stubs;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Helpers.Resx
{
	/// <summary>
	/// tests <see cref="ResxTKeyResultsDisplayer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTKeyResultsDisplayerTests
	{
		private static readonly Dictionary<string, Mock<ResxTFolderResultsDisplayer>> _mocks =
			new Dictionary<string, Mock<ResxTFolderResultsDisplayer>>();

		/// <summary>
		/// tests <see cref="ResxTKeyResultsDisplayer.PrintResults" />
		/// </summary>
		[Test]
		public void TestPrintResults()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var folders = Rnd.GetRandomListString();

			const string key = "KEY";
			var results = new ResxTKeyResults(key, folders);

			var displayer = new ResxTKeyResultsDisplayer(results);

			ResxTKeyResultsDisplayer.GetChildDisplayer = GetChildDisplayer;

			displayer.PrintResults();

			var keys = _mocks.Keys.ToList();
			Assert.IsTrue(UnitTestUtil.ListsHaveSameItems(folders, keys), "not all the folders were used properly");

			var vals = _mocks.Values.ToList();
			foreach (var mock in vals)
			{
				mock.Verify(m => m.PrintResults(), Times.Once);
			}

			Assert.AreEqual($"Translations for RS.{key}\r\n{Out.DashLine}\r\n{Out.DashLine}\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		private static ResxTFolderResultsDisplayer GetChildDisplayer(ResxTFolderResults results)
		{
			var mock = new Mock<ResxTFolderResultsDisplayer>(results);
			mock.Setup(m => m.PrintResults());
			_mocks.Add(results.Folder, mock);
			return mock.Object;
		}
	}
}
