﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.Resx;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Helpers.Resx
{
	/// <summary>
	/// tests <see cref="ResxTFolderResults" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTFolderResultsTests
	{
		/// <summary>
		/// tests the <see cref="ResxTFolderResults" /> constructor
		/// </summary>
		[Test]
		public void TestConstructor()
		{
			var folder = Rnd.GetRandomStr();
			var results = new ResxTFolderResults(folder);

			Assert.AreEqual(folder, results.Folder, $"{nameof(results.Folder)} wasn't properly set");
			Assert.IsTrue(results.Values != null && results.Values.Count == 0,
				$"{nameof(results.Values)} wasn't properly set");
		}

		/// <summary>
		/// tests <see cref="ResxTFolderResults.AddResultFromLang" />
		/// </summary>
		[Test]
		public void TestAddResultFromLang()
		{
			var results = new ResxTFolderResults("");
			AddItemAndTest(results, 1);
			AddItemAndTest(results, 2);
			AddItemAndTest(results, 3);
		}

		private void AddItemAndTest(ResxTFolderResults results, int count)
		{
			(string lang, string val) value = (Rnd.GetRandomStr(), Rnd.GetRandomStr());
			results.AddResultFromLang(value.lang, value.val);

			Assert.IsTrue(results.Values.Count == count, $"Value #{count} wasn't added properly");
			Assert.AreEqual(value.lang, results.Values[count - 1].lang,
				$"{nameof(value.lang)} wasn't set properly on item #{count}");
			Assert.AreEqual(value.val, results.Values[count - 1].val,
				$"{nameof(value.val)} wasn't set properly on item #{count}");
		}
	}
}
