﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using NUnit.Framework;

using Moq;

using Hub.Commands.Helpers.Resx;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Helpers.Resx
{
	/// <summary>
	/// tests <see cref="ResxTResultsDisplayer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTResultsDisplayerTests
	{
		private static readonly Dictionary<string, Mock<ResxTKeyResultsDisplayer>> _mocks =
			new Dictionary<string, Mock<ResxTKeyResultsDisplayer>>();

		/// <summary>
		/// tests <see cref="ResxTKeyResultsDisplayer.PrintResults" />
		/// </summary>
		[Test]
		public void TestPrintResults()
		{
			var keys = Rnd.GetRandomListString();

			var results = new ResxTResults(keys, Rnd.GetRandomListString());

			var displayer = new ResxTResultsDisplayer(results);

			ResxTResultsDisplayer.GetChildDisplayer = GetChildDisplayer;

			void Test()
			{
				displayer.PrintResults();

				var ks = _mocks.Keys.ToList();
				Assert.IsTrue(UnitTestUtil.ListsHaveSameItems(keys, ks), "not all the keys were used properly");

				var vals = _mocks.Values.ToList();
				foreach (var mock in vals)
				{
					mock.Verify(m => m.PrintResults(), Times.Once);
				}
			}

			UnitTestUtil.TestConsoleOutput(Test, "");

		}

		private static ResxTKeyResultsDisplayer GetChildDisplayer(ResxTKeyResults results)
		{
			var mock = new Mock<ResxTKeyResultsDisplayer>(results);
			mock.Setup(m => m.PrintResults());
			_mocks.Add(results.Key, mock);
			return mock.Object;
		}
	}
}
