﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Commands.Helpers.Resx;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Helpers.Resx
{
	/// <summary>
	/// tests <see cref="ResxTResults" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTResultsTests
	{
		#region Variables

		private static readonly Dictionary<string, Mock<ResxTKeyResults>> _mocks =
			new Dictionary<string, Mock<ResxTKeyResults>>();

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests the <see cref="ResxTResults" /> constructor
		/// </summary>
		[Test]
		public void TestConstructor()
		{
			TestKeysAreMadeProperly(GetInstance(out var folders, out var keys), keys, folders);
		}

		/// <summary>
		/// tests <see cref="ResxTKeyResults.AddResultFromLang" />
		/// </summary>
		[Test]
		public void TestAddResultFromLang()
		{
			// arrange
			var result = GetInstance(out var folders, out var keys);
			ConvertChildResultToMock(result, folders);

			var valuesAdded = new Dictionary<string, List<(string folder, string lang, string val)>>();

			// act
			AddValues(result, keys, ref valuesAdded);
			AddValues(result, keys, ref valuesAdded);
			AddValues(result, keys, ref valuesAdded);

			// assert
			TestMockCalls(valuesAdded);
		}

		#endregion Tests

		#region Helpers

		private static void AddValues(ResxTResults results, List<string> keys,
			ref Dictionary<string, List<(string folder, string lang, string val)>> valuesAdded)
		{

			var ktv = Rnd.GetRandomDictStringString(() => Rnd.GetRandomItemFromList(keys));

			var f = Rnd.GetRandomStr();
			var l = Rnd.GetRandomStr();

			foreach (var kvp in ktv)
			{
				var k = kvp.Key;
				var v = kvp.Value;
				valuesAdded.Add(k, (f, l, v));
			}

			results.AddResultsFromLang(f, l, ktv);
		}

		private static void ConvertChildResultToMock(ResxTResults results, List<string> folders)
		{
			var keys = results.Keys.Keys.ToList();
			foreach (var key in keys)
			{
				var mock = new Mock<ResxTKeyResults>(key, folders);
				mock.Setup(m => m.AddResultFromLang(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
				_mocks.Add(key, mock);
				results.Keys[key] = mock.Object;
			}
		}

		private static ResxTResults GetInstance(out List<string> folders, out List<string> keys)
		{
			keys = Rnd.GetRandomListString();
			folders = Rnd.GetRandomListString();
			return new ResxTResults(keys, folders);
		}

		private static void TestKeysAreMadeProperly(ResxTResults results, List<string> keys, List<string> folders)
		{
			Assert.AreEqual(keys.Count, results.Keys.Count, "the wrong number of keys were made");
			foreach (var key in keys)
			{
				Assert.IsTrue(results.Keys.ContainsKey(key), $"{key} wasn't in the results dictionary");
				var result = results.Keys[key];
				Assert.AreEqual(key, result.Key, $"{key} wasn't set as the key for its {nameof(ResxTKeyResults)}");
				UnitTestUtil.TestListsAreEqual(folders, result.FolderResults.Keys.ToList());
			}
		}

		private static void TestMockCalls(Mock<ResxTKeyResults> mock, List<(string folder, string lang, string val)> valuesAdded)
		{
			foreach (var (folder, lang, val) in valuesAdded)
			{
				mock.Verify(m => m.AddResultFromLang(folder, lang, val), Times.Once);
			}
		}

		private static void TestMockCalls(Dictionary<string, List<(string folder, string lang, string val)>> valuesAdded)
		{
			var addedKeys = valuesAdded.Keys.ToList();
			foreach (var key in addedKeys)
			{
				TestMockCalls(_mocks[key], valuesAdded[key]);
			}

			foreach (var kvp in _mocks)
			{
				kvp.Value.VerifyNoOtherCalls();
			}
		}

		#endregion Helpers
	}
}
