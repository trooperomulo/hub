﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using NUnit.Framework;

using Moq;

using RyanCoreTests;

using Hub;
using Hub.Commands.Helpers.Resx;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Helpers.Resx
{
	/// <summary>
	/// tests <see cref="ResxTKeyResults" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTKeyResultsTests
	{
		#region Variable

		private static readonly Dictionary<string, Mock<ResxTFolderResults>> _mocks =
			new Dictionary<string, Mock<ResxTFolderResults>>();

		#endregion Variable

		#region Tests

		/// <summary>
		/// tests the <see cref="ResxTKeyResults" /> constructor
		/// </summary>
		[Test]
		public void TestConstructor()
		{
			var results = GetInstance(out var folders, out var key);

			Assert.AreEqual(key, results.Key, $"{nameof(results.Key)} wasn't set properly");
			CoreTestUtils.TestDictionariesAreEqual(GetExpectedFolderResults(folders), results.FolderResults, RfrAreEqual);
		}

		/// <summary>
		/// tests <see cref="ResxTKeyResults.AddResultFromLang" />
		/// </summary>
		[Test]
		public void TestAddResultFromLang()
		{
			// arrange
			var result = GetInstance(out var folders, out _);
			ConvertChildResultToMock(result);

			var foldersToAddTo = new List<int>();

			foldersToAddTo.AddRange(Rnd.GetRandomListInt(folders.Count, folders.Count / 2));
			foldersToAddTo.AddRange(Rnd.GetRandomListInt(folders.Count, folders.Count / 2));
			foldersToAddTo.AddRange(Rnd.GetRandomListInt(folders.Count, folders.Count / 2));

			// act
			AddValues(result, folders, foldersToAddTo, out var valuesAdded);

			// assert
			TestMockCalls(valuesAdded);
		}

		#endregion Tests

		#region Helpers

		private static void AddValues(ResxTKeyResults results, List<string> folders, List<int> indexesToAddTo,
			out Dictionary<string, List<(string lang, string val)>> valuesAdded)
		{
			valuesAdded = new Dictionary<string, List<(string lang, string val)>>();
			foreach (var index in indexesToAddTo)
			{
				var f = folders[index];
				var l = Rnd.GetRandomStr();
				var v = Rnd.GetRandomStr();
				valuesAdded.Add(f, (l, v));
				results.AddResultFromLang(f, l, v);
			}
		}

		private static void ConvertChildResultToMock(ResxTKeyResults results)
		{
			var keys = results.FolderResults.Keys.ToList();
			foreach (var key in keys)
			{
				var mock = new Mock<ResxTFolderResults>(key);
				mock.Setup(m => m.AddResultFromLang(It.IsAny<string>(), It.IsAny<string>()));
				_mocks.Add(key, mock);
				results.FolderResults[key] = mock.Object;
			}
		}

		private static Dictionary<string, ResxTFolderResults> GetExpectedFolderResults(List<string> folders)
		{
			var result = new Dictionary<string, ResxTFolderResults>();

			foreach (var folder in folders)
			{
				result.Add(folder, new ResxTFolderResults(folder));
			}

			return result;
		}

		private static ResxTKeyResults GetInstance(out List<string> folders, out string key)
		{
			key = Rnd.GetRandomStr();
			folders = Rnd.GetRandomListString();
			return new ResxTKeyResults(key, folders);
		}

		private static bool RfrAreEqual(ResxTFolderResults one, ResxTFolderResults two)
		{
			return one.Folder == two.Folder;
		}
		
		private static void TestMockCalls(Mock<ResxTFolderResults> mock, List<(string lang, string val)> valuesAdded)
		{
			foreach (var (lang, val) in valuesAdded)
			{
				mock.Verify(m => m.AddResultFromLang(lang, val), Times.Once);
			}
		}

		private static void TestMockCalls(Dictionary<string, List<(string lang, string val)>> valuesAdded)
		{
			var addedKeys = valuesAdded.Keys.ToList();
			foreach (var key in addedKeys)
			{
				TestMockCalls(_mocks[key], valuesAdded[key]);
			}

			foreach (var kvp in _mocks)
			{
				kvp.Value.VerifyNoOtherCalls();
			}
		}

		#endregion Helpers
	}
}
