﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands.Helpers.Resx;
using Hub.Interface;

using HubUnitTest.Stubs;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Helpers.Resx
{
	/// <summary>
	/// tests <see cref="ResxTFolderResultsDisplayer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxTFolderResultsDisplayerTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="ResxTFolderResultsDisplayer.PrintResults" /> when the underlying <see cref="ResxTFolderResults" /> has no values
		/// </summary>
		[Test]
		public void TestPrintResultsWithNoValues()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var results = GetResults(false);

			var lines = GetExpectedLines(results);

			InsertLine(lines, "No translations found");

			var expected = LinesToExpected(lines);

			var displayer = new ResxTFolderResultsDisplayer(results);

			displayer.PrintResults();

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ResxTFolderResultsDisplayer.PrintResults" /> when the underlying <see cref="ResxTFolderResults" /> has values
		/// </summary>
		[Test]
		public void TestPrintResultsWithValues()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var results = GetResults(true);

			var lines = GetExpectedLines(results);

			foreach (var (lang, val) in results.Values)
			{
				InsertLine(lines, $"{lang} - {val}");
			}

			var expected = LinesToExpected(lines);

			var displayer = new ResxTFolderResultsDisplayer(results);

			displayer.PrintResults();

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Helpers

		private static List<string> GetExpectedLines(ResxTFolderResults results)
		{
			return new List<string> {Out.DashLine, results.Folder, Out.DashLine, "", ""};
		}

		private static ResxTFolderResults GetResults(bool hasValues)
		{
			var results = new ResxTFolderResults(Rnd.GetRandomStr());
			if (!hasValues)
			{
				return results;
			}

			var howMany = Rnd.Int(2, 5);
			for (var i = 0; i < howMany; i++)
			{
				results.AddResultFromLang(Rnd.GetRandomStr(), Rnd.GetRandomStr());
			}

			return results;
		}

		private static void InsertLine(List<string> lines, string line)
		{
			lines.Insert(lines.Count - 2, line);
		}

		private static string LinesToExpected(List<string> lines)
		{
			var expected = "";
			foreach (var line in lines)
			{
				expected += $"{line}\r\n";
			}

			return expected;
		}

		#endregion Helpers
	}
}
