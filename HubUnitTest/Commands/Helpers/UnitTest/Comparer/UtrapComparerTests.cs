﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

using NUnit.Framework;

namespace HubUnitTest.Commands.Helpers.UnitTest.Comparer
{
	/// <summary>
	/// Tests <see cref="UtrapComparer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UtrapComparerTests
	{
		/// <summary>
		/// tests that an exception is thrown if the solutions don't match
		/// </summary>
		[Test]
		public void TestBadSolutions()
		{
			var before = new UnitTestResultAssemblyParser("a", 1, 1, 1, 1, true, new List<TestCase>());
			var after = new UnitTestResultAssemblyParser("b", 1, 1, 1, 1, true, new List<TestCase>());
			var ex = Assert.Throws<ArgumentException>(() => new UtrapComparer(before, after));
			Assert.AreEqual("Solution names don't match", ex.Message);
		}

		/// <summary>
		/// tests the constructor
		/// </summary>
		[Test]
		public void TestConstructor()
		{
			var befores = new List<TestCase>
			{
				new TestCase("b1", TestCaseResult.Skip, TimeSpan.FromSeconds(56)),
				new TestCase("b2", TestCaseResult.Pass, TimeSpan.FromSeconds(39)),
				new TestCase("b3", TestCaseResult.Fail, TimeSpan.FromSeconds(20)),
				new TestCase("b4", TestCaseResult.Pass, TimeSpan.FromSeconds(2)),
				new TestCase("b5", TestCaseResult.Fail, TimeSpan.FromSeconds(08)),
				new TestCase("b6", TestCaseResult.Skip, TimeSpan.FromSeconds(32)),
				new TestCase("b7", TestCaseResult.Pass, TimeSpan.FromSeconds(98)),
				new TestCase("b8", TestCaseResult.Fail, TimeSpan.FromSeconds(52)),
				new TestCase("b9", TestCaseResult.Pass, TimeSpan.FromSeconds(47)),
				new TestCase("b10", TestCaseResult.Fail, TimeSpan.FromSeconds(3))
			};
			var afters = new List<TestCase>
			{
				new TestCase("b6", TestCaseResult.Pass, TimeSpan.FromSeconds(32)),
				new TestCase("b7", TestCaseResult.Pass, TimeSpan.FromSeconds(98)),
				new TestCase("b8", TestCaseResult.Pass, TimeSpan.FromSeconds(52)),
				new TestCase("b9", TestCaseResult.Skip, TimeSpan.FromSeconds(47)),
				new TestCase("b10", TestCaseResult.Fail, TimeSpan.FromSeconds(3)),
				new TestCase("b11", TestCaseResult.Skip, TimeSpan.FromSeconds(56)),
				new TestCase("b12", TestCaseResult.Pass, TimeSpan.FromSeconds(39)),
				new TestCase("b13", TestCaseResult.Fail, TimeSpan.FromSeconds(20)),
				new TestCase("b14", TestCaseResult.Pass, TimeSpan.FromSeconds(2)),
				new TestCase("b15", TestCaseResult.Fail, TimeSpan.FromSeconds(08))
			};
			var before = new UnitTestResultAssemblyParser("sol", 4, 4, 2, 10, false, befores);
			var after = new UnitTestResultAssemblyParser("sol", 4, 4, 2, 10, false, afters);

			var comparer = new UtrapComparer(before, after);
			Assert.AreEqual(15, comparer.Comparers.Count, "wrong number of comparers");
			TestTcComparer(comparer, 0, "b1", ComparisonResult.Removed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 1, "b10", ComparisonResult.Same, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 2, "b11", ComparisonResult.Added, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 3, "b12", ComparisonResult.Added, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 4, "b13", ComparisonResult.Added, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 5, "b14", ComparisonResult.Added, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 6, "b15", ComparisonResult.Added, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 7, "b2", ComparisonResult.Removed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 8, "b3", ComparisonResult.Removed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 9, "b4", ComparisonResult.Removed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 10, "b5", ComparisonResult.Removed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 11, "b6", ComparisonResult.Fixed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 12, "b7", ComparisonResult.Same, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 13, "b8", ComparisonResult.Fixed, BrokeDifferentlyEnum.Not);
			TestTcComparer(comparer, 14, "b9", ComparisonResult.Broken, BrokeDifferentlyEnum.Not);
		}

		private static void TestTcComparer(UtrapComparer comparer, int index, string sol, ComparisonResult cRes, BrokeDifferentlyEnum bDif)
		{
			var tcComp = comparer.Comparers[index];
			Assert.AreEqual(sol, tcComp.Before?.Name ?? tcComp.After.Name, $"Name was wrong for #{index}");
			Assert.AreEqual(cRes, tcComp.ComparisonResult, $"Comparison result was wrong for #{index}");
			Assert.AreEqual(bDif, tcComp.BrokeDifferently, $"Broke differently was wrong for #{index}");
		}
	}
}
