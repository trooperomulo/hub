﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest.Comparer
{
	/// <summary>
	/// Tests <see cref="UtrpComparer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UtrpComparerTests
	{
		/// <summary>
		/// tests the constructor
		/// </summary>
		[Test]
		public void TestConstructor()
		{
			var befores = new List<UnitTestResultAssemblyParser>
			{
				GetParser("b1"),
				GetParser("b2"),
				GetParser("b3"),
				GetParser("b4"),
				GetParser("b5"),
				GetParser("b6"),
				GetParser("b7"),
				GetParser("b8"),
				GetParser("b9"),
				GetParser("b10")
			};
			var afters = new List<UnitTestResultAssemblyParser>
			{
				GetParser("b6"),
				GetParser("b7"),
				GetParser("b8"),
				GetParser("b9"),
				GetParser("b10"),
				GetParser("b11"),
				GetParser("b12"),
				GetParser("b13"),
				GetParser("b14"),
				GetParser("b15")
			};
			var before = new UnitTestResultParser(befores);
			var after = new UnitTestResultParser(afters);

			var comparer = new UtrpComparer(before, after);
			Assert.AreEqual(15, comparer.Comparers.Count, "wrong number of comparers");
			TestTcComparer(comparer, 0, "b1", ComparisonResult.Removed);
			TestTcComparer(comparer, 1, "b10", ComparisonResult.Same);
			TestTcComparer(comparer, 2, "b11", ComparisonResult.Added);
			TestTcComparer(comparer, 3, "b12", ComparisonResult.Added);
			TestTcComparer(comparer, 4, "b13", ComparisonResult.Added);
			TestTcComparer(comparer, 5, "b14", ComparisonResult.Added);
			TestTcComparer(comparer, 6, "b15", ComparisonResult.Added);
			TestTcComparer(comparer, 7, "b2", ComparisonResult.Removed);
			TestTcComparer(comparer, 8, "b3", ComparisonResult.Removed);
			TestTcComparer(comparer, 9, "b4", ComparisonResult.Removed);
			TestTcComparer(comparer, 10, "b5", ComparisonResult.Removed);
			TestTcComparer(comparer, 11, "b6", ComparisonResult.Same);
			TestTcComparer(comparer, 12, "b7", ComparisonResult.Same);
			TestTcComparer(comparer, 13, "b8", ComparisonResult.Same);
			TestTcComparer(comparer, 14, "b9", ComparisonResult.Same);
		}

		private UnitTestResultAssemblyParser GetParser(string solution)
		{
			return new UnitTestResultAssemblyParser(solution, 3, 10, 1, 14, false, new List<TestCase>());
		}

		private static void TestTcComparer(UtrpComparer comparer, int index, string sol, ComparisonResult cRes)
		{
			var tcComp = comparer.Comparers[index];
			Assert.AreEqual(sol, tcComp.Before?.Solution ?? tcComp.After.Solution, $"Name was wrong for #{index}");
			Assert.AreEqual(cRes, tcComp.Result, $"Comparison result was wrong for #{index}");
		}
	}
}
