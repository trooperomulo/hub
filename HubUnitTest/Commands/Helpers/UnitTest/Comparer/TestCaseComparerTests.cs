﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest.Comparer
{
	/// <summary>
	/// tests <see cref="TestCaseComparer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestCaseComparerTests
	{
		#region Tests

		/// <summary>
		/// tests that an exception is thrown if the names don't match
		/// </summary>
		[Test]
		public void TestBadNames()
		{
			var before = new TestCase("a", TestCaseResult.Pass, TimeSpan.Zero);
			var after = new TestCase("b", TestCaseResult.Pass, TimeSpan.Zero);
			var ex = Assert.Throws<ArgumentException>(() => new TestCaseComparer(before, after));
			Assert.AreEqual("Names don't match", ex.Message);
		}

		/// <summary>
		/// tests that an exception is thrown if either parser has an invalid result
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestExceptionBadResultData))]
		public void TestExceptionBadResult(int before, int after)
		{
			TestCase Parser(int result) =>
				GetTestCase("name", (TestCaseResult) result, TimeSpan.Zero);

			Assert.Throws<ArgumentOutOfRangeException>(() =>
				new TestCaseComparer(Parser(before), Parser(after)));
		}

		/// <summary>
		/// tests that the properties are initialized properly
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestPropertiesInitializedData))]
		public void TestPropertiesInitialized(TestCase before, TestCase after,
			ComparisonResult result, BrokeDifferentlyEnum diff, string name)
		{
			var comparer = new TestCaseComparer(before, after);
			Assert.AreEqual(result, comparer.ComparisonResult, $"{before.Result} and {after.Result} should result in {result}");
			Assert.AreEqual(diff, comparer.BrokeDifferently, $"{before.Result} and {after.Result} should result in {diff}");
		}

		#endregion Tests

		#region Helpers

		private static TestCase GetTestCase(string name, TestCaseResult result, TimeSpan duration, string message = "", List<string> stack = null)
		{
			return new TestCase(name, result, duration, message, stack);
		}

		private static IEnumerable GetTestExceptionBadResultData()
		{
			yield return new TestCaseData(0, 4);
			yield return new TestCaseData(1, 4);
			yield return new TestCaseData(2, 4);
			yield return new TestCaseData(3, 4);
			yield return new TestCaseData(4, 0);
			yield return new TestCaseData(4, 1);
			yield return new TestCaseData(4, 2);
			yield return new TestCaseData(4, 3);
			yield return new TestCaseData(4, 4);
		}

		private static IEnumerable GetTestPropertiesInitializedData()
		{
			const string a = "a";
			const string b = "a";
			var dur = TimeSpan.Zero;
			const TestCaseResult incl = TestCaseResult.Incl;
			const TestCaseResult fail = TestCaseResult.Fail;
			const TestCaseResult pass = TestCaseResult.Pass;
			const TestCaseResult skip = TestCaseResult.Skip;
			const ComparisonResult broke = ComparisonResult.Broken;
			const ComparisonResult fix = ComparisonResult.Fixed;
			const ComparisonResult same = ComparisonResult.Same;
			const ComparisonResult diff = ComparisonResult.BrokenDifferently;
			const BrokeDifferentlyEnum not = BrokeDifferentlyEnum.Not;

			// pass
			yield return new TestCaseData(GetTestCase(a, pass, dur), GetTestCase(b, pass, dur), same, not, "pass to pass");
			yield return new TestCaseData(GetTestCase(a, pass, dur), GetTestCase(b, incl, dur), broke, not, "pass to incl");
			yield return new TestCaseData(GetTestCase(a, pass, dur), GetTestCase(b, skip, dur), broke, not, "pass to skip");
			yield return new TestCaseData(GetTestCase(a, pass, dur), GetTestCase(b, fail, dur), broke, not, "pass to fail");

			// incl
			yield return new TestCaseData(GetTestCase(a, incl, dur), GetTestCase(b, pass, dur), fix, not, "incl to pass");
			yield return new TestCaseData(GetTestCase(a, incl, dur), GetTestCase(b, incl, dur), same, not, "incl to incl");
			yield return new TestCaseData(GetTestCase(a, incl, dur), GetTestCase(b, skip, dur), diff, BrokeDifferentlyEnum.InclToSkip, "incl to skip");
			yield return new TestCaseData(GetTestCase(a, incl, dur), GetTestCase(b, fail, dur), diff, BrokeDifferentlyEnum.InclToFail, "incl to fail");

			// skip
			yield return new TestCaseData(GetTestCase(a, skip, dur), GetTestCase(b, pass, dur), fix, not, "skip to pass");
			yield return new TestCaseData(GetTestCase(a, skip, dur), GetTestCase(b, incl, dur), diff, BrokeDifferentlyEnum.SkipToIncl, "skip to incl");
			yield return new TestCaseData(GetTestCase(a, skip, dur, "message"), GetTestCase(b, skip, dur, "message"),
				same, not, "skip to skip");
			yield return new TestCaseData(GetTestCase(a, skip, dur, "message"), GetTestCase(b, skip, dur, ""), diff,
				BrokeDifferentlyEnum.SkipDiff, "skip to skip msg1");
			yield return new TestCaseData(GetTestCase(a, skip, dur, ""), GetTestCase(b, skip, dur, "message"), diff,
				BrokeDifferentlyEnum.SkipDiff, "skip to skip msg2");
			yield return new TestCaseData(GetTestCase(a, skip, dur, "before message"),
				GetTestCase(b, skip, dur, "after message"), diff, BrokeDifferentlyEnum.SkipDiff, "skip to skip msg3");
			yield return new TestCaseData(GetTestCase(a, skip, dur), GetTestCase(b, fail, dur), diff, BrokeDifferentlyEnum.SkipToFail, "skip to fail");

			// fail
			yield return new TestCaseData(GetTestCase(a, fail, dur), GetTestCase(b, pass, dur), fix, not, "fail to pass");
			yield return new TestCaseData(GetTestCase(a, fail, dur), GetTestCase(b, incl, dur), diff, BrokeDifferentlyEnum.FailToIncl, "fail to incl");
			yield return new TestCaseData(GetTestCase(a, fail, dur), GetTestCase(b, skip, dur), diff, BrokeDifferentlyEnum.FailToSkip, "fail to skip");
			yield return new TestCaseData(GetTestCase(a, fail, dur, "message"), GetTestCase(b, fail, dur, "message"),
				same, not, "fail to fail good1");
			yield return new TestCaseData(GetTestCase(a, fail, dur, "message"), GetTestCase(b, fail, dur, ""), diff,
				BrokeDifferentlyEnum.FailMessage, "fail to fail msg1");
			yield return new TestCaseData(GetTestCase(a, fail, dur, ""), GetTestCase(b, fail, dur, "message"), diff,
				BrokeDifferentlyEnum.FailMessage, "fail to fail msg2");
			yield return new TestCaseData(GetTestCase(a, fail, dur, "before message"),
				GetTestCase(b, fail, dur, "after message"), diff, BrokeDifferentlyEnum.FailMessage, "fail to fail msg3");
			yield return new TestCaseData(GetTestCase(a, fail, dur, b, new List<string> {"a", "b", "c", "d"}),
				GetTestCase(b, fail, dur, b, new List<string> {"a", "b", "c", "d"}), same, not, "fail to fail good2");
			yield return new TestCaseData(GetTestCase(a, fail, dur, b, new List<string> {"a", "b", "c", "d"}),
				GetTestCase(b, fail, dur, b, new List<string> {"a", "b"}), diff, BrokeDifferentlyEnum.FailCallStack, "fail to fail cs1");
			yield return new TestCaseData(GetTestCase(a, fail, dur, b, new List<string> {"a", "b"}),
				GetTestCase(b, fail, dur, b, new List<string> {"a", "b", "c", "d"}), diff, BrokeDifferentlyEnum.FailCallStack, "fail to fail cs2");
			yield return new TestCaseData(GetTestCase(a, fail, dur, b, new List<string> {"a", "b", "c", "d"}),
				GetTestCase(b, fail, dur, b, new List<string> {"d", "c", "b", "a"}), diff, BrokeDifferentlyEnum.FailCallStack, "fail to fail cs3");
			yield return new TestCaseData(GetTestCase(a, fail, dur, b, new List<string> {"a", "b", "c", "d"}),
				GetTestCase(b, fail, dur, b, new List<string> {"d", "c", "b", "a"}), diff, BrokeDifferentlyEnum.FailCallStack, "fail to fail cs4");
		}

		#endregion Helpers
	}
}
