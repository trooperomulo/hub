﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

using NUnit.Framework;

using Hub;
using Hub.Commands.Helpers.UnitTest;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest
{
	/// <summary>
	/// tests <see cref="TestCase" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestCaseTests
	{
		#region Tests
		
		/// <summary>
		/// Tests that all properties are set properly
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestData))]
		public void TestProperties(string name, TestCaseResult result, long ticks, string message, List<string> callStack)
		{
			var tCase = new TestCase(name, result, TimeSpan.FromTicks(ticks), message, callStack);
			Assert.AreEqual(name, tCase.Name);
			Assert.AreEqual(result, tCase.Result);
			Assert.AreEqual(ticks, tCase.Duration.Ticks);
			Assert.AreEqual(message, tCase.Message);
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(callStack ?? new List<string>(), tCase.CallStack));
		}

		/// <summary>
		/// verifies that the GetResultLines and <see cref="TestCase.ToString" /> behave properly for <see cref="TestCaseResult" />.<see cref="TestCaseResult.Pass" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetPassTestData))]
		public void TestPass(string name, string message, List<string> callStack, List<string> lines)
		{
			var tCase = new TestCase(name, TestCaseResult.Pass, TimeSpan.Zero, message, callStack);
			Assert.AreEqual(tCase.Name, tCase.ToString(), $"{nameof(ToString)} failed for {nameof(TestCaseResult.Pass)}");
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(lines, tCase.GetPassedLines()));

			Assert.IsTrue(UnitTestUtil.ListsAreEqual( new List<string>(), tCase.GetFailureLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual( new List<string>(), tCase.GetSkippedLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual( new List<string>(), tCase.GetInconclusiveLines()));
		}

		/// <summary>
		/// verifies that the GetResultLines and <see cref="TestCase.ToString" /> behave properly for <see cref="TestCaseResult" />.<see cref="TestCaseResult.Fail" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetFailTestData))]
		public void TestFail(string name, string message, List<string> callStack, List<string> lines)
		{
			var tCase = new TestCase(name, TestCaseResult.Fail, TimeSpan.Zero, message, callStack);
			Assert.AreEqual(GetStringFromLines(tCase.GetFailureLines()), tCase.ToString(),
				$"{nameof(ToString)} failed for {nameof(TestCaseResult.Fail)}");
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(lines, tCase.GetFailureLines()));

			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetPassedLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetSkippedLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetInconclusiveLines()));
		}

		/// <summary>
		/// verifies that the GetResultLines and <see cref="TestCase.ToString" /> behave properly for <see cref="TestCaseResult" />.<see cref="TestCaseResult.Skip" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetSkipTestData))]
		public void TestSkip(string name, string message, List<string> callStack, List<string> lines)
		{
			var tCase = new TestCase(name, TestCaseResult.Skip, TimeSpan.Zero, message, callStack);
			Assert.AreEqual(GetStringFromLines(tCase.GetSkippedLines()), tCase.ToString(),
				$"{nameof(ToString)} failed for {nameof(TestCaseResult.Skip)}");
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(lines, tCase.GetSkippedLines()));

			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetPassedLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetFailureLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetInconclusiveLines()));
		}

		/// <summary>
		/// verifies that the GetResultLines and <see cref="TestCase.ToString" /> behave properly for <see cref="TestCaseResult" />.<see cref="TestCaseResult.Incl" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetInclTestData))]
		public void TestIncl(string name, string message, List<string> callStack, List<string> lines)
		{
			var tCase = new TestCase(name, TestCaseResult.Incl, TimeSpan.Zero, message, callStack);
			Assert.AreEqual(GetStringFromLines(tCase.GetInconclusiveLines()), tCase.ToString(),
				$"{nameof(ToString)} failed for {nameof(TestCaseResult.Incl)}");
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(lines, tCase.GetInconclusiveLines()));

			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetPassedLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetFailureLines()));
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(new List<string>(), tCase.GetSkippedLines()));
		}

		/// <summary>
		/// tests <see cref="TestCase.ToString" /> when an invalid result is given
		/// </summary>
		[Test]
		public void TestInvalidResult()
		{
			var tCase = new TestCase("name", (TestCaseResult) 88, TimeSpan.Zero, null, null);
			Assert.Throws<ArgumentOutOfRangeException>(() => tCase.ToString());
		}

		/// <summary>
		/// tests <see cref="TestCase.ToXmlString" /> and <see cref="TestCase.FromXmlString" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestData))]
		public void TestXml(string name, TestCaseResult result, long ticks, string message, List<string> stack)
		{
			// ToXmlString
			var test = new TestCase(name, result, TimeSpan.FromTicks(ticks), message, stack);
			var actXml = test.ToXmlString();
			var expXml = $"{Tag(name, (int) result, ticks)}{Msg(message)}{Stack(stack)}</Case>";
			Assert.AreEqual(expXml, actXml);

			// FromXmlString
			var other = TestCase.FromXmlString(actXml);
			Assert.AreEqual(test.Name, other.Name);
			Assert.AreEqual(test.Result, other.Result);
			Assert.AreEqual(test.Duration, other.Duration);
			Assert.AreEqual(test.Message, other.Message);
			UnitTestUtil.TestListsAreEqual(test.CallStack, other.CallStack);
		}

		/// <summary>
		/// tests that <see cref="TestCase.FromXmlString" /> returns null if the xml isn't right
		/// </summary>
		[Test]
		public void TestInvalidXml() => Assert.IsNull(TestCase.FromXmlString(@"<Bad></Bad>"));

		#endregion Tests

		#region Helpers

		private static string GetStringFromLines(List<string> lines)
		{
			var sb = new StringBuilder();
			foreach (var line in lines)
			{
				sb.AppendLine(line);
			}

			return sb.ToString();
		}

		private static IEnumerable GetPassTestData()
		{
			yield return new TestCaseData("Fred", null, null, new List<string> {"Fred"});
			yield return new TestCaseData("Joe", "message", new List<string> {"call", "stack"}, new List<string> {"Joe"});
		}

		private static IEnumerable GetFailTestData()
		{
			yield return new TestCaseData("Hank", "It failed", new List<string> {"call", "stack"}, new List<string> {"Hank", "It failed", "", "call", "stack"});
			yield return new TestCaseData("Mike", null, null, new List<string> {"Mike", "No message was given", "", "No call stack was given"});
		}

		private static IEnumerable GetSkipTestData()
		{
			yield return new TestCaseData("George", "It skipped", new List<string> {"call", "stack"}, new List<string> {"George", "It skipped"});
			yield return new TestCaseData("Herman", null, null, new List<string> {"Herman", "No message was given"});
		}

		private static IEnumerable GetInclTestData()
		{
			yield return new TestCaseData("Alfonzo", "Inconclusive", new List<string> {"call", "stack"}, new List<string> {"Alfonzo", "Inconclusive"});
			yield return new TestCaseData("Roberto", null, null, new List<string> {"Roberto", "No message was given"});
		}

		private static IEnumerable GetTestData()
		{
			yield return new TestCaseData("Fred", TestCaseResult.Pass, 1234, null, null);
			yield return new TestCaseData("Mike", TestCaseResult.Fail, 5678, null, null);
			yield return new TestCaseData("Herman", TestCaseResult.Skip, 9012, null, null);
			yield return new TestCaseData("Roberto", TestCaseResult.Incl, 3456, null, null);
			yield return new TestCaseData("Joe", TestCaseResult.Pass, 7890, "message", new List<string> {"call", "stack"});
			yield return new TestCaseData("Hank", TestCaseResult.Fail, 20357, "It failed", new List<string> {"call", "stack"});
			yield return new TestCaseData("George", TestCaseResult.Skip, 0, "It skipped", new List<string> {"call", "stack"});
			yield return new TestCaseData("Alfonzo", TestCaseResult.Incl, 303945749, "Inconclusive", new List<string> {"call", "stack"});
			yield return new TestCaseData("Saul", TestCaseResult.Pass, 56468, null, new List<string> {"line1", "line2"});
			yield return new TestCaseData("Timbo", TestCaseResult.Fail, 203948, "whatever", new List<string>());
		}

		private static string Line(string line)=> $"<line>{line}</line>";

		private static string Msg(string msg) => msg.IsNullOrEmpty() ? "" : $"<msg>{msg}</msg>";

		private static string Stack(List<string> stack)
		{
			var result = "";

			if (stack == null)
			{
				return result;
			}

			foreach (var line in stack)
			{
				result += Line(line);
			}

			return result;
		}

		private static string Tag(string name, int result, long ticks) =>
			$"<Case name=\"{name}\" res=\"{result}\" ticks=\"{ticks}\">";

		#endregion Helpers
	}
}
