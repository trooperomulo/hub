﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest.Parser
{
	/// <summary>
	/// tests <see cref="UnitTestResultTestCaseParser" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UnitTestResultTestCaseParserTests
	{
		/// <summary>
		/// Test a pass xml with no sub nodes
		/// </summary>
		[Test]
		public void TestPassNoSubNodes()
		{
			const string xml = @"<test-case id=""1027"" name=""MethodWithParameters(2,2)"" fullname=""NUnit.Tests.FixtureWithTestCases.MethodWithParameters(2,2)"" result=""Passed"" time=""0.006"" asserts=""1"" />";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "MethodWithParameters(2,2)", TestCaseResult.Pass);
		}

		/// <summary>
		/// Test a pass xml with sub nodes
		/// </summary>
		[Test]
		public void TestPassSubNodes()
		{
			const string xml = @"<test-case id=""1006"" name=""TestWithManyProperties"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.TestWithManyProperties"" result=""Passed"" time=""0.000"" asserts=""0"">
		<properties>
		  <property name=""TargetMethod"" value=""SomeClassName"" />
		  <property name=""Size"" value=""5"" />
		</properties>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "TestWithManyProperties", TestCaseResult.Pass);
		}

		/// <summary>
		/// Test a fail xml
		/// </summary>
		[Test]
		public void TestFail()
		{
			const string xml = @"  <test-case id=""1005"" name=""FailingTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.FailingTest"" result=""Failed"" time=""0.023"" asserts=""0"">
		<failure>
		  <message><![CDATA[Intentional failure]]></message>
		  <stack-trace><![CDATA[   at NUnit.Framework.Assert.Fail(String message, Object[] args) in D:\Dev\NUnit\nunit-3.0\work\NUnitFramework\src\framework\Assert.cs:line 142
   at NUnit.Framework.Assert.Fail(String message) in D:\Dev\NUnit\nunit-3.0\work\NUnitFramework\src\framework\Assert.cs:line 152
   at NUnit.Tests.Assemblies.MockTestFixture.FailingTest() in D:\Dev\NUnit\nunit-3.0\work\NUnitFramework\src\mock-assembly\MockAssembly.cs:line 121]]></stack-trace>
		</failure>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "FailingTest", TestCaseResult.Fail, "Intentional failure",
				new List<string>
				{
					"at NUnit.Framework.Assert.Fail(String message, Object[] args) in D:\\Dev\\NUnit\\nunit-3.0\\work\\NUnitFramework\\src\\framework\\Assert.cs:line 142",
					"at NUnit.Framework.Assert.Fail(String message) in D:\\Dev\\NUnit\\nunit-3.0\\work\\NUnitFramework\\src\\framework\\Assert.cs:line 152",
					"at NUnit.Tests.Assemblies.MockTestFixture.FailingTest() in D:\\Dev\\NUnit\\nunit-3.0\\work\\NUnitFramework\\src\\mock-assembly\\MockAssembly.cs:line 121"
				});
		}

		/// <summary>
		/// Test a fail xml missing the call stack
		/// </summary>
		[Test]
		public void TestFailNoStack()
		{
			const string xml = @"  <test-case id=""1005"" name=""FailingTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.FailingTest"" result=""Failed"" time=""0.023"" asserts=""0"">
		<failure>
		  <message><![CDATA[Intentional failure]]></message>
		</failure>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "FailingTest", TestCaseResult.Fail);
		}

		/// <summary>
		/// Test a fail xml missing the message
		/// </summary>
		[Test]
		public void TestFailNoMessage()
		{
			const string xml = @"  <test-case id=""1005"" name="""" fullname=""NUnit.Tests.Assemblies.MockTestFixture.FailingTest"" result=""Failed"" time=""0.023"" asserts=""0"">
		<failure>
		</failure>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "", TestCaseResult.Fail);
		}

		/// <summary>
		/// Test a fail xml missing the failure node
		/// </summary>
		[Test]
		public void TestFailNoFailure()
		{
			const string xml = @"  <test-case id=""1005"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.FailingTest"" result=""Failed"" time=""0.023"" asserts=""0"" />";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, null, TestCaseResult.Fail);
		}

		/// <summary>
		/// Test a skip xml
		/// </summary>
		[Test]
		public void TestSkip()
		{
			const string xml = @"<test-case id=""1009"" name=""NotRunnableTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.NotRunnableTest"" result=""Skipped"" label=""Invalid"" time=""0.000"" asserts=""0"">
		<properties>
		  <property name=""_SKIPREASON"" value=""No arguments were provided"" />
		</properties>
		<reason>
		  <message><![CDATA[No arguments were provided]]></message>
		</reason>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "NotRunnableTest", TestCaseResult.Skip, "No arguments were provided");
		}

		/// <summary>
		/// Test a skip xml missing the message
		/// </summary>
		[Test]
		public void TestSkipNoMessage()
		{
			const string xml = @"<test-case id=""1009"" name=""NotRunnableTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.NotRunnableTest"" result=""Skipped"" label=""Invalid"" time=""0.000"" asserts=""0"">
		<properties>
		  <property name=""_SKIPREASON"" value=""No arguments were provided"" />
		</properties>
		<reason>
		  <hank><![CDATA[No arguments were provided]]></hank>
		</reason>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "NotRunnableTest", TestCaseResult.Skip);
		}
		
		/// <summary>
		/// Test a skip xml missing the reason node
		/// </summary>
		[Test]
		public void TestSkipNoReason()
		{
			const string xml = @"<test-case id=""1009"" name=""NotRunnableTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.NotRunnableTest"" result=""Skipped"" label=""Invalid"" time=""0.000"" asserts=""0"">
		<properties>
		  <property name=""_SKIPREASON"" value=""No arguments were provided"" />
		</properties>
	  </test-case>";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "NotRunnableTest", TestCaseResult.Skip);
		}

		/// <summary>
		/// Test an incl xml
		/// </summary>
		[Test]
		public void TestIncl()
		{
			const string xml =
				@"<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Inconclusive"" time=""0.001"" asserts=""0"" />";
			var parser = new UnitTestResultTestCaseParser(XElement.Parse(xml), true);
			TestCaseMatches(parser.TestCase, "InconclusiveTest", TestCaseResult.Incl);
		}

		/// <summary>
		/// Test an xml with a bad value for the result
		/// </summary>
		[Test]
		public void TestBadResult()
		{
			const string xml =
				@"<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Bad"" time=""0.001"" asserts=""0"" />";
			Assert.Throws<ArgumentOutOfRangeException>(() => new UnitTestResultTestCaseParser(XElement.Parse(xml), true));
		}

		/// <summary>
		/// Test an xml without a result
		/// </summary>
		[Test]
		public void TestNoResult()
		{
			const string xml =
				@"<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" time=""0.001"" asserts=""0"" />";
			Assert.Throws<ArgumentOutOfRangeException>(() => new UnitTestResultTestCaseParser(XElement.Parse(xml), true));
		}

		private void TestCaseMatches(TestCase tCase, string name, TestCaseResult result, string message = null, List<string> stack = null)
		{
			Assert.AreEqual(name, tCase.Name, "name was wrong");
			Assert.AreEqual(result, tCase.Result, "result was wrong");
			Assert.AreEqual(message, tCase.Message, "message was wrong");
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(stack ?? new List<string>(), tCase.CallStack), "call stack was wrong");
		}
	}
}
