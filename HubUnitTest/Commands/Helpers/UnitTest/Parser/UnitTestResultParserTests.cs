﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

// ReSharper disable InconsistentNaming
namespace HubUnitTest.Commands.Helpers.UnitTest.Parser
{
	/// <summary>
	/// Tests <see cref="UnitTestResultParser" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UnitTestResultParserTests
	{
		#region Constants

		private const string Xml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>";

		private const string Open =
			"<test-run id=\"2\" testcasecount=\"36\" result=\"Passed\" total=\"36\" passed=\"36\" failed=\"0\" inconclusive=\"0\" skipped=\"0\" asserts=\"145\" engine-version=\"3.7.0.0\" clr-version=\"4.0.30319.42000\" start-time=\"2018-05-03 00:54:34Z\" end-time=\"2018-05-03 00:54:47Z\" duration=\"12.947749\">";

		private const string CmdLn =
			"<command-line><![CDATA[nunit3-console.exe  --skipnontestassemblies datamigrationtool.exe xm8.mp.proj.dll]]></command-line>";

		private const string Close = "</test-run>";

		private const string Passed =
			"<test-suite type=\"Assembly\" name=\"Henrietta.exe\" total=\"16\" passed=\"16\" />";

		private const string Failed =
			"<test-suite type=\"Assembly\" name=\"Bertha.exe\" total=\"17\" failed=\"17\" />";

		private const string Inconcl =
			"<test-suite name=\"Regina.exe\" total=\"18\" inconclusive=\"18\" />";

		private const string Skipped =
			"<test-suite type=\"Assembly\" name=\"Lou.exe\" total=\"19\" skipped=\"19\" />";

		internal static readonly string Pass = BuildTestXmlString(Passed);
		internal static readonly string Fail = BuildTestXmlString(Failed);
		internal static readonly string Skip = BuildTestXmlString(Skipped);
		internal static readonly string Incl = BuildTestXmlString(Inconcl);

		private static readonly string One = Pass;
		private static readonly string Two = BuildTestXmlString(Passed + Skipped);
		private static readonly string Thr = BuildTestXmlString(Passed + Skipped + Failed);
		private static readonly string OneNest = BuildTestXmlString(Open + CmdLn + Passed + Close);

		private static readonly string TwoNest =
			BuildTestXmlString(Open + CmdLn + Open + CmdLn + Passed + Close + Close);

		private static readonly string None = Incl;

		#endregion Constants

		#region Tests

		/// <summary>
		/// Tests that all properties are set properly after constructing
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetConstructorTestData))]
		public void TestConstructor(string xml, List<string> sols, int sFail, int sNoTe, int sSkip, int sPass, int sTot,
			int tFail, int tSkip, int tPass, int tTot)
		{
			var parser = new UnitTestResultParser(XElement.Parse(xml), true);
			TestParser(parser, sols, sFail, sNoTe, sSkip, sPass, sTot, tFail, tSkip, tPass, tTot);
		}

		/// <summary>
		/// Tests that all properties are set properly after adding
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetAddTestData))]
		public void TestAdd(string xml1, string xml2, List<string> sols, int sFail, int sNoTe, int sSkip, int sPass,
			int sTot, int tFail, int tSkip, int tPass, int tTot)
		{
			var parser1 = new UnitTestResultParser(XElement.Parse(xml1), true);
			var parser2 = new UnitTestResultParser(XElement.Parse(xml2), true);
			parser1.AddResults(parser2);
			TestParser(parser1, sols, sFail, sNoTe, sSkip, sPass, sTot, tFail, tSkip, tPass, tTot);
		}

		/// <summary>
		/// tests <see cref="UnitTestResultAssemblyParser.ToXmlString" /> and <see cref="UnitTestResultAssemblyParser.FromXmlString" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestData))]
		public void TestXml(List<UnitTestResultAssemblyParser> results)
		{
			// ToXmlString
			var parser = new UnitTestResultParser(results);
			var actXml = parser.ToXmlString();
			Assert.AreEqual(Tag(results), actXml);

			// FromXmlString
			var other = UnitTestResultParser.FromXmlString(actXml);
			UnitTestUtil.TestListsAreEqual(parser.Results, other.Results, ResultsMatch);
		}

		/// <summary>
		/// tests that <see cref="UnitTestResultParser.FromXmlString" /> returns null if the xml isn't right
		/// </summary>
		[Test]
		public void TestInvalidXml() => Assert.IsNull(UnitTestResultParser.FromXmlString(@"<Bad></Bad>"));

		#endregion Tests

		#region Helpers

		internal static string BuildTestXmlString(string middle) => $"{Xml}{Open}{CmdLn}{middle}{Close}";

		private static IEnumerable GetAddTestData()
		{
			// sFail, sPass, sTot, tFail, tSkip, tPass, tTot
			yield return new TestCaseData(BuildTestXmlString(Passed), BuildTestXmlString(Failed),
				new List<string> {"Henrietta.exe", "Bertha.exe"}, 1, 0, 0, 1, 2, 17, 0, 16, 33);
			yield return new TestCaseData(BuildTestXmlString(Passed), BuildTestXmlString(Inconcl),
				new List<string> {"Henrietta.exe"}, 0, 0, 0, 1, 1, 0, 0, 16, 16);
			yield return new TestCaseData(BuildTestXmlString(Passed), BuildTestXmlString(Skipped),
				new List<string> {"Henrietta.exe", "Lou.exe"}, 0, 0, 1, 1, 2, 0, 19, 16, 35);
			yield return new TestCaseData(BuildTestXmlString(Failed), BuildTestXmlString(Inconcl),
				new List<string> {"Bertha.exe"}, 1, 0, 0, 0, 1, 17, 0, 0, 17);
			yield return new TestCaseData(BuildTestXmlString(Failed), BuildTestXmlString(Skipped),
				new List<string> {"Bertha.exe", "Lou.exe"}, 1, 0, 1, 0, 2, 17, 19, 0, 36);
			yield return new TestCaseData(BuildTestXmlString(Inconcl), BuildTestXmlString(Skipped),
				new List<string> {"Lou.exe"}, 0, 0, 1, 0, 1, 0, 19, 0, 19);
		}

		private static IEnumerable GetConstructorTestData()
		{
			yield return new TestCaseData(One, new List<string> {"Henrietta.exe"}, 0, 0, 0, 1, 1, 0, 0, 16, 16);
			yield return new TestCaseData(Two, new List<string> {"Henrietta.exe", "Lou.exe"}, 0, 0, 1, 1, 2, 0, 19, 16,
				35);
			yield return new TestCaseData(Thr, new List<string> {"Henrietta.exe", "Lou.exe", "Bertha.exe"}, 1, 0, 1, 1,
				3, 17, 19, 16, 52);
			yield return new TestCaseData(OneNest, new List<string> {"Henrietta.exe"}, 0, 0, 0, 1, 1, 0, 0, 16, 16);
			yield return new TestCaseData(TwoNest, new List<string> {"Henrietta.exe"}, 0, 0, 0, 1, 1, 0, 0, 16, 16);
			yield return new TestCaseData(None, new List<string>(), 0, 0, 0, 0, 0, 0, 0, 0, 0);
		}

		private static IEnumerable GetTestData()
		{
			yield return new TestCaseData(new List<UnitTestResultAssemblyParser>
			{
				new UnitTestResultAssemblyParser("Fred", 1, 2, 3, 6, false,
					new List<TestCase>
					{
						new TestCase("a", TestCaseResult.Pass, TimeSpan.FromSeconds(.948)),
						new TestCase("b", TestCaseResult.Fail, TimeSpan.FromSeconds(.04385))
					}),
				new UnitTestResultAssemblyParser("Hank", 4, 6, 2, 12, false,
					new List<TestCase>
					{
						new TestCase("d", TestCaseResult.Pass, TimeSpan.FromSeconds(1.4863)),
						new TestCase("e", TestCaseResult.Pass, TimeSpan.FromSeconds(1.4863)),
						new TestCase("f", TestCaseResult.Pass, TimeSpan.FromSeconds(2.4956))
					}),
				new UnitTestResultAssemblyParser("Fritz", 7, 2, 0, 9, false,
					new List<TestCase>
					{
						new TestCase("r", TestCaseResult.Skip, TimeSpan.FromSeconds(.04863)),
						new TestCase("s", TestCaseResult.Skip, TimeSpan.FromSeconds(.04863)),
						new TestCase("t", TestCaseResult.Skip, TimeSpan.FromSeconds(.04956))
					})
			});
			yield return new TestCaseData(new List<UnitTestResultAssemblyParser>
			{
				new UnitTestResultAssemblyParser("Bill", 0, 0, 1, 1, false,
					new List<TestCase> {new TestCase("c", TestCaseResult.Skip, TimeSpan.FromSeconds(1.234))})
			});
		}

		private bool ResultsMatch(UnitTestResultAssemblyParser a, UnitTestResultAssemblyParser b)
		{
			bool CasesMatch(TestCase c, TestCase d)
			{
				return c.Name == d.Name && c.Result == d.Result && c.Duration.Equals(d.Duration) &&
				       Equals(c.Message, d.Message) && UnitTestUtil.ListsAreEqual(c.CallStack, d.CallStack);
			}

			return a.Solution == b.Solution && a.NumFailed == b.NumFailed && a.NumPassed == b.NumPassed &&
			       a.NumSkipped == b.NumSkipped && a.TotalTests == b.TotalTests && a.Invalid == b.Invalid &&
			       UnitTestUtil.ListsAreEqual(a.TestCases, b.TestCases, CasesMatch);
		}

		private static string Tag(List<UnitTestResultAssemblyParser> results)
		{
			string Taga(string sol, int fail, int pass, int skip, int tot, bool inv, List<TestCase> cases)
			{
				string Tagb(string name, TestCaseResult result, TimeSpan ticks) =>
					$"<Case name=\"{name}\" res=\"{(int) result}\" ticks=\"{ticks.Ticks}\"></Case>";

				var open =
					$"<Result sol=\"{sol}\" nF=\"{fail}\" nP=\"{pass}\" nS=\"{skip}\" tot=\"{tot}\" inv=\"{(inv ? 1 : 0)}\">";
				var middle = "";
				foreach (var tCase in cases)
				{
					middle += Tagb(tCase.Name, tCase.Result, tCase.Duration);
				}

				return $"{open}{middle}</Result>";
			}

			var mid = "";
			foreach (var result in results)
			{
				mid += Taga(result.Solution, result.NumFailed, result.NumPassed, result.NumSkipped, result.TotalTests,
					result.Invalid, result.TestCases);
			}

			return $"<Results>{mid}</Results>";
		}

		private void TestParser(UnitTestResultParser parser, List<string> sols, int sFail, int sNoTe, int sSkip,
			int sPass, int sTot, int tFail, int tSkip, int tPass, int tTot)
		{
			Assert.IsTrue(UnitTestUtil.ListsAreEqual(sols, parser.Solutions), nameof(parser.Solutions));
			Assert.AreEqual(sFail, parser.SolutionsFailed, nameof(parser.SolutionsFailed));
			Assert.AreEqual(sNoTe, parser.SolutionsNoTests, nameof(parser.SolutionsNoTests));
			Assert.AreEqual(sSkip, parser.SolutionsSkips, nameof(parser.SolutionsSkips));
			Assert.AreEqual(sPass, parser.SolutionsPassed, nameof(parser.SolutionsPassed));
			Assert.AreEqual(sTot, parser.SolutionsTested, nameof(parser.SolutionsTested));
			Assert.AreEqual(tFail, parser.TestsFailed, nameof(parser.TestsFailed));
			Assert.AreEqual(tSkip, parser.TestsSkipped, nameof(parser.TestsSkipped));
			Assert.AreEqual(tPass, parser.TestsPassed, nameof(parser.TestsPassed));
			Assert.AreEqual(tTot, parser.TestsTotal, nameof(parser.TestsTotal));
		}

		#endregion Helpers
	}
}
