﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;

using NUnit.Framework;

using Hub.Commands;
using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest.Parser
{
	/// <summary>
	/// tests <see cref="UnitTestResultAssemblyParser" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UnitTestResultAssemblyParserTests
	{
		#region Tests

		/// <summary>
		/// Tests that all properties are set properly
		/// </summary>
		[Test]
		public void TestFullAssembly()
		{
			var assembly = UnitTestUtil.GetTestPath("assembly.xml", nameof(UnitTestCommand));
			var parser = new UnitTestResultAssemblyParser(XElement.Load(assembly), true);
			Assert.AreEqual("Xm8.MP.EWD.dll", parser.Solution);
			Assert.AreEqual(parser.TestCases.Count, parser.TotalTests);
			Assert.AreEqual(396, parser.TotalTests);
			Assert.AreEqual(316, parser.NumPassed);
			Assert.AreEqual(0, parser.NumSkipped);
			Assert.AreEqual(80, parser.NumFailed);
			Assert.IsFalse(parser.Success);
		}

		/// <summary>
		/// test that we won't crash just because the xml is bad
		/// </summary>
		[Test]
		public void TestProblemXml()
		{
			const string xml =
				@"  <test-suite type=""Assembly"" id=""2-1"" fullname=""C:\git\1\xm8\bin\debug\xm8.mp.proj.dll"" testcasecount=""0"" runstate=""Runnable"" result=""Skipped"" label=""NoTests"">
	<properties>
	  <property name=""_SKIPREASON"" value=""Skipping non-test assembly"" />
	</properties>
	<reason>
	  <message>Skipping non-test assembly</message>
	</reason>
	<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Bad"" time=""0.001"" asserts=""0"" />
  </test-suite>";
			var parser = new UnitTestResultAssemblyParser(XElement.Parse(xml), true);
			Assert.AreEqual(null, parser.Solution);
			Assert.AreEqual(0, parser.TestCases.Count);
			Assert.AreEqual(0, parser.TotalTests);
		}

		/// <summary>
		/// tests <see cref="UnitTestResultAssemblyParser.CompareTo" />
		/// </summary>
		[Test]
		public void TestCompareTo()
		{
			const string xml1 =
				@"  <test-suite name=""George"" type=""Assembly"" id=""2-1"" fullname=""C:\git\1\xm8\bin\debug\xm8.mp.proj.dll"" testcasecount=""0"" runstate=""Runnable"" result=""Skipped"" label=""NoTests"">
				<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Bad"" time=""0.001"" asserts=""0"" />
				</test-suite>";
			const string xml2 =
				@"  <test-suite name=""Hank"" type=""Assembly"" id=""2-1"" fullname=""C:\git\1\xm8\bin\debug\xm8.mp.proj.dll"" testcasecount=""0"" runstate=""Runnable"" result=""Skipped"" label=""NoTests"">
				<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Bad"" time=""0.001"" asserts=""0"" />
				</test-suite>";
			const string xml3 =
				@"  <test-suite name=""Fred"" type=""Assembly"" id=""2-1"" fullname=""C:\git\1\xm8\bin\debug\xm8.mp.proj.dll"" testcasecount=""0"" runstate=""Runnable"" result=""Skipped"" label=""NoTests"">
				<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Bad"" time=""0.001"" asserts=""0"" />
				</test-suite>";
			const string xml4 =
				@"  <test-suite name=""George"" type=""Assembly"" id=""2-1"" fullname=""C:\git\1\xm8\bin\debug\xm8.mp.proj.dll"" testcasecount=""0"" runstate=""Runnable"" result=""Skipped"" label=""NoTests"">
				<test-case id=""1010"" name=""InconclusiveTest"" fullname=""NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest"" result=""Bad"" time=""0.001"" asserts=""0"" />
				</test-suite>";
			var parser1 = new UnitTestResultAssemblyParser(XElement.Parse(xml1), true);
			var parser2 = new UnitTestResultAssemblyParser(XElement.Parse(xml2), true);
			var parser3 = new UnitTestResultAssemblyParser(XElement.Parse(xml3), true);
			var parser4 = new UnitTestResultAssemblyParser(XElement.Parse(xml4), true);
			Assert.AreEqual(-1, parser1.CompareTo(parser2));
			Assert.AreEqual(1, parser1.CompareTo(parser3));
			Assert.AreEqual(0, parser1.CompareTo(parser4));
		}

		/// <summary>
		/// tests <see cref="UnitTestResultAssemblyParser.ToXmlString" /> and <see cref="UnitTestResultAssemblyParser.FromXmlString" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestData))]
		public void TestXml(string sol, int fail, int pass, int skip, int tot, bool inv, List<TestCase> cases)
		{
			// ToXmlString
			var parser = new UnitTestResultAssemblyParser(sol, fail, pass, skip, tot, inv, cases);
			var actXml = parser.ToXmlString();
			Assert.AreEqual(Tag(sol, fail, pass, skip, tot, inv, cases), actXml);

			// FromXmlString
			var other = UnitTestResultAssemblyParser.FromXmlString(actXml);
			Assert.AreEqual(parser.Solution, other.Solution);
			Assert.AreEqual(parser.NumFailed, other.NumFailed);
			Assert.AreEqual(parser.NumPassed, other.NumPassed);
			Assert.AreEqual(parser.NumSkipped, other.NumSkipped);
			Assert.AreEqual(parser.TotalTests, other.TotalTests);
			Assert.AreEqual(parser.Invalid, other.Invalid);
			UnitTestUtil.TestListsAreEqual(parser.TestCases, other.TestCases, CasesMatch);
		}

		/// <summary>
		/// tests that <see cref="UnitTestResultAssemblyParser.FromXmlString" /> returns null if the xml isn't right
		/// </summary>
		[Test]
		public void TestInvalidXml() => Assert.IsNull(UnitTestResultAssemblyParser.FromXmlString(@"<Bad></Bad>"));

		#endregion Tests

		#region Helpers

		private bool CasesMatch(TestCase a, TestCase b)
		{
			return a.Name == b.Name && a.Result == b.Result && a.Duration.Equals(b.Duration) &&
			       Equals(a.Message, b.Message) && UnitTestUtil.ListsAreEqual(a.CallStack, b.CallStack);
		}

		private static IEnumerable GetTestData()
		{
			yield return new TestCaseData("Fred", 1, 2, 3, 6, true,
				new List<TestCase>
				{
					new TestCase("a", TestCaseResult.Pass, TimeSpan.FromSeconds(.948)),
					new TestCase("b", TestCaseResult.Fail, TimeSpan.FromSeconds(.04385))
				});
			yield return new TestCaseData("Bill", 0, 0, 1, 16, false,
				new List<TestCase> {new TestCase("c", TestCaseResult.Skip, TimeSpan.FromSeconds(1.234))});
		}

		private static string Tag(string sol, int fail, int pass, int skip, int tot, bool inv, List<TestCase> cases)
		{
			string Tag(string name, TestCaseResult result, TimeSpan ticks) =>
				$"<Case name=\"{name}\" res=\"{(int)result}\" ticks=\"{ticks.Ticks}\"></Case>";

			var open = $"<Result sol=\"{sol}\" nF=\"{fail}\" nP=\"{pass}\" nS=\"{skip}\" tot=\"{tot}\" inv=\"{(inv ? 1 : 0)}\">";
			var middle = "";
			foreach (var tCase in cases)
			{
				middle += Tag(tCase.Name, tCase.Result, tCase.Duration);
			}

			return $"{open}{middle}</Result>";
		}

		#endregion Helpers
	}
}
