﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using NUnit.Framework;

using Hub;
using Hub.Commands.Helpers.UnitTest;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands.Helpers.UnitTest
{
	/// <summary>
	/// tests <see cref="UnitTestSettings"/>
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class UnitTestSettingsTests
	{
		/// <summary>
		/// verifies that all the properties return their default value if no user default or arguments are provided
		/// </summary>
		[Test]
		public void TestDefaultIsReturned()
		{
			UnitTestSettings.UserDefault = null;
			var settings = new UnitTestSettings();
			var message = "";
			message += TestProperty(settings.EstimateTime, false, nameof(settings.EstimateTime));
			message += TestProperty(settings.Progress, Progress.Solution, nameof(settings.Progress));
			message += TestProperty(settings.ShowTime, false, nameof(settings.ShowTime));
			message += TestProperty(settings.ResultsConsole, false, nameof(settings.ResultsConsole));
			message += TestProperty(settings.ResultsTextFile, true, nameof(settings.ResultsTextFile));
			message += TestProperty(settings.ResultsXmlFile, false, nameof(settings.ResultsXmlFile));
			message += TestProperty(settings.InputFile, -1, nameof(settings.InputFile));
			Assert.IsTrue(message.IsNullOrEmpty(), message);
		}

		/// <summary>
		/// verifies that all the properties return the user default
		/// </summary>
		[Test]
		public void TestUserDefaultsAreReturned()
		{
			UnitTestSettings.UserDefault = "-e -p t -tm -rc -rx -i 4";
			var settings = new UnitTestSettings();
			var message = "";
			message += TestProperty(settings.EstimateTime, true, nameof(settings.EstimateTime));
			message += TestProperty(settings.Progress, Progress.Time, nameof(settings.Progress));
			message += TestProperty(settings.ShowTime, true, nameof(settings.ShowTime));
			message += TestProperty(settings.ResultsConsole, true, nameof(settings.ResultsConsole));
			message += TestProperty(settings.ResultsTextFile, false, nameof(settings.ResultsTextFile));
			message += TestProperty(settings.ResultsXmlFile, true, nameof(settings.ResultsXmlFile));
			message += TestProperty(settings.InputFile, 4, nameof(settings.InputFile));
			Assert.IsTrue(message.IsNullOrEmpty(), message);
			UnitTestSettings.UserDefault = null;
		}

		/// <summary>
		/// verifies that arguments supercede defaults and user defaults
		/// </summary>
		[Test]
		public void TestArgumentsAreReturned()
		{
			UnitTestSettings.UserDefault = "-p s -i 6";
			var settings = new UnitTestSettings();
			settings.LoadArguments("-e -p n -tm -rc -rx -i 0".Split(' ').ToList());
			var message = "";
			message += TestProperty(settings.EstimateTime, true, nameof(settings.EstimateTime));
			message += TestProperty(settings.Progress, Progress.None, nameof(settings.Progress));
			message += TestProperty(settings.ShowTime, true, nameof(settings.ShowTime));
			message += TestProperty(settings.ResultsConsole, true, nameof(settings.ResultsConsole));
			message += TestProperty(settings.ResultsTextFile, false, nameof(settings.ResultsTextFile));
			message += TestProperty(settings.ResultsXmlFile, true, nameof(settings.ResultsXmlFile));
			message += TestProperty(settings.InputFile, 0, nameof(settings.InputFile));
			Assert.IsTrue(message.IsNullOrEmpty(), message);
			UnitTestSettings.UserDefault = null;
		}

		/// <summary>
		/// Make sure we handle a bad progress gracefully
		/// </summary>
		[Test]
		public void TestInvalidProgress()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			UnitTestSettings.UserDefault = "-p x";
			var settings = new UnitTestSettings();
			settings.LoadArguments("-p e".Split(' ').ToList());
			var message = "";
			message += TestProperty(settings.Progress, Progress.Solution, nameof(settings.Progress));
			Assert.IsTrue(message.IsNullOrEmpty(), message);
			UnitTestSettings.UserDefault = null;

			Assert.AreEqual("invalid value passed for progress from user params\r\ninvalid value passed for progress from the arguments\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// verifies that the user defaults are returned if only invalid arguments are passed in
		/// </summary>
		[Test]
		public void TestInvalidArgumentsArePassedId()
		{
			UnitTestSettings.UserDefault = "-e -p t -tm -rc -rx -i 4";
			var settings = new UnitTestSettings();
			settings.LoadArguments("xmpcc xuicc cbused mnet".Split(' ').ToList());
			var message = "";
			message += TestProperty(settings.EstimateTime, true, nameof(settings.EstimateTime));
			message += TestProperty(settings.Progress, Progress.Time, nameof(settings.Progress));
			message += TestProperty(settings.ShowTime, true, nameof(settings.ShowTime));
			message += TestProperty(settings.ResultsConsole, true, nameof(settings.ResultsConsole));
			message += TestProperty(settings.ResultsTextFile, false, nameof(settings.ResultsTextFile));
			message += TestProperty(settings.ResultsXmlFile, true, nameof(settings.ResultsXmlFile));
			message += TestProperty(settings.InputFile, 4, nameof(settings.InputFile));
			Assert.IsTrue(message.IsNullOrEmpty(), message);
			UnitTestSettings.UserDefault = null;
		}

		private static string TestProperty<T>(T actual, T expected, string name)
		{
			return Equals(expected, actual)
				? ""
				: $"{name} should have been {expected} but was {actual}{Environment.NewLine}";
		}
	}
}
