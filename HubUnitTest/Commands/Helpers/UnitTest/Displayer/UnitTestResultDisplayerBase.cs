﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;

using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;

using HubUnitTest.Commands.Helpers.UnitTest.Parser;

namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// base class for testing <see cref="UnitTestResultDisplayer" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class UnitTestResultDisplayerBase
	{
		#region Xmls

		#region No Tests

		///
		protected const string None1 = "<test-suite type=\"Assembly\" name=\"Jane.exe\" total=\"0\" passed=\"0\" />";

		///
		protected const string None2 = "<test-suite type=\"Assembly\" name=\"Rose.exe\" total=\"0\" passed=\"0\" />";

		#endregion No Tests

		#region Passed

		///
		protected const string Passed1 = "<test-suite type=\"Assembly\" name=\"Henrietta.exe\" total=\"2\" passed=\"2\" />";

		///
		protected const string Passed2 = "<test-suite type=\"Assembly\" name=\"Georgette.exe\" total=\"3\" passed=\"3\" />";

		#endregion Passed

		#region Failures

		///
		protected const string Failed1 =
			"<test-suite type=\"Assembly\" name=\"Bertha.exe\" total=\"1\" failed=\"1\"><test-case id=\"1005\" " +
			"name=\"Test1\" fullname=\"NUnit.Tests.Assemblies.MockTestFixture.FailingTest\" result=\"Failed\" " +
			"time=\"0.023\" asserts=\"0\">\r\n\t\t<failure>\r\n\t\t  <message><![CDATA[message 1]]></message>\r\n\t\t  " +
			"<stack-trace><![CDATA[stack line 1\r\nstack line 2\r\nstack line 3]]></stack-trace>\r\n\t\t</failure>\r\n\t  " +
			"</test-case></test-suite>";

		///
		protected const string Failed2 =
			"<test-suite type=\"Assembly\" name=\"Freda.exe\" total=\"1\" failed=\"1\"><test-case id=\"1005\" " +
			"name=\"Test2\" fullname=\"NUnit.Tests.Assemblies.MockTestFixture.FailingTest\" result=\"Failed\" " +
			"time=\"0.023\" asserts=\"0\">\r\n\t\t<failure>\r\n\t\t  <message><![CDATA[message 2]]></message>\r\n\t\t  " +
			"<stack-trace><![CDATA[stack line 4\r\nstack line 5\r\nstack line 6]]></stack-trace>\r\n\t\t</failure>\r\n\t  " +
			"</test-case></test-suite>";

		///
		protected const string Inconc = "<test-suite type=\"Assembly\" name=\"Regina.exe\" total=\"2\" inconclusive=\"2\" />";

		#endregion Failures

		#region Skips

		///
		protected const string Skipped1 =
			"<test-suite type=\"Assembly\" name=\"Loretta.exe\" total=\"1\" skipped=\"1\"><test-case id=\"1005\" " +
			"name=\"Test5\" fullname=\"NUnit.Tests.Assemblies.MockTestFixture.FailingTest\" result=\"Skipped\" " +
			"time=\"0.023\" asserts=\"0\">\r\n\t\t<reason>\r\n\t\t  <message><![CDATA[skipped message 1]]></message>\r\n\t\t  " +
			"</reason>\r\n\t  </test-case></test-suite>";

		///
		protected const string Skipped2 =
			"<test-suite type=\"Assembly\" name=\"Dorothy.exe\" total=\"1\" skipped=\"1\"><test-case id=\"1005\" " +
			"name=\"Test6\" fullname=\"NUnit.Tests.Assemblies.MockTestFixture.FailingTest\" result=\"Skipped\" " +
			"time=\"0.023\" asserts=\"0\">\r\n\t\t<reason>\r\n\t\t  <message><![CDATA[skipped message 2]]></message>\r\n\t\t  " +
			"</reason>\r\n\t  </test-case></test-suite>";

		#endregion Skips

		#region Mixtures

		///
		protected const string MixedFs =
			"<test-suite type=\"Assembly\" name=\"MixedFS.exe\" total=\"2\" skipped=\"1\" failed=\"1\">" +
			"<test-case id=\"1005\" name=\"TestFs1\" result=\"Skipped\" asserts=\"0\">\r\n\t\t<reason>\r\n\t\t  " +
			"<message><![CDATA[this test was also skipped]]></message>\r\n\t\t  </reason>\r\n\t  " +
			"</test-case>" +
			"<test-case id=\"1005\" name=\"TestFs2\" result=\"Failed\" asserts=\"0\">\r\n\t\t<failure>\r\n\t\t  " +
			"<message><![CDATA[fs message]]></message>\r\n\t\t  " +
			"<stack-trace><![CDATA[stack line Fs1\r\nstack line Fs2\r\nstack line Fs3]]></stack-trace>\r\n\t\t</failure>\r\n\t  " +
			"</test-case></test-suite>";

		///
		protected const string MixedPf =
			"<test-suite type=\"Assembly\" name=\"MixedPF.exe\" total=\"2\" passed=\"1\" failed=\"1\">" +
			"<test-case id=\"1033\" name=\"Test3\" result=\"Passed\" asserts=\"1\" />" +
			"<test-case id=\"1005\" name=\"Test4\" result=\"Failed\" asserts=\"0\">\r\n\t\t<failure>\r\n\t\t  " +
			"<message><![CDATA[message 3]]></message>\r\n\t\t  " +
			"<stack-trace><![CDATA[stack line 7\r\nstack line 8\r\nstack line 9]]></stack-trace>\r\n\t\t</failure>\r\n\t  " +
			"</test-case></test-suite>";

		///
		protected const string MixedPs =
			"<test-suite type=\"Assembly\" name=\"MixedPS.exe\" total=\"2\" passed=\"1\" skipped=\"1\">" +
			"<test-case id=\"1033\" name=\"TestPs1\" result=\"Passed\" asserts=\"1\" />" +
			"<test-case id=\"1005\" name=\"TestPs2\" result=\"Skipped\" asserts=\"0\">\r\n\t\t<reason>\r\n\t\t  " +
			"<message><![CDATA[this test was skipped]]></message>\r\n\t\t  </reason>\r\n\t  " +
			"</test-case></test-suite>";

		///
		protected const string Everything =
			"<test-suite type=\"Assembly\" name=\"Everything.exe\" total=\"3\" passed=\"1\" skipped=\"1\" failed=\"1\">" +
			"<test-case id=\"1005\" name=\"TestEv1\" result=\"Skipped\" asserts=\"0\">\r\n\t\t<reason>\r\n\t\t  " +
			"<message><![CDATA[this test was another to be skipped]]></message>\r\n\t\t  </reason>\r\n\t  " +
			"</test-case>" +
			"<test-case id=\"1005\" name=\"TestEv2\" result=\"Failed\" asserts=\"0\">\r\n\t\t<failure>\r\n\t\t  " +
			"<message><![CDATA[ev message]]></message>\r\n\t\t  " +
			"<stack-trace><![CDATA[stack line Ev1\r\nstack line Ev2\r\nstack line Ev3]]></stack-trace>\r\n\t\t</failure>\r\n\t  " +
			"</test-case><test-case id=\"1033\" name=\"TestEv3\" result=\"Passed\" asserts=\"1\" /></test-suite>";

		#endregion Mixtures

		#endregion Xmls

		#region Others

		///
		protected const string ShortLine = "--------";
		///
		protected const string MedLine = ShortLine + ShortLine;
		///
		protected const string LongLine = MedLine + ShortLine;

		#endregion Others

		#region Helpers

		///
		protected static void DoTest(string middle, List<string> expected, Func<UnitTestResultDisplayer, List<string>> getter)
		{
			var parser = new UnitTestResultParser(XElement.Parse(UnitTestResultParserTests.BuildTestXmlString(middle)), true);
			var actual = getter(new UnitTestResultDisplayer(parser));
			UnitTestUtil.TestListsAreEqual(expected, actual);
		}

		#endregion Helpers
	}
}
