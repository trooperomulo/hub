﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Enum;

using CDU = Hub.Commands.Helpers.UnitTest.Displayer.ComparerDisplayerUtils;

namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// tests <see cref="CDU" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ComparerDisplayerUtilsTests
	{
		/// <summary>
		/// Tests <see cref="CDU.Blank" />
		/// </summary>
		[Test]
		public void TestBlank()
		{
			Assert.AreEqual(
				"                                                                      |                                                                      ",
				CDU.Blank());
		}

		/// <summary>
		/// Tests <see cref="CDU.Center" />
		/// </summary>
		[Test]
		public void TestCenter()
		{
			Assert.AreEqual("                              teststring                              ",
				CDU.Center("teststring"));
		}

		/// <summary>
		/// Tests <see cref="CDU.Header" />
		/// </summary>
		[Test]
		public void TestHeader()
		{
			Assert.AreEqual(
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				CDU.Header(ComparisonResult.Same));
		}

		/// <summary>
		/// Tests <see cref="CDU.Line(string,ComparisonResult)" />
		/// </summary>
		[Test]
		[TestCase("                                                                      |text                                                                  ", ComparisonResult.Added)]
		[TestCase("text                                                                  |                                                                      ", ComparisonResult.Removed)]
		[TestCase("text                                                                  |text                                                                  ", ComparisonResult.Same)]
		[TestCase("text                                                                  |text                                                                  ", ComparisonResult.Fixed)]
		[TestCase("text                                                                  |text                                                                  ", ComparisonResult.Broken)]
		[TestCase("text                                                                  |text                                                                  ", ComparisonResult.BrokenDifferently)]
		public void TestLineA(string expected, ComparisonResult result) => Assert.AreEqual(expected, CDU.Line("text", result));

		/// <summary>
		/// Tests <see cref="CDU.Line(string,string)" />
		/// </summary>
		[Test]
		[TestCase("a                                                                     |whatever                                                              ", "a","whatever")]
		[TestCase("a                                                                     |                                                                      ", "a", "")]
		[TestCase("                                                                      |whatever                                                              ", "", "whatever")]
		[TestCase("                                                                      |                                                                      ", "", "")]
		public void TestLineB(string expected, string right, string left) => Assert.AreEqual(expected, CDU.Line(right, left));

		/// <summary>
		/// Test <see cref="CDU.Line(string,ComparisonResult)" /> with bad data
		/// </summary>
		[Test]
		public void TestLineBadData()
		{
			var ex = Assert.Throws<ArgumentException>(() =>
				CDU.Line("12345678901234567980123456798012345679801234567890123456789012345678901",
					ComparisonResult.Added));
			Assert.AreEqual("Passed in string is longer (71) than max width (70)", ex.Message);

			ex = Assert.Throws<ArgumentException>(() => CDU.Line("whatever", (ComparisonResult) 99));
			Assert.AreEqual("Value does not fall within the expected range.", ex.Message);
		}

		/// <summary>
		/// Tests <see cref="CDU.Result" />
		/// </summary>
		[Test]
		[TestCase(ComparisonResult.Same, "Same")]
		[TestCase(ComparisonResult.Added, "Added")]
		[TestCase(ComparisonResult.Removed, "Removed")]
		[TestCase(ComparisonResult.Fixed, "Fixed")]
		[TestCase(ComparisonResult.Broken, "Broken")]
		[TestCase(ComparisonResult.BrokenDifferently, "Broken Differently")]
		public void TestResult(ComparisonResult result, string expected) => Assert.AreEqual(expected, CDU.Result(result));

		/// <summary>
		/// Tests <see cref="CDU.Result" /> with bad data
		/// </summary>
		[Test]
		public void TestResultBadData() => Assert.Throws<ArgumentOutOfRangeException>(() => CDU.Result((ComparisonResult) 99));

		/// <summary>
		/// Tests <see cref="CDU.Test(string,TestCaseResult?)" />
		/// </summary>
		[Test]
		[TestCase("albert", TestCaseResult.Pass, "albert - Pass                                                         ")]
		[TestCase("beto", TestCaseResult.Fail, "beto - Fail                                                           ")]
		[TestCase("charles", TestCaseResult.Skip, "charles - Skip                                                        ")]
		[TestCase("dumbledore", TestCaseResult.Incl, "dumbledore - Incl                                                     ")]
		[TestCase("e e cummings", null, "                                                                      ")]
		public void TestTestA(string name, TestCaseResult? result, string expected) => Assert.AreEqual(expected, CDU.Test(name, result));

		/// <summary>
		/// Tests <see cref="CDU.Test(string,TestCaseResult?,TestCaseResult?)" /> with both set
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestTestWithBothTestData))]
		public void TestTestWithBoth(string test, List<string> expected) => UnitTestUtil.TestListsAreEqual(expected, CDU.Test(test, TestCaseResult.Pass, TestCaseResult.Pass));

		/// <summary>
		/// Tests <see cref="CDU.Test(string,TestCaseResult?,TestCaseResult?)" /> with just after set
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestTestWithOnlyAfterTestData))]
		public void TestTestWithOnlyAfter(string test, List<string> expected) => UnitTestUtil.TestListsAreEqual(expected, CDU.Test(test, null, TestCaseResult.Pass));

		/// <summary>
		/// Tests <see cref="CDU.Test(string,TestCaseResult?,TestCaseResult?)" /> with just before set
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestTestWithOnlyBeforeTestData))]
		public void TestTestWithOnlyBefore(string test, List<string> expected) => UnitTestUtil.TestListsAreEqual(expected, CDU.Test(test, TestCaseResult.Pass, null));

		/// <summary>
		/// Tests <see cref="CDU.Test(string,TestCaseResult?,TestCaseResult?)" /> with bad data
		/// </summary>
		[Test]
		public void TestTestBBadData() => Assert.Throws<ArgumentException>(() => CDU.Test("whatever", null, null));

		/// <summary>
		/// Tests <see cref="CDU.TestResults" />
		/// </summary>
		[Test]
		[TestCase(TestCaseResult.Pass, null, "test - Pass                                                           |                                                                      ")]
		[TestCase(null, TestCaseResult.Pass, "                                                                      |test - Pass                                                           ")]
		[TestCase(TestCaseResult.Pass, TestCaseResult.Pass, "test - Pass                                                           |test - Pass                                                           ")]
		public void TestTestResults(TestCaseResult? a, TestCaseResult? b, string expected) => Assert.AreEqual(expected, CDU.TestResults("test", a, b));

		/// <summary>
		/// Tests <see cref="CDU.TestResults" /> with bad data
		/// </summary>
		[Test]
		public void TestTestResultsBadData() => Assert.Throws<ArgumentException>(() => CDU.TestResults("test", null, null));

		/// <summary>
		/// Tests <see cref="CDU.Wrap" />
		/// </summary>
		[Test]
		public void TestWrap()
		{
			var input =
				"1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
				"1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
				"1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
				"1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";

			var expected = new List<string>
			{
				"1234567890123456789012345678901234567890123456789012345678901234567890",
				" -12345678901234567890123456789012345678901234567890123456789012345678",
				" -90123456789012345678901234567890123456789012345678901234567890123456",
				" -78901234567890123456789012345678901234567890123456789012345678901234",
				" -56789012345678901234567890123456789012345678901234567890123456789012",
				" -3456789012345678901234567890123456789012345678901234567890          "
			};
			var actual = CDU.Wrap(input);
			UnitTestUtil.TestListsAreEqual(expected, actual);

			expected = new List<string> {"12345", " -678", " -901", " -234", " -567", " -890"};
			actual = CDU.Wrap("12345678901234567890", 5);
			UnitTestUtil.TestListsAreEqual(expected, actual);

			expected = new List<string> {"12345678            "};
			actual = CDU.Wrap("12345678", 20);
			UnitTestUtil.TestListsAreEqual(expected, actual);
		}

		private static IEnumerable GetTestTestWithBothTestData()
		{
			// 1)
			yield return new TestCaseData("short",
				new List<string>
				{
					"short - Pass                                                          |short - Pass                                                          "
				});
			
			// 2)
			yield return new TestCaseData("this line is too long to be displayed on both sides with its results",
				new List<string>
				{
					"                             Pass - this line is too long to be displayed on both sides with its results - Pass                              "
				});

			// 3)
			yield return new TestCaseData(
				"this line is too long to be displayed on both sides with its results, or to be centered and have its results one the same line as it",
				new List<string>
				{
					"    this line is too long to be displayed on both sides with its results, or to be centered and have its results one the same line as it     ",
					"Pass                                                                  |Pass                                                                  "
				});

			// 4)
			yield return new TestCaseData(
				"this line is too long to be displayed on both sides with its results, or to be centered and have its results one the same line as it, in fact it is so long that it will need to be wrapped, in fact it is so super, duper, extra, obnoxiously long that it will need to be wrapped not just once, but twice",
				new List<string>
				{
					"this line is too long to be displayed on both sides with its results, or to be centered and have its results one the same line as it, in fact",
					" - it is so long that it will need to be wrapped, in fact it is so super, duper, extra, obnoxiously long that it will need to be wrapped not ",
					" -just once, but twice                                                                                                                       ",
					"Pass                                                                  |Pass                                                                  "
				});
		}

		private static IEnumerable GetTestTestWithOnlyAfterTestData()
		{
			// 1)
			yield return new TestCaseData("short",
				new List<string>
				{
					"                                                                      |short - Pass                                                          "
				});

			// 2)
			yield return new TestCaseData("this line is too long to be displayed with its result after it, dumb",
				new List<string>
				{
					"                                                                      |this line is too long to be displayed with its result after it, dumb  ",
					"                                                                      |Pass                                                                  "
				});

			// 3)
			yield return new TestCaseData(
				"this line is super, super, duper, super long and it will need to be wrapped because it is so super, super, duper, super long, in fact it will need to be wrapped twice",
				new List<string>
				{
					"                                                                      |this line is super, super, duper, super long and it will need to be wr",
					"                                                                      | -apped because it is so super, super, duper, super long, in fact it w",
					"                                                                      | -ill need to be wrapped twice                                        ",
					"                                                                      |Pass                                                                  "
				});
		}

		private static IEnumerable GetTestTestWithOnlyBeforeTestData()
		{
			// 1)
			yield return new TestCaseData("short",
				new List<string>
				{
					"short - Pass                                                          |                                                                      "
				});

			// 2)
			yield return new TestCaseData("this line is too long to be displayed with its result after it, dumb",
				new List<string>
				{
					"this line is too long to be displayed with its result after it, dumb  |                                                                      ",
					"Pass                                                                  |                                                                      "
				});

			// 3)
			yield return new TestCaseData(
				"this line is super, super, duper, super long and it will need to be wrapped because it is so super, super, duper, super long, in fact it will need to be wrapped twice",
				new List<string>
				{
					"this line is super, super, duper, super long and it will need to be wr|                                                                      ",
					" -apped because it is so super, super, duper, super long, in fact it w|                                                                      ",
					" -ill need to be wrapped twice                                        |                                                                      ",
					"Pass                                                                  |                                                                      "
				});
		}
	}
}
