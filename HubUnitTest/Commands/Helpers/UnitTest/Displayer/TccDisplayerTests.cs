﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Enum;

using CDU = Hub.Commands.Helpers.UnitTest.Displayer.ComparerDisplayerUtils;

namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// tests <see cref="TccDisplayer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TccDisplayerTests
	{
		private const string Ender =
			"--------                                                              |--------                                                              ";

		/// <summary>
		/// tests <see cref="TccDisplayer.GetLines" /> with no message and no call stack
		/// </summary>
		[Test]
		public void TestGetLinesNoMessageNoStack()
		{
			const string name = "test";
			var before = new TestCase(name, TestCaseResult.Pass, TimeSpan.FromSeconds(29));
			var after = new TestCase(name, TestCaseResult.Pass, TimeSpan.FromSeconds(97));

			var tcc = new TestCaseComparer(before, after);

			var test = "Test 1 of 2 - Same";

			var expected = new List<string>
			{
				CDU.Line(test, test),
				CDU.Blank(),
				"test - Pass                                                           |test - Pass                                                           ",
				Ender
			};
			UnitTestUtil.TestListsAreEqual(expected, TccDisplayer.GetLines(tcc, 1, 2, ComparisonResult.Same));
		}

		/// <summary>
		/// tests <see cref="TccDisplayer.GetLines" /> with a message and no call stack
		/// </summary>
		[Test]
		public void TestGetLinesMessageNoStack()
		{
			const string name = "I know I don't need to, but I still want to test this once more, so ha";
			var before = new TestCase(name, TestCaseResult.Fail, TimeSpan.FromSeconds(98), "This message doesn't wrap");
			var after = new TestCase(name, TestCaseResult.Pass, TimeSpan.FromSeconds(83),
				"This message is much, much longer and so it will need to be wrapped, because it is so long");

			var tcc = new TestCaseComparer(before, after);

			var test = "Test 8 of 10 - Fixed";

			var expected = new List<string>
			{
				CDU.Line(test, test),
				CDU.Blank(),
				"                            Fail - I know I don't need to, but I still want to test this once more, so ha - Pass                             ",
				CDU.Blank(),
				CDU.Line("This message doesn't wrap",
					"This message is much, much longer and so it will need to be wrapped, b"),
				CDU.Line("", " -ecause it is so long"),
				Ender
			};

			UnitTestUtil.TestListsAreEqual(expected, TccDisplayer.GetLines(tcc, 8, 10, ComparisonResult.Same));
		}

		/// <summary>
		/// tests <see cref="TccDisplayer.GetLines" /> with no message and a call stack
		/// </summary>
		[Test]
		public void TestGetLinesNoMessageStack()
		{
			const string name =
				"This functionality has been fully tested elsewhere, but I think it is a good idea to also test it in context so I can be sure it's good";
			var before = new TestCase(name, TestCaseResult.Pass, TimeSpan.FromSeconds(85), null,
				new List<string> { "a line in the call stack that is so long that it will need to be wrapped", "b" });
			var after = new TestCase(name, TestCaseResult.Fail, TimeSpan.FromSeconds(20), null,
				new List<string> { "a", "b", "c", "d", "e", "f" });

			var tcc = new TestCaseComparer(before, after);

			var test = "Test 10 of 300 - Broken";

			var expected = new List<string>
			{
				CDU.Line(test, test),
				CDU.Blank(),
				"   This functionality has been fully tested elsewhere, but I think it is a good idea to also test it in context so I can be sure it's good   ",
				"Pass                                                                  |Fail                                                                  ",
				CDU.Blank(),
				"a line in the call stack that is so long that it will need to be wrapp|a                                                                     ",
				" -ed                                                                  |b                                                                     ",
				"b                                                                     |c                                                                     ",
				"                                                                      |d                                                                     ",
				"                                                                      |e                                                                     ",
				"                                                                      |f                                                                     ",
				Ender
			};

			UnitTestUtil.TestListsAreEqual(expected, TccDisplayer.GetLines(tcc, 10, 300, ComparisonResult.Same));
		}

		/// <summary>
		/// tests <see cref="TccDisplayer.GetLines" /> with a message and a call stack
		/// </summary>
		[Test]
		public void TestGetLinesMessageStack()
		{
			const string name =
				"This functionality has been fully tested elsewhere, but I think it is a good idea to also test it in context so I can be sure it's good so that's what I'm doing here";
			var before = new TestCase(name, TestCaseResult.Fail, TimeSpan.FromSeconds(39), "This message is too long so it will need to be wrapped, which can be really obnoxious",
				new List<string> { "a", "b", "c", "d", "e", "f" });
			var after = new TestCase(name, TestCaseResult.Fail, TimeSpan.FromSeconds(20), "This message isn't really that long",
				new List<string> { "a line in the call stack that is so long that it will need to be wrapped", "b" });

			var tcc = new TestCaseComparer(before, after);

			var test = "Test 4 of 32 - Broken Differently";

			var expected = new List<string>
			{
				CDU.Line(test, test),
				CDU.Blank(),
				"This functionality has been fully tested elsewhere, but I think it is a good idea to also test it in context so I can be sure it's good so th",
				" -at's what I'm doing here                                                                                                                   ",
				"Fail                                                                  |Fail                                                                  ",
				CDU.Blank(),
				"This message is too long so it will need to be wrapped, which can be r|This message isn't really that long                                   ",
				" -eally obnoxious                                                     |                                                                      ",
				CDU.Blank(),
				"a                                                                     |a line in the call stack that is so long that it will need to be wrapp",
				"b                                                                     | -ed                                                                  ",
				"c                                                                     |b                                                                     ",
				"d                                                                     |                                                                      ",
				"e                                                                     |                                                                      ",
				"f                                                                     |                                                                      ",
				Ender
			};

			UnitTestUtil.TestListsAreEqual(expected, TccDisplayer.GetLines(tcc, 4, 32, ComparisonResult.Same));
		}
	}
}
