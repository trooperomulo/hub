﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest.Displayer;

namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// tests <see cref="TestProgressTimeDisplayer"/>
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestProgressTimeDisplayerTests
	{
		#region Tests

		/// <summary>
		/// makes sure an empty object is set up as expected
		/// </summary>
		[Test]
		public void TestEmptyObject()
		{
			var displayer = new TestProgressTimeDisplayer();
			Assert.AreEqual(0.0, displayer.ElapsedSecs, "an empty displayer shouldn't have any elapsed seconds");
			Assert.AreEqual(0.0, displayer.RemainingSecs, "an empty displayer shouldn't have any remaining seconds");
			Assert.AreEqual("0:00:00 passed with an estimated 0:00:00 remaining - 0.00%", displayer.Display);
		}

		/// <summary>
		/// tests <see cref="TestProgressTimeDisplayer.TestToBeRun" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestTestToBeRunData))]
		public void TestTestToBeRun(List<(string sol, double time)> sols, double expected)
		{
			var displayer = new TestProgressTimeDisplayer();
			foreach (var sol in sols)
			{
				displayer.TestToBeRun(sol.sol, sol.time);
			}

			Assert.AreEqual(GetDisplayString(expected), displayer.Display, "display was wrong");
		}

		/// <summary>
		/// tests <see cref="TestProgressTimeDisplayer.TestHasFinished" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetTestTestHasFinishedData))]
		public void TestTestHasFinished(List<(string sol, double time)> toAdd, List<(string sol, double time)> finished,
			double expElap, double expRema)
		{
			var displayer = new TestProgressTimeDisplayer();
			foreach (var sol in toAdd)
			{
				displayer.TestToBeRun(sol.sol, sol.time);
			}

			foreach (var sol in finished)
			{
				displayer.TestHasFinished(sol.sol, sol.time);
			}

			Assert.AreEqual(GetDisplayString(expRema, expElap), displayer.Display, "display was wrong");
		}

		#endregion Tests

		#region Helpers

		private static string GetDisplayString(double remaining, double elapsed = 0.0)
		{
			var elap = TimeSpan.FromSeconds(elapsed);
			var rema = TimeSpan.FromSeconds(remaining);
			var perc = (decimal) elapsed / ((decimal) elapsed + (decimal) remaining);
			return $"{elap:g} passed with an estimated {rema:g} remaining - {perc:P}";
		}

		private static IEnumerable GetTestTestToBeRunData()
		{
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2)}, .3);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2), ("c", 0.6)}, .9);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("a", 0.2), ("c", 0.6)}, .8);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("a", 0.2), ("a", 0.6)}, .6);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.8), ("a", 0.2), ("a", 0.1)}, .1);
		}

		private static IEnumerable GetTestTestHasFinishedData()
		{
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2)},
				new List<(string, double)> {("a", 0.2)}, 0.2, 0.2);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2)},
				new List<(string, double)> {("c", 0.2)}, 0.0, 0.3);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2)},
				new List<(string, double)> {("a", 0.2), ("a", 0.3)}, 0.3, 0.2);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2), ("c", 0.6)},
				new List<(string, double)> {("a", 0.2)}, 0.2, 0.8);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2), ("c", 0.6)},
				new List<(string, double)> {("a", 0.6), ("a", 0.4)}, 0.4, 0.8);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2), ("c", 0.6)},
				new List<(string, double)> {("a", 0.2), ("b", 0.1)}, 0.3, 0.6);
			yield return new TestCaseData(new List<(string, double)> {("a", 0.1), ("b", 0.2), ("c", 0.6)},
				new List<(string, double)> {("a", 0.2), ("b", 0.1), ("c", 0.9)}, 1.2, 0.0);
		}

		#endregion Helpers
	}
}
