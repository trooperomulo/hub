﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// tests <see cref="UtrpcDisplayer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UtrpcDisplayerTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="UtrpcDisplayer.GetLines" /> with only after set
		/// </summary>
		[Test]
		public void TestAfter()
		{
			var after = new List<UtrapComparer>
			{
				Af(Pa(1, new List<TestCase> {Ca(3, "message"), Ca(2, null)})),
				Af(Pa(2, new List<TestCase> {Ca(4, null), Ca(1, "something"), Ca(5, null)}))
			};

			var expected = new List<string>
			{
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |                              Solution 1                              ",
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |Test 1 of 2 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 2 - Pass                                                         ",
				"                                                                      |--------                                                              ",
				"                                                                      |Test 2 of 2 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 3 - Pass                                                         ",
				"                                                                      |                                                                      ",
				"                                                                      |message                                                               ",
				"                                                                      |                                                                      ",
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |                              Solution 2                              ",
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |Test 1 of 3 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 1 - Pass                                                         ",
				"                                                                      |                                                                      ",
				"                                                                      |something                                                             ",
				"                                                                      |--------                                                              ",
				"                                                                      |Test 2 of 3 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 4 - Pass                                                         ",
				"                                                                      |--------                                                              ",
				"                                                                      |Test 3 of 3 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 5 - Pass                                                         "
			};

			UnitTestUtil.TestListsAreEqual(expected, UtrpcDisplayer.GetLines(after));
		}

		/// <summary>
		/// tests <see cref="UtrpcDisplayer.GetLines" /> with only before set
		/// </summary>
		[Test]
		public void TestBefore()
		{
			var before = new List<UtrapComparer>
			{
				Be(Pa(1, new List<TestCase> {Ca(3, "message"), Ca(2, null)})),
				Be(Pa(2, new List<TestCase> {Ca(4, null), Ca(1, "something"), Ca(5, null)}))
			};

			var expected = new List<string>
			{
				"----------------------------------------------------------------------|                                                                      ",
				"                              Solution 1                              |                                                                      ",
				"----------------------------------------------------------------------|                                                                      ",
				"Test 1 of 2 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 2 - Pass                                                         |                                                                      ",
				"--------                                                              |                                                                      ",
				"Test 2 of 2 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 3 - Pass                                                         |                                                                      ",
				"                                                                      |                                                                      ",
				"message                                                               |                                                                      ",
				"                                                                      |                                                                      ",
				"----------------------------------------------------------------------|                                                                      ",
				"                              Solution 2                              |                                                                      ",
				"----------------------------------------------------------------------|                                                                      ",
				"Test 1 of 3 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 1 - Pass                                                         |                                                                      ",
				"                                                                      |                                                                      ",
				"something                                                             |                                                                      ",
				"--------                                                              |                                                                      ",
				"Test 2 of 3 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 4 - Pass                                                         |                                                                      ",
				"--------                                                              |                                                                      ",
				"Test 3 of 3 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 5 - Pass                                                         |                                                                      "
			};

			UnitTestUtil.TestListsAreEqual(expected, UtrpcDisplayer.GetLines(before));
		}

		/// <summary>
		/// tests <see cref="UtrpcDisplayer.GetLines" /> with both set
		/// </summary>
		[Test]
		public void TestBoth()
		{
			var before = new List<UnitTestResultAssemblyParser>
			{
				Pa(2, new List<TestCase> {Ca(1, "message"), Ca(7)}),
				Pa(4, new List<TestCase> {Ca(8), Ca(2, "something", new List<string> {"a", "b", "c"})}),
				Pa(1, new List<TestCase> {Ca(5), Ca(6), Ca(3)})
			};
			var after = new List<UnitTestResultAssemblyParser>
			{
				Pa(4, new List<TestCase> {Ca(8), Ca(2, "something", new List<string> {"a", "b", "c"})}),
				Pa(3, new List<TestCase> {Ca(4)}),
				Pa(1, new List<TestCase> {Ca(9), Ca(3)})
			};

			var comp = new UtrpComparer(new UnitTestResultParser(before), new UnitTestResultParser(after));

			var expected = new List<string>
			{
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				"                              Solution 1                              |                              Solution 1                              ",
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				"Test 1 of 4 - Same                                                    |Test 1 of 4 - Same                                                    ",
				"                                                                      |                                                                      ",
				"test 3 - Pass                                                         |test 3 - Pass                                                         ",
				"--------                                                              |--------                                                              ",
				"Test 2 of 4 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 5 - Pass                                                         |                                                                      ",
				"--------                                                              |--------                                                              ",
				"Test 3 of 4 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 6 - Pass                                                         |                                                                      ",
				"--------                                                              |--------                                                              ",
				"                                                                      |Test 4 of 4 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 9 - Pass                                                         ",
				"                                                                      |                                                                      ",
				"----------------------------------------------------------------------|                                                                      ",
				"                              Solution 2                              |                                                                      ",
				"----------------------------------------------------------------------|                                                                      ",
				"Test 1 of 2 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 1 - Pass                                                         |                                                                      ",
				"                                                                      |                                                                      ",
				"message                                                               |                                                                      ",
				"--------                                                              |                                                                      ",
				"Test 2 of 2 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"test 7 - Pass                                                         |                                                                      ",
				"                                                                      |                                                                      ",
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |                              Solution 3                              ",
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |Test 1 of 1 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |test 4 - Pass                                                         ",
				"                                                                      |                                                                      ",
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				"                              Solution 4                              |                              Solution 4                              ",
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				"Test 1 of 2 - Same                                                    |Test 1 of 2 - Same                                                    ",
				"                                                                      |                                                                      ",
				"test 2 - Pass                                                         |test 2 - Pass                                                         ",
				"                                                                      |                                                                      ",
				"something                                                             |something                                                             ",
				"                                                                      |                                                                      ",
				"a                                                                     |a                                                                     ",
				"b                                                                     |b                                                                     ",
				"c                                                                     |c                                                                     ",
				"--------                                                              |--------                                                              ",
				"Test 2 of 2 - Same                                                    |Test 2 of 2 - Same                                                    ",
				"                                                                      |                                                                      ",
				"test 8 - Pass                                                         |test 8 - Pass                                                         "
			};

			UnitTestUtil.TestListsAreEqual(expected, comp.GetLines());
		}

		#endregion Tests

		#region Helpers

		private static UtrapComparer Af(UnitTestResultAssemblyParser parser) => UtrapComparer.Added(parser);

		private static UtrapComparer Be(UnitTestResultAssemblyParser parser) => UtrapComparer.Removed(parser);

		private static TestCase Ca(int test, string message = null, List<string> stack = null) =>
			new TestCase($"test {test}", TestCaseResult.Pass, TimeSpan.Zero, message, stack);

		private static UnitTestResultAssemblyParser Pa(int sol, List<TestCase> cases) =>
			new UnitTestResultAssemblyParser($"Solution {sol}", 0, 0, 0, 0, false, cases);

		#endregion Helpers
	}
}
