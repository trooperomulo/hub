﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;

using Hub.Commands;
using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;

using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// Tests <see cref="UnitTestResultDisplayer" /> for console results
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UnitTestResultDisplayerConsoleTests : UnitTestResultDisplayerBase
	{
		#region Expected Lines

		// none
		private static readonly List<string> Jane = new List<string> {"Jane.exe"};
		private static readonly List<string> Rose = new List<string> {"Rose.exe"};

		// success
		private static readonly List<string> Georgette = new List<string> {"Georgette.exe - 3 of 3 Tests passed"};
		private static readonly List<string> Henrietta = new List<string> {"Henrietta.exe - 2 of 2 Tests passed"};

		// failure
		#region Bertha

		private static readonly List<string> Bertha = new List<string>
		{
			MedLine,
			"Bertha.exe - 1 of 1 Test failed",
			ShortLine,
			"Failure 1 of 1 - Test1",
			"message 1",
			"",
			"stack line 1",
			"stack line 2",
			"stack line 3",
		};

		#endregion Bertha

		#region Freda

		private static readonly List<string> Freda = new List<string>
		{
			MedLine,
			"Freda.exe - 1 of 1 Test failed",
			ShortLine,
			"Failure 1 of 1 - Test2",
			"message 2",
			"",
			"stack line 4",
			"stack line 5",
			"stack line 6",
		};

		#endregion Freda

		// inconclusive
		private static readonly List<string> Regina = new List<string> {MedLine, "Regina.exe - 2 of 2 Tests failed"};

		// skip
		#region Loretta
		private static readonly List<string> Loretta = new List<string>
		{
			MedLine,
			"Loretta.exe - 1 of 1 Test skipped",
			ShortLine,
			"Skip 1 of 1 - Test5",
			"skipped message 1",
		};
		#endregion Loretta

		#region Dorothy
		private static readonly List<string> Dorothy = new List<string>
		{
			MedLine,
			"Dorothy.exe - 1 of 1 Test skipped",
			ShortLine,
			"Skip 1 of 1 - Test6",
			"skipped message 2",
		};
		#endregion Dorothy

		// mixed
		#region MixedFS.exe
		private static readonly List<string> MixFs = new List<string>
		{
			MedLine,
			"MixedFS.exe - 2 of 2 Tests failed/skipped",
			ShortLine,
			"Skip 1 of 2 - TestFs1",
			"this test was also skipped",
			ShortLine,
			"Failure 2 of 2 - TestFs2",
			"fs message",
			"",
			"stack line Fs1",
			"stack line Fs2",
			"stack line Fs3",
		};
		#endregion MixedPF.exe

		#region MixedPF.exe
		private static readonly List<string> MixPf = new List<string>
		{
			MedLine,
			"MixedPF.exe - 1 of 2 Tests failed",
			ShortLine,
			"Failure 1 of 1 - Test4",
			"message 3",
			"",
			"stack line 7",
			"stack line 8",
			"stack line 9",
		};
		#endregion MixedPF.exe

		#region MixedPS.exe
		private static readonly List<string> MixPs = new List<string>
		{
			MedLine,
			"MixedPS.exe - 1 of 2 Tests skipped",
			ShortLine,
			"Skip 1 of 1 - TestPs2",
			"this test was skipped",
		};
		#endregion MixedPS.exe

		#region MixedFS.exe
		private static readonly List<string> All = new List<string>
		{
			MedLine,
			"Everything.exe - 2 of 3 Tests failed/skipped",
			ShortLine,
			"Skip 1 of 2 - TestEv1",
			"this test was another to be skipped",
			ShortLine,
			"Failure 2 of 2 - TestEv2",
			"ev message",
			"",
			"stack line Ev1",
			"stack line Ev2",
			"stack line Ev3",
		};
		#endregion MixedPF.exe

		#endregion Expected Lines

		#region Headers

		private static readonly List<string> NoTest = Header("1 NO TESTS");
		private static readonly List<string> NoTests = Header("2 NO TESTS");

		private static readonly List<string> Success = Header("1 SUCCESS");
		private static readonly List<string> Successes = Header("2 SUCCESSES");

		private static readonly List<string> Failures1 = Header("1 FAILURE");
		private static readonly List<string> Failures2 = Header("2 FAILURES");

		private static readonly List<string> Skips1 = Header("1 SKIP");
		private static readonly List<string> Skips2 = Header("2 SKIPS");

		#endregion Headers

		#region Tests

		/// <summary>
		/// tests the display results if the parser has 1 successful solution
		/// </summary>
		[Test]
		public void TestNoTests1()
		{
			var expected = new List<string>();
			expected.AddRange(NoTest);
			expected.AddRange(Jane);
			expected.Add("");
			DoTextTest(None1, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 2 successful solutions
		/// </summary>
		[Test]
		public void TestNoTests2()
		{
			var expected = new List<string>();
			expected.AddRange(NoTests);
			expected.AddRange(Jane);
			expected.AddRange(Rose);
			expected.Add("");
			DoTextTest(None1 + None2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has no solutions
		/// </summary>
		[Test]
		public void Test0Pass0Fail() => DoTextTest("", new List<string> { "No solutions were tested" });

		/// <summary>
		/// tests the display results if the parser has 1 failed solution
		/// </summary>
		[Test]
		public void Test0Pass1Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Failures1);
			expected.AddRange(Bertha);
			expected.Add("");
			DoTextTest(Failed1, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 failed solution
		/// </summary>
		[Test]
		public void Test0Pass2Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Failures2);
			expected.AddRange(Bertha);
			expected.AddRange(Freda);
			expected.Add("");
			DoTextTest(Failed1 + Failed2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 successful solution
		/// </summary>
		[Test]
		public void Test1Pass0Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Success);
			expected.AddRange(Henrietta);
			expected.Add("");
			DoTextTest(Passed1, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 successful and 1 failed solution
		/// </summary>
		[Test]
		public void Test1Pass1Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Success);
			expected.AddRange(Henrietta);
			expected.Add("");
			expected.AddRange(Failures1);
			expected.AddRange(Bertha);
			expected.Add("");
			DoTextTest(Failed1 + Passed1, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 successful and 1 failed solution
		/// </summary>
		[Test]
		public void Test1Pass2Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Success);
			expected.AddRange(Henrietta);
			expected.Add("");
			expected.AddRange(Failures2);
			expected.AddRange(Bertha);
			expected.AddRange(Freda);
			expected.Add("");
			DoTextTest(Failed1 + Passed1 + Failed2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 2 successful solutions
		/// </summary>
		[Test]
		public void Test2Pass0Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Successes);
			expected.AddRange(Georgette);
			expected.AddRange(Henrietta);
			expected.Add("");
			DoTextTest(Passed1 + Passed2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 successful and 1 failed solution
		/// </summary>
		[Test]
		public void Test2Pass1Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Successes);
			expected.AddRange(Georgette);
			expected.AddRange(Henrietta);
			expected.Add("");
			expected.AddRange(Failures1);
			expected.AddRange(Bertha);
			expected.Add("");
			DoTextTest(Failed1 + Passed1 + Passed2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 successful and 1 failed solution
		/// </summary>
		[Test]
		public void Test2Pass2Fail()
		{
			var expected = new List<string>();
			expected.AddRange(Successes);
			expected.AddRange(Georgette);
			expected.AddRange(Henrietta);
			expected.Add("");
			expected.AddRange(Failures2);
			expected.AddRange(Bertha);
			expected.AddRange(Freda);
			expected.Add("");
			DoTextTest(Failed1 + Passed1 + Passed2 + Failed2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 inconclusive solution
		/// </summary>
		[Test]
		public void TestIncl()
		{
			var expected = new List<string>();
			expected.AddRange(Failures1);
			expected.AddRange(Regina);
			expected.Add("");
			DoTextTest(Inconc, expected);
		}

		/// <summary>
		/// tests the display reuslts if the parser has 1 failed solution with 
		/// </summary>
		[Test]
		public void TestMixedFailSkip()
		{
			var expected = new List<string>();
			expected.AddRange(Failures1);
			expected.AddRange(MixFs);
			expected.Add("");
			DoTextTest(MixedFs, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 failed solution with mixed test case results
		/// </summary>
		[Test]
		public void TestMixedPassFail()
		{
			var expected = new List<string>();
			expected.AddRange(Failures1);
			expected.AddRange(MixPf);
			expected.Add("");
			DoTextTest(MixedPf, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 skipp solution with mixed test case results
		/// </summary>
		[Test]
		public void TestMixedPassSkipp()
		{
			var expected = new List<string>();
			expected.AddRange(Skips1);
			expected.AddRange(MixPs);
			expected.Add("");
			DoTextTest(MixedPs, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 1 skipped solution
		/// </summary>
		[Test]
		public void TestSkip1()
		{
			var expected = new List<string>();
			expected.AddRange(Skips1);
			expected.AddRange(Loretta);
			expected.Add("");
			DoTextTest(Skipped1, expected);
		}

		/// <summary>
		/// tests the display results if the parser has 2 skipped solution
		/// </summary>
		[Test]
		public void TestSkip2()
		{
			var expected = new List<string>();
			expected.AddRange(Skips2);
			expected.AddRange(Dorothy);
			expected.AddRange(Loretta);
			expected.Add("");
			DoTextTest(Skipped1 + Skipped2, expected);
		}

		/// <summary>
		/// tests the display results if the parser has a bit of everything in one solution
		/// </summary>
		[Test]
		public void TestEverythingInOneSolution()
		{
			var expected = new List<string>();
			expected.AddRange(Failures1);
			expected.AddRange(All);
			expected.Add("");
			DoTextTest(Everything, expected);
		}

		/// <summary>
		/// test the display results if the parser has a bit of everything in their own solutions
		/// </summary>
		[Test]
		public void TestEverythingInFourSolutions()
		{
			var expected = new List<string>();
			expected.AddRange(NoTest);
			expected.AddRange(Jane);
			expected.Add("");
			expected.AddRange(Success);
			expected.AddRange(Henrietta);
			expected.Add("");
			expected.AddRange(Skips1);
			expected.AddRange(Loretta);
			expected.Add("");
			expected.AddRange(Failures1);
			expected.AddRange(Bertha);
			expected.Add("");
			DoTextTest(Failed1 + Skipped1 + Passed1 + None1, expected);
		}

		/// <summary>
		/// test the display results after parsing the sample 
		/// </summary>
		[Test]
		public void TestSampleXml()
		{
			var parser = GetParser();
			var displayer = new UnitTestResultDisplayer(parser);
			var actual = displayer.GetResultsToDisplayInConsole();
			//File.WriteAllLines(@"c:\temp\sample.txt", actual);
			var expected = new List<string>
			{
				"------------------------",
				"1 INVALID",
				"fake.dll",
				"",
				"------------------------",
				"2 NO TESTS",
				"xm8.data.res.dll",
				"xm8.mp.ew.dll",
				"",
				"------------------------",
				"2 SUCCESSES",
				"xm8.mp.controlcenter.dll - 100 of 100 Tests passed",
				"xm8.rpt.dataparse.dll - 58 of 58 Tests passed",
				"",
				"------------------------",
				"2 SKIPS",
				"----------------",
				"core.data.dll - 4 of 1381 Tests skipped",
				"--------",
				"Skip 1 of 4 - PropertiesTest",
				"Test is commented out",
				"--------",
				"Skip 2 of 4 - CreateData",
				"FIX_IT_FAILING_UNIT_TEST",
				"--------",
				"Skip 3 of 4 - TestCheckPassword",
				"THIS TEST RELIES ON FOLDERS OR FILES NOT IN THE XM8 GIT REPO.  THE TEST NEEDS TO BE REWORKED",
				"--------",
				"Skip 4 of 4 - TestPasswordsMatch",
				"THIS TEST RELIES ON FOLDERS OR FILES NOT IN THE XM8 GIT REPO.  THE TEST NEEDS TO BE REWORKED",
				"----------------",
				"xm8.busi.dll - 7 of 898 Tests skipped",
				"--------",
				"Skip 1 of 7 - FiltedCoverageID",
				"I don\'t know how to fix this test",
				"--------",
				"Skip 2 of 7 - TestDoorAndLocksetTestCasesFromVernon",
				"This test takes too long.  It all works now, so turn it off while working on the other tests.",
				"--------",
				"Skip 3 of 7 - TestExcluded",
				"Test is commented out",
				"--------",
				"Skip 4 of 7 - TestFlooringXpertRoundingIssues",
				"This test succeeds, but I have turned it off because it takes about 20 minutes for it to run.  Leave it off unless there is a specific need to test rounding.",
				"--------",
				"Skip 5 of 7 - TestGetSetRuleSettings",
				"Test is commented out",
				"--------",
				"Skip 6 of 7 - TestRoundingErrors",
				"Test is commented out",
				"--------",
				"Skip 7 of 7 - TestWcRoundingErrors",
				"Test is commented out",
				"",
				"------------------------",
				"2 FAILURES",
				"----------------",
				"core.busi.dll - 5 of 90 Tests failed/skipped",
				"--------",
				"Skip 1 of 5 - TestUnzipOnline",
				"Depends on files in a specific file location",
				"--------",
				"Failure 2 of 5 - TestGetCombinedSearchResultsAsync",
				"Expected: True\n  But was:  False",
				"",
				"at Core.Busi.Unit_Tests.MILCombinedResultsTest.TestGetCombinedSearchResultsAsync() in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Busi.Busi\\Unit Tests\\MILCombinedResult.UnitTests.cs:line 32",
				"--------",
				"Failure 3 of 5 - TestSmartTimer",
				"Expected: True\n  But was:  False",
				"",
				"at Core.Busi.Unit_Tests.SmartTimerTest.TestSmartTimer() in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Busi.Busi\\Unit Tests\\SmartTimer.UnitTests.cs:line 35",
				"--------",
				"Skip 4 of 5 - TestCleanOldFilesAndDirectories",
				"THIS TEST RELIES ON FOLDERS OR FILES NOT IN THE XM8 GIT REPO.  THE TEST NEEDS TO BE REWORKED",
				"--------",
				"Skip 5 of 5 - TestDelete",
				"THIS TEST RELIES ON FOLDERS OR FILES NOT IN THE XM8 GIT REPO.  THE TEST NEEDS TO BE REWORKED",
				"----------------",
				"core.interface.itables.dll - 3 of 178 Tests failed",
				"--------",
				"Failure 1 of 3 - IsAvailable",
				"Expected: False\n  But was:  True",
				"",
				"at Core.Interface.ITables.ICoreStateTests.IsAvailable(ICoreState coreState) in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 556",
				"at Core.Interface.ITables.CoreStateBaseTest.IsAvailable() in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 1700",
				"--------",
				"Failure 2 of 3 - TestAllIsAvailable",
				"IsAvailable(CoverageFactor) should return False\n  Expected: True\n  But was:  False",
				"",
				"at Core.Interface.ITables.ICoreStateTests.TestOneIsAvailable(ICoreState coreState, AvailableEnum avail, Boolean expectedValue) in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 579",
				"at Core.Interface.ITables.ICoreStateTests.TestAllIsAvailable(ICoreState coreState) in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 685",
				"at Core.Interface.ITables.CoreStateBaseTest.TestAllIsAvailable() in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 1687",
				"--------",
				"Failure 3 of 3 - IsAvailable",
				"Expected: False\n  But was:  True",
				"",
				"at Core.Interface.ITables.ICoreStateTests.IsAvailable(ICoreState coreState) in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 556",
				"at Core.Interface.ITables.CoreStateTest.IsAvailable() in C:\\git\\2\\xactimate.shared\\xm8core\\Core.Interface.ITables\\Unit Tests\\ICoreState.UnitTests.cs:line 1993",
				""
			};

			UnitTestUtil.TestListsAreEqual(expected, actual);
		}

		#endregion Tests

		#region Helpers

		private static void DoTextTest(string middle, List<string> expected)
		{
			List<string> Getter(UnitTestResultDisplayer displayer) => displayer.GetResultsToDisplayInConsole();

			DoTest(middle, expected, Getter);
		}
		
		private static UnitTestResultParser GetParser()
		{
			try
			{
				return new UnitTestResultParser(XElement.Load(UnitTestUtil.GetTestPath("sample.xml", nameof(UnitTestCommand))), true);
			}
			catch (Exception)
			{
				return new UnitTestResultParser(XElement.Load(UnitTestUtil.GetTestPath("sampleb.xml", nameof(UnitTestCommand))), true);
			}
		}

		private static List<string> Header(string header) => new List<string> {LongLine, header};

		#endregion Helpers
	}
}
