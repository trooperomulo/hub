﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Helpers.UnitTest;
using Hub.Commands.Helpers.UnitTest.Comparer;
using Hub.Commands.Helpers.UnitTest.Displayer;
using Hub.Commands.Helpers.UnitTest.Parser;
using Hub.Enum;

namespace HubUnitTest.Commands.Helpers.UnitTest.Displayer
{
	/// <summary>
	/// Tests <see cref="UtrapcDisplayer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UtrapcDisplayerTests
	{
		/// <summary>
		/// tests <see cref="UtrapcDisplayer.GetLines" /> with just after set
		/// </summary>
		[Test]
		public void TestAfter()
		{
			var cases = new List<TestCase>
			{
				new TestCase("TestBefore", TestCaseResult.Fail, TimeSpan.FromSeconds(235)),
				new TestCase("TestAfter", TestCaseResult.Pass, TimeSpan.FromSeconds(230)),
			};

			var added = Parser("Xm8.App.Internal.SchemaAutomation.UnitTests", cases);

			var utrapc = UtrapComparer.Added(added);

			var expected = new List<string>
			{
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |             Xm8.App.Internal.SchemaAutomation.UnitTests              ",
				"                                                                      |----------------------------------------------------------------------",
				"                                                                      |Test 1 of 2 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |TestAfter - Pass                                                      ",
				"                                                                      |--------                                                              ",
				"                                                                      |Test 2 of 2 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |TestBefore - Fail                                                     ",
				"                                                                      |                                                                      "
			};

			UnitTestUtil.TestListsAreEqual(expected, UtrapcDisplayer.GetLines(utrapc));
		}

		/// <summary>
		/// tests <see cref="UtrapcDisplayer.GetLines" /> with just before set
		/// </summary>
		[Test]
		public void TestBefore()
		{
			var cases = new List<TestCase>
			{
				new TestCase("TestBoth", TestCaseResult.Incl, TimeSpan.FromSeconds(209), "The message from this test",
					new List<string> {"Call stack line 1", "Call stack line 2", "Call stack line 3"})
			};

			var removed = Parser("Xm8.MP.ControlCenter", cases);
			var utrapc = UtrapComparer.Removed(removed);

			var expected = new List<string>
			{
				"----------------------------------------------------------------------|                                                                      ",
				"                         Xm8.MP.ControlCenter                         |                                                                      ",
				"----------------------------------------------------------------------|                                                                      ",
				"Test 1 of 1 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"TestBoth - Incl                                                       |                                                                      ",
				"                                                                      |                                                                      ",
				"The message from this test                                            |                                                                      ",
				"                                                                      |                                                                      ",
				"Call stack line 1                                                     |                                                                      ",
				"Call stack line 2                                                     |                                                                      ",
				"Call stack line 3                                                     |                                                                      ",
				"                                                                      |                                                                      "
			};

			UnitTestUtil.TestListsAreEqual(expected, UtrapcDisplayer.GetLines(utrapc));
		}

		/// <summary>
		/// tests <see cref="UtrapcDisplayer.GetLines" /> with before and after set
		/// </summary>
		[Test]
		public void TestBoth()
		{
			var bCases = new List<TestCase>
			{
				new TestCase("Test5", TestCaseResult.Pass, TimeSpan.Zero, "Message 5", new List<string> {"x", "y", "z"}),
				new TestCase("Test2", TestCaseResult.Pass, TimeSpan.Zero),
				new TestCase("Test1", TestCaseResult.Pass, TimeSpan.Zero, "Message 1"),
				new TestCase("Test4", TestCaseResult.Pass, TimeSpan.Zero, null, new List<string> {"a", "b", "c"})
			};

			var aCases = new List<TestCase>
			{
				new TestCase("Test4", TestCaseResult.Pass, TimeSpan.Zero, null,
					new List<string> {"d", "e", "f", "g", "h", "i"}),
				new TestCase("Test3", TestCaseResult.Pass, TimeSpan.Zero),
				new TestCase("Test6", TestCaseResult.Pass, TimeSpan.Zero, "Message 6", new List<string> {"r", "s", "t"}),
				new TestCase("Test1", TestCaseResult.Pass, TimeSpan.Zero, "Message 1")
			};

			var before = Parser("Xm8.MP.EWD", bCases);
			var after = Parser("Xm8.MP.EWD", aCases);

			var comp = new UtrapComparer(before, after);

			var expected = new List<string>
			{
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				"                              Xm8.MP.EWD                              |                              Xm8.MP.EWD                              ",
				"----------------------------------------------------------------------|----------------------------------------------------------------------",
				"Test 1 of 6 - Same                                                    |Test 1 of 6 - Same                                                    ",
				"                                                                      |                                                                      ",
				"Test1 - Pass                                                          |Test1 - Pass                                                          ",
				"                                                                      |                                                                      ",
				"Message 1                                                             |Message 1                                                             ",
				"--------                                                              |--------                                                              ",
				"Test 2 of 6 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"Test2 - Pass                                                          |                                                                      ",
				"--------                                                              |--------                                                              ",
				"                                                                      |Test 3 of 6 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |Test3 - Pass                                                          ",
				"--------                                                              |--------                                                              ",
				"Test 4 of 6 - Same                                                    |Test 4 of 6 - Same                                                    ",
				"                                                                      |                                                                      ",
				"Test4 - Pass                                                          |Test4 - Pass                                                          ",
				"                                                                      |                                                                      ",
				"a                                                                     |d                                                                     ",
				"b                                                                     |e                                                                     ",
				"c                                                                     |f                                                                     ",
				"                                                                      |g                                                                     ",
				"                                                                      |h                                                                     ",
				"                                                                      |i                                                                     ",
				"--------                                                              |--------                                                              ",
				"Test 5 of 6 - Removed                                                 |                                                                      ",
				"                                                                      |                                                                      ",
				"Test5 - Pass                                                          |                                                                      ",
				"                                                                      |                                                                      ",
				"Message 5                                                             |                                                                      ",
				"                                                                      |                                                                      ",
				"x                                                                     |                                                                      ",
				"y                                                                     |                                                                      ",
				"z                                                                     |                                                                      ",
				"--------                                                              |--------                                                              ",
				"                                                                      |Test 6 of 6 - Added                                                   ",
				"                                                                      |                                                                      ",
				"                                                                      |Test6 - Pass                                                          ",
				"                                                                      |                                                                      ",
				"                                                                      |Message 6                                                             ",
				"                                                                      |                                                                      ",
				"                                                                      |r                                                                     ",
				"                                                                      |s                                                                     ",
				"                                                                      |t                                                                     ",
				"                                                                      |                                                                      "
			};

			UnitTestUtil.TestListsAreEqual(expected, UtrapcDisplayer.GetLines(comp));
		}

		private UnitTestResultAssemblyParser Parser(string sol, List<TestCase> cases)
		{
			return new UnitTestResultAssemblyParser(sol, 0, 0, 0, 0, false, cases);
		}
	}
}
