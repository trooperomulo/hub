﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using NUnit.Framework;

using Hub;
using Hub.Commands.Helpers.UnitTest;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands.Helpers.UnitTest
{
	/// <summary>
	/// tests <see cref="TestTimeTracker" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TestTimeTrackerTests
	{
		#region Constants

		private const double DefaultTime = 2.6051973;
		private const double One = 3.037894737;
		private const double Two = 7.615384615;
		private const double Three = 0.531578947;

		#endregion Constants

		#region Tests

		/// <summary>
		/// tests the constructor for <see cref="TestTimeTracker" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetConstructorTestData))]
		public void TestConstructor(FilesStub files, bool exists, bool dirMade, Dictionary<string, List<double>> doubs)
		{
			// create an instance
			CreateInstance(files, new DirsStub(exists), dirMade, doubs);
		}

		/// <summary>
		/// tests <see cref="TestTimeTracker.AddTime" />
		/// </summary>
		[Test]
		[TestCaseSource(nameof(GetAddTimeTestData))]
		public void TestAddTime(int which, string sol, double time, Dictionary<string, List<double>> doubs)
		{
			// create an instance
			var files = new FilesStub(which);
			var ttt = CreateInstance(files, new DirsStub());
			// add a time
			ttt.AddTime(sol, time);
			// verify the times are correct
			TestAllTimesDictionary(doubs, ttt);
			// verify that the file was written to
			Assert.IsFalse(files.CreatedFiles.Contains($"{sol}.txt"), "wasn't created");
		}

		/// <summary>
		/// tests <see cref="TestTimeTracker.GetEstimatedTime" /> 
		/// </summary>
		[Test]
		[TestCase(1, 2, 0, 1)]
		[TestCase(1, 50, 0, 1)]
		[TestCase(2, 2, -1, 1.55555555556)]
		[TestCase(2, 2, 0, 2)]
		[TestCase(2, 50, 0, 2)]
		[TestCase(1, 2, .9, 1.4)]
		[TestCase(1.9, 2, -.9, 1.5)]
		[TestCase(1, 2, 1, 1.444444444)]
		[TestCase(1, 3, 1, 1.923076923)]
		[TestCase(1, 4, 1, 2.3125)]
		[TestCase(1, 5, 1, 2.736842105)]
		[TestCase(1, 6, 1, 3.181818182)]
		[TestCase(1, 10, 1, 4.84375)]
		[TestCase(1, 50, 1, 16.0137931)]
		public void TestGetEstimatedTime(double start, int count, double increment, double expected)
		{
			// create an instance
			var ttt = new TestTimeTracker(new FilesStub(), new DirsStub());
			var toAdd = GetList(start, count, increment);

			// add all the times
			foreach (var add in toAdd)
			{
				ttt.AddTime("sol", add);
			}

			// test that the estimate time is correct
			TestEstimatedTime(expected, ttt, "sol");
		}

		/// <summary>
		/// tests <see cref="TestTimeTracker.GetEstimatedTime" /> after loading the default testing data
		/// </summary>
		[Test]
		[TestCase(false, 0, DefaultTime, DefaultTime, DefaultTime)]
		[TestCase(true, 0, DefaultTime, DefaultTime, DefaultTime)]
		[TestCase(true, 1, One, DefaultTime, DefaultTime)]
		[TestCase(true, 2, DefaultTime, Two, Three)]
		[TestCase(true, 3, One, Two, Three)]
		public void TestDefaultGetEstimatedTime(bool exists, int which, double one, double two, double thr)
		{
			var ttt = new TestTimeTracker(new FilesStub(which), new DirsStub(exists));
			TestEstimatedTime(one, ttt, "one");
			TestEstimatedTime(two, ttt, "two");
			TestEstimatedTime(thr, ttt, "three");
		}

		#endregion Tests

		#region Helper Methods

		private static TestTimeTracker CreateInstance(FilesStub files, DirsStub dirs, bool? dirMade = null,
			Dictionary<string, List<double>> doubs = null)
		{
			// create an instance
			var ttt = new TestTimeTracker(files, dirs);
			var path = Path.Combine(Hub.Hub.ProgData, "TestTimes");
			
			if (doubs != null)
			{
				// verify the times were correct
				TestAllTimesDictionary(doubs, ttt);
			}

			// verify that the directory was made
			if (dirMade.HasValue)
			{
				Assert.AreEqual(dirMade, dirs.CreatedDirs.Contains(path), "Directory creation didn't match expectation");
			}

			return ttt;
		}

		private static IEnumerable GetConstructorTestData()
		{
			yield return new TestCaseData(new FilesStub(), false, true, new Dictionary<string, List<double>>());
			yield return new TestCaseData(new FilesStub(), true, false, new Dictionary<string, List<double>>());
			yield return new TestCaseData(new FilesStub(1), true, false,
				new Dictionary<string, List<double>> {{"one", new List<double> {1.11, 2.22, 3.33, 4.44, 5.55}}});
			yield return new TestCaseData(new FilesStub(2), true, false,
				new Dictionary<string, List<double>>
					{{"two", new List<double> {6.6, 7.7, 8.8}}, {"three", new List<double> {0.1, 0.2, 0.4, 0.8, 1.6}}});
			yield return new TestCaseData(new FilesStub(3), true, false,
				new Dictionary<string, List<double>>
				{
					{"one", new List<double> {1.11, 2.22, 3.33, 4.44, 5.55}}, {"two", new List<double> {6.6, 7.7, 8.8}},
					{"three", new List<double> {0.1, 0.2, 0.4, 0.8, 1.6}}
				});
		}

		private static IEnumerable GetAddTimeTestData()
		{
			yield return new TestCaseData(0, "abc", 1.1,
				new Dictionary<string, List<double>> {{"abc", new List<double> {1.1}}});
			yield return new TestCaseData(1, "xyz", 9.9,
				new Dictionary<string, List<double>>
					{{"one", new List<double> {1.11, 2.22, 3.33, 4.44, 5.55}}, {"xyz", new List<double> {9.9}}});
			yield return new TestCaseData(3, "efg", 9.9,
				new Dictionary<string, List<double>>
				{
					{"one", new List<double> {1.11, 2.22, 3.33, 4.44, 5.55}}, {"two", new List<double> {6.6, 7.7, 8.8}},
					{"three", new List<double> {0.1, 0.2, 0.4, 0.8, 1.6}}, {"efg", new List<double> {9.9}}
				});
			yield return new TestCaseData(1, "one", 9.9,
				new Dictionary<string, List<double>> {{"one", new List<double> {9.9, 1.11, 2.22, 3.33, 4.44, 5.55}}});
		}

		private static List<double> GetList(double start, int count, double increment)
		{
			var list = new List<double>();
			var toAdd = start;

			for (var i = 0; i < count; i++)
			{
				list.Insert(0, toAdd);
				toAdd += increment;
			}
			return list;
		}

		private static void TestAllTimesDictionary(Dictionary<string, List<double>> doubs, TestTimeTracker ttt)
		{
			var message = "";
			foreach (var kvp in doubs)
			{
				message += UnitTestUtil.ListsAreEqual(kvp.Value, ttt.GetAllTimes(kvp.Key), $" for {kvp.Key}");
			}
			Assert.IsTrue(message.IsNullOrEmpty(), message);
		}

		// ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
		private static void TestEstimatedTime(double exp, TestTimeTracker ttt, string sol)
		{
			var act = ttt.GetEstimatedTime(sol);
			Assert.IsTrue(Math.Abs(exp - act) < .00000001, $"{sol} expected {exp} but got {act}");
		}

		#endregion Helper Methods

		#region Inner Classes

		/// <inheritdoc />
		public class FilesStub : BaseFileStub
		{
			#region Constants

			private const string On = @"C:\ProgramData\Hub\TestTimes\one.txt";
			private const string Tw = @"C:\ProgramData\Hub\TestTimes\two.txt";
			private const string Th = @"C:\ProgramData\Hub\TestTimes\three.txt";

			#endregion Constants

			#region Variable

			internal readonly List<string> CreatedFiles = new List<string>();
			private readonly int _which;

			#endregion Variable

			#region Implementation of IFiles

			/// <inheritdoc />
			public override bool Exists(string path)
			{
				switch (path)
				{
					case On:
						return _which == 1 || _which == 3;
					case Tw:
					case Th:
						return _which == 2 || _which == 3;
					default:
						return false;
				}
			}

			/// <inheritdoc />
			public override IEnumerable<string> ReadLines(string path)
			{
				switch (path)
				{
					case On:
						return new List<string> {"1.11", "2.22", "3.33", "4.44", "5.55"};
					case Tw:
						return new List<string> {"6.6", "7.7", "8.8"};
					case Th:
						return new List<string> {"0.1", "0.2", "0.4", "0.8", "1.6"};
					default:
						return new List<string>();
				}
			}

			/// <inheritdoc />
			public override void WriteAllLines(string path, IEnumerable<string> lines) => CreatedFiles.Add(path);

			#endregion Implementation of IFiles

			#region Constructor

			///
			public FilesStub(int which = 0) => _which = which;

			#endregion Constructor
		}

		/// <inheritdoc />
		private class DirsStub : BaseDirsStub
		{
			#region Variables

			internal readonly List<string> CreatedDirs = new List<string>();
			private readonly bool _exists;

			#endregion Variables

			#region Implementation of IDirectories

			/// <inheritdoc />
			public override void CreateDirectory(string path) => CreatedDirs.Add(path);

			/// <inheritdoc />
			public override bool Exists(string path) => _exists;

			#endregion Implementation of IDirectories

			#region Constructor

			///
			public DirsStub(bool exists = true) => _exists = exists;

			#endregion Constructor
		}

		#endregion Inner Classes
	}
}
