﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

using Hub;

using NUnit.Framework;

using Hub.Commands;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="ReconCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ReconCommandTests
	{
		#region Tests

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, FilesStub>(new FilesStub());
		}

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Description" />, <see cref="ReconCommand.NeedShortcut" />,
		/// <see cref="ReconCommand.Inputs" /> and <see cref="ReconCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new ReconCommand(null, null), new List<string> {"reconfigure", "xactimate"},
				false, false,
				new List<string> {"r", "recon"},
				new List<string>
				{
					"launch",
					"reconfigures",
					"registry",
					@"'c:\bat\systemsetup\'",
					"re-add",
					"keys",
					"xactimate",
					"trickler",
					"run",
					"file name"
				});
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with no parameters passed in
		/// </summary>
		[Test]
		public void TestPerformNoParams()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ReconCommand(null, null);
			cmd.Perform(new List<string>());
			Assert.IsTrue(prog.Launched);
			Assert.AreEqual(0, prog.Paths.Count);

			Assert.AreEqual(null, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with one invalid registry file passed in
		/// </summary>
		[Test]
		public void TestPerformOneFileInvalid()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ReconCommand(null, null);
			cmd.Perform(new List<string> {"bad"});
			Assert.IsTrue(prog.Launched);
			Assert.AreEqual(0, prog.Paths.Count);

			Assert.AreEqual("c:\\bat\\SystemSetup\\bad is not a valid file\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with one registry file passed in
		/// </summary>
		[Test]
		public void TestPerformOneFile()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ReconCommand(null, null);
			cmd.Perform(new List<string> {"file1"});
			Assert.IsTrue(prog.Launched);
			Assert.AreEqual(1, prog.Paths.Count);
			Assert.AreEqual("c:\\bat\\SystemSetup\\file1", prog.Paths[0]);

			Assert.AreEqual("adding keys found in file1\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with two registry files passed in
		/// </summary>
		[Test]
		public void TestPerformTwoFiles()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ReconCommand(null, null);
			cmd.Perform(new List<string> {"file1", "file2"});
			Assert.IsTrue(prog.Launched);
			var expected = new List<string> {"c:\\bat\\SystemSetup\\file1", "c:\\bat\\SystemSetup\\file2"};
			UnitTestUtil.TestListsAreEqual(expected, prog.Paths);

			Assert.AreEqual("adding keys found in file1\r\nadding keys found in file2\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with the launch xm8 flag passed in
		/// </summary>
		[Test]
		public void TestPerformXm8()
		{
			var xm8 = false;
			void Xm8() => xm8 = true;

			void Test()
			{
				var prog = new ProgStub();
				IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
				var cmd = new ReconCommand(Xm8, null);
				cmd.Perform(new List<string> {"-x"});
				Assert.IsTrue(prog.Launched);
				Assert.AreEqual(0, prog.Paths.Count);
				Assert.IsTrue(xm8);
			}

			UnitTestUtil.TestConsoleOutput(Test, "");
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with the launch trickler flag passed in
		/// </summary>
		[Test]
		public void TestPerformTrick()
		{
			var trk = false;

			void Trk() => trk = true;

			void Test()
			{
				var prog = new ProgStub();
				IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
				var cmd = new ReconCommand(null, Trk);
				cmd.Perform(new List<string> {"-tr"});
				Assert.IsTrue(prog.Launched);
				Assert.AreEqual(0, prog.Paths.Count);
				Assert.IsTrue(trk);
			}

			UnitTestUtil.TestConsoleOutput(Test, "");
		}

		/// <summary>
		/// tests <see cref="ReconCommand.Perform" /> with two registry files, one bad file and both launch flags passed in
		/// </summary>
		[Test]
		public void TestPerformAll()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var xm8 = false;
			var trk = false;

			void Xm8() => xm8 = true;

			void Trk() => trk = true;

			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			var cmd = new ReconCommand(Xm8, Trk);
			cmd.Perform(new List<string> {"-x", "-tr", "bad", "file1", "file2"});
			Assert.IsTrue(prog.Launched);
			var expected = new List<string> {"c:\\bat\\SystemSetup\\file1", "c:\\bat\\SystemSetup\\file2"};
			UnitTestUtil.TestListsAreEqual(expected, prog.Paths);
			Assert.IsTrue(xm8);
			Assert.IsTrue(trk);

			var output =
				"c:\\bat\\SystemSetup\\bad is not a valid file\r\n" +
				"adding keys found in file1\r\nadding keys found in file2\r\n";

			Assert.AreEqual(output, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Classes

		private class ProgStub : BaseProgStub
		{
			#region Data Members

			internal bool Launched;
			internal readonly List<string> Paths = new List<string>();

			#endregion Data Members

			#region Implementation of IProgram

			/// <inheritdoc />
			public override void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true,
				ShowOutput show = ShowOutput.No)
			{
				Launched = filename == "x.exe" && label == "Reconfigure" && arguments == "-reconfigure";
			}

			/// <inheritdoc />
			public override bool RunProgram(string path, string arguments = null, ShowOutput show = ShowOutput.No, bool rso = true,
				bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				if (path == "regedit.exe" && !string.IsNullOrEmpty(arguments))
					Paths.Add(arguments.Substring(3));
				return true;
			}

			#endregion Implementation of IProgram
		}

		private class FilesStub : BaseFileStub
		{
			#region Implementation of IFiles

			/// <inheritdoc />
			public override bool Exists(string path) => !path.Contains("bad");

			#endregion Implementation of IFiles
		}

		#endregion Inner Classes
	}
}
