﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Interface;
using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="OrphanCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class OrphanCommandTests
	{
		#region Variable

		private static string _repo;

		#endregion Variable

		#region Tests

		///
		[OneTimeSetUp]
		public static void ClassSetup()
		{
			Env.SetEnvironmentVariable("local", "", true);
			Env.SetEnvironmentVariable("version_tree", "", true);
		}

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, StubDirs>(new StubDirs());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, StubGit>(new StubGit());
		}

		///
		[TearDown]
		public void Teardown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IGit>();
		}

		/// <summary>
		/// tests <see cref="OrphanCommand.Description" />, <see cref="OrphanCommand.NeedShortcut" />,
		///  <see cref="OrphanCommand.Inputs" /> and <see cref="OrphanCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new OrphanCommand(), new List<string> {"find", "orphans"}, false, false,
				new List<string> {"or", "orphan"}, new List<string> {"finds", "lists", "orphan", "branches", "xm8", "data"});
		}

		/// <summary>
		/// tests <see cref="AutoLogCommand.Perform" /> with no repo specified
		/// </summary>
		[Test]
		public void TestPerformNoRepo()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new OrphanCommand();
			cmd.Perform(new List<string>());

			Assert.AreEqual("You need to specify a repo\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="AutoLogCommand.Perform" /> specifying the xm8 repo
		/// </summary>
		[Test]
		public void TestPerformXm8()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new OrphanCommand();
			cmd.Perform(new List<string> { "-x" });

			Assert.AreEqual("Xm8\r\na\r\nb\r\n\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="AutoLogCommand.Perform" /> specifying the data repo
		/// </summary>
		[Test]
		public void TestPerformData()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new OrphanCommand();
			cmd.Perform(new List<string> {"-dt"});

			Assert.AreEqual("No orphans found for the Data repo\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="AutoLogCommand.Perform" /> specifying both repos
		/// </summary>
		[Test]
		public void TestPerformBoth()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var cmd = new OrphanCommand();
			cmd.Perform(new List<string> {"-x", "-dt"});

			Assert.AreEqual("Xm8\r\na\r\nb\r\n\r\nNo orphans found for the Data repo\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Inner Classes

		private class StubGit : BaseGitStub
		{
			#region Implementation of IGit

			/// <inheritdoc />
			public override List<string> GetBranches(bool remote)
			{
				return _repo == $"\\..\\{Env.Branch}"
					? (remote ? new List<string> {"c", "d", "e"} : new List<string> {"a", "b", "c"})
					: (remote ? new List<string> {"a", "b", "c", "d", "e"} : new List<string> {"a", "b", "c"});
			}

			#endregion Implementation of IGit
		}

		/// <summary>
		/// used for <see cref="Utils" />
		/// </summary>
		private class StubDirs : BaseDirsStub
		{
			#region Implementation of IDirectories

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path) => _repo = path;

			#endregion Implementation of IDirectories
		}

		#endregion Inner Classes
	}
}
