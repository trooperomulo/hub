﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands.Param;
using Hub.Interface;

using Moq;

using NUnit.Framework;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamDCommand"/>
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamDCommandTests
	{
		#region Tests
		
		/// <summary>
		/// tests <see cref="ParamDCommand.Description" />, <see cref="ParamDCommand.Input" />
		/// and <see cref="ParamDCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestParamCommand(new ParamDCommand(), new List<string> {"deletes", "preference"}, "-d",
				new List<string> {"stop", "setting", "parameter", "unset", " is ", "multiple"});

		}	// end TestProperties
		
		/// <summary>
		/// tests <see cref="ParamDCommand.Perform" />
		/// </summary>
		[TestCaseSource(nameof(GetPerformTestData))]
		public void TestPerform(int count)
		{
			var args = RandomData.GetRandomListStringSetLength(count);

			var user = new Mock<IUserParams>();
			user.Setup(up => up.Save());
			user.Setup(up => up.UnsetProperty(It.IsAny<string>()));

			IoCContainer.Instance.RegisterAsSingleInstance<IUserParams, IUserParams>(user.Object);

			var prm = new ParamDCommand();
			prm.Perform(args);

			var willRun = count > 0;
			user.Verify(up => up.Save(), willRun ? Times.Once() : Times.Never());

			if (willRun)
			{
				VerifyArgumentsAreAdded(user, args);
			}	// end if
			else
			{
				user.Verify(up => up.UnsetProperty(It.IsAny<string>()), Times.Never);
			}	// end else

			IoCContainer.Instance.RemoveRegisteredType<IUserParams>();

		}	// end TestPerform()

		#endregion Tests

		#region Helpers

		private static IEnumerable GetPerformTestData()
		{
			for (var i = 0; i < 11; i++)
			{
				yield return new TestCaseData(i);
			}	// end for

		}	// end GetPerformTestData()

		private void VerifyArgumentsAreAdded(Mock<IUserParams> up, List<string> args)
		{
			for (var i = 0; i < args.Count; i++)
			{
				up.Verify(u => u.UnsetProperty(args[i]), Times.Once);
			}	// end for

		}	// end VerifyArgumentsAreAdded(Mock<IUserParams>, List<string>)

		#endregion Helpers

	}	// end ParamDCommandTests

}	// end HubUnitTest.Commands.Param
