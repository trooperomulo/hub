﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands.Param;
using Hub.Interface;

using Moq;

using NUnit.Framework;

using UTU = HubUnitTest.UnitTestUtil;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamACommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamACommandTests
	{
		#region Tests
		
		/// <summary>
		/// tests <see cref="ParamACommand.Description" />, <see cref="ParamACommand.Input" />
		/// and <see cref="ParamACommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UTU.TestParamCommand(new ParamACommand(), new List<string> {"add", "preference"}, "-a",
				new List<string> {"start", "setting", "parameter", "value", "isn't", "multiple"});

		}	// end TestProperties

		/// <summary>
		/// tests <see cref="ParamACommand.Perform" />
		/// </summary>
		[TestCaseSource(nameof(GetPerformTestData))]
		public void TestPerform(int count)
		{
			var args = RandomData.GetRandomListStringSetLength(count);

			var user = new Mock<IUserParams>();
			user.Setup(up => up.Save());
			user.Setup(up => up.SetProperty(It.IsAny<string>(), It.IsAny<string>(), false));

			IoCContainer.Instance.RegisterAsSingleInstance<IUserParams, IUserParams>(user.Object);

			var prm = new ParamACommand();
			prm.Perform(args);

			var goodPairs = count > 0 && count % 2 == 0;
			user.Verify(up => up.Save(), goodPairs ? Times.Once() : Times.Never());

			if (goodPairs)
			{
				VerifyArgumentsAreAdded(user, args);
			}	// end if
			else
			{
				user.Verify(up => up.SetProperty(It.IsAny<string>(), It.IsAny<string>(), false), Times.Never);
			}	// end else

			IoCContainer.Instance.RemoveRegisteredType<IUserParams>();

		}	// end TestPerform()

		#endregion Tests

		#region Helpers

		private static IEnumerable GetPerformTestData()
		{
			for (var i = 0; i < 11; i++)
			{
				yield return new TestCaseData(i);
			}	// end for

		}	// end GetPerformTestData()

		private void VerifyArgumentsAreAdded(Mock<IUserParams> up, List<string> args)
		{
			for (var i = 0; i < args.Count; i += 2)
			{
				// ReSharper disable AccessToModifiedClosure
				up.Verify(u => u.SetProperty(args[i], args[i + 1], false), Times.Once);
				// ReSharper restore AccessToModifiedClosure
			}	// end for

		}	// end VerifyArgumentsAreAdded(Mock<IUserParams>, List<string>)

		#endregion Helpers
		
	}	// end ParamACommandTests

}	// end HubUnitTest.Commands.Param
