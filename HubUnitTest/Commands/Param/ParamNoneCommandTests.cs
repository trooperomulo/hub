﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands.Param;
using Hub.Commands.Resx;
using Hub.Decider.Param;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamNoneCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamNoneCommandTests
	{
		#region Variable

		private readonly List<string> _helps = new List<string>
		{
			"used",
			"perform",
			"various",
			"aaa",
			"bbb",
			"-a/-b",
			// these are printed by NoneCommand
			"honored",
			"printed",
			"no action",
			"specified"
		};

		#endregion Variable

		#region Tests

		/// <summary>
		/// tests <see cref="ParamNoneCommand.Description" />, <see cref="ParamNoneCommand.Input" />,
		/// <see cref="ParamNoneCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			ParamDecider.InitCommands(new List<ParamBase> {new ParamA(), new ParamB()}, null, null);
			UnitTestUtil.TestParamCommand(new ParamNoneCommand(), new List<string>(), "", _helps);

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="ResxNoneCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			ParamDecider.InitCommands(new List<ParamBase> {new ParamA(), new ParamB()}, null, null);
			var cmd = new ParamNoneCommand();
			cmd.Perform(null);

			var helps = new List<string> {"you did not specify a valid parameter action to do", "ccc", "ddd"};
			helps.AddRange(_helps);

			UnitTestUtil.TestStringContainsItems(cons.Output.ToLower(), helps);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestPerform()

		#endregion Tests

		#region Inner Classes

		private class ParamA : ParamBase
		{
			#region Overrides of ParamBase

			/// <inheritdoc />
			public override string Description => "aaa";

			/// <inheritdoc />
			public override string Input => "-a";

			/// <inheritdoc />
			public override void Help() => Out.Line("ccc");

			/// <inheritdoc />
			public override void Perform(List<string> args) { }

			#endregion Overrides of ParamBase

		}	// end ParamA

		private class ParamB : ParamBase
		{
			#region Overrides of ParamBase

			/// <inheritdoc />
			public override string Description => "bbb";

			/// <inheritdoc />
			public override string Input => "-b";

			/// <inheritdoc />
			public override void Help() => Out.Line("ddd");

			/// <inheritdoc />
			public override void Perform(List<string> args) { }

			#endregion Overrides of ParamBase

		}	// end ParamB

		#endregion Inner Classes

	}	// end ParamNoneCommandTests

}	// end HubUnitTest.Commands.Param
