﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands.Param;
using Hub.Interface;

using Moq;

using NUnit.Framework;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamECommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamECommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="ParamECommand.Description" />, <see cref="ParamECommand.Input" />
		/// and <see cref="ParamECommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestParamCommand(new ParamECommand(), new List<string> { "explains", "use", "parameter" }, "-e",
				new List<string> { "purpose", "parameter", " multi-select" });

		}   // end TestProperties

		#endregion Tests

	}   // end ParamECommandTests

}	// end HubUnitTest.Commands.Param
