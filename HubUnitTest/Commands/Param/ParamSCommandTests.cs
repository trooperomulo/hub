﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Commands.Param;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamSCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamSCommandTests
	{
		#region Tests
		
		/// <summary>
		/// tests <see cref="ParamSCommand.Description" />, <see cref="ParamSCommand.Input" />
		/// and <see cref="ParamSCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestParamCommand(new ParamSCommand(),
				new List<string> {"shows", "parameters", "are set", "aren't"}, "-s",
				new List<string> {"shows", "displays", "parameters", "all", "indicated", "have been set"});

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="ParamSCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var stub = new UserParamsNoWrapStub();
			var cons = new ConsoleStub(90);
			IoCContainer.Instance.RegisterAsSingleInstance<IUserParams, UserParamsNoWrapStub>(stub);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsoleStub>(cons);

			var prm = new ParamSCommand();
			prm.Perform(null);

			var expected = stub.GetOutput();
			var actual = cons.Output;

			Assert.AreEqual(expected, actual);

			IoCContainer.Instance.RemoveRegisteredType<IUserParams>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			
		}   // end TestPerform()

		/// <summary>
		/// verifies that we are wrapping properly
		/// </summary>
		[Test]
		public void TestWrapping()
		{
			var stub = new UserParamsWrapStub();
			var cons = new ConsoleStub(90);
			IoCContainer.Instance.RegisterAsSingleInstance<IUserParams, UserParamsWrapStub>(stub);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsoleStub>(cons);

			var prm = new ParamSCommand();
			prm.Perform(null);

			var expected =
				"\r\nUSER PARAMETERS\r\n\r\n Property                | Set | Current                                     | Default   \r\n-------------------------|-----|---------------------------------------------|-----------\r\n Short                   | [X] | A short value                               | Default 1 \r\n A Medium One            | [X] | A medium length value instead of short      | Default 2 \r\n The Longest Name of all | [X] | The value is really long.  I mean it is sup | Default 3 \r\n                         |     | er long.  The value is so long that it is g |           \r\n                         |     | uaranteed to need to be wrapped, at least o |           \r\n                         |     | nce.  Hopefully its so long that it needs t |           \r\n                         |     | o be wrapped multiple times.                |           \r\n";
			var actual = cons.Output;
			
			Assert.AreEqual(expected, actual);

			IoCContainer.Instance.RemoveRegisteredType<IUserParams>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}
		
		#endregion Tests

		#region Inner Classes

		private class UserParamsNoWrapStub : BaseUserParamsStub
		{
			#region Variable

			private Dictionary<string, (string cur, string def)> _values;

			#endregion Variable

			#region Implementation of IUserParams
			
			/// <inheritdoc />
			public override string GetDefaultValue(string property) => _values[property].def;

			/// <inheritdoc />
			public override List<string> GetProperties()
			{
				var list = _values.Keys.ToList();
				list.Sort();
				return list;

			}	// end GetProperties()

			/// <inheritdoc />
			public override string GetPropertyValue(string property) => _values[property].cur;

			/// <inheritdoc />
			public override bool HasPropertyBeenSet(string property)
			{
				var propValues = _values[property];
				return propValues.cur != propValues.def;

			}	// end HasPropertyBeenSet(string)
			
			#endregion Implementation of IUserParams

			#region Constructor

			internal UserParamsNoWrapStub()
			{
				_values = new Dictionary<string, (string cur, string def)>();
				FillDictionary();

			}	// end UserParamsNoWrapStub()

			#endregion Constructor
			
			#region Methods

			private void FillDictionary()
			{
				var count = Rnd.Int(10, 20);

				for (var i = 0; i < count; i++)
				{
					AddEntry();
				}	// end for

			}	// end FillDictionary()

			private void AddEntry()
			{
				var property = GetString();
				var cur = GetCurrent();
				var def = GetDefault(cur);

				_values.Add(property, (cur, def));

			}	// end AddEntry()

			private static string GetCurrent()
			{
				switch (Rnd.Int(5))
				{
					// 20% of the time we return null
					case 0:
						return null;
					// 20% of the time we return an empty string
					case 1:
						return "";
					// 60% of the time we return a word-ish
					default:
						return GetString();

				}	// end switch

			}	// end GetCurrent()

			private static string GetDefault(string cur)
			{
				switch (Rnd.Int(10))
				{
					// 20% of the time we return null
					case 0:
					case 1:
						return null;
					// 20% of the time we return an empty string
					case 2:
					case 3:
						return "";
					// 30% of the time we return 
					case 4:
					case 5:
					case 6:
						return cur;
					// 30% of the time we return a word-ish
					default:
						return GetString();

				}	// end switch

			}	// end GetDefault(string)

			private static string GetString() => Rnd.GetRandomStrSetLength(Rnd.Int(5, 15));

			public string GetOutput()
			{
				GetMaxLengths(out var maxP, out var maxC, out var maxD);
				var header = GetHeader(maxP, maxC, maxD);

				var nl = Environment.NewLine;
				var output = $"{nl}USER PARAMETERS{nl}{nl}{header}{nl}";

				var props = GetProperties();

				foreach (var prop in props)
				{
					var values = _values[prop];
					output += $"{GetLine(prop, maxP, values.cur, maxC, values.def, maxD)}{nl}";
				}	// end foreach

				return output;

			}	// end GetOutput()

			#region Outputters

			private static string Fill(int len) => Prep.FillOut("", len, '-');

			private static string Fill(string str, int len) => Prep.FillOut($" {str}", len);

			private static string GetHeader(int propLen, int curLen, int defLen)
			{
				var words = GetLine(Fill("Property", propLen), " Set ", Fill("Current", curLen), Fill("Default", defLen));
				var lines = GetLine(Fill(propLen), Fill(5), Fill(curLen), Fill(defLen));
				return $"{words}{Environment.NewLine}{lines}";

			}	// end GetHeader(int, int, int)

			private static string GetLine(string prop, string set, string cur, string def) => $"{prop}|{set}|{cur}|{def}";

			private static string GetLine(string prop, int pLen, string cur, int cLen, string def, int dLen) =>
				GetLine(Fill(prop, pLen), Set(cur, def), Fill(Safe(cur), cLen), Fill(Safe(def), dLen));

			private void GetMaxLengths(out int maxP, out int maxC, out int maxD)
			{
				maxP = maxC = maxD = 0;

				foreach (var kvp in _values)
				{
					var prop = kvp.Key;
					var cur = kvp.Value.cur;
					var def = kvp.Value.def;

					if (prop?.Length > maxP)
					{
						maxP = prop.Length;
					}	// end if

					if (cur?.Length > maxC)
					{
						maxC = cur.Length;
					}	// end if

					if (def?.Length > maxD)
					{
						maxD = def.Length;
					}	// end if

				}	// end foreach

				maxP += 2;
				maxC += 2;
				maxD += 2;

			}	// end GetMaxLengths(out int, out int, out int)

			private static string Safe(string s)
			{
				if (s == null)
				{
					return "{null}";
				}	// end if

				if (s.IsNullOrEmpty())
				{
					return "\" \"";
				}	// end if

				return s;

			}	// end Safe(string)

			private static string Set(string cur, string def) => $" [{(cur != def ? "X" : " ")}] ";

			#endregion Outputters

			#endregion Methods
		}

		private class UserParamsWrapStub : BaseUserParamsStub
		{
			#region Constants

			public const string Prop1 = "Short";
			public const string Prop2 = "A Medium One";
			public const string Prop3 = "The Longest Name of all";

			#endregion Constants

			#region Implementation of IUserParams
			
			public override string GetDefaultValue(string property)
			{
				switch (property)
				{
					case Prop1:
						return "Default 1";
					case Prop2:
						return "Default 2";
					case Prop3:
						return "Default 3";
					default:
						return "";
				}
			}

			public override List<string> GetProperties() => new List<string> { Prop1, Prop2, Prop3 };

			public override string GetPropertyValue(string property)
			{
				switch (property)
				{
					case Prop1:
						return "A short value";
					case Prop2:
						return "A medium length value instead of short";
					case Prop3:
						return
							"The value is really long.  I mean it is super long.  The value is so long that it is guaranteed to need to be wrapped, at least once.  Hopefully its so long that it needs to be wrapped multiple times.";
					default:
						return "";
				}
			}
			
			public override bool HasPropertyBeenSet(string property) => true;

			#endregion Implementation of IUserParams
		}

		private class ConsoleStub : BaseConsStub
		{
			private readonly int _width;

			///
			public ConsoleStub(int width)
			{
				_width = width;
			}

			#region Overrides of BaseConsStub

			// ReSharper disable once ConvertToAutoProperty
			public override int Width => _width;

			#endregion
		}

		#endregion Inner Classes

	}

}
