﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub.Commands.Param;

using NUnit.Framework;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamCommandTests
	{
		#region Variables

		private static string _cmd;
		private static string _output;

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests <see cref="ParamCommand.Description" />, <see cref="ParamCommand.NeedShortcut" />,
		/// <see cref="ParamCommand.Inputs" /> and <see cref="ParamCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(GetInstance(), Ls("modify", "user", "parameters", "file"), true, false,
				Ls("p", "prm"), null);

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="ParamCommand.Help" />
		/// </summary>
		[TestCase("a", "rdt", "abc", "A was called")]
		[TestCase("b", "lmt", "ajt", "B was called")]
		[TestCase("lrt", "kit", "sgt", "Def was called")]

		public void TestHelp(string a, string b, string c, string expected)
		{
			var cmd = GetInstance();
			cmd.Help(Ls(a, b, c));
			Assert.AreEqual(expected, _output);

		}	// end TestHelp(string, string, string, string)
		
		/// <summary>
		/// tests <see cref="ParamCommand.Perform" />
		/// </summary>
		[TestCase("a", "rdt", "abc", "A")]
		[TestCase("b", "lmt", "ajt", "B")]
		[TestCase("lrt", "kit", "sgt", "Def")]
		public void TestPerform(string a, string b, string c, string expected)
		{
			var cmd = GetInstance();
			cmd.Perform(Ls(a, b, c));
			Assert.AreEqual(expected, _cmd);

		}	// end TestPerform(string, string, string, string)

		#endregion Tests

		#region Helpers

		private static ParamCommand GetInstance() =>
			new ParamCommand(new List<ParamBase> {new ParamAStub(), new ParamBStub()}, new ParamDefStub());
		
		private static List<string> Ls(params string[] args) => args.ToList();

		#endregion Helpers

		#region Inner Classes

		private class ParamAStub : ParamBase
		{
			#region Overrides of ParamBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "a";

			/// <inheritdoc />
			public override void Help() => _output = "A was called";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _cmd = "A";

			#endregion Overrides of ParamBase

		}	// end ParamAStub
		
		private class ParamBStub : ParamBase
		{
			#region Overrides of ParamBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "b";

			/// <inheritdoc />
			public override void Help() => _output = "B was called";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _cmd = "B";

			#endregion Overrides of ParamBase

		}	// end ParamBStub
		
		private class ParamDefStub : ParamBase
		{
			#region Overrides of ParamBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "";

			/// <inheritdoc />
			public override void Help() => _output = "Def was called";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _cmd = "Def";

			#endregion Overrides of ParamBase

		}	// end ParamDefStub

		#endregion Inner Classes

	}	// end ParamCommandTests

}	// end HubUnitTest.Commands.Param
