﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands.Param;
using Hub.Interface;

using Moq;

using NUnit.Framework;

namespace HubUnitTest.Commands.Param
{
	/// <summary>
	/// tests <see cref="ParamMCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamMCommandTests
	{
		#region Tests
		
		/// <summary>
		/// tests <see cref="ParamMCommand.Description" />, <see cref="ParamMCommand.Input" />
		/// and <see cref="ParamMCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestParamCommand(new ParamMCommand(), new List<string> {"modifies", "preference"}, "-m",
				new List<string> {"already", "parameter", " set ", "changed", "multiple"});

		}	// end TestProperties
		
		/// <summary>
		/// tests <see cref="ParamMCommand.Perform" />
		/// </summary>
		[TestCaseSource(nameof(GetPerformTestData))]
		public void TestPerform(int count)
		{
			var args = RandomData.GetRandomListStringSetLength(count);

			var user = new Mock<IUserParams>();
			user.Setup(up => up.Save());
			user.Setup(up => up.ModifyProperty(It.IsAny<string>(), It.IsAny<string>()));

			IoCContainer.Instance.RegisterAsSingleInstance<IUserParams, IUserParams>(user.Object);

			var prm = new ParamMCommand();
			prm.Perform(args);
			
			var goodPairs = count > 0 && count % 2 == 0;
			user.Verify(up => up.Save(), goodPairs ? Times.Once() : Times.Never());

			if (goodPairs)
			{
				VerifyArgumentsAreAdded(user, args);
			}	// end if
			else
			{
				user.Verify(up => up.UnsetProperty(It.IsAny<string>()), Times.Never);
			}	// end else

			IoCContainer.Instance.RemoveRegisteredType<IUserParams>();

		}	// end TestPerform()

		#endregion Tests

		#region Helpers

		private static IEnumerable GetPerformTestData()
		{
			for (var i = 0; i < 11; i++)
			{
				yield return new TestCaseData(i);
			}	// end for

		}	// end GetPerformTestData()

		private void VerifyArgumentsAreAdded(Mock<IUserParams> up, List<string> args)
		{
			for (var i = 0; i < args.Count; i += 2)
			{
				// ReSharper disable AccessToModifiedClosure
				up.Verify(u => u.ModifyProperty(args[i], args[i + 1]), Times.Once);
				// ReSharper restore AccessToModifiedClosure
			}	// end for

		}	// end VerifyArgumentsAreAdded(Mock<IUserParams>, List<string>)

		#endregion Helpers

	}	// end ParamMCommandTests

}	// end HubUnitTest.Commands.Param
