﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using RyanCoreTests;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="TeamCityCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TeamCityCommandTests
	{
		#region Constants

		private const string ExpectedBase = "https://teamcity.xactware.com/project.html?projectId={0}&branch_{0}={1}";
		private const string Win = "-w";
		private const string And = "-a";
		private const string Ios = "-i";

		#endregion Constants

		#region Tests

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IGit>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();

		}	// end TearDown()

		///
		[SetUp]
		public void PreTest()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(new Mock<IConsole>().Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(new Mock<IWaitForUser>().Object);

		}	// end PreTest()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Description" />, <see cref="TeamCityCommand.NeedShortcut" />,
		/// <see cref="TeamCityCommand.Inputs" /> and <see cref="TeamCityCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new TeamCityCommand(), new List<string> {"display", "branch", "team city"},
				false, false, new List<string> {"tc", "teamc"},
				new List<string> {"display", "builds", "branch(es)", "fifo", "windows", "android", "ios", "team city", "current"});

		}	// end TestProperties()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with main as the branch
		/// </summary>
		[Test]
		public void TestPerformMain()
		{
			var prog = new ProgStub();
			const string branch = "main";
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub(branch));
			var cmd = new TeamCityCommand();

			var platform = GetPlatform();
			var args = GetArgs(platform);
			var expected = GetExpected(platform, branch);

			cmd.Perform(args);

			CoreTestUtils.TestListsAreEqual(expected, prog.Processes);

		}	// end TestPerformMain()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with no branch passed in
		/// </summary>
		[Test]
		public void TestPerformNoBranch()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub());
			var cmd = new TeamCityCommand();
			
			var platform = GetPlatform();
			var args = GetArgs(platform);
			var expected = GetExpected(platform, "from_git");

			cmd.Perform(args);

			CoreTestUtils.TestListsAreEqual(expected, prog.Processes);

		}	// end TestPerformNoBranch()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with one branch passed in
		/// </summary>
		[Test]
		public void TestPerformOneBranch()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub());

			var cmd = new TeamCityCommand();
			const string branch = "abc";
			
			var platform = GetPlatform();
			var args = GetArgs(platform, branch);

			var expected = GetExpected(platform, branch);

			cmd.Perform(args);

			CoreTestUtils.TestListsAreEqual(expected, prog.Processes);

		}	// end TestPerformOneBranch()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with two branches passed in
		/// </summary>
		[Test]
		public void TestPerformTwoBranches()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub());
			var cmd = new TeamCityCommand();
			var branches = new List<string> {"abc", "def"};
			
			var platform = GetPlatform();
			var args = GetArgs(platform, branches);
			var expected = GetExpected(platform, branches);

			cmd.Perform(args);

			CoreTestUtils.TestListsAreEqual(expected, prog.Processes);

		}	// end TestPerformTwoBranches()
		
		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with no platforms passed in or selected
		/// </summary>
		[Test]
		public void TestPerformNoPlatforms()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub());

			var wait = new Mock<IWaitForUser>();
			wait.Setup(w => w.MultipleSelectionEnterNotRequired(Ls, Ls, S, S, S)).Returns(new List<int>());

			var cmd = new TeamCityCommand();

			cmd.Perform(new List<string>());

			CoreTestUtils.TestListsAreEqual(new List<string>(), prog.Processes);

		}	// end TestPerformNoPlatforms()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with no platforms passed in, but some selected
		/// </summary>
		[Test]
		public void TestPerformPlatformsSelected()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub());

			var platform = GetPlatform();
			var selected = GetSelected(platform);
			var expected = GetExpected(platform, "from_git");

			var wait = new Mock<IWaitForUser>();
			wait.Setup(w => w.MultipleSelectionEnterNotRequired(Ls, Ls, S, S, S)).Returns(selected);

			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(wait.Object);

			var cmd = new TeamCityCommand();

			cmd.Perform(new List<string>());

			CoreTestUtils.TestListsAreEqual(expected, prog.Processes);

		}	// end TestPerformPlatformsSelected()

		/// <summary>
		/// tests <see cref="TeamCityCommand.Perform" /> with all platforms passed in
		/// </summary>
		[Test]
		public void TestPerformAllPlatformsOneBranch()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, GitStub>(new GitStub());

			var cmd = new TeamCityCommand();
			const string branch = "abc";

			var platform = (Platform) 7;
			var args = GetArgs(platform, branch);

			var expected = GetExpected(platform, branch);

			cmd.Perform(args);

			CoreTestUtils.TestListsAreEqual(expected, prog.Processes);

		}	// end TestPerformAllPlatformsOneBranch()

		#endregion Tests

		#region Helpers

		private static string S => It.IsAny<string>();

		private static List<string> Ls => It.IsAny<List<string>>();
		
		private static List<string> GetArgs(Platform platform) => GetArgs(platform, new List<string> ());

		private static List<string> GetArgs(Platform platform, string branch) => GetArgs(platform, new List<string> {branch});

		private static List<string> GetArgs(Platform platform, List<string> branches)
		{
			var args = new List<string>();

			if (platform.HasFlag(Platform.Windows))
			{
				args.Add(Win);
			}	// end if
			
			if (platform.HasFlag(Platform.Android))
			{
				args.Add(And);
			}	// end if

			if (platform.HasFlag(Platform.iOS))
			{
				args.Add(Ios);
			}	// end if

			args.AddRange(branches);

			return args;

		}	// end GetArgs(Platform, List<string>)
		
		private static List<string> GetExpected(Platform platform, string branch) => GetExpected(platform, new List<string> {branch});

		private static List<string> GetExpected(Platform platform, List<string> branches)
		{
			var expected = new List<string>();

			foreach (var br in branches)
			{
				if (platform.HasFlag(Platform.Windows))
				{
					expected.Add(string.Format(ExpectedBase, "Xactimate", br));
				}	// end if
			
				if (platform.HasFlag(Platform.Android))
				{
					expected.Add(string.Format(ExpectedBase, "ClaimsSolutionsAndroid", br));
				}	// end if
				
				if (platform.HasFlag(Platform.iOS))
				{
					expected.Add(string.Format(ExpectedBase, "ClaimsSolutionsIOS", br));
				}	// end if

			}	// end foreach

			return expected;

		}	// end GetExpected(Platform, List<string>)

		private static Platform GetPlatform() => (Platform) Rnd.Int(1, 8);

		private static List<int> GetSelected(Platform platform)
		{
			var selected = new List<int>();

			if (platform.HasFlag(Platform.Windows))
			{
				selected.Add(0);
			}	// end if
			
			if (platform.HasFlag(Platform.Android))
			{
				selected.Add(1);
			}	// end if
			
			if (platform.HasFlag(Platform.iOS))
			{
				selected.Add(2);
			}	// end if

			return selected;

		}	// end GetSelected(Platform)

		#endregion Helpers

		#region Inner Classes

		private class GitStub : BaseGitStub
		{
			#region Variable

			private readonly string _from;

			#endregion Variable

			#region Implementation of IGit

			/// <inheritdoc />
			public override string GetBranchName() => _from;

			#endregion Implementation of IGit

			#region Constructor

			public GitStub(string from = "from_git") => _from = from;

			#endregion Constructor

		}	// end GitStub

		private class ProgStub : BaseProgStub
		{
			#region Variable

			internal readonly List<string> Processes = new List<string>();

			#endregion Variable

			#region Implementation of IProgram

			/// <inheritdoc />
			public override void StartProcess(string filename) => Processes.Add(filename);

			#endregion Implementation of IProgram

		}	// end ProgStub

		#endregion Inner Classes

	}	// end TeamCityCommandTests

}	// end HubUnitTest.Commands
