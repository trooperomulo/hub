﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="CleanCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class CleanCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="CleanCommand.Description" />, <see cref="CleanCommand.NeedShortcut" />, <see cref="CleanCommand.Inputs" />
		/// and <see cref="CleanCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new CleanCommand(), new List<string> {"clean"}, false, false,
				new List<string> {"clean", "c"}, new List<string> {"clean", "suppress", "times", "output", "batch"});
		}

		/// <summary>
		/// tests <see cref="CleanCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			TestPerform(new List<string>(), 2, false);
			TestPerform(new List<string> {"garbo"}, 2, false);
			TestPerform(new List<string> { "-q" }, 2, true);
			TestPerform(new List<string> { "0" }, 2, false);
			TestPerform(new List<string> { "3" }, 3, false);
			TestPerform(new List<string> { "13" }, 2, false);
			TestPerform(new List<string> { "-q", "3" }, 3, true);
			TestPerform(new List<string> { "3", "-q" }, 3, true); // order of the arguments doesn't matter
		}

		#endregion Tests

		#region Helper

		private static void TestPerform(List<string> args, int count, bool quiet)
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());

			var cmd = new CleanCommand();
			cmd.Perform(args);
			Assert.IsTrue(prog.Process.Equals("clean.bat"), "the wrong file was called");
			Assert.AreEqual(quiet, prog.Quiet, "quiet didn't match");
			Assert.AreEqual(count, prog.Count, "clean was run the wrong number of times");
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Helper

		#region Inner Class

		private class ProgStub : BaseProgStub
		{
			#region Variables

			internal string Process;
			internal int Count;
			internal bool Quiet;

			#endregion Variables

			#region Implementation of IProgram

			/// <inheritdoc />
			public override bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true,
				bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
			{
				Process = fileAndArgs;
				Count++;
				Quiet = rso;
				return true;
			}

			/// <inheritdoc />
			public override void StartProcess(string filename) => Process = filename;

			#endregion Implementation of IProgram
		}
		
		#endregion Inner Class
	}
}
