﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="GoCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class GoCommandTests
	{
		#region Tests

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, ShortcutsTests.ShortcutsFilesStub>(new ShortcutsTests.ShortcutsFilesStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, StubDirectory>(new StubDirectory());
			Shortcuts.Instance.LoadShortcuts("real");
		}

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			Shortcuts.Instance.Clear();
		}

		/// <summary>
		/// tests <see cref="GoCommand.Description" />, <see cref="GoCommand.NeedShortcut" />,
		/// <see cref="GoCommand.Inputs" /> and <see cref="GoCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new GoCommand(), new List<string> {"navigate"}, false, true,
				new List<string> {"g", "go"}, new List<string> {"navigate", "solution", "fifo"});
		}

		/// <summary>
		/// tests <see cref="GoCommand.Perform" />
		/// </summary>
		[Test]
		[TestCase(1, "abcdef\r\n")]
		[TestCase(2, "invalid was not found in the shortcuts\r\nabcdef\r\n")]
		[TestCase(3, "path\r\n")]
		[TestCase(4, "invalid was not found in the shortcuts\r\npath\r\n")]
		[TestCase(5, "invalid was not found in the shortcuts\r\npath\r\n")]
		[TestCase(6, "path\r\n")]
		public void TestPerform(int which, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			SetEnvironment();

			List<string> args = null;

			switch (which)
			{
				case 1:
					args = new List<string>();
					break;
				case 2:
					args = new List<string> {"invalid"};
					break;
				case 3:
					args = new List<string> {"just"};
					break;
				case 4:
					args = new List<string> {"just", "invalid"};
					break;
				case 5:
					args = new List<string> { "invalid", "just" };
					break;
				case 6:
					args = new List<string> { "path", "just" };
					break;
			}

			var cmd = new GoCommand();

			cmd.Perform(args);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="GoCommand.DoGo(string, bool)" />
		/// makes sure silent is behaving properly, doesn't worry about current directory
		/// </summary>
		[Test]
		[TestCase(true, null)]
		[TestCase(false, "path\r\n")]
		public void TestNoisyDoGo(bool silent, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			SetEnvironment();
			GoCommand.DoGo("just", silent);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="GoCommand.DoGo(string)" />
		/// makes sure the current directory is set properly, doesn't worry about silent
		/// </summary>
		[Test]
		[TestCase("", "ghi", "abcdef\r\n")]
		[TestCase("just", "ghi", "path\r\n")]
		[TestCase("path", "ghi", "abcdefand\r\n")]
		[TestCase("mode", "ghi", "testghi\r\n")]
		[TestCase("mode", "jkl", "testjkl\r\n")]
		public void TestSilentDoGo(string path, string mode, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			SetEnvironment();
			Env.SetEnvironmentVariable("mode", mode, true);

			GoCommand.DoGo(path, false);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="GoCommand.DoGo(string)" />
		/// </summary>
		[Test]
		public void TestDoGo()
		{
			SetEnvironment();
			UnitTestUtil.TestConsoleOutput(Test, "");

			void Test()
			{
				GoCommand.DoGo("path");
			}
		}

		#endregion Tests

		#region Helper

		private static void SetEnvironment()
		{
			Env.SetEnvironmentVariable("local", "abc", true);
			Env.SetEnvironmentVariable("version_tree", "def", true);
		}

		#endregion Helper

		#region Inner Class

		private class StubDirectory : BaseDirsStub
		{
			#region Implementation of IDirectories

			/// <inheritdoc />
			public override bool Exists(string path) => !"and".Equals(path);

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path) { }

			#endregion Implementation of IDirectories
		}
		
		#endregion Inner Class
	}
}
