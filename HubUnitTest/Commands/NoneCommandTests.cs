﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="NoneCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class NoneCommandTests
	{
		#region Tests

		/// <summary>
		/// tests <see cref="NoneCommand.Description" />, <see cref="NoneCommand.NeedShortcut" />,
		/// <see cref="NoneCommand.Inputs" /> and <see cref="NoneCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			var def = new NoneCommand(null);
			Assert.IsFalse(def.HasSubCommands);
			Assert.IsFalse(def.NeedShortcut);
			Assert.AreEqual("", def.Description);
			Assert.AreEqual(0, def.Inputs.Count);

			void Test()
			{
				def.Help(new List<string> { "cmd" });
			}

			UnitTestUtil.TestConsoleOutput(Test, "");
		}

		/// <summary>
		/// tests <see cref="NoneCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var called = false;

			void Help() => called = true;

			var def = new NoneCommand(Help);

			def.Perform(new List<string> {"cmd"});
			Assert.IsTrue(called);
			Assert.AreEqual(null, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests
	}
}
