﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Commands;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Commands
{
	/// <summary>
	/// tests <see cref="AutoLogCommand" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class AutoLogCommandTests
	{
		#region Tests
		
		/// <summary>
		/// tests <see cref="AutoLogCommand.Description" />, <see cref="AutoLogCommand.NeedShortcut" />,
		/// <see cref="AutoLogCommand.Inputs" /> and <see cref="AutoLogCommand.Help" />
		/// </summary>
		[Test]
		public void TestProperties()
		{
			UnitTestUtil.TestCommand(new AutoLogCommand(), new List<string> {"automation", "log"}, false, false,
				new List<string> {"autolog", "al"}, new List<string> {"windows", "automation", "log"});
		}

		/// <summary>
		/// tests <see cref="AutoLogCommand.Perform" />
		/// </summary>
		[Test]
		public void TestPerform()
		{
			var prog = new ProgStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, ProgStub>(prog);

			var cmd = new AutoLogCommand();
			cmd.Perform(null);
			Assert.IsTrue(prog.Process.Contains(@"automation\bin\Debug\Logging"));
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
		}

		#endregion Tests

		#region Inner Class

		private class ProgStub : BaseProgStub
		{
			#region Variable

			internal string Process;

			#endregion Variable

			#region Implementation of IProgram

			/// <inheritdoc />
			public override void StartProcess(string filename) => Process = filename;

			#endregion Implementation of IProgram
		}

		#endregion Inner Class
	}
}
