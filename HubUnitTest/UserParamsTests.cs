﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Interface;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using RyanCoreTests;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest
{
	/// <summary>
	/// class to test <see cref="UserParams" /> class
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UserParamsTests
	{
		#region Variable

		// holds the properties (by name), their default value and two another values to set them to
		private static Dictionary<string, (string def, string oth, string thr)> _propsAndValues;

		#endregion Variable

		#region Property

		private static Dictionary<string, (string def, string oth, string thr)> PropsAndValues
		{
			get
			{
				if (_propsAndValues == null)
				{
					SetupDictionary();
				}

				return _propsAndValues;
			}
		}

		#endregion Property
		
		#region Tests

		/// <summary>
		/// verify that the correct defaults values are returned
		/// </summary>
		[TestCaseSource(nameof(GetDefaultValueTestData))]
		public void TestGetDefaultValue(string prop, string expected)
		{
			var files = new UserParamsFilesStub();
			files.WriteAllLines("", null);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);
			
			Assert.AreEqual(expected, new UserParams().GetDefaultValue(prop));

		}	// end TestGetDefaultValue(string, string)

		/// <summary>
		/// verify that all the pertinent properties are returned
		/// </summary>
		[Test]
		public void TestGetProperties()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(new UserParamsFilesStub());

			var prms = new UserParams();

			var properties = prms.GetProperties();

			Assert.AreEqual(PropsAndValues.Count + 1, properties.Count, $"{nameof(UserParams)}.{nameof(UserParams.GetProperties)} should return all the properties, plus the separator");

			var keys = PropsAndValues.Keys;
			foreach (var key in keys)
			{
				Assert.IsTrue(properties.Contains(key));
			}

		}	// end TestGetProperties()

		/// <summary>
		/// verify the edge cases when calling <see cref="UserParams.GetPropertyValue" />
		/// </summary>
		[Test]
		public void TestGetPropertyValueEdgeCases()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(new UserParamsFilesStub());

			var prms = new UserParams();

			prms.SetProperty(nameof(prms.ScrumHours), "1", true);
			prms.SetProperty(nameof(prms.ScrumLocation), "scrum path", true);

			// non-property
			Assert.AreEqual("", prms.GetPropertyValue(Rnd.GetRandomStr()));

			// scrum properties (ignored everywhere else
			Assert.AreEqual("1", prms.GetPropertyValue(nameof(prms.ScrumHours)));
			Assert.AreEqual("scrum path", prms.GetPropertyValue(nameof(prms.ScrumLocation)));

		}	// end TestGetPropertyValueEdgeCases()

		/// <summary>
		/// tests <see cref="UserParams.HasPropertyBeenSet" />
		/// </summary>
		[Test]
		public void TestHasPropertyBeenSet()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(new UserParamsFilesStub());

			var prms = new UserParams();
			
			var property = Rnd.GetRandomKeyFromDictionary(PropsAndValues);

			Assert.IsFalse(prms.HasPropertyBeenSet(property));

			prms.SetProperty(property, PropsAndValues[property].oth, true);
			
			Assert.IsTrue(prms.HasPropertyBeenSet(property));
		}	// end TestHasPropertyBeenSet()

		/// <summary>
		/// tests <see cref="UserParams.ModifyProperty" />
		/// </summary>
		[Test]
		public void TestModifyProperty()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(new UserParamsFilesStub());
			var cons = new Mock<IConsole>();
			cons.Setup(c => c.WriteLine(It.IsAny<string>()));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);

			var prms = new UserParams();

			var property = Rnd.GetRandomKeyFromDictionary(PropsAndValues);
			var values = PropsAndValues[property];
			
			prms.ModifyProperty(property, "whatever");

			cons.Verify(c => c.WriteLine($"'{property}' isn't currently set"));
			cons.VerifyNoOtherCalls();

			prms.SetProperty(property, values.oth, true);

			prms.ModifyProperty(property, values.thr);

			cons.VerifyNoOtherCalls();

			Assert.AreEqual(values.thr, prms.GetPropertyValue(property));
			
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestModifyProperty

		/// <summary>
		/// verify that we ignore a lack of params file
		/// </summary>
		[Test]
		public void TestNoFile()
		{
			void Test()
			{
				var files = new UserParamsFilesStub();
				IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);
				// ReSharper disable once ObjectCreationAsStatement
				new UserParams();
				Assert.IsFalse(files.Read, "it shouldn't have read but it did");
			}

			CoreTestUtils.TestConsoleOutput(Test, "");
		}	// end TestNoFile()

		/// <summary>
		/// tests <see cref="UserParams.Save" />
		/// </summary>
		[Test]
		public void TestSave()
		{
			var files = new UserParamsFilesStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);

			var prms = new UserParams();

			// set up to 8 properties
			var lines = new List<string>();
			var keys = GetRandomKeys();
			foreach (var key in keys)
			{
				var val = PropsAndValues[key].oth;
				prms.SetProperty(key, val, true);
				lines.Add($"{key}={val}");
			}	// end foreach
			prms.Save();

			// verify that the correct lines were generated
			CoreTestUtils.TestListsAreEqual(lines, files.ReadLines());

			// set the separator to verify it is always the first line
			prms.SetProperty("separator", "~", true);
			prms.Save();
			var newLines = new List<string> {"~"};
			newLines.AddRange(lines.Select(line => line.Replace('=', '~')));
			CoreTestUtils.TestListsAreEqual(newLines, files.ReadLines());

		}	// end TestSave()

		/// <summary>
		/// verify that properties can be overriden appropriately
		/// </summary>
		[Test]
		public void TestSetPropertyOverridesAppropriately()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(new UserParamsFilesStub());
			var cons = new Mock<IConsole>();
			cons.Setup(c => c.WriteLine(It.IsAny<string>()));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);

			var prms = new UserParams();

			var property = Rnd.GetRandomKeyFromDictionary(PropsAndValues);
			var value = PropsAndValues[property].oth;
			var other = PropsAndValues[property].thr;

			// set it the first time
			prms.SetProperty(property, value, false);
			Assert.AreEqual(value, prms.GetPropertyValue(property));

			// try to set it again without overriding
			prms.SetProperty(property, other, false);

			cons.Verify(c=>c.WriteLine($"'{property}' has already been set to '{value}' and won't be set to '{other}'"));
			cons.VerifyNoOtherCalls();
			Assert.AreEqual(value, prms.GetPropertyValue(property));

			// override
			prms.SetProperty(property, other, true);
			cons.VerifyNoOtherCalls();
			Assert.AreEqual(other, prms.GetPropertyValue(property));

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSetPropertyOverridesAppropriately()

		/// <summary>
		/// verify that we print out a message if an unknown property is set
		/// </summary>
		[Test]
		public void TestUnknownProperty()
		{
			var files = new UserParamsFilesStub();
			files.WriteAllLines("", new[] {"Name=na", "NotReal=fake"});
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);
			var cons = new Mock<IConsole>();
			cons.Setup(c => c.WriteLine(It.IsAny<string>()));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);

			// ReSharper disable once ObjectCreationAsStatement
			new UserParams();

			cons.Verify(c => c.WriteLine("It isn't a problem, but NotReal isn't used"), Times.Once);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}	// end TestUnknownProperty()

		/// <summary>
		/// verifies that we can unset a property
		/// </summary>
		[Test]
		public void TestUnsetProperty()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(new UserParamsFilesStub());
			var cons = new Mock<IConsole>();
			cons.Setup(c => c.WriteLine(It.IsAny<string>()));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);

			var prms = new UserParams();

			var property = Rnd.GetRandomKeyFromDictionary(PropsAndValues);

			prms.SetProperty(property, PropsAndValues[property].oth, true);

			prms.UnsetProperty(property);
			
			// if a property was set, no output is generated
			cons.VerifyNoOtherCalls();
			
			prms.UnsetProperty(property);

			cons.Verify(c => c.WriteLine($"'{property}' is not currently set, and thus cannot be deleted"));
			cons.VerifyNoOtherCalls();
			
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestUnsetProperty()

		/// <summary>
		/// verify that we correctly read a file using a custom equals symbol
		/// </summary>
		[Test]
		public void TestUsingCustom()
		{
			var files = new UserParamsFilesStub();
			files.WriteAllLines("", GetLines(true));
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);
			TestAllProps(new UserParams());
		}	// end TestUsingCustom()

		/// <summary>
		/// verify that we correctly read a file using the default equals symbol
		/// </summary>
		[Test]
		public void TestUsingEquals()
		{
			var files = new UserParamsFilesStub();
			files.WriteAllLines("", GetLines(false));
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);
			TestAllProps(new UserParams());
		}	// end TestUsingEquals()
		
		#endregion Tests

		#region Helpers
		
		///
		[TearDown]
		public void Cleanup()
		{
			Env.UseRepo = false;
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
		}	// end Cleanup()

		private static IEnumerable GetDefaultValueTestData
		{
			get
			{
				return PropsAndValues.Select(kvp => new TestCaseData(kvp.Key, kvp.Value.def));
			}	// end get
		}	// end DoBuildIndividuallyTestData

		private string[] GetLines(bool custom)
		{
			var ch = custom ? '~' : '=';
			var lines = new List<string>
			{
				Prop(nameof(UserParams.AutoGeneratedKeyStartLength), ch, "5"),
				Prop(nameof(UserParams.Colors), ch, "co"),
				Prop(nameof(UserParams.DataFolder), ch, "da"),
				Prop(nameof(UserParams.DebugBranch), ch, "db"),
				Prop(nameof(UserParams.DefaultUser), ch, "fred"),
				Prop(nameof(UserParams.LogUsages), ch, "0"),
				Prop(nameof(UserParams.Name), ch, "na"),
				Prop(nameof(UserParams.Path), ch, "pa"),
				Prop(nameof(UserParams.PreferCurrent), ch, "1"),
				Prop(nameof(UserParams.PromptWait), ch, "25"),
				Prop(nameof(UserParams.ScrumHours), ch, "0"),
				Prop(nameof(UserParams.ScrumLocation), ch, "sl"),
				Prop(nameof(UserParams.ShortcutFile), ch, "sf"),
				Prop(nameof(UserParams.ShowRemote), ch, "1"),
				Prop(nameof(UserParams.SortResx), ch, "0"),
				Prop(nameof(UserParams.TitleFormat), ch, "tf"),
				Prop(nameof(UserParams.TrackUnitTestTime), ch, "1"),
				Prop(nameof(UserParams.UnitTestDefaults), ch, "abc"),
				Prop(nameof(UserParams.UseRepo), ch, "0")
			};
			if (custom)
			{
				lines.Insert(0, "~");
			}	// end if

			return lines.ToArray();
		}	// end GetLines(bool)

		private List<string> GetRandomKeys()
		{
			var keys = new List<string>();

			for (var i = 0; i < 8; i++)
			{
				var prop = Rnd.GetRandomKeyFromDictionary(PropsAndValues);
				if (!keys.Contains(prop))
				{
					keys.Add(prop);
				}	// end if
			}	// end for

			return keys;
		}	// end GetRandomKeys()

		private static string Prop(string prop, char equal, string value) => $"{prop}{equal}{value}";
		
		private static void SetupDictionary()
		{
			var files = new UserParamsFilesStub();
			files.WriteAllLines("", null);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, UserParamsFilesStub>(files);

			var prms = new UserParams();

			_propsAndValues = new Dictionary<string, (string, string, string)>();

			void AddBool(string name, bool value)
			{
				_propsAndValues.Add(name, ($"{value.ToInt()}", $"{(!value).ToInt()}", $"{value.ToInt()}"));
			}

			void AddInt(string name, int value)
			{
				_propsAndValues.Add(name, ($"{value}", "18", "34"));
			}

			void AddString(string name, string value)
			{
				_propsAndValues.Add(name, (value, "Fred", "George"));
			}

			AddInt(nameof(prms.AutoGeneratedKeyStartLength), prms.AutoGeneratedKeyStartLength);
			AddString(nameof(prms.Colors), prms.Colors);
			AddString(nameof(prms.DataFolder), prms.DataFolder);
			AddString(nameof(prms.DebugBranch), prms.DebugBranch);
			AddString(nameof(prms.DefaultUser), prms.DefaultUser);
			AddBool(nameof(prms.LogUsages), prms.LogUsages);
			AddString(nameof(prms.Name), prms.Name);
			AddString(nameof(prms.Path), prms.Path);
			AddBool(nameof(prms.PreferCurrent), prms.PreferCurrent);
			AddInt(nameof(prms.PromptWait), prms.PromptWait);
			AddString(nameof(prms.ShortcutFile), prms.ShortcutFile);
			AddBool(nameof(prms.ShowRemote), prms.ShowRemote);
			AddBool(nameof(prms.SortResx), prms.SortResx);
			AddString(nameof(prms.TitleFormat), prms.TitleFormat);
			AddBool(nameof(prms.TrackUnitTestTime), prms.TrackUnitTestTime);
			AddString(nameof(prms.UnitTestDefaults), prms.UnitTestDefaults);
			AddBool(nameof(prms.UseRepo), prms.UseRepo);

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();

		}	// end SetupDictionary()

		private static void TestAllProps(UserParams userParams)
		{
			Assert.AreEqual(5, userParams.AutoGeneratedKeyStartLength);
			Assert.AreEqual("co", userParams.Colors);
			Assert.AreEqual("da", userParams.DataFolder);
			Assert.AreEqual("db", userParams.DebugBranch);
			Assert.AreEqual("fred", userParams.DefaultUser);
			Assert.AreEqual(false, userParams.LogUsages);
			Assert.AreEqual("na", userParams.Name);
			Assert.AreEqual("pa", userParams.Path);
			Assert.AreEqual(true, userParams.PreferCurrent);
			Assert.AreEqual(25, userParams.PromptWait);
			Assert.AreEqual(false, userParams.ScrumHours);
			Assert.AreEqual("sl", userParams.ScrumLocation);
			Assert.AreEqual("sf", userParams.ShortcutFile);
			Assert.AreEqual(true, userParams.ShowRemote);
			Assert.AreEqual(false, userParams.SortResx);
			Assert.AreEqual("tf", userParams.TitleFormat);
			Assert.AreEqual(true, userParams.TrackUnitTestTime);
			Assert.AreEqual("abc", userParams.UnitTestDefaults);
			Assert.AreEqual(false, userParams.UseRepo);
		}	// end TestAllProps(UserParams)
		
		#endregion Helpers

		#region Inner Class

		private class UserParamsFilesStub : BaseFileStub
		{
			internal bool Read;

			private bool _created;

			private IEnumerable<string> _lines;

			public override bool Exists(string path) => _created;

			public List<string> ReadLines() => _lines.ToList();

			public override IEnumerable<string> ReadLines(string path)
			{
				Read = true;
				return _lines;
			}	// end ReadLines(string)

			public override void WriteAllLines(string path, IEnumerable<string> lines)
			{
				_created = true;
				_lines = lines ?? new List<string> {""};
			}	// end WriteAllLines(string, IEnumerable<string>)

		}	// end UserParamsFilesStub

		#endregion Inner Class

	}	// end UserParamsTests

}	// end HubUnitTest
