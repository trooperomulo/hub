﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Interface;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="Out" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class OutTests
	{
		#region Property
		
		private static string NewLine => Environment.NewLine;

		#endregion Property

		#region Tests
		
		/// <summary>
		/// tests <see cref="Out.Blank"/>
		/// </summary>
		[Test]
		public void TestBlank()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Blank();

			Assert.AreEqual(NewLine, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Command" />
		/// </summary>
		[Test]
		[TestCase(1, "CmdA\t\t\t\ta\tb\r\n")]
		[TestCase(2, "CmdB\t\t\t\tab\tbc\tcd\r\n")]
		[TestCase(3, "C has a longer description\tabc\tbcd\tcde\tdef\r\n")]
		[TestCase(0, null)]
		public void TestCommand(int which, string expected)
		{
			ICommand cmd;
			switch (which)
			{
				case 1:
					cmd = new CmdA();
					break;
				case 2:
					cmd = new CmdB();
					break;
				case 3:
					cmd = new CmdC();
					break;
				default:
					cmd = null;
					break;
			}

			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Command(cmd);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}
		
		/// <summary>
		/// tests <see cref="Out.Dash"/>
		/// </summary>
		[Test]
		public void TestDash()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Dash();

			Assert.AreEqual($"-------------------------------------------------------------{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Error"/>
		/// </summary>
		[Test]
		public void TestError()
		{
			var cons = new Mock<IConsole>();
			cons.Setup(c => c.WriteLine(It.IsAny<string>()));
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);

			Out.Error("something went wrong");

			cons.Verify(c => c.WriteLine("something went wrong"), Times.Once);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Example"/> with no parameters passed in
		/// </summary>
		[Test]
		public void TestExampleEmpty()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Example();

			Assert.AreEqual(null, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Example"/> with null passed in
		/// </summary>
		[Test]
		public void TestExampleNull()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Example(null);

			Assert.AreEqual(null, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Example"/> with one parameter passed in
		/// </summary>
		[Test]
		public void TestExampleOne()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Example("ab");

			Assert.AreEqual($"{NewLine}ab{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Example"/> with three parameters passed in
		/// </summary>
		[Test]
		public void TestExampleThree()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Example("ab", "cd", "ef");

			Assert.AreEqual($"{NewLine}ab cd ef{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Explain"/>
		/// </summary>
		[Test]
		public void TestExplain()
		{
			var expected = $"-------------------------------------------------------------{NewLine}";
			expected += $"title{NewLine}{NewLine}action{NewLine}{NewLine}";
			expected += $"line1{NewLine}line2{NewLine}";
			expected += $"-------------------------------------------------------------{NewLine}";

			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Explain("title", "action", () =>
			{
				Out.Line("line1");
				Out.Line("line2");
			});

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.GetErrorColor" />
		/// </summary>
		[Test]
		[TestCase(ConsoleColor.Black, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkBlue, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkGreen, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkCyan, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkRed, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkMagenta, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkYellow, ConsoleColor.Red)]
		[TestCase(ConsoleColor.Gray, ConsoleColor.Red)]
		[TestCase(ConsoleColor.DarkGray, ConsoleColor.Red)]
		[TestCase(ConsoleColor.Blue, ConsoleColor.Red)]
		[TestCase(ConsoleColor.Green, ConsoleColor.Red)]
		[TestCase(ConsoleColor.Cyan, ConsoleColor.Red)]
		[TestCase(ConsoleColor.Yellow, ConsoleColor.Red)]
		[TestCase(ConsoleColor.White, ConsoleColor.Red)]
		[TestCase(ConsoleColor.Red, ConsoleColor.Blue)]
		[TestCase(ConsoleColor.Magenta, ConsoleColor.Blue)]
		public void TestGetErrorColor(ConsoleColor start, ConsoleColor expected)
		{
			var actual = Out.GetErrorColor(start);
			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// tests <see cref="Out.Indent"/>
		/// </summary>
		[Test]
		[TestCase(0, "line")]
		[TestCase(1, "\tline")]
		[TestCase(2, "\t\t  line")]
		[TestCase(3, "\t\t\tline")]
		[TestCase(4, "\t\t\t\tline")]
		public void TestIndent(int num, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Indent("line", num);

			Assert.AreEqual(expected + NewLine, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Line(string)"/>
		/// </summary>
		[Test]
		public void TestLineString()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Line("a line I have typed");

			Assert.AreEqual($"a line I have typed{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Line(List{string})" />
		/// </summary>
		[Test]
		public void TestLineListString()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Line(new List<string> { "ab", "bc", "cd", "de", "ef" });

			Assert.AreEqual($"'ab' 'bc' 'cd' 'de' 'ef'{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Option(char, string)" />
		/// </summary>
		[Test]
		public void TestOptionNotSelectableChar() => TestOptionNotSelectable(Rnd.Char, Out.Option);

		/// <summary>
		/// tests <see cref="Out.Option(int, string)" />
		/// </summary>
		[Test]
		public void TestOptionNotSelectableInt() => TestOptionNotSelectable(() => Rnd.Int(10), Out.Option);

		/// <summary>
		/// tests <see cref="Out.Option(char, string, bool)" />
		/// </summary>
		[Test]
		public void TestOptionSelectableChar() => TestOptionSelectable(Rnd.Char, Out.Option);

		/// <summary>
		/// tests <see cref="Out.Option(int, string, bool)" />
		/// </summary>
		[Test]
		public void TestOptionSelectableNumbered() => TestOptionSelectable(() => Rnd.Int(10), Out.Option);

		/// <summary>
		/// tests <see cref="Out.Option(string, string, bool)" />
		/// </summary>
		[Test]
		public void TestOptionSelectableUnnumbered() => TestOptionSelectable(Rnd.GetRandomLetter, Out.Option);

		/// <summary>
		/// tests <see cref="Out.NotAShortcut"/>
		/// </summary>
		[Test]
		public void TestNotAShortcut()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.NotAShortcut("solution");

			Assert.AreEqual($"solution was not found in the shortcuts{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}
		
		/// <summary>
		/// tests <see cref="Out.UnaddedPair"/>
		/// </summary>
		[Test]
		[TestCase(true, true, "KEY: value => duplicate key and value\r\n")]
		[TestCase(false, true, "KEY: value => duplicate value\r\n")]
		[TestCase(true, false, "KEY: value => duplicate key\r\n")]
		[TestCase(false, false, "KEY: value => I'm not sure why this wasn't added\r\n")]
		public void TestUnaddedPair(bool dk, bool dv, string expected)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.UnaddedPair(("key", "value"), dk, dv);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Out.Write"/>
		/// </summary>
		[Test]
		public void TestWrite()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			Out.Write("something written");

			Assert.AreEqual("something written", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Tests

		#region Helper

		private void TestOptionNotSelectable<T>(Func<T> getter, Action<T, string> doer)
		{
			var item = getter();

			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			doer(item, "this is the option");

			Assert.AreEqual($"  {item}\tthis is the option{NewLine}", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestOptionNotSelectable<T>(Func<T>, Action<T, string>)

		private static void TestOptionSelectable<T>(Func<T> getter, Action<T, string, bool> doer)
		{
			var item1 = getter();
			var item2 = getter();

			var opt1 = Rnd.GetRandomStr();
			var opt2 = Rnd.GetRandomStr();

			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			doer(item1, opt1, false);
			doer(item2, opt2, true);

			Assert.AreEqual($"  {item1} [ ]\t{opt1}{NewLine}  {item2} [X]\t{opt2}{NewLine}", cons.Output);
			
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestOptionSelectable<T>(Func<T>, Action<T, string, bool>)

		#endregion Helper

		#region Internal classes

		class CmdA : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "CmdA";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "a", "b" };

			#endregion Implementation of ICommand
		}

		class CmdB : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "CmdB";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "ab", "bc", "cd" };

			#endregion Implementation of ICommand
		}

		class CmdC : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "C has a longer description";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "abc", "bcd", "cde", "def" };

			#endregion Implementation of ICommand
		}

		#endregion Internal classes
	}
}
