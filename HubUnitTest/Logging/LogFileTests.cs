﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Interface;
using Hub.Logging;
using HubUnitTest.Stubs;

using CTU = RyanCoreTests.CoreTestUtils;
using Rnd = HubUnitTest.RandomData;
using UTU = HubUnitTest.UnitTestUtil;

namespace HubUnitTest.Logging
{
	/// <summary>
	/// tests <see cref="LogFile{T}"/>
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class LogFileTests
	{
		///
		[SetUp]
		public void ClearInstance()
		{
			var type = typeof(Logger);
			UTU.SetPrivateStaticProperty(type, "Files", null);
			UTU.SetPrivateStaticProperty(type, "Dirs", null);
			UTU.SetPrivateStaticProperty(type, "Name", null);
			UTU.SetPrivateStaticField(type, "_instance", null);
			UTU.SetPrivateStaticField(type, "_pathBase", null);
			UTU.SetPrivateStaticField(type, "_canLog", false);
		}

		/// <summary>
		/// tests <see cref="LogFile{T}.GetPath" />
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestGetPath(bool exists)
		{
			// Arrange
			var pathBase = Rnd.GetRandomStr();
			var name = Rnd.GetRandomStr();
			var log = Rnd.GetRandomStr();
			CTU.SetPrivateStaticField(typeof(Logger), "_pathBase", pathBase);
			var dirs = GetDirs(exists);
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFileStub>(new BaseFileStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);
			Logger.Instance.Initialize(name, false);
			var test = new TestLog(log);

			// Act
			var path = test.Path;

			// Assert
			Assert.AreEqual($@"{pathBase}{name}\{log}.hub", path);
			dirs.Verify(d => d.Exists($"{pathBase}{name}"), Times.Once);
			CTU.MoqMethodWasCalled(dirs, d => d.CreateDirectory($"{pathBase}{name}"), !exists);

			// About face
			CTU.SetPrivateStaticField(typeof(Logger), "_pathBase", null);

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		/// <summary>
		/// tests <see cref="LogFile{T}.LoadFromLogFile" />
		/// </summary>
		[Test]
		public void TestLoadFromLogFile()
		{
			var path = Rnd.GetRandomStr();
			var lines = Rnd.GetRandomListString();
			var data = new Mock<ILogData>();
			data.Setup(d => d.Add(It.Is<string[]>(a => UTU.ListsAreEqual(lines, a.ToList()))));
			var files = new Mock<IFiles>();
			files.Setup(f => f.ReadLines(It.IsAny<string>())).Returns(lines);
			files.Setup(f => f.Delete(It.IsAny<string>()));

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());

			Logger.Instance.Initialize("hank", true);

			var log = new TestLog("");

			log.Load(path, data.Object);

			Assert.AreEqual(0, log.Version);
			Assert.AreEqual(data.Object, log.GetData());
			data.Verify(d => d.Add(It.IsAny<string[]>()));
			files.Verify(f => f.ReadLines(path));
			files.Verify(f => f.Delete(path));

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
		}

		private Mock<IDirectories> GetDirs(bool exists)
		{
			var dirs = new Mock<IDirectories>();
			dirs.Setup(d => d.Exists(It.IsAny<string>())).Returns(exists);
			dirs.Setup(d => d.CreateDirectory(It.IsAny<string>()));
			return dirs;
		}

		private class TestLog : LogFile<ILogData>
		{
			#region Variable

			private readonly string _log;

			internal int Version = -1;

			#endregion Variable

			#region Overrides of LogFile<ILogData>

			/// <inheritdoc />
			protected override string LogPath => GetPath(_log);

			/// <inheritdoc />
			protected override void UpdateVersion(int oldVersion, ref string[] lines)
			{
				Version = oldVersion;
			}

			#endregion Overrides of LogFile<ILogData>

			#region Property

			internal string Path => LogPath;

			#endregion Property

			#region Constructor

			///
			internal TestLog(string log) => _log = log;

			#endregion Constructor

			#region Methods

			internal ILogData GetData() => Data;

			internal void Load(string path, ILogData data) => LoadFromLogFile(path, data);

			#endregion Methods
		}
	}
}
