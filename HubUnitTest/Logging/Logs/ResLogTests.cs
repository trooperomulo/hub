﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ResLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ResLogTests : LogsWithDataTestBase<ResLog, ResLogData, ResData>
	{
		#region Overrides of LogsWithDataTestBase<ResLog, ResLogData, ResData>
		
		/// <inheritdoc />
		protected override bool CheckData(ResLogData data) => Equals(ResLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ResData data) => Logger.Instance.LogRes(data.NoAct, data.MultSol, data.NoPath,
			data.BadPairs, data.NoRKeys, data.NoFVals, data.NoTVals);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ResData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.NoAct.ToInt(),
				start[2] + data.MultSol.ToInt(),
				start[3] + data.NoPath.ToInt(),
				start[4] + data.BadPairs.ToInt(),
				start[5] + data.NoRKeys.ToInt(),
				start[6] + data.NoFVals.ToInt(),
				start[7] + data.NoTVals.ToInt()
			};

			return exp;

		}	// end GetExpected(int, List<int>, ResData)
		
		/// <inheritdoc />
		protected override ResData GetTestData() => new ResData(GetRandomBools(7));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(8);

		#endregion Overrides of LogsWithDataTestBase<ResLog, ResLogData, ResData>

	}	// end ResLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ResData : TestData
	{
		internal bool NoAct { get; }
		// ReSharper disable once IdentifierTypo
		internal bool MultSol { get; }
		internal bool NoPath { get; }
		internal bool BadPairs { get; }
		internal bool NoRKeys { get; }
		// ReSharper disable once IdentifierTypo
		internal bool NoFVals { get; }
		// ReSharper disable once IdentifierTypo
		internal bool NoTVals { get; }

		///
		public ResData(List<bool> boolValues)
		{
			NoAct = boolValues[0];
			MultSol = boolValues[1];
			NoPath = boolValues[2];
			BadPairs = boolValues[3];
			NoRKeys = boolValues[4];
			NoFVals = boolValues[5];
			NoTVals = boolValues[6];

		}	// end ResData(List<bool>)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({NoAct}, {MultSol}, {NoPath}, {BadPairs}, {NoRKeys}, {NoFVals}, {NoTVals})";

		#endregion Overrides of TestData

	}	// end ResData

}	// end HubUnitTest.Logging.Logs
