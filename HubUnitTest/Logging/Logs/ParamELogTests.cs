﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ParamELog" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamELogTests : LogsWithDataTestBase<ParamELog, ParamELogData, ParamEData>
	{
		#region Overrides of LogsWithDataTestBase<ParamELog, ParamELogData, ParamEData>

		/// <inheritdoc />
		protected override bool CheckData(ParamELogData data) => Equals(ParamELogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ParamEData data) => Logger.Instance.LogParamE(data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ParamEData data)
		{
			return new List<int>
			{
				start[0] + 1,
				start[1] + data.Num,
				start[2] + (data.Num == 0 ? 1 : 0),
			};

		}   // end GetExpected(int, List<int>, ParamEData)

		/// <inheritdoc />
		protected override ParamEData GetTestData() => new ParamEData(GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(3);

		#endregion Overrides of LogsWithDataTestBase<ParamELog, ParamELogData, ParamEData>

	}   // end ParamELogTests

	///
	[ExcludeFromCodeCoverage]
	public class ParamEData : TestData
	{
		internal int Num { get; }

		///
		public ParamEData(int num) => Num = num;

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num})";

		#endregion Overrides of TestData

	}   // end ParamEData

}   // end HubUnitTest.Logging.Logs