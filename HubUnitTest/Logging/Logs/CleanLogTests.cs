﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="CleanLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class CleanLogTests : LogsWithDataTestBase<CleanLog, CleanLogData, CleanData>
	{
		#region Overrides of LogsWithDataTestBase<CleanLog, CleanLogData>
		
		/// <inheritdoc />
		protected override bool CheckData(CleanLogData data) => Equals(CleanLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(CleanData data) => Logger.Instance.LogClean(data.Noisy, data.Num, data.UsedDef);
		
		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, CleanData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Noisy.ToInt(),
				start[2] + data.Num,
				start[3] + data.UsedDef.ToInt()
			};
			
			return exp;

		}	// end GetExpected(List<int>, bool, int, bool)
		
		/// <inheritdoc />
		protected override CleanData GetTestData() => new CleanData(Rnd.Bool(), Rnd.Int(10), Rnd.Bool());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(4);

		#endregion Overrides of LogsWithDataTestBase<CleanLog, CleanLogData>

	}	// end CleanLogTests

	///
	[ExcludeFromCodeCoverage]
	public class CleanData : TestData
	{
		internal bool Noisy { get; }
		internal int Num { get; }
		internal bool UsedDef { get; }
		
		internal CleanData(bool noisy, int num, bool usedDef)
		{
			Noisy = noisy;
			Num = num;
			UsedDef = usedDef;

		}	// end CleanData(List<bool>, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Noisy}, {Num}, {UsedDef})";

		#endregion Overrides of TestData

	}	// end CleanData

}	// end HubUnitTest.Logging.Logs
