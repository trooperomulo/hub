﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="UnitTestLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class UnitTestLogTests : LogsWithDataTestBase<UnitTestLog, UnitTestLogData, UnitTestData>
	{
		#region Overrides of LogsWithDataTestBase<UnitTestLog, UnitTestLogData, UnitTestData>
		
		/// <inheritdoc />
		protected override bool CheckData(UnitTestLogData data) => Equals(UnitTestLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(UnitTestData data) => Logger.Instance.LogUnit(data.EstTime, data.Progress,
			data.ShowTime, data.Console, data.Text, data.Xml, data.Input, data.Xm8, data.Trickler, data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, UnitTestData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				GetEstimateTime(version, start, data.EstTime),
				GetProgress(version, start, data.Progress),
				GetShowTime(version, start, data.ShowTime),
				GetConsole(version, start, data.Console),
				GetText(version, start, data.Text),
				GetXml(version, start, data.Xml),
				GetInput(version, start, data.Input),
				GetXm8(version, start, data.Xm8),
				GetTrick(version, start, data.Trickler),
				GetNotBuilt(version, start, data.Num),
				GetBuilt(version, start, data.Num)
			};
			
			return exp;

		}	// end GetExpected(int, List<int>, UnitTestData)

		/// <inheritdoc />
		protected override UnitTestData GetTestData() => new UnitTestData(GetRandomBools(9), Rnd.Int(10));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(version < 1 ? 5 : 12);

		#endregion Overrides of LogsWithDataTestBase<UnitTestLog, UnitTestLogData, UnitTestData>

		#region Helpers

		private static int GetEstimateTime(int version, List<int> start, bool estTime)
		{
			var index = GetValueBasedOnVersion(version, -1, 1);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + estTime.ToInt();

		}	// end GetEstimateTime(int, List<int>, bool)

		private static int GetProgress(int version, List<int> start, bool progress)
		{
			var index = GetValueBasedOnVersion(version, -1, 2);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + progress.ToInt();

		}	// end GetProgress(int, List<int>, bool)

		private static int GetShowTime(int version, List<int> start, bool showTime)
		{
			var index = GetValueBasedOnVersion(version, -1, 3);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + showTime.ToInt();

		}	// end GetShowTime(int, List<int>, bool)

		private static int GetConsole(int version, List<int> start, bool console)
		{
			var index = GetValueBasedOnVersion(version, -1, 4);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + console.ToInt();

		}	// end GetConsole(int, List<int>, bool)

		private static int GetText(int version, List<int> start, bool text)
		{
			var index = GetValueBasedOnVersion(version, -1, 5);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + text.ToInt();

		}	// end GetText(int, List<int>, bool)

		private static int GetXml(int version, List<int> start, bool xml)
		{
			var index = GetValueBasedOnVersion(version, -1, 6);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + xml.ToInt();

		}	// end GetXml(int, List<int>, bool)

		private static int GetInput(int version, List<int> start, bool input)
		{
			var index = GetValueBasedOnVersion(version, -1, 7);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + input.ToInt();

		}	// end GetInput(int, List<int>, bool)

		private static int GetXm8(int version, List<int> start, bool xm8)
		{
			var index = GetValueBasedOnVersion(version, 1, 8);
			return start[index] + xm8.ToInt();

		}	// end GetXm8(int, List<int>, bool)

		private static int GetTrick(int version, List<int> start, bool trick)
		{
			var index = GetValueBasedOnVersion(version, 2, 9);
			return start[index] + trick.ToInt();

		}	// end GetTrick(int, List<int>, bool)

		private static int GetNotBuilt(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, 3, 10);
			return start[index] + (num == 0).ToInt();

		}	// end GetNoBuild(int, List<int>, int)

		private static int GetBuilt(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, 4, 11);
			return start[index] + num;

		}	// end GetBuilt(int, List<int>, int)

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, 1, one, two);

		#endregion Helpers

	}	// end UnitTestLogTests

	///
	[ExcludeFromCodeCoverage]
	public class UnitTestData : TestData
	{
		internal bool EstTime { get; }
		internal bool Progress { get; }
		internal bool ShowTime { get; }
		internal bool Console { get; }
		internal bool Text { get; }
		internal bool Xml { get; }
		internal bool Input { get; }
		internal bool Xm8 { get; }
		internal bool Trickler { get; }
		internal int Num { get; }

		///
		public UnitTestData(List<bool> boolValues, int num)
		{
			EstTime = boolValues[0];
			Progress = boolValues[1];
			ShowTime = boolValues[2];
			Console = boolValues[3];
			Text = boolValues[4];
			Xml = boolValues[5];
			Input = boolValues[6];
			Xm8 = boolValues[7];
			Trickler = boolValues[8];
			Num = num;

		}	// end UnitTestData(List<bool>, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() =>
			$"({EstTime}, {Progress}, {ShowTime}, {Console}, {Text}, {Xml}, {Input}, {Xm8}, {Trickler}, {Num})";

		#endregion Overrides of TestData

	}	// end UnitTestData

}	// end HubUnitTest.Logging.Logs
