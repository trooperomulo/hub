﻿using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging;
using Hub.Logging.Logs;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="GoLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class GoLogTests : LogJustTotalTests<GoLog>
	{
		#region Overrides of LogJustTotalTests

		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogGo;

		#endregion Overrides of LogJustTotalTests

	}	// end GoLogTests

}	// end HubUnitTest.Logging.Logs
