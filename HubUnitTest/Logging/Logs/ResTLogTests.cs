﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ResTLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ResTLogTests : LogsWithDataTestBase<ResTLog, ResTLogData, ResTData>
	{
		#region Constants

		// these are the versions where changes happened
		private const int Vc1 = 1;
		private const int Vc2 = 3;

		#endregion Constants

		#region Overrides of LogsWithDataTestBase<ResTLog, ResTLogData, ResTData>
		
		/// <inheritdoc />
		protected override bool CheckData(ResTLogData data) => Equals(ResTLogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(ResTData data) => Logger.Instance.LogResT(data.Res, data.Num);
		
		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ResTData data)
		{
			var res = data.Res;

			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + res.HasFlag(Res.Core).ToInt(),
				start[2] + res.HasFlag(Res.Xm8).ToInt(),
				ExpectedShared(version, start, res),
				ExpectedNone(version, start, res),
				ExpectedAll(version, start, res),
				ExpectedNum(version, start, data.Num)
			};

			return exp;

		}	// end GetExpectedInts(int, List<int>, ResTData)
		
		/// <inheritdoc />
		protected override ResTData GetTestData() => new ResTData(GetRandomRes(), GetRandomNum());
		
		/// <inheritdoc />
		protected override List<int> GetTestStart(int version)
		{
			var count = GetValueBasedOnVersion(version, 5, 6, 7);

			return GetRandomInts(count);

		}	// end GetTestStart(int)

		#endregion Overrides of LogsWithDataTestBase<ResTLog, ResTLogData, ResTData>

		#region Helpers
		
		private static int ExpectedShared(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, -1, 3);
			var startVal = index < 0 ? 0 : start[index];
			var toAdd = res.HasFlag(Res.Shared).ToInt();

			return startVal + toAdd;

		}	// end ExpectedShared(int, List<int>, Res)

		private static int ExpectedNone(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, 3, 4);
			var toAdd = (res == Res.None).ToInt();

			return start[index] + toAdd;

		}	// end ExpectedShared(int, List<int>, Res)

		private static int ExpectedAll(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, -1, 4, 5);
			var startVal = index < 0 ? 0 : start[index];

			var toAdd = (res == Res.All).ToInt();

			return startVal + toAdd;

		}	// end ExpectedAll(int, List<int>, Res)

		private static int ExpectedNum(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, 4, 5, 6);

			return start[index] + num;

		}	// end ExpectedNum(int, List<int>, int

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, Vc2, one, two);

		private static int GetValueBasedOnVersion(int version, int one, int two, int thr) =>
			GetValueBasedOnVersion3(version, Vc1, Vc2, one, two, thr);

		#endregion Helpers

	}	// end ResTLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ResTData : TestData
	{
		internal int Num { get; }

		internal Res Res { get; }

		internal ResTData(Res res, int num)
		{
			Res = res;
			Num = num;

		}	// end ResTData(Res, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Res}, {Num})";

		#endregion Overrides of TestData

	}	// end ResTData

}	// end HubUnitTest.Logging.Logs
