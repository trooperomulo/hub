﻿using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging;
using Hub.Logging.Logs;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="RfbLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class RfbLogTests : LogJustTotalTests<RfbLog>
	{
		#region Overrides of LogJustTotalTests

		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogRfb;

		#endregion Overrides of LogJustTotalTests

	}	// end RfbLogTests

}	// end HubUnitTest.Logging.Logs
