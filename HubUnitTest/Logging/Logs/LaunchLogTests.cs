﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="LaunchLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class LaunchLogTests : LogsWithDataTestBase<LaunchLog, LaunchLogData, LaunchData>
	{
		#region Overrides of LogsWithDataTestBase<LaunchLog, LaunchLogData, LaunchData>
		
		/// <inheritdoc />
		protected override bool CheckData(LaunchLogData data) => Equals(LaunchLogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(LaunchData data) =>
			Logger.Instance.LogLaunch(data.Trick, data.Test, data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, LaunchData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Trick.ToInt(),
				start[2] + data.Test.ToInt(),
				start[3] + (data.Num == 0).ToInt(),
				start[4] + data.Num
			};
			
			return exp;

		}	// end GetExpectedInts(int, List<int>, LaunchData)
		
		/// <inheritdoc />
		protected override LaunchData GetTestData() => new LaunchData(Rnd.Bool(), Rnd.Bool(), Rnd.Int(10));
		
		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(5);

		#endregion Overrides of LogsWithDataTestBase<LaunchLog, LaunchLogData, LaunchData>

	}	// end LaunchLogTests

	///
	[ExcludeFromCodeCoverage]
	public class LaunchData : TestData
	{
		internal bool Trick { get; }
		internal bool Test { get; }
		internal int Num { get; }

		///
		public LaunchData(bool trick, bool test, int num)
		{
			Trick = trick;
			Test = test;
			Num = num;

		}	// end LaunchData(bool, bool, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Trick}, {Test}, {Num})";

		#endregion
	}	// end LaunchData

}	// end HubUnitTest.Logging.Logs
