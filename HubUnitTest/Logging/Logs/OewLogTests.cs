﻿using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging;
using Hub.Logging.Logs;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="OewLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class OewLogTests : LogJustTotalTests<OewLog>
	{
		#region Overrides of LogJustTotalTests

		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogOew;

		#endregion Overrides of LogJustTotalTests

	}   // end OewLogTests

}	// end HubUnitTest.Logging.Logs
