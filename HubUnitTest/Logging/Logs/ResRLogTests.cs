﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ResRLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ResRLogTests : LogsWithDataTestBase<ResRLog, ResRLogData, ResRData>
	{
		#region Overrides of LogsWithDataTestBase<ResRLog, ResRLogData, ResRData>
		
		/// <inheritdoc />
		protected override bool CheckData(ResRLogData data) => Equals(ResRLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ResRData data) => Logger.Instance.LogResR(data.Res, data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ResRData data)
		{
			var res = data.Res;

			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + res.HasFlag(Res.Core).ToInt(),
				start[2] + res.HasFlag(Res.Xm8).ToInt(),
				ExpectedShared(version, start, res),
				ExpectedNone(version, start, res),
				ExpectedNum(version, start, data.Num)
			};
			
			return exp;

		}	// end GetExpectedInts(int, List<int>, ResRData)
		
		/// <inheritdoc />
		protected override ResRData GetTestData() => new ResRData(GetRandomRes(), GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(version < 3 ? 5 : 6);

		#endregion Overrides of LogsWithDataTestBase<ResRLog, ResRLogData, ResRData>

		#region Helpers
		
		private static int ExpectedShared(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, -1, 3);
			var startVal = index < 0 ? 0 : start[index];

			var toAdd = res.HasFlag(Res.Shared).ToInt();

			return startVal + toAdd;

		}	// end ExpectedShared(int, List<int>, Res)

		private static int ExpectedNone(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, 3, 4);
			var toAdd = (res == Res.None).ToInt();

			return start[index] + toAdd;

		}	// end ExpectedNone(int, List<int>, Res)

		private static int ExpectedNum(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, 4, 5);
			return start[index] + num;

		}	// end ExpectedNum(int, List<int>, int)

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, 3, one, two);

		#endregion Helpers

	}	// end ResRLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ResRData : TestData
	{
		internal Res Res { get; }
		internal int Num { get; }

		///
		public ResRData(Res res, int num)
		{
			Res = res;
			Num = num;

		}	// end ResRData(Res, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Res}, {Num})";

		#endregion Overrides of TestData

	}	// end ResRData

}	// end HubUnitTest.Logging.Logs
