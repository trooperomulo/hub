﻿using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging;
using Hub.Logging.Logs;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="DbLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class DbLogTests : LogJustTotalTests<DbLog>
	{
		#region Overrides of LogJustTotalTests

		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogDb;

		#endregion Overrides of LogJustTotalTests

	}	// end DbLogTests

}	// end HubUnitTest.Logging.Logs
