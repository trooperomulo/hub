﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ResSLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ResSLogTests : LogsWithDataTestBase<ResSLog, ResSLogData, ResSData>
	{
		#region Overrides of LogsWithDataTestBase<ResSLog, ResSLogData, ResSData>
		
		/// <inheritdoc />
		protected override bool CheckData(ResSLogData data) => Equals(ResSLogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(ResSData data) => Logger.Instance.LogResS(data.Res);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ResSData data)
		{
			var res = data.Res;

			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + res.HasFlag(Res.Core).ToInt(),
				start[2] + res.HasFlag(Res.Xm8).ToInt(),
				ExpectedShared(version, start, res),
				ExpectedNone(version, start, res)
			};
			
			return exp;

		}	// end GetExpectedInts(int, List<int>, ResSData)
		
		/// <inheritdoc />
		protected override ResSData GetTestData() => new ResSData(GetRandomRes());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(version < 3 ? 4 : 5);

		#endregion Overrides of LogsWithDataTestBase<ResSLog, ResSLogData, ResSData>

		#region Helpers
		
		private static int ExpectedShared(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, -1, 3);
			var startVal = index < 0 ? 0 : start[index];

			var toAdd = res.HasFlag(Res.Shared).ToInt();

			return startVal + toAdd;

		}	// end ExpectedShared(int, List<int>, Res)

		private static int ExpectedNone(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, 3, 4);
			var toAdd = (res == Res.None).ToInt();

			return start[index] + toAdd;

		}	// end ExpectedNone(int, List<int>, Res)

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, 3, one, two);

		#endregion Helpers

	}	// end ResSLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ResSData : TestData
	{
		internal Res Res { get; }

		///
		public ResSData(Res res)
		{
			Res = res;

		}	// end ResSData(Res)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Res})";

		#endregion Overrides of TestData

	}	// end ResSData

}	// end HubUnitTest.Logging.Logs
