﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ImportLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ImportLogTests : LogsWithDataTestBase<ImportLog, ImportLogData, ImportData>
	{
		#region Overrides of LogsWithDataTestBase<ImportLog, ImportLogData, ImportData>
		
		/// <inheritdoc />
		protected override bool CheckData(ImportLogData data) => Equals(ImportLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ImportData data) =>
			Logger.Instance.LogImport(data.Num, data.Delete, data.Days, data.User, data.Status);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ImportData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Num,
				start[2] + data.Delete.ToInt(),
				start[3] + data.Days.ToInt(),
				start[4] + data.User.ToInt(),
				start[5] + data.Status.ToInt()
			};
			
			return exp;

		}	// end GetExpected(List<int>, int, List<bool>)
		
		/// <inheritdoc />
		protected override ImportData GetTestData() => new ImportData(Rnd.Int(10), GetRandomBools(5));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(6);

		#endregion Overrides of LogsWithDataTestBase<ImportLog, ImportLogData, ImportData>

	}	// end ImportLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ImportData : TestData
	{
		internal int Num { get; }
		internal bool Delete { get; }
		internal bool Days { get; }
		internal bool User { get; }
		internal bool Status { get; }

		///
		public ImportData(int num, List<bool> boolValues)
		{
			Num = num;
			Delete = boolValues[0];
			Days = boolValues[1];
			User = boolValues[2];
			Status = boolValues[3];

		}	// end ImportData(int, List<bool>)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num}, {Delete}, {Days}, {User}, {Status})";

		#endregion Overrides of TestData

	}	// end ImportData

}	// end HubUnitTest.Logging.Logs
