﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="OrphanLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class OrphanLogTests : LogsWithDataTestBase<OrphanLog, OrphanLogData, OrphanData>
	{
		#region Overrides of LogsWithDataTestBase<OrphanLog, OrphanLogData, OrphanData>
		
		/// <inheritdoc />
		protected override bool CheckData(OrphanLogData data) => Equals(OrphanLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(OrphanData data) => Logger.Instance.LogOrphan(data.Xm8, data.Data);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, OrphanData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Xm8.ToInt(),
				GetData(version, start, data.Data)
			};
			
			return exp;

		}	// end GetExpectedInts(List<int>, bool, int, bool)
		
		/// <inheritdoc />
		protected override OrphanData GetTestData() => new OrphanData(Rnd.Bool(), Rnd.Bool());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(3);

		#endregion Overrides of LogsWithDataTestBase<OrphanLog, OrphanLogData, OrphanData>

		#region Helpers
		
		private static int GetData(int version, List<int> start, bool data)
		{
			var index = GetValueBasedOnVersion(version, -1, 2);
			var startVal = index > 0 ? start[index] : 0;
			return startVal + data.ToInt();

		}	// end GetRevision(int, List<int>, bool)

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, 2, one, two);

		#endregion Helpers

	}	// end OrphanLogTests

	///
	[ExcludeFromCodeCoverage]
	public class OrphanData : TestData
	{
		internal bool Xm8 { get; }
		internal bool Data { get; }

		///
		public OrphanData(bool xm8, bool data)
		{
			Xm8 = xm8;
			Data = data;

		}	// end OrphanData(bool, bool)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Xm8}, {Data})";

		#endregion Overrides of TestData

	}	// end OrphanData

}	// end HubUnitTest.Logging.Logs
