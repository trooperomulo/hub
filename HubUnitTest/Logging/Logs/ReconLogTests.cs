﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ReconLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ReconLogTests : LogsWithDataTestBase<ReconLog, ReconLogData, ReconData>
	{
		#region Overrides of LogsWithDataTestBase<ReconLog, ReconLogData, ReconData>
		
		/// <inheritdoc />
		protected override bool CheckData(ReconLogData data) => Equals(ReconLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ReconData data) => Logger.Instance.LogRecon(data.Xm8, data.Trick, data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ReconData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Xm8.ToInt(),
				start[2] + data.Trick.ToInt(),
				start[3] + (data.Num == 0).ToInt(),
				start[4] + data.Num
			};
			
			return exp;

		}	// end GetExpectedInts(int, List<int>, ReconData)

		/// <inheritdoc />
		protected override ReconData GetTestData() => new ReconData(Rnd.Bool(), Rnd.Bool(), Rnd.Int(10));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(5);

		#endregion Overrides of LogsWithDataTestBase<ReconLog, ReconLogData, ReconData>

	}	// end ReconLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ReconData : TestData
	{
		internal bool Xm8 { get; }
		internal bool Trick { get; }
		internal int Num { get; }

		///
		public ReconData(bool xm8, bool trick, int num)
		{
			Xm8 = xm8;
			Trick = trick;
			Num = num;

		}	// end ReconData(bool, bool, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Xm8}, {Trick}, {Num})";

		#endregion Overrides of TestData

	}	// end ReconData

}	// end HubUnitTest.Logging.Logs
