﻿using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging;
using Hub.Logging.Logs;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="KillLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class KillLogTests : LogJustTotalTests<KillLog>
	{
		#region Overrides of LogJustTotalTests

		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogKill;

		#endregion Overrides of LogJustTotalTests

	}	// end KillLogTests

}	// end HubUnitTest.Logging.Logs
