﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ScrumLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ScrumLogTests : LogsWithDataTestBase<ScrumLog, ScrumLogData, ScrumData>
	{
		#region Overrides of LogsWithDataTestBase<ScrumLog, ScrumLogData, ScrumData>

		/// <inheritdoc />
		protected override double AddHours(ScrumData data, double hours)
		{
			if (hours < 0)
			{
				return data.H;
			}	// end if

			if (data.H < 0)
			{
				return hours;
			}	// end if

			return hours + data.H;

		}	// end AddHours(ScrumData, double)

		/// <inheritdoc />
		protected override bool CheckData(ScrumLogData data) => Equals(ScrumLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ScrumData data) =>
			Logger.Instance.LogScrum(data.D, data.Hstr, data.W, data.B, data.T, data.Future);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ScrumData data)
		{
			var exp = new List<int>
			{
				start[00] + 1,
				start[01] + (data.D + data.W + data.B == 0).ToInt(),
				start[02] + data.Future.ToInt(),
				start[03] + (data.D == 0).ToInt(),
				start[04] + (data.H < 0).ToInt(),
				start[05] + (data.W == 0).ToInt(),
				start[06] + (data.B == 0).ToInt(),
				start[07] + (data.T == 0).ToInt(),
				start[08] + data.D,
				start[09] + data.W,
				start[10] + data.B,
				start[11] + data.T
			};

			return exp;

		}	// end GetExpectedInts(int, List<int>, ScrumData)

		/// <inheritdoc />
		protected override double GetStartHours() => GetRandomHours();

		/// <inheritdoc />
		protected override ScrumData GetTestData() => new ScrumData(GetRandomInts(4), GetRandomHours(), Rnd.Bool());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(12);

		#endregion Overrides of LogsWithDataTestBase<ScrumLog, ScrumLogData, ScrumData>

		#region Helper

		private static double GetRandomHours() => Rnd.Int(4) > 0 ? Rnd.Doub(2, 10) : -1;

		#endregion Helper

	}	// end ScrumLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ScrumData : TestData
	{
		internal int D { get; }
		internal double H { get; }
		internal int W { get; }
		internal int B { get; }
		internal int T { get; }
		internal bool Future { get; }

		// ReSharper disable once SpecifyACultureInStringConversionExplicitly
		internal string Hstr => H < 0 ? "" : $"{H}";

		///
		public ScrumData(List<int> intValues, double h, bool future)
		{
			D = intValues[0];
			H = h;
			W = intValues[1];
			B = intValues[2];
			T = intValues[3];
			Future = future;

		}	// end ScrumData(List<int>, double?, bool)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({D}, {H}, {W}, {B}, {T}, {Future})";

		#endregion Overrides of TestData

	}	// end ScrumData

}	// end HubUnitTest.Logging.Logs
