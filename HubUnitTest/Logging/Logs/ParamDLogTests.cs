﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ParamDLog" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamDLogTests : LogsWithDataTestBase<ParamDLog, ParamDLogData, ParamDData>
	{
		#region Overrides of LogsWithDataTestBase<ParamDLog, ParamDLogData, ParamDData>

		/// <inheritdoc />
		protected override bool CheckData(ParamDLogData data) => Equals(ParamDLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ParamDData data) => Logger.Instance.LogParamD(data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ParamDData data)
		{
			return new List<int>
			{
				start[0] + 1,
				start[1] + data.Num
			};
			
		}	// end GetExpected(int, List<int>, ParamDData)

		/// <inheritdoc />
		protected override ParamDData GetTestData() => new ParamDData(GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(2);

		#endregion Overrides of LogsWithDataTestBase<ParamDLog, ParamDLogData, ParamDData>

	}	// end ParamDLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ParamDData : TestData
	{
		internal int Num { get; }

		///
		public ParamDData(int num) => Num = num;

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num})";

		#endregion Overrides of TestData

	}	// end ParamDData

}	// end HubUnitTest.Logging.Logs