﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub.Logging;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="AutoLogLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ParamSLogTests : LogJustTotalTests<ParamSLog>
	{
		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogParamS;

	}	// end ParamSLogTests

}	// HubUnitTest.Logging.Logs
