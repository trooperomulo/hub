﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="BuildLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class BuildLogTests : LogsWithDataTestBase<BuildLog, BuildLogData, BuildData>
	{
		#region Overrides of LogsWithDataTestBase<BuildLog, BuildLogData, BuildData>
		
		/// <inheritdoc />
		protected override bool CheckData(BuildLogData data) => Equals(BuildLogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(BuildData data) =>
			Logger.Instance.LogBuild(data.Prod, data.Debug, data.Xm8, data.Trk, data.Unit, data.Sols);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, BuildData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Prod.ToInt(),
				start[2] + data.Debug.ToInt(),
				start[3] + data.Xm8.ToInt(),
				start[4] + data.Trk.ToInt(),
				start[5] + data.Unit.ToInt(),
				start[6] + (data.Sols == 1).ToInt(),
				start[7] + (data.Sols == 0).ToInt(),
				start[8] + data.Sols
			};

			return exp;

		}	// end GetExpectedInts(int, List<int>, BuildData)

		/// <inheritdoc />
		protected override BuildData GetTestData() => new BuildData(GetRandomBools(5), Rnd.Int(10));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(9);

		#endregion Overrides of LogsWithDataTestBase<BuildLog, BuildLogData, BuildData>

	}	// end BuildLogTests

	///
	[ExcludeFromCodeCoverage]
	public class BuildData : TestData
	{
		internal bool Prod { get; }
		internal bool Debug { get; }
		internal bool Xm8 { get; }
		internal bool Trk { get; }
		internal bool Unit { get; }
		internal int Sols { get; }
		
		internal BuildData(List<bool> boolValues, int sols)
		{
			Prod = boolValues[0];
			Debug = boolValues[1];
			Xm8 = boolValues[2];
			Trk = boolValues[3];
			Unit = boolValues[4];
			Sols = sols;

		}	// end BuildData(List<bool>, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Prod}, {Debug}, {Xm8}, {Trk}, {Unit}, {Sols})";

		#endregion Overrides of TestData

	}	// end BuildData

}	// end HubUnitTest.Logging.Logs
