﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ResALog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ResALogTests : LogsWithDataTestBase<ResALog, ResALogData, ResAData>
	{
		#region Overrides of LogsWithDataTestBase<ResALog, ResALogData, ResAData>
		
		/// <inheritdoc />
		protected override bool CheckData(ResALogData data) => Equals(ResALogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(ResAData data) => Logger.Instance.LogResA(data.Res, data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ResAData data)
		{
			var res = data.Res;
			var num = data.Num;

			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + res.HasFlag(Res.Core).ToInt(),
				start[2] + res.HasFlag(Res.Xm8).ToInt(),
				(version < 3 ? 0 : start[3]) + res.HasFlag(Res.Shared).ToInt(),
				start[version < 3 ? 3 : 4] + (res == Res.None).ToInt(),
				start[version < 3 ? 4 : 5] + (num == 0).ToInt(),
				start[version < 3 ? 5 : 6] + num
			};
			
			return exp;

		}	// end GetExpected(int, List<int>, ResAData)
		
		/// <inheritdoc />
		protected override ResAData GetTestData() => new ResAData(GetRandomRes(), GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(version < 3 ? 6 : 7);

		#endregion Overrides of LogsWithDataTestBase<ResALog, ResALogData, ResAData>

	}	// end ResALogTests

	///
	[ExcludeFromCodeCoverage]
	public class ResAData : TestData
	{
		internal Res Res { get; }
		internal int Num { get; }

		///
		public ResAData(Res res, int num)
		{
			Res = res;
			Num = num;

		}	// end ResAData(Res, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Res}, {Num})";

		#endregion Overrides of TestData

	}	// end ResAData

}	// end HubUnitTest.Logging.Logs
