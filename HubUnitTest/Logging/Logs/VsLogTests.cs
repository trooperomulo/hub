﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="VsLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class VsLogTests : LogsWithDataTestBase<VsLog, VsLogData, VsData>
	{
		#region Overrides of LogsWithDataTestBase<VsLog, VsLogData, VsData>

		/// <inheritdoc />
		protected override bool CheckData(VsLogData data) => Equals(VsLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(VsData data) => Logger.Instance.LogVs(data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, VsData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + (data.Num == 1).ToInt(),
				start[2] + data.Num
			};
			
			return exp;

		}	// end GetExpectedInts(int, List<int>, VsData)
		
		/// <inheritdoc />
		protected override VsData GetTestData() => new VsData(Rnd.Int(10));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(3);

		#endregion Overrides of LogsWithDataTestBase<VsLog, VsLogData, VsData>
		
	}	// end VsLogTests

	///
	[ExcludeFromCodeCoverage]
	public class VsData : TestData
	{
		internal int Num { get; }

		///
		public VsData(int num) => Num = num;

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num})";

		#endregion Overrides of TestData

	}	// end VsData

}	// end HubUnitTest.Logging.Logs
