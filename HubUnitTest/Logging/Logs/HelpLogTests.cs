﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="HelpLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class HelpLogTests : LogsWithDataTestBase<HelpLog, HelpLogData, HelpData>
	{
		#region Overrides of LogsWithDataTestBase<HelpLog, HelpLogData, HelpData>
		
		/// <inheritdoc />
		protected override bool CheckData(HelpLogData data) => Equals(HelpLogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(HelpData data) => Logger.Instance.LogHelp(data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, HelpData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + (data.Num == 0).ToInt(),
				start[2] + data.Num
			};
			
			return exp;

		}	// end GetExpectedInts(int, List<int>, HelpData)
		
		/// <inheritdoc />
		protected override HelpData GetTestData() => new HelpData(Rnd.Int(10));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(3);

		#endregion Overrides of LogsWithDataTestBase<HelpLog, HelpLogData, HelpData>

	}	// end HelpLogTests

	///
	[ExcludeFromCodeCoverage]
	public class HelpData : TestData
	{
		internal int Num { get; }

		///
		public HelpData(int num) => Num = num;

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num})";

		#endregion	Overrides of TestData

	}	// end HelpData

}	// end HubUnitTest.Logging.Logs
