﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ParamLog" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamLogTests : LogsWithDataTestBase<ParamLog, ParamLogData, ParamData>
	{
		#region Overrides of LogsWithDataTestBase<ParamLog, ParamLogData, ParamData>

		/// <inheritdoc />
		protected override bool CheckData(ParamLogData data) => Equals(ParamLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ParamData data) => Logger.Instance.LogParam(data.NoAct, data.NoAdd, data.BadAdd,
			data.NoDel, data.NoMod, data.BadMod);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ParamData data)
		{
			return new List<int>
			{
				start[0] + 1,
				start[1] + data.NoAct.ToInt(),
				start[2] + data.NoAdd.ToInt(),
				start[3] + data.BadAdd.ToInt(),
				start[4] + data.NoDel.ToInt(),
				start[5] + data.NoMod.ToInt(),
				start[6] + data.BadMod.ToInt()
			};

		}	// end GetExpectedInts(int, List<int>, ParamData)

		/// <inheritdoc />
		protected override ParamData GetTestData() => new ParamData(GetRandomBools(6));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(7);

		#endregion Overrides of LogsWithDataTestBase<ParamLog, ParamLogData, ParamData>

	}	// end ParamLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ParamData : TestData
	{
		internal bool NoAct { get; }
		internal bool NoAdd { get; }
		internal bool BadAdd { get; }
		internal bool NoDel { get; }
		internal bool NoMod { get; }
		internal bool BadMod { get; }

		///
		public ParamData(List<bool> boolValues)
		{
			NoAct = boolValues[0];
			NoAdd = boolValues[1];
			BadAdd = boolValues[2];
			NoDel = boolValues[3];
			NoMod = boolValues[4];
			BadMod = boolValues[5];

		}	// end ParamData(List<bool>)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({NoAct}, {NoAdd}, {BadAdd}, {NoDel}, {NoMod}, {BadMod})";

		#endregion Overrides of TestData

	}	// end ParamData

}	// end HubUnitTest.Logging.Logs
