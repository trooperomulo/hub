﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="TeamCityLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TeamCityLogTests : LogsWithDataTestBase<TeamCityLog, TeamCityLogData, TeamCityData>
	{
		#region Constants

		// these are the versions where changes happened
		private const int Vc1 = 1;
		private const int Vc2 = 4;

		#endregion Constants

		#region Overrides of LogsWithDataTestBase<TeamCityLog, TeamCityLogData, TeamCityData>
		
		/// <inheritdoc />
		protected override bool CheckData(TeamCityLogData data) => Equals(TeamCityLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(TeamCityData data) => Logger.Instance.LogTeamCity(data.Platform, data.Num);
		
		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, TeamCityData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				ExpectedWindows(version, start, data.Platform),
				ExpectedAndroid(version, start, data.Platform),
				ExpectedIos(version, start, data.Platform),
				ExpectedNo(version, start, data.Platform),
				ExpectedBranch(version, start, data.Num),
				ExpectedBranches(version, start, data.Num)
			};

			return exp;

		}	// end GetExpectedInts(int, List<int>, Platform, int)
		
		/// <inheritdoc />
		protected override TeamCityData GetTestData() => new TeamCityData(GetRandomPlatform(), GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version)
		{
			var count = GetValueBasedOnVersion(version, 2, 3, 7);

			return GetRandomInts(count);

		}	// end GetTestStart(int)
		
		#endregion Overrides of LogsWithDataTestBase<TeamCityLog, TeamCityLogData, TeamCityData>

		#region Helpers

		private static int ExpectedWindows(int version, List<int> start, Platform platform)
		{
			var index = GetValueBasedOnVersion(version, -1, 1);
			var startVal = index < 0 ? 0 : start[index];
			var toAdd = platform.HasFlag(Platform.Windows).ToInt();

			return startVal + toAdd;

		}	// end ExpectedWindows(int, List<int>, Platform)
		
		private static int ExpectedAndroid(int version, List<int> start, Platform platform)
		{
			var index = GetValueBasedOnVersion(version, -1, 2);
			var startVal = index < 0 ? 0 : start[index];
			var toAdd = platform.HasFlag(Platform.Android).ToInt();

			return startVal + toAdd;

		}	// end ExpectedAndroid(int, List<int>, Platform)
		
		private static int ExpectedIos(int version, List<int> start, Platform platform)
		{
			var index = GetValueBasedOnVersion(version, -1, 3);
			var startVal = index < 0 ? 0 : start[index];
			var toAdd = platform.HasFlag(Platform.iOS).ToInt();

			return startVal + toAdd;

		}	// end ExpectedIos(int, List<int>, Platform)
		
		private static int ExpectedNo(int version, List<int> start, Platform platform)
		{
			var index = GetValueBasedOnVersion(version, -1, 4);
			var startVal = index < 0 ? 0 : start[index];
			var toAdd = (platform == Platform.None).ToInt();

			return startVal + toAdd;

		}	// end ExpectedNo(int, List<int>, Platform)

		private static int ExpectedBranch(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, 1, 1, 5);

			return start[index] + (num > 0).ToInt();

		}	// end ExpectedBranch(int, List<int>, int)
		
		private static int ExpectedBranches(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, -1, 2, 6);
			var startVal = index < 0 ? 0 : start[index];

			return startVal + num;

		}	// end ExpectedBranches(int, List<int>, int)

		private static Platform GetRandomPlatform() => (Platform) Rnd.Int(8);

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, Vc2, one, two);

		private static int GetValueBasedOnVersion(int version, int one, int two, int thr) =>
			GetValueBasedOnVersion3(version, Vc1, Vc2, one, two, thr);

		#endregion Helpers

	}	// end TeamCityLogTests

	///
	[ExcludeFromCodeCoverage]
	public class TeamCityData : TestData
	{
		internal Platform Platform { get; }
		internal int Num { get; }

		///
		public TeamCityData(Platform platform, int num)
		{
			Platform = platform;
			Num = num;

		}	// end TeamCityData(Platform, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Platform}, {Num})";

		#endregion Overrides of TestData

	}	// end TeamCityData

}	// end HubUnitTest.Logging.Logs
