﻿using System;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging;
using Hub.Logging.Logs;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="AutoLogLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class AutoLogLogTests : LogJustTotalTests<AutoLogLog>
	{
		/// <inheritdoc />
		protected override Action Log => Logger.Instance.LogAutoLog;
		
	}	// end AutoLogLogTests

}	// end HubUnitTest.Logging.Logs
