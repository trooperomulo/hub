﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ParamMLog" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamMLogTests : LogsWithDataTestBase<ParamMLog, ParamMLogData, ParamMData>
	{
		#region Overrides of LogsWithDataTestBase<ParamMLog, ParamMLogData, ParamMData>

		/// <inheritdoc />
		protected override bool CheckData(ParamMLogData data) => Equals(ParamMLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ParamMData data) => Logger.Instance.LogParamM(data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ParamMData data)
		{
			return new List<int>
			{
				start[0] + 1,
				start[1] + data.Num
			};
			
		}	// end GetExpected(int, List<int>, ParamMData)

		/// <inheritdoc />
		protected override ParamMData GetTestData() => new ParamMData(GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(2);

		#endregion Overrides of LogsWithDataTestBase<ParamMLog, ParamMLogData, ParamMData>

	}	// end ParamMLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ParamMData : TestData
	{
		internal int Num { get; }

		///
		public ParamMData(int num) => Num = num;

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num})";

		#endregion Overrides of TestData

	}	// end ParamMData

}	// end HubUnitTest.Logging.Logs