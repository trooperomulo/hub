﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ParamALog" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamALogTests : LogsWithDataTestBase<ParamALog, ParamALogData, ParamAData>
	{
		#region Overrides of LogsWithDataTestBase<ParamALog, ParamALogData, ParamAData>

		/// <inheritdoc />
		protected override bool CheckData(ParamALogData data) => Equals(ParamALogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(ParamAData data) => Logger.Instance.LogParamA(data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ParamAData data)
		{
			return new List<int>
			{
				start[0] + 1,
				start[1] + data.Num
			};
			
		}	// end GetExpected(int, List<int>, ParamAData)

		/// <inheritdoc />
		protected override ParamAData GetTestData() => new ParamAData(GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(2);

		#endregion Overrides of LogsWithDataTestBase<ParamALog, ParamALogData, ParamAData>

	}	// end ParamALogTests

	///
	[ExcludeFromCodeCoverage]
	public class ParamAData : TestData
	{
		internal int Num { get; }

		///
		public ParamAData(int num) => Num = num;

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Num})";

		#endregion Overrides of TestData

	}	// end ParamAData

}	// end HubUnitTest.Logging.Logs
