﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="ResFLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class ResFLogTests : LogsWithDataTestBase<ResFLog, ResFLogData, ResFData>
	{
		#region Overrides of LogsWithDataTestBase<ResFLog, ResFLogData, ResFData>
		
		/// <inheritdoc />
		protected override bool CheckData(ResFLogData data) => Equals(ResFLogData.Empty(), data);
		
		/// <inheritdoc />
		protected override void DoLog(ResFData data) => Logger.Instance.LogResF(data.Res, data.Num);
		
		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, ResFData data)
		{
			var res = data.Res;
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + res.HasFlag(Res.Core).ToInt(),
				start[2] + res.HasFlag(Res.Xm8).ToInt(),
				ExpectedShared(version, start, res),
				ExpectedNone(version, start, res),
				ExpectedAll(version, start, res),
				ExpectedNum(version, start, data.Num)
			};
			
			return exp;

		}	// end GetExpected(int, List<int>, Res, int)
		
		/// <inheritdoc />
		protected override ResFData GetTestData() => new ResFData(GetRandomRes(), GetRandomNum());

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(version < 3 ? 6 : 7);

		#endregion Overrides of LogsWithDataTestBase<ResFLog, ResFLogData, ResFData>

		#region Helpers
		
		private static int ExpectedShared(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, -1, 3);
			var startVal = index < 0 ? 0 : start[index];

			var toAdd = res.HasFlag(Res.Shared).ToInt();

			return startVal + toAdd;

		}	// end ExpectedShared(int, List<int>, Res)

		private static int ExpectedNone(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, 3, 4);
			var toAdd = (res == Res.None).ToInt();

			return start[index] + toAdd;

		}	// end ExpectedNone(int, List<int>, Res)

		private static int ExpectedAll(int version, List<int> start, Res res)
		{
			var index = GetValueBasedOnVersion(version, 4, 5);
			var toAdd = (res == Res.All).ToInt();

			return start[index] + toAdd;

		}	// end ExpectedAll(int, List<int>, Res)

		private static int ExpectedNum(int version, List<int> start, int num)
		{
			var index = GetValueBasedOnVersion(version, 5, 6);
			return start[index] + num;

		}	// end ExpectedNum(int, List<int>, int)

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, 3, one, two);

		#endregion Helpers

	}	// end ResFLogTests

	///
	[ExcludeFromCodeCoverage]
	public class ResFData : TestData
	{
		internal Res Res { get; }
		internal int Num { get; }

		///
		public ResFData(Res res, int num)
		{
			Res = res;
			Num = num;

		}	// end ResFData(Res, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Res}, {Num})";

		#endregion Overrides of TestData

	}	// end ResFData

}	// end HubUnitTest.Logging.Logs
