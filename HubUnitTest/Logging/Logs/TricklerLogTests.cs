﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="TricklerLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class TricklerLogTests : LogsWithDataTestBase<TricklerLog, TricklerLogData, TricklerData>
	{
		#region Overrides of LogsWithDataTestBase<TricklerLog, TricklerLogData, TricklerData>
		
		/// <inheritdoc />
		protected override bool CheckData(TricklerLogData data) => Equals(TricklerLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(TricklerData data) => Logger.Instance.LogTrickler(data.Xm8, data.Unit, data.Num);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, TricklerData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + data.Xm8.ToInt(),
				start[2] + data.Unit.ToInt(),
				start[3] + (data.Num == 0).ToInt(),
				start[4] + data.Num
			};
			
			return exp;

		}	// end GetExpected(int, List<int>, TricklerData)
		
		/// <inheritdoc />
		protected override TricklerData GetTestData() => new TricklerData(Rnd.Bool(), Rnd.Bool(), Rnd.Int(10));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(5);

		#endregion Overrides of LogsWithDataTestBase<TricklerLog, TricklerLogData, TricklerData>

	}	// end TricklerLogTests

	///
	[ExcludeFromCodeCoverage]
	public class TricklerData : TestData
	{
		internal bool Xm8 { get; }
		internal bool Unit { get; }
		internal int Num { get; }

		///
		public TricklerData(bool xm8, bool unit, int num)
		{
			Xm8 = xm8;
			Unit = unit;
			Num = num;

		}	// end TricklerData(bool, bool, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Xm8}, {Unit}, {Num})";

		#endregion Overrides of TestData

	}	// end TricklerData

}	// end HubUnitTest.Logging.Logs
