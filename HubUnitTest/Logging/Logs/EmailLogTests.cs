﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Logging;
using Hub.Logging.Data;
using Hub.Logging.Logs;

using NUnit.Framework;

namespace HubUnitTest.Logging.Logs
{
	/// <summary>
	/// tests <see cref="EmailLog" />
	/// </summary>
	[ExcludeFromCodeCoverage]
	[TestFixture]
	public class EmailLogTests : LogsWithDataTestBase<EmailLog, EmailLogData, EmailData>
	{
		#region Overrides of LogsWithDataTestBase<EmailLog, EmailLogData, EmailData>
		
		/// <inheritdoc />
		protected override bool CheckData(EmailLogData data) => Equals(EmailLogData.Empty(), data);

		/// <inheritdoc />
		protected override void DoLog(EmailData data) => Logger.Instance.LogEmail(data.Folder, data.Xm8, data.Data,
			data.Patch, data.Major, data.Minor, data.Build, data.Revision);

		/// <inheritdoc />
		protected override List<int> GetExpectedInts(int version, List<int> start, EmailData data)
		{
			var exp = new List<int>
			{
				start[0] + 1,
				start[1] + (!data.AtLeastOne).ToInt(),
				start[2] + data.Folder.ToInt(),
				start[3] + data.Xm8.ToInt(),
				GetData(version, start, data.Data),
				GetPatch(version, start, data.Patch),
				GetMajor(version, start, data.Major),
				GetMinor(version, start, data.Minor),
				GetBuild(version, start, data.Build),
				GetRevision(version, start, data.Revision)
			};

			return exp;

		}	// end GetExpected(List<int>, List<bool>, int)
		
		/// <inheritdoc />
		protected override EmailData GetTestData() => new EmailData(GetRandomBools(8));

		/// <inheritdoc />
		protected override List<int> GetTestStart(int version) => GetRandomInts(version < 2 ? 13 : 10);

		#endregion Overrides of LogsWithDataTestBase<EmailLog, EmailLogData, EmailData>

		#region Helpers

		private static int GetData(int version, List<int> start, bool data)
		{
			var index = GetValueBasedOnVersion(version, 6, 4);
			return start[index] + data.ToInt();

		}	// end GetData(int, List<int>, bool)
		
		private static int GetPatch(int version, List<int> start, bool patch)
		{
			var index = GetValueBasedOnVersion(version, 8, 5);
			return start[index] + patch.ToInt();

		}	// end GetPatch(int, List<int>, bool)

		private static int GetMajor(int version, List<int> start, bool major)
		{
			var index = GetValueBasedOnVersion(version, 9, 6);
			return start[index] + major.ToInt();

		}	// end GetMajor(int, List<int>, bool)

		private static int GetMinor(int version, List<int> start, bool minor)
		{
			var index = GetValueBasedOnVersion(version, 10, 7);
			return start[index] + minor.ToInt();

		}	// end GetMinor(int, List<int>, bool)

		private static int GetBuild(int version, List<int> start, bool build)
		{
			var index = GetValueBasedOnVersion(version, 11, 8);
			return start[index] + build.ToInt();

		}	// end GetBuild(int, List<int>, bool)

		private static int GetRevision(int version, List<int> start, bool revision)
		{
			var index = GetValueBasedOnVersion(version, 12, 9);
			return start[index] + revision.ToInt();

		}	// end GetRevision(int, List<int>, bool)

		private static int GetValueBasedOnVersion(int version, int one, int two) =>
			GetValueBasedOnVersion2(version, 2, one, two);

		#endregion Helpers

	}	// end EmailLogTests

	///
	[ExcludeFromCodeCoverage]
	public class EmailData : TestData
	{
		internal bool Folder { get; }
		internal bool Xm8 { get; }
		internal bool Data { get; }
		internal bool Patch { get; }
		internal bool Major { get; }
		internal bool Minor { get; }
		internal bool Build { get; }
		internal bool Revision { get; }

		internal bool AtLeastOne => Folder || Xm8 || Data || Patch || Major || Minor || Build || Revision;
		
		internal EmailData(List<bool> boolValues)
		{
			Folder = boolValues[0];
			Xm8 = boolValues[1];
			Data = boolValues[2];
			Patch = boolValues[3];
			Major = boolValues[4];
			Minor = boolValues[5];
			Build = boolValues[6];
			Revision = boolValues[7];

		}	// end EmailData(List<bool>, int)

		#region Overrides of TestData

		/// <inheritdoc />
		public override string Str() => $"({Folder}, {Xm8}, {Data}, {Patch}, {Major}, {Minor}, {Build}, {Revision})";

		#endregion Overrides of TestData

	}	// end EmailData

}	// end HubUnitTest.Logging.Logs
