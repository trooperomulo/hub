﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using RyanCoreTests;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging
{
	/// <summary>
	/// class to test <see cref="Logger" /> class
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class LoggerTests
	{
		#region Variables

		private Logger _logger;
		private readonly LoggerFilesStub _file = new LoggerFilesStub();
		private readonly LoggerDirsStub _dirs = new LoggerDirsStub();

		private readonly Type _type = typeof(Logger);

		#endregion Variables

		#region Tests

		///
		[SetUp]
		public void ClearInstance()
		{
			CoreTestUtils.SetPrivateStaticProperty(_type, "Files", null);
			CoreTestUtils.SetPrivateStaticProperty(_type, "Dirs", null);
			CoreTestUtils.SetPrivateStaticProperty(_type, "Name", null);
			CoreTestUtils.SetPrivateStaticField(_type, "_instance", null);
			CoreTestUtils.SetPrivateStaticField(_type, "_pathBase", null);
			CoreTestUtils.SetPrivateStaticField(_type, "_canLog", false);
		}

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<ILogMover>();
		}

		/// <summary>
		/// tests <see cref="Logger.Initialize" />
		/// </summary>
		/// <param name="n">the user's name</param>
		/// <param name="p">the user param</param>
		/// <param name="dir">should the base directory be "created"</param>
		/// <param name="en">the expected name</param>
		/// <param name="ec">the expected can log</param>
		[Test]
		[TestCase("", true, true, null, false)]
		[TestCase("name", false, true, "name", false)]
		[TestCase("name", true, false, "name", true)]
		[TestCase("name", true, true, "name", true)]
		public void SetUp(string n, bool p, bool dir, string en, bool ec)
		{
			var file = new LoggerFilesStub();
			var dirs = new LoggerDirsStub();
			if (dir)
			{
				dirs.CreateDirectory(Logger.LogBase);
			}

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, LoggerFilesStub>(file);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, LoggerDirsStub>(dirs);

			// initialize
			Logger.Instance.Initialize(n, p);

			// get private fields
			var na = Logger.Name;
			var cl = CoreTestUtils.GetPrivateField<bool>(_type, Logger.Instance, "_canLog");

			// verify values
			Assert.AreEqual(en, na);
			Assert.AreEqual(ec, cl);
		}

		/// <summary>
		/// tests case where both the temp and permanent directories exist, but there are no logs to move
		/// </summary>
		[Test]
		public void TestBothDirectoriesExistNoLogs()
		{
			var dirs = new Mock<IDirectories>();
			dirs.Setup(d => d.Exists(AnyString)).Returns(true);
			dirs.Setup(d => d.GetFiles(AnyString, AnyString)).Returns(new List<string>());

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(new Mock<IFiles>().Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<ILogMover, ILogMover>(new Mock<ILogMover>().Object);

			// ReSharper disable StringLiteralTypo
			Logger.Instance.Initialize("jirard", true);

			var path = Logger.GetPath("greg");

			Assert.AreEqual($"{Logger.LogBase}jirard\\greg.hub", path);

			dirs.Verify(d => d.Exists(Logger.LogBase), Times.Once);
			dirs.Verify(d => d.Exists($"{Logger.Temp}jirard"), Times.Once);
			dirs.Verify(d => d.Exists($"{Logger.LogBase}jirard"), Times.Once);
			dirs.Verify(d => d.GetFiles($"{Logger.Temp}jirard", "*.hub"), Times.Once);
			// ReSharper restore StringLiteralTypo
			dirs.VerifyNoOtherCalls();
		}

		/// <summary>
		/// tests <see cref="Logger.GetPath" />
		/// </summary>
		[Test]
		public void TestGetPath()
		{
			SetupLogger();
			Assert.AreEqual($@"{Logger.LogBase}user\hank.hub", Logger.GetPath("hank"));
		}

		/// <summary>
		/// tests needing to move logs when getting the base path
		/// </summary>
		[Test]
		public void TestMovingLogs()
		{
			var mover = new Mock<ILogMover>();
			mover.Setup(m => m.MoveLog(AnyString, AnyString));
			IoCContainer.Instance.RegisterAsSingleInstance<ILogMover, ILogMover>(mover.Object);

			var logs = GetLogsToMove();
			var first = logs[0];

			var dirs = new Mock<IDirectories>();
			dirs.Setup(d => d.Exists(AnyString)).Returns(true);
			dirs.Setup(d => d.GetFiles(AnyString, AnyString)).Returns(logs);

			var files = new Mock<IFiles>();
			files.Setup(f => f.Exists(AnyString)).Returns((string s) => !Equals(s, $"{Logger.LogBase}bill\\{first}"));
			files.Setup(f => f.Copy(AnyString, AnyString));

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);

			Logger.Instance.Initialize("bill", true);

			var path = Logger.GetPath("hank");

			var user = $"{Logger.Temp}bill";

			dirs.Verify(d => d.Exists(Logger.LogBase), Times.Once);
			dirs.Verify(d => d.Exists(user), Times.Once);
			dirs.Verify(d => d.GetFiles(user, "*.hub"), Times.Exactly(2),
				"This should be called to see if logs needs to be moved, and then again to actually move them");
			dirs.Verify(d => d.Exists($"{Logger.LogBase}bill"), Times.Once);

			files.Verify(f => f.Copy(first, $"{Logger.LogBase}bill\\{first}"), Times.Once);

			foreach (var log in logs)
			{
				files.Verify(f => f.Exists($"{Logger.LogBase}bill\\{log}"), Times.Once);
				CoreTestUtils.MoqMethodWasCalled(mover, m => m.MoveLog(log.Replace(".hub", ""), log), !Equals(log, first));
			}

			Assert.AreEqual($"{Logger.LogBase}bill\\hank.hub", path);

			mover.VerifyNoOtherCalls();
			dirs.VerifyNoOtherCalls();
			files.VerifyNoOtherCalls();

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
		}

		/// <summary>
		/// tests case where neither the temp and permanent directories exist
		/// </summary>
		[Test]
		public void TestNoDirectoriesPreExist()
		{
			var dirs = new Mock<IDirectories>();
			dirs.Setup(d => d.Exists(AnyString)).Returns(false);
			dirs.Setup(d => d.CreateDirectory(AnyString));

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(new Mock<IFiles>().Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<ILogMover, ILogMover>(new Mock<ILogMover>().Object);

			Logger.Instance.Initialize("fred", true);

			var path = Logger.GetPath("george");

			Assert.AreEqual($"{Logger.Temp}fred\\george.hub", path);

			dirs.Verify(d => d.Exists(Logger.LogBase), Times.Once);
			dirs.Verify(d => d.Exists(Logger.Temp), Times.Once);
			dirs.Verify(d => d.Exists($"{Logger.Temp}fred"), Times.Once);
			dirs.Verify(d => d.CreateDirectory(Logger.Temp), Times.Once);
			dirs.Verify(d => d.CreateDirectory($"{Logger.Temp}fred"), Times.Once);
			dirs.VerifyNoOtherCalls();
		}

		/// <summary>
		/// tests all the log methods if the Logger is can't log
		/// </summary>
		[Test]
		public void TestUninitialized()
		{
			// call all the log methods
			Logger.Instance.LogAutoLog();
			Logger.Instance.LogBuild(true, true, true, true, true, 5);
			Logger.Instance.LogClean(true, 5, true);
			Logger.Instance.LogDb();
			Logger.Instance.LogEmail(true, true, true, true, true, true, true, true);
			Logger.Instance.LogGo();
			Logger.Instance.LogHelp(5);
			Logger.Instance.LogImport(5, true, true, true, true);
			Logger.Instance.LogKill();
			Logger.Instance.LogLaunch(true, true, 5);
			Logger.Instance.LogOew();
			Logger.Instance.LogOrphan(true, true);
			Logger.Instance.LogParam();
			Logger.Instance.LogParamA(5);
			Logger.Instance.LogParamD(5);
			Logger.Instance.LogParamE(5);
			Logger.Instance.LogParamM(5);
			Logger.Instance.LogParamS();
			Logger.Instance.LogRecon(true, true, 5);
			Logger.Instance.LogRes();
			Logger.Instance.LogResA(Res.Core, 5);
			Logger.Instance.LogResF(Res.Core, 5);
			Logger.Instance.LogResR(Res.Core, 5);
			Logger.Instance.LogResS(Res.Core);
			Logger.Instance.LogResT(Res.Core, 5);
			Logger.Instance.LogRfb();
			Logger.Instance.LogScrum(5, "5.55", 5, 5, 5, true);
			Logger.Instance.LogTeamCity(Platform.Windows, 5);
			Logger.Instance.LogTrickler(true, true, 5);
			Logger.Instance.LogUnit(true, true, true, true, true, true, true, true, true, 5);
			Logger.Instance.LogVs(5);

			// verify that the files weren't created
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.AutoLog)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Build)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Clean)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Db)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Email)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Go)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Help)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Import)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Kill)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Launch)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Oew)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Orphan)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Param)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ParamA)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ParamD)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ParamE)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ParamM)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ParamS)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Recon)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Res)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ResA)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ResF)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ResR)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ResS)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.ResT)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Rfb)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Scrum)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.TeamCity)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Trickler)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.UnitTest)));
			Assert.IsFalse(_file.Exists(Logger.GetPath(Logger.Vs)));
		}

		#endregion Tests

		#region Helpers

		private static string AnyString => It.IsAny<string>();

		private static List<string> GetLogsToMove()
		{
			var temp = Rnd.GetRandomListString(10, 20);
			var logs = new List<string>();

			foreach (var log in temp)
			{
				logs.Add($"{log}.hub");
			}

			return logs;
		}

		private void SetupLogger()
		{
			if (_logger == null)
			{
				_dirs.CreateDirectory(Logger.LogBase);
				IoCContainer.Instance.RegisterAsSingleInstance<IFiles, LoggerFilesStub>(_file);
				IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, LoggerDirsStub>(_dirs);
				Logger.Instance.Initialize("user", true);
				_logger = Logger.Instance;
			}
		}

		#endregion Helpers

		#region Inner Classes

		internal class LoggerFilesStub : BaseFileStub
		{
			private readonly Dictionary<string, List<string>> _created = new Dictionary<string, List<string>>();

			public override void AppendAllLines(string path, List<string> lines)
			{
				if (!_created.ContainsKey(path))
				{
					WriteAllLines(path, lines);
					return;
				}

				_created[path].AddRange(lines);
			}

			public override void Delete(string path)
			{
				if (_created.ContainsKey(path))
				{
					_created.Remove(path);
				}
			}

			public override bool Exists(string path) => _created.ContainsKey(path);

			public override IEnumerable<string> ReadLines(string path) => _created.ContainsKey(path) ? _created[path] : new List<string>();

			public override void WriteAllLines(string path, IEnumerable<string> lines)
			{
				_created[path] = lines.ToList();
			}
		}

		private class LoggerDirsStub : BaseDirsStub
		{
			private readonly List<string> _created = new List<string>();

			public override void CreateDirectory(string path)
			{
				if (!_created.Contains(path))
				{
					_created.Add(path);
				}
			}

			public override bool Exists(string path) => _created.Contains(path);
		}

		#endregion Inner Classes
	}
}
