﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Castle.Core.Internal;

using Hub.Interface;
using Hub.Logging;

using Moq;

using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace HubUnitTest.Logging
{
	/// <summary>
	/// base class for testing logs with data
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class LogsWithDataTestBase<L, D, T> : LoggingTestsBase
		where L : LogFile<D>
		where D : ILogData
		where T : TestData
	{
		#region Tests

		/// <summary>
		/// tests the constructor that takes a string
		/// </summary>
		[Test]
		public void TestStringConstructor()
		{
			var path = RandomData.GetRandomStr();
			var log = new Mock<L>(path);
			log.Setup(l => l.LoadFromLogFile(It.IsAny<string>(), It.Is<D>(d => CheckData(d))));

			// ReSharper disable once UnusedVariable - We need to access Object so the constructor will be called
			var instance = log.Object;

			log.Verify(l => l.LoadFromLogFile(path, It.IsAny<D>()));

		}	// end TestStringConstructor()
		
		/// <summary>
		/// tests that multiple calls result in accurate numbers
		/// </summary>
		[Test]
		public void TestMultipleCalls()
		{
			var start = GetTestStart(V);
			var startHours = GetStartHours();

			var data1 = GetTestData();
			var data2 = GetTestData();
			var data3 = GetTestData();

			void Round1() => DoLog(data1);
			void Round2() => DoLog(data2);
			void Round3() => DoLog(data3);

			GetExpected(start, startHours, data1, data2, data3, out var exp1, out var exp2, out var exp3);
			var sHours = HoursString(startHours);

			string StartData()
			{
				var ints = StartToString(start);
				var sHoursString = sHours.IsNullOrEmpty() ? "" : $", {sHours}";

				var r1 = data1.Str();
				var r2 = data2.Str();
				var r3 = data3.Str();

				var nl = Environment.NewLine;
				return $"{nl}Start: ({ints}{sHoursString}); R1: {r1}; R2: {r2}; R3: {r3}{nl}";

			}	// end GetStartData()

			TestMultipleCalls(start, sHours, Round1, Round2, Round3, exp1, exp2, exp3, StartData);

		}	// end TestMultipleCalls()

		/// <summary>
		/// tests the case when there isn't already a log file
		/// </summary>
		[Test]
		public void TestNo()
		{
			var start = GetTestStart(V);
			EmptyTheList(start);
			var hours = GetStartHours();
			if (hours > -1.5)
			{
				hours = -1;
			}	// end if

			var data = GetTestData();

			void Call() => DoLog(data);

			var exp = GetExpected(V, start, hours, data);

			TestNo(Call, exp);

		}	// end TestNo()

		/// <summary>
		/// tests that we don't process a log with a future version
		/// </summary>
		[Test]
		public void TestV999() => TestV999(() => DoLog(GetTestData()));

		///
		[TestCaseSource(nameof(GetLogTests))]
		public void TestV(int version, Action<List<int>, string, Action, List<string>, Func<string>> test) => DoVTest(version, test);

		#endregion Tests
		
		#region Helpers
		
		private void DoVTest(int version, Action<List<int>, string, Action, List<string>, Func<string>> testCall)
		{
			var start = GetTestStart(version);
			var hours = GetStartHours();

			var data = GetTestData();

			void Call() => DoLog(data);

			var exp = GetExpected(version, start, hours, data);

			var startHours = HoursString(hours);

			testCall(start, startHours, Call, exp, () => GetStartData(start, startHours, data));

		}	// end DoVTest(int, Action<List<int>, Action, List<int>>)

		private void GetExpected(List<int> start, double hours, T data1, T data2, T data3, out List<string> exp1,
			out List<string> exp2, out List<string> exp3)
		{
			var ints1 = GetExpectedInts(V, start, data1);
			var ints2 = GetExpectedInts(V, ints1, data2);
			var ints3 = GetExpectedInts(V, ints2, data3);

			var hours1 = AddHours(data1, hours);
			var hours2 = AddHours(data2, hours1);
			var hours3 = AddHours(data3, hours2);

			exp1 = IntsToStrings(ints1, HoursString(hours1));
			exp2 = IntsToStrings(ints2, HoursString(hours2));
			exp3 = IntsToStrings(ints3, HoursString(hours3));

		}	// end GetExpected(List<int>, T, T, T, out List<string>, out List<string>, out List<string>)

		///
		protected List<string> GetExpected(int version, List<int> start, double hours, T data) =>
			IntsToStrings(GetExpectedInts(version, start, data), HoursString(AddHours(data, hours)));

		///
		public static IEnumerable GetLogTests() => GetTests();

		///
		protected string GetStartData(List<int> start, string hours, T data)
		{
			var nl = Environment.NewLine;
			var sHoursString = hours.IsNullOrEmpty() ? "" : $", {hours}";
			return $"{nl}{StartToString(start)}{sHoursString}; {data.Str()}{nl}";

		}	// end GetStart(List<int>, T)

		private static string HoursString(double hours)
		{
			if (hours < -1.5)
			{
				return null;
			}	// end if

			if (hours < -.5)
			{
				return "0";
			}	// end if

			return $"{hours}";

		}	// end HoursString(double)

		private static string StartToString(List<int> start)
		{
			var ints = start.Aggregate("", (current, i) => current + ", " + i);
			return ints.Substring(2);

		}	// end StartToString(List<int>)

		#endregion Helpers

		#region Abstract Methods

		///
		protected virtual double AddHours(T data, double hours) => -2;

		///
		protected abstract bool CheckData(D data);
		
		///
		protected abstract void DoLog(T data);

		///
		protected abstract List<int> GetExpectedInts(int version, List<int> start, T data);

		///
		protected virtual double GetStartHours() => -2;

		///
		protected abstract T GetTestData();

		///
		protected abstract List<int> GetTestStart(int version);

		#endregion Abstract Methods

	}	// end LogsWithDataTestBase<L, D>

	///
	[ExcludeFromCodeCoverage]
	public abstract class TestData
	{
		///
		public abstract string Str();
	}	// end TestData

}	// end HubUnitTest.Logging
