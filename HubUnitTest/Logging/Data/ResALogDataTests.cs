﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging.Data;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ResALogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResALogDataTests : LogDataBaseTests<ResALogData>
	{
		#region Overrides of LogDataBaseTests<ResALogData>

		/// <inheritdoc />
		protected override int Count => 8;

		/// <inheritdoc />
		protected override ResALogData GetEmptyData() => ResALogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ResALogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Core);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Shared);
			Assert.AreEqual(0, data.NoSol);
			Assert.AreEqual(0, data.File);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<ResALogData>
	}
}
