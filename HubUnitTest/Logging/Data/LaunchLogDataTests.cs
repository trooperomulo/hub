﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="LaunchLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class LaunchLogDataTests : LogDataBaseTests<LaunchLogData>
	{
		#region Overrides of LogDataBaseTests<LaunchLogData>

		/// <inheritdoc />
		protected override int Count => 6;

		/// <inheritdoc />
		protected override LaunchLogData GetEmptyData() => LaunchLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(LaunchLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Trick);
			Assert.AreEqual(0, data.Test);
			Assert.AreEqual(0, data.None);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<LaunchLogData>
	}
}
