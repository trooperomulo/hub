﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="HelpLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class HelpLogDataTests : LogDataBaseTests<HelpLogData>
	{
		#region Overrides of LogDataBaseTests<HelpLogData>

		/// <inheritdoc />
		protected override int Count => 4;

		/// <inheritdoc />
		protected override HelpLogData GetEmptyData() => HelpLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(HelpLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.General);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<HelpLogData>
	}
}
