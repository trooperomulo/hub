﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="OrphanLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class OrphanLogDataTests : LogDataBaseTests<OrphanLogData>
	{
		#region Overrides of LogDataBaseTests<OrphanLogData>

		/// <inheritdoc />
		protected override int Count => 4;

		/// <inheritdoc />
		protected override OrphanLogData GetEmptyData() => OrphanLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(OrphanLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Data);
		}

		#endregion Overrides of LogDataBaseTests<OrphanLogData>
	}
}
