﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="TricklerLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TricklerLogDataTests : LogDataBaseTests<TricklerLogData>
	{
		#region Overrides of LogDataBaseTests<TricklerLogData>

		/// <inheritdoc />
		protected override int Count => 6;

		/// <inheritdoc />
		protected override TricklerLogData GetEmptyData() => TricklerLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(TricklerLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Unit);
			Assert.AreEqual(0, data.None);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<TricklerLogData>
	}
}
