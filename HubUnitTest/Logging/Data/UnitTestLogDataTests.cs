﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="UnitTestLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UnitTestLogDataTests : LogDataBaseTests<UnitTestLogData>
	{
		#region Overrides of LogDataBaseTests<UnitTestLogData>

		/// <inheritdoc />
		protected override int Count => 13;

		/// <inheritdoc />
		protected override UnitTestLogData GetEmptyData() => UnitTestLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(UnitTestLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.EstimateTime);
			Assert.AreEqual(0, data.Progress);
			Assert.AreEqual(0, data.ShowTime);
			Assert.AreEqual(0, data.Console);
			Assert.AreEqual(0, data.Text);
			Assert.AreEqual(0, data.Xml);
			Assert.AreEqual(0, data.Input);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Trick);
			Assert.AreEqual(0, data.NoSols);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<UnitTestLogData>
	}
}
