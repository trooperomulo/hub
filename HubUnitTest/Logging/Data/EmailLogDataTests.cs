﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging.Data;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="EmailLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class EmailLogDataTests : LogDataBaseTests<EmailLogData>
	{
		#region Overrides of LogDataBaseTests<EmailLogData>

		/// <inheritdoc />
		protected override int Count => 11;

		/// <inheritdoc />
		protected override EmailLogData GetEmptyData() => EmailLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(EmailLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.None);
			Assert.AreEqual(0, data.Folder);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Data);
			Assert.AreEqual(0, data.Patch);
			Assert.AreEqual(0, data.Major);
			Assert.AreEqual(0, data.Minor);
			Assert.AreEqual(0, data.Build);
			Assert.AreEqual(0, data.Revis);

		}	// end VerifyDataIsEmpty(EmailLogData)

		#endregion Overrides of LogDataBaseTests<EmailLogData>

	}	// end EmailLogDataTests

}	// end HubUnitTest.Logging.Data
