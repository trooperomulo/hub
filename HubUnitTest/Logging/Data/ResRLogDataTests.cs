﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ResRLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResRLogDataTests : LogDataBaseTests<ResRLogData>
	{
		#region Overrides of LogDataBaseTests<ResRLogData>

		/// <inheritdoc />
		protected override int Count => 7;

		/// <inheritdoc />
		protected override ResRLogData GetEmptyData() => ResRLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ResRLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Core);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Shared);
			Assert.AreEqual(0, data.NoSol);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<ResRLogData>
	}
}
