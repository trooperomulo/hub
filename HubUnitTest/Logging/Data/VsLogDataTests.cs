﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="VsLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class VsLogDataTests : LogDataBaseTests<VsLogData>
	{
		#region Overrides of LogDataBaseTests<VsLogData>

		/// <inheritdoc />
		protected override int Count => 4;

		/// <inheritdoc />
		protected override VsLogData GetEmptyData() => VsLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(VsLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.One);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<VsLogData>
	}
}
