﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ResSLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResSLogDataTests : LogDataBaseTests<ResSLogData>
	{
		#region Overrides of LogDataBaseTests<ResSLogData>

		/// <inheritdoc />
		protected override int Count => 6;

		/// <inheritdoc />
		protected override ResSLogData GetEmptyData() => ResSLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ResSLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Core);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Shared);
			Assert.AreEqual(0, data.NoSol);
		}

		#endregion Overrides of LogDataBaseTests<ResSLogData>
	}
}
