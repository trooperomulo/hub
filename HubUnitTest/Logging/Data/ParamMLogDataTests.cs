﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ParamMLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamMLogDataTests : LogDataBaseTests<ParamMLogData>
	{
		#region Overrides of LogDataBaseTests<ParamMLogData>

		/// <inheritdoc />
		protected override int Count => 3;

		/// <inheritdoc />
		protected override ParamMLogData GetEmptyData() => ParamMLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ParamMLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Num);

		}	// end VerifyDataIsEmpty(ParamMLogData)

		#endregion Overrides of LogDataBaseTests<ParamMLogData>

	}	// end ParamMLogDataTests

}	// end HubUnitTest.Logging.Data
