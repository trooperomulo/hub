﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ParamDLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamDLogDataTests : LogDataBaseTests<ParamDLogData>
	{
		#region Overrides of LogDataBaseTests<ParamDLogData>

		/// <inheritdoc />
		protected override int Count => 3;

		/// <inheritdoc />
		protected override ParamDLogData GetEmptyData() => ParamDLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ParamDLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Num);

		}	// end VerifyDataIsEmpty(ParamDLogData)

		#endregion Overrides of LogDataBaseTests<ParamDLogData>

	}	// end ParamDLogDataTests

}	// end HubUnitTest.Logging.Data
