﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging.Data;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="BuildLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class BuildLogDataTests : LogDataBaseTests<BuildLogData>
	{
		#region Overrides of LogDataBaseTests<BuildLogData>

		/// <inheritdoc />
		protected override int Count => 10;

		/// <inheritdoc />
		protected override BuildLogData GetEmptyData() => BuildLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(BuildLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Prod);
			Assert.AreEqual(0, data.Debug);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Trick);
			Assert.AreEqual(0, data.Unit);
			Assert.AreEqual(0, data.One);
			Assert.AreEqual(0, data.All);
			Assert.AreEqual(0, data.Sols);
		}

		#endregion Overrides of LogDataBaseTests<BuildLogData>
	}
}
