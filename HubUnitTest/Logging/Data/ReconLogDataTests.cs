﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ReconLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ReconLogDataTests : LogDataBaseTests<ReconLogData>
	{
		#region Overrides of LogDataBaseTests<ReconLogData>

		/// <inheritdoc />
		protected override int Count => 6;

		/// <inheritdoc />
		protected override ReconLogData GetEmptyData() => ReconLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ReconLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Trick);
			Assert.AreEqual(0, data.None);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<ReconLogData>
	}
}
