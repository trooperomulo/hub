﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ImportLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ImportLogDataTests : LogDataBaseTests<ImportLogData>
	{
		#region Overrides of LogDataBaseTests<ImportLogData>

		/// <inheritdoc />
		protected override int Count => 7;

		/// <inheritdoc />
		protected override ImportLogData GetEmptyData() => ImportLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ImportLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Import);
			Assert.AreEqual(0, data.Delete);
			Assert.AreEqual(0, data.Days);
			Assert.AreEqual(0, data.User);
			Assert.AreEqual(0, data.Status);
		}

		#endregion Overrides of LogDataBaseTests<ImportLogData>
	}
}
