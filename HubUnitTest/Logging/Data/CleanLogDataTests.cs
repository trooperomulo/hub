﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="CleanLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class CleanLogDataTests : LogDataBaseTests<CleanLogData>
	{
		#region Overrides of LogDataBaseTests<CleanLogData>

		/// <inheritdoc />
		protected override int Count => 5;

		/// <inheritdoc />
		protected override CleanLogData GetEmptyData() => CleanLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(CleanLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Quiet);
			Assert.AreEqual(0, data.Batch);
			Assert.AreEqual(0, data.Deflt);
		}

		#endregion Overrides of LogDataBaseTests<CleanLogData>
	}
}
