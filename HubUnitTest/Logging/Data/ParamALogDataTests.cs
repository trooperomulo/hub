﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ParamALogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamALogDataTests : LogDataBaseTests<ParamALogData>
	{
		#region Overrides of LogDataBaseTests<ParamALogData>

		/// <inheritdoc />
		protected override int Count => 3;

		/// <inheritdoc />
		protected override ParamALogData GetEmptyData() => ParamALogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ParamALogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Num);

		}	// end VerifyDataIsEmpty(ParamALogData)

		#endregion Overrides of LogDataBaseTests<ParamALogData>

	}	// end ParamALogDataTests

}	// end HubUnitTest.Logging.Data
