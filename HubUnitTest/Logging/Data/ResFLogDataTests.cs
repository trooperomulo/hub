﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ResFLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResFLogDataTests : LogDataBaseTests<ResFLogData>
	{
		#region Overrides of LogDataBaseTests<ResFLogData>

		/// <inheritdoc />
		protected override int Count => 8;

		/// <inheritdoc />
		protected override ResFLogData GetEmptyData() => ResFLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ResFLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Core);
			Assert.AreEqual(0, data.Xm8);
			Assert.AreEqual(0, data.Shared);
			Assert.AreEqual(0, data.NoSol);
			Assert.AreEqual(0, data.All);
			Assert.AreEqual(0, data.Num);
		}

		#endregion Overrides of LogDataBaseTests<ResFLogData>
	}
}
