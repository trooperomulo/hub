﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// base class for testing log data classes
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class LogDataBaseTests<T>
		where T : ILogData
	{
		#region Property

		/// <summary>
		/// how many lines the file should have
		/// </summary>
		protected abstract int Count { get; }

		#endregion  Property

		#region Tests

		/// <summary>
		/// verifies that the add function doesn't add when the wrong number of lines are passed in
		/// </summary>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void TestAddWithBadLines(bool more)
		{
			var empty = GetEmptyData();
			var count = Count + (more ? 1 : -1);
			var lines = new List<string>();
			for (var i = 1; i <= count; i++)
			{
				lines.Add(i.ToString());
			}

			empty.Add(lines.ToArray());
			VerifyDataIsEmpty(empty);

		}	// end TestWithBadLines(bool)

		/// <summary>
		/// verifies that the Empty method creates an empty data object
		/// </summary>
		[Test]
		public void TestEmpty() => VerifyDataIsEmpty(GetEmptyData());

		/// <summary>
		/// verifies that the correct number of lines are returned
		/// </summary>
		[Test]
		public void TestLineCount()
		{
			Assert.AreEqual(Count - 1, GetLineCount());

		}	// end TestLineCount()

		#endregion Tests

		#region Helpers

		/// <summary>
		/// gets an empty data
		/// </summary>
		protected abstract T GetEmptyData();

		/// <summary>
		/// gets the number of lines
		/// </summary>
		protected virtual int GetLineCount() => GetEmptyData().ToInts().Length;

		/// <summary>
		/// verifies that the data is actually empty
		/// </summary>
		protected abstract void VerifyDataIsEmpty(T data);
		
		#endregion Helpers

	}	// end LogDataBaseTests

}	// end HubUnitTest.Logging.Data
