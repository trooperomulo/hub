﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ParamLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamLogDataTests : LogDataBaseTests<ParamLogData>
	{
		#region Overrides of LogDataBaseTests<ParamLogData>

		/// <inheritdoc />
		protected override int Count => 8;

		/// <inheritdoc />
		protected override ParamLogData GetEmptyData() => ParamLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ParamLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.NoAct);
			Assert.AreEqual(0, data.NoAdd);
			Assert.AreEqual(0, data.BadAdd);
			Assert.AreEqual(0, data.NoDel);
			Assert.AreEqual(0, data.NoMod);
			Assert.AreEqual(0, data.BadMod);

		}	// end VerifyDataIsEmpty(ParamLogData)

		#endregion Overrides of LogDataBaseTests<ParamLogData>

	}	// end ParamLogDataTests

}	// end HubUnitTest.Logging.Data
