﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ParamELogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ParamELogDataTests : LogDataBaseTests<ParamELogData>
	{
		#region Overrides of LogDataBaseTests<ParamELogData>

		/// <inheritdoc />
		protected override int Count => 4;

		/// <inheritdoc />
		protected override ParamELogData GetEmptyData() => ParamELogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ParamELogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Num);
			Assert.AreEqual(0, data.None);

		}	// end VerifyDataIsEmpty(ParamELogData)

		#endregion Overrides of LogDataBaseTests<ParamELogData>

	}	// end ParamELogDataTests

}	// end HubUnitTest.Logging.Data
