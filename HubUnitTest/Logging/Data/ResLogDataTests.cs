﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging.Data;

using NUnit.Framework;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ResLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResLogDataTests : LogDataBaseTests<ResLogData>
	{
		#region Overrides of LogDataBaseTests<ResLogData>

		/// <inheritdoc />
		protected override int Count => 9;

		/// <inheritdoc />
		protected override ResLogData GetEmptyData() => ResLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ResLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.NoAct);
			Assert.AreEqual(0, data.MultSol);
			Assert.AreEqual(0, data.NoPath);
			Assert.AreEqual(0, data.BadPairs);
			Assert.AreEqual(0, data.NoRKeys);
			Assert.AreEqual(0, data.NoFVals);
			Assert.AreEqual(0, data.NoTVals);
		}

		#endregion Overrides of LogDataBaseTests<ResLogData>
	}
}
