﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging.Data;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="ScrumLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ScrumLogDataTests : LogDataBaseTests<ScrumLogData>
	{
		#region Overrides of LogDataBaseTests<ScrumLogData>

		/// <inheritdoc />
		protected override int Count => 14;

		/// <inheritdoc />
		protected override ScrumLogData GetEmptyData() => ScrumLogData.Empty();

		/// <inheritdoc />
		protected override int GetLineCount() => GetEmptyData().GetLines().Count;

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(ScrumLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Blank);
			Assert.AreEqual(0, data.Future);
			Assert.AreEqual(0, data.NumNoD);
			Assert.AreEqual(0, data.NumNoH);
			Assert.AreEqual(0, data.NumNoW);
			Assert.AreEqual(0, data.NumNoB);
			Assert.AreEqual(0, data.NumNoT);
			Assert.AreEqual(0, data.NumDid);
			Assert.AreEqual(0, data.NumWill);
			Assert.AreEqual(0, data.NumBlock);
			Assert.AreEqual(0, data.NumTasks);
			Assert.AreEqual(0, data.Hours);

		}	// end VerifyDataIsEmpty(ScrumLogData)

		#endregion Overrides of LogDataBaseTests<ScrumLogData>

	}	// end ScrumLogDataTests

}	// end HubUnitTest.Logging.Data
