﻿using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Logging.Data;

namespace HubUnitTest.Logging.Data
{
	/// <summary>
	/// Tests <see cref="TeamCityLogData" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TeamCityLogDataTests : LogDataBaseTests<TeamCityLogData>
	{
		#region Overrides of LogDataBaseTests<TeamCityLogData>

		/// <inheritdoc />
		protected override int Count => 8;

		/// <inheritdoc />
		protected override TeamCityLogData GetEmptyData() => TeamCityLogData.Empty();

		/// <inheritdoc />
		protected override void VerifyDataIsEmpty(TeamCityLogData data)
		{
			Assert.AreEqual(0, data.Total);
			Assert.AreEqual(0, data.Windows);
			Assert.AreEqual(0, data.Android);
			Assert.AreEqual(0, data.Ios);
			Assert.AreEqual(0, data.NoPlat);
			Assert.AreEqual(0, data.Branches);
			Assert.AreEqual(0, data.Num);

		}	// end VerifyDataIsEmpty(TeamCityLogData)

		#endregion Overrides of LogDataBaseTests<TeamCityLogData>

	}	// end TeamCityLogDataTests

}	// end HubUnitTest.Logging.Data
