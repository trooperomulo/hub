﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Interface;
using Hub.Logging;

using Moq;

using NUnit.Framework;

using RyanCoreTests;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging
{
	/// <summary>
	/// helper class for testing log classes that only log total times called
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class LogJustTotalTests<T> : LoggingTestsBase
		where T : LogJustTotalFile
	{
		#region Abstract Member

		/// <summary>
		/// the logging method to be called
		/// </summary>
		protected abstract Action Log { get; }

		#endregion Abstract Member

		#region Tests

		/// <summary>
		/// tests that multiple calls result is accurate numbers
		/// </summary>
		[TestCaseSource(nameof(GetTestData))]
		public void TestMultipleCalls(int total)
		{
			var start = new List<int> {total};

			var exp1 = StringList(CV, total + 1);
			var exp2 = StringList(CV, total + 2);
			var exp3 = StringList(CV, total + 3);

			TestMultipleCalls(start, null, Log, Log, Log, exp1, exp2, exp3, () => $"{total}");

		}	// end TestMultipleCalls(int)

		/// <summary>
		/// tests there isn't already a log file
		/// </summary>
		[Test]
		public void TestNo() => TestNo(Log, StringList(CV, 1));

		/// <summary>
		/// tests reading the v files
		/// </summary>
		[TestCaseSource(nameof(GetVTestData))]
		public void TestV(int version, Action<List<int>, string, Action, List<string>, Func<string>> test, int total) => DoVTest(test, total);

		/// <summary>
		/// tests that we don't process a log with a future version
		/// </summary>
		[Test]
		public void TestV999() => TestV999(Log);

		/// <summary>
		/// tests the constructor that takes a temp path
		/// </summary>
		[TestCaseSource(nameof(GetTempPathTestData))]
		public void TestConstructorTempPath(int version)
		{
			var tempPath = Rnd.GetRandomStr();
			var lines = GetLines(version, out var num);
			var files = new Mock<IFiles>();
			files.Setup(f => f.ReadLines(It.IsAny<string>())).Returns(lines);
			files.Setup(f => f.Delete(It.IsAny<string>()));

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, IFiles>(files.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(new Mock<IDirectories>().Object);

			Logger.Instance.Initialize("whatever", true);

			var just = new Mock<T>(tempPath) {CallBase = true};

			var obj = just.Object;

			files.Verify(f => f.ReadLines(tempPath));
			files.Verify(f => f.Delete(tempPath));

			var count = CoreTestUtils.GetPrivateField<int>(typeof(LogJustTotalFile), obj, "_count");
			Assert.AreEqual(num, count, "_count was wrong");

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();

		}	// end TestConstructorTempPath(int)

		#endregion Tests

		#region Helpers

		private void DoVTest(Action<List<int>, string, Action, List<string>, Func<string>> test, int total)
		{
			var start = new List<int> {total};

			var exp = StringList(CV, total + 1);

			test(start, null, Log, exp, () => $"{total}");

		}	// end DoVTest(int, Action<List<int>, Action, List<string>, Func<string>>)

		private List<string> GetLines(int version, out int num)
		{
			var random = Rnd.GetRandomStr();
			num = random.ToInt();
			var results = new List<string> { random };
			if (version != 0)
			{
				results.Insert(0, $"v{version}");
			}	// end if

			return results;

		}	// end GetLines(int, out int)

		///
		public static IEnumerable GetTempPathTestData()
		{
			for (var i = 0; i <= V; i++)
			{
				yield return new TestCaseData(i);
			}	// end for

		}	// end GetTempPathTestData()

		///
		public static IEnumerable GetTestData()
		{
			yield return new TestCaseData(1).SetName("One");
			yield return new TestCaseData(Rnd.Int(2, 10)).SetName("Below 10");
			yield return new TestCaseData(Rnd.Int(11, 101)).SetName("Between 11-100");
			yield return new TestCaseData(Rnd.Int(101, 1001)).SetName("Between 101-1000");

		}	// end GetTestData()

		///
		public static IEnumerable GetVTestData() => UnitTestUtil.CombineTestData(GetTests(), GetTestData());

		#endregion Helpers

	}	// end LogJustTotalTests<T>

}	// end HubUnitTests
