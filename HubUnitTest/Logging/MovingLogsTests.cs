﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Interface;
using Hub.Logging;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

using CTU = RyanCoreTests.CoreTestUtils;
using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest.Logging
{
	/// <summary>
	/// tests moving logs from the temp to the permanent location (including <see cref="LogMover" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class MovingLogsTests
	{
		#region Variable

		private readonly Dictionary<string, List<string>> _exp = new Dictionary<string, List<string>>();

		#endregion Variable

		#region Tests

		///
		[SetUp]
		public void ClearInstance()
		{
			var type = typeof(Logger);
			CTU.SetPrivateStaticProperty(type, "Files", null);
			CTU.SetPrivateStaticProperty(type, "Dirs", null);
			CTU.SetPrivateStaticProperty(type, "Name", null);
			CTU.SetPrivateStaticField(type, "_instance", null);
			CTU.SetPrivateStaticField(type, "_pathBase", null);

		}	// end ClearInstance()

		/// <summary>
		/// tests moving logs
		/// </summary>
		/// <param name="tempVersion">the version of the temporary file</param>
		/// <param name="permVersion">the version of the permanent file, -1 for the file not already being there</param>
		[Test]
		[TestCaseSource(nameof(GetTestData))]
		//[TestCase(0,0)] - Used to debug a specific case
		public void TestMoving(int tempVersion, int permVersion)
		{
			var names = GetLogNames();		// for example, auto.hub, vs.hub
			var dirs = new Mock<IDirectories>();
			dirs.Setup(d => d.Exists(AnyString)).Returns(true);	// say that any directory exists
			dirs.Setup(d => d.GetFiles(AnyString, AnyString)).Returns(names);	// return the file names

			#region GetIFiles
			LogMovingFiles GetIFiles()
			{
				var f = new LogMovingFiles();

				DoLinesAu(f, tempVersion, permVersion);
				DoLinesBl(f, tempVersion, permVersion);
				DoLinesCl(f, tempVersion, permVersion);
				DoLinesDb(f, tempVersion, permVersion);
				DoLinesEm(f, tempVersion, permVersion);
				DoLinesGo(f, tempVersion, permVersion);
				DoLinesHl(f, tempVersion, permVersion);
				DoLinesIm(f, tempVersion, permVersion);
				DoLinesKl(f, tempVersion, permVersion);
				DoLinesLa(f, tempVersion, permVersion);
				DoLinesOe(f, tempVersion, permVersion);
				DoLinesOr(f, tempVersion, permVersion);
				DoLinesPa(f, tempVersion, permVersion);
				DoLinesPd(f, tempVersion, permVersion);
				DoLinesPe(f, tempVersion, permVersion);
				DoLinesPm(f, tempVersion, permVersion);
				DoLinesPr(f, tempVersion, permVersion);
				DoLinesPs(f, tempVersion, permVersion);
				DoLinesRa(f, tempVersion, permVersion);
				DoLinesRb(f, tempVersion, permVersion);
				DoLinesRc(f, tempVersion, permVersion);
				DoLinesRe(f, tempVersion, permVersion);
				DoLinesRf(f, tempVersion, permVersion);
				DoLinesRr(f, tempVersion, permVersion);
				DoLinesRs(f, tempVersion, permVersion);
				DoLinesRt(f, tempVersion, permVersion);
				DoLinesSc(f, tempVersion, permVersion);
				DoLinesTc(f, tempVersion, permVersion);
				DoLinesTr(f, tempVersion, permVersion);
				DoLinesUt(f, tempVersion, permVersion);
				DoLinesVs(f, tempVersion, permVersion);

				return f;
			}	// end GetIFiles()
			#endregion GetIFiles

			var files = GetIFiles();

			// register the implementations
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, LogMovingFiles>(files);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, IDirectories>(dirs.Object);
			IoCContainer.Instance.RegisterAsSingleInstance<ILogMover, LogMover>(new LogMover());

			Logger.Instance.Initialize("Harold", true);

			Logger.GetPath("eh");	// calling GetPath is what will move any file that needs moved

			// I'm not going to verify the path, or what was called on Dirs, because that is verified in other tests

			var errors = "";
			var noPerm = permVersion == -1;

			foreach (var name in names)
			{
				// verify IFiles.Copy was called properly
				CTU.AddError(noPerm, files.Copied.TryGetValue(name, out var dest), $"{name} wasn't copied properly",
					ref errors);
				CTU.AddError(noPerm ? Perm(name.Replace(".hub", "")) : null, dest,
					$"{name} was copied to the wrong destination", ref errors);

				// verify the correct lines were written out
				var message = CTU.TestLists(_exp[name], files.WrittenLines[Perm(name.Replace(".hub", ""))].ToList());
				CTU.AddError(message.IsNullOrEmpty(),
					$"{name} had the wrong expected lines:{Environment.NewLine}{message}", ref errors);
				
				// verify IFiles.Delete was called properly
				CTU.AddError(noPerm, !files.Deleted.Contains(name), $"{name} wasn't deleted properly", ref errors);
			}

			Assert.IsTrue(errors.IsNullOrEmpty(), errors);

			_exp.Clear();
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<ILogMover>();

		}	// end TestMoving(int, int)

		/// <summary>
		/// tests calling <see cref="LogMover" /> with a bad log file name
		/// </summary>
		[Test]
		public void TestBadLog()
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

			var name = Rnd.GetRandomStr();

			var mover = new LogMover();
			mover.MoveLog(name, "");

			Assert.AreEqual($"I don't know what to do with a log file titled '{name}'\r\n", cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestBadLog()

		#endregion Tests

		#region Helpers

		private static string AnyString => It.IsAny<string>();

		private void DoLines(IFiles files, int tempVers, int permVers, Func<int, List<int>> getLines, string log,
			Action<List<int>, List<int>> prepper = null)
		{
			var temp = getLines(tempVers);
			var perm = getLines(permVers);

			HandleTempAndPerm(files, tempVers, permVers, temp, perm, log);

			// prepper is a means for a specific log to modify the lines to accomodate changes between versions
			// i.e., it should reflect the work done by any UpgradeVXToVY method
			prepper?.Invoke(temp, perm);
			DoExpected(temp, perm, log, tempVers, permVers);

		}	// end DoLines(IFiles, int, int, Func<int, List<int>>, string, Action<List<int>, List<int>>)

		private void DoExpected(List<double> temp, List<double> perm, string log, int tempVers, int permVers) => DoExpected(temp, perm, (a, b) => a + b, log, tempVers, permVers);

		private void DoExpected(List<int> temp, List<int> perm, string log, int tempVers, int permVers) => DoExpected(temp, perm, (a, b) => a + b, log, tempVers, permVers);

		private void DoExpected<T>(List<T> temp, List<T> perm, Func<T, T, T> adder, string log, int tempVers, int permVers)
		{
			var version = permVers == -1 ? tempVers : LogFile<EhData>.Current;
			var expected = new List<string>();
			if (version > 0)
			{
				expected.Add($"v{version}");
			}	// end if

			var count = temp.Count;
			for (var i = 0; i < count; i++)
			{
				var sum = perm == null ? temp[i] : adder(temp[i], perm[i]);
				expected.Add($"{sum}");
			}	// end for

			_exp.Add(Temp(log), expected);

		}	// end DoExpected<T>(List<T>, List<T>, Func<T, T, T>, string, int, int)

		private static List<string> GetLogNames(string file = "")
		{
			if (!file.IsNullOrEmpty())
			{
				// used to debug a specific case
				return new List<string> {$"{file}.hub"};
			}

			var names = new List<string>
			{
				Logger.AutoLog,
				Logger.Build,
				Logger.Clean,
				Logger.Db,
				Logger.Email,
				Logger.Go,
				Logger.Help,
				Logger.Import,
				Logger.Kill,
				Logger.Launch,
				Logger.Oew,
				Logger.Orphan,
				Logger.Param,
				Logger.ParamA,
				Logger.ParamD,
				Logger.ParamE,
				Logger.ParamM,
				Logger.ParamS,
				Logger.Recon,
				Logger.Res,
				Logger.ResA,
				Logger.ResF,
				Logger.ResR,
				Logger.ResS,
				Logger.ResT,
				Logger.Rfb,
				Logger.Scrum,
				Logger.TeamCity,
				Logger.Trickler,
				Logger.UnitTest,
				Logger.Vs
			};

			for (var i = 0; i < names.Count; i++)
			{
				names[i] = $"{names[i]}.hub";
			}

			return names;

		}	// end GetLogNames()

		private static IEnumerable GetTestData()
		{
			var max = LogFile<EhData>.Current;
			for (var i = 0; i <= max; i++)
			{
				for (var j = -1; j <= max; j++)
				{
					// the version of the temporary file, the version of the permanent file
					// -1 for the permanent file indicates that there wasn't a permanent file there already
					yield return new TestCaseData(i, j);
				}	// end inner for
			}	// end outer for

		}	// end GetTestData()

		private static void HandleTempAndPerm<T>(IFiles files, int tempVers, int permVers, List<T> temp, List<T> perm,
			string log)
		{
			var tempLines = temp.Select(i => $"{i}").ToList();
			if (tempVers > 0)
			{
				tempLines.Insert(0, $"v{tempVers}");
			}

			var permLines = perm?.Select(i => $"{i}").ToList();
			if (permVers > 0)
			{
				permLines?.Insert(0, $"v{permVers}");
			}

			files.AddLines(log, tempLines, permLines);

		}	// end HandleTempAndPerm<T>(IFiles, int, int, List<T>, List<T>, string)

		internal static string Perm(string log) => $"{Logger.LogBase}Harold\\{log}.hub";

		internal static string Temp(string log) => $"{log}.hub";

		#region Do Lines

		private void DoLinesAu(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesAu, Logger.AutoLog);

		private void DoLinesBl(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesBl, Logger.Build);

		private void DoLinesCl(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesCl, Logger.Clean);

		private void DoLinesDb(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesDb, Logger.Db);

		private void DoLinesEm(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV2(tempVers, permVers, tLines, pLines, lines => lines.RemoveAts(4, 5, 7));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesEm, Logger.Email, Prepper);

		}	// DoLinesEm(IFiles, int, int)

		private void DoLinesGo(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesGo, Logger.Go);

		private void DoLinesHl(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesHl, Logger.Help);

		private void DoLinesIm(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesIm, Logger.Import);

		private void DoLinesKl(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesKl, Logger.Kill);

		private void DoLinesLa(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesLa, Logger.Launch);

		private void DoLinesOe(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesOe, Logger.Oew);

		private void DoLinesOr(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV2(tempVers, permVers, tLines, pLines, lines => lines[2] = 0);

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesOr, Logger.Orphan, Prepper);

		}	// end DoLinesOr(IFiles, int, int)
		
		private void DoLinesPa(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesPa, Logger.ParamA);

		private void DoLinesPd(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesPd, Logger.ParamD);

		private void DoLinesPe(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesPe, Logger.ParamE);

		private void DoLinesPm(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesPm, Logger.ParamM);

		private void DoLinesPr(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesPr, Logger.Param);

		private void DoLinesPs(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesPs, Logger.ParamS);

		private void DoLinesRa(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV3(tempVers, permVers, tLines, pLines, lines => lines.Insert(3, 0));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesRa, Logger.ResA, Prepper);

		}	// end DoLinesRa(IFiles, int, int)

		private void DoLinesRb(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesRb, Logger.Rfb);

		private void DoLinesRc(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesRc, Logger.Recon);

		private void DoLinesRe(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesRe, Logger.Res);

		private void DoLinesRf(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV3(tempVers, permVers, tLines, pLines, lines => lines.Insert(3, 0));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesRf, Logger.ResF, Prepper);

		}	// end DoLinesRf(IFiles, int, int)

		private void DoLinesRr(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV3(tempVers, permVers, tLines, pLines, lines => lines.Insert(3, 0));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesRr, Logger.ResR, Prepper);

		}	// end DoLinesRr(IFiles, int, int)

		private void DoLinesRs(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV3(tempVers, permVers, tLines, pLines, lines => lines.Insert(3, 0));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesRs, Logger.ResS, Prepper);

		}	// end DoLinesRs(IFiles, int, int)

		private void DoLinesRt(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV1(tempVers, permVers, tLines, pLines, lines => lines.Insert(4, 0));
				
				UpdateToV3(tempVers, permVers, tLines, pLines, lines => lines.Insert(3, 0));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesRt, Logger.ResT, Prepper);

		}	// end DoLinesRt(IFiles, int, int)

		private void DoLinesSc(IFiles files, int tempVers, int permVers)
		{
			var temp = GetLinesSc(tempVers);
			var perm = GetLinesSc(permVers);

			// all but the last one should really be ints
			for (var i = 0; i < 12; i++)
			{
				temp[i] = Convert.ToInt32(temp[i]);
				if (perm != null)
				{
					perm[i] = Convert.ToInt32(perm[i]);
				}	// end if

			}	// end for

			HandleTempAndPerm(files, tempVers, permVers, temp, perm, Logger.Scrum);
			DoExpected(temp, perm, Logger.Scrum, tempVers, permVers);

		}	// end DoLinesSc(IFiles, int, int)

		private void DoLinesTc(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				UpdateToV1(tempVers, permVers, tLines, pLines, lines => lines.Add(0));

				UpdateToV4(tempVers, permVers, tLines, pLines, lines => lines.InsertLines(1, 4));

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesTc, Logger.TeamCity, Prepper);

		}	// end DoLinesTc(IFiles, int, int)

		private void DoLinesTr(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesTr, Logger.Trickler);

		private void DoLinesUt(IFiles files, int tempVers, int permVers)
		{
			void Prepper(List<int> tLines, List<int> pLines)
			{
				void Updater(List<int> lines)
				{
					for (var i = 0; i < 7; i++)
					{
						lines.Insert(1, 0);
					}	// end for

				}	// end Updater(List<int>)

				UpdateToV1(tempVers, permVers, tLines, pLines, Updater);

			}	// end Prepper(List<int>, List<int>)

			DoLines(files, tempVers, permVers, GetLinesUt, Logger.UnitTest, Prepper);

		}	// end DoLinesUt(IFiles, int, int)

		private void DoLinesVs(IFiles files, int tempVers, int permVers) => DoLines(files, tempVers, permVers, GetLinesVs, Logger.Vs);

		#endregion Do Lines

		#region Get Lines

		/// <summary>
		/// gets the specified number of lines with random data
		/// </summary>
		/// <typeparam name="T">int or double</typeparam>
		/// <param name="getter">gets the random data</param>
		/// <param name="version">the log file version the data is needed for</param>
		/// <param name="num0">the number of lines for version 0 data</param>
		/// <param name="num1">the number of lines for version 1 data</param>
		/// <param name="num2">the number of lines for version 2 data</param>
		/// <param name="num3">the number of lines for version 3 data</param>
		/// <param name="num4">the number of lines for version 4 data</param>
		private static List<T> GetLines<T>(Func<int, List<T>> getter, int version, int num0, int? num1, int? num2,
			int? num3, int? num4)
		{
			var n1 = num1 ?? num0;
			var n2 = num2 ?? n1;
			var n3 = num3 ?? n2;
			var n4 = num4 ?? n3;
			switch (version)
			{
				case -1: // no file
					return null;
				case 0:
					return getter(num0);
				case 1:
					return getter(n1);
				case 2:
					return getter(n2);
				case 3:
					return getter(n3);
				case 4:
					return getter(n4);
				default:
					throw new NotImplementedException("these tests don't support that version number");
			}	// end switch

		}	// end GetLines<T>(Func<int, List<T>>, int, int, int?, int?, int?)

		private static List<int> GetLinesInt(int num)
		{
			var lines = new List<int>();

			for (var i = 0; i < num; i++)
			{
				lines.Add(Rnd.Int(100));
			}	// end for

			return lines;

		}	// end GetLinesInt(int)

		private static List<int> GetLinesInt(int version, int num0, int? num1 = null, int? num2 = null,
			int? num3 = null, int? num4 = null) => GetLines(GetLinesInt, version, num0, num1, num2, num3, num4);

		private static List<double> GetLinesDoub(int num)
		{
			var lines = new List<double>();

			for (var i = 0; i < num; i++)
			{
				lines.Add(Rnd.Doub(2, 100));
			}	// end for

			return lines;

		}	// end GetLinesDoub(int)

		private static List<double> GetLinesDoub(int version, int num0, int? num1 = null, int? num2 = null,
			int? num3 = null, int? num4 = null) => GetLines(GetLinesDoub, version, num0, num1, num2, num3, num4);

		private static List<int> GetLinesAu(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesBl(int version) => GetLinesInt(version, 9);

		private static List<int> GetLinesCl(int version) => GetLinesInt(version, 4);

		private static List<int> GetLinesDb(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesEm(int version) => GetLinesInt(version, 13, num2: 10);

		private static List<int> GetLinesGo(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesHl(int version) => GetLinesInt(version, 3);

		private static List<int> GetLinesIm(int version) => GetLinesInt(version, 6);

		private static List<int> GetLinesKl(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesLa(int version) => GetLinesInt(version, 5);

		private static List<int> GetLinesOe(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesOr(int version) => GetLinesInt(version, 3);

		private static List<int> GetLinesPa(int version) => GetLinesInt(version, 2);
		
		private static List<int> GetLinesPd(int version) => GetLinesInt(version, 2);

		private static List<int> GetLinesPe(int version) => GetLinesInt(version, 3);

		private static List<int> GetLinesPm(int version) => GetLinesInt(version, 2);

		private static List<int> GetLinesPr(int version) => GetLinesInt(version, 7);

		private static List<int> GetLinesPs(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesRa(int version) => GetLinesInt(version, 6, num3: 7);

		private static List<int> GetLinesRb(int version) => GetLinesInt(version, 1);

		private static List<int> GetLinesRc(int version) => GetLinesInt(version, 5);

		private static List<int> GetLinesRe(int version) => GetLinesInt(version, 8);

		private static List<int> GetLinesRf(int version) => GetLinesInt(version, 6, num3: 7);

		private static List<int> GetLinesRr(int version) => GetLinesInt(version, 5, num3: 6);

		private static List<int> GetLinesRs(int version) => GetLinesInt(version, 4, num3: 5);

		private static List<int> GetLinesRt(int version) => GetLinesInt(version, 5, 6, num3: 7);

		private static List<double> GetLinesSc(int version) => GetLinesDoub(version, 13);

		private static List<int> GetLinesTc(int version) => GetLinesInt(version, 2, 3, num4: 7);

		private static List<int> GetLinesTr(int version) => GetLinesInt(version, 5);

		private static List<int> GetLinesUt(int version) => GetLinesInt(version, 5, 12);

		private static List<int> GetLinesVs(int version) => GetLinesInt(version, 3);

		#endregion Get Lines
		
		#region Prepper Helpers

		private static void UpdateToV1(int tempVers, int permVers, List<int> tLines, List<int> pLines, Action<List<int>> updater)
		{
			UpdateToV(1, tempVers, permVers, tLines, pLines, updater);

		}	// end UpdateToV1(int, int, List<int>, List<int>)
		
		private static void UpdateToV2(int tempVers, int permVers, List<int> tLines, List<int> pLines, Action<List<int>> updater)
		{
			UpdateToV(2, tempVers, permVers, tLines, pLines, updater);

		}	// end UpdateToV2(int, int, List<int>, List<int>)
		
		private static void UpdateToV3(int tempVers, int permVers, List<int> tLines, List<int> pLines, Action<List<int>> updater)
		{
			UpdateToV(3, tempVers, permVers, tLines, pLines, updater);

		}	// end UpdateToV3(int, int, List<int>, List<int>)

		private static void UpdateToV4(int tempVers, int permVers, List<int> tLines, List<int> pLines, Action<List<int>> updater)
		{
			UpdateToV(4, tempVers, permVers, tLines, pLines, updater);

		}	// end UpdateToV4(int, int, List<int>, List<int>, Action<List<int>>)

		private static void UpdateToV(int v, int tempVers, int permVers, List<int> tLines, List<int> pLines,
			Action<List<int>> updater)
		{
			if (ApplyToVTemp(v, tempVers, permVers))
			{
				updater(tLines);
			}	// end if

			if (ApplyToVPerm(v, permVers) && pLines != null)
			{
				updater(pLines);
			}	// end if

		}	// end UpdateToV(int, int, int, List<int>, List<int>, Action<List<int>>)

		private static bool ApplyToVTemp(int v, int tempVers, int permVers) => tempVers < v && permVers != -1;

		private static bool ApplyToVPerm(int v, int permVers) => permVers < v && permVers > -1;

		#endregion Prepper Helpers

		#endregion Helpers

		#region Inner Class

		private class LogMovingFiles : BaseFileStub
		{
			#region Variables

			internal readonly Dictionary<string, string> Copied = new Dictionary<string, string>();

			internal readonly List<string> Deleted = new List<string>();

			internal readonly Dictionary<string, IEnumerable<string>> WrittenLines =
				new Dictionary<string, IEnumerable<string>>();

			#endregion Variables

			#region Overrides of BaseFileStub

			/// <inheritdoc />
			public override void AppendAllLines(string path, List<string> lines) => WrittenLines.Add(path, lines);

			/// <inheritdoc />
			public override void Copy(string source, string destination)
			{
				Copied[source] = destination;
				WrittenLines[destination] = WrittenLines[source];

			}	// end Copy(string, string)

			/// <inheritdoc />
			public override void Delete(string path) => Deleted.Add(path);

			/// <inheritdoc />
			public override bool Exists(string path) => WrittenLines.ContainsKey(path);

			/// <inheritdoc />
			public override IEnumerable<string> ReadLines(string path) => WrittenLines.ContainsKey(path) ? WrittenLines[path] : throw new NotImplementedException();

			/// <inheritdoc />
			public override void WriteAllLines(string path, IEnumerable<string> lines) => WrittenLines[path] = lines;

			#endregion Overrides of BaseFileStub

		}	// end LogMovingFiles

		#endregion Inner Class

	}	// end MovingLogsTests

	///
	[ExcludeFromCodeCoverage]
	public static class Ext
	{
		///
		public static void AddLines(this IFiles files, string log, IEnumerable<string> temp,
			IEnumerable<string> perm)
		{
			files.WriteAllLines(MovingLogsTests.Temp(log), temp);
			if (perm != null)
			{
				files.WriteAllLines(MovingLogsTests.Perm(log), perm);
			}	// end if

		}	// end AddLines(this IFiles, string, IEnumerable<string>, IEnumerable<string>)

		///
		public static void RemoveAts(this List<int> list, params int[] toRemove)
		{
			var removeList = toRemove.ToList();
			removeList.Sort();
			removeList.Reverse();
			foreach (var remove in removeList)
			{
				list.RemoveAt(remove);
			}	// end foreach

		}	// end RemoveAts(this List<int>, params int[])

		///
		public static void InsertLines(this List<int> list, int where, int num)
		{
			for (var i = 0; i < num; i++)
			{
				list.Insert(where, 0);
			}	// end for

		}	// end InsertLines(this List<int>, int, int)

	}	// end Ext

}	// end HubUnitTest.Logging
