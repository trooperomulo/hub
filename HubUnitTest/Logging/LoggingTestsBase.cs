﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Enum;
using Hub.Interface;
using Hub.Logging;

using HubUnitTest.Stubs.Logging;

using NUnit.Framework;

using RyanCoreTests;

namespace HubUnitTest.Logging
{
	///
	[ExcludeFromCodeCoverage]
	public abstract class LoggingTestsBase
	{
		#region Constants

		///
		protected const int V = 4;

		///
		// ReSharper disable once InconsistentNaming
		protected static readonly string CV = $"v{V}";
		///
		protected const bool T = true;
		///
		protected const bool F = false;

		#endregion Constants

		#region Setup / Tear Down

		///
		[SetUp]
		public void ResetInstance()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, LoggerTests.LoggerFilesStub>(new LoggerTests.LoggerFilesStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, LoggingDirsStub>(new LoggingDirsStub());

			Logger.Instance.Initialize("bill", true);

		}	// end ResetInstance()

		///
		[TearDown]
		public void TearDown()
		{
			var type = typeof(Logger);
			CoreTestUtils.SetPrivateStaticProperty(type, "Files", null);
			CoreTestUtils.SetPrivateStaticProperty(type, "Dirs", null);
			CoreTestUtils.SetPrivateStaticProperty(type, "Name", null);
			CoreTestUtils.SetPrivateStaticField(type, "_instance", null);
			CoreTestUtils.SetPrivateStaticField(type, "_pathBase", null);
			CoreTestUtils.SetPrivateStaticField(type, "_canLog", false);

			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();

		}	// end TearDown()

		#endregion Setup / Tear Down
		
		#region Test Helpers

		#region Test V Helpers

		private static void TestV<T>(T files, Action call, List<string> exp, Func<string> getStartData)
			where T : LoggingFilesStubBase
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, T>(files);
			call();
			var msg = CoreTestUtils.TestLists(exp, files.WrittenLines);
			
			if (!msg.IsNullOrEmpty())
			{
				// we only want to call getStartData if there was a failure
				Assert.Fail(msg + getStartData());
			}	// end if

		}	// end TestV<T>(T, Action, List<string>, Func<string>)

		///
		protected static void TestV0(List<int> start, string hours, Action call, List<string> exp,
			Func<string> getStartData) =>
			TestV(hours == null ? new LoggingV0FilesStub(start) : new ScrumV0FilesStub(start, hours), call, exp,
				getStartData);

		///
		protected static void TestV1(List<int> start, string hours, Action call, List<string> exp,
			Func<string> getStartData) =>
			TestV(hours == null ? new LoggingV1FilesStub(start) : new ScrumV1FilesStub(start, hours), call, exp,
			getStartData);

		///
		protected static void TestV2(List<int> start, string hours, Action call, List<string> exp,
			Func<string> getStartData) =>
			TestV(hours == null ? new LoggingV2FilesStub(start) : new ScrumV2FilesStub(start, hours), call, exp,
				getStartData);

		///
		protected static void TestV3(List<int> start, string hours, Action call, List<string> exp,
			Func<string> getStartData) =>
			TestV(hours == null ? new LoggingV3FilesStub(start) : new ScrumV3FilesStub(start, hours), call, exp,
				getStartData);

		///
		protected static void TestV4(List<int> start, string hours, Action call, List<string> exp,
			Func<string> getStartData) =>
			TestV(hours == null ? new LoggingV4FilesStub(start) : new ScrumV4FilesStub(start, hours), call, exp,
				getStartData);

		#endregion Test V Helpers

		///
		protected void TestMultipleCalls(List<int> start, string hours, Action rnd1, Action rnd2, Action rnd3,
			List<string> exp1, List<string> exp2, List<string> exp3, Func<string> getStartData)
		{
			var files = hours == null ? new LoggingV4FilesStub(start) : new ScrumV4FilesStub(start, hours);

			// for this test I want to make sure we're using the most current version
			Assert.AreEqual(files.Version, LogFile<EhData>.Current, "Wrong version, update the files stub");

			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, LoggingV4FilesStub>(files);

			var nl = Environment.NewLine;

			rnd1();

			var msg = CoreTestUtils.ListsAreEqual(exp1, files.WrittenLines, $"Round 1 failed{nl}");

			rnd2();

			msg += CoreTestUtils.ListsAreEqual(exp2, files.WrittenLines, $"Round 2 failed{nl}");

			rnd3();

			msg += CoreTestUtils.ListsAreEqual(exp3, files.WrittenLines, $"Round 3 failed{nl}");

			if (!msg.IsNullOrEmpty())
			{
				// we only want to call getStartData if there was a failure
				Assert.Fail(msg + getStartData());
			}	// end if

		}	// end TestMultipleCalls(LoggingV4FilesStub, Action, Action, Action, List<string>, List<string>, List<string>, Func<string>)

		///
		protected void TestNo(Action call, List<string> exp)
		{
			var files = new NoFilesLoggingStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, NoFilesLoggingStub>(files);

			call();

			CoreTestUtils.TestListsAreEqual(exp, files.WrittenLines);

		}	// end TestNo(Action, List<string>)

		///
		protected void TestV999(Action call)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, LoggingV999FilesStub>(new LoggingV999FilesStub());
			var ex = Assert.Throws<ArgumentOutOfRangeException>(() => call());
			Assert.AreEqual($"this build only supports log files up to version {V} but found a file with version 999\r\nParameter name: file version",
				ex.Message);

		}	// end TestV999(action)

		#endregion Test Helpers

		#region Helpers

		/// <summary>
		/// changes the values in the given list to 0
		/// </summary>
		protected void EmptyTheList(List<int> list)
		{
			for (var i = 0; i < list.Count; i++)
			{
				list[i] = 0;
			}	// end for

		}	// end EmptyTheList(List<int>)

		/// <summary>
		/// gets a list of the given length filled with random integers
		/// </summary>
		protected List<int> GetRandomInts(int count)
		{
			var list = new List<int>();
			for (var i = 0; i < count; i++)
			{
				list.Add(RandomData.Int(10));
			}	// end for

			return list;

		}	// end GetRandomInts(int)
		
		/// <summary>
		/// gets a list of the given length filled with random booleans
		/// </summary>
		// ReSharper disable once IdentifierTypo
		protected List<bool> GetRandomBools(int count)
		{
			var list = new List<bool>();
			for (var i = 0; i < count; i++)
			{
				list.Add(RandomData.Bool());
			}	// end for

			return list;

			// ReSharper disable CommentTypo
		}	// end GetRandomBools(int)
		// ReSharper restore CommentTypo
		
		/// <summary>
		/// gets a random int
		/// </summary>
		protected static int GetRandomNum() => RandomData.Int(10);

		// ReSharper disable once CommentTypo
		/// <summary>
		/// gets a random value of Res, including all or'ed combinations
		/// </summary>
		protected static Res GetRandomRes() => (Res) RandomData.Int(8);

		///
		public static IEnumerable GetTests()
		{
			yield return new TestCaseData(0, (Action<List<int>, string, Action, List<string>, Func<string>>) TestV0).SetName("V0");
			yield return new TestCaseData(1, (Action<List<int>, string, Action, List<string>, Func<string>>) TestV1).SetName("V1");
			yield return new TestCaseData(2, (Action<List<int>, string, Action, List<string>, Func<string>>) TestV2).SetName("V2");
			yield return new TestCaseData(3, (Action<List<int>, string, Action, List<string>, Func<string>>) TestV3).SetName("V3");
			yield return new TestCaseData(4, (Action<List<int>, string, Action, List<string>, Func<string>>) TestV4).SetName("V4");

		}	// end GetTests()

		///
		protected static int GetValueBasedOnVersion2(int version, int limit1, int value1, int value2)
		{
			if (version < limit1)
			{
				return value1;
			}	// end if

			return value2;

		}	// end GetValueBasedOnVersion2(int, int, int, int)

		///
		protected static int GetValueBasedOnVersion3(int version, int limit1, int limit2, int value1, int value2,
			int value3)
		{
			if (version < limit1)
			{
				return value1;
			}	// end if

			if (version < limit2)
			{
				return value2;
			}	// end if

			return value3;

		}	// end GetValueBasedOnVersion3(int, int, int, int, int, int)

		///
		protected static int GetValueBasedOnVersion4(int version, int limit1, int limit2, int limit3, int value1,
			int value2, int value3, int value4)
		{
			if (version < limit1)
			{
				return value1;
			}	// end if

			if (version < limit2)
			{
				return value2;
			}	// end if

			if (version < limit3)
			{
				return value3;
			}	// end if

			return value4;

		}	// end GetValueBasedOnVersion4(int, int, int, int, int, int, int, int)

		///
		protected static List<string> IntsToStrings(List<int> ints, string hours)
		{
			var strings = new List<string> {CV};
			strings.AddRange(ints.Select(i => $"{i}"));
			if (hours != null)
			{
				strings.Add(hours);
			}	// end if

			return strings;

		}	// end IntsToStrings(List<int>)

		///
		protected static List<string> StringList(string version, params int[] ints)
		{
			var list = new List<string>{version};
			list.AddRange(ints.ToStringList());
			return list;

		}	// end StringList(string, params int[])

		#endregion Helpers

	}	// end LoggingTestsBase

}	// end HubUnitTest.Logging
