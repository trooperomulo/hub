﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;

using NUnit.Framework;
using RyanCoreTests;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="IoCContainer" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class IoCContainerTests
	{

		#region Constants

		// ReSharper disable InconsistentNaming
		private static readonly Type ConType = typeof( IoCContainer );
		private static readonly Type IctType = typeof( ICounter );
		private static readonly Type IotType = typeof( IOtherCounter );
		private const string Mult = "_multiInstance";
		private const string Sing = "_singleInstance";
		// ReSharper restore InconsistentNaming

		#endregion Constants

		#region Tests

		/// <summary>
		/// an exception is throw if null is passed in
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Exception is thrown
		/// - Coordinator:      When
		/// - Conditions:       Null is passed in
		/// </remarks>
		[Test]
		public void TestRegisterAsMultiInstance_ExceptionIsThrown_When_NullIsPassedIn( )
		{
			void FailingCall( )
			{
				IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( null );
			}

			var ex = Assert.Throws<ArgumentException>( ( ) => FailingCall( ) );
			Assert.AreEqual( "cannot register null instance", ex.Message );
		}

		/// <summary>
		/// Make sure multi instances are registered properly
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Func is stored
		/// - Coordinator:      After
		/// - Conditions:       A multi instance is registered
		/// </remarks>
		[Test]
		public void TestRegisterAsMultiInstance_FuncIsStored_After_AMultiInstanceIsRegistered( )
		{
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( CreateCountA );
			var mult = GetMultDictionary( );
			Assert.AreEqual( ( Func<CountA> ) CreateCountA, mult[ IctType ] );
		}

		/// <summary>
		/// Make sure multi instances are registered properly
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Second func is stored
		/// - Coordinator:      After
		/// - Conditions:       Two func are registered
		/// </remarks>
		[Test]
		public void TestRegisterAsMultiInstance_SecondFuncIsStored_After_TwoFuncAreRegistered( )
		{
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( CreateCountA );
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountB>( CreateCountB );
			var mult = GetMultDictionary( );
			Assert.AreEqual( ( Func<CountB> ) CreateCountB, mult[ IctType ] );
		}

		/// <summary>
		/// Singleton instance is removed when multi instance is registered
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Singleton is removed
		/// - Coordinator:      When
		/// - Conditions:       Multi instance is registered
		/// </remarks>
		[Test]
		public void TestRegisterAsMultiInstance_SingletonIsRemoved_When_MultiInstanceIsRegistered( )
		{
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountB>( new CountB( ) );
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( CreateCountA );
			var mult = GetMultDictionary( );
			var sing = GetSingDictionary( );
			Assert.AreEqual( ( Func<CountA> ) CreateCountA, mult[ IctType ] );
			Assert.IsFalse( sing.ContainsKey( IctType ) );
		}

		/// <summary>
		/// an exception is throw if null is passed in
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Exception is thrown
		/// - Coordinator:      When
		/// - Conditions:       Null is passed in
		/// </remarks>
		[Test]
		public void TestRegisterAsSingleInstance_ExceptionIsThrown_When_NullIsPassedIn( )
		{
			void FailingCall( )
			{
				IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountA>( null );
			}

			var ex = Assert.Throws<ArgumentException>( ( ) => FailingCall( ) );
			Assert.AreEqual( "cannot register null instance", ex.Message );
		}

		/// <summary>
		/// Make sure singletons are registered properly
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Singleton is stored
		/// - Coordinator:      After
		/// - Conditions:       A singleton is registered
		/// </remarks>
		[Test]
		public void TestRegisterAsSingleInstance_SingletonIsStored_After_ASingletonIsRegistered( )
		{
			var countA = new CountA( );
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountA>( countA );
			var sing = GetSingDictionary( );
			Assert.AreEqual( countA, sing[ IctType ] );
		}

		/// <summary>
		/// Make sure singletons are registered properly
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Second singleton is stored
		/// - Coordinator:      After
		/// - Conditions:       Two singletons are registered
		/// </remarks>
		[Test]
		public void TestRegisterAsSingleInstance_SecondSingletonIsStored_After_TwoSingletonsAreRegistered( )
		{
			var countA = new CountA( );
			var countB = new CountB( );
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountA>( countA );
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountB>( countB );
			var sing = GetSingDictionary( );
			Assert.AreEqual( countB, sing[ IctType ] );
		}

		/// <summary>
		/// Multi instance is removed when singleton is registered
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Multi instance is removed
		/// - Coordinator:      When
		/// - Conditions:       Singleton is registered
		/// </remarks>
		[Test]
		public void TestRegisterAsSingleInstance_MultiInstanceIsRemoved_When_SingletonIsRegistered( )
		{
			var countA = new CountA( );
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( CreateCountA );
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountA>( countA );
			var mult = GetMultDictionary( );
			var sing = GetSingDictionary( );
			Assert.AreEqual( countA, sing[ IctType ] );
			Assert.IsFalse( mult.ContainsKey( IctType ) );
		}

		/// <summary>
		/// Only the appropriate multi instance is removed
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Only the specified multi instance is removed
		/// - Coordinator:      When
		/// - Conditions:       Called
		/// </remarks>
		[Test]
		public void TestRemoveRegisteredType_OnlyTheSpecifiedMultiInstanceIsRemoved_When_Called( )
		{
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( CreateCountA );
			IoCContainer.Instance.RegisterAsMultiInstance<IOtherCounter, OtherA>( CreateOtherA );
			IoCContainer.Instance.RemoveRegisteredType<ICounter>( );
			var mult = GetMultDictionary( );
			Assert.IsFalse( mult.ContainsKey( IctType ) );
			Assert.IsTrue( mult.ContainsKey( IotType ) );
		}

		/// <summary>
		/// Only the appropriate singleton is removed
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Only the specified singleton is removed
		/// - Coordinator:      When
		/// - Conditions:       Called
		/// </remarks>
		[Test]
		public void TestRemoveRegisteredType_OnlyTheSpecifiedSingletonIsRemoved_When_Called( )
		{
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountA>( new CountA( ) );
			IoCContainer.Instance.RegisterAsSingleInstance<IOtherCounter, OtherA>( new OtherA( ) );
			IoCContainer.Instance.RemoveRegisteredType<ICounter>( );
			var sing = GetSingDictionary( );
			Assert.IsFalse( sing.ContainsKey( IctType ) );
			Assert.IsTrue( sing.ContainsKey( IotType ) );
		}

		/// <summary>
		/// Resolve returns the multi instance
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Returns new instance
		/// - Coordinator:      When
		/// - Conditions:       Multi instance is resolved
		/// </remarks>
		[Test]
		public void TestResolve_ReturnsNewInstance_When_MultiInstanceIsResolved( )
		{
			IoCContainer.Instance.RegisterAsMultiInstance<ICounter, CountA>( CreateCountA );
			var one = IoCContainer.Instance.Resolve<ICounter>( );
			var two = IoCContainer.Instance.Resolve<ICounter>( );
			Assert.AreNotEqual( one, two );
			Assert.AreEqual( one.Count( ), two.Count( ) );
		}

		/// <summary>
		/// Resolve returns same singleton
		/// </summary>
		/// <remarks>
		/// - Desired Result:	ReturnsSameInstance
		/// - Coordinator:      When
		/// - Conditions:       SingleInstanceIsResolved
		/// </remarks>
		[Test]
		public void TestResolve_ReturnsSameInstance_When_SingleInstanceIsResolved( )
		{
			IoCContainer.Instance.RegisterAsSingleInstance<ICounter, CountA>( new CountA( ) );
			var one = IoCContainer.Instance.Resolve<ICounter>( );
			var two = IoCContainer.Instance.Resolve<ICounter>( );
			Assert.AreEqual( one, two );
			Assert.AreNotEqual( one.Count( ), two.Count( ) );
		}

		/// <summary>
		/// an exception is thown if bad multi instance is returned
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Throws exception
		/// - Coordinator:      When
		/// - Conditions:       Bad multi instance is returned
		/// </remarks>
		[Test]
		public void TestResolve_Throws_When_BadMultiInstanceIsReturned( )
		{
			var bad = new Dictionary<Type, object> { { IctType, new CountA( ) } };
			CoreTestUtils.SetPrivateField( ConType, IoCContainer.Instance, Mult, bad );
			void FailingCall( ) => IoCContainer.Instance.Resolve<ICounter>( );

			var ex = Assert.Throws<ArgumentException>( ( ) => FailingCall( ) );
			Assert.AreEqual( "Somehow the registered function for type ICounter returns the wrong type", ex.Message );
			var mult = GetMultDictionary( );
			Assert.IsFalse( mult.ContainsKey( IctType ) );
		}

		/// <summary>
		/// an exception is thown if bad singleton is returned
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Throws exception
		/// - Coordinator:      When
		/// - Conditions:       Bad singleton is returned
		/// </remarks>
		[Test]
		public void TestResolve_Throws_When_BadSingletonIsReturned( )
		{
			var bad = new Dictionary<Type, object> { { IctType, ( Func<CountA> ) CreateCountA } };
			CoreTestUtils.SetPrivateField( ConType, IoCContainer.Instance, Sing, bad );
			void FailingCall( ) => IoCContainer.Instance.Resolve<ICounter>( );

			var ex = Assert.Throws<ArgumentException>( ( ) => FailingCall( ) );
			Assert.AreEqual( "Somehow the registered instance for type ICounter was the wrong type", ex.Message );
			var sing = GetSingDictionary( );
			Assert.IsFalse( sing.ContainsKey( IctType ) );
		}

		/// <summary>
		/// an exception is thrown when resolving an unregistered type
		/// </summary>
		/// <remarks>
		/// - Desired Result:	Throws exception
		/// - Coordinator:      When
		/// - Conditions:       Unregistered type is resolved
		/// </remarks>
		[Test]
		public void TestResolve_Throws_When_UnregisteredTypeIsResolved( )
		{
			void FailingCall( ) => IoCContainer.Instance.Resolve<ICounter>( );

			var ex = Assert.Throws<ArgumentException>( ( ) => FailingCall( ) );
			Assert.AreEqual("Tried to resolve type 'ICounter', but nothing had been registered for that type", ex.Message );
		}

		#endregion Tests

		#region Helpers

		///
		[SetUp]
		public void Setup( )
		{
			IoCContainer.Instance.RemoveRegisteredType<ICounter>( );
			IoCContainer.Instance.RemoveRegisteredType<IOtherCounter>( );
		}

		private CountA CreateCountA( ) => new CountA( );

		private CountB CreateCountB( ) => new CountB( );

		private OtherA CreateOtherA( ) => new OtherA( );

		private Dictionary<Type, object> GetMultDictionary( )
		{
			return CoreTestUtils.GetPrivateField<Dictionary<Type, object>>( ConType, IoCContainer.Instance, Mult );
		}

		private Dictionary<Type, object> GetSingDictionary( )
		{
			return CoreTestUtils.GetPrivateField<Dictionary<Type, object>>( ConType, IoCContainer.Instance, Sing );
		}

		#endregion Helpers

		#region Test Classes
		
		///
		public interface ICounter
		{
			/// <summary>
			/// returns a count
			/// </summary>
			int Count( );
		}
		
		///
		public interface IOtherCounter
		{
			/// <summary>
			/// returns a count
			/// </summary>
			int Count( );
		}
		
		///
		public class CountA : ICounter
		{
			private int _count;
			
			///
			public int Count( )
			{
				_count++;
				return _count;
			}
		}
		
		///
		public class CountB : ICounter
		{
			private int _count;
			
			///
			public int Count( )
			{
				_count += 10;
				return _count;
			}
		}
		
		///
		public class OtherA : IOtherCounter
		{
			private int _count;
			
			///
			public int Count( )
			{
				_count += 2;
				return _count;
			}
		}
		
		///
		public class OtherB : IOtherCounter
		{
			private int _count;
			
			///
			public int Count( )
			{
				_count += 7;
				return _count;
			}
		}

		#endregion Test Classes

	}
}
