﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using RyanCoreTests;

using Hub;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="Shortcuts" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ShortcutsTests
	{
		#region Tests

		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, ShortcutsFilesStub>(new ShortcutsFilesStub());
		}

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
		}

		/// <summary>
		/// tests <see cref="Shortcuts.Clear" />
		/// </summary>
		[Test]
		public void TestClear()
		{
			if (!Shortcuts.Instance.Loaded)
			{
				LoadShortcuts("real");
			}

			Shortcuts.Instance.Clear();

			Assert.IsFalse(Shortcuts.Instance.Loaded);
		}

		/// <summary>
		/// tests <see cref="Shortcuts.GetOther" />
		/// </summary>
		[Test]
		public void TestGetOther()
		{
			LoadShortcuts("real");
			Assert.AreEqual(null, Shortcuts.Instance.GetOther("just"));
			Assert.AreEqual("dll", Shortcuts.Instance.GetOther("path"));
			Assert.AreEqual("", Shortcuts.Instance.GetOther("not_a_key"));
			Shortcuts.Instance.Clear();
			Assert.AreEqual("", Shortcuts.Instance.GetOther("just"));
		}

		/// <summary>
		/// tests <see cref="Shortcuts.GetPath" />
		/// </summary>
		[Test]
		public void TestGetPath()
		{
			LoadShortcuts("real");
			Assert.AreEqual("path", Shortcuts.Instance.GetPath("just"));
			Assert.AreEqual("and", Shortcuts.Instance.GetPath("path"));
			Assert.AreEqual("", Shortcuts.Instance.GetPath("not_a_key"));
			Shortcuts.Instance.Clear();
			Assert.AreEqual("", Shortcuts.Instance.GetPath("just"));
		}

		/// <summary>
		/// tests <see cref="Shortcuts.HasPath" />
		/// </summary>
		[Test]
		public void TestHasPath()
		{
			LoadShortcuts("real");
			Assert.IsTrue(Shortcuts.Instance.HasPath("path"));
			Assert.IsTrue(Shortcuts.Instance.HasPath("and"));
			Assert.IsFalse(Shortcuts.Instance.HasPath("not_a_key"));
			Shortcuts.Instance.Clear();
			Assert.IsFalse(Shortcuts.Instance.HasPath("path"));
		}

		/// <summary>
		/// tests <see cref="Shortcuts.HasShortcut" />
		/// </summary>
		[Test]
		public void TestHasShortcut()
		{
			LoadShortcuts("real");
			Assert.IsTrue(Shortcuts.Instance.HasShortcut("just"));
			Assert.IsTrue(Shortcuts.Instance.HasShortcut("path"));
			Assert.IsFalse(Shortcuts.Instance.HasShortcut("not_a_key"));
			Shortcuts.Instance.Clear();
			Assert.IsFalse(Shortcuts.Instance.HasShortcut("just"));
		}

		/// <summary>
		/// tests <see cref="Shortcuts.LoadShortcuts" /> with failures
		/// </summary>
		[Test]
		public void TestLoadShortcutsFailures()
		{
			Shortcuts.Instance.Clear();
			Shortcuts.Instance.LoadShortcuts("fake");
			Assert.IsFalse(Shortcuts.Instance.Loaded);
		}

		/// <summary>
		/// tests <see cref="Shortcuts.LoadShortcuts" /> running successfully
		/// </summary>
		[Test]
		public void TestLoadShortcutsSuccessfully()
		{
			LoadShortcuts("real");
			Assert.IsTrue(Shortcuts.Instance.Loaded);
			var dict = CoreTestUtils.GetPrivateField<Dictionary<string, (string path, string dll)>>(typeof(Shortcuts),
				Shortcuts.Instance, "_shortcuts");
			Assert.IsNotNull(dict);
			Assert.IsFalse(dict.ContainsKey("should"));
			Assert.IsFalse(dict.ContainsKey("do_not_show_up"));
			Assert.IsTrue(dict.ContainsKey("just"));
			Assert.IsTrue(dict.ContainsKey("path"));
		}

		/// <summary>
		/// tests <see cref="Shortcuts.PathToShortcut" />
		/// </summary>
		[Test]
		public void TestPathToShortcut()
		{
			LoadShortcuts("real");
			Assert.AreEqual("path", Shortcuts.Instance.PathToShortcut("and"));
			Assert.AreEqual("just", Shortcuts.Instance.PathToShortcut("path"));
			Assert.AreEqual("", Shortcuts.Instance.PathToShortcut("not_a_key"));
			Shortcuts.Instance.Clear();
			Assert.AreEqual("", Shortcuts.Instance.PathToShortcut("just"));
		}

		#endregion Tests

		#region Helper

		private static void LoadShortcuts(string path)
		{
			if (!Shortcuts.Instance.Loaded)
			{
				Shortcuts.Instance.LoadShortcuts(path);
			}
		}

		#endregion Helper

		#region Inner Class

		internal class ShortcutsFilesStub : BaseFileStub
		{
			#region Implementation of IFiles

			/// <inheritdoc />
			public override bool Exists(string path) => !path.Equals("fake");

			/// <inheritdoc />
			public override IEnumerable<string> ReadLines(string path)
			{
				return new[]
				{
					";should=        not          =appear",
					"do_not_show_up",
					"=      no          =shortcut",
					"just=   path",
					"path=       and       =dll",
					"mode=test%mode%"
				};
			}

			#endregion Implementation of IFiles
		}

		#endregion Inner Class
	}
}
