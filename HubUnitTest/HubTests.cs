﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Interface;

using HubUnitTest.Stubs;

using Moq;

using NUnit.Framework;

namespace HubUnitTest
{
	/// <summary>
	/// tests general things in the program
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class HubTests
	{
		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IFiles, BaseFileStub>(new BaseFileStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, BaseDirsStub>(new BaseDirsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IProgram, BaseProgStub>(new BaseProgStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IGit, BaseGitStub>(new BaseGitStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(new Mock<IWaitForUser>().Object);

		}	// end Setup()

		///
		[TearDown]
		public void TearDown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IFiles>();
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IProgram>();
			IoCContainer.Instance.RemoveRegisteredType<IGit>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();

		}	// end TearDown()

		/// <summary>
		/// makes sure there aren't any duplicate inputs in the main commands
		/// </summary>
		[Test]
		public void TestMainCommandsInputs()
		{
			Env.SetEnvironmentVariable("version_tree", @"\git\1\xm8", true);
			var cmds = Hub.Hub.GetMainCommands(0, true, true, "", /*"", "", true,*/ false, "");
			TestDuplicateInputs(cmds);

		}	// end TestMainCommandsInputs()

		/// <summary>
		/// makes sure there aren't any duplicate inputs in the user parameter commands
		/// </summary>
		[Test]
		public void TestParamCommandsInputs() => TestDuplicateInputs(Hub.Hub.GetParamCommands());

		/// <summary>
		/// make sure there aren't any duplicate inputs in the resx commands
		/// </summary>
		[Test]
		public void TestResxCommandsInputs() => TestDuplicateInputs(Hub.Hub.GetRealResxCommands(5));
		
		private static void TestDuplicateInputs<T>(List<T> cmds) where T: IInput
		{
			var inputs = new Dictionary<string, List<string>>();
			foreach (var cmd in cmds)
			{
				foreach (var input in cmd.Inputs)
				{
					var type = cmd.GetType().Name;
					if (inputs.ContainsKey(input))
					{
						inputs[input].Add(type);
					}	// end if
					else
					{
						inputs.Add(input, new List<string> {type});
					}	// end else
				}	// end foreach
			}	// end foreach

			var dupes = "";
			foreach (var key in inputs.Keys)
			{
				var list = inputs[key];
				if (list.Count > 1)
				{
					dupes += $"'{key}' is used by {list.Aggregate("", (c, p) => $"{c} '{p}'").Substring(1)}{Environment.NewLine}";
				}	// end if
			}	// end foreach

			Assert.IsTrue(dupes.IsNullOrEmpty(), dupes);

		}	// end TestDuplicateInputs<T>(List<T>)

	}	// end HubTests

}	// end HubUnitTest
