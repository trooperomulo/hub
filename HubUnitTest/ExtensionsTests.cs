﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;

using NUnit.Framework;

using RyanCoreTests;

using Extensions = Hub.Extensions;

namespace HubUnitTest
{
	/// <summary>
	/// class to test <see cref="Extensions" /> class
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ExtensionsUnitTests
	{
		/// <summary>
		/// tests <see cref="Extensions.CleanBranchOutput" />
		/// </summary>
		[TestCase("", "")]
		[TestCase(null, "")]
		[TestCase("origin/*  *   ", "")]
		[TestCase("valid", "valid")]
		[TestCase("origin/*valid1/  valid2/*** valid3     ", "valid1/valid2/valid3")]
		public void TestCleanBranchOutput(string input, string expected) => TestValues(expected, input.CleanBranchOutput(), input);

		/// <summary>
		/// tests <see cref="Extensions.IsNullOrEmpty" />
		/// </summary>
		[TestCase(null, true)]
		[TestCase("", true)]
		[TestCase("something", false)]
		public void TestIsNullOrEmpty(string input, bool expected) => TestValues(expected, input.IsNullOrEmpty(), input);

		/// <summary>
		/// tests <see cref="Extensions.StripCData" />
		/// </summary>
		/// <param name="input"></param>
		/// <param name="expected"></param>
		[TestCase("", "")]
		[TestCase(null, "")]
		[TestCase("whatever", "whatever")]
		[TestCase("whatever]]", "whatever]]")]
		[TestCase("![CDATA[whatever", "![CDATA[whatever")]
		[TestCase("![cdata[whatever]]", "![cdata[whatever]]")]
		[TestCase("![CDATA[whatever]]", "whatever")]
		[TestCase("![CDATA[Now this part is really long]]", "Now this part is really long")]
		[TestCase("![CDATA[Carriage returns are maintained\r\nSee\r\nThis is still 3 lines long]]", "Carriage returns are maintained\r\nSee\r\nThis is still 3 lines long")]
		public void TestStripCData(string input, string expected) => TestValues(expected, input.StripCData(), input);

		/// <summary>
		/// tests <see cref="Extensions.ToDoub" />
		/// </summary>
		[TestCase(null, 0d, null)]
		[TestCase("", 0d, null)]
		[TestCase("9432.0", 9432.0, null)]
		[TestCase("-322", -322, null)]
		[TestCase("+4302", 4302, null)]
		[TestCase("abc", 0d, null)]
		[TestCase(null, -1, -1d)]
		[TestCase("", -1, -1d)]
		[TestCase("1234", 1234, -1d)]
		[TestCase("abc", -1, -1d)]
		// ReSharper disable once IdentifierTypo
		public void TestToDoub(string input, double expected, double? def) => TestValues(expected, input.ToDoub(def), input);

		/// <summary>
		/// tests <see cref="Extensions.ToInt(bool)" />
		/// </summary>
		[TestCase(true, 1)]
		[TestCase(false, 0)]
		public void TestToIntBool(bool input, int expected) => TestValues(expected, input.ToInt(), $"{input}");

		/// <summary>
		/// tests <see cref="Extensions.ToInt(string, int?)" />
		/// </summary>
		[TestCase(null, 0, null)]
		[TestCase("", 0, null)]
		[TestCase("9432.0", 9432, null)]
		[TestCase("-322", -322, null)]
		[TestCase("+4302", 4302, null)]
		[TestCase("abc", 0, null)]
		[TestCase(null, -1, -1)]
		[TestCase("", -1, -1)]
		[TestCase("1234", 1234, -1)]
		[TestCase("abc", -1, -1)]
		public void TestToIntString(string input, int expected, int? def) => TestValues(expected, input.ToInt(def), input);

		/// <summary>
		/// tests <see cref="Extensions.ToStringList" />
		/// </summary>
		[TestCaseSource(nameof(GetDoBuildIndividuallyTestData))]
		public void TestToStringList(string testName, List<int> input, List<string> expected) => CoreTestUtils.TestListsAreEqual(expected, input.ToStringList());

		/// <summary>
		/// tests <see cref="Extensions.ToTimeSpan" />
		/// </summary>
		[TestCase("3.14159", true, 0, 0)]
		[TestCase("", false, 0, 0)]
		[TestCase(null, false, 0, 0)]
		[TestCase("invalid", false, 0, 0)]
		[TestCase("8", false, 8, 0)]
		[TestCase("0.25", false, 0, 250)]
		[TestCase("3.14159", false, 3, 142)]
		public void TestToTimeSpan(string input, bool ignore, int seconds, int theRest)
		{
			var actual = input.ToTimeSpan(ignore);
			Assert.AreEqual(seconds, actual.Seconds);
			Assert.AreEqual(theRest, actual.Milliseconds);
		}

		/// <summary>
		/// tests <see cref="Extensions.QuoteTheSpaces" />
		/// </summary>
		// ReSharper disable StringLiteralTypo
		[TestCase("noquotes", "noquotes")]
		// ReSharper restore StringLiteralTypo
		[TestCase("abc\\def\\ghi", "abc\\def\\ghi")]
		[TestCase("ab c\\def\\ghi", "\"ab c\"\\def\\ghi")]
		[TestCase("abc\\d ef\\ghi", "abc\\\"d ef\"\\ghi")]
		[TestCase("abc\\def\\g hi", "abc\\def\\\"g hi\"")]
		[TestCase("abc\\d e f\\ghi", "abc\\\"d e f\"\\ghi")]
		[TestCase("a bc\\de f\\ghi", "\"a bc\"\\\"de f\"\\ghi")]
		[TestCase("ab c\\def\\g hi", "\"ab c\"\\def\\\"g hi\"")]
		[TestCase("abc\\d ef\\g hi", "abc\\\"d ef\"\\\"g hi\"")]
		[TestCase("a bc\\de f\\g hi", "\"a bc\"\\\"de f\"\\\"g hi\"")]
		public void TestQuoteTheSpaces(string input, string expected) => TestValues(expected, input.QuoteTheSpaces(), input);

		private static IEnumerable GetDoBuildIndividuallyTestData()
		{
			yield return new TestCaseData("test 1", new List<int> {4, 9, 2, 8}, new List<string> {"4", "9", "2", "8"});
			yield return new TestCaseData("test 2", new List<int> {35, 1983, 375, 489, 185, 76},
				new List<string> {"35", "1983", "375", "489", "185", "76"});
			yield return new TestCaseData("test 3", new List<int>(), new List<string>());
		}

		private static void TestValues<T>(T expected, T actual, string input)
		{
			Assert.AreEqual(expected, actual, $"'{input}', returned '{actual}' instead of '{expected}'");
		}
	}
}
