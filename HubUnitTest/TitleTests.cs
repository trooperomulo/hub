﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Moq;

using Hub;
using Hub.Interface;
using HubUnitTest.Stubs;

using CTU = RyanCoreTests.CoreTestUtils;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="Title" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class TitleTests
	{
		#region Constants

		private const string Admin = "Administrator:  ";
		private const string Base = "2name0debug";
		private const string Remote = "2orig\\name0debug";

		private readonly IGit _git = new TitleGitStub();

		private readonly Type _type = typeof(Title);
		
		#endregion Constants

		#region Tests

		///
		[SetUp]
		public void Setup() => IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, UtilsTests.UtilsDirsStub>(new UtilsTests.UtilsDirsStub());

		///
		[TearDown]
		public void Teardown()
		{
			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests <see cref="Title.Initialize" />
		/// </summary>
		[Test]
		[TestCase(null, 0, null, 0)]
		[TestCase("format", 0, null, 0)]
		[TestCase(null, 1, null, 0)]
		[TestCase("format", 1, "format", 1)]
		public void SetUp(string f, int g, string ef, int eg)
		{
			Title.Instance.Initialize(f, GetGit(g), true, true);
			var format = CTU.GetPrivateField<string>(_type, Title.Instance, "_format");
			var git = CTU.GetPrivateField<IGit>(_type, Title.Instance, "_git");
			var isAdmin = CTU.GetPrivateField<bool>(_type, Title.Instance, "_isAdmin");
			var remote = CTU.GetPrivateField<bool>(_type, Title.Instance, "_showRemote");
			Assert.AreEqual(ef, format);
			Assert.AreEqual(GetGit(eg), git);
			Assert.AreEqual(f != null && g > 0, isAdmin);
			Assert.AreEqual(f != null && g > 0, remote);
		}

		/// <summary>
		/// tests <see cref="Title.Update" /> before <see cref="Title" /> has been initialized
		/// </summary>
		[Test]
		public void TestUpdatePreInitialize()
		{
			var cons = new Mock<IConsole>();
			cons.SetupProperty(c => c.Title);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);
			ClearInstance();
			var title = cons.Object.Title;
			Title.Instance.Update();
			Assert.AreEqual(title, cons.Object.Title);
		}
		
		/// <summary>
		/// tests <see cref="Title.Update" /> after <see cref="Title" /> has been initialized
		/// </summary>
		[Test]
		[TestCase(false, false, 0, Base)]
		[TestCase(false, true, 0, Remote)]
		[TestCase(true, false, 0, Admin + Base)]
		[TestCase(true, true, 0, Admin + Remote)]
		[TestCase(false, false, 1, Base)]
		[TestCase(false, true, 1, Remote)]
		[TestCase(true, false, 1, Admin + Base)]
		[TestCase(true, true, 1, Admin + Remote)]
		[TestCase(false, false, 2, Base + " - ab")]
		[TestCase(false, true, 2, Remote + " - ab")]
		[TestCase(true, false, 2, Admin + Base + " - ab")]
		[TestCase(true, true, 2, Admin + Remote + " - ab")]
		[TestCase(false, false, 3, Base + " - ab bc cd")]
		[TestCase(false, true, 3, Remote + " - ab bc cd")]
		[TestCase(true, false, 3, Admin + Base + " - ab bc cd")]
		[TestCase(true, true, 3, Admin + Remote + " - ab bc cd")]
		public void TestUpdate(bool isAdmin, bool showRemote, int which, string expected)
		{
			var cons = new Mock<IConsole>();
			cons.SetupProperty(c => c.Title);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(cons.Object);
			Environment.SetEnvironmentVariable("num", "2");
			Environment.SetEnvironmentVariable("mode", "debug");
			Title.Instance.Initialize("{0}{1}{2}{3}", new TitleGitStub(), isAdmin, showRemote);
			Title.Instance.Update(GetArgs(which));
			Assert.AreEqual(expected, cons.Object.Title);
		}

		#endregion Tests

		#region Helpers
		
		private void ClearInstance()
		{
			CTU.SetPrivateField(_type, Title.Instance, "_format", null);
			CTU.SetPrivateField(_type, Title.Instance, "_git", null);
			CTU.SetPrivateField(_type, Title.Instance, "_isAdmin", null);
			CTU.SetPrivateField(_type, Title.Instance, "_showRemote", null);
			CTU.SetPrivateStaticField(_type, "_instance", null);
		}

		private List<string> GetArgs(int which)
		{
			switch (which)
			{
				case 1:
					return new List<string>();
				case 2:
					return new List<string> { "ab" };
				case 3:
					return new List<string> { "ab", "bc", "cd" };
				default:
					return null;
			}
		}

		private IGit GetGit(int which) => which == 1 ? _git : null;

		#endregion Helpers

		#region Inner Class

		///
		private class TitleGitStub : BaseGitStub
		{
			private int _count;

			#region Implementation of IGit

			/// <inheritdoc />
			public override string GetBranchName() => $"name{_count++}";

			/// <inheritdoc />
			public override List<string> GetBranches(bool remote) => new List<string> { "a", "b", "c" };

			/// <inheritdoc />
			public override string GetRemoteName() => $@"orig\name{_count++}";

			#endregion Implementation of IGit
		}

		#endregion Inner Class
	}
}
