﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all console stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseConsStub : IConsole
	{
		internal string Output;

		#region Implementation of IConsole

		/// <inheritdoc />
		public virtual ConsoleColor BackgroundColor { get; set; }

		/// <inheritdoc />
		public virtual int CursorTop { get; set; }

		/// <inheritdoc />
		public virtual ConsoleColor ForegroundColor { get; set; }

		/// <inheritdoc />
		// ReSharper disable once UnassignedGetOnlyAutoProperty
		public virtual bool KeyAvailable { get; }

		/// <inheritdoc />
		public virtual string Title { get; set; }

		/// <inheritdoc />
		// ReSharper disable once UnassignedGetOnlyAutoProperty
		public virtual int Width { get; }

		/// <inheritdoc />
		public virtual void Beep() => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual char ReadKey() => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void SetCursorPosition(int left, int top) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void Write(string line) => Output += line;

		/// <inheritdoc />
		public virtual void WriteLine() => Output += Environment.NewLine;

		/// <inheritdoc />
		public virtual void WriteLine(string line) => Output += $"{line}{Environment.NewLine}";

		#endregion Implementation of IConsole

	}
}
