﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base call for all colorizer stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseSolutionColorizer : ISolutionColorizer
	{
		#region Implementation of ISolutionColorizer

		/// <inheritdoc />
		public virtual void ColorizeSolution(string colors, string path, string shortcut)
		{
			throw new NotImplementedException();
		}

		#endregion Implementation of ISolutionColorizer
	}
}
