﻿using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	///
	[ExcludeFromCodeCoverage]
	public class NoFilesLoggingStub : LoggingFilesStubBase
	{
		#region Overrides of BaseFileStub

		/// <inheritdoc />
		public override bool Exists(string path) => false;

		#endregion Overrides of BaseFileStub
	}
}
