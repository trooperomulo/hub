﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// used for testing v3 logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class LoggingV3FilesStub : LoggingVFilesStubBase
	{
		#region Constructor

		///
		public LoggingV3FilesStub(List<int> values) : base(values) { }

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override int Version => 3;

		#endregion Overrides of LoggingVFilesStubBase

	}	// end LoggingV3FilesStub

}	// end HubUnitTest.Stubs.Logging