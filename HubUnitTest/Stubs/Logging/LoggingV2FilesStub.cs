﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// used for testing v2 logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class LoggingV2FilesStub : LoggingVFilesStubBase
	{
		#region Constructor

		///
		public LoggingV2FilesStub(List<int> values) : base(values) { }

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override int Version => 2;

		#endregion Overrides of LoggingVFilesStubBase
	}
}
