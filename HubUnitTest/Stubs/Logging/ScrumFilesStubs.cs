﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Castle.Core.Internal;

namespace HubUnitTest.Stubs.Logging
{
	[ExcludeFromCodeCoverage]
	internal class ScrumFilesStubHelper
	{
		#region Variable

		private string _hours;

		#endregion Variable

		#region Constructor

		internal ScrumFilesStubHelper(string hours) => _hours = hours;

		#endregion Constructor

		#region Method

		internal void AddHours(List<string> lines)
		{
			if (!_hours.IsNullOrEmpty())
			{
				lines.Add(_hours);
				_hours = null;
			}	// end if

		}	// end AddHours(List<string>)

		#endregion Method

	}	// end ScrumFilesStubHelper
	
	[ExcludeFromCodeCoverage]
	internal class ScrumV0FilesStub : LoggingV0FilesStub
	{
		#region Variable

		private readonly string _hours;

		#endregion Variable

		#region Constructor

		internal ScrumV0FilesStub(List<int> values, string hours) : base(values) => _hours = hours;

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			var lines = base.ReadLines(path).ToList();
			if(!_hours.IsNullOrEmpty())
			{
				lines.Add(_hours);
			}	// end if
			return lines;

		} // end ReadLines(string)

		#endregion Overrides of LoggingVFilesStubBase

	} // end ScrumV0FilesStub
	
	[ExcludeFromCodeCoverage]
	internal class ScrumV1FilesStub : LoggingV1FilesStub
	{
		#region Variable

		private readonly ScrumFilesStubHelper _helper;

		#endregion Variable

		#region Constructor

		internal ScrumV1FilesStub(List<int> values, string hours) : base(values) =>
			_helper = new ScrumFilesStubHelper(hours);

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			var lines = base.ReadLines(path).ToList();

			_helper.AddHours(lines);

			return lines;

		} // end ReadLines(string)

		#endregion Overrides of LoggingVFilesStubBase

	}	// end ScrumV1FilesStub
	
	[ExcludeFromCodeCoverage]
	internal class ScrumV2FilesStub : LoggingV2FilesStub
	{
		#region Variable
		
		private readonly ScrumFilesStubHelper _helper;

		#endregion Variable

		#region Constructor

		internal ScrumV2FilesStub(List<int> values, string hours) : base(values) =>
			_helper = new ScrumFilesStubHelper(hours);

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			var lines = base.ReadLines(path).ToList();

			_helper.AddHours(lines);

			return lines;

		}	// end ReadLines(string)

		#endregion Overrides of LoggingVFilesStubBase

	}	// end ScrumV2FilesStub
	
	[ExcludeFromCodeCoverage]
	internal class ScrumV3FilesStub : LoggingV3FilesStub
	{
		#region Variable
		
		private readonly ScrumFilesStubHelper _helper;

		#endregion Variable

		#region Constructor

		internal ScrumV3FilesStub(List<int> values, string hours) : base(values) =>
			_helper = new ScrumFilesStubHelper(hours);

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			var lines = base.ReadLines(path).ToList();

			_helper.AddHours(lines);

			return lines;

		}	// end ReadLines(string)

		#endregion Overrides of LoggingVFilesStubBase

	}	// end ScrumV3FilesStub
	
	[ExcludeFromCodeCoverage]
	internal class ScrumV4FilesStub : LoggingV4FilesStub
	{
		#region Variable
		
		private readonly ScrumFilesStubHelper _helper;

		#endregion Variable

		#region Constructor

		internal ScrumV4FilesStub(List<int> values, string hours) : base(values) =>
			_helper = new ScrumFilesStubHelper(hours);

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			var lines = base.ReadLines(path).ToList();

			_helper.AddHours(lines);

			return lines;

		}	// end ReadLines(string)

		#endregion Overrides of LoggingVFilesStubBase

	}	// end ScrumV4FilesStub

}	// end HubUnitTest.Stubs.Logging
