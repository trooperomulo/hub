﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// base class for V0 logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class LogVFilesBase<T> : LoggingFilesStubBase
	{
		#region Variable

		///
		protected readonly T ToReturn;

		#endregion Variable

		#region Constructor

		internal LogVFilesBase(T toReturn) => ToReturn = toReturn;

		#endregion Constructor

		#region Abstract Member

		/// <summary>
		/// converts the values to return to lines
		/// </summary>
		/// <returns></returns>
		protected abstract IEnumerable<string> ConvertReturn();

		#endregion Abstract Member

		#region Overrides of BaseFileStub

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			return WrittenLines != null && WrittenLines.Count > 0
				? WrittenLines
				: ConvertReturn();
		}

		#endregion Overrides of BaseFileStub
	}
}
