﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// stub for testing v0 logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class LoggingV0FilesStub : LogVFilesBase<List<int>>
	{
		#region Constructor

		internal LoggingV0FilesStub(List<int> total) : base(total) { }

		#endregion Constructor

		#region Overrides of LogV0FilesBase<int>

		/// <inheritdoc />
		protected override IEnumerable<string> ConvertReturn() => ToReturn.Select(i => $"{i}");

		#endregion Overrides of LogV0FilesBase
	}
}
