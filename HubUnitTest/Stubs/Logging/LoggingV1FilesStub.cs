﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// used for testing v1 logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class LoggingV1FilesStub : LoggingVFilesStubBase
	{
		#region Constructor

		///
		public LoggingV1FilesStub(List<int> values) : base(values) { }

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override int Version => 1;

		#endregion Overrides of LoggingVFilesStubBase

	}	// end LoggingV1FilesStub

}	// end HubUnitTest.Stubs.Logging
