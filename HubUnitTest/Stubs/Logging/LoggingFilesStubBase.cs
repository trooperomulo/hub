﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace HubUnitTest.Stubs.Logging
{
	///
	[ExcludeFromCodeCoverage]
	public class LoggingFilesStubBase : BaseFileStub
	{
		#region Property

		internal List<string> WrittenLines { get; private set; }

		#endregion Property

		#region Overrides of BaseFileStub

		/// <inheritdoc />
		public override void AppendAllLines(string path, List<string> lines) => WrittenLines.AddRange(lines);

		/// <inheritdoc />
		public override bool Exists(string path) => true;

		/// <inheritdoc />
		public override void WriteAllLines(string path, IEnumerable<string> lines) => WrittenLines = lines.ToList();

		#endregion Overrides of BaseFileStub
	}
}
