﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// class to help with testing versioned logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public abstract class LoggingVFilesStubBase : LoggingFilesStubBase
	{
		#region Abstract Member

		///
		public abstract int Version { get; }

		#endregion Abstract Member

		#region Variable

		private readonly List<int> _values;

		#endregion Variable

		#region Constructor

		///
		protected LoggingVFilesStubBase(List<int> values) => _values = values;

		#endregion Constructor

		#region Overrides of BaseFileStub

		/// <inheritdoc />
		public override IEnumerable<string> ReadLines(string path)
		{
			if (WrittenLines != null && WrittenLines.Count > 0)
			{
				return WrittenLines;
			}

			var result = new List<string> {$"v{Version}"};
			result.AddRange(_values.Select(v => $"{v}"));
			return result;
		}

		#endregion Overrides of BaseFileStub
	}
}
