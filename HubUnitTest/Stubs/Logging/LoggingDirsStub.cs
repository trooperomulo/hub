﻿using System.Diagnostics.CodeAnalysis;

using Hub.Logging;

namespace HubUnitTest.Stubs.Logging
{
	///
	[ExcludeFromCodeCoverage]
	public class LoggingDirsStub : BaseDirsStub
	{
		#region Overrides of BaseDirsStub

		/// <inheritdoc />
		public override void CreateDirectory(string path) { }

		/// <inheritdoc />
		public override bool Exists(string path) => !path.Contains(Logger.Temp);

		#endregion Overrides of BaseDirsStub
	}
}