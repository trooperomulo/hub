﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// used for testing v4 logging files
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class LoggingV4FilesStub : LoggingVFilesStubBase
	{
		#region Constructor

		///
		public LoggingV4FilesStub(List<int> values) : base(values) { }

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inheritdoc />
		public override int Version => 4;

		#endregion Overrides of LoggingVFilesStubBase

	}	// end LoggingV4FilesStub

}	// end HubUnitTest.Stubs.Logging