﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	/// <summary>
	/// stub to test logging files return a version that is too large
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class LoggingV999FilesStub : LoggingVFilesStub
	{
		#region Constructor

		internal LoggingV999FilesStub() : base(new List<int>()) { }

		#endregion Constructor

		#region Overrides of VFiles

		/// <inheritdoc />
		public override int Version => 999;

		#endregion Overrides of VFiles
	}
}
