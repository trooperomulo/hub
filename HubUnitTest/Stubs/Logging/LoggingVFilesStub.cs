﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HubUnitTest.Stubs.Logging
{
	///
	[ExcludeFromCodeCoverage]
	public class LoggingVFilesStub : LoggingVFilesStubBase
	{
		#region Constructor

		internal LoggingVFilesStub(List<int> ints) : base(ints) { }

		#endregion Constructor

		#region Overrides of LoggingVFilesStubBase

		/// <inherticdoc />
		public override int Version => 0;

		#endregion Overrides of LoggingVFilesStubBase
	}
}

