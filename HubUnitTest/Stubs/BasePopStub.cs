﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all pop up stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BasePopStub : IPopupCloser
	{
		#region Implementation of IPopupCloser

		/// <inheritdoc />
		public virtual void StartProcess() => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void StopProcess() => throw new NotImplementedException();

		#endregion Implementation of IPopupCloser
	}
}
