﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all git stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseGitStub : IGit
	{
		#region Implementation of IGit

		/// <inheritdoc />
		public virtual string GetBranchName() => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual List<string> GetBranches(bool remote) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual string GetRemoteName() => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual bool IsNextGen() => throw new NotImplementedException();

		#endregion Implementation of IGit
	}
}
