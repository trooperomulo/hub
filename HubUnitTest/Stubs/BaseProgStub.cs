﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

using Hub.Enum;
using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all program stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseProgStub : IProgram
	{
		#region Implementation of IProgram

		/// <inheritdoc />
		public virtual ProcessStartInfo GetStartInfo(string path, string arguments, bool rso = true, bool rse = false)
		{
			throw new NotImplementedException();
		}

		/// <inheritdoc />
		public virtual void LaunchBinProgram(string filename, string label, string arguments = "", bool wait = true,
			ShowOutput show = ShowOutput.No)
		{
			throw new NotImplementedException();
		}

		/// <inheritdoc />
		public virtual bool RunBatchFile(string fileAndArgs, ShowOutput show = ShowOutput.No, bool rso = true, bool rse = false,
			bool wait = true, DataReceivedEventHandler outputHandler = null)
		{
			throw new NotImplementedException();
		}

		/// <inheritdoc />
		public virtual bool RunProgram(string path, string arguments = null, ShowOutput show = ShowOutput.No, bool rso = true,
			bool rse = false, bool wait = true, DataReceivedEventHandler outputHandler = null)
		{
			throw new NotImplementedException();
		}

		/// <inheritdoc />
		public virtual void KillProcess(string name) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void StartProcess(string filename) => throw new NotImplementedException();

		#endregion Implementation of IProgram
	}
}
