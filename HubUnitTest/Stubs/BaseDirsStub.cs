﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all directory stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseDirsStub : IDirectories
	{
		#region Implementation of IDirectories

		/// <inheritdoc />
		public virtual void CreateDirectory(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void Delete(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual bool Exists(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual string GetCurrentDirectory() => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual DirectoryInfo[] GetDirectories(string root, string searchPattern) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual List<string> GetFiles(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual List<string> GetFiles(string path, string filter) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual List<string> GetFiles(string path, string filter, SearchOption option) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual FileInfo[] GetFileInfos(string root, string filter) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual FileInfo GetMostRecentlyChangedFile(string dirPath, string filter) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void SetCurrentDirectory(string path) => throw new NotImplementedException();

		#endregion Implementation of IDirectories
	}
}
