﻿using System;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all email stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseEmailStub : IEmail
	{
		#region Implementation of IEmail

		/// <inheritdoc />
		public virtual void DisplayEmail(string to, string subject, string body) => throw new NotImplementedException();

		#endregion Implementation of IEmail
	}
}
