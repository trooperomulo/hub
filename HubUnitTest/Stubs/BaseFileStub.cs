﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all file stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseFileStub : IFiles
	{
		#region Implementation of IFiles

		/// <inheritdoc />
		public virtual void AppendAllLines(string path, List<string> lines) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void Copy(string source, string destination) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void Delete(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual bool Exists(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual DateTime GetLastWriteTime(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual Stream GetStream(string path, FileMode fileMode) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual string ReadAllText(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual IEnumerable<string> ReadLines(string path) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void WriteAllLines(string path, IEnumerable<string> lines) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void WriteXml(string path, string xml) => throw new NotImplementedException();

		#endregion Implementation of IFiles
	}
}
