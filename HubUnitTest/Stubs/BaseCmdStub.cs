﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub.Interface;

namespace HubUnitTest.Stubs
{
	/// <summary>
	/// base class for all command stubs
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class BaseCmdStub : ICommand
	{
		#region Implementation of IInput

		/// <inheritdoc />
		public virtual List<string> Inputs => throw new NotImplementedException();

		#endregion Implementation of IInput

		#region Implementation of ICommand

		/// <inheritdoc />
		public virtual string Description => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual bool HasSubCommands => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual bool NeedShortcut => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void Help(List<string> args) => throw new NotImplementedException();

		/// <inheritdoc />
		public virtual void Perform(List<string> args) => throw new NotImplementedException();

		#endregion Implementation of ICommand
	}
}
