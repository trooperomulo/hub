﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Hub;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

using RyanCoreTests;

using Entries = System.Collections.Generic.Queue<(int when, char key)>;
using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="WaitForUser" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class WaitForUserTests
	{
		#region Constants

		private const int MaxWait = 10;
		
		///
		public const char AllNumA = WaitForUser.AllNumA;
		///
		public const char AllNumB = WaitForUser.AllNumB;
		///
		public const char AllLet = WaitForUser.AllLet;
		///
		public const char NoneNumA = WaitForUser.NoneNumA;
		///
		public const char NoneNumB = WaitForUser.NoneNumB;
		///
		public const char NoneLet = WaitForUser.NoneLet;

		///
		public const int NumToUpLet = WaitForUser.NumToUpLet;

		///
		public const int NumToLoLet = WaitForUser.NumToLoLet;


		#endregion Constants

		#region Tests
		
		#region Multiple Selection

		#region Output Tests

		/// <summary>
		/// tests the console output with bad input
		/// </summary>
		[Test]
		public void TestMultipleOutputBadInput()
		{
			const int optCount = 3;
			var options = Options(optCount);
			var queue = BuildQueueBad();
			TestMultipleOutput(queue, false, 8, optCount,
				waitForUser =>
					waitForUser.MultipleSelectionEnterNotRequired(new List<string>(), options, "", "jumping", "nix", 8));
		}

		/// <summary>
		/// tests the console output with a custom max
		/// </summary>
		[TestCaseSource(nameof(GetMultipleOutputTestData))]
		public void TestMultipleOutputCustomMax(int entries, int optCount, bool enter)
		{
			var options = Options(optCount);
			var queue = BuildQueue(entries, optCount, enter, out var start);
			TestMultipleOutput(queue, enter, start, options.Count,
				waitForUser =>
					waitForUser.MultipleSelectionEnterNotRequired(new List<string>(), options, "", "jumping", "nix", start));
		}

		/// <summary>
		/// tests the console output with a default max
		/// </summary>
		[TestCaseSource(nameof(GetMultipleOutputTestData))]
		public void TestMultipleOutputDefaultMax(int entries, int optCount, bool enter)
		{
			var options = Options(optCount);
			var queue = BuildQueue(entries, optCount, enter, out _);
			TestMultipleOutput(queue, enter, MaxWait, options.Count,
				waitForUser =>
					waitForUser.MultipleSelectionEnterNotRequired(new List<string>(), options, "", "jumping", "nix"));
		}

		/// <summary>
		/// tests the console output when too many are given
		/// </summary>
		public void TestMultipleOutputTooManyOptions()
		{
			var exception = Assert.Throws<NotImplementedException>(Call);

			void Call()
			{
				CreateInstance().MultipleSelectionEnterNotRequired(new List<string>(), Options(99), "", "", "");
			}	// end Call
				
			Assert.AreEqual("I need a new options type that handles 99 options.", exception.Message);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestMultipleOutputTooManyOptions()

		private void TestMultipleOutput(Entries entries, bool enter, int start, int numOpts, Action<WaitForUser> call)
		{
			var passed = 0;
			var nextEntry = entries.TryPeek();
			var count = start;
			var selected = List();
			var expected = BuildExpectedForMultiple("", numOpts, selected);
			var lastVerb = "jumping";

			#region Inner Methods

			// ReSharper disable once AccessToModifiedClosure
			// adds a new line to the expected output
			void AddToExp() => expected = AddToExpected(expected, $"\r{GetVerb()} in {count} seconds    ");

			// gets the verb used in the output based on how many items are in the selected list
			string GetVerb()
			{
				if (count != start)
				{
					return lastVerb;
				}	// end if

				lastVerb = selected.Count == 0 ? "jumping" : "nix";
				return lastVerb;

			}	// end GetVerb()

			// updates the selected list based on what key was "entered"
			void UpdateSelected(char ch)
			{
				if (IsAll(ch, numOpts))
				{
					selected.SelectAll(numOpts);
					return;
				}	// end if

				if (IsNone(ch, numOpts))
				{
					selected.Clear();
					return;
				}	// end if

				if (TryToGetNum(ch, numOpts, out var num))
				{
					selected.Flip(num);
				}	// end if

			}	// end UpdateSelected(char)

			// this mimics the console method that determines if a key has been pressed
			// we press a key if there is an entry that needs to be pressed, and the time to
			// press it has come
			bool Avail() => entries.Count > 0 && nextEntry.when == passed;

			// this is called when WaitForUser tries to read the user's input
			char Read()
			{
				// ReSharper disable once PossibleInvalidOperationException
				var ch = nextEntry.key;
				UpdateSelected(ch);
				// get ready for the next "keystroke"
				entries.Dequeue();
				nextEntry = entries.TryPeek();
				return ch;
			}	// end Read()

			// in normal execution, this moves the cursor to new position
			void SetCursorPosition(int left, int top)
			{
				AddToExp();
				expected = BuildExpectedForMultiple($"{expected}\r\n", numOpts, selected);
				count = start;
				passed = 0;
			}	// end SetCursorPosition(int, int)

			// in normal execution, this tells wait for use to wait a second before checking 
			// for another keystroke
			void Sleep(int num)
			{
				AddToExp();
				count--;
				passed++;
			}	// end Sleep(int)

			#endregion Inner Methods

			var cons = new ConsStub(Avail, Read, SetCursorPosition);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			call(CreateInstance(MaxWait, Sleep));

			if (enter)
			{
				// if enter was pressed, that will add more output so we need to account for that
				AddToExp();
				expected = AddToExpected(expected, "\r\n");
			}	// end if

			Assert.AreEqual(expected, cons.Output, $"{nameof(start)}: {start}");

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}
		
		#endregion Output Tests

		#region Result Tests

		/// <summary>
		/// tests the user entering the smallest possible
		/// </summary>
		[Test]
		public void TestMultiple0Entry() => TestMultipleEntryOutput(Queue('0'), new List<int> {0});

		/// <summary>
		/// tests the user entering all
		/// </summary>
		[Test]
		public void TestMultipleAllEntry() => TestMultipleEntryOutput(Queue(All(5)), List(0, 1, 2, 3, 4));

		/// <summary>
		/// tests the user entering all then a valid number
		/// </summary>
		[Test]
		public void TestMultipleAllGoodEntry() => TestMultipleEntryOutput(Queue(AllNumA, '2'), List(0, 1, 3, 4));

		/// <summary>
		/// tests the user entering all then none
		/// </summary>
		[Test]
		public void TestMultipleAllNoneEntry() => TestMultipleEntryOutput(Queue(AllNumB, NoneNumA));

		/// <summary>
		/// tests the user entering bad input
		/// </summary>
		[Test]
		public void TestMultipleBadEntry() => TestMultipleEntryOutput(Queue('b', ';', ' ', '*'));

		/// <summary>
		/// tests the user entering a valid number then all
		/// </summary>
		[Test]
		public void TestMultipleGoodAllEntry() => TestMultipleEntryOutput(Queue('2', AllNumA), List(0, 1, 2, 3, 4));

		/// <summary>
		/// tests the user entering something valid
		/// </summary>
		[Test]
		public void TestMultipleGoodEntry() => TestMultipleEntryOutput(Queue('2'), List(2));

		/// <summary>
		/// tests the user entering a valid number then all
		/// </summary>
		[Test]
		public void TestMultipleGoodNoneEntry() => TestMultipleEntryOutput(Queue('2', NoneNumB));

		/// <summary>
		/// tests the user entering the largest possible
		/// </summary>
		[Test]
		public void TestMultipleLargestEntry() => TestMultipleEntryOutput(Queue('4'), List(4));

		/// <summary>
		/// tests the user not entering anything
		/// </summary>
		[Test]
		public void TestMultipleNoEntry() => TestMultipleEntryOutput(Queue());

		/// <summary>
		/// tests the user entering none then all
		/// </summary>
		[Test]
		public void TestMultipleNoneAllEntry() => TestMultipleEntryOutput(Queue(NoneNumA, AllNumB), List(0, 1, 2, 3, 4));

		/// <summary>
		/// tests the user entering none
		/// </summary>
		[Test]
		public void TestMultipleNoneEntry() => TestMultipleEntryOutput(Queue(None(5)));

		/// <summary>
		/// tests the user entering none then something valid
		/// </summary>
		[Test]
		public void TestMultipleNoneGoodEntry() => TestMultipleEntryOutput(Queue(NoneNumB, '2'), List(2));

		/// <summary>
		/// tests the user entering a letter too large
		/// </summary>
		[Test]
		public void TestMultipleTooLargeLetEntry() => TestMultipleEntryOutput(Queue('Z'), num: 15);

		/// <summary>
		/// tests the user entering a number too large
		/// </summary>
		[Test]
		public void TestMultipleTooLargeNumEntry() => TestMultipleEntryOutput(Queue('9'));
		
		private void TestMultipleEntryOutput(Queue<char> queue, List<int> expected = null, int num = 5)
		{
			var wasRead = queue.Count > 0;
			var read = false;
			bool Avail() => queue.Count > 0;

			char Read()
			{
				read = true;
				return queue.Dequeue();
			}

			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(new ConsStub(Avail, Read));
			
			var actual = CreateInstance().MultipleSelectionEnterNotRequired(new List<string>(), Options(num), "", "", "", 8);
			CoreTestUtils.TestListsAreEqual(expected ?? List(), actual);
			Assert.AreEqual(wasRead, read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		#endregion Result Tests

		#endregion Multiple Selection

		#region Single Selection

		#region Output Tests

		/// <summary>
		/// tests the console output if the user doesn't enter anything
		/// </summary>
		[Test]
		public void TestSingleOutputNoEntry()
		{
			var count = 8;
			var expected = "\r\n";

			var cons = new ConsStub(() => false);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			void Sleep(int num)
			{
				expected += $"\rwarning in {count} seconds    ";
				count--;
			}

			CreateInstance(Sleep).SingleSelection(new List<string>(), new List<string>(), "", "warning", 8);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests the console output if the user does enter something valid
		/// </summary>
		[Test]
		public void TestSingleOutputEntry()
		{
			var count = 8;
			var expected = "\r\n";
			bool Avail() => count < 4;

			char Read() => '5';

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			void Sleep(int num)
			{
				expected += $"\rwarning in {count} seconds    ";
				count--;
			}

			CreateInstance(Sleep).SingleSelection(new List<string>(), Options(), "", "warning", 8);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests the console output if the user doesn't enter anything with the default max
		/// </summary>
		[Test]
		public void TestSingleOutputNoEntryDef()
		{
			var count = 10;
			var expected = "\r\n";

			var cons = new ConsStub(() => false);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			void Sleep(int num)
			{
				expected += $"\rwarning in {count} seconds    ";
				count--;
			}

			CreateInstance(Sleep).SingleSelection(new List<string>(), Options(), "", "warning");

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests the console output if the user does enter something valid
		/// </summary>
		[Test]
		public void TestSingleOutputEntryDef()
		{
			var count = 10;
			var expected = BuildExpectedForSingle(5);
			bool Avail() => count < 4;

			char Read() => '5';

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			void Sleep(int num)
			{
				expected += $"\rwarning in {count} seconds    ";
				count--;
			}

			CreateInstance(Sleep).SingleSelection(new List<string>(), Options(5), "", "warning", count);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}
		
		/// <summary>
		/// tests that the header and options are printed
		/// </summary>
		[Test]
		public void TestSinglePrintingLetters()
		{
			var cons = new ConsStub(() => false);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var header = new List<string> {"head1", "head2"};
			var options = new List<string>
			{
				"one", "two", "thr", "fou", "fiv", "six", "sev", "eig", "nin", "ten", "ele", "twe", "thi", "fou", "fif"
			};
			var question = "asking";

			var nl = "\r\n";
			var expected =
				$"head1{nl}head2{nl}{nl}" +
				$"  A\tone{nl}  B\ttwo{nl}  C\tthr{nl}  D\tfou{nl}" +
				$"  E\tfiv{nl}  F\tsix{nl}  G\tsev{nl}  H\teig{nl}" +
				$"  I\tnin{nl}  J\tten{nl}  K\tele{nl}  L\ttwe{nl}" +
				$"  M\tthi{nl}  N\tfou{nl}  O\tfif{nl}{nl}" +
				$"{question}{nl}{nl}\rwarn in 0 seconds    ";

			CreateInstance().SingleSelection(header, options, question, "warn", 0);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests that the header and options are printed
		/// </summary>
		[Test]
		public void TestSinglePrintingNumbers()
		{
			var cons = new ConsStub(() => false);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var header = new List<string> {"head1", "head2"};
			var options = new List<string> {"one", "two", "thr", "fou", "fiv"};
			var question = "asking";

			var nl = "\r\n";
			var expected =
				$"head1{nl}head2{nl}{nl}  0\tone{nl}  1\ttwo{nl}  2\tthr{nl}  3\tfou{nl}  4\tfiv{nl}{nl}{question}{nl}{nl}\rwarn in 0 seconds    ";

			CreateInstance().SingleSelection(header, options, question, "warn", 0);

			Assert.AreEqual(expected, cons.Output);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
		}

		/// <summary>
		/// tests the console output when too many are given
		/// </summary>
		[Test]
		public void TestSingleOutputTooManyOptions()
		{
			var exception = Assert.Throws<NotImplementedException>(Call);

			void Call()
			{
				CreateInstance().SingleSelection(new List<string>(), Options(99), "", "");
			}	// end Call
				
			Assert.AreEqual("I need a new options type that handles 99 options.", exception.Message);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleOutputTooManyOptions()

		#endregion Output Tests

		#region Result Tests

		/// <summary>
		/// tests the user selecting a letter
		/// </summary>
		[Test]
		public void TestSingleLetters()
		{
			var read = false;
			bool Avail() => true;

			var num = Rnd.Int(11, 27);
			var expected = Rnd.Int(0, num);

			char Read()
			{
				read = true;
				return ToChar(expected);
			}	// end Read()

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "");
			Assert.AreEqual(expected, actual);
			Assert.IsTrue(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleLetters()

		/// <summary>
		/// tests the user not entering anything
		/// </summary>
		[TestCaseSource(nameof(GetJustOptionTypes))]
		public void TestSingleNoEntry(int num)
		{
			var read = false;
			bool Avail() => false;

			char Read()
			{
				read = true;
				return 'c';

			}	// end Read()

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "warning", 8);
			Assert.AreEqual(-1, actual);
			Assert.IsFalse(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleNoEntry(int)

		/// <summary>
		/// tests the user entering something that isn't a number
		/// </summary>
		[TestCaseSource(nameof(GetJustOptionTypes))]
		public void TestSingleInvalidEntry(int num)
		{
			var read = false;
			bool Avail() => true;

			char Read()
			{
				read = true;
				return '(';

			}	// end Read()

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);
			
			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "warning", 8);
			Assert.AreEqual(-1, actual);
			Assert.IsTrue(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleInvalidEntry(int)

		/// <summary>
		/// tests the user entering in 0 (the lower edge case)
		/// </summary>
		[TestCaseSource(nameof(GetJustOptionTypes))]
		public void TestSingleMin(int num)
		{
			var read = false;
			bool Avail() => true;

			char Read()
			{
				read = true;
				return ToChar(0, num);

			}	// end Read()

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "warning", 8);
			Assert.AreEqual(0, actual);
			Assert.IsTrue(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleMin(int)

		/// <summary>
		/// tests the user entering in something between the lower and upper cases
		/// </summary>
		[TestCaseSource(nameof(GetOptionAndSelection))]
		public void TestSingleMid(int num, int expected)
		{
			var read = false;
			bool Avail() => true;

			char Read()
			{
				read = true;
				return ToChar(expected, num);

			}	// end Read()

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "warning", 8);
			Assert.AreEqual(expected, actual);
			Assert.IsTrue(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleMid(int, int)

		/// <summary>
		/// tests the user entering in the upper case
		/// </summary>
		[TestCaseSource(nameof(GetJustOptionTypes))]
		public void TestSingleMax(int num)
		{
			var read = false;
			bool Avail() => true;

			var expected = num - 1;

			char Read()
			{
				read = true;
				return ToChar(expected, num);

			}	// end Read()

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "warning", 8);
			Assert.AreEqual(expected, actual);
			Assert.IsTrue(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleMax(int)

		/// <summary>
		/// tests the user entering in something to large
		/// </summary>
		[TestCaseSource(nameof(GetJustOptionTypes))]
		public void TestSingleTooLarge(int num)
		{
			var read = false;
			bool Avail() => true;

			char Read()
			{
				read = true;
				return ToChar(num, num);
			}

			var cons = new ConsStub(Avail, Read);
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, ConsStub>(cons);

			var actual = CreateInstance().SingleSelection(new List<string>(), Options(num), "", "warning", 8);
			Assert.AreEqual(-1, actual);
			Assert.IsTrue(read);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();

		}	// end TestSingleTooLarge(int)

		#endregion Result Tests

		#endregion Single Selection

		#endregion Tests

		#region Helpers

		private static string AddToExpected(string expected, string toAdd) => $"{expected}{toAdd}";

		private static char All(int numOpts) => numOpts < 11 ? Rnd.Bool() ? AllNumA : AllNumB : AllLet;

		private static string BuildExpectedForMultiple(string expected, int numOpts, List<int> selected)
		{
			const string nl = "\r\n";
			var allChar = (numOpts > 10 ? "*" : "A");
			var nonChar = numOpts > 10 ? "/" : "N";
			var end = $"{nl}  {allChar} [X]\tAll{nl}  {nonChar} [ ]\tNone{nl}{nl}";

			if (numOpts == 0)
			{
				return $"{expected}{end}";
			}

			Func<int, string> sel;
			
			if (numOpts < 11)
			{
				sel = ToNumStr;
			}	// end if
			else // if (numOpts < 27)
			{
				sel = ToLetStr;
			}	// end else

			for (var i = 0; i < numOpts; i++)
			{
				expected = $"{expected}  {sel(i)} [{(selected.Contains(i) ? "X" : " ")}]\t{nl}";
			}	// end for

			return $"{expected}{end}";
		}

		private static string BuildExpectedForSingle(int numOpts)
		{
			const string nl = "\r\n";
			string expected = "";
			
			Func<int, string> sel;
			
			if (numOpts < 11)
			{
				sel = ToNumStr;
			}	// end if
			else // if (numOpts < 27)
			{
				sel = ToLetStr;
			}	// end else

			for (var i = 0; i < numOpts; i++)
			{
				expected = $"{expected}  {sel(i)}\t{nl}";
			}

			return expected + nl;

		}	// end BuildExpectedForSingle(int)

		private static Entries BuildQueue(int entries, int optsCount, bool enter, out int total)
		{
			total = 0;
			var queue = new Entries();

			char GetKey()
			{
				var useChar = Rnd.Bool() || Rnd.Bool();	// 3 out of 4 times we'll enter a number
				if (!useChar)
				{
					return Rnd.Bool() ? All(optsCount) : None(optsCount);
				}	// end if

				char key;
				if (optsCount < 11)
				{
					var str = Rnd.Int(0, optsCount).ToString();
					key = str[0];
				}	// end if
				else
				{
					var num = Rnd.Int(0, optsCount);
					key = ToChar(num);
				}	// end else

				return key;
			}	// end GetKey()

			for (var i = 0; i < entries; i++)
			{
				var when = Rnd.Int(1, 4); // wait 1, 2 or 3 seconds before entering
				var key = GetKey(); // which option will I pick
				queue.Enqueue((when, key));
				total += when;
			}

			if (enter)
			{
				var when = Rnd.Int(1, 4); // wait 1, 2 or 3 seconds before entering
				var key = Rnd.Bool() ? '\r' : '\n'; // which type of enter
				queue.Enqueue((when, key));
				total += when;
			}

			return queue;
		}

		private static Entries BuildQueueBad()
		{
			var queue = new Entries();

			queue.Enqueue((0, 'b'));
			queue.Enqueue((1, ' '));
			queue.Enqueue((2, '9'));
			queue.Enqueue((3, '-'));
			queue.Enqueue((4, '&'));

			return queue;
		}
		
		private static WaitForUser CreateInstance(Action<int> sleeper) => CreateInstance(MaxWait, sleeper);

		private static WaitForUser CreateInstance(int promptWait = MaxWait, Action<int> sleeper = null)
		{
			if (sleeper == null)
			{
				sleeper = i => { };
			}

			return new WaitForUser(promptWait, sleeper);
		}

		private static IEnumerable GetMultipleOutputTestData()
		{
			for (var i = 0; i < 11; i++)
			{
				yield return new TestCaseData(i, Rnd.Int(3, 11), true) {TestName = $"Numbers, {i} entries, Enter"};
				yield return new TestCaseData(i, Rnd.Int(3, 11), false) {TestName = $"Numbers, {i} entries, No Enter"};
				yield return new TestCaseData(i, Rnd.Int(11, 27), true) {TestName = $"Letters, {i} entries, Enter"};
				yield return new TestCaseData(i, Rnd.Int(11, 27), false) {TestName = $"Letters, {i} entries, No Enter"};
			}	// end for

		}	// end GetMultipleOutputTestData()

		private static IEnumerable GetJustOptionTypes()
		{
			var num = Rnd.Int(1, 10);
			var let = Rnd.Int(11, 27);
			yield return new TestCaseData(num) {TestName = $"Numbers {num}"};
			yield return new TestCaseData(let) {TestName = $"Letters {let}"};

		}	// end GetSingleNoEntryTestData()

		private static IEnumerable GetOptionAndSelection()
		{
			var cases = GetJustOptionTypes().Cast<TestCaseData>().ToList();
			foreach (var @case in cases)
			{
				var num = (int) @case.Arguments[0];
				var exp = Rnd.Int(0, num);
				yield return new TestCaseData(num, exp) {TestName = $"{@case.TestName}, {exp}"};
			}	// end foreach

		}	// end GetSingleMidTestData()

		private static bool IsAll(char ch, int numOpts) => numOpts < 11 ? ch == AllNumA || ch == AllNumB : ch == AllLet;
		private static bool IsNone(char ch, int numOpts) => numOpts < 11 ? ch == NoneNumA || ch == NoneNumB : ch == NoneLet;

		private static List<int> List(params int[] nums) => nums.ToList();

		private static char None(int numOpts) => numOpts < 11 ? Rnd.Bool() ? NoneNumA : NoneNumB : NoneLet;

		private static List<string> Options(int num = 0)
		{
			var options = new List<string>();

			for (var i = 0; i < num; i++)
			{
				options.Add("");
			}

			return options;
		}

		private static Queue<char> Queue(params char[] chars)
		{
			var queue = new Queue<char>();

			foreach (var ch in chars)
			{
				queue.Enqueue(ch);
			}

			return queue;
		}
		
		private static char ToChar(int i) => (char) (i + (Rnd.Bool() ? NumToLoLet : NumToUpLet));	// 50% upper, 50% lower

		private static char ToChar(int toConvert, int numOpts)
		{
			if (numOpts < 11)
			{
				return (char) (toConvert + 48);
			}	// end if

			return ToChar(toConvert);

		}	// end ToChar(int, int)

		private static string ToNumStr(int i) => $"{i}";

		private static string ToLetStr(int i) => $"{(char) (i + 65)}";

		private static bool TryToGetNum(char ch, int numOpts, out int num)
		{
			if (numOpts < 11)
			{
				return int.TryParse(ch.ToString(), out num);
			}	// end if

			if (ch >= NumToLoLet)
			{
				num = ch - NumToLoLet;
				return true;
			}	// end if

			if (ch >= NumToUpLet)
			{
				num = ch - NumToUpLet;
				return true;
			}	// end if

			num = -1;
			return false;

		}	// end TryToGetNum(char, int, out int)

		#endregion Helpers

		#region Inner Class

		private class ConsStub : BaseConsStub
		{
			private readonly Func<bool> _keyAvail;

			private readonly Func<char> _read;

			private readonly Action<int, int> _setCurPos;

			internal ConsStub(Func<bool> keyAvail, Func<char> read = null, Action<int, int> setCurPos = null)
			{
				_keyAvail = keyAvail;
				_read = read;
				_setCurPos = setCurPos;
			}

			public override bool KeyAvailable => _keyAvail();

			/// <inheritdoc />
			public override char ReadKey() => _read?.Invoke() ?? default(char);

			#region Overrides of BaseConsStub

			/// <inheritdoc />
			public override void SetCursorPosition(int left, int top) => _setCurPos?.Invoke(left, top);

			#endregion Overrides of BaseConsStub
		}

		#endregion Inner Class
	}
}
