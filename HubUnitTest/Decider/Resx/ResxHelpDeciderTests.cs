﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Resx;
using Hub.Decider.Resx;
using Hub.Enum;

namespace HubUnitTest.Decider.Resx
{
	/// <summary>
	/// tests <see cref="ResxHelpDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxHelpDeciderTests
	{
		#region Variable

		private static string _output;

		#endregion Variable

		#region Test

		/// <summary>
		/// tests <see cref="ResxHelpDecider" />
		/// </summary>
		[Test]
		[TestCase("a", "rdt", "abc", "A was called")]
		[TestCase("bb", "zy", "lmt", "B was called")]
		[TestCase("ajt", "lrt", "kit", "Def was called")]
		public void TestHelp(string a, string b, string c, string expected)
		{
			ResxDecider.InitCommands(new List<ResxBase> {new HelpAStub(), new HelpBStub()}, new HelpDefStub(), null);
			ResxHelpDecider.Instance.Help(new List<string> {a, b, c});
			Assert.AreEqual(expected, _output);
		}

		#endregion Test

		#region Internal Classes

		private class HelpAStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "a";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() => _output = "A was called";

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path) => _output = "";

			#endregion Overrides of ResxBase
		}

		private class HelpBStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "bb";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() => _output = "B was called";

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path) => _output = "";

			#endregion Overrides of ResxBase
		}

		private class HelpDefStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() => _output = "Def was called";

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path) => _output = "";
			
			#endregion Overrides of ResxBase
		}

		#endregion Internal Classes
	}
}
