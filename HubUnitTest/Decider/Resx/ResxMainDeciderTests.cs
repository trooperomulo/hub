﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Moq;

using NUnit.Framework;

using Hub;
using Hub.Commands.Resx;
using Hub.Decider.Resx;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Decider.Resx
{
	/// <summary>
	/// Tests <see cref="ResxMainDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxMainDeciderTests
	{
		#region Variables

		private static Res _res;
		private static int _count;
		private static string _path;
		private static string _cmd;

		#endregion Variables

		#region Tests

		/// <summary>
		/// tests <see cref="ResxMainDecider.AskUserForPath" />
		/// </summary>
		[TestCaseSource(nameof(GetAskUserForPathTestData))]
		public void TestAskUserForPath(bool showAll, int sel, string selection)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, IConsole>(new Mock<IConsole>().Object);
			
			var wait = new Mock<IWaitForUser>();
			wait.Setup(w => w.SingleSelection(AnyStringList(), AnyStringList(), AnyString(), AnyString())).Returns(sel);
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(wait.Object);

			List<string> GetFolders() => new List<string> {"one", "two", "three"};

			ResxMainDecider.Initialize(GetFolders);
			var actual = ResxMainDecider.AskUserForPath(new DirStub(), showAll);
			Assert.AreEqual(selection.Trim(), actual);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();

		}	// end TestAskUserForPath(bool, int, string)
		
		/// <summary>
		/// tests <see cref="ResxMainDecider.DetermineUseCore" />
		/// </summary>
		[TestCaseSource(nameof(GetDetermineUseCoreTestData))]
		public void TestDetermineUseCore(bool core, bool xm8, bool shared, Res expRes, int expected)
		{
			Assert.AreEqual(expected, ResxMainDecider.DetermineUseCore(core, xm8, shared, out var res), "returned the wrong value");
			Assert.AreEqual(expRes, res, $"{nameof(expRes)} was set incorrectly");

		}	// end TestDetermineUseCore(bool, bool, bool, Res, int)

		/// <summary>
		/// tests <see cref="ResxMainDecider.GetResParams" />
		/// </summary>
		[TestCaseSource(nameof(GetGetResParamsTestData))]
		public void TestGetResParams(bool core, bool xm8, bool shared, bool showAll, int sel, Res expRes, string expPath, bool expResult,
			string expOutput)
		{
			var cons = new BaseConsStub();
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);
			
			var wait = new Mock<IWaitForUser>();
			wait.Setup(w => w.SingleSelection(AnyStringList(), AnyStringList(), AnyString(), AnyString())).Returns(sel);
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(wait.Object);

			List<string> GetFolders() => new List<string> {"one"};

			ResxMainDecider.Initialize(GetFolders);
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirStub>(new DirStub());
			var result = ResxMainDecider.GetResParams(core, xm8, shared, showAll, out var res, out var path);
			Assert.AreEqual(expRes, res, "res was wrong");
			Assert.AreEqual(expPath, path, "path was wrong");
			Assert.AreEqual(expResult, result, "result was wrong");

			if(!expOutput.IsNullOrEmpty())
			{
				Assert.AreEqual(expOutput, cons.Output);
			}

			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();

		}	// end TestGetResParams()

		/// <summary>
		/// tests <see cref="ResxMainDecider.GetResPath" />
		/// </summary>
		[TestCase(Res.None, "")]
		[TestCase(Res.Core, @"xactimate\xactimate.shared\xm8core\Core.Data.Res")]
		[TestCase(Res.Xm8, @"xactimate\xactimate.shared\xm8core\Xm8.Data.Res")]
		[TestCase(Res.Shared, @"xactimate\xactimate.shared\Shared.Res")]
		[TestCase(Res.All, ResxBase.UseAll)]
		[TestCase((Res)999, null)]
		public void TestGetResPath(Res res, string expected)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());
			
			var wait = new Mock<IWaitForUser>();
			wait.Setup(w => w.SingleSelection(AnyStringList(), AnyStringList(), AnyString(), AnyString())).Returns(-1);
			IoCContainer.Instance.RegisterAsSingleInstance<IWaitForUser, IWaitForUser>(wait.Object);

			List<string> GetFolders() => new List<string> { "one", "two", "three" };

			ResxMainDecider.Initialize(GetFolders);
			var actual = ResxMainDecider.GetResPath(new DirStub(), res, false);
			Assert.AreEqual(expected, actual);

			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			IoCContainer.Instance.RemoveRegisteredType<IWaitForUser>();

		}	// end TestGetResPath(Res, string)

		/// <summary>
		/// tests <see cref="ResxMainDecider.Perform" />
		/// </summary>
		[TestCaseSource(nameof(GetPerformTestData))]
		public void TestPerform(List<string> args, Res expRes, int expCount, string expCmd, string expPath)
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(new BaseConsStub());

			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, DirStub>(new DirStub());

			ResxDecider.InitCommands(new List<ResxBase> {new ResxAStub(), new ResxBStub()}, new ResxDefStub(), null);
			ResxMainDecider.Instance.Perform(args);
			Assert.AreEqual(expRes, _res, "res was wrong");
			Assert.AreEqual(expCount, _count, "count was wrong");
			Assert.AreEqual(expCmd, _cmd, "cmd was wrong");
			Assert.AreEqual(expPath, _path, "path was wrong");

			IoCContainer.Instance.RemoveRegisteredType<IDirectories>();
			IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			
		}	// end TestPerform(List<string>, Res, int, string, string)
		
		#endregion Test

		#region Helpers

		private static string AnyString() => It.IsAny<string>();

		private static List<string> AnyStringList() => It.IsAny<List<string>>();

		private static IEnumerable GetAskUserForPathTestData()
		{
			yield return new TestCaseData(false, -1, "");
			yield return new TestCaseData(true, -1, "");
			yield return new TestCaseData(false, 0, "\r\none\r\n");
			yield return new TestCaseData(true, 0, "\r\nUse All\r\n");
			yield return new TestCaseData(false, 1, "\r\ntwo\r\n");
			yield return new TestCaseData(true, 1, "\r\none\r\n");
			yield return new TestCaseData(false, 2, "\r\nthree\r\n");
			yield return new TestCaseData(true, 2, "\r\ntwo\r\n");

		}	// end GetAskUserForPathTestData()

		private static IEnumerable GetDetermineUseCoreTestData()
		{
			// none
			yield return new TestCaseData(false, false, false, Res.None, 0);
			// one
			yield return new TestCaseData(true, false, false, Res.Core, 1);
			yield return new TestCaseData(false, true, false, Res.Xm8, 1);
			yield return new TestCaseData(false, false, true, Res.Shared, 1);
			// two
			yield return new TestCaseData(true, true, false, Res.Core | Res.Xm8, 2);
			yield return new TestCaseData(true, false, true, Res.Core | Res.Shared, 2);
			yield return new TestCaseData(false, true, true, Res.Xm8 | Res.Shared, 2);
			// three
			yield return new TestCaseData(true, true, true, Res.All, 3);

		}	// end GetDetermineUseCoreTestData()
		
		private static IEnumerable GetGetResParamsTestData()
		{
			// all passed in
			yield return new TestCaseData(true, true, true, true, -1, Res.All, ResxBase.UseAll, true, null);
			yield return new TestCaseData(true, true, true, false, -1, Res.All, null, false,
				"You are only allowed to specify one or no resource solutions.  Do not specify more than one.\r\n");
			// two passed in
			yield return new TestCaseData(false, true, true, true, -1, Res.Xm8 | Res.Shared, null, false,
				"You are only allowed to specify one or all resource solutions.\r\n");
			yield return new TestCaseData(true, false, true, true, -1, Res.Core | Res.Shared, null, false,
				"You are only allowed to specify one or all resource solutions.\r\n");
			yield return new TestCaseData(true, true, false, true, -1, Res.Core | Res.Xm8, null, false,
				"You are only allowed to specify one or all resource solutions.\r\n");
			// one passed in
			yield return new TestCaseData(true, false, false, false, -1, Res.Core,
				@"xactimate\xactimate.shared\xm8core\Core.Data.Res", true, null);
			yield return new TestCaseData(false, true, false, false, -1, Res.Xm8,
				@"xactimate\xactimate.shared\xm8core\Xm8.Data.Res", true, null);
			yield return new TestCaseData(false, false, true, false, -1, Res.Shared,
				@"xactimate\xactimate.shared\Shared.Res", true, null);
			// none passed in
			yield return new TestCaseData(false, false, false, false, -1, Res.None, "", false, null);
			yield return new TestCaseData(false, false, false, false, 0, Res.None, "one", true, null);
			yield return new TestCaseData(false, false, false, true, -1, Res.None, "", false, null);
			yield return new TestCaseData(false, false, false, true, 0, Res.All, ResxBase.UseAll, true, null);

		}	// end GetGetResParamsTestData()

		private static IEnumerable GetPerformTestData()
		{
			const string a = "a";
			const string b = "b";
			const string d = "c";
			const string c = "-c";
			const string x = "-x";
			const string s = "-s";
			const string r = "rdt";
			// too many res specified
			yield return new TestCaseData(Ls(a, c, x, s, r), Res.None, 0, null, null) {TestName = "Perform - All Res"};
			yield return new TestCaseData(Ls(a, c, x, r), Res.None, 0, null, null) {TestName = "Perform - Core and Xm8"};
			yield return new TestCaseData(Ls(a, c, s, r), Res.None, 0, null, null) {TestName = "Perform - Core and Shared"};
			yield return new TestCaseData(Ls(a, x, s, r), Res.None, 0, null, null) {TestName = "Perform - Xm8 and Shared"};
			// others
			yield return new TestCaseData(Ls(a, c, r), Res.Core, 1, "A", @"xactimate\xactimate.shared\xm8core\Core.Data.Res")
				{TestName = "Perform - Core"};
			yield return new TestCaseData(Ls(a, x, r), Res.Xm8, 1, "A", @"xactimate\xactimate.shared\xm8core\Xm8.Data.Res")
				{TestName = "Perform - Xm8"};
			yield return new TestCaseData(Ls(b, s, r), Res.Shared, 1, "B", @"xactimate\xactimate.shared\Shared.Res")
				{TestName = "Perform - Shared"};
			yield return new TestCaseData(Ls(d, r), Res.None, 2, "Def", "")
				{TestName = "Perform - Def"};

		}	// end GetPerformTestData()

		private static List<string> Ls(params string[] args) => args.ToList();

		#endregion Helpers

		#region Inner Classes

		private class ResxAStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "a";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path)
			{
				_count = count;
				_res = res;
				_path = path;
				_cmd = "A";

			}	// end Perform(List<string>, int, Res, string)

			#endregion Overrides of ResxBase

		}	// end ResxAStub

		private class ResxBStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "b";

			/// <inheritdoc />
			public override bool ShowAll => true;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path)
			{
				_count = count;
				_res = res;
				_path = path;
				_cmd = "B";

			}	// end Perform(List<string>, int, Res, string)

			#endregion Overrides of ResxBase

		}	// end ResxBStub

		private class ResxDefStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "";

			/// <inheritdoc />
			public override string Input => "";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(List<string> args, int count, Res res, string path)
			{
				_count = count;
				_res = res;
				_path = path;
				_cmd = "Def";

			}	// end Perform(List<string>, int, Res, string)

			#endregion Overrides of ResxBase

		}	// end ResxDefStub

		private class DirStub : BaseDirsStub
		{
			#region Implementation of IDirectories

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path) { }

			#endregion Implementation of IDirectories

		}	// end DirStub

		#endregion Inner Classes

	}	// end ResxMainDeciderTests

}	// end HubUnitTest.Decider.Resx
