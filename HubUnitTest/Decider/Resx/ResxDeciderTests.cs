﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Commands.Resx;
using Hub.Decider;
using Hub.Decider.Resx;
using Hub.Enum;

using Args = System.Collections.Generic.List<string>;

namespace HubUnitTest.Decider.Resx
{
	/// <summary>
	/// tests <see cref="ResxDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class ResxDeciderTests
	{
		#region Test

		/// <summary>
		/// tests <see cref="BaseDecider{T}.InitCommands(List{T},T,T)" />
		/// </summary>
		[Test]
		public void TestDecide()
		{
			var resx = new List<ResxBase> {new ResxAStub(), new ResxBStub()};
			ResxDecider.InitCommands(resx, new ResxDefStub(), new ResxNoneStub());
			var stub = new ResxDeciderStub();
			Assert.AreEqual("This is A", stub.FigureItOut(new List<string> {"aa"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new List<string> {"AA"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new List<string> {"aA"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new List<string> {"Aa"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new List<string> {"bb"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new List<string> {"BB"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new List<string> {"bB"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new List<string> {"Bb"}).Description);
			Assert.AreEqual("This is Default", stub.FigureItOut(new List<string> {"ab"}).Description);
			Assert.AreEqual("This is Default", stub.FigureItOut(new List<string> {""}).Description);
			Assert.AreEqual("This is none", stub.FigureItOut(new List<string>()).Description);
			Assert.AreEqual("This is none", stub.FigureItOut(null).Description);

		}

		#endregion Test

		#region Inner Classes

		private class ResxDeciderStub : ResxDecider
		{
			///
			public ResxBase FigureItOut(Args args) => Decide(args);
		}

		private class ResxAStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "This is A";

			/// <inheritdoc />
			public override string Input => "aa";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(Args args, int count, Res res, string path) { }

			#endregion Overrides of ResxBase
		}

		private class ResxBStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "This is B";

			/// <inheritdoc />
			public override string Input => "bb";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(Args args, int count, Res res, string path) { }

			#endregion Overrides of ResxBase
		}

		private class ResxDefStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "This is Default";

			/// <inheritdoc />
			public override string Input => "";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(Args args, int count, Res res, string path) { }

			#endregion Overrides of ResxBase
		}

		private class ResxNoneStub : ResxBase
		{
			#region Overrides of ResxBase

			/// <inheritdoc />
			public override string Description => "This is none";

			/// <inheritdoc />
			public override string Input => "";

			/// <inheritdoc />
			public override bool ShowAll => false;

			/// <inheritdoc />
			public override void Help() { }

			/// <inheritdoc />
			public override void Perform(Args args, int count, Res res, string path) { }

			#endregion Overrides of ResxBase
		}

		#endregion Inner Classes
	}
}
