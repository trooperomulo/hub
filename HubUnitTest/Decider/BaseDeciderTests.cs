﻿using System.Diagnostics.CodeAnalysis;

using Hub.Decider;
using Hub.Interface;

using NUnit.Framework;

using RyanCoreTests;

using Args = System.Collections.Generic.List<string>;

namespace HubUnitTest.Decider
{
	/// <summary>
	/// tests <see cref="BaseDecider{T}" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class BaseDeciderTests
	{
		#region Test
		
		/// <summary>
		/// tests <see cref="BaseDecider{T}.GetRest" />
		/// </summary>
		[Test]
		public void TestGetRest()
		{
			var stub = new BaseDeciderStub();
			var test0 = new Args { "a", "b", "c", "d", "e", "f" };
			var test1 = stub.GetTheRest(test0);
			var test2 = stub.GetTheRest(test1);
			var test3 = stub.GetTheRest(test2);
			var test4 = stub.GetTheRest(test3);
			var test5 = stub.GetTheRest(test4);
			var test6 = stub.GetTheRest(test5);
			var test7 = stub.GetTheRest(test6);
			var test8 = stub.GetTheRest(null);

			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args { "b", "c", "d", "e", "f" }, test1));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args { "c", "d", "e", "f" }, test2));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args { "d", "e", "f" }, test3));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args { "e", "f" }, test4));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args { "f" }, test5));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args(), test6));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args(), test7));
			Assert.IsTrue(CoreTestUtils.ListsAreEqual(new Args(), test8));

		}	// end TestGetRest()

		#endregion Test

		#region Inner Class

		private class BaseDeciderStub : BaseDecider<IInput>
		{
			public Args GetTheRest(Args args) => GetRest(args);

		}	// end BaseDeciderStub

		#endregion Inner Class

	}	// end BaseDeciderTests

}	// end HubUnitTest.Decider
