﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Decider.Command;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Decider.Command
{
	/// <summary>
	/// Tests <see cref="CommandMainDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class CommandMainDeciderTests
	{
		#region Variable

		private static string _output;

		#endregion Variable

		#region Test

		/// <summary>
		/// tests <see cref="CommandMainDecider.Perform" />
		/// </summary>
		[Test]
		[TestCase("a", "rdt", "abc", "A was called 'rdt' 'abc'")]
		[TestCase("bb", "zy", "lmt", "B was called 'zy' 'lmt'")]
		[TestCase("ajt", "lrt", "kit", "Def was called 'ajt' 'lrt' 'kit'")]
		public void TestPerform(string a, string b, string c, string expected)
		{
			CommandDecider.InitCommands(new List<ICommand> {new MainAStub(), new MainBStub()}, new MainDefStub(), null);
			CommandMainDecider.Instance.Perform(new List<string> {a, b, c});
			Assert.AreEqual(expected, _output);
		}

		#endregion Test

		#region Internal Classes

		private class MainAStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> {"a", "aa"};

			/// <inheritdoc />
			public override void Help(List<string> args) => _output = "";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _output = $"A was called {Utils.CombineArgs(args)}";

			#endregion Implementation of ICommand
		}

		private class MainBStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> {"b", "bb"};

			/// <inheritdoc />
			public override void Help(List<string> args) => _output = "";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _output = $"B was called {Utils.CombineArgs(args)}";

			#endregion Implementation of ICommand
		}

		private class MainDefStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string>();

			/// <inheritdoc />
			public override void Help(List<string> args) => _output = "";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _output = $"Def was called {Utils.CombineArgs(args)}";

			#endregion Implementation of ICommand
		}

		#endregion Internal Classes
	}
}
