﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Decider;
using Hub.Decider.Command;
using Hub.Interface;
using HubUnitTest.Stubs;

using Args = System.Collections.Generic.List<string>;

namespace HubUnitTest.Decider.Command
{
	/// <summary>
	/// tests <see cref="CommandDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class CommandDeciderTests
	{
		/// <summary>
		/// tests <see cref="BaseDecider{T}.InitCommands(List{T},T,T)" />
		/// </summary>
		[Test]
		public void TestDecide()
		{
			var cmds = new List<ICommand> {new CommandShortcutDeciderTests.DeciderCmdAStub(), new CommandShortcutDeciderTests.DeciderCmdBStub()};
			CommandDecider.InitCommands(cmds, new DeciderCmdDefStub(), new DeciderCmdNoneStub());
			var stub = new CommandDeciderStub();
			Assert.AreEqual("This is A", stub.FigureItOut(new Args {"a"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new Args {"A"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new Args {"aa"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new Args {"AA"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new Args {"aA"}).Description);
			Assert.AreEqual("This is A", stub.FigureItOut(new Args {"Aa"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new Args {"b"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new Args {"bb"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new Args {"B"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new Args {"BB"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new Args {"bB"}).Description);
			Assert.AreEqual("This is B", stub.FigureItOut(new Args {"Bb"}).Description);
			Assert.AreEqual("This is Default", stub.FigureItOut(new Args {"ab"}).Description);
			Assert.AreEqual("This is Default", stub.FigureItOut(new Args {""}).Description);
			Assert.AreEqual("This is none", stub.FigureItOut(new Args()).Description);
			Assert.AreEqual("This is none", stub.FigureItOut(null).Description);
		}

		#region Inner Classes

		private class CommandDeciderStub : CommandDecider
		{
			///
			public ICommand FigureItOut(Args args) => Decide(args);
		}

		///
		private class DeciderCmdDefStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is Default";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string>();

			#endregion Implementation of ICommand
		}

		///
		private class DeciderCmdNoneStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is none";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string>();

			#endregion Implementation of ICommand
		}

		#endregion Inner Classes
	}
}
