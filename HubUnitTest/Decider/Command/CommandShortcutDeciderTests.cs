﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub.Decider.Command;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Decider.Command
{
	/// <summary>
	/// Tests <see cref="CommandShortcutDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class CommandShortcutDeciderTests
	{
		#region Test

		/// <summary>
		/// tests <see cref="CommandShortcutDecider.NeedsShortcut" />
		/// </summary>
		[Test]
		[TestCase("a", false)]
		[TestCase("b", false)]
		[TestCase("c", true)]
		[TestCase("d", true)]
		[TestCase("e", false)]
		public void TestNeedsShortcut(string input, bool expected)
		{
			CommandDecider.InitCommands(
				new List<ICommand>
				{
					new DeciderCmdAStub(),
					new DeciderCmdBStub(),
					new DeciderCmdCStub(),
					new DeciderCmdDStub(),
					new DeciderCmdEStub()
				}, null, null);
			var actual = CommandShortcutDecider.Instance.NeedsShortcut(new List<string> {input});
			Assert.AreEqual(expected, actual);
		}

		#endregion Test

		#region Inner Classes

		///
		public class DeciderCmdAStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is A";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "a", "aa" };

			/// <inheritdoc />
			public override bool NeedShortcut => false;

			#endregion Implementation of ICommand
		}

		///
		public class DeciderCmdBStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is B";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "b", "bb" };

			/// <inheritdoc />
			public override bool NeedShortcut => false;

			#endregion Implementation of ICommand
		}

		///
		private class DeciderCmdCStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is C";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "c", "cc" };

			/// <inheritdoc />
			public override bool NeedShortcut => true;

			#endregion Implementation of ICommand
		}

		///
		private class DeciderCmdDStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is D";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "d", "dd" };

			/// <inheritdoc />
			public override bool NeedShortcut => true;

			#endregion Implementation of ICommand
		}

		///
		private class DeciderCmdEStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override string Description => "This is E";

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> { "e", "ee" };

			/// <inheritdoc />
			public override bool NeedShortcut => false;

			#endregion Implementation of ICommand
		}

		#endregion Inner Classes
	}
}
