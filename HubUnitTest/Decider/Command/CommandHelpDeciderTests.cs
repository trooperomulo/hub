﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using NUnit.Framework;

using Hub;
using Hub.Decider.Command;
using Hub.Interface;

using HubUnitTest.Stubs;

namespace HubUnitTest.Decider.Command
{
	/// <summary>
	/// Tests <see cref="CommandHelpDecider" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class CommandHelpDeciderTests
	{
		#region Variable

		private static string _output;

		#endregion Variable

		#region Test

		/// <summary>
		/// tests <see cref="CommandHelpDecider" />
		/// </summary>
		[Test]
		[TestCase("a", "rdt", "abc", "A was called 'rdt' 'abc'")]
		[TestCase("bb", "zy", "lmt", "B was called 'zy' 'lmt'")]
		[TestCase("ajt", "lrt", "kit", "Def was called 'ajt' 'lrt' 'kit'")]
		public void TestHelp(string a, string b, string c, string expected)
		{
			CommandDecider.InitCommands(new List<ICommand> {new HelpAStub(), new HelpBStub()}, new HelpDefStub(), null);
			CommandHelpDecider.Instance.Help(new List<string> {a, b, c});
			Assert.AreEqual(expected, _output);
		}

		#endregion Test

		#region Internal Classes

		private class HelpAStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> {"a", "aa"};

			/// <inheritdoc />
			public override void Help(List<string> args) => _output = $"A was called {Utils.CombineArgs(args)}";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _output = "";

			#endregion Implementation of ICommand
		}

		private class HelpBStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string> {"b", "bb"};

			/// <inheritdoc />
			public override void Help(List<string> args) => _output = $"B was called {Utils.CombineArgs(args)}";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _output = "";

			#endregion Implementation of ICommand
		}

		private class HelpDefStub : BaseCmdStub
		{
			#region Implementation of ICommand

			/// <inheritdoc />
			public override List<string> Inputs { get; } = new List<string>();

			/// <inheritdoc />
			public override void Help(List<string> args) => _output = $"Def was called {Utils.CombineArgs(args)}";

			/// <inheritdoc />
			public override void Perform(List<string> args) => _output = "";

			#endregion Implementation of ICommand
		}

		#endregion Internal Classes
	}
}
