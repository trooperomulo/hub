﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Hub;
using Hub.Enum;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

using Rnd = HubUnitTest.RandomData;

namespace HubUnitTest
{
	/// <summary>
	/// tests <see cref="Utils" />
	/// </summary>
	[TestFixture]
	[ExcludeFromCodeCoverage]
	public class UtilsTests
	{
		#region Tests
		
		///
		[SetUp]
		public void Setup()
		{
			IoCContainer.Instance.RegisterAsSingleInstance<IDirectories, UtilsDirsStub>(new UtilsDirsStub());

			Utils.DataFolder = "data";

			Env.SetEnvironmentVariable("local", "c:", true);
			Env.SetEnvironmentVariable("version_tree", @"\git\1\xm8", true);
			Env.SetEnvironmentVariable("branch", "branch", true);
		}

		///
		[TearDown]
		public void Teardown() => IoCContainer.Instance.RemoveRegisteredType<IDirectories>();

		/// <summary>
		/// Tests the extension method <see cref="Utils.Add{T1,T2}" />
		/// </summary>
		[Test]
		public void TestAdd()
		{
			var message = "";
			message += TestAdd(1, 2, 3);
			message += TestAdd("one", 2, 3);
			message += TestAdd(1, "two", "thr");
			message += TestAdd("one", "two", "thr");

			Assert.IsTrue(message.IsNullOrEmpty(), message);
		}

		private static string TestAdd<T1, T2>(T1 key, T2 val1, T2 val2)
		{
			var preface = $"{Environment.NewLine}Error in {nameof(TestAdd)} for T1 - {typeof(T1)}, T2 - {typeof(T2)}";
			var dict = new Dictionary<T1, List<T2>>();

			if (dict.ContainsKey(key))
			{
				return $"{preface} - it shouldn't have the key yet";
			}

			dict.Add(key, val1);

			if (!dict.ContainsKey(key))
			{
				return $"{preface} - it should contain the key now";
			}

			if (dict[key].Count != 1)
			{
				return $"{preface} - wrong number of values after 1 add";
			}

			if (!Equals(dict[key][0], val1))
			{
				return $"{preface} - wrong item added after 1 add";
			}

			dict.Add(key, val2);

			if (dict[key].Count != 2)
			{
				return $"{preface} - wrong number of values after 2 adds";
			}

			if (!Equals(dict[key][0], val1))
			{
				return $"{preface} - wrong item in index 0 after 2 adds";
			}

			if (!Equals(dict[key][1], val2))
			{
				return $"{preface} - wrong item in index 1 after 2 adds";
			}

			return "";
		}

		/// <summary>
		/// tests <see cref="Utils.AddIfExists{T1,T2}"/>
		/// </summary>
		[Test]
		public void TestAddIfExists()
		{
			var message = "";
			message += TestAddIfExists(Rnd.Int, Rnd.Int);
			message += TestAddIfExists(Rnd.GetRandomStr, Rnd.Int);
			message += TestAddIfExists(Rnd.Int, Rnd.GetRandomStr);
			message += TestAddIfExists(Rnd.GetRandomStr, Rnd.GetRandomStr);

			Assert.IsTrue(message.IsNullOrEmpty(), message);
		}

		private static string TestAddIfExists<T1, T2>(Func<T1> getKey, Func<T2> getVal)
		{
			var preface = $"{Environment.NewLine}Error in {nameof(TestAddIfExists)} for T1 - {typeof(T1)}, T2 - {typeof(T2)}";
			var dict = new Dictionary<T1, List<T2>>();

			var key = getKey();
			var val1 = getVal();
			var val2 = getVal();

			dict.AddIfExists(key, val1);

			if (dict.ContainsKey(key))
			{
				return $"{preface} - it shouldn't have the key yet";
			}

			dict.Add(key, val1);

			if (!dict.ContainsKey(key))
			{
				return $"{preface} - it should have the key now";
			}

			dict.AddIfExists(key, val2);

			if (dict[key].Count != 2)
			{
				return $"{preface} - wrong number of values after 2 adds";
			}

			if (!Equals(dict[key][0], val1))
			{
				return $"{preface} - wrong item in index 0 after 2 adds";
			}

			if (!Equals(dict[key][1], val2))
			{
				return $"{preface} - wrong item in index 1 after 2 adds";
			}

			return "";
		}

		/// <summary>
		/// tests <see cref="Utils.BranchIsNextGen" />
		/// </summary>
		/// <param name="branch"></param>
		/// <param name="expected"></param>
		[Test]
		[TestCase("nextgen/XWNG-187_Addresses_rdt", true)]
		[TestCase("feature/XWNG-187_Addresses_rdt", false)]
		public void TestBranchIsNextGen(string branch, bool expected)
		{
			Assert.AreEqual(expected, Utils.BranchIsNextGen(branch));
		}

		/// <summary>
		/// tests <see cref="Utils.Center" />
		/// </summary>
		[Test]
		[TestCase("test", 5, "test ")]
		[TestCase("test", 6, " test ")]
		[TestCase("test", 7, " test  ")]
		[TestCase("test", 8, "  test  ")]
		[TestCase("test", 9, "  test   ")]
		[TestCase("test", 10, "   test   ")]
		[TestCase("Xm8.App.Internal.SchemaAutomation.UnitTests", 70, "             Xm8.App.Internal.SchemaAutomation.UnitTests              ")]
		public void TestCenter(string text, int totalLen, string expected)
		{
			Assert.AreEqual(expected, Utils.Center(text, totalLen));
		}

		/// <summary>
		/// tests <see cref="Utils.Center" /> when the passed in text is longer than the desired length
		/// </summary>
		[Test]
		public void TestCenterTooLong()
		{
			var ex = Assert.Throws<ArgumentException>(() => Utils.Center("This is too long", 5));
			Assert.AreEqual("Passed in string is longer (16) than desired length (5)", ex.Message);
		}

		/// <summary>
		/// tests <see cref="Utils.CombineArgs" />
		/// </summary>
		[Test]
		public void TestCombineArgs()
		{
			Func<List<string>, string> combine = Utils.CombineArgs;

			Assert.AreEqual("", combine(null));
			Assert.AreEqual("", combine(new List<string>()));
			Assert.AreEqual("'a'", combine(new List<string> {"a"}));
			Assert.AreEqual("'a' 'b'", combine(new List<string> {"a", "b"}));
			Assert.AreEqual("'a' 'b' 'c'", combine(new List<string> {"a", "b", "c"}));
			Assert.AreEqual("'a' 'b' 'c' 'd'", combine(new List<string> {"a", "b", "c", "d"}));
		}

		/// <summary>
		/// tests <see cref="Utils.HasTrkArg" />
		/// </summary>
		[Test]
		[TestCase("eh", "-tr", "-dm", 2)]
		[TestCase("eh", "-tr", "-t", 2)]
		[TestCase("eh", "-dm", "-t", 2)]
		[TestCase("eh", "-t", "-dm", 2)]
		[TestCase("eh", "-t", "-tr", 2)]
		[TestCase("-tr", "eh", "wha", 2)]
		[TestCase("eh", "-t", "-r", 3)]
		public void TestHasTrkArg(string one, string two, string thr, int expected)
		{
			TestArgsMethod(one, two, thr, expected, Utils.HasTrkArg);
		}

		/// <summary>
		/// tests <see cref="Utils.HasXm8Arg" />
		/// </summary>
		[Test]
		[TestCase("eh", "-x", "-l", 2)]
		[TestCase("eh", "-x", "-t", 2)]
		[TestCase("eh", "-l", "-t", 2)]
		[TestCase("eh", "-t", "-l", 2)]
		[TestCase("eh", "-t", "-x", 2)]
		[TestCase("-x", "eh", "wha", 2)]
		[TestCase("eh", "-t", "-r", 3)]
		public void TestHasXm8Arg(string one, string two, string thr, int expected)
		{
			TestArgsMethod(one, two, thr, expected, Utils.HasXm8Arg);
		}

		/// <summary>
		/// tests <see cref="Utils.IsFirstTrk" />
		/// </summary>
		[Test]
		[TestCase("-tr", "-t", 1)]
		[TestCase("-dm", "-t", 1)]
		[TestCase("-t", "-tr", 2)]
		[TestCase("-t", "-r", 2)]
		public void TestIsFirstTrk(string one, string two, int expected)
		{
			TestArgsMethod(one, two, expected, Utils.IsFirstTrk);
		}

		/// <summary>
		/// tests <see cref="Utils.IsFirstUnt" />
		/// </summary>
		[Test]
		[TestCase("-t", "-tr", 1)]
		[TestCase("-u", "-tr", 1)]
		[TestCase("-tr", "-t", 2)]
		[TestCase("-tr", "-r", 2)]
		public void TestIsFirstUnt(string one, string two, int expected)
		{
			TestArgsMethod(one, two, expected, Utils.IsFirstUnt);
		}

		/// <summary>
		/// tests <see cref="Utils.IsFirstXm8" />
		/// </summary>
		[Test]
		[TestCase("-x", "-tr", 1)]
		[TestCase("-l", "-tr", 1)]
		[TestCase("-tr", "-x", 2)]
		[TestCase("-tr", "-r", 2)]
		public void TestIsFirstXm8(string one, string two, int expected)
		{
			TestArgsMethod(one, two, expected, Utils.IsFirstXm8);
		}

		/// <summary>
		/// tests <see cref="Utils.NavigateToRepo" />
		/// </summary>
		/// <param name="repo"></param>
		/// <param name="expected"></param>
		[Test]
		[TestCase(Repository.Xm8, "branch")]
		[TestCase(Repository.Data, "data")]
		public void TestNavigateToRepo(Repository repo, string expected)
		{
			Utils.NavigateToRepo(repo);
			Assert.AreEqual($@"c:\git\1\xm8\..\{expected}", IoCContainer.Instance.Resolve<IDirectories>().GetCurrentDirectory());
		}

		/// <summary>
		/// tests <see cref="Utils.NavigateToRepo" /> with an invalid repo
		/// </summary>
		[Test]
		public void TestNavigateToRepoWithInvalidRepo()
		{
			var ex = Assert.Throws<ArgumentOutOfRangeException>(() => Utils.NavigateToRepo((Repository) 16));
			Assert.AreEqual("Exception of type 'System.ArgumentOutOfRangeException' was thrown.\r\nParameter name: repo\r\nActual value was 16.", ex.Message);
		}

		/// <summary>
		/// tests <see cref="Utils.ReadFirstArg(List{string},string)" />
		/// </summary>
		[Test]
		public void TestReadFirstArgOne()
		{
			var args = new List<string> {"eh", "whatever", "see"};

			Assert.IsFalse(Utils.ReadFirstArg(null, "eh"));
			Assert.IsFalse(Utils.ReadFirstArg(new List<string>(), "eh"));
			Assert.IsFalse(Utils.ReadFirstArg(args, null));
			Assert.IsFalse(Utils.ReadFirstArg(args, ""));
			Assert.IsFalse(Utils.ReadFirstArg(args, "whatever"));
			Assert.IsTrue(Utils.ReadFirstArg(args, "eh"));
			Assert.AreEqual(2, args.Count);
			Assert.IsFalse(Utils.ReadFirstArg(args, "eh"));
			Assert.AreEqual(2, args.Count);
			Assert.IsTrue(Utils.ReadFirstArg(args, "whatever"));
			Assert.AreEqual(1, args.Count);
		}

		/// <summary>
		/// tests <see cref="Utils.ReadFirstArg(List{string},string,string)" />
		/// </summary>
		[Test]
		public void TestReadFirstArgTwo()
		{
			var args = new List<string> { "eh", "whatever", "see" };

			Assert.IsFalse(Utils.ReadFirstArg(args, "nope", "nuh-uh"));
			Assert.IsTrue(Utils.ReadFirstArg(args, "eh", "nope"));
			Assert.IsTrue(Utils.ReadFirstArg(args, "nope", "whatever"));
		}

		/// <summary>
		/// tests <see cref="Utils.ReadFirstArg(List{string},string,out string)" />
		/// </summary>
		[Test]
		public void TestReadFirstArgOut()
		{
			var args = new List<string> {"one", "1", "two", "2", "leftover"};

			// failures
			Assert.IsFalse(Utils.ReadFirstArg(null, "eh", out var val));
			Assert.AreEqual("", val);
			Assert.IsFalse(Utils.ReadFirstArg(new List<string>(), "eh", out val));
			Assert.AreEqual("", val);
			Assert.IsFalse(Utils.ReadFirstArg(args, null, out val));
			Assert.AreEqual("", val);
			Assert.IsFalse(Utils.ReadFirstArg(args, "", out val));
			Assert.AreEqual("", val);
			Assert.IsFalse(Utils.ReadFirstArg(args, "two", out val));
			Assert.AreEqual("", val);

			Assert.IsTrue(Utils.ReadFirstArg(args, "one", out val));
			Assert.AreEqual("1", val);
			Assert.AreEqual(3, args.Count);

			Assert.IsTrue(Utils.ReadFirstArg(args, "two", out val));
			Assert.AreEqual("2", val);
			Assert.AreEqual(1, args.Count);

			Assert.IsFalse(Utils.ReadFirstArg(args, "leftover", out val));
			Assert.AreEqual("", val);
			Assert.AreEqual(1, args.Count);
		}

		/// <summary>
		/// tests <see cref="Utils.Update{T1,T2}" />
		/// </summary>
		[Test]
		public void TestUpdate()
		{
			var message = "";
			message += TestUpdate(1, 2, 3, 4);
			message += TestUpdate("one", 2, 3, 4);
			message += TestUpdate(1, "two", "thr", "fou");
			message += TestUpdate("one", "two", "thr", "fou");

			Assert.IsTrue(message.IsNullOrEmpty(), message);
		}

		private static string TestUpdate<T1, T2>(T1 key, T2 val1, T2 val2, T2 val3)
		{
			var preface = $"{Environment.NewLine}Error in {nameof(TestUpdate)} for T1 - {typeof(T1)}, T2 - {typeof(T2)}";
			var dict = new Dictionary<T1, T2>();

			dict.Update(key, val1);

			if (dict.ContainsKey(key))
			{
				return $"{preface} - the key shouldn't have been added";
			}

			dict.Add(key, val1);

			if (!dict.ContainsKey(key))
			{
				return $"{preface} - the key should be there now";
			}

			if (!Equals(dict[key], val1))
			{
				return $"{preface} - somehow the wrong value was added";
			}

			dict.Update(key, val2);

			if (!dict.ContainsKey(key))
			{
				return $"{preface} - the key should still be there";
			}

			if (!Equals(dict[key], val2))
			{
				return $"{preface} - the value wasn't updated";
			}

			dict.Update(key, val3);

			if (!dict.ContainsKey(key))
			{
				return $"{preface} - there's no reason for it to be gone now";
			}

			if (!Equals(dict[key], val3))
			{
				return $"{preface} - the value didn't update the 2nd time";
			}

			return "";
		}
		
		#endregion Tests

		#region Helpers

		private static void TestArgsMethod(string one, string two, int expCount, Func<List<string>, bool> testMethod)
		{
			var args = new List<string> {one, two};
			Assert.AreEqual(expCount == 1, testMethod(args));
			Assert.AreEqual(expCount, args.Count);
		}

		private static void TestArgsMethod(string one, string two, string thr, int expCount, Func<List<string>, bool> testMethod)
		{
			var args = new List<string> {one, two, thr};
			Assert.AreEqual(expCount < 3, testMethod(args));
			Assert.AreEqual(expCount, args.Count);
		}

		#endregion Helpers

		#region Inner Class

		///
		public class UtilsDirsStub : BaseDirsStub
		{
			#region Variable

			private string _location;

			#endregion Variable

			#region Implementation of IDirectories

			/// <inheritdoc />
			public override bool Exists(string path) => true;

			/// <inheritdoc />
			public override string GetCurrentDirectory() => _location;

			/// <inheritdoc />
			public override void SetCurrentDirectory(string path) => _location = path;

			#endregion Implementation of IDirectories
		}

		#endregion Inner Class
	}
}
