﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Hub;
using Hub.Commands.Param;
using Hub.Commands.Resx;
using Hub.Interface;

using HubUnitTest.Stubs;

using NUnit.Framework;

using RyanCoreTests;

// ReSharper disable MemberCanBePrivate.Global
namespace HubUnitTest
{
	/// <summary>
	/// useful methods for unit test classes
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class UnitTestUtil : CoreTestUtils
	{
		/// <summary>
		/// combines the test data from two different sets of test cases
		/// </summary>
		/// <returns>a new set of test cases, length equal to case1.Length * case2.Length</returns>
		public static IEnumerable CombineTestData(IEnumerable cases1, IEnumerable cases2)
		{
			var set1 = cases1.Cast<TestCaseData>().ToList();
			var set2 = cases2.Cast<TestCaseData>().ToList();

			foreach (var case1 in set1)
			{
				foreach (var case2 in set2)
				{
					yield return GetTestCase(case1, case2);
				}	// end inner foreach
			}	// end outer foreach

		}	// end CombineTestData(IEnumerable, IEnumerable)

		private static TestCaseData GetTestCase(TestCaseData case1, TestCaseData case2)
		{
			var arguments = case1.Arguments.ToList();
			arguments.AddRange(case2.Arguments);

			return new TestCaseData(arguments.ToArray()) {TestName = $"{case1.TestName}-{case2.TestName}"};

		}	// end GetTestCase(TestCaseData, TestCaseData)

		/// <summary>
		/// Gets the path to a file in the TestData folder
		/// </summary>
		public static string GetTestPath(string filename, string commandFolder)
		{
			return GetTestPath(nameof(HubUnitTest), "TestData", commandFolder, filename);
		}

		private static void TestCommand(string desc, List<string> descs, Action help, List<string> helps)
		{
			var lower = desc.ToLower();
			foreach (var d in descs)
			{
				Assert.IsTrue(lower.Contains(d), $"'{d}' wasn't in the description");
			}	// end foreach

			if (helps != null)
			{
				var cons = new BaseConsStub();
				IoCContainer.Instance.RegisterAsSingleInstance<IConsole, BaseConsStub>(cons);

				help();
				
				TestStringContainsItems(cons.Output.ToLower(), helps);

				IoCContainer.Instance.RemoveRegisteredType<IConsole>();
			}	// end if
			
		}	// end TestCommand(string, List<string>, Action)

		///
		public static void TestCommand(ICommand cmd, List<string> descs, bool subs, bool scut, List<string> inputs, List<string> helps, List<string> args = null)
		{
			Assert.AreEqual(subs, cmd.HasSubCommands, $"{nameof(cmd.HasSubCommands)} value was wrong");
			Assert.AreEqual(scut, cmd.NeedShortcut, $"{nameof(cmd.NeedShortcut)} value was wrong");
			Assert.IsTrue(ListsHaveSameItems(inputs, cmd.Inputs), "inputs didn't match");

			helps?.Add(Prep.Slash(cmd.Inputs.ToArray()));

			TestCommand(cmd.Description, descs, () => cmd.Help(args), helps);
		}

		///
		public static void TestParamCommand(ParamBase prm, List<string> descs, string input, List<string> helps)
		{
			Assert.AreEqual(input, prm.Input, $"{nameof(prm.Input)} was wrong");

			helps.Add(prm.Input);

			TestCommand(prm.Description, descs, prm.Help, helps);

		}	// end TestParamCommand(ParamBase, List<string>, string, List<string>)

		///
		public static void TestResxCommand(ResxBase resx, List<string> descs, string input, bool showAll, List<string> helps)
		{
			Assert.AreEqual(showAll, resx.ShowAll, $"{nameof(resx.ShowAll)} value was wrong");
			Assert.AreEqual(input, resx.Input, $"{nameof(resx.Input)} was wrong");

			helps.Add(resx.Input);

			TestCommand(resx.Description, descs, resx.Help, helps);
		}

		///
		// ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Global
		public static void TestStringContainsItems(string str, List<string> items, string where = "help")
		{
			var notInList = "";
			foreach (var item in items)
			{
				if (!str.Contains(item))
				{
					notInList += $", {item}";
				}
			}

			if (notInList.StartsWith(", "))
			{
				notInList = notInList.Substring(2);
			}

			Assert.IsTrue(notInList.IsNullOrEmpty(),
				$"The following didn't show up in the {where}:{Environment.NewLine}{notInList}");
		}
	}
}
