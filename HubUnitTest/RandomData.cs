﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

using Hub;
using Hub.Commands.Helpers.UnitTest.Displayer;

namespace HubUnitTest
{
	/// <summary>
	/// creates random data for tests
	/// </summary>
	// ReSharper disable MemberCanBePrivate.Global
	[ExcludeFromCodeCoverage]
	public static class RandomData
	{
		private static readonly Random Rnd = new Random();

		/// <summary>
		/// Returns a random bool
		/// </summary>
		public static bool Bool() => Int(2) == 1;

		/// <summary>
		/// Returns a random character
		/// </summary>
		public static char Char() => (char) Int(128);

		/// <summary>
		/// Returns a random double
		/// </summary>
		// ReSharper disable once IdentifierTypo
		public static double Doub(int? decPres = 2, int mult = 1)
		{
			var val = Rnd.NextDouble() * mult;
			if (decPres.HasValue)
			{
				val = Math.Round(val, decPres.Value);
			}

			return val;
		}

		/// <summary>
		/// Returns a random integer.
		/// </summary>
		public static int Int() => Rnd.Next();

		/// <summary>
		/// Returns a random integer.
		/// </summary>
		/// <param name="max">The exclusive upper bound of the random number to be generated. max must be greater than or equal to 0.</param>
		public static int Int(int max) => Rnd.Next(max);

		/// <summary>
		/// Returns a random integer.
		/// </summary>
		/// <param name="min">The inclusive lower bound of the random number returned.</param>
		/// <param name="max">The exclusive upper bound of the random number to be generated. max must be greater than or equal to 0.</param>
		public static int Int(int min, int max) => Rnd.Next(min, max);

		/// <summary>
		/// returns a nullable bool, with an equal chance of null, true and false
		/// </summary>
		public static bool? NullBool()
		{
			switch (Int(3))
			{
				case 0:
					return null;
				case 1:
					return false;
				case 2:
					return true;
			}

			return null;

		}	// end NullBool()


		/// <summary>
		/// returns a random letter character
		/// </summary>
		public static char GetRandomCharLetter() => Bool() ? GetRandomCharLetterLower() : GetRandomCharLetterUpper();

		/// <summary>
		/// returns a random upper case letter character
		/// </summary>
		public static char GetRandomCharLetterLower() => (char) ('a' + Int(26));
		
		/// <summary>
		/// returns a random upper case letter character
		/// </summary>
		public static char GetRandomCharLetterUpper() => (char) ('A' + Int(26));

		///// <summary>
		///// returns a random upper case letter
		///// </summary>
		//public static string GetRandomLetter() => ((char)('A' + Int(26))).ToString();

		///// <summary>
		///// returns a random lower case letter
		///// </summary>
		//public static string GetRandomLetterLower() => ((char) ('a' + Int(26))).ToString();

		/// <summary>
		/// creates a dictionary of types string and List{string}, with random data
		/// </summary>
		/// <param name="getKey">the method to generate the random keys, defaults to <see cref="GetRandomStr" /></param>
		/// <param name="getVal">the method to generate the random values, defaults to <see cref="GetRandomListString" /></param>
		public static Dictionary<string, List<string>> GetRandomDictStrListStr(Func<string> getKey = null, Func<List<string>> getVal = null)
		{
			var dict = new Dictionary<string, List<string>>();

			if (getKey == null)
			{
				getKey = GetRandomStr;
			}

			if (getVal == null)
			{
				getVal = () => GetRandomListString();
			}

			// fill in up to 11 items
			var num = Int(1, 11);

			for (var i = 0; i < num; i++)
			{
				var key = getKey();
				if (!dict.ContainsKey(key))
				{
					dict.Add(key, getVal());
				}
			}

			return dict;
		}

		/// <summary>
		/// returns a dictionary of types string and List{(string, string)}, with random data
		/// </summary>
		public static Dictionary<string, List<(string lang, string val)>> GetRandomDictStrListStrStr()
		{
			var dict = new Dictionary<string, List<(string lang, string value)>>();

			// fill in up to 11 items
			var num = Int(3, 11);

			for (var i = 0; i < num; i++)
			{
				var key = GetRandomStr();
				if (!dict.ContainsKey(key))
				{
					dict.Add(key, GetRandomListStrStr());
				}
			}

			return dict;
		}

		/// <summary>
		/// creates a dictionary of types string and string, with random data
		/// </summary>
		/// <param name="getKey">the method to generate the random keys, defaults to <see cref="GetRandomStr" /></param>
		/// <param name="getVal">the method to generate the random values, defaults to <see cref="GetRandomStr" /></param>
		public static Dictionary<string, string> GetRandomDictStringString(Func<string> getKey = null, Func<string> getVal = null)
		{
			var dict = new Dictionary<string, string>();

			if (getKey == null)
			{
				getKey = GetRandomStr;
			}

			if (getVal == null)
			{
				getVal = GetRandomStr;
			}

			// fill in up to 11 items
			var num = Int(3, 11);

			for (var i = 0; i < num; i++)
			{
				var key = getKey();
				if (!dict.ContainsKey(key))
				{
					dict.Add(key, getVal());
				}
			}

			return dict;
		}

		/// <summary>
		/// returns a random item from the given list
		/// </summary>
		public static T GetRandomItemFromList<T>(List<T> list) => list[Int(0, list.Count)];

		/// <summary>
		/// returns one of the keys from the given dictionary
		/// </summary>
		public static T1 GetRandomKeyFromDictionary<T1, T2>(Dictionary<T1, T2> dict)
		{
			var keys = dict.Keys.ToList();
			return keys[Int(keys.Count)];
		}

		/// <summary>
		/// returns a list of random, single upper case letters
		/// </summary>
		public static List<string> GetRandomKeys()
		{
			var keys = new List<string>();

			var count = Int(6, 16);
			for (var i = 0; i < count; i++)
			{
				var key = GetRandomLetter();
				if (!keys.Contains(key))
				{
					keys.Add(key);
				}
			}

			return keys;
		}

		/// <summary>
		/// returns a random upper case letter
		/// </summary>
		public static string GetRandomLetter() => GetRandomCharLetterUpper().ToString();

		/// <summary>
		/// returns a random lower case letter
		/// </summary>
		public static string GetRandomLetterLower() => GetRandomCharLetterLower().ToString();

		/// <summary>
		/// returns a list of random ints
		/// </summary>
		/// <param name="max">the maximum any int should be, exclusive</param>
		/// <param name="count">the maximum number of entries in the list</param>
		/// <returns></returns>
		public static List<int> GetRandomListInt(int max, int count)
		{
			var list = new List<int>();

			for (var i = 0; i < count; i++)
			{
				var num = Int(max);
				if (!list.Contains(num))
				{
					list.Add(num);
				}
			}

			return list;
		}

		/// <summary>
		/// returns a list of random strings
		/// </summary>
		public static List<string> GetRandomListString(int min = 3, int max = 11)
		{
			var list = new List<string>();

			var num = Int(min, max);

			for (var i = 0; i < num; i++)
			{
				TryToAddRandomStringToList(list);
			}

			return list;
		}

		/// <summary>
		/// returns a list of random strings of a set length
		/// </summary>
		/// <param name="length">how many strings should be in the list</param>
		public static List<string> GetRandomListStringSetLength(int length)
		{
			var list = new List<string>();

			while (list.Count < length)
			{
				TryToAddRandomStringToList(list);
			}

			return list;
		}

		/// <summary>
		/// returns a list of random key, value pairs
		/// </summary>
		public static List<(string key, string value)> GetRandomListStrStr()
		{
			var pairs = new List<(string key, string value)>();

			// fill in up to 11 items
			var num = Int(3, 11);

			for (var i = 0; i < num; i++)
			{
				TryToAddRandomTupleToList(pairs);
			}

			return pairs;
		}

		/// <summary>
		/// returns a list of random key, value pairs of a set length
		/// </summary>
		public static List<(string key, string value)> GetRandomListStrStrSetLength(int length)
		{
			var pairs = new List<(string key, string value)>();

			while (pairs.Count < length)
			{
				TryToAddRandomTupleToList(pairs);
			}

			return pairs;
		}

		/// <summary>
		/// returns a random string, comprised of numbers up to 99
		/// </summary>
		public static string GetRandomStr() => Int(100).ToString();

		/// <summary>
		/// Gets a string of random letters, upper and lower, of a set length
		/// </summary>
		public static string GetRandomStrSetLength(int length)
		{
			// ReSharper disable StringLiteralTypo
			const string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			// ReSharper restore StringLiteralTypo
			char[] chars = new char[length];

			for (int i = 0; i < length; i++)
			{
				chars[i] = allowedChars[Int(0, allowedChars.Length)];

			}	// end for

			return new string(chars);

		}	// end GetRandomStrSetLength(int)

		/// <summary>
		/// returns a random string, comprised of capital letters
		/// </summary>
		public static string GetRandomWord() => GetRandomWord(null);

		/// <summary>
		/// returns a random string, comprised of capital letters
		/// </summary>
		/// <param name="len">how many letters in the word</param>
		public static string GetRandomWord(int? len) => RandomWord(len, GetRandomLetter);

		/// <summary>
		/// returns a random string, comprised of lower letters
		/// </summary>
		public static string GetRandomWordLower() => GetRandomWordLower(null);

		/// <summary>
		/// returns a random string, comprised of lower letters
		/// </summary>
		/// <param name="len">how many letters in the word</param>
		public static string GetRandomWordLower(int? len) => RandomWord(len, GetRandomLetterLower);

		/// <summary>
		/// creates an instance of <see cref="TestProgressTimeDisplayer" /> filled with random data, including the given solution
		/// </summary>
		// ReSharper disable once IdentifierTypo
		public static TestProgressTimeDisplayer GetRandomlyFilledTptd(string solToRun = null)
		{
			// ReSharper disable once IdentifierTypo
			var disp = new TestProgressTimeDisplayer();

			// add anywhere from 0 to 10 solutions
			var times = Int(11);

			double RndTime() => (double)(Int(10000) + 1) / 100;

			void AddSol(string sol)
			{
				disp.TestToBeRun(sol, RndTime());
				if (Int(2) == 1)
				{
					disp.TestHasFinished(sol, RndTime());
				}
			}

			for (var i = 0; i < times; i++)
			{
				AddSol(Int(1000).ToString());
			}

			if (!solToRun.IsNullOrEmpty())
			{
				disp.TestToBeRun(solToRun, RndTime());
			}

			return disp;
		}

		private static string RandomWord(int? len, Func<string> getter)
		{
			if (!len.HasValue)
			{
				len = Int(3, 11);   // make the word between 3 and 10 letters long
			}

			var word = new StringBuilder();
			for (var i = 0; i < len; i++)
			{
				word.Append(getter());
			}

			return word.ToString();
		}

		private static void TryToAddRandomStringToList(List<string> list)
		{
			var str = GetRandomStr();
			if (!list.Contains(str))
			{
				list.Add(str);
			}
		}

		private static void TryToAddRandomTupleToList(List<(string key, string value)> list)
		{
			var key = GetRandomWordLower();
			var value = GetRandomWordLower();
			var tuple = (key, value);
			if (!list.Contains(tuple))
			{
				list.Add(tuple);
			}
		}
	}
}
